import numpy as np
import pyPICommands
import time

from py_lab.config import degrees, number_types, CONF_DT_50
from py_lab.utils import select_movement

# from py_lab.config import CONF_SMC_100

# Default values
DEFAULT_MOVE_TIME = 1  # 1 second
DEFAULT_MAX_VEL = {
    "DT50": 100  # 100 mm/s
}
DEFAULT_POS_UNITS = 'rad'
DEFAULT_VEL_UNITS = 'rad/s'

# TODO
# Check speed
# Check movement range


class Motor_Rotatory(object):
    """
    General class for rotatory motors.

    Args:
        name (string): Name of the motor. Available motors are: DT50.

    Atributes:
        name (string): Name of the motor.
        sign (+1 or -1): Sign to invert the stage movement direction if desired.
        ref (dict): Dictionary of saved references.
        origin (float): Position of origin. Positions will be refered to this if origin variable of methods is True. Units: rad.
        _object (variable): Motor object. Its class depends on which device is being used.

    Supported devices:
        * DT50.
    """

    def __init__(self, name="DT50"):
        """Initialize the object.

        Supported devices:
            * DT50.
        """
        self.name = name

        if name == "DT50":
            pass

        else:
            raise ValueError('{} is not a valid motor name.'.format(name))

        self.ref = {}
        self.ref["default"] = 0
        self.origin = 0

    def __enter__(self):
        pass

    def __exit__(self):
        """Delete object last function.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            self.Close()

    def Open(self, port=None, invert=False):
        """Open the object.

        Args:
            port (int): Device port number. If None, it uses the default port given by the configuration file. Default: None.
            invert (bool): If True, the movement direction is inverted (switches the sign of positions).

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            port = port or CONF_DT_50["ports"][0]
            self._object = pyPICommands.pyPICommands(CONF_DT_50["dll_lib"],
                                                     'PI_')
            try:
                if not self._object.ConnectRS232(port, CONF_DT_50["baudrate"]):
                    print('Cannot connect to COM %d, %d Baud' %
                          (port, CONF_DT_50["baudrate"]))
                    exit()
                print("Connected to " + self._object.qIDN())

            except Exception as exc:
                ERR_NUM = self._object.GetError()
                print('GCS ERROR %d: %s' %
                      (ERR_NUM, self._object.TranslateError(ERR_NUM)))
                self._object.CloseConnection()
                raise exc

            self._object.VEL({"1": CONF_DT_50["velocity"]})
            ref_ok = self._object.qFRF(' '.join(CONF_DT_50["axes"]))
            for axis_to_ref in CONF_DT_50["axes"]:
                self._object.SVO({axis_to_ref: 1})  # switch on servo
                if ref_ok[axis_to_ref] != 1:
                    print('referencing axis {}, PORT: {}'.format(
                        axis_to_ref, port))
                    self._object.FNL(axis_to_ref)

            ref_ok = self._object.FRF(' '.join(CONF_DT_50["axes"]))
            axes_are_referencing = True
            while axes_are_referencing:
                time.sleep(CONF_DT_50["sleeping_time"])
                ref = self._object.IsMoving(' '.join(CONF_DT_50["axes"]))
                axes_are_referencing = sum(ref.values()) > 0

        if invert:
            self.sign = -1
        else:
            self.sign = 1

    def Close(self):
        """Close the object.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            self._object.CloseConnection()

    def Home(self, busy=True):
        """Home the stage.

        Args:
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.

        Supported devices:
            * DT50.
        """
        self.Clear_Origin()
        if self.name == "DT50":
            self.Move_Absolute(pos=0, busy=busy)

    def Invert(self, sign=None):
        """Invert the stage direction.

        Args:
            sign (+1, -1 or None): If None, switchs the current sign. If +1 or -1, sets that sign.

        Supported devices:
            * All.
        """
        if sign is None:
            self.sign = -self.sign
        elif sign == 1:
            self.sign = 1
        elif sign == -1:
            self.sign = -1
        else:
            raise ValueError('{} is not a correct sign variable.'.format(sign))

    def Set_Velocity(self, vel, units=DEFAULT_VEL_UNITS, verbose=False):
        """Set the current stage velocity.

        Args:
            vel (float): New velocity.
            units (str): Position units to choose between 'rad/s' and 'deg/s'. Default: DEFAULT_VEL_UNITS.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            if units == 'rad/s':
                vel = vel / degrees
            elif units == 'deg/s':
                pass
            else:
                raise ValueError('{} is not a valid unit.'.format(units))
            self._object.VEL({"1": vel})
            time.sleep(CONF_DT_50["sleeping_time"])

        if verbose:
            print('Current velocity is {} {}'.format(vel, units))
        return vel

    def Get_Velocity(self, units=DEFAULT_VEL_UNITS, verbose=False):
        """Get the current stage velocity.

        Args:
            units (str): Position units to choose between rad/s and deg/s. Default: DEFAULT_VEL_UNITS.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            pos (float): Current velocity.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            vel = self._object.qVEL('1')['1']
            if units == 'rad/s':
                pos = pos / degrees
            elif units == 'deg/s':
                pass
            else:
                raise ValueError('{} is not a valid unit.'.format(units))

        if verbose:
            print('Current velocity is {} {}'.format(vel, units))
        return vel

    def Get_Position(self, units=DEFAULT_POS_UNITS, origin=True,
                     verbose=False):
        """Get the current position.

        Args:
            units (str): Position units to choose between 'rad' and 'deg'. Default: DEFAULT_POS_UNITS.
            origin (bool): If True, the current position is calculated respect to the stored origin. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Current position.

        Supported devices:
            * DT50.
        """
        pos = None
        shift = origin * self.origin
        if self.name == "DT50":
            pos = self.sign * (self._object.qPOS('1')['1'] + shift)
            if units == 'rad':
                pos = pos / degrees
            elif units == 'deg':
                pass
            else:
                raise ValueError('{} is not a valid unit.'.format(units))

        if verbose:
            print('Current position is {} {}'.format(pos, units))
        return pos

    def Is_Moving(self, verbose=False):
        """Sees if the motor is moving or not.

        Args:
            verbose (bool): If True, the current status is printed. Default: False.

        Returns:
            moving (bool): Result.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            moving = self._object.IsMoving(' '.join(CONF_DT_50["axes"]))
            moving = sum(moving.values()) > 0

        if verbose:
            if moving:
                print("Motor is moving")
            else:
                print("Motor is not moving")

        return moving

    def Wait_Movement(self, verbose=False):
        """Gets the python kernel busy until the movement has ended.

        Args:
            verbose (bool): If True, the final position is printed. Default: False.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            moving = self.Is_Moving()
            while moving:
                if verbose:
                    self.Get_Position(verbose=True)
                time.sleep(sleeping_time)
                moving = self.Is_Moving()

    def Move_Absolute(self,
                      pos=0,
                      units=DEFAULT_POS_UNITS,
                      move_time=DEFAULT_MOVE_TIME,
                      origin=True,
                      busy=True,
                      verbose=False):
        """Move stage to a given position.

        Args:
            pos (float or None): Position to move to. If None, the motor won't move. Default: 0.
            units (str): Position units to choose between 'rad' and 'deg'. Default: DEFAULT_POS_UNITS.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            origin (bool): If True, the absolute position is calculated respect to the stored origin. Default: False.
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Final position.

        Supported devices:
            * DT50.
        """
        if pos is None:
            return self.Get_Position(units=units, verbose=verbose)

        # Auto calculate velocity
        if move_time:
            old_vel = self.Get_Velocity()
            dist = np.abs(self.Get_Position(units=units) - pos, origin=origin)
            new_vel = dist / move_time
            self.Set_Velocity(vel=new_vel, units=(units + '/s'))

        # Move
        if self.name == "DT50":
            if units == 'rad':
                vel = vel / degrees
            elif units == 'deg':
                pass
            else:
                raise ValueError('{} is not a valid unit.'.format(units))
            self._object.MOV({"1": pos})

        if busy:
            self.Wait_Movement()

        if move_time:
            self.Set_Velocity(vel=old_vel)

        return self.Get_Position(units=units, verbose=verbose)

    def Move_Relative(self,
                      dist,
                      units=DEFAULT_POS_UNITS,
                      move_time=DEFAULT_MOVE_TIME,
                      busy=True,
                      verbose=False):
        """Move stage relative to the current position.

        Args:
            dist (float or None): Distance to move. If None, the motor won't move.
            units (str): Position units to choose between m, mm and um. Default: DEFAULT_POS_UNITS.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Final position.

        Supported devices:
            * DT50.
        """
        if pos is None:
            return self.Get_Position(units=units, verbose=verbose)

        # Auto calculate velocity (NOT REQUIRED FOR DT50)
        # if move_time:
        #     old_vel = self.Get_Velocity()
        #     new_vel = np.abs(dist) / move_time
        #     self.Set_Velocity(vel=new_vel, units=(units + '/s'))

        # Move
        if self.name == "DT50":
            new_pos = self.Get_Position(units=units) + dist
            final_pos = self.Move_Absolute(
                new_pos,
                units=units,
                move_time=move_time,
                busy=busy,
                verbose=verbose)

        # if move_time:
        #     self.Set_Velocity(vel=old_vel)

        return final_pos

    def Set_Origin(self, pos=None, verbose=False):
        """Sets the origin (0 value position).

        Args:
            pos (None or float): If None, the current position is set as the zero. If float, that number is used as 0. Default: None.
            verbose (bool): If True and pos is None, the current position is printed. Default: True.

        Returns:
            pos (float): Current position.

        Supported devices:
            * All.
        """
        if pos is None:
            self.origin = self.Get_Position(
                verbose=verbose, units=DEFAULT_POS_UNITS, origin=False)
        else:
            self.origin = pos
        return self.origin

    def Clear_Origin(self):
        """Clears the origin.

        Supported devices:
            * All."""
        self.origin = 0

    def Save_Ref(self, name="default", verbose=True):
        """Saves the current position in the references library.

        Args:
            name (str): Name of the reference. Default: 'default'.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            pos (float): Current position.

        Supported devices:
            * All.
        """
        self.ref[name] = self.Get_Position(verbose=verbose, origin=False)
        return self.ref[name]

    def Clear_Ref(self, name='default'):
        """Saves the current position in the references library.

        Args:
            name (str): Name of the reference. Default: 'default'.

        Supported devices:
            * All.
        """
        self.ref.pop(name, 0)

    def Move_To_Ref(self,
                    name="default",
                    move_time=DEFAULT_MOVE_TIME,
                    busy=True,
                    verbose=False):
        """Move stage to the stored reference position.

        Args:
            name (str): Name of the reference. Default: 'default'.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Final position.

        Supported devices:
            * All.
        """
        if name in self.ref:
            pos = self.Move_Absolute(
                pos=self.ref[name],
                move_time=move_time,
                verbose=verbose,
                busy=busy,
                origin=False)
        else:
            raise ValueError(
                'There is no saved reference of name {}'.format(name))
            pos = None
        return pos


class Motor_Rotatory_Multiple(object):
    """
    General class for multiple rotatory motors.

    Args:
        name (string): Name of the motor. Available motors are: DT50.

    Atributes:
        name (string): Name of the motor.
        N (int): Number of motors used.
        sign (np.ndarray of +1 or -1): Sign to invert the stage movement direction if desired.
        ref (dict): Dictionary of saved references.
        origin (np.ndarray): Position of origin. Positions will be refered to this if origin variable of methods is True. Units: rad.
        _object (list): List of Motor objects.

    Supported devices:
        * All.
    """

    def __init__(self, name="DT50"):
        """Initialize the object.

        Supported devices:
            * All.
        """
        self.name = name

        if name == "DT50":
            pass

        else:
            raise ValueError('{} is not a valid motor name.'.format(name))

        self.ref = {}
        self.ref["default"] = np.zeros(1)
        self.origin = np.zeros(1)
        self._object = []

    def __del__(self):
        """Delete object last function.

        Supported devices:
            * All.
        """
        for obj in self._object:
            obj.Close()

    def Open(self, port=None, invert=False):
        """Open the object.

        Args:
            port (None or iterable): List of device port number. If None, it uses the default port given by the configuration file. Default: None.
            invert (bool or iterable): If True, the movement direction is inverted (switches the sign of positions).

        Supported devices:
            * DT50.
        """
        # Common
        port = port or CONF_DT_50["ports"]
        if isinstance(invert, bool):
            invert = [invert] * len(port)
        self.N = len(port)
        self.origin = np.zeros(self.N)
        self.ref["default"] = np.zeros(self.N)

        if self.name == "DT50":
            self.sign = np.zeros(len(port))
            self._object = []
            for ind, p in enumerate(port):
                motor = Motor_Rotatory(self.name).Open(
                    port=p, invert=invert[ind])
                self._object.append(motor)
                if invert[ind]:
                    self.sign[ind] = -1
                else:
                    self.sign[ind] = 1

    def Close():
        """Close the objects.

        Supported devices:
            * DT50.
        """
        for obj in self._object:
            obj.Close()

    def Home(self, busy=True):
        """Home the stages.

        Args:
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.

        Supported devices:
            * DT50.
        """
        pass

    def Invert(self, sign=None):
        """Invert the stage direction.

        Args:
            sign (+1, -1 or None): If None, switchs the current sign. If +1 or -1, sets that sign.

        Supported devices:
            * All.
        """
        if isinstance(invert, bool):
            invert = [invert] * self.N

        self.sign = np.zeros(self.N)
        for ind, value in enumerate(invert):
            if value
            self.sign[ind] = -1
            else:
                self.sign[ind] = 1

    def Set_Velocity(self, units=DEFAULT_VEL_UNITS, verbose=False):
        """Set the current stages velocity.

        Args:
            vel (float): New velocity.
            units (str): Position units to choose between 'rad/s' and 'deg/s'. Default: DEFAULT_VEL_UNITS.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Supported devices:
            * DT50.
        """
        for obj in self._object:
            obj.Set_Velocity(units=units)

        if verbose:
            self.Get_Velocity(units=units, verbose=True)

    def Get_Velocity(self, *args, **kwargs):
        """Get the current stage velocity.

        Args:
            units (str): Position units to choose between rad/s and deg/s. Default: DEFAULT_VEL_UNITS.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            pos (float): Current velocity.

        Supported devices:
            * DT50.
        """
        vel = np.zeros(self.N)
        for ind, obj in enumerate(self._object):
            vel[ind] = obj.Set_Velocity(*args, **kwargs)

        if verbose:
            print("The current volocities are: {} {}.".format(vel, units))

        return vel

    def Wait_Movement(self, verbose=False):
        """Gets the python kernel busy until the movement of all motors has ended.

        Args:
            verbose (bool): If True, the final position is printed. Default: False.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            moving = self.Is_Moving(complete_output=False)
            while moving:
                if verbose:
                    self.Is_Moving(verbose=True)
                time.sleep(sleeping_time)
                moving = self.Is_Moving(complete_output=False)

    def Is_Moving(self, complete_output=False, verbose=False):
        """Sees if the motors are moving or not.

        Args:
            complete_output (bool): If True, the result is an array with information of each individual motor. If False, only the global result is given as output. Default: False.
            verbose (bool): If True, the current status is printed. Default: False.

        Supported devices:
            * DT50.
        """
        if self.name == "DT50":
            moving = False
            moving_all = np.zeros(self.N, dtype=bool)
            for ind, obj in enumerate(self._object):
                moving_all[ind] = obj.Is_Moving()
        moving = np.any(moving_all)

        if verbose:
            print("The current motors are moving: {} ({})".format(
                moving_all, moving))

        if complete_output:
            return moving_all
        else:
            return moving

    def Get_Position(self, units=DEFAULT_POS_UNITS, origin=True,
                     verbose=False):
        """Get the current position.

        Args:
            units (str): Position units to choose between 'rad' and 'deg'. Default: DEFAULT_POS_UNITS.
            origin (bool): If True, the current position is calculated respect to the stored origin. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (np.ndarray): Current position.

        Supported devices:
            * DT50.
        """
        pos = np.zeros(self.N)
        for ind, obj in enumerate(self._object):
            pos[ind] = obj.Get_Position(
                units=units, origin=origin, verbose=False)

        if verbose:
            print("The position of the motors is: {} {}.".format(pos, units))
        return pos

    def Move_Absolute(self,
                      pos=0,
                      motor=None,
                      units=DEFAULT_POS_UNITS,
                      move_time=DEFAULT_MOVE_TIME,
                      origin=True,
                      busy=True,
                      verbose=False):
        """Move stages to a given position.

        Args:
            pos (float or iterable): Position to move to. If one value is None, tha motor won't move. Default: 0.
            motor (None, int or iterable): Only if pos is float. If None, all motors are moved. If int or iterable, only the specified motors are moved. Default: None.
            units (str): Position units to choose between 'rad' and 'deg'. Default: DEFAULT_POS_UNITS.
            move_time (float or np.ndarray): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            origin (bool): If True, the absolute position is calculated respect to the stored origin. Default: False.
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Final position.

        Supported devices:
            * DT50.
        """
        # Adapt variables
        if isinstance(move_time, number_types):
            move_time = [move_time] * self.N
        pos = select_movement(pos=pos, motor=motor, obj=self,
                              units=units, absolute=True, origin=origin)

        # Move
        for ind, obj in enumerate(self._object):
            obj.Move_Absolute(
                pos=pos[ind], units=units, move_time=move_time[ind], origin=origin, busy=False, verbose=False)

        # Rest
        pos = self.Get_Position(units=units, verbose=verbose)

        if busy:
            self.Wait_Movement()

        return pos

    def Move_Relative(self,
                      dist,
                      motor=None,
                      units=DEFAULT_POS_UNITS,
                      move_time=DEFAULT_MOVE_TIME,
                      busy=True,
                      verbose=False):
        """Move stage relative to the current position.

        Args:
            dist (float or np.ndarray): Distance to move.
            motor (None, int or iterable): Only if dist is float. If None, all motors are moved. If int or iterable, only the specified motors are moved. Default: None.
            units (str): Position units to choose between m, mm and um. Default: DEFAULT_POS_UNITS.
            move_time (float or np.ndarray): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Final position.

        Supported devices:
            * DT50.
        """
        # Adapt variables
        if isinstance(move_time, number_types):
            move_time = [move_time] * self.N
        dist = select_movement(
            pos=dist, motor=motor, obj=self, units=units, absolute=False, origin=origin)

        # Move
        for ind, obj in enumerate(self._object):
            obj.Move_Relative(
                dist=dist[ind], units=units, move_time=move_time[ind], origin=origin, busy=False, verbose=False)

        # Rest
        pos = self.Get_Position(units=units, verbose=verbose)

        if busy:
            self.Wait_Movement()

        return pos

    def Set_Origin(self, pos=None, motor=None, verbose=False):
        """Sets the origin (0 value position).

        Args:
            pos (None, float or np.ndarray): If None, the current position is set as the zero. If float, that number is used as 0. Default: None.
            motor (None, int or iterable): Only if pos is float. If None, all motors are moved. If int or iterable, only the specified motors are moved. Default: None.
            verbose (bool): If True and pos is None, the current position is printed. Default: True.

        Returns:
            pos (float): Current position.

        Supported devices:
            * All.
        """
        # Calculate origin
        if pos is None:
            self.origin = self.Get_Position(
                verbose=verbose, units=DEFAULT_POS_UNITS, origin=False)
        else:
            self.origin = select_movement(pos=pos, motor=motor, obj=self,
                                          units=DEFAULT_POS_UNITS, absolute=True, origin=False)

        # Set origin
        for ind, obj in enumerate(self._object):
            obj.Set_Origin(pos=self.origin[ind], verbose=False)
        return self.origin

    def Clear_Origin(self, motor=None):
        """Clears the origin.

        Args:
            motor (None, int or iterable): Only if pos is float. If None, all motors are moved. If int or iterable, only the specified motors are moved. Default: None.

        Supported devices:
            * All.
        """
        if isinstance(motor, number_types):
            motor = [motor]

        if motor is None:
            self.origin = np.zeros(self.N)
            for obj in self._object:
                obj.Clear_Origin()
        else:
            for mot in motor:
                self.origin[mot] = 0
                self._object[mot].Clear_Origin()

    def Save_Ref(self, name="default", verbose=True):
        """Saves the current position in the references library.

        Args:
            name (str): Name of the reference. Default: 'default'.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            pos (float): Current position.

        Supported devices:
            * All.
        """
        self.ref[name] = self.Get_Position(verbose=verbose, origin=False)

        for ind, obj in enumerate(self._object):
            obj.Save_Ref(name=name, verbose=False)

        return self.ref[name]

    def Clear_Ref(self, name='default'):
        """Saves the current position in the references library.

        Args:
            name (str): Name of the reference. Default: 'default'.

        Supported devices:
            * All.
        """
        self.ref.pop(name, 0)
        for ind, obj in enumerate(self._object):
            obj.Clear_Ref(name=name)

    def Move_To_Ref(self,
                    name="default",
                    move_time=DEFAULT_MOVE_TIME,
                    busy=True,
                    verbose=False):
        """Move stage to the stored reference position.

        Args:
            name (str): Name of the reference. Default: 'default'.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        Returns:
            pos (float): Final position.

        Supported devices:
            * All.
        """
        if name in self.ref:
            pos = self.Move_Absolute(
                pos=self.ref[name],
                move_time=move_time,
                verbose=verbose,
                busy=busy,
                origin=False)
        else:
            raise ValueError(
                'There is no saved reference of name {}'.format(name))
            pos = None
        return pos
