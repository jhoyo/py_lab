# -*- coding: utf-8 -*-
import time

import cv2
import screeninfo

from config import CONF_HOLOEYE2500, ID_SCREEN, amplification_4f, wavelength, z_ini, t_mirror
from utils import show_image

from diffractio import degrees, mm, nm, np, plt, sp, um
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.utils_optics import field_parameters
s = 1.


class Holoeye(object):
    """Class for sending images to Holoeye

    Args:
        x (numpy.array): linear array with equidistant positions.

    Attributes:
        self.x (numpy.array): linear array with equidistant positions.
            The number of data is preferibly 2**n.
        self.y (numpy.array): linear array wit equidistant positions for y values
        self.wavelength (float): wavelength of the incident field.
        self.u (numpy.array): (x,z) complex field
        self.info (str): String with info about the simulation
    """

    def __init__(self, config_SLM):
        """Info.

        write here
        """
        # print("init de Scalar_mask_XY")
        self.config_SLM = config_SLM
        self.wavelength = config_SLM['wavelength']
        self.num_pixels = config_SLM['num_pixels']
        SLM_pixel_size = config_SLM['size_pixels']

        self.x0 = np.linspace(
            -SLM_pixel_size[0] * self.num_pixels[0] / (2 * amplification_4f),
            SLM_pixel_size[0] * self.num_pixels[0] / (2 * amplification_4f),
            self.num_pixels[0])

        self.y0 = np.linspace(
            -SLM_pixel_size[1] * self.num_pixels[1] / (2 * amplification_4f),
            SLM_pixel_size[1] * self.num_pixels[1] / (2 * amplification_4f),
            self.num_pixels[1])
        self.id_screen = ID_SCREEN
        self.background = None
        self.image_raw = None

    def mask_to_rawImage(self,
                         mask_XY,
                         kind='amplitude',
                         normalize=False,
                         remove_mirror=True):
        """Gets a XY mask, conditions it:
        """

        amplitude, intensity, phase = field_parameters(mask_XY.u)
        if remove_mirror is True:
            phase = self.remove_astimagtism(mask_XY)

        if kind == 'amplitude':
            image_raw = amplitude
        elif kind == 'intensity':
            image_raw = intensity
        elif kind == 'phase':
            phase = (phase + np.pi) % (2 * np.pi) - np.pi

            image_raw = phase + np.pi
            # image_raw[0, 0] = 0
            # image_raw[0, 1] = 2 * sp.pi
            image_raw = image_raw / (2 * sp.pi)

        self.image_raw = image_raw
        self.condition_image(normalize=normalize)
        return self.image_raw

    def condition_image(self, normalize=False):
        """'False', 'complete', '255', 'max'"""
        if normalize is True:
            self.image_raw = (255 * self.image_raw).astype(np.uint8)
        else:
            self.image_raw = (self.image_raw).astype(np.uint8)

    def remove_astimagtism(self, mask_XY):

        fase_mirror = np.angle(t_mirror.u)
        fase_modulator = np.angle(mask_XY.u)
        new_fase = np.exp(1j * (fase_mirror + fase_modulator))
        return fase_mirror + fase_modulator

    def send_image_screen(self, id_screen='', verbose=False):
        """
        takes the images and sends the images to a screen in full size,
        according to  ***TODO

        TODO: make dynamic use of image_screen as proposed in jupyter notebook
        """

        image = self.image_raw

        print(type(image), image.shape)

        if verbose is True:
            print("send_image_screen: max={}. min={}".format(
                image.max(), image.min()))
            print("send_image_screen: size x={}".format(image.shape))
            show_image(image)

        if id_screen in ('', None, []):
            ids = self.id_screen
        else:
            ids = id_screen

        screen = screeninfo.get_monitors()[ids]
        window_name = 'SLM projector'
        cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)
        cv2.moveWindow(window_name, screen.x - 1, screen.y - 1)
        cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                              cv2.WINDOW_FULLSCREEN)
        cv2.imshow(window_name, image)
        cv2.waitKey(10)

    def get_info(self):
        print(screeninfo.get_monitors())
