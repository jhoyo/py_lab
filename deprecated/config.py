# -*- coding: utf-8 -*-
"""Main module."""
from diffractio import nm, um, mm, np, degrees
from diffractio.scalar_masks_XY import Scalar_mask_XY

# Configuration Params for Holoeye
CONF_HOLOEYE2500 = {}
CONF_HOLOEYE2500['num_pixels'] = (1024, 768)
CONF_HOLOEYE2500['size_pixels'] = (19 * um, 19 * um)
CONF_HOLOEYE2500['wavelength'] = 632.8 * nm
CONF_HOLOEYE2500['callibration_table'] = None
CONF_HOLOEYE2500['astigmatism_removal'] = None
CONF_HOLOEYE2500['pos_screen'] = None

# Configuration Params for Screen at lab
CONF_PACKARD_BELL = {}
CONF_PACKARD_BELL['num_pixels'] = (1440, 900)
CONF_PACKARD_BELL['size_pixels'] = (285 * um, 295 * um)
CONF_PACKARD_BELL['wavelength'] = 632.8 * nm
CONF_PACKARD_BELL['callibration_table'] = None
CONF_PACKARD_BELL['astigmatism_removal'] = None
CONF_PACKARD_BELL['pos_screen'] = None

CONF_IMAGING_SOURCE = {}
CONF_IMAGING_SOURCE['id_camera'] = "DMx 72BUC02 14210296"
CONF_IMAGING_SOURCE['format'] = "Y800 (2592x1944)"
CONF_IMAGING_SOURCE['num_pixels'] = (2592, 1944)
CONF_IMAGING_SOURCE['framerate'] = 5
CONF_IMAGING_SOURCE['size_pixels'] = (2.2 * um, 2.2 * um)

amplification_4f = 2.01
z_ini = -125 * mm
sign_movement = +1
wavelength = 0.6328 * um
time_waitKey = 500

ID_SCREEN = 1

DIR_DRIVER_CAMERA = "H:\Codigo\py_lab\py_lab\camera"
SLM_pixel_size = CONF_HOLOEYE2500['size_pixels']
num_pixels = CONF_HOLOEYE2500['num_pixels']

x0_slm = np.linspace(
    -SLM_pixel_size[0] * num_pixels[0] / (2 * amplification_4f),
    SLM_pixel_size[0] * num_pixels[0] / (2 * amplification_4f), num_pixels[0])

y0_slm = np.linspace(
    -SLM_pixel_size[1] * num_pixels[1] / (2 * amplification_4f),
    SLM_pixel_size[1] * num_pixels[1] / (2 * amplification_4f), num_pixels[1])

f1_mirror = 4583 * mm
f2_mirror = 11694 * mm
theta_mirror = 108 * degrees
t_mirror = Scalar_mask_XY(x0_slm, y0_slm, wavelength)
t_mirror.elliptical_phase(f1=f1_mirror, f2=f2_mirror, angle=theta_mirror)
