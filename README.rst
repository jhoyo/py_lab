======
py_lab
======


.. image:: https://img.shields.io/pypi/v/py_lab.svg
        :target: https://pypi.python.org/pypi/py_lab

.. image:: https://img.shields.io/travis/optbrea/py_lab.svg
        :target: https://travis-ci.org/optbrea/py_lab

.. image:: https://readthedocs.org/projects/py-slm/badge/?version=latest
        :target: https://py-slm.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python project to use lab equipment


* Free software: MIT license
* Documentation: https://py-slm.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
