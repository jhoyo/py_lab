#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# LCC22 de Thorlabs

# # FUNCIONA

import serial
import time
import serial.tools.list_ports

V = 1.
mV = V / 1000
A = 1.
mA = A / 1000
error_text = "*****"

"HAMEG Instruments,HM8143,2.41"


def detect_hameg_HM8143():
    list = serial.tools.list_ports.comports()
    connected = []
    for element in list:
        connected.append(element.device)
    print("Connected COM ports: " + str(connected))

    print connected

    instrument_hameg_port = None
    for instrument_port in connected:
        print instrument_port
        ser = serial.Serial(
            port=instrument_port,
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False)

        ser.isOpen()

        print("consultando a {}".format(instrument_port))
        ser.write("*idn?" + '\r')
        time.sleep(.25)
        out = ser.read_all()
        print out

        if 'HM8143' in out:
            print("{} detected at {}".format(out, instrument_port))
            instrument_hameg_port = instrument_port
        # out = ''
        # while ser.inWaiting() > 0:
        #    out += ser.read(1)

        ser.close()

    return instrument_hameg_port


class PowerSupply_HM8143_HAMEG():
    def __init__(self, port='/dev/ttyUSB0'):
        if port in [None, '']:
            port = instrument_hameg_port()

        self.sleep_time = 0.25
        self.verbose = True

        self.ser = serial.Serial(
            port=port,
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False)

        self.ser.isOpen()

        self.ser.write("*IDN?" + '\r')
        time.sleep(.25)
        out = self.ser.read_all()
        print out

    def __del__(self):
        self.ser.close()

    def close(self):
        self.ser.close()

    def query(self, command):
        self.ser.write(command + '\r')
        time.sleep(self.sleep_time)
        result = self.ser.read_all()
        if self.verbose is True:
            print("command:    {}".format(command))
            print("    result: {}".format(result))
        return result

    def get_name(self):
        """
        Format:
        ID?
        *IDN?
        Reply: HAMEG Instruments, HM8143,x.xx
        Function: HAMEG device identifi cation
        Example: HAMEG Instruments, HM8143,1.15
        """
        return self.query("*IDN?")

    def get_version(self):
        """
        Format:     VER
        Reply:      x.xx
        Function:   Displays the software version of HM8143.
        Example:    1.15
        """
        return self.query("VER?")

    def clear(self):
        """
        Format: CLR
        Function: This command interrupts all functions of the
        HM8143. The outputs are switched off, the voltages
        and currents are set to 0.
        """
        return self.query("CLR")

    def activate_fuse(self, activate=True):
        """
        Format: SF
        Funktion: Activation of the electronic fuse.
        (Set fuse)
        Format: CF
        Funktion: De-activation of the electronic fuse.
        (Clear fuse)
        """
        if activate is True:
            return self.query("SF")
        elif activate is False:
            return self.query("CF")

    def switch_outputs(self, switch=True):
        """
        Format: OP1
        Function: The outputs are switched on.
        Format: OP0
        Function: The outputs are switched off.
        """
        if switch is True:
            return self.query("OP1")
        elif switch is False:
            return self.query("OP0")

    def get_status(self):
        """
        Format:         STA         STA?
        Reply:     OP1/0 CV1/CC1 CV2/CC2 RM0/1
        Function: This command causes the HM8143 to send a text-
        string containing information of the actual status.
            OP0 The outputs are switched off.
            OP1 The outputs are switched on.
            CV1 Source 1: constant voltage operation
            CC1 Source 1: constant current operation
            CV2 Source 2: constant voltage operation
            CC2 Source 2: constant current operation
            RMI Device in remote control mode
            RM0 Device not in remotecontrol mode
        Example: If the outputs are on, the HM8143 answers for
        example with the following string (channel I is in
        constant voltage mode and channel II is in constant
        current mode:
            OP1 CV1 CC2 RM1
            If the outputs are off, the answer string contains
            instead of the status of channels I and II two times
            three dashes (––– –––).
            OP0 ––– ––– RM1
        """
        return self.query("STA?")

    def set_remote(self, mode=True):
        """
        Format: RM1
        Function: Puts the power supply in remote mode.
        The frontpanel controls are disabled. In this mode,
        the power supply can only be operated by interface.
        This mode can be terminated by sending a RM0 com-
        mand.
        Format: RM0
        Function: Disables the remote mode, returning the power
        supply to local mode (permitting operation using
        the front panel controls).
        """
        if mode is True:
            return self.query("RM1")
        elif mode is False:
            return self.query("RM0")

    """
    MX1 + MX0
    Format: MX1
    Function: Switches the power supply from remote mode into
    mixed mode. In mixed mode, the instrument can be
    operated either by interface or using the frontpanel
    controls.
    Format: MX0
    Function: Terminates mixed mode and returns the instrument
    to remote mode.
    """

    def set_voltage(self, channel, volts):
        """SU1 + SU2
        Format: SU1:VV.mVmV or SU2:01.34
        SU1:VV.mVmV or SU2:01.34
        Function: Sets voltage 1 or voltage 2 to the indicated value (SET
        value; BCD format)
        Example:    SU1:1.23           U1 = 1.23 V
                    SU2:12.34          U2 = 12.34 V

        TODO: control limits
        """

        if channel in [1, 2]:
            return self.query("SU{}:{}".format(channel, volts))
        else:
            print(
                "error in set_remote. ONLY CHANNELS (1,2) are possible. Channel {} was introduced".
                format(channel))
            return "set_voltage" + error_text

    def set_current_limit(self, channel, current):
        """
        Format: SI1:A.mAmAmA or SI1:0.123
                SI1:A.mAmAmA or SI1:0.123
        Function: Sets current limit 1 or current limit 2 to the indicated
        value (LIMIT value; BCD format)
            Examples:   SI1:1.000        I1 = 1.000 A
                        SI2:0.123        I2 = 0.123 A

        TODO: control limits
        """

        if channel in [1, 2]:
            return self.query("SI{}:{}".format(channel, current))
        else:
            print(
                "error in set_current_limit. ONLY CHANNELS (1,2) are possible. Channel {} was introduced".
                format(channel))
            return "set_current_limit " + error_text

    def get_voltage(self, channel):
        """
        Format: RU1 or RU2
        Reply:  U1:12.34V or U2:12.34V
        Function: The voltage values sent back by the HM8143 are
        the programmed voltage values. Use the MUx com-
        mands to query the actual values.
        """

        if channel in [1, 2]:
            return self.query("RU{}".format(channel))
        else:
            print(
                "error in get_voltage. ONLY CHANNELS (1,2) are possible. Channel {} was introduced".
                format(channel))
            return "get_voltage" + error_text

    def get_programmed_current(self, channel):
        """
        Format: RI1 or RI2
        Reply:  I1:+1.000A or I2:–0.012A
        Function: The current values sent back by the HM8143 repre-
        sent the programmed limit values for the current.
        Use the MIx commands to query the actual current
        values.
        """

        if channel in [1, 2]:
            return self.query("RI{}".format(channel))
        else:
            print(
                "error in get_current. ONLY CHANNELS (1,2) are possible. Channel {} was introduced".
                format(channel))
            return "get_current" + error_text

    def get_measured_voltage(self, channel):
        """
        Format: MU1 or MU2
        Reply:  U1:12.34V or U2:12.24V
        Function: The voltage values sent back by the HM8143 repre-
        sent the actual voltage values last measured at the
        outputs. Use the RUx commands to query the voltage
        values set.
        """

        if channel in [1, 2]:
            return self.query("MU{}".format(channel))
        else:
            print(
                "error in get_measured_voltage. ONLY CHANNELS (1,2) are possible. Channel {} was introduced".
                format(channel))
            return "get_measured_voltage" + error_text

    def set_track_voltage(self, voltage):
        """
        Format:        TRU:VV.mVmV
        Function: Sets voltage 1 and voltage 2 to the indicated value
        (voltage values in TRACKING mode). The values
        must follow the BCD format.
        Examples:   TRU:1.23         U1 = U2 = 1.23 V
                    TRU:01.23        U1 = U2 = 1.23 V
        TRU:12.34   U1 = U2 = 12.34 V
        """
        return self.query("TRU:{}".format(voltage))

    def set_track_current(self, current):
        """
        Function: Sets current 1 and current 2 to the indicated value
        (LIMIT values in TRACKING mode). The values must
        follow the BCD format.
        Examples:   TRI:1.000      I1 = I2 = 1.000 A
                    TRI:0.123      I1 = I2 = 0.123 A
        """
        return self.query("TRI:{}".format(current))

    """
    TODO: (pag 28 manual)
    Arbitrary
    The arbitrary waveform mode can be used for generation of
    virtually any desired waveforms. For this purpose, a table com-
    prising up to 1024 voltage and time values can be defi ned. This
    table is stored in nonvolatile memory with a backup battery,
    and is not lost for several days when the instrument is powered
    down. The following commands are available for operating and
    programming this function by interface:
    ABT
    RUN
    STP
    Transfer of arbitrary values
    Start waveform generation
    Stop waveform generation
    """


instrument_hameg_port = '/dev/ttyUSB0'
# instrument_hameg_port = detect_hameg_HM8143()

pw = PowerSupply_HM8143_HAMEG(instrument_hameg_port)
pw.get_name()
pw.get_version()
pw.set_voltage(channel=1, volts=15.0 * V)
pw.set_current_limit(channel=1, current=.25 * A)
pw.get_voltage(channel=1)
pw.get_programmed_current(channel=1)
pw.set_remote(mode=False)
pw.get_measured_voltage(channel=1)
pw.get_status()
pw.set_track_voltage(voltage=10 * V)
pw.set_track_current(current=1 * A)
pw.set_remote(mode=False)

pw.close()
