#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Control for Thorlabs LCC25 Liquid Crystal Controller.:

It is controlled by serial port and pyserial module.

"""


import time

import numpy as np
import serial
import serial.tools.list_ports


class LCD():
    def __init__(self):
        self.serial = None
        self.sleep_time = 0.2

    def __del__(self):
        self.close()

    def close(self):
        self.set_voltage(voltage=0, channel=1)
        self.set_voltage(voltage=0, channel=2)
        self.set_mode(mode=1)
        self.set_enable(enable=False)
        self.serial.close()
        del self

    def get_port_list(self):
        list = serial.tools.list_ports.comports()
        connected = []
        for element in list:
            connected.append(element.device)
        print("Connected COM ports: " + str(connected))
        return list

    def open(self, port):
        self.serial = serial.Serial(
            port=port, baudrate=115200, parity='N', stopbits=1, bytesize=8,  xonxoff=False)

    def send_command(self, command, data=None, verbose=False):

        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
        time.sleep(self.sleep_time)

        if data is None:
            text = "{}\r".format(command)
        else:
            text = "{}={}\r".format(command, data)

        if verbose:
            print("input text: {}".format(text))

        try:
            self.serial.write(text.encode("utf-8"))
            time.sleep(self.sleep_time)
            out = self.serial.read_all()
            time.sleep(self.sleep_time)
            out2 = out.replace(b'\r', b'\n')
            # out2 = out2.replace(b'\t', b'\n')
            out3 = out2.decode()
            out4 = out3.split("\n")
        except self.serial.SerialException:
            print('Port is not available')
        except self.serial.portNotOpenError:
            print('Attempting to use a port that is not open')
            print('End of script')

        if verbose:
            print("output text:{}\n".format(out2.decode()))

        return out4

    def idn(self, verbose=False):
        code = "*idn?"

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def info(self, verbose=False):
        code = "?"

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def set_voltage(self,  voltage, channel=1, verbose=False):

        if voltage < 0:
            voltage = 0.0
        if voltage > 25:
            voltage = 25

        if channel == 1:
            code = "volt1={:2.3f}".format(voltage)
        elif channel == 2:
            code = "volt2={:2.3f}".format(voltage)
        else:
            print("error in channel")
            return -1

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def get_voltage(self,  channel=1, verbose=False):

        if channel == 1:
            code = "volt1?"
        elif channel == 2:
            code = "volt2?"

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def set_freq(self,  freq, verbose=False):

        if freq < 0.5:
            freq = 0.5
        if freq > 150:
            freq = 150

        code = "freq={}".format(freq)

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def get_freq(self,   verbose=False):

        code = "freq?"

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def set_mode(self,  mode,  verbose=False):

        mode = int(mode)
        if mode not in [0, 1, 2]:
            print("error in channel")
            return -1

        code = "mode={}".format(mode)

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def get_mode(self,   verbose=False):

        code = "mode?"

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def set_enable(self,  enable,  verbose=False):

        enable = int(enable)
        if enable not in [0, 1]:
            print("error in set_enable")
            return -1

        code = "enable={}".format(enable)

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def get_enable(self,   verbose=False):

        code = "enable?"

        out = self.send_command(command=code, data=None, verbose=verbose)
        return out

    def set_test(self, v1=0, v2=25, increment=1, dwell_time=0.1, verbose=False):
        code = "min={}".format(v1)
        self.send_command(command=code, data=None, verbose=verbose)
        code = "max={}".format(v2)
        self.send_command(command=code, data=None, verbose=verbose)
        code = "increment={}".format(increment)
        self.send_command(command=code, data=None, verbose=verbose)
        code = "dwell={}".format(dwell_time)
        self.send_command(command=code, data=None, verbose=verbose)
        code = "test"
        self.send_command(command=code, data=None, verbose=verbose)

    def set_modulation(self, v1=0, v2=25, freq=1, verbose=False):
        # quizás meterlo en un thread para poner un tiempo determinado
        self.set_enable(enable=False, verbose=verbose)
        self.set_voltage(voltage=v1, channel=1, verbose=verbose)
        self.set_voltage(voltage=v2, channel=2, verbose=verbose)
        self.set_freq(freq=freq, verbose=verbose)
        self.set_mode(mode=0, verbose=verbose)
        self.set_enable(enable=True, verbose=verbose)

    def set_rampa(self, voltages, times, verbose=False):
        # quizás meterlo en un thread para poner un tiempo determinado

        if isinstance(times, (int, float)):
            t = times
            times = t * np.ones_like(voltages)

        self.set_mode(mode=1)
        self.set_enable(enable=True, verbose=verbose)

        for i, voltage in enumerate(voltages):
            self.set_voltage(voltage=voltage, channel=1, verbose=verbose)
            time.sleep(times[i])
