Connected COM ports: ['/dev/ttyUSB0']
input text: ?
output text:?
Commands:

*idn?	- Identification Query
id?	- Identification Query
?	- Commands Query
save	- Save Parameters
volt1	- Set Voltage 1
volt1?	- Get Voltage 1
volt2	- Set Voltage 2
volt2?	- Get Voltage 2
freq	- Set Modulation Frequency
freq?	- Get Modulation Frequency
set	- Set Preset
get	- Get Preset
mode	- Set Output Mode (0: mod, 1: V1, 2: V2)
mode?	- Get Output Mode (0: mod, 1: V1, 2: V2)
extern	- Set External Mode
extern?	- Get External Mode
enable	- Set Enable Output
enable?	- Get Enable Output
dwell	- Set Test Mode Dwell Time (ms)
dwell?	- Get Test Mode Dwell Time (ms)
increment	- Set Test Mode Increment (V)
increment?	- Get Test Mode Increment (V)
min	- Set Test Min Voltage (V)
min?	- Get Test Min Voltage (V)
max	- Set Test Max Voltage (V)
max?	- Get Test Max Voltage (V)
remote	- Set Remote Mode (0: off, 1: on)
remote?	- Get Remote Mode (0: off, 1: on)
test	- Run Test Mode
Arrow Keys	- up, down, left, right
> 

['?', 'Commands:', '', '*idn?\t- Identification Query', 'id?\t- Identification Query', '?\t- Commands Query', 'save\t- Save Parameters', 'volt1\t- Set Voltage 1', 'volt1?\t- Get Voltage 1', 'volt2\t- Set Voltage 2', 'volt2?\t- Get Voltage 2', 'freq\t- Set Modulation Frequency', 'freq?\t- Get Modulation Frequency', 'set\t- Set Preset', 'get\t- Get Preset', 'mode\t- Set Output Mode (0: mod, 1: V1, 2: V2)', 'mode?\t- Get Output Mode (0: mod, 1: V1, 2: V2)', 'extern\t- Set External Mode', 'extern?\t- Get External Mode', 'enable\t- Set Enable Output', 'enable?\t- Get Enable Output', 'dwell\t- Set Test Mode Dwell Time (ms)', 'dwell?\t- Get Test Mode Dwell Time (ms)', 'increment\t- Set Test Mode Increment (V)', 'increment?\t- Get Test Mode Increment (V)', 'min\t- Set Test Min Voltage (V)', 'min?\t- Get Test Min Voltage (V)', 'max\t- Set Test Max Voltage (V)', 'max?\t- Get Test Max Voltage (V)', 'remote\t- Set Remote Mode (0: off, 1: on)', 'remote?\t- Get Remote Mode (0: off, 1: on)', 'test\t- Run Test Mode', 'Arrow Keys\t- up, down, left, right', '> ']
['volt1=15', '> ']
['volt2?', '0', '> ']
['*idn?', 'THORLABS LCC25 vers 1.01', '> ']
input text: ?
output text:?
Commands:

*idn?	- Identification Query
id?	- Identification Query
?	- Commands Query
save	- Save Parameters
volt1	- Set Voltage 1
volt1?	- Get Voltage 1
volt2	- Set Voltage 2
volt2?	- Get Voltage 2
freq	- Set Modulation Frequency
freq?	- Get Modulation Frequency
set	- Set Preset
get	- Get Preset
mode	- Set Output Mode (0: mod, 1: V1, 2: V2)
mode?	- Get Output Mode (0: mod, 1: V1, 2: V2)
extern	- Set External Mode
extern?	- Get External Mode
enable	- Set Enable Output
enable?	- Get Enable Output
dwell	- Set Test Mode Dwell Time (ms)
dwell?	- Get Test Mode Dwell Time (ms)
increment	- Set Test Mode Increment (V)
increment?	- Get Test Mode Increment (V)
min	- Set Test Min Voltage (V)
min?	- Get Test Min Voltage (V)
max	- Set Test Max Voltage (V)
max?	- Get Test Max Voltage (V)
remote	- Set Remote Mode (0: off, 1: on)
remote?	- Get Remote Mode (0: off, 1: on)
test	- Run Test Mode
Arrow Keys	- up, down, left, right
> 

input text: volt1=6.500
output text:volt1=6.500
> 

input text: volt2=12.500
output text:volt2=12.500
> 

input text: volt1?
output text:volt1?
6.5
> 

input text: volt2?
output text:volt2?
12.5
> 

input text: freq=50
output text:freq=50
> 

input text: freq?
output text:freq?
50
> 

input text: mode=0
output text:mode=0
> 

input text: mode=1
output text:mode=1
> 

input text: mode=2
output text:mode=2
> 

input text: mode=1
output text:mode=1
> 

error in channel
input text: mode?
output text:mode?
1
> 

input text: enable=1
output text:enable=1
> 

input text: enable?
output text:enable?
1
> 

input text: min=0
output text:min=0
> 

input text: max=25
output text:max=25
> 

input text: increment=1
output text:increment=1
> 

input text: dwell=100
output text:dwell=100
> 

input text: test
output text:test
> 

input text: enable=0
output text:enable=0
> 

input text: volt1=0.000
output text:volt1=0.000
> 

input text: volt2=25.000
output text:volt2=25.000
> 

input text: freq=1
output text:freq=1
> 

input text: mode=0
output text:mode=0
> 

input text: enable=1
output text:enable=1
> 

