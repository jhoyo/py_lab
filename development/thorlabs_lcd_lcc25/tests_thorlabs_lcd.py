#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#!/usr/bin/env python
# coding: utf-8

# ## Test funcionamiento LCD throlabs
#
# LCC25

# In[1]:


import time

import numpy as np
import serial
import serial.tools.list_ports
from py_lab.developing.thorlabs_lcd_lcc25.thorlabs_lcd import LCD

# In[2]:


# ## Detección de puertos COM

# In[3]:


list = serial.tools.list_ports.comports()
connected = []
for element in list:
    connected.append(element.device)
print("Connected COM ports: " + str(connected))


# ## Conexión al láser

#
# For linux, if the port COM cannot be opened:
#
# - sudo chmod 666 /dev/ttyACM0
#

# In[6]:


# In[44]:


lcd = LCD()


# In[45]:


lcd.open('/dev/ttyUSB0')


# In[64]:


lcd.sleep_time = 0.1


# ## Send raw commands

# In[65]:


out2 = lcd.send_command(command='?', data=None, verbose=True)
print(out2)


# In[56]:


out2 = lcd.send_command(command='volt1', data=15, verbose=False)
print(out2)


# In[58]:


out2 = lcd.send_command(command='volt2?', data=None, verbose=False)
print(out2)


# ## Class commands

# In[68]:


out2 = lcd.idn(verbose=False)
print(out2)


# In[89]:


out2 = lcd.info(verbose=True)


# In[94]:


out2 = lcd.set_voltage(voltage=6.5, channel=1, verbose=True)
out2 = lcd.set_voltage(voltage=12.5, channel=2, verbose=True)


# In[97]:


out2 = lcd.get_voltage(channel=1, verbose=True)
out2 = lcd.get_voltage(channel=2, verbose=True)


# In[87]:


out2 = lcd.set_freq(50, verbose=True)


# In[88]:


out2 = lcd.get_freq(verbose=True)


# In[82]:


out2 = lcd.set_mode(mode=0, verbose=True)
out2 = lcd.set_mode(mode=1, verbose=True)
out2 = lcd.set_mode(mode=2, verbose=True)
out2 = lcd.set_mode(mode=1.0, verbose=True)
out2 = lcd.set_mode(mode=6, verbose=True)


# In[81]:


out2 = lcd.get_mode(verbose=True)


# In[99]:


out2 = lcd.set_enable(1, verbose=True)


# In[100]:


out2 = lcd.get_enable(verbose=True)


# ## New procedures

# In[108]:


out2 = lcd.set_test(v1=0, v2=25, increment=1, dwell_time=100, verbose=True)


# In[124]:


lcd.set_modulation(v1=0, v2=25, freq=1, verbose=True)


# In[123]:


lcd.set_rampa(voltages=np.linspace(0, 25, 50), times=0.01, verbose=False)


# ## Close

# In[43]:


lcd.close()
