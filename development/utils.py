# !/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np


def nearest(vector, numero):
    """calcula la posicion de numero en el vector
        actualmente esta implementado para 1-D, pero habria que hacerlo para nD
        inputs:
        *vector es un array donde estan los points
        *numero es un dato de los points que se quieren compute

        outputs:
        *imenor - orden del elemento
        *value  - value del elemento
        *distance - diferencia entre el value elegido y el incluido
    """
    indexes = np.abs(vector - numero).argmin()
    values = vector.flat[indexes]
    distances = values - numero
    return indexes, values, distances


def to_bits(variable_integer, num_bits=16):
    """
    takes an integer an generates a list with bits

    Args:
        variable_integer (int): integer with data
        num_bits (int): num of output bits: 8,16, 32, 64
    """
    if num_bits == 8:
        output = map(int, [x for x in '{:08b}'.format(variable_integer)])
    elif num_bits == 16:
        output = map(int, [x for x in '{:016b}'.format(variable_integer)])
    elif num_bits == 32:
        output = map(int, [x for x in '{:032b}'.format(variable_integer)])
    elif num_bits == 64:
        output = map(int, [x for x in '{:064b}'.format(variable_integer)])

    print output
