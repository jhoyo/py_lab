# !/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is a copy of https://github.com/sbrinkmann/PyOscilloskop/blob/master/src/usbtmc.py

from aocg_instruments import plt, np, sp, hickle
from aocg_instruments.driver_usbtmc import UsbTmcDriver

V = 1.
mV = V / 1000.
s = 1.
ms = s / 1000.
us = ms / 1000.
ns = us / 1000.


class waveform_generator_Rigol_DG4062(UsbTmcDriver):
    def __init__(self, device=None):
        super(self.__class__, self).__init__(device)

    # def __del__(self):
    #     self.unclock()
    #     # os.close(self.FILE)

    def set_beep(self, state):
        if state is True:
            state = 'ON'
        elif state is False:
            state = 'OFF'

        self.write(":SYST:BEEP:STAT {}".format(state))

    def beep(self):
        self.write(":SYST:BEEP")

    def unclock(self):
        self.write(":SYST:KLOC OFF")

    def output(self, channel, state):

        if state is True:
            state = 'ON'
        elif state is False:
            state = 'OFF'
        self.write(":OUTP{}:STAT {}".format(channel, state))

    def send_sinusoid(self, channel, frequency, amplitude, offset, phase):

        self.write(":SOUR{}:APPL:SIN {},{},{},{}".format(
            channel, frequency, amplitude, offset, phase))

    def send_square(self, channel, frequency, amplitude, offset, phase):

        self.write(":SOUR{}:APPL:SQU {},{},{},{}".format(
            channel, frequency, amplitude, offset, phase))

    def send_ramp(self, channel, frequency, amplitude, offset, phase):

        self.write(":SOUR{}:APPL:RAMP {},{},{},{}".format(
            channel, frequency, amplitude, offset, phase))

    def send_noise(self, channel, amplitude, offset):

        self.write(
            ":SOUR{}:APPL:NOIS {},{}".format(channel, amplitude, offset))

    def send_pulse(self, channel, amplitude, offset):

        self.write(
            ":SOUR{}:APPL:PULS {},{}".format(channel, amplitude, offset))

    def send_harmonic(self, channel, frequency, amplitude, offset, phase):

        self.write(":SOUR{}:APPL:HARM {},{},{},{}".format(
            channel, frequency, amplitude, offset, phase))

    def send_user(self, channel, frequency, amplitude, offset, phase):

        self.write(":SOUR{}:APPL:USER {},{},{},{}".format(
            channel, frequency, amplitude, offset, phase))

    def send_custom(self, channel, frequency, amplitude, offset, phase):

        self.write(":SOUR{}:APPL:CUST {},{},{},{}".format(
            channel, frequency, amplitude, offset, phase))
