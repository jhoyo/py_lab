%Esta funci�n abre un archivo .dat del confocal y lo convierte en una
%matriz 2D para poder visualizarlo y trabajar con �l.
%06/02/18 Francisco Torcal

%Hago bastante promediado pero hay que ajustar a mano cada imagen de
%confocal. No se tarda mucho, s�lo el desfase en micras para generar la
%m�scara. EL period se calcula solo, si va bien el ajuste.

%% Usar este para las im�genes que haya red

A=importdata('arg 1 - 50x red.dat');
x = A(:,1);
y = A(:,2);
z = A(:,3);
pixel=x(2)-x(1); %en micras
xi=linspace(min(x),max(x),560);
yi=linspace(min(y),max(y),762);
[XI, YI]=meshgrid(xi,yi);
ZI = griddata(x,y,z,XI,YI);
figure;imagesc(xi,yi,ZI);colormap gray
figure; imagesc(ZI)

%% Primero saco el perfil de la franjas y ajusto a una sinusoidal para sacar el periodo
franjas =mean(ZI);
%figure; plot(xi,franjas);

%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData( xi, franjas );

% Set up fittype and options.
ft = fittype( 'fourier1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Robust = 'Bisquare';
opts.StartPoint = [0 0 0 0.316685966966851];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
figure;plot( fitresult, xData, yData );


%% Genero una red binaria para usarla de m�scara
parametros.sx=xi;
parametros.sy=yi;

def_red.amp_max=1;
def_red.amp_min=0;
def_red.pha_max=0;
def_red.pha_min=0;
def_red.periodo=2*pi/fitresult.w; % en micras
def_red.factor_forma=0.5.*def_red.periodo;
def_red.desfaseX=9; %Esto se podr�a automatizar, por ahora a ojo.


t=red_Binaria_2D(parametros,def_red)';
figure;plot(xi,franjas); hold on; plot(xi,t,'r')

%% Parte grabada
acf_media= zeros(size(yi))';
grabacion= ZI.*t;
%Hago un promedio dentro de las grabaciones
for i=1:length(xi)
    perfil=grabacion(:,i);
    format long
    %desviaci�n est�ndar en las alturas
    rms(i)=std(perfil);

    %longitud de correlaci�n
    N = length(yi); % n�mero de puntos de muestreo
    % calculo de la autocavarianza
    c=xcov(perfil,'coeff'); % autocovarianza
    acf=c(N:2*N-1); % right-sided version
    acf_media = acf_media + acf;
%     if i==1
%         figure;plot(yi,acf); hold on
%     else
%         plot(yi,acf); hold on
%     end
    
    if rms(i) == 0 % c�lculo de la longitud de correlaci�n
        cl(i)=0;
    else
        k = 1;
        while (acf(k) > 1/exp(1))
            k = k + 1;
        end;
        cl(i) = 1/2*(yi(k-1)+yi(k)-2*yi(1)); % the correlation length
    end
end

sigma=mean(rms(rms>0));
correla=mean(cl(cl>0));

%% Parte sin graban entre las franjas grabadas (par�metos eje y)

acf_media_fleje= zeros(size(yi))';
fleje= ZI.*(1-t);
%Hago un promedio dentro de las grabaciones
for i=1:length(xi)
    perfil=fleje(:,i);
    format long
    %desviaci�n est�ndar en las alturas
    rms_fleje(i)=std(perfil);

    %longitud de correlaci�n
    N = length(yi); % n�mero de puntos de muestreo
    % calculo de la autocavarianza
    c=xcov(perfil,'coeff'); % autocovarianza
    acf_fleje=c(N:2*N-1); % right-sided version
    acf_media_fleje = acf_media_fleje + acf_fleje;
%     if i==1
%         figure;plot(yi,acf_fleje); hold on
%     else
%         plot(yi,acf_fleje); hold on
%     end
    
    if rms_fleje(i) == 0 % c�lculo de la longitud de correlaci�n
        cl_fleje(i)=0;
    else
        k = 1;
        while (acf(k) > 1/exp(1))
            k = k + 1;
        end;
        cl_fleje(i) = 1/2*(yi(k-1)+yi(k)-2*yi(1)); % the correlation length
    end
end

sigma_fleje=mean(rms_fleje(rms_fleje>0));
correla_fleje=mean(cl_fleje(cl_fleje>0));


%% Usar este para las im�genes que NO haya red
