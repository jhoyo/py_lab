%Esta funci�n abre un archivo .dat del confocal y lo convierte en una
%matriz 2D para poder visualizarlo y trabajar con �l.
%06/02/18 Francisco Torcal

%Hago bastante promediado pero hay que ajustar a mano cada imagen de
%confocal. No se tarda mucho, s�lo el desfase en micras para generar la
%m�scara. EL period se calcula solo, si va bien el ajuste.

%% Usar este para las im�genes que no haya red

A=importdata('arg 2 - 50x vacio 2.dat');
x = A(:,1);
y = A(:,2);
z = A(:,3);
pixel=x(2)-x(1); %en micras
xi=linspace(min(x),max(x),560);
yi=linspace(min(y),max(y),762);
[XI, YI]=meshgrid(xi,yi);
ZI = griddata(x,y,z,XI,YI);
figure;imagesc(xi,yi,ZI);colormap gray


%% Fleje paralelo a las franjas
for i=1:length(xi)
    perfil=ZI(:,i); %Perfiles verticales de la imagen
    format long
    %desviaci�n est�ndar en las alturas
    rms_fleje(i)=std(perfil);

    %longitud de correlaci�n
    N = length(yi); % n�mero de puntos de muestreo
    % calculo de la autocavarianza
    c=xcov(perfil,'coeff'); % autocovarianza
    acf=c(N:2*N-1); % right-sided version
%     if i==1
%         figure;plot(yi,acf); hold on
%     else
%         plot(yi,acf); hold on
%     end
    
    k = 1;
    while (acf(k) > 1/exp(1))
        k = k + 1;
    end;
    cl_fleje(i) = 1/2*(yi(k-1)+yi(k)-2*yi(1)); % the correlation length

end

sigma=mean(rms_fleje);
desv_sigma=std(rms_fleje);
correla=mean(cl_fleje);
desv_correla=std(cl_fleje);

%% Fleje perpendicular a las franjas
for i=1:length(yi)
    perfil=ZI(i,:); %Perfiles horizontales de la imagen
    format long
    %desviaci�n est�ndar en las alturas
    rms_fleje_2(i)=std(perfil);

    %longitud de correlaci�n
    N = length(xi); % n�mero de puntos de muestreo
    % calculo de la autocavarianza
    c=xcov(perfil,'coeff'); % autocovarianza
    acf_2=c(N:2*N-1); % right-sided version
%     if i==1
%         figure;plot(yi,acf); hold on
%     else
%         plot(yi,acf); hold on
%     end
    
    k = 1;
    while (acf_2(k) > 1/exp(1))
        k = k + 1;
    end;
    cl_fleje_2(i) = 1/2*(xi(k-1)+xi(k)-2*xi(1)); % the correlation length
end

sigma_2=mean(rms_fleje_2);
desv_sigma_2=std(rms_fleje_2);
correla_2=mean(cl_fleje_2);
desv_correla_2=std(cl_fleje_2);
