#!/usr/bin/env python3
# coding: utf-8

import os
import os.path

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import fftconvolve

from PIL import Image


def extract_binary_image_from_microscope(filename, ending='_binarized', cut_level=(0.9, 0.05), convolution_size=0, reduce_size=False, image_size=1024, has_draw=False, verbose=False):
    """Gets an image from the optical microscope (x5) and extract the binary image. It was developed for analyzing the fabricated DOEs.
    Images were acquired using a mobile phone.

    Args:
        filename (string): name of the file.
        ending (string): add text to image
        cut_level (float, float): binarization levels.
        convolution_size (float): if 0 it does not convolute. If >0 it processes the image and generates a convolution for determining the background intensity level. It is useful when the background intensity level is variable.
        reduce_size (bool): If True, the image automatically reduces in order to remove black areas of the ocular.
        image_size (int): image size (1024x1024) for example.
        has_draw (bool): if True, draws several figures to check the correct functioning of the proccess.
        verbose (bool): If True, returns data


    Attributes:
        data_final (numpy.array): matrix with data of binarized image.
        im: PIL Structure of final image for post-processing.
    """
    cut_level1, cut_level2 = cut_level

    # extract image
    imagen1 = mpimg.imread(filename)
    data = (imagen1[:, :, 0]) + 0.0
    x_pixels, y_pixels = data.shape

    # normalization
    data = (data - data.min()) / (data.max() - data.min())

    if has_draw:
        plt.figure()
        plt.imshow(imagen1)
        plt.title('raw image')
        plt.tight_layout()

    # if background level is not uniform.
    if convolution_size > 0:
        dim = convolution_size
        square = np.ones((dim, dim)) / dim**2
        data_conv = fftconvolve(data, square, mode='same')

        data_new = np.zeros_like(data, dtype=float)
        data_new[data_conv > 0.2] = data[data_conv > 0.2] / \
            data_conv[data_conv > 0.2]
        data_new[data_new > 1] = 1
        data = data_new

        if has_draw:
            plt.figure()
            plt.imshow(data_conv)
            plt.colorbar(orientation='horizontal')
            plt.title('convolved image')
            plt.tight_layout()

            plt.figure()
            plt.imshow(data_new)
            plt.colorbar(orientation='horizontal')
            plt.title('normalized image')
            plt.tight_layout()

            hist_0, bins = np.histogram(data_new, 256)
            hist_0[0:5] = 0
            hist_0[-1] = 0

            plt.figure()
            plt.plot(hist_0)
            plt.title('histogram')
            plt.tight_layout()

            hist_0[0:5] = 0
            hist_0[-1] = 0
            plt.plot(hist_0, 'k')
            plt.tight_layout()

    # binarization process
    data[data < cut_level1] = 0
    data[data >= cut_level1] = 1

    if verbose:
        print(data.shape)
        print(type(data))

    if has_draw:
        plt.figure()
        hist_0, bins = np.histogram(data, 256)
        plt.plot(hist_0)
        plt.title('histogram after binarization')
        plt.tight_layout()

    if reduce_size:
        x_pos = data.mean(axis=0)
        y_pos = data.mean(axis=1)

        x_pos[x_pos < cut_level2] = 0
        x_pos[x_pos >= cut_level2] = 1

        y_pos[y_pos < cut_level2] = 0
        y_pos[y_pos >= cut_level2] = 1

        if has_draw:
            plt.figure()
            plt.plot(x_pos)
            plt.plot(y_pos)
            plt.title('location of edges 1')
            plt.tight_layout()

        dif_x = np.diff(x_pos)
        dif_y = np.diff(y_pos)

        if has_draw:
            plt.figure()
            plt.plot(dif_x)
            plt.plot(dif_y)
            plt.title('location of edges 2')
            plt.tight_layout()


        if verbose:
            print("diff_x: {}".format(dif_x))
            print("diff_y: {}".format(dif_y))

        x_min = dif_x.argmax()
        x_max = dif_x.argmin()

        y_min = dif_y.argmax()
        y_max = dif_y.argmin()
        if verbose:
            print("bordes: ", x_min, x_max, y_min, y_max)

        x_left = np.where((dif_x > 0.75) == 1)[0]
        x_rigth = np.where((dif_x < -0.75) == 1)[0]
        y_bottom = np.where((dif_y > 0.75) == 1)[0]
        y_top = np.where((dif_y < -0.75) == 1)[0]

        if np.any(x_left):
            x_left = x_left[0]
        else:
            x_left = 0

        if np.any(x_rigth):
            x_rigth = x_rigth[-1]
        else:
            x_rigth = len(x_pos)

        if np.any(y_bottom):
            y_bottom = y_bottom[0]
        else:
            y_bottom = 0

        if np.any(y_top):
            y_top = y_top[-1]
        else:
            y_top = len(y_pos)

        if verbose:
            print("bordes2: ", x_left, x_rigth, y_bottom, y_top)

        # x_center = int((x_min + x_max) / 2)
        # y_center = int((y_min + y_max) / 2)
        # x_radius = int((x_max - x_min) / 2)
        # y_radius = int((y_max - y_min) / 2)

        x_center = int((x_left + x_rigth) / 2)
        y_center = int((y_bottom + y_top) / 2)
        x_radius = int((x_rigth - x_left) / 2)
        y_radius = int((y_top - y_bottom) / 2)

        radius = min(x_radius, y_radius)

        if verbose:
            print(x_center, y_center, x_radius, y_radius, radius)

        y_min = y_center - radius
        y_max = y_center + radius
        x_min = x_center - radius
        y_max = y_center + radius

        if y_min < 0:
            y_center = y_center - y_min
            y_min = 0
        if x_min < 0:
            x_center = x_center - x_min
            x_min = 0

        if(0):
            y_min = max(0, y_center - radius)
            y_max = min(y_center + radius, x_pixels)
            x_min = max(0, x_center - radius)
            y_max = min(y_center + radius, y_pixels)

        if verbose:
            print(x_min, x_max, y_min, y_max)

        data_new = data[y_min:y_max, x_min:x_max]
        # data_new = data_new.transpose()
        # data_new = np.fliplr(data_new)

        if has_draw:
            plt.figure()
            plt.imshow(np.uint8(data_new * 255))
            plt.title('final image')
            plt.tight_layout()
    else:
        data_new = data

    if reduce_size:
        im = Image.fromarray(np.uint8(data_new * 255))
        im = im.resize((image_size, image_size))
        data_final = np.array(list(im.getdata())).reshape(
            image_size, image_size)
    else:
        im = Image.fromarray(np.uint8(data_new * 255))
        data_final = np.array(list(im.getdata())).reshape(
            x_pixels, y_pixels)
    if has_draw:
        plt.imshow(data_final)
        plt.axis('off')
        plt.tight_layout()
        plt.set_cmap('gray')

    name, file_type = os.path.splitext(filename)

    im.save(name + ending + '.bmp')

    if verbose:
        print("Shape of final image: {}".format(data_final.shape))

    return data_final, im


def batch_extract_binary_image_from_microscope(folder=".", ending='_binarized', cut_level=(0.5, 0.05), convolution_size=0,  reduce_size=False,
                                               image_size=1024, extension='jpg', has_draw=False, verbose=False):
    """Gets an image from the optical microscope (x5) and extract the binary image. It was developed for analyzing the fabricated DOEs.
    Images were acquired using a mobile phone.

    Args:
        folder (string): folder to analyze.
        ending (string): add text to image.
        cut_level (float, float): binarization levels.
        convolution_size (float): if 0 it does not convolute. If >0 it processes the image and generates a convolution for determining the background intensity level. It is useful when the background intensity level is variable.
        reduce_size (bool): If True, the image automatically reduces in order to remove black areas of the ocular.
        image_size (int): image size (1024x1024) for example.
        extension (string): extension of the images to process.
        has_draw (bool): if True, draws several figures to check the correct functioning of the proccess.
        verbose (bool): If True, returns data


    Attributes:
        None: only processed images.

    """

    for dirpath, dirnames, filenames in os.walk(folder):
        for filename in [f for f in filenames if f.endswith("." + extension)]:
            fn_complete = os.path.join(dirpath, filename)
            extract_binary_image_from_microscope(
                fn_complete, ending, cut_level, convolution_size, reduce_size,
                image_size, has_draw, verbose)
            plt.show()


if __name__ == "__main__":

    batch_extract_binary_image_from_microscope(folder=".", ending='_processed', cut_level=(
        0.5, 0.05), convolution_size=0, reduce_size=True, image_size=1024, has_draw=False, verbose=False)
    plt.show()
