#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# vim: ts=4 sw=4 tw=78 ft=python

# This is not finished yet: tehcnique 2 is quite working

import serial
import time
import sys
import os


def open_serial(port, baudrate, parity, stopbits, bytesize, xonxoff):
    try:
        ser = serial.Serial(port, baudrate, parity, stopbits, bytesize,
                            xonxoff)
        return ser
    except OSError as e:
        if str(e).find('No such file or directory') != -1:
            print("ERROR: Path to USB device does not exist (" + ser + ")")
            sys.exit()
        elif str(e).find("Permission denied:"):
            print "Getting permission to change permissions of " + ser
            os.system("gksudo chmod 777 " + port)
            try:
                ser = serial.Serial(port, baudrate, parity, stopbits, bytesize,
                                    xonxoff)
                print "Changed permissions and opened device"
                return ser
            except:
                print "Error opening device!"
                sys.exit()
        else:
            print("Can't open path to device, have you ran: \n" +
                  "sudo chmod 777 " + ser)
            sys.exit()


def connected_serial_ports(verbose=True):
    import serial.tools.list_ports
    list = serial.tools.list_ports.comports()
    connected = []
    for element in list:
        connected.append(element.device)
    if verbose is True:
        print("Connected COM ports: " + str(connected))
    return connected


def read_all():
    import serial.tools.list_ports
    list = serial.tools.list_ports.comports()
    connected = []
    for element in list:
        connected.append(element.device)
    print("Connected COM ports: " + str(connected))

    print connected

    for instrument in connected:
        # configure the serial connections (the parameters differs on the device you are connecting to)
        print instrument
        ser = open_serial(
            port=instrument,
            baudrate=4800,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False)

        ser.isOpen()

        print("consultando a {}".format(instrument))
        ser.write("*idn?" + '\r')
        time.sleep(.25)
        out = ser.read_all()
        print out

        # out = ''
        # while ser.inWaiting() > 0:
        #    out += ser.read(1)

        ser.close()


connected_serial_ports(verbose=True)
read_all()
