#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Si no funciona nada
https://github.com/Spark-Concepts/xPRO/wiki/5.-Troubleshooting#settings-for-most-common-machines

Configuración de parametros
https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration

G codes
http://www.linuxcnc.org/docs/2.5/html/gcode/gcode.html

M codes
http://www.linuxcnc.org/docs/2.5/html/gcode/m-code.html

GRBL codes
https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands

* $ Help
* $$ (grbl settings)
* $# (view # parameters)
* $G (view parser state)
* $I (view build info)
* $N (view startup blocks)
* $x=value (save Grbl setting)
* $Nx=line (save startup block)
* $C (check gcode mode)
* $X (kill alarm lock)
* $H (run homing cycle)
* ~ (cycle start)
* ! (feed hold)
* ? (current status)
* ctrl-x (reset Grbl)

https://www.staticboards.es/blog/dominar-motor-paso-a-paso-con-grbl/
"""

from aocg_instruments.driver_serial_ports import connected_serial_ports
import serial
import time
import pprint
pp = pprint.PrettyPrinter(indent=4)


class Vslot(object):
    def __init__(self, device='/dev/ttyUSB0', verbose=True):

        connected_serial_ports(verbose=True)

        self.vslot = serial.Serial(device, 115200)
        self.verbose = verbose
        self.answer = ""

        # Open grbl serial port
        self.vslot.write("\r\n\r\n")
        time.sleep(2)  # Wait for grbl to initialize
        self.vslot.flushInput()  # Flush startup text in serial input

    def __del__(self):
        self.vslot.close()

    # def _read_line_(self):
    #     grbl_out = self.vslot.readline()
    #     if self.verbose:
    #         print(' : {}'.format(grbl_out))
    #     return grbl_out

    def read_lines(self):
        answers = []
        grbl_out = self.vslot.readline()
        answers.append(grbl_out)
        while 'ok' not in grbl_out:
            grbl_out = self.vslot.readline()
            answers.append(grbl_out)

        if self.verbose:
            pp.pprint(answers)

        self.answer = answers

    def query(self, command):
        if self.verbose:
            print("sending: {}".format(command))
        self.vslot.write("{}\n".format(command))
        self.answer = self.read_lines()

    def send_file(self, filename):
        f = open(filename, 'r')
        answers = []
        for line in f:
            l = line.strip()  # Strip all EOL characters for consistency
            self.query(l)
            answers.append(self.answer)

        f.close()
        return answers

    def close_gcode(self):
        self.vslot.close()

    def help_grbl(self):
        self.query("$")

    def grbl_settings(self):
        self.query("$$")

    def current_status(self):
        self.query("?")

    def view_parameters(self):
        self.query("$#")

    def view_parser_state(self):
        self.query("$G")

    def view_build_info(self):
        self.query("$I")

    def view_startup_blocks(self):
        self.query("$N")

    def check_gcode_mode(self):
        self.query("$C")

    def kill_alarm_lock(self):
        self.query("$X")

    def homming(self):
        self.query("$H")

    def set_position(self, position):
        self.query("X{}".format(position))

    def set_velocity(self, velocity):
        """mm/minute"""
        self.query("$110={}".format(velocity))

    def cycle_start(self):
        self.query("~")

    def feed_hold(self):
        self.query("!")

    def jog_motion(self, position):
        self.query("$J=X{}".format(position))

    def init_parameters(self):
        self.save_grbl_setting(130, 50)  # X max_travel
        self.save_grbl_setting(100, 250)  # X Max rate, mm/min

    def save_grbl_setting(self, x, value):
        self.query("${}={}".format(x, value))

    # Nx=line (save startup block)
