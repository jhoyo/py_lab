# !/usr/bin/env python
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------
# Name:        microscope.py
# Purpose:     class for managing a usb-camera
#
# Author:      Luis Miguel Sanchez Brea
#
# Creation:    03/11/2017
# History:     23/11/2017 - the first version was for lithography, this is for aocg-instruments
#
# Dependences: cv2,datetime
# License:     UCM/AOCG
# ----------------------------------------------------------------------
"""
This module generates allows to control a usb camera, as a webcam or the
microscope.

*Definition of a scalar field*
    * set_resolution
    * see_video
    * capture_image
    * capture_images_from_keyboard
    * show_image, save_image

cameras can be detected with lsusb.
    For microscope_camera: Bus 003 Device 003: ID 0c45:6300 Microdia PC Microscope camera

But it is better using:
    `ls -ltrh /dev/video*`
For example, it returns:
    crw-rw----+ 1 root video 81, 0 nov 23 07:27 /dev/video0
    crw-rw----+ 1 root video 81, 1 nov 23 07:46 /dev/video1


"""

from aocg_instruments import np, plt
import cv2
import datetime
import os


class VideoCamera(object):
    """Class Control the microscope-camera in the lithography project

    Args:
        color (str): 'color' or 'gray'
        has_date (bool): If True adds date (year-month-day-hour-minute-second)
        auto_numbering (bool): If True adds number (0,1,2,...)


    Attributes:
        video (int): control for cv2
        has_date (bool): includes date in filename of saved images
        auto_numbering (bool): includes auto-numbering in file of saved images
        number_frame (int): starts at 0 - number for autonumbering
        gray (bool): if True image is (num_x, num_y) gray_scale.
                     if False image is (num_x, num_y, 3) color RGB

    Todo:
        * pass number_frame to iterator
        * correct color-gray
        * images per second
        * as_image or as list
    """

    def __init__(self,
                 num_device=0,
                 has_date=True,
                 auto_numbering=True,
                 is_gray=True):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        self.video = cv2.VideoCapture(num_device)
        self.verbose = True
        self.has_date = False
        self.has_auto_numbering = False
        self.is_gray = is_gray
        self.set_resolution(width=1600, height=1200)

        self.save_image(filename='dummy_init')

        # properties
        self.has_date = has_date
        self.has_auto_numbering = auto_numbering
        self.number_frame = 0

        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')

    def __del__(self):
        self.video.release()

    # def get_frame(self, filename):
    #     success, frame = self.video.read()
    #     # We are using Motion JPEG, but OpenCV defaults to capture raw images,
    #     # so we must encode it into JPEG in order to correctly display the
    #     # video stream.
    #     ret, frame = cv2.imencode(filename, frame)
    #     return frame.tobytes()

    def show_available_cameras(self):
        os.system('ls -ltrh /dev/video*')

    def show_camera_info(self):
        # for example 046d:082d
        """if ID is None:
            print(os.system('lsusb'))
            ID = raw_input("write ID of camera number:  ")
            print ID
        info = os.system("lsusb -v -d {}".format(ID))
        return ID, info"""
        text = """for d in /dev/video*
            do echo $d
            v4l2-ctl --device=$d -D --list-formats
            echo '==============='
            done"""

        os.system(text)

    def set_resolution(self, width=1600, height=1200):
        """Changes the resolution. Several possible values are:
        (120, 160), (480,  640), (960,  1280), (1200, 1600)

        You can see the values setting to different height width (for example 2000, 2000)
        and after get the properties to see the real.
        """

        self.video.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        self.video.set(cv2.CAP_PROP_FRAME_WIDTH, width)

    def see_video(self):
        """Opens camera and show gray video in window.
        The window is closed hitting 'q' key
        """
        while (True):
            # Capture frame-by-frame
            ret, frame = self.video.read()
            # Our operations on the frame come here
            if self.is_gray:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            # Display the resulting frame
            cv2.imshow('see_video', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()

    def capture_image(self, verbose=True):
        """Acquire image and return matrix. If gray scale (nx,ny), if color (nx,ny,3)

        Returns:
            (bool): retval. True if properly acquired
            (numpy.array): frame.
        """
        # frame = ''
        retval, frame = self.video.read()
        cv2.waitKey(10)

        if self.is_gray:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        else:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        cv2.waitKey(10)

        if verbose is True:
            print("image size = {}".format(frame.shape))

        return retval, frame

    def show_image(self, frame, is_new=True):
        """shows image in a matplotlib window

        Args:
            frame (numpy.array)
            is_new (bool): if True, new figure, else overlaps
        """
        if is_new is True:
            plt.figure()

        matriz = np.array(frame)
        plt.axis('off')
        plt.imshow(matriz)
        plt.show()

    def save_image(self, filename=''):
        """Takes a picture of the camera

        Args:
            filename (str): filename, if '' generates a named
            color (str): 'color' or 'gray'
            has_date (bool): If True include date (year-month-day-hour-minute-second)
            auto_numbering (bool): If True adds number (0,1,2,...) TODO
        """

        if filename == '':
            filename = 'microscope'
        if self.has_date is True:
            date = datetime.datetime.strftime(
                datetime.datetime.today(), format="%y%m%d_%H%M%S_%f")
            filename = "{}_{}".format(filename, date)
        if self.has_auto_numbering is True:
            filename = "{}_{}".format(filename, self.number_frame)
            self.number_frame = self.number_frame + 1

        filename = "{}.png".format(filename)

        retval, frame = self.video.read()
        cv2.waitKey(1)
        if self.is_gray == 'gray':
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cv2.waitKey(1)
        cv2.imwrite(filename, frame)
        print retval
        return filename

    def capture_images_from_keyboard(self,
                                     filename='',
                                     is_saved=True,
                                     is_listed=True):
        """Takes a picture of the camera

        Args:
            filename (str): filename, if '' generates a named
            is_saved (bool): if True, saves image
            is_listed (bool): if True, returns a list with frames

        Returns:
            (list): list_frames - list with images
        """

        if filename == '':
            filename = 'capture_images_from_keyboard'
        if self.has_date is True:
            date = datetime.datetime.strftime(
                datetime.datetime.today(), format="%y%m%d_%H%M%S_%f")
            filename = "{}_{}".format(filename, date)

        cv2.namedWindow("capture_images_from_keyboard")

        img_counter = 0
        list_frames = []

        while True:
            ret, frame = self.video.read()
            if self.is_gray:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow("capture_images_from_keyboard", frame)
            if not ret:
                break
            k = cv2.waitKey(1)

            if k % 256 == 27:
                # ESC pressed
                print("Escape hit, closing...")
                break
            elif k % 256 == 32:
                # SPACE pressed

                # change names
                if self.has_auto_numbering is True:
                    img_name = "{}_{}.png".format(filename, img_counter)
                else:
                    img_name = "{}.png".format(filename)

                print("space pressed: {}".format(img_name))

                if is_saved is True:
                    cv2.imwrite(img_name, frame)
                    print("{} written!".format(img_name))
                if is_listed is True:
                    list_frames.append(frame)
                img_counter += 1

        cv2.destroyAllWindows()
        return list_frames


if __name__ == '__main__':
    c = VideoCamera(
        num_device=1, has_date=True, auto_numbering=True, is_gray=True)

    def test_see_video():
        c.is_gray = True
        c.set_resolution(480, 640)
        c.see_video()

    # capture image
    def test_capture_image():
        c.is_gray = False
        ret_val, frame = c.capture_image()
        c.show_image(frame, is_new=True)

        c.is_gray = True
        ret_val, frame = c.capture_image()
        filename = c.show_image(frame, is_new=True)
        print filename

    def test_several_images_show():
        num_frames = 5
        delay = 1000

        list_images = []
        c.auto_numbering = True
        c.has_date = False
        c.number_frame = 0
        for i in range(num_frames):
            print(i)
            retval, frame = c.capture_image()
            cv2.waitKey(delay)
            list_images.append(frame)
        c.number_frame = 0
        print("finished")

        print(len(list_images))
        for frame in list_images:
            plt.figure()
            matriz = np.array(frame)
            plt.imshow(matriz)

    test_see_video()
    plt.show()
    print("finished")
