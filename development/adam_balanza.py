#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# LCC22 de Thorlabs

# # FUNCIONA

import serial
import time
import serial.tools.list_ports
from aocg_instruments.driver_serial_ports import open_serial, connected_serial_ports

import serial.tools.list_ports as sertools
import re
import platform

error_text = "*****"


def detect_balanza():
    connected_instruments = connected_serial_ports(verbose=True)

    instrument_balanza = None
    for instrument_port in connected_instruments:
        print instrument_port
        ser = open_serial(instrument_port, 4800, serial.PARITY_NONE,
                          serial.STOPBITS_ONE, serial.EIGHTBITS, False)
        # try:
        #     ser = serial.Serial(
        #         port=instrument_port,
        #         baudrate=4800,
        #         parity=serial.PARITY_NONE,
        #         stopbits=serial.STOPBITS_ONE,
        #         bytesize=serial.EIGHTBITS,
        #         xonxoff=False)
        #
        #     ser.isOpen()
        #     ser.close()
        #     print("{} OPENED".format(instrument_port))
        #
        # except:
        #     print("{} not opened".format(instrument_port))

        # print("consultando a {}".format(instrument_port))
        # ser.write("*idn?" + '\r')
        # time.sleep(.25)
        # out = ser.read_all()
        # print out
        #
        # if 'LCC25' in out:
        #     print("Weight detected at {}".format(instrument_port))
        #     instrument_balanza = instrument_port
        # # out = ''
        # # while ser.inWaiting() > 0:
        # #    out += ser.read(1)
        #
        # ser.close()

    return instrument_balanza


class Weight_adam(object):
    def __init__(self, port='/dev/ttyUSB0'):
        if port in [None, '']:
            port = detect_balanza()

        self.sleep_time = 1
        self.verbose = True

        self.ser = None
        self.ser = open_serial(port, 4800, serial.PARITY_NONE,
                               serial.STOPBITS_ONE, serial.EIGHTBITS, False)

        self.ser.isOpen()
        print("ser={}".format(self.ser))
        time.sleep(self.sleep_time)

    def __del__(self):
        self.ser.close()

    def close(self):
        self.ser.close()

    def query(self, command):
        self.ser.write(command + '\r\n')
        time.sleep(self.sleep_time)
        result = self.ser.read_all()
        if self.verbose is True:
            print("command:    {}".format(command))
            print("    result: {}".format(result))
        return result

    def init_weigth(self):
        pass

    def send(self, command):
        self.ser.write("{}\r\n".format(command))
        self.wait()

    def wait(self):
        time.sleep(self.sleep_time)

    def measure(self):
        return self.query("P")

    def tare(self):
        return self.ser.write("T\r\n")

    def zero_point(self):
        return self.ser.write("Z\r\n")
