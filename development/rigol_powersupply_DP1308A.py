# !/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is a copy of https://github.com/sbrinkmann/PyOscilloskop/blob/master/src/usbtmc.py

from aocg_instruments import plt, np, sp
from aocg_instruments.driver_usbtmc import UsbTmcDriver
import time

V = 1.
mV = V / 1000.
A = 1.
mA = A / 1000.

CV = dict(P6V='P6V', P25V='P25V', N25V='N25V')

print(CV['P6V'])
print(CV.values())


class PowerSupply_Rigol_DP1308A(UsbTmcDriver):
    def __init__(self, device=None):
        super(self.__class__, self).__init__(device)
        self.verbose = True

    def query_to_data(self, command, channel, size_data=20):
        """ejecutes a query and convert it to a float"""
        if "{}" in command:
            try:
                data = float(self.query(command.format(channel), size_data))
            except:
                data = "*****"
        else:
            try:
                data = float(self.query(command, size_data))
            except:
                data = "*****"
        return data

    def self_test(self):
        """Query the results of self-test."""
        return self.query("*TST?")

    def save_config(self, channel):
        """
        Save the current system state to the nonvolatile memory with the
        name assigned by <name>.
        channel=(1,2,3,4)
        """
        if channel in (1, 2, 3, 4):
            return self.write("*SAV {}".format(channel))

    def read_config(self, channel):
        """
        Recall the stored instrument state.
        channel=(1,2,3,4)
        """
        if channel in (1, 2, 3, 4):
            return self.write("*RCL {}".format(channel))

    def get_voltage_current(self, channel):
        """Syntax APPLy? [{P6V|P25V|N25V}]
            Function Query the setting values of voltage and current of the specified
            channel.
            Explanations If no channel is specified, the query returns the voltage and current
            of the current channel.
            Example P25V,Limit,10.0000V,0.5000A
        """
        if channel in CV.values():
            print(channel in CV.values())
            return self.query("APPL? {}".format(channel))
        else:
            return "Bad channel"

    def set_voltage_current(self, channel, voltage, current):
        """Syntax APPLy? [{P6V|P25V|N25V}]
            Function Query the setting values of voltage and current of the specified
            channel.
            Explanations If no channel is specified, the query returns the voltage and current
            of the current channel.
            Example P25V,Limit,10.0000V,0.5000A
        """
        if channel in CV.values():
            self.write("APPL {},{},{}".format(channel, voltage, current))
        else:
            print("Bad channel")

    def set_min_voltage_current(self, channel, voltage, current):
        """Syntax APPLy? [{P6V|P25V|N25V}]
            Function Query the setting values of voltage and current of the specified
            channel.
            Explanations If no channel is specified, the query returns the voltage and current
            of the current channel.
            Example P25V,Limit,10.0000V,0.5000A
        """
        if channel in CV.values():
            self.write(
                "APPL {},{} MIN,{} MIN".format(channel, voltage, current))
        else:
            print("Bad channel")

    def set_max_voltage_current(self, channel, voltage, current):
        """Syntax APPLy? [{P6V|P25V|N25V}]
            Function Query the setting values of voltage and current of the specified
            channel.
            Explanations If no channel is specified, the query returns the voltage and current
            of the current channel.
            Example P25V,Limit,10.0000V,0.5000A
        """
        if channel in CV.values():
            self.write(
                "APPL {},{} MAX,{} MAX".format(channel, voltage, current))
        else:
            print("Bad channel")

    def select_instrument(self, channel):
        self.write("INST:SELE {}".format(channel))
        time.sleep(0.25)
        return self.query("INST:SELE?".format(channel))

    def measure(self, channel):
        voltage = self.query_to_data("MEAS:VOLT?", channel)
        current = self.query_to_data("MEAS:CURR?", channel)
        power = self.query_to_data("MEAS:POWE?", channel)
        text = "CHANNEL: {} --> V={}, A={} P={}".format(
            channel, voltage, current, power)
        if self.verbose is True:
            print(text)

        return voltage, current, power, text


pw = PowerSupply_Rigol_DP1308A(device='/dev/usbtmc0')
pw.get_name()
#pw.self_test()
# pw.save_config(channel=1)
# pw.read_config(channel=1)
# pw.get_voltage_current(channel=CV['P6V'])
# pw.get_voltage_current(channel='P6V')
# pw.get_voltage_current(channel='P6dV')
# pw.set_voltage_current(channel=CV['P25V'], voltage=10 * V, current=0.5 * A)
pw.set_min_voltage_current(channel='P25V', voltage=1 * V, current=0.1 * A)
pw.set_min_voltage_current(channel='P25V', voltage=10 * V, current=5 * A)
pw.select_instrument(channel='N25V')
V, C, P, text = pw.measure(channel='N25V')
print(text)

pw.close()
