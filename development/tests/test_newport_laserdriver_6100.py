# !/usr/bin/env python
# -*- coding: utf-8 -*-

from aocg_instruments.newport_laserdriver_6100 import Newport_laserDriver_6100
import os
device = '/dev/bus/usb/003/004'

FILE = os.open(device, os.O_RDWR)
a = os.read(FILE, 1000)
print(a)
command = "*RST" + hex(0xD).encode('ascii') + hex(0xA).encode('ascii')
print command
os.write(FILE, command)

os.close()
