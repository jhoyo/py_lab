# !/usr/bin/env python
# -*- coding: utf-8 -*-

import scipy as sp
from aocg_instruments.utils import closest

x = sp.linspace(0, 100, 101)

solucion = closest(x, 31.5)
print(solucion)
