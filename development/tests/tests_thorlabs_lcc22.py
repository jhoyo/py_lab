# !/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
from aocg_instruments.thorlabs_lcc22 import detect_lcc22, LiquidCristalLCC25_Thorlabs


def test_lcc22_simple(port='/dev/ttyUSB0'):
    """configure the serial connections
    (the parameters differs on the device you are connecting to)
    """
    ser = serial.Serial(
        port=port,
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        xonxoff=False)

    ser.isOpen()

    ser.write("*idn?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("min?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("max?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("freq?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("extern?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("volt1?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("mode=0" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("mode=1" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("mode=2" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("enable=0" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("enable=1" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("volt1=15.123" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("volt2=5.123" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("freq=15.321" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("dwell?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("dwell=1" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("increment?" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("increment=0.5" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("remote=0" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("remote=1" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    ser.write("test" + '\r')
    time.sleep(.25)
    out = ser.read_all()
    print out

    # out = ''
    # while ser.inWaiting() > 0:
    #    out += ser.read(1)

    ser.close()


def test_lcc22_class(port):
    lcd = LiquidCristalLCC25_Thorlabs(port)
    lcd.get_name()
    lcd.set_test()
    lcd.get_volt1()
    lcd.set_remote(remote_mode=1)
    lcd.set_remote(remote_mode=33)
    lcd.set_voltage(level=12.345, channel=2)
    lcd.close()


last_instrument_port = '/dev/ttyUSB0'
# instrument_port = detect_lcc22()
# test_lcc22_simple(port=instrument_port)
test_lcc22_class(port=last_instrument_port)
