# !/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is a copy of https://github.com/sbrinkmann/PyOscilloskop/blob/master/src/usbtmc.py

from aocg_instruments import np, plt
from aocg_instruments.usb_camera import VideoCamera
import cv2
import datetime

num_device = 1
"""
cameras can be detected with lsusb,

For microscope_camera: Bus 003 Device 003: ID 0c45:6300 Microdia PC Microscope camera
"""


def test_show_available_cameras():
    c = VideoCamera(
        num_device=1, has_date=True, auto_numbering=True, is_gray=True)
    c.show_available_cameras()


def test_show_camera_info():
    c = VideoCamera(
        num_device=0, has_date=True, auto_numbering=True, is_gray=True)
    c.show_camera_info()


def test_see_video():
    c = VideoCamera(
        num_device=1, has_date=True, auto_numbering=True, is_gray=True)
    c.is_gray = True
    c.set_resolution(480, 640)
    c.see_video()


# capture image
def test_capture_image():
    c = VideoCamera(
        num_device=1, has_date=True, auto_numbering=True, is_gray=True)
    c.is_gray = False
    ret_val, frame = c.capture_image()
    c.show_image(frame, is_new=True)

    c.is_gray = True
    ret_val, frame = c.capture_image()
    filename = c.show_image(frame, is_new=True)
    print filename


def test_several_images_show():
    c = VideoCamera(
        num_device=1, has_date=True, auto_numbering=True, is_gray=True)
    num_frames = 5
    delay = 1000

    list_images = []
    c.auto_numbering = True
    c.has_date = False
    c.number_frame = 0
    for i in range(num_frames):
        print(i)
        retval, frame = c.capture_image()
        cv2.waitKey(delay)
        list_images.append(frame)
    c.number_frame = 0
    print("finished")

    print(len(list_images))
    for frame in list_images:
        plt.figure()
        matriz = np.array(frame)
        plt.imshow(matriz)


test_show_available_cameras()
test_show_camera_info()
#test_see_video()
plt.show()
print("finished")
