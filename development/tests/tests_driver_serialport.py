# !/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is a copy of https://github.com/sbrinkmann/PyOscilloskop/blob/master/src/usbtmc.py

import os
from aocg_instruments.driver_serial_ports import connected_serial_ports, open_serial, read_all


def test_connected_serial_ports():
    connected = connected_serial_ports(verbose=False)
    print("Connected COM ports: " + str(connected))


#
# def test_connect_selected_device(device_name='DP1380A'):
#     """
#     posibilities:
#         DS1302CA  Rigol Technologies,DS1302CA,DS1AA134900317,04.02.00
#         DG4062    Rigol Technologies,DG4062,DG4D140900074,00.01.01
#         DP1380A   Rigol Technologies,DP1308A,DP1A154500429,00.01.00.00.01.03.01.01.08.00
#                 Bus 003 Device 007: ID 104d:1009 Newport Corporation
#
#     """
#     device = getDeviceFromList(string=device_name)

test_connected_serial_ports()
#test_connect_selected_device()
