# !/usr/bin/env python
# -*- coding: utf-8 -*-

from time import sleep, time
from aocg_instruments import plt, sp
from aocg_instruments.driver_usbtmc import getDeviceFromList
from aocg_instruments.rigol_waveform import waveform_generator_Rigol_DG4062, s

device_name = '/dev/usbtmc2'


def test_connect_selected_device():
    """
    posibilities:
        DS1302CA  Rigol Technologies,DS1302CA,DS1AA134900317,04.02.00
        DG4062    Rigol Technologies,DG4062,DG4D140900074,00.01.01
    """
    device = getDeviceFromList(string="DG4062")
    print("the selected device is {}".format(device))


def test_send_sinusoid():
    wfg = waveform_generator_Rigol_DG4062(device_name)
    wfg.send_sinusoid(
        channel=2, frequency=1001.001, amplitude=1, offset=0, phase=0)
    wfg.output(channel=1, state=True)
    wfg.close()


def test_send_square():
    wfg = waveform_generator_Rigol_DG4062(device_name)
    wfg.send_square(
        channel=2, frequency=1234.567, amplitude=1, offset=0, phase=0)
    wfg.output(channel=2, state=True)
    wfg.close()


def test_send_various():
    wfg = waveform_generator_Rigol_DG4062(device_name)
    wfg.send_sinusoid(
        channel=2, frequency=1001.001, amplitude=1, offset=0, phase=0)
    wfg.send_square(
        channel=1, frequency=1234.567, amplitude=1, offset=0, phase=0)

    wfg.output(channel=1, state=True)
    wfg.output(channel=2, state=True)
    wfg.close()


def test_frequency_variation():
    wfg = waveform_generator_Rigol_DG4062(device_name)
    wfg.output(channel=1, state=True)
    wfg.output(channel=2, state=True)
    wfg.set_beep(state=False)
    wait_time = 0.01 * s

    frequencies = sp.linspace(1000, 2000, 101)

    t1 = time()
    print("total time = {} s".format(len(frequencies) * wait_time))
    for freq in frequencies:
        wfg.send_sinusoid(
            channel=2, frequency=freq, amplitude=1, offset=0, phase=0)
        wfg.send_square(
            channel=1, frequency=freq, amplitude=1, offset=0, phase=0)
    print("time={} s".format(time() - t1))
    wfg.unclock()
    wfg.close()


# test_connect_selected_device()
# test_send_sinusoid()
# test_send_square()
# test_send_various()
test_frequency_variation()

plt.show()
