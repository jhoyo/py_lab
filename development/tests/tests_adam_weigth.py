# !/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import serial
import os
from aocg_instruments.adam_balanza import detect_balanza, Weight_adam


def test_detect_balanza():
    balanza = detect_balanza()
    print balanza


def test_open_simple(port='/dev/ttyUSB0'):
    """configure the serial connections
    (the parameters differs on the device you are connecting to)
    """
    # os.system("gksudo chmod 777 " + port)

    ser = serial.Serial(
        port=port,
        baudrate=4800,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        xonxoff=False)

    ser.isOpen()
    print(ser)
    ser.write("P\r\n")
    time.sleep(0.5)
    result = ser.read_all()
    print("    result: {}".format(result))

    ser.close()


def test_measure_simple(port='/dev/ttyUSB0'):
    """configure the serial connections
    (the parameters differs on the device you are connecting to)
    """
    # os.system("gksudo chmod 777 " + port)

    ser = serial.Serial(
        port=port,
        baudrate=4800,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        xonxoff=False)

    ser.isOpen()
    print(ser)
    ser.write("P\r\n")
    time.sleep(0.5)
    result = ser.read_all()
    print("    result: {}".format(result))

    ser.close()


def test_measure_class(port='/dev/ttyUSB0'):
    peso = Weight_adam(port='/dev/ttyUSB0')
    peso.wait()
    peso.zero_point()
    peso.wait()
    peso.measure()
    peso.wait()
    peso.tare()


# test_detect_balanza()
test_open_simple(port='/dev/ttyUSB0')
# test_open_simple(port='/dev/ttyUSB0')
# test_measure_simple(port='/dev/ttyUSB0')
# test_measure_class(port='/dev/ttyUSB0')
