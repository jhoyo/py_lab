# !/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from py_lab.rigol_osciloscope import osciloscopioRigoDS1302CA, V, us
from py_lab.driver_usbtmc import UsbTmcDriver, getDeviceFromList, getDeviceList, closeDevices
from py_lab.utils import nearest


def test_scales():
    osciloscopio = osciloscopioRigoDS1302CA('/dev/usbtmc0')
    osciloscopio.verbose = False

    osciloscopio.set_voltage_scale(channel=2, scale=1 * V)
    osciloscopio.set_voltage_scale(channel=1, scale=1 * V)
    osciloscopio.set_temporal_scale(scale=1 * us)

    osciloscopio.set_filter(channel=1, is_on=True)

    osciloscopio.unclock()
    osciloscopio.close()


def test_get_parameters():
    osciloscopio = osciloscopioRigoDS1302CA('/dev/usbtmc0')
    osciloscopio.verbose = True

    # osciloscopio.get_parameters(channel=1)
    osciloscopio.get_parameters(channel=2)

    # memory_depth = osciloscopio.get_memory_depth(channel=1)
    # print("memory_depth={}".format(memory_depth))
    osciloscopio.unclock()


def test_get_measurement():
    osciloscopio = osciloscopioRigoDS1302CA('/dev/usbtmc0')
    osciloscopio.verbose = False
    # data1_unprocessed = osciloscopio.get_data(channel=1)
    # data2_unprocessed = osciloscopio.get_data(channel=2)

    time2, data2, parameters2 = osciloscopio.get_data_processed(channel=2)
    osciloscopio.draw(time2, data2, filename='test_use_oscilloscope.svg')
    osciloscopio.show_parameters(parameters2)
    # plt.close('all')

    # Start data acquisition again, and put the scope back in local mode
    # osciloscopio.write(":RUN")
    # osciloscopio.write(":KEY:FORC")
    osciloscopio.unclock()
    # osciloscopio.write(":BEEP:ACTion")


test_scales()
test_get_parameters()
test_get_measurement()

plt.show()
