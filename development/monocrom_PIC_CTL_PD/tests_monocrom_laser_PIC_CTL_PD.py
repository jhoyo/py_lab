import time


import numpy as np
import serial
import serial.tools.list_ports
from monocrom_laser_PIC_CTL_PD import Laser

l_monocrom = Laser()
ports = l_monocrom.get_port_list()
print(ports)
l_monocrom.open_laser('/dev/ttyACM0')


potencias = np.linspace(0, 1023, 100)
potencias = np.int0(potencias)
for potencia in potencias:
    print(potencia, sep='\r', end='\r')
    time.sleep(0.05)
    l_monocrom.set_power(potencia)


l_monocrom.close()
