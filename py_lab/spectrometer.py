from py_lab.config import CONF_AVANTES
import time
import matplotlib.pyplot as plt
from msl.equipment import (EquipmentRecord,ConnectionRecord,Backend)
import numpy as np
import py_lab.utils as utils                                

UNITS_POWER = {'counts': 1}

# REQUIREMENTS: 
# pip install https://github.com/MSLNZ/msl-equipment/releases/download/v0.1.0/msl_equipment-0.1.0-py2.py3-none-any.whl



class Spectrometer(object):
    """
    General class for spectrometers.

    Args:
        name (string): Name of the spectrometer.

    Atributes:
        name (string): Name of the power meter.
        _object (variable): spectrometer object. Its class depends on which device is being used.

    Supported devices:
        * Avantes (AvaSpec ULS3648-USB2)
    """

    def __init__(self, name="Avantes"):
        """Initialize the object.

        Supported devices:
            * Avantes (AvaSpec ULS3648-USB2)
            """
        # Initialize variables
        self.name = name
        self.cfg = None
        self.record = None
        self._object = None

        self.background = np.array((0))
        self.ref = 1
        self.wavelengths = None
        self.number_px = None
        self.exposure = 10
        self.N_average = 1
        self.dark_pixel_data = None
        
        # Initialize object
        if self.name == "Avantes":
            self.record = EquipmentRecord(manufacturer=name,
                model=CONF_AVANTES['model'],  
                serial=CONF_AVANTES['serial'],  
                connection=ConnectionRecord(
                    address=CONF_AVANTES['address'],  
                backend=Backend.MSL,)
            )
        else:
            raise ValueError('{} is not a valid spectrometer name.'.format(name))

    def __enter__(self):
        pass

    def __exit__(self):
        """Delete object last function.

        Supported devices:
            * All.
        """
        self.Close()

    def Open(self):
        """Open object.

        Supported devices:
            * All.
        """
        if self.name == "Avantes":
            self._object = self.record.connect()
            self._object.use_high_res_adc(True)
            self.cfg = self._object.MeasConfigType()
            print('Spectrometer connected!')
        else:
            raise ValueError('Open is not a valid function for spectrometer {}.'.format(self.name))

    def integrate(self, x, y, x_sel = None, width = 2, verbose=False): 
        """Integrate the data in a certain range of wavelengths.

        Args:
            x (array): Wavelengths.
            y (array): Data.
            y_sel (array): Selected y data.
            width (float): Width of the range of wavelengths.
            verbose (bool): If True, prints the integral. Default: False.
        
        Returns:
            float: Integral of the data in the range of wavelengths.

        """
        # Find the maximum value or the selected y of the data
        if x_sel is not None:
            index = np.where(np.abs(x - x_sel) <= 0.1)[0][0]
        else: 
            index = np.argmax(y)

        # Find the range of wavelengths
        min_x = x[index] - width/2
        max_x = x[index] + width/2
        # Integrate the data in the range of wavelengths
        sum_data = np.sum(y[(x >= min_x) & (x <= max_x)])
        length = np.shape(y[(x >= min_x) & (x <= max_x)])[0]
        # Calculate the integral
        result = sum_data/length

        if verbose: 
            print("La integral de los datos en el rango de {:.2f} nm a {:.2f} nm es: {:.0f}".format(min_x,max_x,result))
        return result
        
    def Get_Wavelength(self):
        """Get wavelength values for spectrometer measurements.

        Supported devices:
            * Avantes
        """
        if self.name == "Avantes":
            self.wavelengths = self._object.get_lambda()
            print('The spectrometer has wavelengths from %.2f nm to %.2f nm' % (self.wavelengths[0], self.wavelengths[-1]))
        else:
            raise ValueError('Get_Wavelength is not a valid function for spectrometer {}.'.format(self.name))

    def Get_Number_Px(self):
        """Get get the number of pixels that the spectrometer has.

        Supported devices:
            * Avantes
        """
        if self.name == "Avantes":
            self.number_px = self._object.get_num_pixels()
            print('The spectrometer has %d pixels' % self.number_px)
        else:
            raise ValueError('Get_Wavelength is not a valid function for spectrometer {}.'.format(self.name))

    def Set_Parameters(self,exposure = None,N_average = None):
        """Set the parameters for measurements with the spectrometer.

            Supported devices:
              * Avantes
        """
        if self.name == "Avantes":
            if self.cfg is None: 
                print("Spectrometer config. not initialized. ")
            else:
                if self.wavelengths is None:
                    print("Wavelengths not set. Getting default values.")
                    self.Get_Wavelength()
                    time.sleep(1)
                if self.number_px is None:
                    print("Number of pixels not set. Getting default values.")
                    self.Get_Number_Px()
                    time.sleep(1)
                self.cfg.m_StopPixel = self.number_px - 1
                self.cfg.m_IntegrationTime = CONF_AVANTES["exposure"] if exposure is None else exposure  # in milliseconds
                self.cfg.m_NrAverages = CONF_AVANTES["N_average"] if N_average is None else N_average  # number of averages
                self.cfg.m_Smoothing.m_SmoothPix = 10
                self._object.prepare_measure(self.cfg)

                print('\nParameters set for the spectrometer: ')
                print('     Integration time: {} ms'.format(self.cfg.m_IntegrationTime))
                print('     Number of averages: {}'.format(self.cfg.m_NrAverages))
        else:
            raise ValueError('Set_Parameters is not a valid function for spectrometer {}.'.format(self.name))

        return    

    def Get_Background(self): 
        """Get the background.

        Supported devices:
            * Avantes
        """
        if self.name == "Avantes":
            if self.background.all() == 0: 
                self.background = self.Get_Signal(is_background=False)
            else: 
                self.background = self.Get_Signal()
            self.dark_pixel_data = self._object.get_dark_pixel_data()
            print('Background acquired!')
        else:
            raise ValueError('Get_Background is not a valid function for spectrometer {}.'.format(self.name))

        

        return    
    
    def Clear_Background(self, verbose=True):
        """Clears power background. Must be performed when scale or detector are changed.
        
        Args:
            verbose (bool): If True, prints that background has been cleared. Default: False.
        """
        self.background = 0
        self.dark_pixel_data = None
        if verbose:
            print("Background cleared!")

    def Get_Signal(self, verbose=False, is_background=True, wavelengths = None):
        """Get spectrometer measurement.

        Args:
            verbose (bool): If True, plots the measurement has been taken. Default: False.
            is_background (bool): If True, the background is subtracted. Default: True.
            wavelengths (array): Wavelengths. Default: None.
        
        Supported devices:
            * Avantes
                   
        """
        if self.wavelengths is None or self.number_px is None: 
            self.Set_Parameters()
            print("Parameters not set. Setting default parameters.")

        self._object.measure(1)
        while not self._object.poll_scan():
            time.sleep(0.01)
        _ , data = self._object.get_data()

        if  is_background and self.background.all() == 0:
            print("Background not set.")
        else: 
            data = data - self.background
            data[data < 0] = 0  # Negative values are not possible
            
        if wavelengths is not None:
            # Localizar los índices de los elementos de self.wavelengths que estén más próximos a los elementos de wavelengths
            indices = np.array([np.abs(self.wavelengths - w).argmin() for w in wavelengths])
            data = data[indices]
        
        if verbose:
            plt.figure(figsize=[8,3])
            if wavelengths is not None:
                plt.plot(wavelengths, data)
            else: 
                plt.plot(self.wavelengths, data)
            plt.xlabel('Wavelength (nm)')
            plt.ylabel('Intensity (counts)')
            plt.title('Spectrometer Measurement')
            plt.show()

        return data

        
    def Get_Measurement(self, mode = "max", wavelength = None, width = 4, verbose=False, use_ref = True):
        """Get power signal from spectrometer.

        Args:

            mode (string): Mode of the signal. Default: "integral".
            wavelength (float): Wavelength of the signal. Default: None.
            width (float): Width of the range of wavelengths. Default: 4.
            verbose (bool): If True, plots the signal. Default: False.
            use_ref (bool): If True, uses the reference signal. Default: True.
        
        Returns:

            float: Power signal.

        Supported devices:

            * Avantes
        """ 

        data = self.Get_Signal(verbose=verbose)
        if mode == "max": 
            signal = np.max(data)
        elif mode == "wavelength": 
            index = np.where(np.abs(self.wavelengths - wavelength) <= 0.1)[0][0]   
            signal = data[index]
        elif mode == "integral":
            signal = self.integrate(x = self.wavelengths, y = data, x_sel= wavelength, width = width)
        
        if use_ref:
            if self.ref == 1:
                if verbose: 
                    print("Reference not set. Using signal as reference.")
            signal = signal / self.ref
        
        return signal
    

    def Spectrometer_Stability(self, Nmeasurements, Twait, mode = "max", wavelength = None, width = 4, ref = False):
    
        """Function to measure spectrometer stability.
        
        Args:
        
            Nmeasurements (int): Number of measurements.
            Twait (float): Time to wait between measurements.
            mode (string): Mode of the signal. Default: "integral".
            wavelength (float): Wavelength of the signal. Default: None.
            width (float): Width of the range of wavelengths. Default: 4.
            verbose (bool): If True, plots the signal. Default: False.

        Returns:

        """
        # Print the mode
        print("The mode of the signal is: " + mode)

        # Initialize variables
        Iexp = np.zeros([Nmeasurements, 2])

        # Make measurements
        for ind in range(Nmeasurements):
            
            Iexp[ind, :] = self.Get_Measurement(mode = mode, wavelength = wavelength, width = width)
            time.sleep(Twait)
            print("Measurement {} of {} done".format(ind+1, Nmeasurements), end='\r', flush=True)

        # Make stadistics
        ratio_individual = Iexp[:, 0] / Iexp[:, 1]
        mean = np.mean(Iexp, axis=0)
        error = np.std(Iexp, axis=0)
        ratio2 = np.mean(ratio_individual)
        ratio_error2 = np.std(ratio_individual)
        error_correction = np.sqrt((error[0] / mean[0])**2 +
                                2 * (error[1] / mean[1])**2)
        # Fake plot data
        t = range(Nmeasurements)
        meanCh1y = np.ones(Nmeasurements) * mean[0]
        meanCh1yUp = np.ones(Nmeasurements) * (mean[0] + error[0])
        meanCh1yDown = np.ones(Nmeasurements) * (mean[0] - error[0])
        meanCh2y = np.ones(Nmeasurements) * mean[1]
        meanCh2yUp = np.ones(Nmeasurements) * (mean[1] + error[1])
        meanCh2yDown = np.ones(Nmeasurements) * (mean[1] - error[1])
        meanRy2 = np.ones(Nmeasurements) * ratio2
        meanRyUp2 = np.ones(Nmeasurements) * (ratio2 + ratio_error2)
        meanRyDown2 = np.ones(Nmeasurements) * (ratio2 - ratio_error2)
        # Plot it
        plt.figure(figsize=(18, 8))
        plt.plot(t, Iexp[:, 0] / meanCh1y, 'k')
        plt.plot(t, meanCh1y / meanCh1y, 'b')
        plt.plot(t, meanCh1yUp / meanCh1y, 'r--')
        plt.plot(t, meanCh1yDown / meanCh1y, 'r--')
        plt.title('Normalized Measures (Signal)')
        plt.xlabel('Time (s)')
        plt.ylabel('Intensity (V)')
        # plt.subplot(1, 2, 2)
        # plt.plot(t, Iexp[:, 1] / meanCh2y, 'k')
        # plt.plot(t, meanCh2y / meanCh2y, 'b')
        # plt.plot(t, meanCh2yUp / meanCh2y, 'r--')
        # plt.plot(t, meanCh2yDown / meanCh2y, 'r--')
        # plt.title('Normalized PhD 2 (Reference)')
        # plt.xlabel('Time (s)')
        # plt.ylabel('Intensity (V)')
        # plt.subplot(1, 3, 3)
        # plt.plot(t, ratio_individual / meanRy2, 'k')
        # plt.plot(t, meanRy2 / meanRy2, 'b')
        # plt.plot(t, meanRyUp2 / meanRy2, 'r--')
        # plt.plot(t, meanRyDown2 / meanRy2, 'r--')
        # plt.title('Ratio PhD1 / PhD2')
        # plt.xlabel('Time (s)')
        # plt.ylabel('Intensity (V)')

        # Print result
        print('The resuts are:')
        print('   - Signal channel          : {:.4f} +- {:.4f} V.'.format(
            mean[0], error[0]))
        print('   - Reference channel       : {:.4f} +- {:.4f} V.'.format(
            mean[1], error[1]))
        print('   - Ratio error             : {:.2f} %.'.format(
            ratio_error2 * 100))
        print('   - Error in corrected I    : {:.2f} %.'.format(
            error_correction * 100))
        
        if ref: 
            self.ref = mean[0]
            print('Reference set!')

        return mean[0] #, ratio_error2  


        
    def Close(self):
        """Close object.

        Supported devices:
            * All.
        """
        if self.name == "Avantes":
            if self._object is not None:
                self.Clear_Background(verbose=False)
                self._object.disconnect()
                self._object = None
                print('Spectrometer disconnected!')
            else: 
                print('Spectrometer already disconnected!')

    
    