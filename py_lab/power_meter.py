from py_lab.config import CONF_SOLO2, CONF_OPHIR

from py_lab.drivers.power_meters.ophir.ophir import Ophir
from py_lab.drivers.power_meters.solo2.solo2 import Solo2

import time
import numpy as np

UNITS_POWER = {'W': 1, 'mW': 1e-3, 'uW': 1e-6, 'kW': 1e3}


class Power_Meter(object):
    """
    General class for power meters.

    Args:
        name (string): Name of the power meter. Available power meters are: SMC100.

    Atributes:
        name (string): Name of the power meter.
        background (float): Stored background.
        _object (variable): power meter object. Its class depends on which device is being used.

    Supported devices:
        * Ophir
        * Solo2
    """

    def __init__(self, name="Ophir", port=None):
        """Initialize the object.

        Solo2 args:
            port (string): Serial port connected to the controller.

        Supported devices:
            * Ophir
            * Solo2
            """
        self.name = name
        self.background = 0

        if name == "Ophir":
            self._object = Ophir()
            self.is_live = False
        elif name == "Solo2":
            if port is None:
                port = CONF_SOLO2['port']
            self._object = Solo2(port)
            self._object.set_multipliers(1, 1)
            self._object.set_offsets(0, 0)
            self._object.set_trigger_mode(False)
            self._object.set_background_mode(0)

        else:
            raise ValueError('{} is not a valid power meter name.'.format(name))


    def __enter__(self):
        pass

    def __exit__(self):
        """Delete object last function.

        Supported devices:
            * All.
        """
        self.Close()
        
    def Print_Ranges(self):
        """Get measurement ranges.

        Supported devices:
            * Ophir
        """
        if self.name == "Ophir":
            ranges = self._object.get_range()[1]
        elif self.name == "Solo2":
            ranges = self._object.print_scales()
        else:
            raise ValueError('Print_Ranges is not a valid function for power meter {}.'.format(self.name))
            
        for ind, range in enumerate(ranges):
            print("Rango ", ind, " = ", range)
        
        
    def Get_Range(self, verbose=False):
        """Get measurement range.

        Args:
            verbose (bool): If True, prints range. Default: False.

        Supported devices:
            * Ophir
        """
        if self.name == "Ophir":
            ranges = self._object.get_range()
            range = ranges[0]
            
            if verbose:
                print("Escala: ", ranges[1][range])
            
        else:
            raise ValueError('Get_Range is not a valid function for power meter {}.'.format(self.name))

            
        return range
    
    def Set_Range(self, range, verbose=False):
        """Set measurement range.

        Args:
            range (int): Scale number.
            verbose (bool): If True, prints range. Default: False.

        Supported devices:
            * Ophir
        """
        if self.name == "Ophir":
            if self.is_live:
                self.Stop_Live()
                self.Is_Live(verbose=True)
                
            self._object.set_range(range)
            range = self.Get_Range(verbose)
            
        elif self.name == "Solo2":
            self._object.set_scale(range)

        else:
            raise ValueError('Set_Range is not a valid function for power meter {}.'.format(self.name))
            
        self.Clear_Background(verbose=True)
        return range
    
    def Start_Live(self):
        """Start acquiring data.

        Supported devices:
            * Ophir
        """
        if self.name == "Ophir":
            self._object.start_live()
            
        else:
            raise ValueError('Start_Live is not a valid function for power meter {}.'.format(self.name))
            
        self.is_live = True
    
    def Stop_Live(self):
        """Stop acquiring data.

        Supported devices:
            * Ophir
        """
        if self.name == "Ophir":
            self._object.stop_live()
            
        else:
            raise ValueError('Stop_Live is not a valid function for power meter {}.'.format(self.name))
            
        self.is_live = False
    
    
    def Is_Live(self, verbose=True):
        """Query if device is measuring data.

        Supported devices:
            * Ophir
        """
        if verbose:
            print("Device is measuring: ", self.is_live)
            
        else:
            raise ValueError('Is_Live is not a valid function for power meter {}.'.format(self.name))
            
        return self.is_live
    
    def Clear_Background(self, verbose=False):
        """Clears power background. Must be performed when scale or detector are changed.
        
        Args:
            verbose (bool): If True, prints that background has been cleared. Default: False.
        """
        self.background = 0
        if verbose:
            print("Background cleared!!!")
        
        
    def Get_Power(self, Nmeasures=1, units="W", twait=None, average=False, is_background=False, use_background=True, verbose=False):
        """Get power measurement.

        Args:
            Nmeasures (int): Number of measurements to take. If <= 0, takes the average. Default: 1.
            units (str): Units of the power. Default: 'W'.
            twait (float or None): Wait time between measurements. If None, uses the default for each device. Default: None.
            average (bool): If True, averages the results. Default: False.
            is_background (bool): If True, stores the measurement as background. Default: False.
            use_background (bool): If True, subtracts the stored background from the measurement. Default: True.
            verbose (bool): If True, prints power. Default: False.

        Supported devices:
            * Ophir
        """  
        # Particular PM functions
        if self.name == "Ophir" and not self.is_live:
            self.Start_Live()
            
        # Loop several measurements
        power = np.zeros(Nmeasures)
        for ind in range(Nmeasures):
            
            # Measurement
            if self.name == "Ophir":
                if twait is None:
                    twait = CONF_OPHIR['Twait']
                    
                data = list(self._object.get_data())
                power[ind] = data[0][-1]
                
            elif self.name == "Solo2":
                if twait is None:
                    twait = CONF_SOLO2['Twait']
                power[ind] = self._object.get_power()
            time.sleep(twait)
        
        # Average
        if average:
            power = np.mean(power)
        elif Nmeasures == 1:
            power = power[0]
            
        # Transform units
        if is_background:
            units = 'W'
        if units in UNITS_POWER:
            power = power * UNITS_POWER[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))
        
        # Background operations
        if is_background:
            self.background = power
            
        elif use_background:
            power = power - self.background
            
        else:
            raise ValueError('Get_Power is not a valid function for power meter {}.'.format(self.name))
            
        if verbose:
            print("Power: ", power)
            
        return power
    
    def Set_Wavelength(self, wavelength):
        """Set wavelength value for power correction.

        Args:
            wvelength (int): wavelength in nanometers.

        Supported devices:
            * Solo2
        """
        if self.name == "Solo2":
            self._object.set_correction_wavelength(wavelength)
            
        else:
            raise ValueError('Set_Wavelength is not a valid function for power meter {}.'.format(self.name))

    
    def Close(self):
        """Close object.

        Supported devices:
            * All.
        """
        if self.name == "Ophir":
            self._object.close()
        elif self.name == "Solo2":
            self._object.close()

    
    