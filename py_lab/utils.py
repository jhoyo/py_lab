import numpy as np
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
from matplotlib import cm
from PIL import Image
from copy import deepcopy
import serial
from serial.tools import list_ports
import usb.core
import matplotlib.pyplot as plt
import cv2
import os

from py_lab.config import number_types

from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_sources_XY import Scalar_source_XY

from py_pol.utils import charac_angles_2_azimuth_elipt, azimuth_elipt_2_charac_angles, which_quad, degrees
from py_pol.stokes import Stokes
from py_pol.jones_vector import Jones_vector

TYPES_DIFFRACTIO = (Scalar_mask_XY, Scalar_field_XY, Scalar_source_XY)
EQ_TOLERANCE = 1e-4

um = 1e3
UNITS_POS = {'mm': 1, 'm': 1e3, 'um': 1e-3, 'deg': 1, 'rad': np.pi / 180}

###############################
# POLARIMETRY
###############################


def _POLARIMETRY():
    x = 2 + 3
    return x


def PSG_angles_2_states(phi_P, phi_Q, output='azimuth'):
    """Función que calcula el estado que sale del PSG (con intensidad normalizada) a partir de los ángulos del polarizador y el retardador.
    La salida puede ser tanto en azimut y elipticidad (por defecto) como en alpha y delay (ángulos caracteristicos, paper de Moreno) o objeto Stokes o Jones_vector.
    """
    phi_P = vectorize(phi_P)
    phi_Q = vectorize(phi_Q)
    # Si son numeros, hacer arrays los ángulos
    phi_Q = np.array(phi_Q % np.pi)
    phi_P = np.array(phi_P % np.pi)
    phi_dif = phi_Q - phi_P
    # Calcular el octante en que se encuentran los ángulos
    Odif = which_quad(phi_dif, octant=True)
    Sdif = np.sign(Odif)
    Odif = np.abs(Odif)
    Oq = which_quad(phi_Q, octant=True)
    Op = which_quad(phi_P, octant=True)

    # Primero la elipticidad que es mas facil
    el = deepcopy(phi_dif)
    cond = (Odif > 1.5) * (Odif < 4)
    el[cond] = Sdif[cond] * 90 * degrees - phi_dif[cond]
    cond = Odif >= 4
    el[cond] = phi_dif[cond] - Sdif[cond] * 180 * degrees
    # Ahora el azimut. Este depende del azimuth del polarizador
    az = deepcopy(phi_Q)
    cond = (Odif > 1.5) * (Odif < 3.5)
    az[cond] = az[cond] + 90 * degrees
    az = az % np.pi
    # Transformar a alpha y delay si hace falta
    S = None
    
    if output.lower() == "stokes":
        S = Stokes().general_azimuth_ellipticity(intensity=1, azimuth=az, ellipticity=el)
    elif output.lower() in ("jones", "jones_vector", "jones vector"):
        S = Jones_vector().general_azimuth_ellipticity(intensity=1, azimuth=az, ellipticity=el)
    elif output.lower() != 'azimuth':
        az, el = azimuth_elipt_2_charac_angles(az, el)
    # Return
    az = devectorize(az)
    el = devectorize(el)
    if S is None:
        return az, el
    else:
        return S


def PSG_states_2_angles(azimuth=None, ellipticity=None, S=None, alpha=None,
                        delay=None):
    """Función para pasar de estados a ángulos de giro de los elementos del PSG.
    Únicamente hay que darle un par de valores, azimut y elipticidad o alpha y delay (ángulos característicos, paper de Moreno).
    Si se dan ambas combinaciones, los ángulos característicos prevalecen."""
    # Obtener valor del estado
    if S is not None:
        azimuth, ellipticity = S.parameters.azimuth_ellipticity(verbose=False, out_number=True, use_nan=False)
    # Transformar si es necesario
    if alpha is not None and delay is not None:
        azimuth, ellipticity = charac_angles_2_azimuth_elipt(alpha, delay)
    # Hacer el cáclculo
    angle_R = azimuth
    angle_P = (azimuth - ellipticity) % np.pi
    # Return
    return angle_P, angle_R


def PSA_angles_2_states(phi_P, phi_Q, output='azimuth'):
    """Función que calcula el estado de máxima transmitancia del analizador de estados (con intensidad normalizada) a partir de los ángulos del polarizador
    y el retardador. La salida puede ser tanto en azimut y elipticidad (por defecto) como en alpha y delay (ángulos caracteristicos, paper de Moreno)."""
    phi_P = vectorize(phi_P)
    phi_Q = vectorize(phi_Q)
    # Si son numeros, hacer arrays los ángulos
    phi_Q = np.array(phi_Q % np.pi)
    phi_P = np.array(phi_P % np.pi)
    phi_dif = phi_P - phi_Q
    # Calcular el octante en que se encuentran los ángulos
    Odif = which_quad(phi_dif, octant=True)
    Sdif = np.sign(Odif)
    Odif = np.abs(Odif)
    Oq = which_quad(phi_Q, octant=True)
    Op = which_quad(phi_P, octant=True)

    # Primero la elipticidad que es mas facil
    el = deepcopy(phi_dif)
    cond = (Odif > 1.5) * (Odif < 4)
    el[cond] = Sdif[cond] * 90 * degrees - phi_dif[cond]
    cond = Odif >= 4
    el[cond] = phi_dif[cond] - Sdif[cond] * 180 * degrees
    # Ahora el azimut. Este depende del azimuth del polarizador
    az = deepcopy(phi_Q)
    cond = (Odif > 1.5) * (Odif < 3.5)
    az[cond] = az[cond] + 90 * degrees
    az = az % np.pi
    # Transformar a alpha y delay si hace falta
    S = None
    if output.lower() == "stokes":
        S = Stokes().general_azimuth_ellipticity(intensity=1, azimuth=az, ellipticity=el)
    elif output.lower() in ("jones", "jones_vector", "jones vector"):
        S = Jones_vector().general_azimuth_ellipticity(intensity=1, azimuth=az, ellipticity=el)
    elif output.lower() != 'azimuth':
        az, el = azimuth_elipt_2_charac_angles(az, el)
    # Return
    az = devectorize(az)
    el = devectorize(el)
    if S is None:
        return az, el
    else:
        return S


def PSA_states_2_angles(azimuth=None, ellipticity=None, alpha=None, S=None,
                        delay=None):
    """Función para pasar de estados a ángulos de giro de los elementos del analizador de estados.
    Únicamente hay que darle un par de valores, azimut y elipticidad o alpha y delay (ángulos característicos, paper de Moreno).
    Si se dan ambas combinaciones, los ángulos característicos prevalecen."""
    # Obtener valor del estado
    if S is not None:
        azimuth, ellipticity = S.parameters.azimuth_ellipticity(verbose=False, out_number=True, use_nan=False)
    # Transformar si es necesario
    if alpha is not None and delay is not None:
        azimuth, ellipticity = charac_angles_2_azimuth_elipt(alpha, delay)
    # Hacer el cáclculo
    angle_R = azimuth
    angle_P = (azimuth + ellipticity) % np.pi
    # Return
    return angle_P, angle_R


def sort_pes(p1, p2, angle, wrap=np.pi):
    """Function that rearranges pes so p1 is always >= p2. If the pes are changed, the angle is changed accordingly."""
    # Make variables of the same length
    lengths = np.array([len_all(p1), len_all(p2), len_all(angle)])
    Nmax = lengths.max()
    if not np.all((lengths == 1) | (lengths == Nmax)):
        raise ValueError(
            "p1, p2 and angle of lengths {}, {} and {} are not equal or 1".
            format(*lengths))
    if lengths.max() > 1:
        p1 *= np.ones(Nmax)
        p2 *= np.ones(Nmax)
        angles *= np.ones(Nmax)

    # Correct
    cond = p2 > p1
    if cond.any():
        paux = p1[cond]
        p1[cond] = p2[cond]
        p2[cond] = paux
        angle[cond] = angle[cond] + wrap

    # Return
    if Nmax == 1:
        p1 = p1[0]
        p2 = p2[0]
        angle = angle[0]
    return p1, p2, angle


###############################
# PROCESS CURVES
###############################


def _CURVES():
    pass


def Unwrap(x, axis=-1, step=np.pi, threshold=0.5, progresive=False):
    """Function that calculates the 1-D unwrap of phase using different intervals.

    Args:
        x (numpy.ndarray): Array to unwrap.
        axis (int): Axis to perform the unwrap. Default: -1.
        step (float): Reference step to perform the unwrap. Default: np.pi.
        threshold (float): Threshold to add step. Default: 0.5.
        progresive (bool): If phase is progresive, one point should not affect the following. Default: False.

    Returns:
        result (np.ndarray): Unwraped phase.
    """
    # Calculate the dif
    if progresive:
        dif = x / step
    else:
        dif = np.diff(x, axis=axis, prepend=0) / step

    # Calculate the int step
    dif_int = np.floor(dif + threshold)

    # Take into acount the result of previous points
    if not progresive:
        dif_int = np.cumsum(dif_int, axis=axis)

    # Calculate result
    result = x - dif_int * step

    # plt.figure(figsize=(12,9))
    # plt.plot(x/step)
    # plt.plot(dif)
    # plt.plot(np.floor(dif) + (dif % 1) > threshold)
    # plt.plot(dif_int)
    # plt.plot(result/step)
    # plt.xlim(120, 200)
    # plt.legend(('x/step', 'dif', 'np.floor(dif)', 'dif_int', 'result'))

    return result


def Filter_Curves(I,
                  method="narrowpeaks",
                  threshold=50,
                  type_peaks=(True, True),
                  max_width_peaks=(4, 4)):
    """Function to filter curves that, once in a while, show isolated peaks.

    Args:
        I (np.ndarray): 1xN array of intensities to filter.
        method (string): Filtering method. Default: ''.
            'narrowpeaks': Assumes 1 point peaks. Finds them and interpolates them.
            'fft': Flattens the highest points of the FFT.
            'widepeaks': Uses scipy find_peaks to find the peaks and calculate their width
        threshold (int): If method is 'fft', determines how many end points of the fft are flattened.
        type_peaks ((bool, bool)): A 2 element tuple that states if positive and negative peaks must be searched if method is 'widepeaks'. Default: (True, True)
        max_width_peaks ((int, int)): Max width of the considered peaks in the widepeaks method. Default: (4, 4).

    Returns:
        If (np.ndarray): Filtered intensity.
    """
    if method.lower() == "narrowpeaks":
        # Find the peaks
        ind = np.arange(I.size)
        dif = np.diff(I, prepend=0)
        cond = dif < np.mean(np.abs(dif))
        ind_good = ind[cond]
        I_good = I[cond]
        # Interpolate
        If = np.interp(ind, ind_good, I_good)
        # plt.figure(figsize=(10,5))
        # plt.plot(I)
        # plt.plot(If)

    elif method.lower() == "fft":
        Ifft = np.fft.rfft(I)
        Ifft2 = Ifft
        Ifft2[-threshold + 1:] = Ifft[-threshold]
        If = np.fft.irfft(Ifft2)
        # plt.figure(figsize=(20,5))
        # plt.subplot(1,2,1)
        # plt.plot(I)
        # plt.plot(If)
        # plt.subplot(1,2,2)
        # plt.semilogy(Ifft+1)
        # plt.semilogy(Ifft2+1)
        # plt.draw()

    elif method == "widepeaks":
        # Find peaks
        dif = np.diff(I)
        threshold = np.mean(np.abs(dif))
        indices = np.arange(I.size)

        # Positive peaks
        if type_peaks[0]:
            peaks, prop = find_peaks(
                I, height=threshold, width=(1, max_width_peaks[0]))
            ind_left = np.floor(prop["left_ips"])
            ind_right = np.ceil(prop["right_ips"])
            # Find points to be interpolated
            cond = np.zeros_like(I, dtype=bool)
            for ind in range(ind_left.size):
                cond = cond + \
                    ((indices >= ind_left[ind]) * (indices <= ind_right[ind]))
            # Interpolate
            ind_good = indices[np.logical_not(cond)]
            I_good = I[np.logical_not(cond)]
            If = np.interp(indices, ind_good, I_good)
        else:
            If = I
        # Repeat for negative peaks
        if type_peaks[1]:
            peaks, prop = find_peaks(
                5 - If, height=threshold, width=(1, max_width_peaks[1]))
            ind_left = np.floor(prop["left_ips"])
            ind_right = np.ceil(prop["right_ips"])
            cond = np.zeros_like(If, dtype=bool)
            for ind in range(ind_left.size):
                cond = cond + \
                    ((indices >= ind_left[ind]) * (indices <= ind_right[ind]))
            # Interpolate
            ind_good = indices[np.logical_not(cond)]
            I_good = If[np.logical_not(cond)]
            If2 = np.interp(indices, ind_good, I_good)
        else:
            If2 = If
        # plt.figure(figsize=(10,5))
        # plt.plot(I)
        # plt.plot(If)
        # plt.plot(If2)
        If = If2
    return If


def Prepare_For_Interpolation(y, kind='amplitude'):
    """Function to prepare a curve for interpolation.

    Args:
        y (np.ndarray): Curve to prepare. It is supposed to be an amplitude or phase curve.
        kind (str): Kind of curve: 'amplitude' or 'phase'.

    Returns:
        y_final (np.ndarray): Final curve.
        levels (np.ndarray): Final gray level.
    """
    # Variables
    levels = np.arange(y.size)
    prominence = 0.05 if kind == 'amplitude' else 0.1
    window_len = 11 if kind == 'amplitude' else 21

    # plt.figure(figsize=(20, 5))
    # plt.subplot(1, 2, 1)
    # plt.plot(levels, y)

    # First processing: Only smooth separating by big jumps
    # Separate the curve in peaks for smoothing
    peaks, _ = find_peaks(y, prominence=prominence)
    peaks2, _ = find_peaks(-y, prominence=prominence)
    peaks = np.sort(np.concatenate((peaks, peaks2)))
    # Remove first and last points
    if peaks.size > 0 and peaks[0] == 0:
        peaks = peaks[1:]
    if peaks.size > 0 and peaks[0] == 1:
        peaks = peaks[1:]
    if peaks.size > 0 and peaks[-1] == y.size - 1:
        peaks = peaks[:-1]
    if peaks.size > 0 and peaks[-1] == y.size - 2:
        peaks = peaks[:-1]
    # Remove adyacent or repeated points
    to_remove = []
    for ind, elem in enumerate(peaks):
        if ind > 0 and elem - peaks[ind - 1] <= 1:
            to_remove.append(ind)
    peaks = np.delete(peaks, to_remove) if len(to_remove) > 0 else peaks
    # Split the curve and levels
    y_split = np.split(y, peaks + 1)
    # Smooth the curves
    for ind, elem in enumerate(y_split):
        elem = Smooth(elem, window_len=window_len)
        y_split[ind] = elem
    # Merge again
    y = np.concatenate(y_split)

    # plt.plot(levels, y)

    # Second processing: Split by absolute peaks
    # Separate the curve in peaks for smoothing
    peaks, _ = find_peaks(y)
    peaks2, _ = find_peaks(-y)
    peaks = np.sort(np.concatenate((peaks, peaks2)))
    # Remove first and last points
    if peaks.size > 0 and peaks[0] == 0:
        peaks = peaks[1:]
    if peaks.size > 0 and peaks[0] == 1:
        peaks = peaks[1:]
    if peaks.size > 0 and peaks[-1] == y.size - 1:
        peaks = peaks[:-1]
    if peaks.size > 0 and peaks[-1] == y.size - 2:
        peaks = peaks[:-1]
    # Remove adyacent or repeated points
    to_remove = []
    for ind, elem in enumerate(peaks):
        if ind > 0 and elem - peaks[ind - 1] <= 1:
            to_remove.append(ind)
    peaks = np.delete(peaks, to_remove) if len(to_remove) > 0 else peaks
    # Split the curve and levels
    y_split = np.split(y, peaks + 1)
    levels_split = np.split(levels, peaks + 1)
    # Smooth the curves
    for ind, elem in enumerate(y_split):
        elem = Smooth(elem)
        y_split[ind] = elem
    # Process curves
    mins = []
    maxs = []
    difs = []
    to_remove = []
    for ind, elem in enumerate(y_split):
        # Remove objects of size < 1
        if elem.size < 2:
            to_remove.append(ind)
        else:
            # Retrict between min and max
            ind_min = np.argmin(elem)
            ind_max = np.argmax(elem)
            ind_min, ind_max = (min(ind_min, ind_max), max(ind_min, ind_max))
            elem = elem[ind_min:ind_max + 1]
            levels_split[ind] = levels_split[ind][ind_min:ind_max + 1]
            # Store data
            if elem.size > 1:
                dif = np.abs(np.diff(elem))
                difs.append(np.concatenate((np.array([dif[0]]), dif)))
            else:
                difs.append(np.array([1e10]))
            mins.append(np.min(elem))
            maxs.append(np.max(elem))
            y_split[ind] = elem
    for ind in to_remove:
        del y_split[ind]
        del levels_split[ind]

    # Unitarize
    new_y_split = []
    for ind, elem in enumerate(y_split):
        to_remove = []
        for indY, y in enumerate(elem):
            for indC, elemC in enumerate(y_split):
                if (ind != indC) and (mins[indC] <= y <= maxs[indC]):
                    # Take only the one with less variation
                    dif1 = difs[ind][indY]
                    indYC = np.interp(y, elemC, np.arange(elemC.size))
                    dif2 = np.interp(indYC, np.arange(elemC.size), difs[indC])
                    if dif1 > dif2:
                        to_remove.append(indY)
                        break
        new_y_split.append(np.delete(elem, to_remove))
        levels_split[ind] = np.delete(levels_split[ind], to_remove)

    # Merge
    y = np.concatenate(new_y_split)
    levels = np.concatenate(levels_split)

    # Minimum phase change is 0
    if kind == 'phase':
        y = y - np.min(y)

    # plt.plot(levels, y, '+g')
    # plt.legend(('Original', 'Smooth', 'Piecewise'))

    # Sort
    ind = np.argsort(y)
    y = y[ind]
    levels = levels[ind]

    # plt.subplot(1, 2, 2)
    # plt.plot(y, levels)
    #
    # # Smooth final result
    # y = Smooth(y, window_len=window_len)
    #
    # plt.plot(y, levels)
    # plt.legend(('Original', 'Smoothed'))

    return y, levels


def Smooth(x, window_len=11, window='hanning'):
    """Smooth the data using a window with requested size. Taken from https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        x.flatten()

    # if x.size < window_len:
    #     window_len = 2 * (x.size // 2 + x.size % 2) - 1

    if window_len < 3 or x.size < window_len:
        return x

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError(
            "Window must be 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
        )

    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    # print(len(s))
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='valid')
    return y[(window_len // 2 - 1):-(window_len // 2 + 1)]


def Interpolate_Mask(mask, cal, kind, norm=True):
    """Interpolate the mask according to calibration.

    Args:
        mask (np.ndarray): Mask to interpolate.
        cal (tuple with 6 np.ndarrays): Calibration tuple.
        kind (string): If IMAGE is Scalar_mask_XY, selects the information sent to the SLM. There are three options: 'phase', 'amplitude' or 'intensity'. Default: 'phase'.
        norm (bool): If True, amplitude masks are normalized to maximum values. Default: True.

    Returns:
        im (np.ndarray): Interpolated mask.
    """
    # Normalize
    if norm and kind.lower() == 'amplitude':
        mask = mask * np.max(cal[0]) / np.max(mask)

    # Interpolate
    if kind.lower() == 'amplitude':
        # Amplitude interpolation is easy
        mask = np.interp(mask, cal[0], cal[2])
    else:
        # We have to restrict mask to tha maximum range
        mask = mask % np.max(cal[4])
        mask = np.interp(mask, cal[4], cal[5])

    return mask


def Make_Non_Negative(x):
    """Function to make some values non-negative (>= 0).

    Args:
        x (np.ndarray): Array to be transformed.

    Returns:
        result (np.ndarray): Transformed array.
    """
    res = x
    res[x < 0] = 0
    return res


def plot_experiment_residuals_1D(x,
                                 y,
                                 y_fitting,
                                 title='',
                                 xlabel='Angle (deg)',
                                 ylabel='Intensity (V)',
                                 unit='rad'):
    if unit in ('rad', 'Rad', 'Radian', 'radian', 'radians', 'Radians'):
        x = x / degrees
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(x, y, "bo", lw=2, label='Exp. data')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)

    if y_fitting is not None:
        plt.plot(x / degrees, y_fitting, "b-", lw=2, label='Fitting')
        plt.legend()

        residuals = y_fitting - y

        plt.subplot(2, 2, 2)
        plt.plot(x / degrees, residuals / y_fitting.max(), 'r', lw=2)
        plt.title('Normalized residuals')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()

        error = np.linalg.norm(residuals) / (
            np.size(residuals) * np.max(y_fitting))
        print('The normalized MSE is: {:.2f} %.\n'.format(error * 100))


def plot_experiment_residuals_2D(x,
                                 y,
                                 Z,
                                 Z_fitting,
                                 title='',
                                 xlabel='Angle 1 (deg)',
                                 ylabel='Angle 2 (deg)',
                                 unit='rad',
                                 color="magma"):
    # Calculate range
    extension = np.array([x[0, 0], x[-1, -1], y[0, 0], y[-1, -1]]) / degrees
    dif_norm = (Z - Z_fitting) / Z.max()
    # First image, experimental
    plt.figure(figsize=(18, 6))
    plt.subplot(1, 3, 1)
    plt.suptitle(title, fontsize=16)
    IDimage = plt.imshow(
        Z,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title('Experimental')
    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu
    # Second image, model
    plt.subplot(1, 3, 2)
    IDimage = plt.imshow(
        Z_fitting,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title('Model')
    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu
    # Third image, residuals
    plt.subplot(1, 3, 3)
    IDimage = plt.imshow(
        dif_norm,
        interpolation='bilinear',
        aspect='auto',
        origin='lower',
        extent=extension)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title('Normalized residuals')
    plt.axis(extension)
    plt.axis('scaled')
    plt.colorbar()
    IDimage.set_cmap(color)  # YlGnBu  RdBu
    # Print errors
    error = np.linalg.norm(dif_norm) / dif_norm.size
    print('The normalized MSE is: {:.2f} %'.format(error * 100))


def plot_experiment_1D(x,
                       y,
                       title='',
                       xlabel='Angle (deg)',
                       ylabel='Intensity (V)',
                       unit='rad'):
    if unit in ('rad', 'Rad', 'Radian', 'radian', 'radians', 'Radians'):
        x = x / degrees
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(x, y, "bo", lw=2, label='Exp. data')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


###############################
# IMAGES
###############################


def _IMAGES():
    pass


def Load_Image(filename,
               path=None,
               var_name=['image'],
               bw=True,
               no_list=True,
               draw=False):
    """Function that extracts data from a file. It is intended for images or npz files.

    Args:
        filename (str): File name, including extension.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        var_name (tuple or list): If the file is a npz file, the function extracts this variables. Default: ('image').
        bw (bool): If True, converts RGB images to black and white by averaging. Default: True.
        no_list (bool): If True and only one variable is extracted, don't give it as a list. Default: True.
        draw (bool): If True, draws the images.

    Returns:
        data (numpy.ndarray or list of arrays): Array or list of arrays of extracted data.
    """
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)
    extension = filename[-3:]

    if extension == 'npz':
        # NPZ files
        loaded = np.load(filename)
        data = []
        for var in var_name:
            image = loaded[var]
            if bw and isinstance(image, np.ndarray) and image.ndim == 3:
                image = np.mean(image, axis=2)
            data.append(image)

    if draw:
        plt.figure(figsize=(10, 8))
        if bw:
            plt.imshow(image, cmap=cm.gray)
        else:
            plt.imshow(image)
        plt.colorbar()

    if len(data) == 1 and no_list:
        data = data[0]

    # Return to original folder
    if path:
        os.chdir(old_path)

    return data


def Process_Window(window, shape):
    """Function that proccesses a window variable to be easily iterable. Used in functions that probes images for cetain regions.

    Args:
        window (list or tuple): If the method uses a window, this variable specifies it. It must be a 4-element list like this: [row start, column start, row span, column span]. One or more of the elements can be iterables of the same length. In that case, several windows will be used. Default: ('center', 'left', 15, 'full').
            Starts (iterable of int or str): If int, it is the index. Some strings are allowed: 'top', 'bottom', 'center', 'left', 'right', 'start', 'end'.
            Span (int or str): If int, it is the number of rows/collumns. Some strings are also accepted: 'full', 'half'.
        shape (iterable of 2 elements): Shape of the probed image.
    """
    # Look for element lengths
    Len = [0, 0, 0, 0]
    for ind, elem in enumerate(window):
        if isinstance(elem, (list, tuple)):
            Len[ind] = len(elem)
        elif isinstance(elem, np.ndarray):
            Len[ind] = elem.size
        elif isinstance(elem, (int, str)):
            Len[ind] = 0
        else:
            raise ValueError('Element {} of window is not valid'.format(elem))

    # Check if the lengths are correct
    Lmax = Len[0]
    for L in Len:
        if L > 1 and L != Lmax:
            if Lmax <= 1:
                Lmax = L
            else:
                raise ValueError(
                    'Elements of window must be of the same length or length one'
                )
    if Lmax == 0:
        Lmax = 1

    # Create the new window
    new_win = []
    for indE in range(Lmax):
        all_elem = np.zeros(4)
        for ind, L in enumerate(Len):
            # Extract the element
            if L > 1:
                elem = window[ind][indE]
            elif L == 1:
                elem = window[ind][0]
            else:
                elem = window[ind]
            # Process accepted strings
            sign = np.ones(4)
            center = np.zeros(4)
            if isinstance(elem, str):
                if elem.lower() in ('left', 'start', 'bottom'):
                    if ind in (0, 1):
                        elem = 0
                    else:
                        raise ValueError(
                            'Element {} of window is not valid in position {}.'
                            .format(elem, ind))
                elif elem.lower() in ('right', 'end', 'top'):
                    if ind in (0, 1):
                        elem = shape[ind]
                        sign[ind + 2] = -1
                    else:
                        raise ValueError(
                            'Element {} of window is not valid in position {}.'
                            .format(elem, ind))
                elif elem.lower() == 'center':
                    if ind in (0, 1):
                        elem = shape[ind] / 2
                        center[ind] = 1
                    else:
                        raise ValueError(
                            'Element {} of window is not valid in position {}.'
                            .format(elem, ind))
                elif elem.lower() == 'full':
                    if ind in (2, 3):
                        elem = shape[ind - 2]
                    else:
                        raise ValueError(
                            'Element {} of window is not valid in position {}.'
                            .format(elem, ind))
                elif elem.lower() == 'half':
                    if ind in (2, 3):
                        elem = shape[ind - 2] / 2
                    else:
                        raise ValueError(
                            'Element {} of window is not valid in position {}.'
                            .format(elem, ind))
            all_elem[ind] = int(elem)
        # Center and change sign if required
        all_elem = sign * all_elem + center * np.concatenate(
            (all_elem[2:], all_elem[:2]))
        # Don't go over limits
        if all_elem[0] < 0:
            all_elem[0] = 0
        if all_elem[1] < 0:
            all_elem[1] = 0
        if all_elem[2] > shape[0]:
            all_elem[2] = shape[0]
        if all_elem[3] > shape[1]:
            all_elem[3] = shape[1]
        # Get it easy transforming into start end values
        all_elem = np.array(
            [
                all_elem[0], all_elem[0] + all_elem[2], all_elem[1],
                all_elem[1] + all_elem[3]
            ],
            dtype=int)
        # Add element
        new_win.append(all_elem)

    return new_win


def Get_Image(obj,
              image,
              path=None,
              var_name=None,
              kind='phase',
              norm=None,
              bw=True):
    """Function that gets an image from several possible sources and transforms it to a np.ndarray.

    Args:
        image (string, np.array or Scalar_mask_XY): Image to send to the SLM. If string, it is supposed to be a file in an image format (PNG, JPEG, etc.) or a npz file saved with np.savez(). If Scalar_mask_XY, only the amplitude, intensity or phase can be sent to the SLM.
        path (string): If IMAGE is string, this is the path of the image. If None, the image must be located in the current working directory. Default: None.
        var_name (str): If IMAGE is the name of a npz file, var is the name of the variable where the image is stored. If None, the first variable is picked. Default: None.
        kind (string): If IMAGE is Scalar_mask_XY, selects the information sent to the SLM. There are three options: 'phase', 'amplitude' or 'intensity'. Default: 'phase'.
        norm (float): If not None, this value will correspond to the maximum gray level. Default: None.
        bw (bool): If True converts RGB images to black and white. Default: True.

    Returns:
        im (np.ndarray): Array of the image.
    """
    # First option, Image is a file
    if isinstance(image, str):
        # If there is a path, go to it
        if path:
            old_path = os.getcwd()
            os.chdir(path)

        # See if it is a npz file
        if image[-3:] == 'npz':
            file = np.load(image)
            if var_name:
                im = file[var_name]
            else:
                im = file[file.files[0]]
        # Load the image and transform it to np.array
        else:
            im = np.asarray(Image.open(image))

    # Second option, it is a numpy array
    elif isinstance(image, np.ndarray):
        if kind.lower() == 'phase':
            im = np.angle(image) % (2 * np.pi)
        elif kind.lower() == 'amplitude':
            im = np.abs(image)
        elif kind.lower() == 'intensity':
            im = np.abs(image)**2

    # Third option, it is a diffractio Scalar_mask_XY object.
    elif isinstance(image, TYPES_DIFFRACTIO):
        # Check that the units of the mask are the same as of the SLM
        condX = np.abs(obj.dx - image.x[1] + image.x[0]) / obj.dx > EQ_TOLERANCE
        condY = np.abs(obj.dy - image.y[1] + image.y[0]) / obj.dy > EQ_TOLERANCE
        if condX or condY:
            print('WARNING: Mask space and SLM space are not the same.')
        # Take the apropiate variable
        if kind.lower() == 'phase':
            im = np.angle(image.u) % (2 * np.pi)
        elif kind.lower() == 'amplitude':
            im = np.abs(image.u)
        elif kind.lower() == 'intensity':
            im = np.abs(image.u)**2
        else:
            raise ValueError('{} is an incorrect kind of mask.'.format(kind))
        # im = np.transpose(im)

    else:
        raise ValueError("{} is not a valid type for image.".format(
            type(image)))

    # Check that the array has the correct dimensions
    if im.ndim != 2:
        if im.ndim == 3 and im.shape[2] in (3, 4):
            if bw:
                im = Color_to_BW(im)
                print(
                    'WARNING: Image transformed from color to black and white.'
                )
            else:
                im = np.array(im, dtype=float)

        else:
            raise ValueError('Image has an incorrect format.')

    # Normalize
    if norm and isinstance(norm, str):
        norm = np.max(im)
        if norm > 0:
            im = im / np.max(im)
    elif norm and norm != 0:
        im = im / norm

    # Restrict values between 0 and 1
    im[im < 0] = 0
    im[im > 1] = 1

    return im


def Color_to_BW(image):
    """Transforms the np.ndarray of a color image to a black and white one.

    Args:
        image (np.ndarray): Original array.

    Returns:
        image (np.ndarray): Updated array.
    """
    image = np.array(image, dtype=float)
    # print(np.mean(image[:,:,0]), np.max(image[:,:,0]))
    # print(np.mean(image[:,:,1]), np.max(image[:,:,1]))
    # print(np.mean(image[:,:,2]), np.max(image[:,:,2]))
    image = np.mean(image, axis=2)
    # image = image[:,:,1]
    return np.array(image, dtype=float)


def Resize_Image(image, width, height):
    """Resizes an image maintaining its aspect ratio to given width and height.

    Args:
        image (np.ndarray of 2D): Image to be resized.
        width, height (int): Size of the window where the image must fit.

    Returns:
        im (np.ndarray): Resized image.
    """
    # Calculate aspect ratios
    ar_image = image.shape[0] / image.shape[1]
    ar_window = height / width

    # Calculate new sizes
    if ar_window > ar_image:
        # Width is limiting
        height = width * ar_image
    else:
        # Height is limiting
        width = height / ar_image

    # Resize
    return cv2.resize(image, (width, height))


    # # Create space variables
    # x_im = np.arange(image.shape[1])
    # y_im = np.arange(image.shape[0])
    # if ar_window > ar_image:
    #     # Width is limiting
    #     x_win = np.arange(width)
    #     y_win = np.arange(width * ar_image)
    # else:
    #     # Height is limiting
    #     x_win = np.arange(height / ar_image)
    #     y_win = np.arange(height)



############################################
# COMUNICATION
############################################


def _COMUNICATION():
    pass


def List_COM_Ports(verbose=False, close=False):
    """
    Function to check the available COM ports.

    Args:
        verbose (bool): If True, print information of each port. Default: False.
        close (bool): If True, close all ports. Default: False.

    Returns:
        ports (list): List of ports.
    """
    ports = list(list_ports.comports())
    for port in ports:
        if close:
            ser = serial.Serial(port.device)
            ser.close()
        if verbose:
            print(port)
    return ports


def List_USB_Devices(verbose=False):
    """
    Function to check the conected USB devices.

    Args:
        verbose (bool): If True, print information of each device. Default: False.

    Returns:
        ports (usb.core.generator): Iterator of devices.
    """
    devices = usb.core.find(find_all=True)
    if verbose:
        for ind, device in enumerate(devices):
            print(device)
    return devices


##############################################
# OTHER
##############################################


def _OTHER():
    pass


def Tomar_Medidas(daca, spectrometer=None, N=100):
    """Funcion que sirve para tomar medidas angulo-potencia.

    Args:
        N (int): Numero maximo de medidas."""
    angulos = np.zeros(N)
    P = np.zeros(N)

    for ind in range(N):
        ang = input('Angulo (en grados). Fin para terminar.')
        try:
            angulos[ind] = float(ang)
            if spectrometer is not None:    
                P[ind] = spectrometer.Get_Measurement(mode = 'integral', wavelength = None, width = 4, verbose = False)
            else: 
                P[ind] = daca.Get_Signal()
        except:
            if ang.lower() in ('fin', 'end', 'stop'):
                angulos = angulos[:ind]
                P = P[:ind]
                break
            else:
                print('Value {} not accepted'.format(ang))

    return angulos*degrees, P

def Calculate_Space(obj):
    """Calculates the x and y space vectors for a py_lab object.

    Args:
        obj (py_lab): Object to calculate the vectors.

    Returns:
        obj (py_lab): Updated obj.
    """
    # X dimension
    span = (obj.resolution[0] - 1) * obj.pixel_size[0] * obj.M
    obj.x = np.linspace(-span / 2, span / 2, obj.resolution[0]) * um
    obj.dx = obj.x[1] - obj.x[0]
    # Y dimension
    span = (obj.resolution[1] - 1) * obj.pixel_size[1] * obj.M
    obj.y = np.linspace(-span / 2, span / 2, obj.resolution[1]) * um
    obj.dy = obj.y[1] - obj.y[0]

    obj.extent = (obj.x[0], obj.x[-1], obj.y[0], obj.y[-1])

    return obj


def vectorize(x):
    """Function to make x a np.array if it is not.

    Args:
        x (np.ndarray, int, float or bool): Input.

    Returns:
        x (np.ndarray): Result.
    """
    if isinstance(x, (int, float, bool)):
        x = np.array([x])
    return x


def devectorize(x):
    """Function to make a number a np.ndarray if it only has 1 element

    Args:
        x (np.ndarray): Input.

    Returns:
        x (np.ndarray, int, float or bool): Result.
    """
    if x.size == 1:
        return x[0]
    else:
        return x


def select_movement(pos, motor, obj, units, absolute=True, origin=True):
    """Function to calculate a good position array to move.

    Args:
        pos (float or np.ndarray): Input position.
        motor (None, int or iterable): Only if pos is float. If None, all motors are moved. If int or iterable, only the specified motors are moved.
        obj (object): Motor object.
        units (str): Position units to choose between 'rad' and 'deg'. Default: DEFAULT_POS_UNITS.
        absolute (bool): If True, the movement is absolute. If not, it is relative. Default: True.
        origin (bool): If True, the absolute position is calculated respect to the stored origin. Default: False.

    Returns:
        pos (np.ndarray): Result position.
    """
    if isinstance(pos, number_types):
        if motor is None:
            pos_final = np.array([pos] * obj.N)
        else:
            if absolute:
                final_pos = obj.Get_Position(units=units)
            else:
                pos_final = np.zeros()
            if isinstance(motor, int):
                motor = [motor]
            for ind, mot in enumerate(motor):
                if mot >= obj.N:
                    raise ValueError(
                        "Specified motor index {} exceds the number of motors {}"
                        .format(motor, obj.N))
                else:
                    pos_final[ind] = pos
    else:
        if len(pos) == obj.N:
            pos_final = pos
        elif len(pos) > obj.N:
            pos_final = pos[:obj.N]
        else:
            raise ValueError(
                "Position array of length {} is too short for {} motors".
                format(len(pos), obj.N))

    return pos_final


def Get_Diffractio_Space(obj):
    """Calculates diffractio x and y variables for diffractio.

    Returns:
        x, y, wavelength (np.ndarray): Arrays of space.
    """
    lim = obj.M * obj.pixel_size[0] * um * (obj.resolution[0] - 1) / 2
    x = np.linspace(-lim, lim, obj.resolution[0])
    lim = obj.M * obj.pixel_size[1] * um * (obj.resolution[1] - 1) / 2
    y = np.linspace(-lim, lim, obj.resolution[1])
    return x, y, obj.wavelength


def nearest(vector, numero):
    """calcula la posicion de numero en el vector
        actualmente esta implementado para 1-D, pero habria que hacerlo para nD
        inputs:
        *vector es un array donde estan los points
        *numero es un dato de los points que se quieren compute

        outputs:
        *imenor - orden del elemento
        *value  - value del elemento
        *distance - diferencia entre el value elegido y el incluido
    """
    indexes = np.abs(vector - numero).argmin()
    values = vector.flat[indexes]
    distances = values - numero
    return indexes, values, distances


def to_bits(variable_integer, num_bits=16):
    """
    takes an integer an generates a list with bits

    Args:
        variable_integer (int): integer with data
        num_bits (int): num of output bits: 8,16, 32, 64
    """
    if num_bits == 8:
        output = map(int, [x for x in '{:08b}'.format(variable_integer)])
    elif num_bits == 16:
        output = map(int, [x for x in '{:016b}'.format(variable_integer)])
    elif num_bits == 32:
        output = map(int, [x for x in '{:032b}'.format(variable_integer)])
    elif num_bits == 64:
        output = map(int, [x for x in '{:064b}'.format(variable_integer)])

    print(output)


def transform_axis_variable(axis, N):
    """Function that transforms the axis variable depending on the number of motors.

    Args:
        axis (None, int or list of ints): Axes to move. If None, all axis are moved.
        N (int): Required size.

    Returns:
        axis (list): Transformed variable.
    """
    if isinstance(axis, int):
        axis = [axis]
    elif isinstance(axis, (list, tuple)):
        pass
    else:
        axis = range(N)

    return axis


def adapt_to_multiple(N, var, axis=None):
    """Function that takes several variables in a list and adapts it to be used in multiple elements classes.

    Args:
        N (int): Required size.
        var (tuple or list): List of variables.
        axis (None, int or list of ints): Axes to move. If None, all axis are moved. Default: None.

    Returns:
        var (list or variable): Transformed variables. If var was only one variable, return it. If not, return a list.
    """
    all_numbers = True
    return_single = False

    axis = transform_axis_variable(axis, N)

    if not isinstance(var, (list, tuple)):
        adressed_var = [var]
        return_single = True
    else:
        adressed_var = deepcopy(var)
    return_single = len(adressed_var) == 1

    new_var = []
    for ind, v in enumerate(adressed_var):
        if isinstance(v, (int, float, complex, str, type(None))):
            v = [v] * N
            if isinstance(v, (int, float, complex)):
                v = np.array(v)
            else:
                all_numbers = False
        new_var.append(v)

    if all_numbers:
        new_var = np.array(new_var)

    if return_single:
        return new_var[0]
    else:
        return new_var


def select_pos(pos1, pos2, pos3=None, units=None, axis=None):
    """Function to select between positions.

    Args:
        pos1 (np.ndarray): Preference position.
        pos2 (np.ndarray): Default position.
        pos3 (np.ndarray or None): If not None, position to use when not in the correct axis. Default: None.
        units (None or list): List of units for pos1. Default None.
        axis (None or list of ints): If not None, list of the only axes which will be selected. The rest will be None. Default: None.

    Returns:
        pos (np.ndarray): Final position. If axis is not None, positions of different axes will be set to np.nan.
    """
    if isinstance(axis, int):
        axis = [axis]
    result = np.zeros(len(pos1))
    for ind, p in enumerate(pos1):
        if not axis or not ind in axis:
            if p is None or p is np.nan:
                result[ind] = pos2[ind]
            else:
                if units:
                    p *= UNITS_POS[units[ind]]
                result[ind] = p
        else:
            if pos3 is not None:
                result[ind] = pos3[ind]
            else:
                result[ind] = np.nan

    return result


def percentage_complete(current=None, total=None):
    """Función que calcula e imprime el porcentaje de completitud de una medición actual."""
    if (current is None) or (total is None):
        #print('Completado:\t', end='')
        print('Completado:{:3d} %'.format(0), end='\r')
    else:
        # Adaptar para listas si es necesario
        try:
            Ncurrent = np.ravel_multi_index(current, total) + 1
            Ncurrent = Ncurrent / np.prod(total)
        except:
            Ncurrent = (current) / total
        
        # Calcular porcentaje
        Ncurrent = int(np.floor(Ncurrent * 100))
        
        # Imprimir
        print('Completado:{:3d} %'.format(Ncurrent), end='\r')
        
        if Ncurrent == 100:
            print('')


def plot_experiment_residuals_1D(x,
                                 y,
                                 y_fitting,
                                 title='',
                                 xlabel='Angle (deg)',
                                 ylabel='Intensity (V)',
                                 unit='rad'):
    if unit in ('rad', 'Rad', 'Radian', 'radian', 'radians', 'Radians'):
        x = x / degrees
    plt.figure(figsize=(16, 8))
    plt.subplot(2, 2, 1)
    plt.plot(x, y, "bo", lw=2, label='Exp. data')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)

    if y_fitting is not None:
        plt.plot(x, y_fitting, "b-", lw=2, label='Fitting')
        plt.legend()

        residuals = y_fitting - y

        plt.subplot(2, 2, 2)
        plt.plot(x, residuals / y_fitting.max(), 'r', lw=2)
        plt.title('Normalized residuals')
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()

        error = np.linalg.norm(residuals) / (
            np.size(residuals) * np.max(y_fitting))
        print('The normalized MSE is: {:.2f} %.\n'.format(error * 100))


def len_all(x):
    """Calculates length of iterables. If it is not iterable, returns 1 instead."""
    # None has length 0
    if x is None:
        return 0

    # If not, calculate
    try:
        length = len(x)
    except:
        try:
            length = x.size
        except:
            length = 1

    return length


def fit_errors(res, int_error=1):
    """Function to calculate the error in a fit performed using scipy.optimize.least_squares."""
    jac = res.jac / int_error
    U, s, Vh = np.linalg.svd(jac, full_matrices=False)
    tol = np.finfo(float).eps * s[0] * max(jac.shape)
    w = s > tol
    cov = (Vh[w].T / s[w]**2) @ Vh[w]  # robust covariance matrix
    perr = np.sqrt(np.diag(cov))  # 1sigma uncertainty on fitted parameters
    return perr

#########################################
## ANGLES FOR POLARIMETRY
#########################################

def _ANGLES_FOR_POLARIMETRY():
    pass

# Constantes
phi = (1+np.sqrt(5))/2
r3 = np.sqrt(3)
raiz = np.sqrt(1 + phi**2)
Tetraedro = np.array([[1, 1, 0, 0], [1, -1/3, np.sqrt(8)/3, 0], [1, -1/3, -np.sqrt(2)/3, np.sqrt(6)/3], [1, -1/3, -np.sqrt(2)/3, -np.sqrt(6)/3]]).T
Octaedro = np.array([[1, 1, 0, 0], [1, -1, 0, 0], [1, 0, 1, 0], [1, 0, -1, 0], [1, 0, 0, 1], [1, 0, 0, -1] ]).T
Cubo = np.array([[r3, 1, 1, 1], [r3, 1, 1, -1], [r3, 1, -1, 1], [r3, -1, 1, 1], [r3, -1, -1, 1], [r3, -1, 1, -1], [r3, 1, -1, -1], [r3, -1, -1, -1]]) / r3
Icosaedro = np.array([[raiz, 0, 1, phi], [raiz, 0, -1, phi], [raiz, 0, 1, -phi], [raiz, 0, -1, -phi], 
    [raiz, 1, phi, 0], [raiz, -1, phi, 0], [raiz, 1, -phi, 0], [raiz, -1, -phi, 0],
    [raiz, phi, 0, 1], [raiz, -phi, 0, 1], [raiz, phi, 0, -1], [raiz, -phi, 0, -1],
]).T / raiz
Dodecaedro = np.array([[r3, 1, 1, 1], [r3, 1, 1, -1], [r3, 1, -1, 1], [r3, -1, 1, 1], [r3, -1, -1, 1], [r3, -1, 1, -1], [r3, 1, -1, -1], [r3, -1, -1, -1], 
    [r3, 0, phi, 1/phi], [r3, 0, -phi, 1/phi], [r3, 0, phi, -1/phi], [r3, 0, -phi, -1/phi], 
    [r3, 1/phi, 0, phi], [r3, -1/phi, 0, phi], [r3, 1/phi, 0, -phi], [r3, -1/phi, 0, -phi], 
    [r3, phi, 1/phi, 0], [r3, -phi, 1/phi, 0], [r3, phi, -1/phi, 0], [r3, -phi, -1/phi, 0], ]).T / r3

S_4 = Stokes().from_matrix(Tetraedro)#.analysis.filter_physical_conditions(tol=0)
S_6 = Stokes().from_matrix(Octaedro)#.analysis.filter_physical_conditions(tol=0)
S_8 = Stokes().from_matrix(Cubo)#.analysis.filter_physical_conditions(tol=0)
S_12 = Stokes().from_matrix(Icosaedro)#.analysis.filter_physical_conditions(tol=0)
S_20 = Stokes().from_matrix(Dodecaedro)#.analysis.filter_physical_conditions(tol=0)

def calculate_polarimetry_angles(Nmeasures, type="perfect", rotation = None):
    """Calculates the angles for a Mueller matrix polarimetry experiment.

    Args:
        Nmeasures (int): Number of measurements.
        type (str, optional): Type of algorithm to calculate the angles. Valid options are RANDOM, LINSPACE, PERFECT or SPIRAL. Defaults to "perfect".
        rotation (2x3 np.ndarray): Measurement rotation angles. Default: None

    Returns:
        np.ndarray: Nx4 array of angles.
    """
    type = type.lower()
    if type == "random":
        angles = np.random.rand(Nmeasures, 4) * 180*degrees

    elif type == "linspace":
        # Make the 4D grid
        Nx = np.ceil(Nmeasures**0.25)
        Nmeasures = Nx**4
        angles_x = np.linspace(0, 180*degrees, Nx)
        # Assign angles
        angles = np.zeros((Nmeasures, 4))
        ind = 0
        for a1 in angles_x:
            for a2 in angles_x:
                for a3 in angles_x:
                    for a4 in angles_x:
                        angles[ind, :] = [a1, a2, a3, a4]

    elif type == "perfect":
        S_PSG, S_PSA = find_perfect_combination(Nmeasures)
        angles = states_2_angles(S_PSG, S_PSA)
        
        

    elif type == "spiral":
        if isinstance(Nmeasures, (list, tuple)):
            _, _, _, S_spiral_1 = fill_sphere_fibonacci(num_samples=Nmeasures[0], kind_exit='Stokes', has_draw=False) 
            _, _, _, S_spiral_2 = fill_sphere_fibonacci(num_samples=Nmeasures[1], kind_exit='Stokes', has_draw=False) 
            angles = states_2_angles(S_spiral_1, S_spiral_2)       
        else:
            _, _, _, S_spiral = fill_sphere_fibonacci(num_samples=np.floor(Nmeasures**0.5), kind_exit='Stokes', has_draw=False)
            angles = states_2_angles(S_spiral, S_spiral)


    if rotation is not None: 
        if len(rotation)>1:
            angles_new = np.asarray(angles)
            PSG_az,PSG_el = PSG_angles_2_states(angles[:,0],angles[:,1])
            PSA_az,PSA_el = PSA_angles_2_states(angles[:,3],angles[:,2])
            PSG_az_new,PSG_el_new = Rotation_Poincare(PSG_az,PSG_el,rotation[0])
            PSA_az_new,PSA_el_new = Rotation_Poincare(PSA_az,PSA_el,rotation[1])
            angles_new[:,0],angles_new[:,1] = PSG_states_2_angles(PSG_az_new,PSG_el_new)
            angles_new[:,3],angles_new[:,2] = PSA_states_2_angles(PSA_az_new,PSA_el_new)
        else:
            print("Add a correct 2x3 array: [rot_x, rot_y, rot_z]")
        angles = angles_new

    return angles

def find_perfect_combination(Nmeasures):
    """Finds the nearest combination of states for angles distributed percectly upon the Poincare sphere.

    Args:
        Nmeasures (int): Number of measurements.

    Returns:
        Stokes: States of the PSG.
        Stokes: States of the PSA.
    """
    # Safety
    if Nmeasures <= 16:
        return S_4, S_4

    # Normal, list
    if isinstance(Nmeasures, (list, tuple)):
        N = [4, 6, 8, 12, 20]
        states = [S_4, S_6, S_8, S_12, S_20]
        S_PSG = states[N.index(Nmeasures[0])]           
        S_PSA = states[N.index(Nmeasures[1])]
    else: 
        N = np.array([4, 6, 8, 12, 20])
        states = [S_4, S_6, S_8, S_12, S_20]
        Nfinal = 0
        for indG, Ng in enumerate(N):
            for indA, Na in enumerate(N):
                Ncurrent = Ng * Na
                if Ncurrent > Nfinal and Ncurrent <= Nmeasures:
                    S_PSG = states[indG]
                    S_PSA = states[indA]
                    Nfinal = Ncurrent

    return S_PSG, S_PSA

def states_2_angles(S_PSG, S_PSA):
    """Function to calculate the angles of elements of PSG and PSA from their states

    Args:
        S_PSG (Stokes): States of the PSG.
        S_PSA (Stokes): States of the PSA.

    Returns:
        np.ndarray: Nx4 array of angles.
    """    
    # Array of angles
    az, el = S_PSG.parameters.azimuth_ellipticity(use_nan=False)
    angles_P1, angles_Q1 = PSG_states_2_angles(azimuth=az, ellipticity=el)
    az, el = S_PSA.parameters.azimuth_ellipticity(use_nan=False)
    angles_P2, angles_Q2 = PSA_states_2_angles(azimuth=az, ellipticity=el)
    # Multiplexate
    Ng, Na = S_PSG.size, S_PSA.size
    N = Ng * Na
    angles_P1_def = np.zeros(N)
    angles_Q1_def = np.zeros(N)
    angles_Q2_def = np.zeros(N)
    angles_P2_def = np.zeros(N)
    for indG in range(Ng):
        for indA in range(Na):
            ind = indG * Na + indA
            angles_P1_def[ind] = angles_P1[indG]
            angles_Q1_def[ind] = angles_Q1[indG]
            angles_Q2_def[ind] = angles_Q2[indA]
            angles_P2_def[ind] = angles_P2[indA]
    # Combine
    angles = np.array([angles_P1_def, angles_Q1_def, angles_Q2_def, angles_P2_def]).T
    return angles

def fill_sphere_fibonacci(num_samples=100, kind_exit='list', has_draw=False):
    """Generate a quasi - uniform distribution around the poincare sphere.

    Arguments:
        num_samples(int): number of samples.
        kind_exit(str): ('list', 'numpy_array', 'Stokes', 'Jones')

    Reference:
         https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
    """
    golden_angle = np.pi * (3. - np.sqrt(5.))  # golden angle in radians

    i = np.linspace(0, num_samples, num_samples)
    y = 1 - (i / (num_samples - 1)) * 2
    y[y > 1] = 1
    y[y < -1] = -1
    radius = np.sqrt(1 - y**2)  # radius at y
    theta = golden_angle * i  # golden angle increment
    x = np.cos(theta) * radius
    z = np.sin(theta) * radius

    if kind_exit == 'list':
        return (x, y, z)
    elif kind_exit == 'numpy_array':
        return np.vstack((x, y, z)).transpose()
    elif kind_exit == 'Stokes':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        return x, y, z, S
    elif kind_exit == 'Jones':
        S = Stokes("S1").from_components([np.ones_like(x), x, y, z])
        J = Jones_vector().from_Stokes(S)
        return x, y, z, J
    else:
        print("No kind_exit parameter")
        return None

def sort_positions(M, pos_ini):
    """
    Luis Miguel Sánchez Brea
    10/09/2018
    Pequeña utilidad para ordenar las posiciones aleatorias de los 4 motores de forma que se reduzca el tiempo de adquisición.

    Tenemos una matriz de tamaño $N*m$ donde N es el número de medidas y m el número de motores. Estas posiciones las hemos determinado de forma aleatoria (u otro procedimiento). Queremos ordenar la matriz de forma que el desplazamiento de los motores sea el mínimo posible

    Con los resultados generamos una función donde entra una Matriz Matriz1 y sale otra Matriz2, pero ordenada
    """
    num_medidas, num_motores = M.shape

    M2 = np.zeros_like(M)
    pos_current = pos_ini

    for i in range(num_medidas):
        # medida de distancia como suma de distancias
        # distance=np.sum(np.abs(M-pos_current),axis=1)
        # medida de distancia como distancia maxima
        distance = np.max(np.abs(M - pos_current), axis=1)

        i_min = distance.argmin()

        M2[i, :] = M[i_min, :]
        pos_current = M[i_min, :]
        M = np.delete(M, [i_min], axis=0)

    return M2

def Rotation_Poincare(az, el, rotation):

    S = Stokes().general_azimuth_ellipticity(az,el)
    Sf = S.rotate_Poincare(rotation[0],rotation[1],rotation[2])
    new_az, new_el = Sf.parameters.azimuth_ellipticity()

    return new_az, new_el



