"""This file contains functions for diffractometry.

It is used in the SLM-motor-laser-camera set-up"""


import os
import numpy as np
import imageio
from pprint import pprint
import screeninfo
import serial.tools.list_ports
import serial
import cv2
import time

import matplotlib.cm as cm
import matplotlib

from diffractio import degrees, mm, plt, sp, um, np
from diffractio.scalar_sources_XY import Scalar_source_XY
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_masks_XY import Scalar_mask_XY

from diffractio.scalar_fields_XYZ import Scalar_field_XYZ
from diffractio.utils_common import get_date
from diffractio.utils_drawing import draw_several_fields


from py_lab.camera import Camera
from py_lab.motor import Motor
from py_lab.slm import SLM
from py_lab.utils import List_COM_Ports


# Camara
ms = 1.
seconds = 1000 * ms

# Motores
s = 1


class Diffractometer(object):
    """Class for experiment of SLM modulator in AOCG 1 - HoloEye2500

    Parameters:
        motor (class): motor lineal
        slm (class): SLM Holoeye
        camera (class): camera

    Attributes:
        self.wavelength (float): Wavelength of the incident field.
        self.date (str): Date when performed.
        self.info (str): Information about the experiment
        self.z0_image (float): position of motor when image after 4f-system
        self.magnification (float): magnification of the 4f-system
    """

    def __init__(self, slm=None, motor=None, camera=None, z0_image=None, magnification=1, wavelength=0.6328 * um, info=""):
        self.slm = slm
        self.motor = motor
        self.camera = camera
        self.z0_image = z0_image
        self.magnification = magnification
        self.wavelength = wavelength
        self.port_motor = 'COM3'
        self.camera_gain = 2
        self.camera_integration_time = -10
        self.camera_resolution = "Y800 (1024x768)"
        self.date = get_date()

    def __str__(self):
        """Represents main data of the atributes."""

        print("to do.")

        return -1

    def __del__(self):
        self.close()

    def __exit__(self):
        self.close()

    def close(self, verbose=False):
        """Close the devices : Camera, SLM and motors"""
        self.slm.Close_All_Images()
        self.motor.Close()
        self.camera.Stop_Live()

        del self.slm
        del self.motor
        del self.camera

        self.slm = None
        self.motor = None
        self.camera = None

        if verbose is True:
            print("Devices closed.")

        return 0

    def load_devices(self, has_camera=True, has_slm=True, has_motor=True):
        """Load camera, slm and motors"""

        if has_camera:
            cam = Camera(name="ImagingSource")
            devices = cam.List_Devices(verbose=True)
            cam.Open()
            cam.Set_Property("Resolution", "Y800 (1024x768)")

            cam.Set_Property("Resolution", self.camera_resolution)
            cam.Set_Property("FrameRate", 5)
            cam.Set_Property("Gain", 0, is_switcher=True)
            cam.Set_Property("Gain", self.camera_gain)
            # Aquí podemos variar el tiempo de exposición de la cámara.
            #value = int(np.log2(1 / 2000.))
            cam.Set_Property("Exposure", self.camera_integration_time)
            cam.Start_Live()

        if has_motor:
            motor = Motor(name='SMC100')
            motor.Open(port=self.port_motor)
            motor.Test_Connection()
            motor.Home()

        if has_slm:
            slm = SLM(name="HoloEye2500", M=self.magnification)

        self.camera = cam
        self.slm = slm
        self.motor = motor

    def compute_integration_time(self):
        """Determines automatically which is the best integration time to have high levels between 128 and 255, with no saturation

        Procedure:
            - We sent a two_levels and check that black is black and white is white.

        Returns:
            - Best integration time

            - Average high level and low-level
            - an image with the best integhration time
        """
        print(" to do")
        return -1

    def compute_z0_motor_1loop(self,  positions, grado, remove_background=True, has_draw=True, verbose=False):
        """pass an chess image and get the position of geometrical image"""

        #self.z0_image = -1000000
        slm = self.slm
        cam = self.camera
        motor = self.motor

        stdsol = np.zeros_like(positions)
        mask = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)
        mask.grating_2D_chess(period=1000 * um, amin=0, amax=1.,
                              phase=0, r0=[0, 0], fill_factor=0.5, angle=0.0 * degrees)

        mask_null = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)
        mask_null.one_level(0)

        for i, z in enumerate(positions):
            motor.Move_Absolute(
                pos=positions[i], units='mm', verbose=False, move_time=None)
            std_max = 0
            if remove_background:
                slm.Send_Image(mask_null, norm=1, kind='intensity')
                time.sleep(0.2)
                imageb = cam.Get_Image()
                slm.Send_Image(mask, norm=1, kind='intensity')
                time.sleep(0.2)
                imagem = cam.Get_Image()
                image = imagem - imageb
                image[image < 0] = 0
            else:
                image = cam.Get_Image()

            std_new = np.std(image)

            if std_new > std_max:
                std_max = std_new
                image_best = image
            stdsol[i] = std_new
            print(z, end='\r')

        fajuste = np.polyfit(positions, stdsol, grado)
        y2 = np.poly1d(fajuste)
        x2 = np.linspace(positions[0], positions[-1], 100)
        max_ajuste = x2[y2(x2).argmax()]
        max_array = positions[stdsol.argmax()]

        if has_draw is True:
            plt.figure()
            plt.imshow(image_best)

            stdfig = plt.figure()
            ax = stdfig.subplots()
            ax.plot(positions, stdsol, '.', label='Datos')
            ax.plot(
                x2, y2(x2), label='Ajuste, máximo = {:.3f} mm'.format(max_ajuste))
            plt.xlabel('Posición de la cámara (mm)')
            plt.ylabel('STD')
            plt.title('STD en función de z. Obtención de z=0')
            plt.legend()
            plt.show()

        self.z0_image = max_ajuste

        if verbose is True:
            print("The position of z0 at geometrical image is {:2.4f} mm".format(
                self.z0_image))

            # print(max_array)

        return self.z0_image, max_array, std_max, image_best

    def compute_z0_motor_loops(self, z_inicial, Deltas, num_data, grados_polinomio, remove_background=True, has_draw=True, verbose=False):
        """
        This function do several loops in order to get the best position of geometrical image
        """
        (slm, cam, motor) = self.slm, self.camera, self.motor
        std_absolute = 0
        z_best = 0
        image_final = cam.Get_Image()
        for i, Dz in enumerate(Deltas):
            positions = np.linspace(
                z_inicial - Dz / 2, z_inicial + Dz / 2, num_data[i])
            max_ajuste, max_array, std_max,  image_best = self.compute_z0_motor_1loop(
                positions, grado=grados_polinomio[i], remove_background=remove_background, has_draw=has_draw, verbose=verbose)
            if std_max > std_absolute:
                std_absolute = std_max
                z_best = max_array
                image_final = image_best
            z_inicial = max_array
        z_final = z_best
        motor.Move_Absolute(pos=z_final, units='mm',
                            verbose=False, move_time=None)

        self.z0_image = z_final

        return z_final, image_final

    def compute_magnification(self, verbose=False):
        """determine the magnification of the 4f-system.
        We load a slit, perform the image and measure the position of edges.
        Warning: We need to be at z0_image since it is computed with the geometrical image.

            Returns:
                (float): Magnification, it is f1/f2 but it changes as not always lenses are well placed.
        """
        if verbose is True:
            print("The magnification is {:2.4f} mm".format(self.magnification))

        return self.magnification

    def callibrate_phase(self):
        """The phase of the SLM with the polarizers placed is computed.
        We use a phase_fresnel lens and compute the intensity at focus.


        Reference: "paper"
        """
        return -1

    def images_z(mask, positions, filename, remove_background=True, has_diffractio=False,  has_video=True, has_images=False, has_npz=False, show_images=False, fps=1):
        """Function to perform the measurements to calculate the global phase of the SLM.
        - Generates a gif video.
        - Generates npz files with the images.
        - generates functions

        Args:

            mask (slm): mask to execeute
            positions (np.array): positions in mm from the z0 image
            filename (str): Template for filenames.
            remove_background (bool): If True, removes_background
            has_diffractio (bool): If True returns scalar_fields_XYZ structure with image
            has_video (bool): If True, makes a gif file
            has_images (bool): If True, keeps png images for the gif file.
            has_npz (bool): if True, generates npz files of images

        Returns (only if return_vars is True):
            images (list): List of arrays of the images.
        """
        slm, cam, motor = self.slm, self.camera, self.motor
        z0 = self.z0_image

        mask_null = Scalar_mask_XY(
            x=mask.x, y=mask.y, wavelength=mask.wavelength)
        mask_null.u = np.zeros_like(mask.u, dtype=float)

        if has_diffractio:
            u_field = Scalar_field_XYZ(
                x=mask.x, y=mask.y, z=positions, wavelength=mask.wavelength)

        names = []

        for i, z in enumerate(positions):
            motor.Move_Absolute(pos=z0 - z, units='mm',
                                verbose=False, move_time=None)

            if remove_background:
                slm.Send_Image(mask_null, norm=1, kind='intensity')
                time.sleep(0.3)
                imageb = cam.Get_Image()
                slm.Send_Image(mask, norm=1, kind='intensity')
                time.sleep(0.3)
                imagem = cam.Get_Image()
                image = imagem - imageb
                image[image < 0] = 0
            else:
                image = cam.Get_Image()

            if has_diffractio:
                u_field.u[:, :, i] = np.sqrt(image.transpose())

            if np.sign(z) >= 0:
                filename_npz = '{}_{:.1f}.npz'.format(filename, abs(z))
                names.append(filename_npz)
                np.savez(filename_npz)
                plt.imshow(image, origin='lower')
                plt.axis('off')
                plt.title('{} z = {:.1f} mm'.format(filename, z))
                plt.savefig('{}_{:.1f}.png'.format(filename, abs(z)))
                if show_images:
                    plt.show()

            print('{:.1f}'.format(z), end='\r')

        if has_video == True:
            gif_path = "{}.gif".format(filename)
            with imageio.get_writer(gif_path, mode='I', fps=fps) as writer:
                for i, z in enumerate(positions):
                    if np.sign(z) >= 0:
                        writer.append_data(imageio.imread(
                            '{}_{:.1f}.png'.format(filename, abs(z))))

        if has_images is False:
            for i, z in enumerate(positions):
                if np.sign(z) >= 0:
                    os.remove('{}_{:.1f}.png'.format(filename, abs(z)))

        if has_npz is False:
            for i, z in enumerate(positions):
                if np.sign(z) >= 0:
                    os.remove('{}_{:.1f}.npz'.format(filename, abs(z)))

        print('¡Finished!')

        if has_diffractio:
            return names, u_field
        else:
            return names, None


def images_z(devices, mask, positions, z0, filename, remove_background=True, has_diffractio=False,  has_video=True, has_images=False, has_npz=False, show_images=False, fps=1):
    """Function to perform the measurements to calculate the global phase of the SLM.
    - Generates a gif video.
    - Generates npz files with the images.
    - generates functions

    Args:
        devices=(slm, cam,motor)
            slm (SLM): SLM.
            cam (Camera): Object of the camera.
            motor (Motor): Object of the motor
        mask (slm): mask to execeute
        positions (np.array): positions in mm from the z0 image
        z0 (float): motor position of the image after 4f.
        filename (str): Template for filenames.
        remove_background (bool): If True, removes_background
        has_diffractio (bool): If True returns scalar_fields_XYZ structure with image
        has_video (bool): If True, makes a gif file
        has_images (bool): If True, keeps png images for the gif file.
        has_npz (bool): if True, generates npz files of images

    Returns (only if return_vars is True):
        images (list): List of arrays of the images.
    """
    slm, cam, motor = devices

    mask_null = Scalar_mask_XY(x=mask.x, y=mask.y, wavelength=mask.wavelength)
    mask_null.u = np.zeros_like(mask.u, dtype=float)

    if has_diffractio:
        u_field = Scalar_field_XYZ(
            x=mask.x, y=mask.y, z=positions, wavelength=mask.wavelength)

    names = []

    for i, z in enumerate(positions):
        motor.Move_Absolute(pos=z0 - z, units='mm',
                            verbose=False, move_time=None)

        if remove_background:
            slm.Send_Image(mask_null, norm=1, kind='intensity')
            time.sleep(0.3)
            imageb = cam.Get_Image()
            slm.Send_Image(mask, norm=1, kind='intensity')
            time.sleep(0.3)
            imagem = cam.Get_Image()
            image = imagem - imageb
            image[image < 0] = 0
        else:
            image = cam.Get_Image()

        if has_diffractio:
            u_field.u[:, :, i] = np.sqrt(image.transpose())

        if np.sign(z) >= 0:
            filename_npz = '{}_{:.1f}.npz'.format(filename, abs(z))
            names.append(filename_npz)
            np.savez(filename_npz)
            plt.imshow(image, origin='lower')
            plt.axis('off')
            plt.title('{} z = {:.1f} mm'.format(filename, z))
            plt.savefig('{}_{:.1f}.png'.format(filename, abs(z)))
            if show_images:
                plt.show()

        print('{:.1f}'.format(z), end='\r')

    if has_video == True:
        gif_path = "{}.gif".format(filename)
        with imageio.get_writer(gif_path, mode='I', fps=fps) as writer:
            for i, z in enumerate(positions):
                if np.sign(z) >= 0:
                    writer.append_data(imageio.imread(
                        '{}_{:.1f}.png'.format(filename, abs(z))))

    if has_images is False:
        for i, z in enumerate(positions):
            if np.sign(z) >= 0:
                os.remove('{}_{:.1f}.png'.format(filename, abs(z)))

    if has_npz is False:
        for i, z in enumerate(positions):
            if np.sign(z) >= 0:
                os.remove('{}_{:.1f}.npz'.format(filename, abs(z)))

    print('¡Finished!')

    if has_diffractio:
        return names, u_field
    else:
        return names, None
