### SPECTRAL POLARIMETER ###

# Made by: Joaquín Andrés Porras
# Date: 01/09/2024
# Last modification: 01/09/2024
# Version: 1.0.0

# Description: This file contains the functions needed to calibrate and use the spectral polarimeter. 
# Classes: 
#   - SpectralPolarimeterCalibration
#   - SpectralPolarimeter
#   - SpectralPolarimeter_utils

############################################################################################################################################################################
# IMPORTS
# GENERAL MODULES

import numpy as np                                          # Numpy
import os                                                   # OS
import datetime                                             # Datetime
import time                                                 # Time

# CALCULUS MODULES

import numpy as np                                          # Numpy
import matplotlib.pyplot as plt                             # Matplotlib
# %matplotlib widget                                          # Widget
from scipy.optimize import least_squares                    # Least squares
from pyswarms.single.global_best import GlobalBestPSO       # PSO

# PY-LAB MODULES

import py_lab.utils as utils                                # Utils py-lab
from py_lab.motor import Motor_Multiple
from py_lab.daca import DACA
from py_lab.spectrometer import Spectrometer
from py_lab.config import degrees, CONF_DT_50, CONF_U6, CONF_POLARIMETER, CONF_INTEL, CONF_T7, CONF_ZABER, CONF_AVANTES
from py_lab.utils import Rotation_Poincare, PSG_angles_2_states, PSA_angles_2_states, PSG_states_2_angles, PSA_states_2_angles

# MÓDULOS PY-POL

from py_pol.mueller import Mueller                          # Mueller
from py_pol.stokes import Stokes                            # Stokes

############################################################################################################################################################################

# SPECTRAL POLARIMETER CALIBRATION CLASS

class SpectralPolarimeterCalibration(object):
    
    """
    Class for calibrating the spectral polarimeter. 
    It contains the following functions:
        - Load_Cal_Files
        - Print_Calibration_Parameters
        - Save_Cal_Files
        - make_step
        - model_step
        - error_step
        - analyze_step
        - Process_Iterative_Calibration
        - Process_Simultaneous_Calibration
    """
    
    def Load_Cal_Files(folder=None, filename=None, single = False, wavelengths = None): 
        """Function to generate the initial cal_dict, except calibration files are loaded.
        
        Args:
            folder (string): Folder where the calibration files are located. Default: None.
            filename (string): Name of the calibration files. Default: None.
            single (bool): If True, general parameters is loaded. Default: False.
            wavelengths (np.ndarray): Array with the wavelengths. Default: None.

        Returns:
            cal_dict (dict): Dictionary with the calibration configuration.
        """
        
        # Open the folder
        if folder is not None:
            old_folder = os.getcwd()
            os.chdir(folder)
        
        # Initialize the dictionary
        cal_dict = {}

        # Load the wavelengths
        if single:
            if type(wavelengths) in (int, float):
                cal_dict['num_wavelengths'] = np.ones(1)
                cal_dict['wavelengths'] = np.array([wavelengths])
            elif wavelengths is None:
                cal_dict['num_wavelengths'] = np.ones(1)
                cal_dict['wavelengths'] = wavelengths
            else: 
                print("Wavelengths must be a number when single is True")
                return
        else:  
            if wavelengths is None: 
                cal_dict['num_wavelengths'] = np.ones(3648)
                cal_dict['wavelengths'] = np.linspace(400, 700, 3648)
            else: 
                cal_dict['num_wavelengths'] = np.ones_like(wavelengths)
                cal_dict['wavelengths'] = wavelengths

        # Create the dictionary if the filename is None
        if filename is None:
            cal_dict['Twait'] = 0.2
            cal_dict["S0"] = 1 * cal_dict['num_wavelengths']
            cal_dict["S0_error"] = 0 * cal_dict['num_wavelengths']
            cal_dict["N_measures"] = 91
            cal_dict["max_angle"] = 360
            cal_dict["angles"] = np.linspace(0, cal_dict["max_angle"], cal_dict["N_measures"])
            cal_dict["illum_pol_degree"] = 1 * cal_dict['num_wavelengths']
            cal_dict["illum_az"] = 0 * degrees * cal_dict['num_wavelengths']
            cal_dict["illum_el"] = 45 * degrees * cal_dict['num_wavelengths']
            cal_dict["P1_p1"] = 1 * cal_dict['num_wavelengths']
            cal_dict["P1_p2"] = 0 * cal_dict['num_wavelengths']
            cal_dict["P1_az"] = 0 * degrees * cal_dict['num_wavelengths']
            cal_dict["P2_p1"] = 1 * cal_dict['num_wavelengths']
            cal_dict["P2_p2"] = 0 * cal_dict['num_wavelengths']
            cal_dict["P2_az"] = 0 * degrees * cal_dict['num_wavelengths']
            cal_dict["Pc_p1"] = 1 * cal_dict['num_wavelengths']
            cal_dict["Pc_p2"] = 0 * cal_dict['num_wavelengths']
            cal_dict["R1_p1"] = 1 * cal_dict['num_wavelengths']
            cal_dict["R1_p2"] = 1 * cal_dict['num_wavelengths']
            cal_dict["R1_az"] = 0 * degrees * cal_dict['num_wavelengths']
            cal_dict["R1_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
            cal_dict["R2_p1"] = 1 * cal_dict['num_wavelengths']
            cal_dict["R2_p2"] = 1 * cal_dict['num_wavelengths']
            cal_dict["R2_az"] = 0 * degrees * cal_dict['num_wavelengths']
            cal_dict["R2_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
            cal_dict["Rc_p1"] = 1 * cal_dict['num_wavelengths']
            cal_dict["Rc_p2"] = 1 * cal_dict['num_wavelengths']
            cal_dict["Rc_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
            cal_dict["Rc_offset"] = 0 * degrees * cal_dict['num_wavelengths']
        
        # Load the calibration files if the filename is not None
        else: 
            if filename and filename != "Calibration_measurements.npz":

                data = np.load("Step_0_" + filename + ".npz")
                cal_dict["S0"] = data["S0"]
                cal_dict["S0_error"] = data["S0_error"]
                
                data = np.load("Step_2a_" + filename + ".npz")
                cal_dict["I_step_2a"] = data["Iexp"]
                cal_dict["Angles_step_2a"] = data["angles"]

                data = np.load("Step_2b_" + filename + ".npz")
                cal_dict["I_step_2b"] = data["Iexp"]
                cal_dict["Angles_step_2b"] = data["angles"]

                data = np.load("Step_2c_" + filename + ".npz")
                cal_dict["I_step_2c"] = data["Iexp"]
                cal_dict["Angles_step_2c"] = data["angles"]

                data = np.load("Step_3a_" + filename + ".npz")
                cal_dict["I_step_3a"] = data["Iexp"]
                cal_dict["Angles_step_3a"] = data["angles"]

                data = np.load("Step_3b_" + filename + ".npz")
                cal_dict["I_step_3b"] = data["Iexp"]
                cal_dict["Angles_step_3b"] = data["angles"]

                data = np.load("Step_4_" + filename + ".npz")
                cal_dict["I_step_4"] = data["Iexp"]
                cal_dict["Angles_step_4"] = data["angles"]

                data = np.load("Step_5a_" + filename + ".npz")
                cal_dict["I_step_5a"] = data["Iexp"]
                cal_dict["Angles_step_5a"] = data["angles"]

                data = np.load("Step_5b_" + filename + ".npz")
                cal_dict["I_step_5b"] = data["Iexp"]
                cal_dict["Angles_step_5b"] = data["angles"]

                data = np.load("Step_6_" + filename + ".npz")
                cal_dict["I_step_6"] = data["Iexp"]
                cal_dict["Angles_step_6"] = data["angles"]

                cal_dict['Twait'] = 0.2
                cal_dict["N_measures"] = 91
                cal_dict["max_angle"] = 360
                cal_dict["angles"] = np.linspace(0, cal_dict["max_angle"], cal_dict["N_measures"])

                cal_dict["illum_pol_degree"] = 1 * cal_dict['num_wavelengths']
                cal_dict["illum_az"] = 0 * degrees * cal_dict['num_wavelengths']
                cal_dict["illum_el"] = 45 * degrees * cal_dict['num_wavelengths']
                cal_dict["P1_p1"] = 1 * cal_dict['num_wavelengths']
                cal_dict["P1_p2"] = 0 * cal_dict['num_wavelengths']
                cal_dict["P1_az"] = 0 * degrees * cal_dict['num_wavelengths']
                cal_dict["P2_p1"] = 1 * cal_dict['num_wavelengths']
                cal_dict["P2_p2"] = 0 * cal_dict['num_wavelengths']
                cal_dict["P2_az"] = 0 * degrees * cal_dict['num_wavelengths']
                cal_dict["Pc_p1"] = 1 * cal_dict['num_wavelengths']
                cal_dict["Pc_p2"] = 0 * cal_dict['num_wavelengths']
                cal_dict["R1_p1"] = 1 * cal_dict['num_wavelengths']
                cal_dict["R1_p2"] = 1 * cal_dict['num_wavelengths']
                cal_dict["R1_az"] = 0 * degrees * cal_dict['num_wavelengths']
                cal_dict["R1_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
                cal_dict["R2_p1"] = 1 * cal_dict['num_wavelengths']
                cal_dict["R2_p2"] = 1 * cal_dict['num_wavelengths']
                cal_dict["R2_az"] = 0 * degrees * cal_dict['num_wavelengths']
                cal_dict["R2_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
                cal_dict["Rc_p1"] = 1 * cal_dict['num_wavelengths']
                cal_dict["Rc_p2"] = 1 * cal_dict['num_wavelengths']
                cal_dict["Rc_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
                cal_dict["Rc_offset"] = 0 * degrees * cal_dict['num_wavelengths']

            else:
                data = np.load("Calibration_measurements.npz")
                cal_dict = dict(data)
        
        # Return to the old folder
        if folder and old_folder:
            os.chdir(old_folder)
        
        # Return the dictionary
        return cal_dict
    
    def Print_Calibration_Parameters(cal_dict, wavelength=None): 
        """Print the calibration parameters.
        
        Args:
            cal_dict (dict): Dictionary with the calibration configuration.
            wavelength (float): Wavelength of the calibration. Default: None.
        """
        if cal_dict['num_wavelengths'].size == 1: 
            idx = 0
        else: 
            if wavelength is not None: 
                idx = np.argmin(np.abs(cal_dict['wavelengths'] - wavelength))
            else: 
                print("Wavelength must be specified when num_wavelengths > 1")
                return
                    
        # Print the calibration parameters
        print("Parámetros de Calibración: ")
        if cal_dict['wavelengths'] is None:
            print("Longitud de onda: no definida")
        else: 
            print("Longitud de onda: {:.2f} nm".format(cal_dict['wavelengths'][idx]))

        print("Datos de la fuente: ")
        print("\tIntensidad: {:.2f}".format(cal_dict['S0'][idx]))
        print("\tAzimuth: {:.2f}°".format(cal_dict['illum_az'][idx]/degrees))
        print("\tElipticidad: {:.2f}°".format(cal_dict['illum_el'][idx]/degrees))
        print("\tGrado de polarización: {:.2f}".format(cal_dict['illum_pol_degree'][idx]))

        print("Datos de P1, P2 y Pc: ")
        print("\tP1:\tp1: {:.2f}\tp2: {:.2f}\taz: {:.2f}°".format(cal_dict['P1_p1'][idx],cal_dict['P1_p2'][idx],cal_dict['P1_az'][idx]/degrees))
        print("\tP2:\tp1: {:.2f}\tp2: {:.2f}\taz: {:.2f}°".format(cal_dict['P2_p1'][idx],cal_dict['P2_p2'][idx],cal_dict['P2_az'][idx]/degrees))
        print("\tPc:\tp1: {:.2f}\tp2: {:.2f}".format(cal_dict['Pc_p1'][idx],cal_dict['Pc_p2'][idx]))
        
        print("Datos de R1, R2 y Rc: ")
        print("\tR1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R1_p1'][idx],cal_dict['R1_p2'][idx],cal_dict['R1_Ret'][idx]/degrees, cal_dict['R1_az'][idx]/degrees))
        print("\tR2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R2_p1'][idx],cal_dict['R2_p2'][idx],cal_dict['R2_Ret'][idx]/degrees, cal_dict['R2_az'][idx]/degrees))
        print("\tRc:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\toffset: {:.2f}°".format(cal_dict['Rc_p1'][idx],cal_dict['Rc_p2'][idx],cal_dict['Rc_Ret'][idx]/degrees, cal_dict['Rc_offset'][idx]/degrees))

        return
        
    def Save_Cal_Files(folder=None, filename=None, cal_dict=None):
        """Function to save the calibration files.

        Args:
            folder (string): Folder where the calibration files are located. Default: None.
            filename (string): Name of the calibration files. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        
        Returns:
            None
        """ 
        # Open the folder
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)

        # Save the calibration files
        if filename and cal_dict:
            np.savez("Step_0_" + filename, S0=cal_dict["S0"], S0_error=cal_dict["S0_error"])
            np.savez("Step_2a_" + filename, Iexp=cal_dict["I_step_2a"], angles=cal_dict["Angles_step_2a"])
            np.savez("Step_2b_" + filename, Iexp=cal_dict["I_step_2b"], angles=cal_dict["Angles_step_2b"])
            np.savez("Step_2c_" + filename, Iexp=cal_dict["I_step_2c"], angles=cal_dict["Angles_step_2c"])
            np.savez("Step_3a_" + filename, Iexp=cal_dict["I_step_3a"], angles=cal_dict["Angles_step_3a"])
            np.savez("Step_3b_" + filename, Iexp=cal_dict["I_step_3b"], angles=cal_dict["Angles_step_3b"])
            np.savez("Step_4_" + filename, Iexp=cal_dict["I_step_4"], angles=cal_dict["Angles_step_4"])
            np.savez("Step_5a_" + filename, Iexp=cal_dict["I_step_5a"], angles=cal_dict["Angles_step_5a"])
            np.savez("Step_5b_" + filename, Iexp=cal_dict["I_step_5b"], angles=cal_dict["Angles_step_5b"])
            np.savez("Step_6_" + filename, Iexp=cal_dict["I_step_6"], angles=cal_dict["Angles_step_6"])
            np.savez("Calibration_measurements"+filename, **cal_dict)
        if cal_dict and not filename:
            np.savez("Calibration_measurements.npz", **cal_dict)

        # Return to the old folder
        if folder and old_folder:
            os.chdir(old_folder)
        return
    
    def make_step(pol = None, cal_dict = None, step = None, single = False, N=100, verbose = False):
        
        """Function to make a calibration step.

        Args:
            pol (SpectralPolarimeter): Spectral polarimeter object.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            step (string): Step to make. Default: None.
            single (bool): If True, only one measurement is made. Default: False.
            N (int): Number of measurements in the step. Default: 100.
            verbose (bool): If True, the function is verbose. Default: False.


        Returns:
            in step 0: 
                mean (np.ndarray): Array with the mean intensity of the step.
                error (np.ndarray): Array with the error intensity of the step.

            in the other steps:
                angles (np.ndarray): Array with the angles of the step.
                Iexp (np.ndarray): Array with the intensities of the step.
        """
        # Initialize the variables
        Iexp = []
        
        # Step 0 - Reference measurement
        if step == "0":
            angles = np.zeros(N)
            # Make the measurements
            for ind in range(N): 
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                time.sleep(cal_dict['Twait'])
                print("Measurement {} of {} done".format(ind+1, N), end='\r', flush=True)
            
            # Calculate the mean and error
            Iexp = np.array(Iexp) 
            mean = np.mean(Iexp, axis=0)
            error = np.std(Iexp, axis=0)
            
            # Save data in the dictionary
            cal_dict["S0"] = mean
            cal_dict["S0_error"] = error
            
            # Verbose
            if verbose: 
                if single: 
                    print("\nMean intensity: {:.2f}".format(mean))
                    print("Error intensity: {:.2f}".format(error))
                else: 
                    plt.subplot(1,2,1)
                    plt.plot(cal_dict['wavelengths'],mean)
                    plt.title("\nMean intensity - S0")
                    plt.xlabel("Wavelength (nm)")
                    plt.ylabel("Intensity (a.u.)")
                    plt.subplot(1,2,2)
                    plt.plot(cal_dict['wavelengths'], error)
                    plt.title("\nError intensity - S0")
                    plt.xlabel("Wavelength (nm)")
                    plt.ylabel("Intensity (a.u.)")
                    
            return mean, error

        # Step 1a and 1b - Malus Law and Retarder mesaurement manuallly 
        elif step == "1a" or step == "1b":
            angles = np.zeros(N)
            # Make the measurements
            for ind in range(N):
                ang = input('Angle (in degrees). Type "End" to close.')
                try:
                    angles[ind] = float(ang)
                    if single:
                        Iexp.append(pol.spectrometer.Get_Measurement(mode = "max"))
                    else: 
                        Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                except:
                    if ang.lower() in ('fin', 'end', 'stop'):
                        angles = angles[:ind]
                        break
                    else:
                        print('Value {} not accepted'.format(ang))
                        
            Iexp = np.array(Iexp)
            angles = np.array(angles*degrees)
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
           
        # Step 2a - Malus Law to measure Pref and P1
        elif step == "2a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([angle, 0, 0, 0])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[0] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 2b - Malus Law to measure Pref and P2
        elif step == "2b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([0, 0, 0, angle])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[3] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 2c - Malus Law to measure P1 and P2
        elif step == "2c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, 0, angle])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[3] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 3a - Retarder R2 Mesaurement
        elif step == "3a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, angle, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 3b - Retarder R2 - Rc Measurement
        elif step == "3b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]+cal_dict['R2_az']/degrees):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, angle, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 4 - Illumination analysis
        elif step == '4': 
            angles = utils.PSA_states_2_angles(S = utils.S_20)
            # Make the measurements
            utils.percentage_complete()
            for ind in range(len(angles[0])):
                # theta = np.array((0, 0, angles[1][ind]+cal_dict['R2_az'][ind], angles[0][ind] + cal_dict['P2_az'][ind]))
                theta = np.array((0, 0, angles[1][ind]+cal_dict['R2_az'], angles[0][ind] + cal_dict['P2_az']))
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="rad")
                #print(angles_aux)
                angles[1][ind] = angles_aux[2]
                angles[0][ind] = angles_aux[3]
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                utils.percentage_complete(ind, 20)
            Iexp = np.array(Iexp)
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles

        # Step 5a - Retarder R1 Mesaurement
        elif step == "5a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, angle, 0, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 5b - Retarder R1 - Rc Measurement
        elif step == "5b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]+cal_dict['R1_az']/degrees):
                theta = np.array([cal_dict["P1_az"]/degrees, angle, 0, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 6 - Air Mueller Matrix
        elif step == '6': 
            angles = utils.calculate_polarimetry_angles(144, type='perfect')
            # Make the measurements
            utils.percentage_complete()
            for ind, angle in enumerate(angles):
                theta_0 = np.array((cal_dict['P1_az'],cal_dict['R1_az'],cal_dict['R2_az'],cal_dict['P2_az']))
                theta = theta_0 + angle
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="rad")
                #print(angles_aux)
                angles[ind] = angles_aux 
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "max", wavelength = None, width = 4))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                utils.percentage_complete(ind, 144)
            Iexp = np.array(Iexp)
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles

        else: 
            return("Error. Step not recognized")
      
    
    def model_step(par = None,  step = None,  angle = None, cal_dict = None): 
        """Function to model the calibration step.
        
        Args:
            step (string): Step to model. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            par (np.ndarray): Parameters of the model. Default: None.
            angle (np.ndarray): Array with the angles of the step. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        """
        if step == "1a":
            Imodel = model_cos2(par, angle)
            return Imodel
        elif step == "1b":
            Imodel = model_cos2_2(par, angle)
            return Imodel              
        elif step == '2': 
            # Rename parameters
            (P1_p1, P1_p2, Pc_p1, Pc_p2) = par
            # (P1_p1, P2_p1, Pc_p1) = par
            # Create objects
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            Mp1 = Mueller().diattenuator_azimuth_ellipticity(
                p1=P1_p1, p2=P1_p2, azimuth=-cal_dict['P1_az'])
            Mp2 = Mueller().diattenuator_azimuth_ellipticity(
                p1=P1_p1, p2=P1_p2, azimuth=-cal_dict['P2_az'])
            Mpc = Mueller().diattenuator_azimuth_ellipticity(
                p1=Pc_p1, p2=Pc_p2, azimuth=0*degrees)
            # Step 2a
            Mp1_rot = Mp1.rotate(angle=cal_dict["Angles_step_2a"], keep=True)
            Sf =  (Mp1_rot * (Mpc * S))
            I_2a = Sf.parameters.intensity()
            
            # Step 2b
            Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_2b"], keep=True)
            Sf = (Mp2_rot * (Mpc * S))
            I_2b = Sf.parameters.intensity()
            
            # Step 2c
            Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_2c"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Sf = (Mp2_rot * (Mp1_rot * S))
            I_2c = Sf.parameters.intensity()

            # return I_2a, I_2b, I_2c
            return I_2c

        
        elif step == '3a':
            # Rename parameters
            (R2_p1, R2_p2, R2_Ret) = par

            # Create objects
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            
            Mp1 = Mueller().diattenuator_linear(
                p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'])
            Mp2 = Mueller().diattenuator_linear(
                p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])

            # Step 3a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_3a"], keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S)))
            I_3a = Sf.parameters.intensity()
            return I_3a 
        
        elif step == '3b':
            # Rename parameters
            (Rc_p1, Rc_p2, Rc_Ret, dif_con) = par

            # Create objects
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            
            Mp1 = Mueller().diattenuator_linear(
                p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict['R2_az'])
            Mp2 = Mueller().diattenuator_linear(
                p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
            Mrc = Mueller().diattenuator_retarder_linear(
                p1=Rc_p1, p2=Rc_p2, R=Rc_Ret, azimuth= 45*degrees - dif_con)
            
            # Step 3b
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_3b"], keep=True)
            Sf = Mp2_rot *(Mr2_rot *(Mrc * (Mp1_rot * S)))
            I_3b = Sf.parameters.intensity()
            return I_3b
        
        elif step == '4':
            (illum_az, illum_el, illum_pol_degree) = par
            # Create objects
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict['S0'],
                azimuth=illum_az,
                ellipticity=illum_el,
                degree_pol=illum_pol_degree)
            
            
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict['R2_az'])
            Mp2 = Mueller().diattenuator_linear(
                p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
            
            # Step 4
            Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_4"][0], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_4"][1], keep=True)
            Sf = Mp2_rot * Mr2_rot * S
            I_4 = Sf.parameters.intensity()
            return I_4
        
        elif step == '5a':
            (R1_p1, R1_p2, R1_Ret) = par
            # Create objects
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            Mp1 = Mueller().diattenuator_linear(
                p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'])
            Mp2 = Mueller().diattenuator_linear(
                p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
            # Step 5a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_5a"], keep=True)
            Sf = Mp2_rot * Mr1_rot * Mp1_rot * S
            I_5a = Sf.parameters.intensity()
            return I_5a

        elif step == '5b':

            # Rename parameters
            (Rc_p1, Rc_p2, Rc_Ret, dif_con) = par

            # Create objects
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            
            Mp1 = Mueller().diattenuator_linear(
                p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R1_p1"], p2=cal_dict["R1_p2"], R=cal_dict["R1_Ret"], azimuth=-cal_dict['R1_az'])
            Mp2 = Mueller().diattenuator_linear(
                p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
            Mrc = Mueller().diattenuator_retarder_linear(
                p1=Rc_p1, p2=Rc_p2, R=Rc_Ret, azimuth=45*degrees - dif_con)

            # Step 5b
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_5b"], keep=True)
            Sf = Mp2_rot * Mrc *Mr1_rot * Mp1_rot * S
            I_5b = Sf.parameters.intensity()

            return I_5b

        else: 
            return("Error. Step not recognized")
        
            
        
    def error_step(par = None, step = None, angle = None, Iexp = None, cal_dict = None):
        """Function to calculate the error for adjusting the model to the experimental data.
        
        Args:
            step (string): Step to model. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            par (np.ndarray): Parameters of the model. Default: None.
            angle (np.ndarray): Array with the angles of the step. Default: None.
            Iexp (np.ndarray): Array with the intensities of the step. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        """
        if step == "1a":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "1a", angle)
            dif = Iexp - Imodel
            return dif
        elif step == "1b":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "1b", angle)
            dif = Iexp - Imodel
            return dif
        elif step == "2":
            # I_2a, I_2b, I_2c = SpectralPolarimeterCalibration.model_step(par,'2',angle = None,cal_dict=cal_dict)
            I_2c = SpectralPolarimeterCalibration.model_step(par,'2',angle = None,cal_dict=cal_dict)

            # E_2a = (cal_dict["I_step_2a"] - I_2a) /91
            # E_2b = (cal_dict["I_step_2b"] - I_2b) /91
            E_2c = (cal_dict["I_step_2c"] - I_2c)
            # Error = np.concatenate((E_2a, E_2b, E_2c))
            Error = np.sum(np.square(E_2c))
            return Error
        elif step == "3a":
            I_3a = SpectralPolarimeterCalibration.model_step(par,'3a',angle = None, cal_dict=cal_dict)
            E_3a = (cal_dict["I_step_3a"] - I_3a) /91
            Error = np.array(E_3a)
            return Error
        elif step == "3b":
            I_3b = SpectralPolarimeterCalibration.model_step(par,'3b',angle = None, cal_dict=cal_dict)
            E_3b = (cal_dict["I_step_3b"] - I_3b)
            Error = np.array(E_3b)
            return Error
        elif step == "4":
            Imodel = SpectralPolarimeterCalibration.model_step(par,'4',angle = None,cal_dict=cal_dict)
            E_4 = (cal_dict["I_step_4"] - Imodel)
            Error = np.array(E_4)
            return Error
        elif step == "5a":
            I_5a = SpectralPolarimeterCalibration.model_step(par,'5a',angle = None, cal_dict=cal_dict)
            E_5a = (cal_dict["I_step_5a"] - I_5a) /91
            Error = np.array(E_5a)
            return Error
        elif step == "5b":
            I_5b = SpectralPolarimeterCalibration.model_step(par,'5b',angle = None, cal_dict=cal_dict)
            E_5b = (cal_dict["I_step_5b"] - I_5b) /91
            Error = np.array(E_5b)
            return Error
        else: 
            return("Error. Step not recognized")
        
    def analyze_step(step = None, cal_dict = None, single = False, angles = None, Iexp = None, N = 3648):
        """Function to analyze the calibration step."""
        
        

        if step == "1a": 
            optm_result = []
            if single: 
                I = Iexp/cal_dict['S0']
                (Imax, Imin) = (np.max(I), np.min(I))
                bounds = ([0, 0, 0*degrees], [0.3*Imax,Imax, 180 * degrees])
                par0 = [np.max((0,Imin)), Imax , np.random.rand() * 180*degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("1a", angles, I), bounds=bounds)
                optm_result.append(result.x)
            else: 
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i]/cal_dict['S0'][i]
                    (Imax, Imin) = (np.max(I[:,i]), np.min(I[:,i]))
                    bounds = ([0, 0, -360*degrees], [0.3*Imax,Imax, 360 * degrees])
                    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 180 *degrees]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("1a", angles, I[:,i]), bounds=bounds)
                    # while result.x[2] - 0*degrees < 1e-3 or result.x[2] - 180*degrees < 1e-3:
                    #     result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("1a", angles, I[:,i]), bounds=bounds)
                    result.x[2] = result.x[2]%(180*degrees)
                    optm_result.append(result.x)
                    
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")

            if single: 
                # Plot result
                optm_result = optm_result[0]

                Imodel = model_cos2(optm_result, angles)

                utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Step 1a')
                # Print results
                print('The values obtained are:')
                print('   - Imax       : {:.4f} V'.format(optm_result[1] + optm_result[0]))
                print('   - Imin       : {:.4f} V'.format(optm_result[0]))
                print('   - Maximum angle : {:.2f} deg'.format(optm_result[2]/degrees))  
            # else: 
            #     plt.plot(angles, optm_result[:,2]/degrees)
            #     plt.title("Azimuth angle")
            #     plt.xlabel("Wavelegth (nm)")
            #     plt.ylabel("Intensity (a.u.)")     

            return np.array(optm_result)
        
        elif step == "1b": 
            optm_result = []

            if single: 
                I = Iexp/cal_dict['S0']
                (Imax, Imin) = (np.max(I), np.min(I))
                bounds = ([0, 0, 0*degrees], [Imax, Imax, 90 * degrees])
                par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 90 *degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("1b", angles, I), bounds=bounds)
                optm_result.append(result.x)
            else: 
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i]/cal_dict['S0'][i]
                    (Imax, Imin) = (np.max(I[:,i]), np.min(I[:,i]))
                    bounds = ([0, 0, -90*degrees], [Imax,Imax, 90 * degrees])
                    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 90 *degrees]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("1b", angles, I[:,i]), bounds=bounds)
                    result.x[2] = result.x[2]%(90*degrees)
                    optm_result.append(result.x)
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")

            if single: 
                # Plot result
                optm_result = optm_result[0]
                Imodel = model_cos2_2(optm_result, angles)
                utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Step 1b')
                # Print results
                print('The values obtained are:')
                print('   - Imax       : {:.4f} V'.format(optm_result[1] + optm_result[0]))
                print('   - Imin       : {:.4f} V'.format(optm_result[0]))
                print('   - Maximum angle : {:.2f} deg'.format(optm_result[2]/degrees))  
            # else: 
            #     plt.plot(angles, optm_result[:,2]/degrees)
            #     plt.title("Azimuth angle")
            #     plt.xlabel("Wavelegth (nm)")
            #     plt.ylabel("Intensity (a.u.)")
                
            return np.array(optm_result)
        
        elif step == "2":
            optm_result = []
            bound_up = np.array([
                1,                  # P1-p1 
                1,                  # P1-p2
                1,                  # Pc-p1 
                1,                  # Pc-p2
            ])
            bound_down = np.array([
                0,                  # P1-p1 
                0,                  # P1-p2
                0,                  # Pc-p1 
                0,                  # Pc-p2 
            ])

            bounds = (bound_down, bound_up)

            # Least squares
            
            par0 = (1,0,1,0)

            if single: 
                args = ['2', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    # Create cal_dict_temp with the data of the measurement only arrays
                    # cal_dict_temp = {key: cal_dict[key][i] if key in ['wavelengths', 'S0', 'S0_error', 'illum_pol_degree', 'illum_az', 'illum_el', 'P1_p1', 'P1_p2', 'P2_p1', 'P2_p2', 'Pc_p1', 'Pc_p2', 'R1_p1', 'R1_p2', 'R2_p1', 'R2_p2', 'Rc_p1', 'Rc_p2', 'R1_Ret', 'R2_Ret', 'Rc_Ret', 'Rc_offset'] else cal_dict[key] for key in cal_dict.keys()}
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['2', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")
            
            # [cal_dict["P1_p1"], cal_dict["P1_p2"],
            #     cal_dict["P2_p1"], cal_dict["P2_p2"], 
            #     cal_dict["Pc_p1"], cal_dict["Pc_p2"]] = result
            
            
            return np.array(optm_result)

        elif step == "3a": 
            optm_result = []
            bound_up = np.array([
                1,                  # R2-p1 
                1,                  # R2-p2 
                180 * degrees       # R2-retardancia 
            ])
            bound_down = np.array([
                0,                  # R2-p1 
                0,                  # R2-p2 
                0 * degrees         # R2-retardancia 
            ])
            bounds = (bound_down, bound_up)

            # Least squares
           
            
            par0 = (1,1, 90*degrees)

            if single: 
                args = ['3a', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    # Create cal_dict_temp with the data of the measurement only arrays
                    # cal_dict_temp = {key: cal_dict[key][i] if key in ['wavelengths', 'S0', 'S0_error', 'illum_pol_degree', 'illum_az', 'illum_el', 'P1_p1', 'P1_p2', 'P2_p1', 'P2_p2', 'Pc_p1', 'Pc_p2', 'R1_p1', 'R1_p2', 'R2_p1', 'R2_p2', 'Rc_p1', 'Rc_p2', 'R1_Ret', 'R2_Ret', 'Rc_Ret', 'Rc_offset'] else cal_dict[key] for key in cal_dict.keys()}
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['3a', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")
            
            return np.array(optm_result)
        
        elif step == "3b": 

            optm_result = []
            bound_up = np.array([
                1,                  # Rc-p1 
                1,                  # Rc-p2 
                180 * degrees,      # Rc-retardancia 
                45 * degrees        # Rc-offset 
            ])
            bound_down = np.array([
                0,                  # Rc-p1 
                0,                  # Rc-p2 
                0 * degrees,       # Rc-retardancia 
                -45 * degrees       # Rc-offset 
            ])

            bounds = (bound_down, bound_up)

            par0 = (1,1, 90*degrees, 0*degrees)
            
            if single: 
                args = ['3b', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    # Create cal_dict_temp with the data of the measurement only arrays
                    # cal_dict_temp = {key: cal_dict[key][i] if key in ['wavelengths', 'S0', 'S0_error', 'illum_pol_degree', 'illum_az', 'illum_el', 'P1_p1', 'P1_p2', 'P2_p1', 'P2_p2', 'Pc_p1', 'Pc_p2', 'R1_p1', 'R1_p2', 'R2_p1', 'R2_p2', 'Rc_p1', 'Rc_p2', 'R1_Ret', 'R2_Ret', 'Rc_Ret', 'Rc_offset'] else cal_dict[key] for key in cal_dict.keys()}
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['3b', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")
            return np.array(optm_result)
        
        elif step == "4":

            optm_result = []
            bound_up = np.array([
                180 * degrees,      # Azimuth de la iluminación 
                45 * degrees,       # Elipticidad de la iluminación 
                1,                  # Grado de pol. de la iluminación 
            ])
            bound_down = np.array([
                0 * degrees,        # Azimuth de la iluminación 
                -45 * degrees,      # Elipticidad de la iluminación 
                0,                  # Grado de pol. de la iluminación
            ])
            bounds = (bound_down, bound_up)

            par0 = (90*degrees, 0*degrees, 1)

            if single: 
                args = ['4', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    # Create cal_dict_temp with the data of the measurement only arrays
                    # cal_dict_temp = {key: cal_dict[key][i] if key in ['wavelengths', 'S0', 'S0_error', 'illum_pol_degree', 'illum_az', 'illum_el', 'P1_p1', 'P1_p2', 'P2_p1', 'P2_p2', 'Pc_p1', 'Pc_p2', 'R1_p1', 'R1_p2', 'R2_p1', 'R2_p2', 'Rc_p1', 'Rc_p2', 'R1_Ret', 'R2_Ret', 'Rc_Ret', 'Rc_offset'] else cal_dict[key] for key in cal_dict.keys()}
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['4', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")
            return np.array(optm_result)

        elif step == "5a":
            optm_result = []
            bound_up = np.array([
                1,                  # R1-p1 
                1,                  # R1-p2 
                180 * degrees       # R1-retardancia 
            ])
            bound_down = np.array([
                0,                  # R1-p1 
                0,                  # R1-p2 
                0 * degrees         # R1-retardancia 
            ])
            bounds = (bound_down, bound_up)

            par0 = (1,1, 90*degrees)

            if single: 
                args = ['5a', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    # Create cal_dict_temp with the data of the measurement only arrays
                    # cal_dict_temp = {key: cal_dict[key][i] if key in ['wavelengths', 'S0', 'S0_error', 'illum_pol_degree', 'illum_az', 'illum_el', 'P1_p1', 'P1_p2', 'P2_p1', 'P2_p2', 'Pc_p1', 'Pc_p2', 'R1_p1', 'R1_p2', 'R2_p1', 'R2_p2', 'Rc_p1', 'Rc_p2', 'R1_Ret', 'R2_Ret', 'Rc_Ret', 'Rc_offset'] else cal_dict[key] for key in cal_dict.keys()}
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['5a', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")
            return np.array(optm_result)

        elif step == "5b": 

            optm_result = []
            bound_up = np.array([
                1,                  # Rc-p1 
                1,                  # Rc-p2 
                180 * degrees,      # Rc-retardancia 
                45 * degrees        # Rc-offset 
            ])
            bound_down = np.array([
                0,                  # Rc-p1 
                0,                  # Rc-p2 
                0 * degrees,       # Rc-retardancia 
                -45 * degrees       # Rc-offset 
            ])

            bounds = (bound_down, bound_up)

            par0 = (1,1, 90*degrees, 0*degrees)
            
            if single: 
                args = ['5b', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    # Create cal_dict_temp with the data of the measurement only arrays
                    # cal_dict_temp = {key: cal_dict[key][i] if key in ['wavelengths', 'S0', 'S0_error', 'illum_pol_degree', 'illum_az', 'illum_el', 'P1_p1', 'P1_p2', 'P2_p1', 'P2_p2', 'Pc_p1', 'Pc_p2', 'R1_p1', 'R1_p2', 'R2_p1', 'R2_p2', 'Rc_p1', 'Rc_p2', 'R1_Ret', 'R2_Ret', 'Rc_Ret', 'Rc_offset'] else cal_dict[key] for key in cal_dict.keys()}
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['5b', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")
            return np.array(optm_result)

        else:
            return "Error. Step not recognized"
    
    def Process_Iterative_Calibration():
        pass
    def Process_Simultaneous_Calibration():
        pass
    


############################################################################################################################################################################

# SPECTRAL POLARIMETER CLASS

class SpectralPolarimeter(object):
    
    """
    Class for spectral polarimeter. 
    It will include control of the rotary motors of PSG and PSA elements, the spectrometer for measuring signal.

    Args:
        calibration (string): Load all calibration files ("full"), only the corresponding to illumination, PSG and PSA ("minimal") or none ("none"). Default: "full".
        conf_dict (dict): Dictionary with the calibration configuration. Default: CONF_POLARIMETER.
        name_motor (string): Name of the rotating motors used. Default: "DT50".
        name_spectrometer (string): Name of the spectrometer used. Default: "AVANTES".

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        daca (DACA): Data acquisition card object.
        norm (float): Intensity normalization constant.
        I_Distribution (np.ndarray): Intensity normalization array.
    """
    def __init__(self, calibration="full", conf_dict=CONF_POLARIMETER, name_motor="DT50", name_spectrometer="Avantes"): 

        # Initialize the objects
        self.motor = Motor_Multiple(name=name_motor, N=4)
        self.spectrometer = Spectrometer(name=name_spectrometer)
        
        self.name_motor = name_motor 
        self.name_spectrometer = name_spectrometer

        # # Load calibration info
        if calibration.lower() == "full":
             self.Load_Calibration(filename=conf_dict['cal_file'], BS_ref=conf_dict['BS_ref'], BS_trans=conf_dict['BS_trans'], ref_mirror=conf_dict['ref_mirror'], folder=conf_dict["cal_folder"], objective = conf_dict['objective_trans'])

    def Open(self): 

        """Open all devices."""

        # Motor configuration
        if self.name_motor == "DT50":
            config_motor = CONF_DT_50
        
        elif self.name_motor == "InteliDrives":
            config_motor = CONF_INTEL

        elif self.name_motor == "Zaber":
            config_motor = CONF_ZABER
        
        if self.name_spectrometer == "Avantes":
            config_spectrometer = CONF_AVANTES
        
        # Open the motor
        self.motor.Open(port=config_motor["ports"], invert=config_motor["invert"],axis=config_motor["axes"])
        self.motor.Home()
        print('\n')
        
        # Open the spectrometer
        self.spectrometer.Open()
        self.spectrometer.Get_Wavelength()
        self.spectrometer.Get_Number_Px()
        self.spectrometer.Set_Parameters(exposure = config_spectrometer["exposure"], N_average= config_spectrometer["N_average"])   

    def Load_Calibration(): 
        pass
    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=np.zeros(4), units="rad")
        pass
    def Close(self):
        """Close all devices."""
        self.motor.Close()
        self.spectrometer.Close()
        
    def Set_Property():
        pass
    
    def Measure_ND_Mueller_Matrix():
        pass


############################################################################################################################################################################

# SPECTRAL POLARIMETER UTILS CLASS

class SpectralPolarimeter_utils(object):
    
    """
    Class for spectral polarimeter utilities. 
        
    Args:
        

    Atributes:
        
    """

    def Draw_Mueller_Matrix(): 
        pass
    def Calculate_Mueller_Matrix(): 
        pass
    def MSystem_from_dict():
        pass   
    def Calculate_Stokes():
        pass
    def Analyze_Mueller_Matrix():
        pass
    
    ############################################################################################################################################################################


# OTHER FUNCTIONS


def model_cos2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2
    return Imodel

def error_cos2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2(par, angle)
    dif = Iexp - Imodel
    return dif

def model_cos2_2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(2*(angle - par[2]))**2
    return Imodel

def error_cos2_2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2_2(par, angle)
    dif = Iexp - Imodel
    return dif

def draw_values(param_x, param_y, values, param_x_name='Param_X', param_y_name='Param_Y', values_name='Values'):
    """Draw the values in 3d projection plot of the polarimeter."""
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.meshgrid(param_x, param_y[:, np.newaxis])
    ax.plot_surface(X, Y, values, cmap='viridis')
    ax.set_xlabel(param_x_name)
    ax.set_ylabel(param_y_name)
    ax.set_zlabel(values_name)
    return 

def create_temp_dict(cal_dict, value):
    """Create a dictionary with the same keys as cal_dict but with the value given."""
    cal_dict_temp = cal_dict.copy()
    cal_dict_temp['wavelengths'] = cal_dict['wavelengths'][value]
    cal_dict_temp['S0'] = cal_dict['S0'][value]
    cal_dict_temp['S0_error'] = cal_dict['S0_error'][value]
    cal_dict_temp['illum_pol_degree'] = cal_dict['illum_pol_degree'][value]
    cal_dict_temp['illum_az'] = cal_dict['illum_az'][value]
    cal_dict_temp['illum_el'] = cal_dict['illum_el'][value]
    cal_dict_temp['P1_p1'] = cal_dict['P1_p1'][value]
    cal_dict_temp['P1_p2'] = cal_dict['P1_p2'][value]
    cal_dict_temp['P2_p1'] = cal_dict['P2_p1'][value]
    cal_dict_temp['P2_p2'] = cal_dict['P2_p2'][value]
    cal_dict_temp['Pc_p1'] = cal_dict['Pc_p1'][value]
    cal_dict_temp['Pc_p2'] = cal_dict['Pc_p2'][value]
    cal_dict_temp['R1_p1'] = cal_dict['R1_p1'][value]
    cal_dict_temp['R1_p2'] = cal_dict['R1_p2'][value]
    cal_dict_temp['R1_Ret'] = cal_dict['R1_Ret'][value]
    cal_dict_temp['R2_p1'] = cal_dict['R2_p1'][value]
    cal_dict_temp['R2_p2'] = cal_dict['R2_p2'][value]
    cal_dict_temp['R2_Ret'] = cal_dict['R2_Ret'][value]
    cal_dict_temp['Rc_p1'] = cal_dict['Rc_p1'][value]
    cal_dict_temp['Rc_p2'] = cal_dict['Rc_p2'][value]
    cal_dict_temp['Rc_Ret'] = cal_dict['Rc_Ret'][value]
    cal_dict_temp['Rc_offset'] = cal_dict['Rc_offset'][value]
    cal_dict_temp['I_step_2a'] = cal_dict['I_step_2a'][:,value]
    cal_dict_temp['I_step_2b'] = cal_dict['I_step_2b'][:,value]
    cal_dict_temp['I_step_2c'] = cal_dict['I_step_2c'][:,value]
    cal_dict_temp['I_step_3a'] = cal_dict['I_step_3a'][:,value]
    cal_dict_temp['I_step_3b'] = cal_dict['I_step_3b'][:,value]
    cal_dict_temp['I_step_4'] = cal_dict['I_step_4'][:,value]
    cal_dict_temp['I_step_5a'] = cal_dict['I_step_5a'][:,value]
    cal_dict_temp['I_step_5b'] = cal_dict['I_step_5b'][:,value]
    cal_dict_temp['I_step_6'] = cal_dict['I_step_6'][:,value]
    return cal_dict_temp
    
def reduce_dict(cal_dict, N):
    """Create a dictionary with the same keys as cal_dict but with the value given."""
    cal_dict_temp = cal_dict.copy()
    cal_dict_temp['wavelengths'] = cal_dict['wavelengths'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['S0'] = cal_dict['S0'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['S0_error'] = cal_dict['S0_error'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['illum_pol_degree'] = cal_dict['illum_pol_degree'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['illum_az'] = cal_dict['illum_az'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['illum_el'] = cal_dict['illum_el'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['P1_p1'] = cal_dict['P1_p1'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['P1_p2'] = cal_dict['P1_p2'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['P2_p1'] = cal_dict['P2_p1'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['P2_p2'] = cal_dict['P2_p2'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['Pc_p1'] = cal_dict['Pc_p1'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['Pc_p2'] = cal_dict['Pc_p2'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['R1_p1'] = cal_dict['R1_p1'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['R1_p2'] = cal_dict['R1_p2'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['R1_Ret'] = cal_dict['R1_Ret'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['R2_p1'] = cal_dict['R2_p1'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['R2_p2'] = cal_dict['R2_p2'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['R2_Ret'] = cal_dict['R2_Ret'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['Rc_p1'] = cal_dict['Rc_p1'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['Rc_p2'] = cal_dict['Rc_p2'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['Rc_Ret'] = cal_dict['Rc_Ret'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['Rc_offset'] = cal_dict['Rc_offset'][np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_2a'] = cal_dict['I_step_2a'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_2b'] = cal_dict['I_step_2b'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_2c'] = cal_dict['I_step_2c'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_3a'] = cal_dict['I_step_3a'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_3b'] = cal_dict['I_step_3b'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_4'] = cal_dict['I_step_4'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_5a'] = cal_dict['I_step_5a'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_5b'] = cal_dict['I_step_5b'][:,np.linspace(0,3647,N,dtype=int)]
    cal_dict_temp['I_step_6'] = cal_dict['I_step_6'][:,np.linspace(0,3647,N,dtype=int)]
    return cal_dict_temp

