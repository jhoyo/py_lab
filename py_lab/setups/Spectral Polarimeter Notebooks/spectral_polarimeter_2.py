### SPECTRAL POLARIMETER V 2.0 ###

# Made by: Joaquín Andrés Porras
# Date: 01/10/2024
# Last modification: 01/10/2024
# Version: 2.0.0

# Description: This file contains the functions needed to calibrate and use the spectral polarimeter. 
# Classes: 
#   - SpectralPolarimeterCalibration
#   - SpectralPolarimeter
#   - SpectralPolarimeter_utils

############################################################################################################################################################################
# IMPORTS
# GENERAL MODULES

import numpy as np                                              # Numpy
import os                                                       # OS
import datetime                                                 # Datetime
import time                                                     # Time

# CALCULUS MODULES

import numpy as np                                              # Numpy
import matplotlib.pyplot as plt                                 # Matplotlib
# %matplotlib widget                                            # Widget
from scipy.optimize import least_squares                        # Least squares
from pyswarms.single.global_best import GlobalBestPSO           # PSO

# PY-LAB MODULES

import py_lab.utils as utils                                    # Utils py-lab
from py_lab.motor import Motor_Multiple
from py_lab.spectrometer import Spectrometer
from py_lab.config import degrees, CONF_DT_50, CONF_POLARIMETER, CONF_INTEL, CONF_ZABER, CONF_AVANTES
from py_lab.utils import Rotation_Poincare, PSG_angles_2_states, PSA_angles_2_states, PSG_states_2_angles, PSA_states_2_angles
from py_lab.setups.polarimeter_utils import Calculate_Mueller   # Mueller - Polarimeter

# MÓDULOS PY-POL

from py_pol.mueller import Mueller                              # Mueller
from py_pol.stokes import Stokes                                # Stokes

############################################################################################################################################################################

# SPECTRAL POLARIMETER CALIBRATION CLASS

class SpectralPolarimeterCalibration(object):
    
    """
    Class for calibrating the spectral polarimeter. 
    It contains the following functions:
        - Create_Cal_Files
        - Load_Cal_Files
        - Print_Calibration_Parameters
        - Save_Cal_Files
        - make_step
        - model_step
        - error_step
        - analyze_step
        - Process_Iterative_Calibration
        - Process_Simultaneous_Calibration
    """
    
    def Create_Cal_Files(single = False, wavelengths = None):
        """Function to generate the initial cal_dict.
        
        Args:
            single (bool): If True, individual dict; else, spectral dict. Default: False.
            wavelengths (np.ndarray): Array with the wavelengths. Default: None.

        Returns:
            cal_dict (dict): Dictionary with the calibration configuration.
        """

        # Initialize the dictionary
        cal_dict = {}

        # Load the wavelengths
        if single:
            if type(wavelengths) in (int, float):
                cal_dict['num_wavelengths'] = np.ones(1)
                cal_dict['wavelengths'] = np.array([wavelengths])
            elif wavelengths is None:
                cal_dict['num_wavelengths'] = np.ones(1)
                cal_dict['wavelengths'] = wavelengths
            else: 
                print("Wavelengths must be a number when single is True")
                return
        else:  
            if wavelengths is None: 
                print("Wavelengths must be specified")
                return
            else: 
                if not isinstance(wavelengths, np.ndarray):
                    wavelengths = np.array(wavelengths)  
                cal_dict['num_wavelengths'] = np.ones_like(wavelengths)
                cal_dict['wavelengths'] = wavelengths
        
        # Create the dictionary
        cal_dict['Twait'] = 0.2
        cal_dict["N_measures"] = 361
        cal_dict["max_angle"] = 180
        cal_dict["angles"] = np.linspace(0, cal_dict["max_angle"], cal_dict["N_measures"])

        cal_dict["S0"] = 1 * cal_dict['num_wavelengths']
        cal_dict["S0_error"] = 0 * cal_dict['num_wavelengths']
        cal_dict["S1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["S1_error"] = 0 * cal_dict['num_wavelengths']
        cal_dict["P0_az"] = 0 * degrees * cal_dict['num_wavelengths']
        
        cal_dict["illum_pol_degree"] = 1 * cal_dict['num_wavelengths']
        cal_dict["illum_az"] = 0 * degrees * cal_dict['num_wavelengths']
        cal_dict["illum_el"] = 45 * degrees * cal_dict['num_wavelengths']

        cal_dict["P1_p1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["P1_p2"] = 0 * cal_dict['num_wavelengths']
        cal_dict["P1_az"] = 0 * degrees * cal_dict['num_wavelengths']
        cal_dict["P1_Ret"] = 0 * degrees * cal_dict['num_wavelengths']

        cal_dict["P2_p1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["P2_p2"] = 0 * cal_dict['num_wavelengths']
        cal_dict["P2_az"] = 0 * degrees * cal_dict['num_wavelengths']
        cal_dict["P2_Ret"] = 0 * degrees * cal_dict['num_wavelengths']

        cal_dict["Pc_p1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["Pc_p2"] = 0 * cal_dict['num_wavelengths']

        cal_dict["R1_p1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["R1_p2"] = 1 * cal_dict['num_wavelengths']
        cal_dict["R1_az"] = 0 * degrees * cal_dict['num_wavelengths']
        cal_dict["R1_Ret"] = 90 * degrees * cal_dict['num_wavelengths']

        cal_dict["R2_p1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["R2_p2"] = 1 * cal_dict['num_wavelengths']
        cal_dict["R2_az"] = 0 * degrees * cal_dict['num_wavelengths']
        cal_dict["R2_Ret"] = 90 * degrees * cal_dict['num_wavelengths']

        cal_dict["Rc_p1"] = 1 * cal_dict['num_wavelengths']
        cal_dict["Rc_p2"] = 1 * cal_dict['num_wavelengths']
        cal_dict["Rc_Ret"] = 90 * degrees * cal_dict['num_wavelengths']
        cal_dict["Rc_offset"] = 0 * degrees * cal_dict['num_wavelengths']

        return cal_dict
    
    def Load_Cal_Files(folder=None, filename=None, single = False, wavelengths = None):
        """Function to generate the initial cal_dict, except calibration files are loaded.
        
        Args:
            folder (string): Folder where the calibration files are located. Default: None.
            filename (string): Name of the calibration files. Default: None.
            single (bool): If True, general parameters is loaded. Default: False.
            wavelengths (np.ndarray): Array with the wavelengths. Default: None.
            
            Returns:
            cal_dict (dict): Dictionary with the calibration configuration.
        """
        # Open the folder
        if folder is not None:
            old_folder = os.getcwd()
            os.chdir(folder)

        if filename == "Calibration_measurements.npz":
            data = np.load("Calibration_measurements.npz")
            cal_dict = dict(data) 
        else:
            cal_dict = SpectralPolarimeterCalibration.Create_Cal_Files(single = single, wavelengths = wavelengths)

            data = np.load("Step_0_" + filename + ".npz")
            cal_dict["S0"] = data["S0"]
            cal_dict["S0_error"] = data["S0_error"]
            cal_dict["S1"] = data["S1"]
            cal_dict["S1_error"] = data["S1_error"]
            
            data = np.load("Step_1a_" + filename + ".npz")
            cal_dict["I_step_1a"] = data["Iexp"]
            cal_dict["Angles_step_1a"] = data["angles"]
            
            data = np.load("Step_1b_" + filename + ".npz")
            cal_dict["I_step_1b"] = data["Iexp"]
            cal_dict["Angles_step_1b"] = data["angles"]
            
            data = np.load("Step_1c_" + filename + ".npz")
            cal_dict["I_step_1c"] = data["Iexp"]
            cal_dict["Angles_step_1c"] = data["angles"]
            
            data = np.load("Step_2a_" + filename + ".npz")
            cal_dict["I_step_2a"] = data["Iexp"]
            cal_dict["Angles_step_2a"] = data["angles"]

            data = np.load("Step_2b_" + filename + ".npz")
            cal_dict["I_step_2b"] = data["Iexp"]
            cal_dict["Angles_step_2b"] = data["angles"]

            data = np.load("Step_2c_" + filename + ".npz")
            cal_dict["I_step_2c"] = data["Iexp"]
            cal_dict["Angles_step_2c"] = data["angles"]

            data = np.load("Step_3_" + filename + ".npz")
            cal_dict["I_step_3"] = data["Iexp"]
            cal_dict["Angles_step_3"] = data["angles"]

            data = np.load("Step_4a_" + filename + ".npz")
            cal_dict["I_step_4a"] = data["Iexp"]
            cal_dict["Angles_step_4a"] = data["angles"]

            data = np.load("Step_4b_" + filename + ".npz")
            cal_dict["I_step_4b"] = data["Iexp"]
            cal_dict["Angles_step_4b"] = data["angles"]

            data = np.load("Step_4c_" + filename + ".npz")
            cal_dict["I_step_4c"] = data["Iexp"]
            cal_dict["Angles_step_4c"] = data["angles"]

            data = np.load("Step_5_" + filename + ".npz")
            cal_dict["I_step_5"] = data["Iexp"]
            cal_dict["Angles_step_5"] = data["angles"]

        # Return to the old folder
        if folder and old_folder:
            os.chdir(old_folder)
        
        # Return the dictionary
        return cal_dict
    
    def Print_Cal_Files(cal_dict, wavelength=None): 
        """Print the calibration parameters.
        
        Args:
            cal_dict (dict): Dictionary with the calibration configuration.
            wavelength (float): Wavelength of the calibration. Default: None.
        """
        if cal_dict['num_wavelengths'].size == 1: 
            idx = 0
        else: 
            if wavelength is not None: 
                idx = np.argmin(np.abs(cal_dict['wavelengths'] - wavelength))
            else: 
                print("Wavelength must be specified when num_wavelengths > 1")
                return
                    
        # Print the calibration parameters
        print("Parámetros de Calibración: ")
        if cal_dict['wavelengths'] is None:
            print("Longitud de onda: no definida")
        else: 
            print("Longitud de onda: {:.2f} nm".format(cal_dict['wavelengths'][idx]))

        print("Datos de la fuente: ")
        if cal_dict['num_wavelengths'].size == 1:
            print("\tIntensidad: {:.2f}".format(cal_dict['S0']))
            print("\tAzimuth: {:.2f}°".format(cal_dict['illum_az']/degrees))
            print("\tElipticidad: {:.2f}°".format(cal_dict['illum_el']/degrees))
            print("\tGrado de polarización: {:.2f}".format(cal_dict['illum_pol_degree']))

            print("Datos de P1, P2 y Pc: ")
            print("\tP1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P1_p1'],cal_dict['P1_p2'],cal_dict['P1_Ret']/degrees,cal_dict['P1_az']/degrees))
            print("\tP2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P2_p1'],cal_dict['P2_p2'],cal_dict['P2_Ret']/degrees,cal_dict['P2_az']/degrees))
            print("\tPc:\tp1: {:.2f}".format(cal_dict['Pc_p1']))
            
            print("Datos de R1, R2 y Rc: ")
            print("\tR1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R1_p1'],cal_dict['R1_p2'],cal_dict['R1_Ret']/degrees, cal_dict['R1_az']/degrees))
            print("\tR2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R2_p1'],cal_dict['R2_p2'],cal_dict['R2_Ret']/degrees, cal_dict['R2_az']/degrees))
            print("\tRc:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\toffset: {:.2f}°".format(cal_dict['Rc_p1'][0],cal_dict['Rc_p2'][0],cal_dict['Rc_Ret'][0]/degrees, cal_dict['Rc_offset'][0]/degrees))

        else:
            print("\tIntensidad: {:.2f}".format(cal_dict['S0'][idx]))
            print("\tAzimuth: {:.2f}°".format(cal_dict['illum_az'][idx]/degrees))
            print("\tElipticidad: {:.2f}°".format(cal_dict['illum_el'][idx]/degrees))
            print("\tGrado de polarización: {:.2f}".format(cal_dict['illum_pol_degree'][idx]))

            print("Datos de P1, P2 y Pc: ")
            print("\tP1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P1_p1'][idx],cal_dict['P1_p2'][idx],cal_dict['P1_Ret'][idx]/degrees,cal_dict['P1_az'][idx]/degrees))
            print("\tP2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P2_p1'][idx],cal_dict['P2_p2'][idx],cal_dict['P2_Ret'][idx]/degrees,cal_dict['P2_az'][idx]/degrees))
            print("\tPc:\tp1: {:.2f}\tp2: {:.2f}".format(cal_dict['Pc_p1'][idx],cal_dict['Pc_p2'][idx]))
            
            print("Datos de R1, R2 y Rc: ")
            print("\tR1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R1_p1'][idx],cal_dict['R1_p2'][idx],cal_dict['R1_Ret'][idx]/degrees, cal_dict['R1_az'][idx]/degrees))
            print("\tR2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R2_p1'][idx],cal_dict['R2_p2'][idx],cal_dict['R2_Ret'][idx]/degrees, cal_dict['R2_az'][idx]/degrees))
            print("\tRc:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\toffset: {:.2f}°".format(cal_dict['Rc_p1'][idx],cal_dict['Rc_p2'][idx],cal_dict['Rc_Ret'][idx]/degrees, cal_dict['Rc_offset'][idx]/degrees))
        return
        
    def Save_Cal_Files(folder=None, filename=None, cal_dict=None):
        """Function to save the calibration files.

        Args:
            folder (string): Folder where the calibration files are located. Default: None.
            filename (string): Name of the calibration files. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        
        Returns:
            None
        """ 
        # Open the folder
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)

        # Save the calibration files
        if filename and cal_dict:
            np.savez("Step_0_" + filename, S0=cal_dict["S0"], S0_error=cal_dict["S0_error"], S1 = cal_dict["S1"], S1_error = cal_dict["S1_error"])
            np.savez("Step_1a_" + filename, Iexp=cal_dict["I_step_1a"], angles=cal_dict["Angles_step_1a"])
            np.savez("Step_1b_" + filename, Iexp=cal_dict["I_step_1b"], angles=cal_dict["Angles_step_1b"])
            np.savez("Step_1c_" + filename, Iexp=cal_dict["I_step_1c"], angles=cal_dict["Angles_step_1c"])
            np.savez("Step_2a_" + filename, Iexp=cal_dict["I_step_2a"], angles=cal_dict["Angles_step_2a"])
            np.savez("Step_2b_" + filename, Iexp=cal_dict["I_step_2b"], angles=cal_dict["Angles_step_2b"])
            np.savez("Step_2c_" + filename, Iexp=cal_dict["I_step_2c"], angles=cal_dict["Angles_step_2c"])
            np.savez("Step_3_" + filename, Iexp=cal_dict["I_step_3"], angles=cal_dict["Angles_step_3"])
            np.savez("Step_4a_" + filename, Iexp=cal_dict["I_step_4a"], angles=cal_dict["Angles_step_4a"])
            np.savez("Step_4b_" + filename, Iexp=cal_dict["I_step_4b"], angles=cal_dict["Angles_step_4b"])
            np.savez("Step_4c_" + filename, Iexp=cal_dict["I_step_4c"], angles=cal_dict["Angles_step_4c"])
            np.savez("Step_5_" + filename, Iexp=cal_dict["I_step_5"], angles=cal_dict["Angles_step_5"])
            np.savez("Calibration_measurements_" + filename, **cal_dict)
        if cal_dict and not filename:
            np.savez("Calibration_measurements.npz", **cal_dict)

        # Return to the old folder
        if folder and old_folder:
            os.chdir(old_folder)
        return
    
    def Reduce_Cal_Files(cal_dict, N, intensities = True): 
        """Function to reduce the number of measurements in the calibration files.
        
        Args:
            cal_dict (dict): Dictionary with the calibration configuration.
            N (int): New number of measurements.
            intensities (bool): If True, the intensities are reduced. Default: True.
            
            Returns:
            cal_dict_new (dict): New dictionary with the calibration configuration.
        """
        cal_dict_new = cal_dict.copy()
        if cal_dict['num_wavelengths'].size == 1:
            print("Single wavelength, no reduction needed")
            return cal_dict
        else: 
            N_old = cal_dict['num_wavelengths'].size -1
            cal_dict_new['wavelengths'] = cal_dict['wavelengths'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S0'] = cal_dict['S0'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S0_error'] = cal_dict['S0_error'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1'] = cal_dict['S1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1_error'] = cal_dict['S1_error'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['illum_pol_degree'] = cal_dict['illum_pol_degree'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['illum_az'] = cal_dict['illum_az'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['illum_el'] = cal_dict['illum_el'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P1_p1'] = cal_dict['P1_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P1_p2'] = cal_dict['P1_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P1_Ret'] = cal_dict['P1_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P2_p1'] = cal_dict['P2_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P2_p2'] = cal_dict['P2_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P2_Ret'] = cal_dict['P2_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Pc_p1'] = cal_dict['Pc_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Pc_p2'] = cal_dict['Pc_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R1_p1'] = cal_dict['R1_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R1_p2'] = cal_dict['R1_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R1_Ret'] = cal_dict['R1_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R2_p1'] = cal_dict['R2_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R2_p2'] = cal_dict['R2_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R2_Ret'] = cal_dict['R2_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_p1'] = cal_dict['Rc_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_p2'] = cal_dict['Rc_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_Ret'] = cal_dict['Rc_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_offset'] = cal_dict['Rc_offset'][np.linspace(0,N_old,N,dtype=int)]
            if intensities: 
                cal_dict_new['I_step_1a'] = cal_dict['I_step_1a'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_1b'] = cal_dict['I_step_1b'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_1c'] = cal_dict['I_step_1c'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_2a'] = cal_dict['I_step_2a'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_2b'] = cal_dict['I_step_2b'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_2c'] = cal_dict['I_step_2c'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_3'] = cal_dict['I_step_3'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_4a'] = cal_dict['I_step_4a'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_4b'] = cal_dict['I_step_4b'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_4c'] = cal_dict['I_step_4c'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_5'] = cal_dict['I_step_5'][:,np.linspace(0,N_old,N,dtype=int)]

            return cal_dict_new

    
    def make_step(pol = None, cal_dict = None, step = None, single = False, verbose = False, mode = None):
        
        """Function to make a calibration step.

        Args:
            pol (SpectralPolarimeter): Spectral polarimeter object.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            step (string): Step to make. Default: None.
            single (bool): If True, only one measurement is made. Default: False.
            verbose (bool): If True, the function is verbose. Default: False.
            mode(bool): If True, the function changes measurement. Default: False.

        Returns:
            mode intensity: 
                mean (np.ndarray): Array with the mean intensity of the step.
                error (np.ndarray): Array with the error intensity of the step.

            mode manual or motor:
                angles (np.ndarray): Array with the angles of the step.
                Iexp (np.ndarray): Array with the intensities of the step.
        """

        # Initialize the variables
        Iexp = []
        
        # Step 0 : INITIAL LIGHT MEASUREMENT 
    
        if step == "0": 
            
            if mode == "intensity":
                for ind in range(cal_dict["N_measures"]): 
                    if single:
                        Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                    else: 
                        Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    time.sleep(cal_dict['Twait'])
                    print("Measurement {} of {} done".format(ind+1, cal_dict["N_measures"]), end='\r', flush=True)
                
                # Calculate the mean and error
                Iexp = np.array(Iexp) 
                mean = np.mean(Iexp, axis=0)
                error = np.std(Iexp, axis=0)
                
                # Verbose
                if verbose: 
                    if single: 
                        print("\nMean intensity: {:.2f}".format(mean))
                        print("Error intensity: {:.2f}".format(error))
                    else: 
                        plt.subplot(1,2,1)
                        plt.plot(cal_dict['wavelengths'],mean)
                        plt.title("\nMean intensity - S0")
                        plt.xlabel("Wavelength (nm)")
                        plt.ylabel("Intensity (a.u.)")
                        plt.subplot(1,2,2)
                        plt.plot(cal_dict['wavelengths'], error)
                        plt.title("\nError intensity - S0")
                        plt.xlabel("Wavelength (nm)")
                        plt.ylabel("Intensity (a.u.)")
                        
                return mean, error
            
            elif mode == "manual": 
                angles = np.zeros(100)
                # Make the measurements
                for ind in range(100):
                    ang = input('Angle (in degrees). Type "End" to close.')
                    try:
                        angles[ind] = float(ang)
                        if single:
                            Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                        else: 
                            Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    except:
                        if ang.lower() in ('fin', 'end', 'stop'):
                            angles = angles[:ind]
                            break
                        else:
                            print('Value {} not accepted'.format(ang))
                            
                Iexp = np.array(Iexp)
                angles = np.array(angles*degrees)
                
                return Iexp, angles
            
            elif mode == "motor":
                utils.percentage_complete()
                angles = np.zeros_like(cal_dict["angles"])
                # Make the measurements
                for ind, angle in enumerate(cal_dict["angles"]):
                    theta = np.array([angle, 0, 0, 0])
                    angles_aux = pol.motor.Move_Absolute(
                        pos=theta, waiting='busy', units="deg", move_time=0.5)
                    angles[ind] = angles_aux[0] * degrees
                    if single:
                        Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                    else: 
                        Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                        
                    utils.percentage_complete(ind+1, cal_dict["N_measures"])
            
                Iexp = np.array(Iexp)
                
                return Iexp, angles
            
            if mode is None: 
                print("'mode' parameter must be specified")
                return
            
        # Step 1a, 1b, 1c : POLARIZER MEASUREMENTS
        
        elif step == "1a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([angle, 0, 0, 0])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[0] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
                        
            Iexp = np.array(Iexp)
                        
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        elif step == "1b" or step == "1c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([0, 0, 0, angle])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[3] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
                        
            Iexp = np.array(Iexp)
                        
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
           
        
        # Step 2a - Malus Law to measure Pref and P1
        elif step == "2a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, angle, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 2b - Malus Law to measure Pref and P2
        elif step == "2b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]+cal_dict['R2_az']/degrees):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, angle, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 2c - Malus Law to measure P1 and P2
        elif step == "2c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            angle_2c = cal_dict["P0_az"]/degrees - 45 - cal_dict["P1_az"]/degrees + cal_dict["P2_az"]/degrees
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P0_az"]/degrees - 45, 0, angle, angle_2c])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        
        # Step 3 - Illumination analysis
        elif step == '3': 
            angles = utils.PSA_states_2_angles(S = utils.S_20)
            # Make the measurements
            utils.percentage_complete()
            for ind in range(len(angles[0])):
                theta = np.array((0, 0, angles[1][ind]+cal_dict['R2_az'], angles[0][ind] + cal_dict['P2_az']))
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="rad")
                #print(angles_aux)
                angles[1][ind] = angles_aux[2]
                angles[0][ind] = angles_aux[3]
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                utils.percentage_complete(ind, 20)
            Iexp = np.array(Iexp)
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles

        # Step 4a - Retarder R1 Mesaurement
        elif step == "4a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, angle, 0, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 4b - Retarder R1 - Rc Measurement
        elif step == "4b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]+cal_dict['R1_az']/degrees):
                theta = np.array([cal_dict["P1_az"]/degrees, angle, 0, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        elif step == "4c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            angle_2c = cal_dict["P0_az"]/degrees - 45 - cal_dict["P1_az"]/degrees + cal_dict["P2_az"]/degrees
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P0_az"]/degrees - 45, angle, 0, angle_2c])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 5 - Air Mueller Matrix
        elif step == '5': 
            angles = utils.calculate_polarimetry_angles(144, type='perfect')
            # Make the measurements
            utils.percentage_complete()
            for ind, angle in enumerate(angles):
                theta_0 = np.array((cal_dict['P1_az'],cal_dict['R1_az'],cal_dict['R2_az'],cal_dict['P2_az']))
                theta = theta_0 + angle
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="rad")
                angles[ind] = angles_aux 
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False))
                utils.percentage_complete(ind, 144)
            Iexp = np.array(Iexp)
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles

        else: 
            return("Error. Step not recognized")
      
    
    def model_step(par = None,  step = None,  angle = None, cal_dict = None): 
        """Function to model the calibration step.
        
        Args:
            step (string): Step to model. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            par (np.ndarray): Parameters of the model. Default: None.
            angle (np.ndarray): Array with the angles of the step. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        """
        if step == "polyfit":
            Imodel = model_polyfit(par, angle)
            return Imodel
        if step == "0a":
            Imodel = model_cos2(par, angle)
            return Imodel
        elif step == "0b":
            Imodel = model_cos2_2(par, angle)
            return Imodel              
        elif step == '2': 
            # Rename parameters
            # (P1_p2, P1_Ret, R2_p1, R2_p2, R2_Ret, R2_error) = par
            (R2_p1, R2_p2, R2_Ret, R2_error) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth= cal_dict['P0_az'] - cal_dict['P1_az'],
                ellipticity= 0,
                degree_pol=1)
            S1 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S1"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'] - R2_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P2_az'])
            
            # Step 2a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_2a"], keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S1)))
            I_2a = Sf.parameters.intensity()
            
            # Step 2c
            angle_2c = cal_dict['P0_az'] - 45*degrees 
            Mp2_rot = Mp2.rotate(angle = angle_2c-cal_dict['P1_az']+cal_dict['P2_az'], keep=True)
            Mp1_rot = Mp1.rotate(angle = angle_2c, keep=True)
            Mr2_rot = Mr2.rotate(angle = cal_dict["Angles_step_2c"]+angle_2c -cal_dict['P1_az']+cal_dict['R2_az'] , keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S0)))
            I_2c = Sf.parameters.intensity()
            
            return I_2a, I_2c
        
        elif step == '2_global': 
            # Rename parameters
            (P1_p2, P1_Ret, R2_p1, R2_p2, R2_Ret, R2_error) = par
            # (R2_p1, R2_p2, R2_Ret, R2_error) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth= cal_dict['P0_az'] - cal_dict['P1_az'],
                ellipticity= 0,
                degree_pol=1)
            S1 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S1"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=P1_p2, R = P1_Ret, azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'] - R2_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=P1_p2, R = P1_Ret, azimuth=-cal_dict['P2_az'])
            
            # Step 2a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_2a"], keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S1)))
            I_2a = Sf.parameters.intensity()
            
            # Step 2c
            angle_2c = cal_dict['P0_az'] - 45*degrees 
            Mp2_rot = Mp2.rotate(angle = angle_2c-cal_dict['P1_az']+cal_dict['P2_az'], keep=True)
            Mp1_rot = Mp1.rotate(angle = angle_2c, keep=True)
            Mr2_rot = Mr2.rotate(angle = cal_dict["Angles_step_2c"]+angle_2c -cal_dict['P1_az']+cal_dict['R2_az'] , keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S0)))
            I_2c = Sf.parameters.intensity()
            
            return I_2a, I_2c

        elif step == '4': 
            # Rename parameters
            # (P1_p2, P1_Ret, R1_p1, R1_p2, R1_Ret, R1_error) = par
            (R1_p1, R1_p2, R1_Ret, R1_error) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth= cal_dict['P0_az'] - cal_dict['P1_az'],
                ellipticity= 0,
                degree_pol=1)
            S1 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S1"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P1_az'])
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'] - R1_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P2_az'])
            
            # Step 4a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_4a"], keep=True)
            Sf = (Mp2_rot * (Mr1_rot * (Mp1_rot * S1)))
            I_4a = Sf.parameters.intensity()
            
            # Step 4c
            angle_2c = cal_dict['P0_az'] - 45*degrees 
            Mp2_rot = Mp2.rotate(angle = angle_2c-cal_dict['P1_az']+cal_dict['P2_az'], keep=True)
            Mp1_rot = Mp1.rotate(angle = angle_2c, keep=True)
            Mr1_rot = Mr1.rotate(angle = cal_dict["Angles_step_2c"]+angle_2c -cal_dict['P1_az']+cal_dict['R1_az'] , keep=True)
            Sf = (Mp2_rot * (Mr1_rot * (Mp1_rot * S0)))
            I_4c = Sf.parameters.intensity()
            
            return I_4a, I_4c

        elif step == '4_global': 
            # Rename parameters
            (P1_p2, P1_Ret, R1_p1, R1_p2, R1_Ret, R1_error) = par
            #(R1_p1, R1_p2, R1_Ret, R1_error) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S0"],
                azimuth= cal_dict['P0_az'] - cal_dict['P1_az'],
                ellipticity= 0,
                degree_pol=1)
            S1 = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S1"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=P1_p2, R = P1_Ret, azimuth=-cal_dict['P1_az'])
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'] - R1_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=P1_p2, R = P1_Ret, azimuth=-cal_dict['P2_az'])
            
            # Step 4a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_4a"], keep=True)
            Sf = (Mp2_rot * (Mr1_rot * (Mp1_rot * S1)))
            I_4a = Sf.parameters.intensity()
            
            angle_2c = cal_dict['P0_az'] - 45*degrees 
            Mp2_rot = Mp2.rotate(angle = angle_2c-cal_dict['P1_az']+cal_dict['P2_az'], keep=True)
            Mp1_rot = Mp1.rotate(angle = angle_2c, keep=True)
            Mr1_rot = Mr1.rotate(angle = cal_dict["Angles_step_2c"]+angle_2c -cal_dict['P1_az']+cal_dict['R1_az'] , keep=True)
            Sf = (Mp2_rot * (Mr1_rot * (Mp1_rot * S0)))
            I_4c = Sf.parameters.intensity()
            
            return I_4a, I_4c

        else: 
            return("Error. Step not recognized")
        
            
        
    def error_step(par = None, step = None, angle = None, Iexp = None, cal_dict = None):
        """Function to calculate the error for adjusting the model to the experimental data.
        
        Args:
            step (string): Step to model. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            par (np.ndarray): Parameters of the model. Default: None.
            angle (np.ndarray): Array with the angles of the step. Default: None.
            Iexp (np.ndarray): Array with the intensities of the step. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        """
        if step == "polyfit":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "polyfit", angle)
            dif = Iexp - Imodel
            return dif
        if step == "0a":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "0a", angle)
            dif = Iexp - Imodel
            return dif
        elif step == "0b":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "0b", angle)
            dif = Iexp - Imodel
            return dif
        elif step == "2":
            I_2a, I_2c = SpectralPolarimeterCalibration.model_step(par,'2',angle = None,cal_dict=cal_dict)

            E_2a = (cal_dict["I_step_2a"] - I_2a)
            E_2c = (cal_dict["I_step_2c"] - I_2c)
            
            Error = np.concatenate((E_2a, E_2c))
            return Error

        elif step == "2_global":
            I_2a, I_2c = SpectralPolarimeterCalibration.model_step(par,'2_global',angle = None,cal_dict=cal_dict)

            E_2a = (cal_dict["I_step_2a"] - I_2a)
            E_2c = (cal_dict["I_step_2c"] - I_2c)
            
            Error = np.concatenate((E_2a, E_2c))
            return Error
        
        elif step == "4":
            I_4a, I_4c = SpectralPolarimeterCalibration.model_step(par,'4',angle = None,cal_dict=cal_dict)

            E_4a = (cal_dict["I_step_4a"] - I_4a)
            E_4c = (cal_dict["I_step_4c"] - I_4c)
            
            Error = np.concatenate((E_4a, E_4c))
            return Error

        elif step == "4_global":
            I_4a, I_4c = SpectralPolarimeterCalibration.model_step(par,'4_global',angle = None,cal_dict=cal_dict)

            E_4a = (cal_dict["I_step_4a"] - I_4a)
            E_4c = (cal_dict["I_step_4c"] - I_4c)
            
            Error = np.concatenate((E_4a, E_4c))
            return Error

        else: 
            return("Error. Step not recognized")
        
    def analyze_step(step = None, cal_dict = None, single = False, Iexp = None, angles = None, verbose = True):
        """Function to analyze the calibration step."""
        
        if step == "polyfit":
            optm_result = []
            if single: 
                I = Iexp# /cal_dict['S0']
                par0 = [1,1,1]
                result = least_squares(SpectralPolarimeterCalibration.error_step,par0, args=("polyfit", angles, I) )
                optm_result.append(result.x)
            else:
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i]# /cal_dict['S0'][i]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, args=("polyfit", angles, I[:,i]))
                    optm_result.append(result.x)
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")
            if single: 
                # Plot result
                optm_result = optm_result[0]

                Imodel = model_polyfit(optm_result, angles)
                if verbose: 
                    utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Polyfit')
                # Print results
                print('The values obtained are:')
                print('   - Coefficients: {}'.format(optm_result))

            return np.array(optm_result)
            

        if step == "0a": 
            optm_result = []
            if single: 
                I = Iexp# /cal_dict['S0']
                (Imax, Imin) = (np.max(I), np.min(I))
                bounds = ([0, 0, 0*degrees], [Imax,Imax, 180 * degrees])
                par0 = [np.max((0,Imin)), Imax , np.random.rand() * 90*degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("0a", angles, I), bounds=bounds)
                optm_result.append(result.x)
            else: 
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i]# /cal_dict['S0'][i]
                    (Imax, Imin) = (np.max(I[:,i]), np.min(I[:,i]))
                    bounds = ([0, 0, 0*degrees], [Imax,Imax, 180 * degrees])
                    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 90 *degrees]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("0a", angles, I[:,i]), bounds=bounds)
                    result.x[2] = result.x[2]%(180*degrees)
                    optm_result.append(result.x)
                    
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")

            if single: 
                # Plot result
                optm_result = optm_result[0]

                Imodel = model_cos2(optm_result, angles)

                utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Step 0a')
                # Print results
                print('The values obtained are:')
                print('   - Imax       : {:.4f} V'.format(optm_result[1] + optm_result[0]))
                print('   - Imin       : {:.4f} V'.format(optm_result[0]))
                print('   - Maximum angle : {:.2f} deg'.format(optm_result[2]/degrees))  
            else: 
                plt.plot(cal_dict['wavelengths'], np.array(optm_result)[:,2]/degrees, xlabel = 'Wavelengths (nm)', ylabel = 'Angle (deg)')
                print('The values obtained are:')
                print('   - Maximum angle : {:.2f} deg'.format(np.mean(np.array(optm_result)[:,2]/degrees)))  
            
            return np.array(optm_result)
        
        elif step == "0b": 
            optm_result = []

            if single: 
                I = Iexp # /cal_dict['S0']
                (Imax, Imin) = (np.max(I), np.min(I))
                bounds = ([0, 0, 0*degrees], [Imax, Imax, 90 * degrees])
                par0 = [np.max((0,Imin)), Imax - np.abs(Imin),  np.random.rand() * 45 *degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("0b", angles, I), bounds=bounds)
                optm_result.append(result.x)
            else: 
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i] # /cal_dict['S0'][i]
                    (Imax, Imin) = (np.max(I[:,i]), np.min(I[:,i]))
                    bounds = ([0, 0, -90*degrees], [Imax,Imax, 90 * degrees])
                    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 45 *degrees]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("0b", angles, I[:,i]), bounds=bounds)
                    result.x[2] = result.x[2]
                    optm_result.append(result.x)
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")

            if single: 
                # Plot result
                optm_result = optm_result[0]
                
                Imodel = model_cos2_2(optm_result, angles)
                
                utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Step 0b')
                # Print results
                print('The values obtained are:')
                print('   - Imax       : {:.4f} V'.format(optm_result[1] + optm_result[0]))
                print('   - Imin       : {:.4f} V'.format(optm_result[0]))
                print('   - Maximum angle : {:.2f} deg'.format(optm_result[2]/degrees))  
                
            else: 
                plt.plot(cal_dict['wavelengths'], np.array(optm_result)[:,2]/degrees, xlabel = 'Wavelengths (nm)', ylabel = 'Angle (deg)')
                print('The values obtained are:')
                print('   - Maximum angle : {:.2f} deg'.format(np.mean(np.array(optm_result)[:,2]/degrees))) 
                
            return np.array(optm_result)
        
        elif step == "2":
            optm_result = []
            bound_up = np.array([
                # 1,                  # P1_p2
                # 180*degrees,        # P1_Ret
                1,                  # R2_p1 
                1,                  # R2_p2  
                180*degrees,        # R2_Ret
                180*degrees,          # R2_az
            ])
            bound_down = np.array([
                # 0,                  # P1_p2
                # 0*degrees,          # P1_Ret
                0,                  # R2_p1 
                0,                  # R2_p2
                0,                  # R2_Ret 
                -180*degrees,         # R2_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares
            
            # par0 = (0,90*degrees,1,1,90*degrees,0*degrees)
            par0 = (1,1,90*degrees,0*degrees)

            if single: 
                args = ['2', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['2', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)

        elif step == "2_global":
            optm_result = []
            bound_up = np.array([
                1,                  # P1_p2
                180*degrees,        # P1_Ret
                1,                  # R2_p1 
                1,                  # R2_p2  
                180*degrees,        # R2_Ret
                15*degrees,          # R2_az
            ])
            bound_down = np.array([
                0,                  # P1_p2
                0*degrees,          # P1_Ret
                0,                  # R2_p1 
                0,                  # R2_p2
                0,                  # R2_Ret 
                -15*degrees,         # R2_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares
            
            par0 = (0,90*degrees,1,1,np.random.rand()*90*degrees,0*degrees)

            if single: 
                args = ['2_global', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['2_global', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)

        elif step == "3":
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])
            Mr2 = Mueller().diattenuator_retarder_linear(p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict["R2_az"])
            system = (Mr2, Mp2)
            
            S = Calculate_Stokes(I = Iexp, angles = angles, system = system)
            illum_az = S.parameters.azimuth(use_nan = False)
            illum_el = S.parameters.ellipticity_angle()
            illum_pol_degree = np.round(S.parameters.degree_polarization(),10)
            
            print('Azimuth: ', S.parameters.azimuth(use_nan = False)/degrees)
            print("Intensidad: ", S.parameters.intensity())
            print("Ángulo de elipticidad: ", S.parameters.ellipticity_angle()/degrees)
            print("Grado de polarización: ", np.round(S.parameters.degree_polarization(),10))
            
            optm_result = [illum_az, illum_el, illum_pol_degree]
            return np.array(optm_result)


        elif step == "4":
            optm_result = []
            bound_up = np.array([
                # 1,                  # P1_p2
                # 180*degrees,        # P1_Ret
                1,                  # R1_p1 
                1,                  # R1_p2  
                180*degrees,        # R1_Ret
                15*degrees,          # R1_az
            ])
            bound_down = np.array([
                # 0,                  # P1_p2
                # 0*degrees,          # P1_Ret
                0,                  # R1_p1 
                0,                  # R1_p2
                0,                  # R1_Ret 
                -15*degrees,         # R1_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares
            
            # par0 = (0,90*degrees,1,1,90*degrees,0*degrees)
            par0 = (1,1,90*degrees,0*degrees)

            if single: 
                args = ['4', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['4', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)

        elif step == "4":
            optm_result = []
            bound_up = np.array([
                1,                  # P1_p2
                180*degrees,        # P1_Ret
                1,                  # R1_p1 
                1,                  # R1_p2  
                180*degrees,        # R1_Ret
                15*degrees,          # R1_az
            ])
            bound_down = np.array([
                0,                  # P1_p2
                0*degrees,          # P1_Ret
                0,                  # R1_p1 
                0,                  # R1_p2
                0,                  # R1_Ret 
                -15*degrees,         # R1_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares
            
            par0 = (0,90*degrees,1,1,90*degrees,0*degrees)

            if single: 
                args = ['4_global', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['4_global', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)
        
        elif step == "5":
            
            Mp1 = Mueller().diattenuator_linear(p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict["P1_az"])
    
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R1_p1"], p2=cal_dict["R1_p2"], R=cal_dict["R1_Ret"], azimuth=-cal_dict["R1_az"])
            
            Mp2 = Mueller().diattenuator_linear(p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])
            
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict["R2_az"])
            
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S1"],
                azimuth=cal_dict["illum_az"],
                ellipticity=cal_dict["illum_el"],
                degree_pol=cal_dict["illum_pol_degree"])
            
            system = [S, Mp1, Mr1, Mr2, Mp2]
            
            M,_ = Calculate_Mueller(I=Iexp, angles=angles, system=system, filter=False)
              
            print("The Mueller matrix is:\n", M)

            return M
        
        else:
            return "Error. Step not recognized"
    
    def Process_Iterative_Calibration():
        pass
    def Process_Simultaneous_Calibration():
        pass
    


############################################################################################################################################################################

# SPECTRAL POLARIMETER CLASS

class SpectralPolarimeter(object):
    
    """
    Class for spectral polarimeter. 
    It will include control of the rotary motors of PSG and PSA elements, the spectrometer for measuring signal.

    Args:
        calibration (string): Load all calibration files ("full"), only the corresponding to illumination, PSG and PSA ("minimal") or none ("none"). Default: "full".
        conf_dict (dict): Dictionary with the calibration configuration. Default: CONF_POLARIMETER.
        name_motor (string): Name of the rotating motors used. Default: "DT50".
        name_spectrometer (string): Name of the spectrometer used. Default: "AVANTES".

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        daca (DACA): Data acquisition card object.
        norm (float): Intensity normalization constant.
        I_Distribution (np.ndarray): Intensity normalization array.
    """
    def __init__(self, calibration="full", conf_dict=CONF_POLARIMETER, name_motor="DT50", name_spectrometer="Avantes"): 

        # Initialize the objects
        self.motor = Motor_Multiple(name=name_motor, N=4)
        self.spectrometer = Spectrometer(name=name_spectrometer)
        
        self.name_motor = name_motor 
        self.name_spectrometer = name_spectrometer

        # # Load calibration info
        if calibration.lower() == "full":
             self.Load_Calibration(filename=conf_dict['cal_file'], BS_ref=conf_dict['BS_ref'], BS_trans=conf_dict['BS_trans'], ref_mirror=conf_dict['ref_mirror'], folder=conf_dict["cal_folder"], objective = conf_dict['objective_trans'])

    def Open(self): 

        """Open all devices."""

        # Motor configuration
        if self.name_motor == "DT50":
            config_motor = CONF_DT_50
        
        elif self.name_motor == "InteliDrives":
            config_motor = CONF_INTEL

        elif self.name_motor == "Zaber":
            config_motor = CONF_ZABER
        
        if self.name_spectrometer == "Avantes":
            config_spectrometer = CONF_AVANTES
        
        # Open the motor
        self.motor.Open(port=config_motor["ports"], invert=config_motor["invert"],axis=config_motor["axes"])
        self.motor.Home()
        print('\n')
        
        # Open the spectrometer
        self.spectrometer.Open()
        self.spectrometer.Get_Wavelength()
        self.spectrometer.Get_Number_Px()
        self.spectrometer.Set_Parameters(exposure = config_spectrometer["exposure"], N_average= config_spectrometer["N_average"])   

    def Load_Calibration(): 
        pass
    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=np.zeros(4), units="rad")
        pass
    def Close(self):
        """Close all devices."""
        self.motor.Close()
        self.spectrometer.Close()
        
    def Set_Property():
        pass
    
    def Measure_ND_Mueller_Matrix():
        pass


############################################################################################################################################################################

# SPECTRAL POLARIMETER UTILS CLASS

# class SpectralPolarimeter_utils(object):
    
#     """
#     Class for spectral polarimeter utilities. 
        
#     Args:
        

#     Atributes:
        
#     """

#     def Draw_Mueller_Matrix(): 
#         pass
#     def Calculate_Mueller_Matrix(): 
#         pass
#     def MSystem_from_dict():
#         pass   
#     def Calculate_Stokes():
#         pass
#     def Analyze_Mueller_Matrix():
#         pass
    
    ############################################################################################################################################################################


# OTHER FUNCTIONS


def model_cos2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2
    return Imodel

def error_cos2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2(par, angle)
    dif = Iexp - Imodel
    return dif

def model_cos2_2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(2*(angle - par[2]))**2
    return Imodel

def error_cos2_2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2_2(par, angle)
    dif = Iexp - Imodel
    return dif

def model_polyfit(par, angle):
    """Function polyfit with parameters."""
    Imodel = par[0] * angle**2 + par[1] * angle + par[2]
    return par[0] * angle**2 + par[1] * angle + par[2]

def error_polyfit(par, angle, Iexp):
    """Function that serves as optimization for polyfit."""
    Imodel = model_polyfit(par, angle)
    dif = Iexp - Imodel
    return dif

def draw_values(param_x, param_y, values, param_x_name='Param_X', param_y_name='Param_Y', values_name='Values'):
    """Draw the values in 3d projection plot of the polarimeter."""
    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.meshgrid(param_x, param_y[:, np.newaxis])
    ax.plot_surface(X, Y, values, cmap='viridis')
    ax.set_xlabel(param_x_name)
    ax.set_ylabel(param_y_name)
    ax.set_zlabel(values_name)
    return 

def create_temp_dict(cal_dict, value):
    """Create a dictionary with the same keys as cal_dict but with the value given."""
    cal_dict_temp = cal_dict.copy()
    cal_dict_temp['wavelengths'] = cal_dict['wavelengths'][value]
    cal_dict_temp['S0'] = cal_dict['S0'][value]
    cal_dict_temp['S0_error'] = cal_dict['S0_error'][value]
    cal_dict_temp['S1'] = cal_dict['S1'][value]
    cal_dict_temp['S1_error'] = cal_dict['S1_error'][value]
    cal_dict_temp['illum_pol_degree'] = cal_dict['illum_pol_degree'][value]
    cal_dict_temp['illum_az'] = cal_dict['illum_az'][value]
    cal_dict_temp['illum_el'] = cal_dict['illum_el'][value]
    cal_dict_temp['P1_p1'] = cal_dict['P1_p1'][value]
    cal_dict_temp['P1_p2'] = cal_dict['P1_p2'][value]
    cal_dict_temp['P1_Ret'] = cal_dict['P1_Ret'][value]
    cal_dict_temp['P2_p1'] = cal_dict['P2_p1'][value]
    cal_dict_temp['P2_p2'] = cal_dict['P2_p2'][value]
    cal_dict_temp['Pc_p1'] = cal_dict['Pc_p1'][value]
    cal_dict_temp['Pc_p2'] = cal_dict['Pc_p2'][value]
    cal_dict_temp['R1_p1'] = cal_dict['R1_p1'][value]
    cal_dict_temp['R1_p2'] = cal_dict['R1_p2'][value]
    cal_dict_temp['R1_Ret'] = cal_dict['R1_Ret'][value]
    cal_dict_temp['R2_p1'] = cal_dict['R2_p1'][value]
    cal_dict_temp['R2_p2'] = cal_dict['R2_p2'][value]
    cal_dict_temp['R2_Ret'] = cal_dict['R2_Ret'][value]
    cal_dict_temp['Rc_p1'] = cal_dict['Rc_p1'][value]
    cal_dict_temp['Rc_p2'] = cal_dict['Rc_p2'][value]
    cal_dict_temp['Rc_Ret'] = cal_dict['Rc_Ret'][value]
    cal_dict_temp['Rc_offset'] = cal_dict['Rc_offset'][value]
    cal_dict_temp['I_step_2a'] = cal_dict['I_step_2a'][:,value]
    cal_dict_temp['I_step_2b'] = cal_dict['I_step_2b'][:,value]
    cal_dict_temp['I_step_2c'] = cal_dict['I_step_2c'][:,value]
    cal_dict_temp['I_step_3a'] = cal_dict['I_step_3a'][:,value]
    cal_dict_temp['I_step_3b'] = cal_dict['I_step_3b'][:,value]
    cal_dict_temp['I_step_4'] = cal_dict['I_step_4'][:,value]
    cal_dict_temp['I_step_5a'] = cal_dict['I_step_5a'][:,value]
    cal_dict_temp['I_step_5b'] = cal_dict['I_step_5b'][:,value]
    cal_dict_temp['I_step_6'] = cal_dict['I_step_6'][:,value]
    return cal_dict_temp
    
def Calculate_Stokes(I, angles, system):
    """Function to calculate the Mueller matrix from the intensity measurements.

    Args:
        I (np.array): First dimension is the corresponding to different measurements.
        angles (Nx2 np.array): Angles array.
        system (list) List with the Stokes and Mueller objects for illumination, R2 and P2.

    Returns:
        result (Stokes): Result.
    """
    
    R2, P2 = system
    Mr2_rot = R2.rotate(angle=angles[1], keep=True)
    Mp2_rot = P2.rotate(angle=angles[0], keep=True)
    
    PSA = Mp2_rot * Mr2_rot
    a = PSA.M[0,:,:].T
    
    at = a.T
    ai = np.linalg.inv(at @ a) @ at
    s = np.dot(ai,I[:, np.newaxis])

    S = Stokes()
    
    S.from_components(components=s)
    
    S = S.analysis.filter_physical_conditions(tol = 1e-10)

    return S

    
    