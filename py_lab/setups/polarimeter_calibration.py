import numpy as np
from scipy.optimize import least_squares
# import pyswarms as ps
from pyswarms.single.global_best import GlobalBestPSO
from copy import deepcopy
import matplotlib.pyplot as plt
import datetime
import os
import time
from drawnow import drawnow
from scipy.signal import fftconvolve

from py_pol.mueller import Mueller
from py_pol.stokes import Stokes
from py_lab.utils import calculate_polarimetry_angles

import py_lab.utils as utils
from py_lab.config import degrees, CONF_DT_50, CONF_U6
from py_lab.setups.polarimeter_utils import (Calculate_Mueller, Calculate_Mueller_Matrix_0D,
                                             Msystem_From_Dict)

options_individual = {'c1': 0.5, 'c2': 0.3, 'w':0.9}
options_global = {'c1': 0.5, 'c2': 0.3, 'w':0.9}
PSO_part_1D = 50
PSO_iter_1D = 50
PSO_cores_1D = 7
PSO_part_2D = 50
PSO_iter_2D = 50
PSO_cores_2D = None
PSO_part_global = 100
PSO_iter_global = 200
PSO_cores_global = None
tol = 1e-11

sign_turn = 1    # Due to motor 2 and 3 rotating counter-clockwise. Fix in the future

def model_cos(par, angle):
    """Funcion cos with parameters."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])
    return Imodel


def error_cos(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos(par, angle)
    dif = Iexp - Imodel
    return dif

def model_cos2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2
    return Imodel


def error_cos2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2(par, angle)
    dif = Iexp - Imodel
    return dif



def model_cos2_2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(2*(angle - par[2]))**2
    return Imodel


def error_cos2_2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2_2(par, angle)
    dif = Iexp - Imodel
    return dif

def make_step_0d(pol, units="deg", angles_def=None, Iexp=None):
    """Step 0d: find angle of P0"""
    # Get angles and powers
    if angles_def is None or Iexp is None:
        angles_def, Iexp = utils.Tomar_Medidas(pol.daca)
    print(Iexp)

    # Fit
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(1.2*Imin), 1.2*Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds)
    par1 = result.x

    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 0d')
    # Print results
    Imax = par1[1] + par1[0]
    el = np.arctan(par1[0] / Imax)
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(Imax))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Ellipt. laser : {:.1f} deg'.format(el/degrees))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
    print("\nThe correct angle for P0 is {:.2f} deg".format(par1[2]/degrees))

    return angles_def, Iexp



def make_step_0e_1(pol, num_data=60, max_angle=180, motor_num=3):
    """First part of step 0e: find angle of polarizer at Motor N"""
    # Create array of angles that will be used
    var_angle = np.linspace(0, max_angle, num_data) * degrees
    # Initialize data
    angles_def = np.zeros(num_data)
    Iexp = np.zeros(num_data)

    # Make the loop
    utils.percentage_complete()
    for ind, angle in enumerate(var_angle):
        angles_def[ind] = pol.motor.Move_Absolute(pos=angle, units="rad", axis=motor_num)[motor_num]
        Iexp[ind] = pol.daca.Get_Signal()
        utils.percentage_complete(ind + 1, num_data)
        # print(angles_def[ind]/degrees, Iexp[ind], end = "   ")

    # Fit to the function
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([-0.02, 0, 0], [np.abs(1.2*Imin), 1.2*Imax, 180 * degrees])
    par0 = [Imin, Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds)
    par1 = result.x
    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 0e part 1')
    angle_P = par1[2]/degrees+90
    # Print results
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
    print("\nThe correct angle for next part is {:.2f} deg".format(angle_P))

    return angle_P

def make_step_0e_2(pol, angle_P=0, motor_num=3, units="deg", angles_def=None, Iexp=None):
    """Second part of step 0e: find angle of Q0"""
    # Get angles and powers
    if angles_def is None or Iexp is None:
        pol.motor.Move_Absolute(pos=angle_P, units=units, axis=motor_num)
        angles_def, Iexp = utils.Tomar_Medidas(pol.daca)

    # Fit
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [1.2*Imin, 1.2*Imax, 90 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 90*degrees]
    result = least_squares(error_cos2_2, par0, args=(angles_def, Iexp), bounds=bounds)
    par1 = result.x

    # Plot result
    Imodel = model_cos2_2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 0e part 2')
    # Print results
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Maximum angle : {:.2f} deg or {:.2f} deg'.format(par1[2]/degrees, par1[2]/degrees + 90))
    print("\nThe correct angle for next part is {:.2f} deg".format(par1[2]/degrees))

    return angles_def, Iexp


def make_step_0e_3(pol, num_data=60, max_angle=180, motor_num=3):
    """Step 0e: align R0. We make a loop with one motor with a polarizer to check the circularity of the illumination"""
    # Create array of angles that will be used
    var_angle = np.linspace(0, max_angle, num_data) * degrees
    # Initialize data
    angles_def = np.zeros(num_data)
    Iexp = np.zeros(num_data)

    # Make the loop
    utils.percentage_complete()
    for ind, angle in enumerate(var_angle):
        angles_def[ind] = pol.motor.Move_Absolute(pos=angle, units="rad", axis=motor_num)[motor_num]
        Iexp[ind] = pol.daca.Get_Signal()
        utils.percentage_complete(ind + 1, num_data)

    # Fit to the function
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [1.2*Imin, 1.2*Imax, 360 * degrees])
    par0 = [np.max((Imin,0)), Imax - Imin, np.random.rand() * 360*degrees]
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds)
    par1 = result.x
    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 0e part 1')
    angle_P = par1[2]/degrees+90
    # Print results
    Imax = par1[1] + par1[0]
    el = np.arctan(par1[0] / Imax)
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(Imax))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
    print('   - Ellipt. fuente : {:.1f} deg'.format(el/degrees))


def make_step_0f(pol, cal_dict, extra=None, verbose=False):
    """Function to make step 0f: photodetector stability."""
    # Make the measurement
    Iexp = np.zeros([cal_dict["Nmeasurements"], 2])
    utils.percentage_complete()
    for ind in range(cal_dict["Nmeasurements"]):
        Iexp[ind, :] = pol.daca.Get_Signal(return_ref=True, use_ref=False)
        time.sleep(cal_dict["Twait"])
        utils.percentage_complete(ind + 1, cal_dict["Nmeasurements"])

    # Correct if filter is present
    try:
        trans = cal_dict['P_filter'] / cal_dict['P_no_filter']
        Iexp[:, 0] /= trans
    except:
        cal_dict['P_filter'] = 1
        cal_dict['P_no_filter'] = 1

    # Make stadistics
    ratio_individual = Iexp[:, 0] / Iexp[:, 1]
    mean = np.mean(Iexp, axis=0)
    error = np.std(Iexp, axis=0)
    ratio2 = np.mean(ratio_individual)
    ratio_error2 = np.std(ratio_individual)
    error_correction = np.sqrt((error[0] / mean[0])**2 +
                               2 * (error[1] / mean[1])**2)
    # Fake plot data
    t = range(cal_dict["Nmeasurements"])
    meanCh1y = np.ones(cal_dict["Nmeasurements"]) * mean[0]
    meanCh1yUp = np.ones(cal_dict["Nmeasurements"]) * (mean[0] + error[0])
    meanCh1yDown = np.ones(cal_dict["Nmeasurements"]) * (mean[0] - error[0])
    meanCh2y = np.ones(cal_dict["Nmeasurements"]) * mean[1]
    meanCh2yUp = np.ones(cal_dict["Nmeasurements"]) * (mean[1] + error[1])
    meanCh2yDown = np.ones(cal_dict["Nmeasurements"]) * (mean[1] - error[1])
    meanRy2 = np.ones(cal_dict["Nmeasurements"]) * ratio2
    meanRyUp2 = np.ones(cal_dict["Nmeasurements"]) * (ratio2 + ratio_error2)
    meanRyDown2 = np.ones(cal_dict["Nmeasurements"]) * (ratio2 - ratio_error2)
    # Plot it
    plt.figure(figsize=(24, 4))
    plt.subplot(1, 3, 1)
    plt.plot(t, Iexp[:, 0] / meanCh1y, 'k')
    plt.plot(t, meanCh1y / meanCh1y, 'b')
    plt.plot(t, meanCh1yUp / meanCh1y, 'r--')
    plt.plot(t, meanCh1yDown / meanCh1y, 'r--')
    plt.title('Normalized PhD 1 (Signal)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 2)
    plt.plot(t, Iexp[:, 1] / meanCh2y, 'k')
    plt.plot(t, meanCh2y / meanCh2y, 'b')
    plt.plot(t, meanCh2yUp / meanCh2y, 'r--')
    plt.plot(t, meanCh2yDown / meanCh2y, 'r--')
    plt.title('Normalized PhD 2 (Reference)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 3)
    plt.plot(t, ratio_individual / meanRy2, 'k')
    plt.plot(t, meanRy2 / meanRy2, 'b')
    plt.plot(t, meanRyUp2 / meanRy2, 'r--')
    plt.plot(t, meanRyDown2 / meanRy2, 'r--')
    plt.title('Ratio PhD1 / PhD2')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    # Print result
    print('The resuts are:')
    print('   - Signal channel          : {:.4f} +- {:.4f} V.'.format(
        mean[0], error[0]))
    print('   - Reference channel       : {:.4f} +- {:.4f} V.'.format(
        mean[1], error[1]))
    print('   - Ratio error             : {:.2f} %.'.format(
        ratio_error2 * 100))
    print('   - Error in corrected I    : {:.2f} %.'.format(
        error_correction * 100))

    # Save data
    filename = "Step_0f_{}".format(datetime.date.today())
    np.savez(
        filename + cal_dict["extra_name"] + '.npz',
        Nmeasurements=cal_dict["Nmeasurements"],
        Twait=cal_dict["Twait"],
        Iexp=Iexp,
        mean=mean,
        error=error,
        P_filter=cal_dict['P_filter'],
        P_no_filter=cal_dict['P_no_filter'],
        error_correction=error_correction,
        **CONF_DT_50, **CONF_U6)
    # Output
    return ratio2, ratio_error2


def model_step_1(par, th1, Mp1, S):
    # Crear objetos
    Mpc = Mueller().diattenuator_linear(p1=np.max(par[0:2]), p2=np.min(par[0:2]), azimuth=0)
    Mp1_rot = Mp1.rotate(angle=th1 - par[2], keep=True)
    # Calcular
    Sf = Mp1_rot * (Mpc * S)
    return Sf.parameters.intensity()


def error_step_1(par, th1, Mp1, S, Iexp):
    Imodel = model_step_1(par, th1, Mp1, S)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_1(cal_dict, pol=None, extra = None, verbose=False, fit=True):
    """Function that performs step 1 of calibration: origin of P1_temp."""
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_1"]
        if np.min(Iexp)<0: 
            Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_1"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind] = angles_aux[3] * degrees
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_1"] = Iexp
        cal_dict["Angles_step_1"] = Angles_def

    # Test objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    Mp1 = Mueller().diattenuator_linear(
        p1=1, p2=0, azimuth=0)
        # p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=0)

    # Fit
    if fit:
        bounds = ([0, 0, 0* degrees], [1, 1, 180 * degrees])
        args = (Angles_def, Mp1, S, Iexp)
        # PSO
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_1D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_1D, verbose=verbose, n_processes=PSO_cores_1D, fun=error_step_1, args=args)
        # Least squares
        result = least_squares(
            error_step_1, result, args=args, bounds=bounds, ftol=tol, xtol=tol, gtol=tol)
        result = result.x

        cal_dict["Pc_p1"] = result[0]
        cal_dict["Pc_p2"] = result[1]
        cal_dict["P1_az_temp"] = result[2]

    # Plot results, 1D
    if verbose:
        Imodel = model_step_1(
            [cal_dict["Pc_p1"], cal_dict["Pc_p2"], cal_dict["P1_az_temp"]],
            Angles_def, Mp1, S)
        utils.plot_experiment_residuals_1D(
            Angles_def, Iexp, Imodel, title='Step 1', xlabel='P1 angle (deg)')
        # Print results
        print('Preliminarr analysis first iteration:')
        print('   - Pref p1       : {:.3f}'.format(cal_dict["Pc_p1"]))
        print('   - Pref p2       : {:.3f}'.format(cal_dict["Pc_p2"]))
        print('   - P1 theta_0    : {:.1f} deg'.format(
            cal_dict["P1_az_temp"] / degrees))
        print('   - Max Intensity : {:.3f} a.u.'.format(Iexp.max()))

    # Save data
    if pol is not None:
        filename = "Step_1_{}".format(datetime.date.today())
        np.savez(filename + cal_dict["extra_name"] + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict


def model_step_2a(par, th1, Mp1, S0, degree_pol):
    # Crear objetos
    Mp1_rot = Mp1.rotate(angle=th1, keep=True)
    S = Stokes().general_azimuth_ellipticity(
        intensity=S0,
        azimuth=par[0],
        ellipticity=par[1],
        degree_pol=degree_pol)
    # Calcular
    Sf = Mp1_rot * S
    return Sf.parameters.intensity() 


def error_step_2a(par, th1, Mp1, S0, degree_pol, Iexp):
    Imodel = model_step_2a(par, th1, Mp1, S0, degree_pol)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_2a(cal_dict, pol=None, extra = None, verbose=False, fit=True):
    """Function that performs step 2a of calibration: azimuth and max ellipticity of illumination."""
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_2a"]
        Angles_def = cal_dict["Angles_step_2a"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind] = angles_aux[3] * degrees
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        # Save
        cal_dict["I_step_2a"] = Iexp
        cal_dict["Angles_step_2a"] = Angles_def

    # Test objects
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"],
        p2=cal_dict["P1_p2"],
        azimuth=-cal_dict["P1_az_temp"])

    # Fit
    args = (Angles_def, Mp1, cal_dict["S0"], cal_dict["illum_pol_degree"],
            Iexp)
    if fit:
        bounds = ([0, 0], [180 * degrees, 90 * degrees])
        # PSO
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_1D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_1D, verbose=verbose, n_processes=PSO_cores_1D, fun=error_step_2a, args=args)
        # least squares
        result = least_squares(error_step_2a, result, args=args, bounds=bounds, ftol=tol, xtol=tol, gtol=tol)
        result = result.x

        cal_dict["illum_az"], cal_dict["illum_el_max"] = result

    # Plot results, 1D
    if verbose:
        Imodel = model_step_2a(
            [cal_dict["illum_az"], cal_dict["illum_el_max"]], *args[:-1])
        utils.plot_experiment_residuals_1D(
            Angles_def, Iexp, Imodel, title='Step 2a', xlabel='P1 angle (deg)')
        # Print results
        print('Preliminarr analysis first iteration:')
        print('   - Illumination azimuth       : {:.1f} deg'.format(
            cal_dict["illum_az"] / degrees))
        print('   - Max. illum. ellipticity    : {:.1f} deg'.format(
            cal_dict["illum_el_max"] / degrees))
        print('   - Max Intensity : {:.3f} a.u.'.format(Iexp.max()))

    # Save data
    if pol is not None:
        filename = "Step_2a_{}".format(datetime.date.today())
        np.savez(filename + cal_dict["extra_name"] + '.npz', angles_1=Angles_def, Iexp=Iexp, P_filter=cal_dict['P_filter'],
        P_no_filter=cal_dict['P_no_filter'], **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict


# def model_step_2b(par, th1, Mp1, p1, p2, S0, az, degree_pol):
#     # Crear objetos
#     Mp1_rot = Mp1.rotate(angle=th1, keep=True)
#     Mrc = Mueller().diattenuator_retarder_linear(
#         p1=p1, p2=p2, R=par[0], azimuth=45 * degrees - par[1])
#     S = Stokes().general_azimuth_ellipticity(
#         intensity=S0, azimuth=az, ellipticity=par[2], degree_pol=degree_pol)
#     # Calcular
#     Sf = Mp1_rot * (Mrc * S)
#     return Sf.parameters.intensity()
#
#
# def error_step_2b(par, th1, Mp1, p1, p2, S0, az, degree_pol, Iexp):
#     Imodel = model_step_2b(par, th1, Mp1, p1, p2, S0, az, degree_pol)
#     diferencia = Imodel - Iexp
#     return diferencia
#
#
# def make_step_2b(cal_dict, pol=None, verbose=False, fit=True):
#     """Function that performs step 2a of calibration: origin of R1_temp."""
#     # If no polarimeter, take data from dictionary
#     if pol is None:
#         Iexp = cal_dict["I_step_2b"]
#         Angles_def = cal_dict["Angles_step_2b"]
#
#     else:
#         Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
#         Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
#         utils.percentage_complete()
#
#         for ind, angle in enumerate(cal_dict["angles_1"]):
#             theta = np.array([0, 0, 0, angle])
#             angles_aux = pol.motor.Move_Absolute(
#                 pos=theta, waiting='busy', units="deg")
#             Angles_def[ind] = angles_aux[3] * degrees
#             Iexp[ind] = pol.daca.Get_Signal()
#             utils.percentage_complete(ind, cal_dict["N_measures_1D"])
#
#         cal_dict["I_step_2b"] = Iexp
#         cal_dict["Angles_step_2b"] = Angles_def
#
#     # Test objectsb
#     Mp1 = Mueller().diattenuator_linear(
#         p1=cal_dict["P1_p1"],
#         p2=cal_dict["P1_p2"],
#         azimuth=-cal_dict["P1_az_temp"])
#
#     # Fit
#     args = (Angles_def, Mp1, cal_dict["Rc_p1"], cal_dict["Rc_p2"],
#             cal_dict["S0"], cal_dict["illum_az"], cal_dict["illum_pol_degree"],
#             Iexp)
#     if fit:
#         par0 = [
#             cal_dict["Rc_R"], cal_dict["Rc_offset"],
#             np.sign(cal_dict["illum_el"]) * cal_dict["illum_el_max"]
#         ]
#         bounds = ([0, -cal_dict["Rc_offset_max"], -45 * degrees],
#                   [180 * degrees, cal_dict["Rc_offset_max"], 45 * degrees])
#         result = least_squares(error_step_2b, par0, args=args, bounds=bounds)
#         cal_dict["Rc_R"], cal_dict["Rc_offset"], cal_dict[
#             "illum_el"] = result.x
#
#     # Plot results, 1D
#     if verbose:
#         Imodel = model_step_2b(
#             [cal_dict["Rc_R"], cal_dict["Rc_offset"], cal_dict["illum_el"]],
#             *args[:-1])
#         utils.plot_experiment_residuals_1D(
#             Angles_def, Iexp, Imodel, title='Step 2b', xlabel='P1 angle (deg)')
#         # Print results
#         print('Preliminarr analysis first iteration:')
#         print('   - Rc retardance              : {:.1f} deg'.format(
#             cal_dict["Rc_R"] / degrees))
#         print('   - Rc offset                  : {:.1f} deg'.format(
#             cal_dict["Rc_offset"] / degrees))
#         print('   - Illumination ellipticity   : {:.1f} deg'.format(
#             cal_dict["illum_el"] / degrees))
#         print('   - Illumination pol. degree   : {:.1f}'.format(
#             cal_dict["illum_pol_degree"]))
#         print('   - Max Intensity : {:.3f} a.u.'.format(Iexp.max()))
#
#     # Save data
#     if pol is not None:
#         filename = "Step_2b_{}".format(datetime.date.today())
#         np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp)
#
#     # Output
#     return cal_dict


def model_step_2b(par, th1, Mp1, S0, az, el, sign, degree_pol):
    # Crear objetos
    Mp1_rot = Mp1.rotate(angle=th1, keep=True)
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=par[2], p2=par[3], R=par[0], azimuth=45 * degrees - par[1])
    S = Stokes().general_azimuth_ellipticity(
        intensity=S0, azimuth=az, ellipticity=sign * el, degree_pol=degree_pol)
    # Calcular
    Sf = Mp1_rot * (Mrc * S)
    return Sf.parameters.intensity()*2.163/1.128


def error_step_2b(par, th1, Mp1, S0, az, el, sign, degree_pol, Iexp):
    Imodel = model_step_2b(par, th1, Mp1, S0, az, el, sign, degree_pol)
    diferencia = Imodel - Iexp
    return diferencia


def make_step_2b(cal_dict, pol=None, verbose=False, fit=True):
    """Function that performs step 2b of calibration: illumination elipticity sign."""
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_2b"]
    
        if np.min(Iexp)<0: 
            Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_2b"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind] = angles_aux[3] * degrees
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        # Correct if filter is present
        try:
            trans = cal_dict['P_filter'] / cal_dict['P_no_filter']
            Iexp /= trans
        except:
            cal_dict['P_filter'] = 1
            cal_dict['P_no_filter'] = 1
        
        # Save
        cal_dict["I_step_2b"] = Iexp
        cal_dict["Angles_step_2b"] = Angles_def

    # Test objects
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"],
        p2=cal_dict["P1_p2"],
        azimuth=-cal_dict["P1_az_temp"])

    # Fit
    if fit:
        # PSO Signo +
        args = (Angles_def, Mp1, cal_dict["S0"], cal_dict["illum_az"], cal_dict["illum_el_max"],
                1, cal_dict["illum_pol_degree"], Iexp)
        bounds = ([0*degrees, -cal_dict["Rc_offset_max"], 0, 0], [180 * degrees, cal_dict["Rc_offset_max"],1,1])
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_1D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_1D, verbose=verbose, n_processes=PSO_cores_1D, fun=error_step_2b, args=args)

        # PSO Signo -
        args2 = (Angles_def, Mp1, cal_dict["S0"], cal_dict["illum_az"], cal_dict["illum_el_max"], -1, cal_dict["illum_pol_degree"], Iexp)
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_1D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost2, result2 = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_1D, verbose=verbose, n_processes=PSO_cores_1D, fun=error_step_2b, args=args2)

        # Choose result
        if cost2 < cost:
            args = args2
            result = result2
            cal_dict["illum_el_sign"] = -1
        else:
            cal_dict["illum_el_sign"] = 1

        # Least squares
        result = least_squares(error_step_2b, result, args=args, bounds=bounds, ftol=tol, xtol=tol, gtol=tol)
        result = result.x

        cal_dict["Rc_R"], cal_dict["Rc_offset"], cal_dict["Rc_p1"], cal_dict["Rc_p2"] = result
        cal_dict["illum_el"] = cal_dict["illum_el_sign"] * cal_dict["illum_el_max"]

    else:
        args = (Angles_def, Mp1, cal_dict["S0"], cal_dict["illum_az"], cal_dict["illum_el_max"], cal_dict["illum_el_sign"], cal_dict["illum_pol_degree"], Iexp)

    # Plot results, 1D
    if verbose:
        Imodel = model_step_2b(
            [cal_dict["Rc_R"], cal_dict["Rc_offset"], cal_dict["Rc_p1"], cal_dict["Rc_p2"]],
            *args[:-1])
        utils.plot_experiment_residuals_1D(
            Angles_def, Iexp, Imodel, title='Step 2b', xlabel='P1 angle (deg)')
        # Print results
        print('Preliminarr analysis first iteration:')
        print('   - Illum. ellip. sign    : {:.0f}'.format(
            cal_dict["illum_el_sign"]))
        print('   - Rc retardance         : {:.1f} deg'.format(
            cal_dict["Rc_R"] / degrees))
        print('   - Rc offset             : {:.1f} deg'.format(
            cal_dict["Rc_offset"] / degrees))
        print('   - Rc p1                 : {:.3f} deg'.format(
            cal_dict["Rc_p1"]))
        print('   - Rc p2                 : {:.3f} deg'.format(
            cal_dict["Rc_p2"]))
        print('   - Max Intensity : {:.3f} a.u.'.format(Iexp.max()))

    # Save data
    if pol is not None:
        filename = "Step_2b_{}".format(datetime.date.today())
        np.savez(filename + cal_dict["extra_name"] + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)
    print(Iexp.max())
    print(Imodel.max())
    # Output
    return cal_dict


def make_step_3(cal_dict, pol, substep, verbose=False):
    """Function that performs step 3x of calibration: characterization of Pi."""
    # No fit so polarimeter must be present
    Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
    Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
    utils.percentage_complete()

    for ind, angle in enumerate(cal_dict["angles_1"]):
        theta = np.array([0, 0, 0, angle])
        angles_aux = pol.motor.Move_Absolute(
            pos=theta, waiting='busy', units="deg")
        Angles_def[ind] = angles_aux[3] * degrees
        Iexp[ind] = pol.daca.Get_Signal()
        utils.percentage_complete(ind, cal_dict["N_measures_1D"])

    cal_dict["I_step_3{}".format(substep)] = Iexp
    cal_dict["Angles_step_3{}".format(substep)] = Angles_def

    # Plot results, 1D
    if verbose:
        plt.figure(figsize=(6, 4))
        plt.plot(Angles_def / degrees, Iexp)
        plt.title("Step 3" + substep)
        plt.xlabel("Angle (deg)")
        plt.ylabel("Intensity (a.u.)")

    # Save data
    if pol is not None:
        filename = "Step_3{}_{}".format(substep, datetime.date.today())
        np.savez(filename + cal_dict["extra_name"] + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict


def model_step_3(par, th1_a, th1_b, th1_c, th0p1_temp, S):
    # Order parameters
    P1_p1, P1_p2, P2_p1, P2_p2, P3_p1, P3_p2, th0p1, th0p2, th0p3 = par
    # Create Mueller objects
    Mp1 = Mueller()
    Mp1.diattenuator_linear(p1=P1_p1, p2=P1_p2)
    Mp2 = Mueller()
    Mp2.diattenuator_linear(p1=P2_p1, p2=P2_p2)
    Mp3 = Mueller()
    Mp3.diattenuator_linear(p1=P3_p1, p2=P3_p2, azimuth=-th0p3)
    # First, P3 and P1
    Mp1_rot = Mp1.rotate(angle=th1_a - th0p1_temp, keep=True)
    Sf = Mp1_rot * (Mp3 * S)
    Ia = Sf.parameters.intensity()
    # Then, P3 and P2
    Mp2_rot = Mp2.rotate(angle=th1_b - th0p2, keep=True)
    Sf = Mp2_rot * (Mp3 * S)
    Ib = Sf.parameters.intensity()
    # Last, P1 and P2
    Mp1_rot = Mp1.rotate(angle=-th0p1, keep=True)
    Mp2_rot = Mp2.rotate(angle=th1_c - th0p2, keep=True)
    Sf = Mp2_rot * (Mp1_rot * S)
    Ic = Sf.parameters.intensity()
    # End
    return Ia, Ib, Ic


def error_step_3(par, th1_a, th1_b, th1_c, th0p1_temp, S, IexpA, IexpB, IexpC):
    Ia, Ib, Ic = model_step_3(par, th1_a, th1_b, th1_c, th0p1_temp, S)
    errA = IexpA - Ia
    errB = IexpB - Ib
    errC = IexpC - Ic
    return np.concatenate((errA, errB, errC))


def analysis_step_3(cal_dict, verbose=False, fit=True):
    """Makes the first analysis of step 3: measurement of polarizers"""
    # Test objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])

    # Fit
    I1 = cal_dict["I_step_3a"]
    if np.min(I1)<0:
        I1 = I1- np.min(I1)
    I2 = cal_dict["I_step_3b"]
    if np.min(I2)<0:
        I2 = I2- np.min(I2)
    I3 = cal_dict["I_step_3c"]

    if np.min(I3)<0:
        I3 = I3- np.min(I3)
    
    args = (cal_dict["Angles_step_3a"], cal_dict["Angles_step_3b"],
            cal_dict["Angles_step_3c"], cal_dict["P1_az_temp"], S,
            I1, I2,
            I3)
    if fit:
        bounds = ([0.5, 0, 0.5, 0, 0.5, 0, 0, 0, 0], [
            1, 0.5, 1, 0.5, 1, 0.5, 180 * degrees, 180 * degrees, 180 * degrees
        ])
        # PSO
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_2D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_2D, verbose=verbose, n_processes=PSO_cores_2D, fun=error_step_3, args=args)
        # least squares
        result = least_squares(error_step_3, result, args=args, bounds=bounds, ftol=tol, xtol=tol, gtol=tol)
        result = result.x

        (cal_dict["P1_p1"], cal_dict["P1_p2"], cal_dict["P2_p1"],
         cal_dict["P2_p2"], cal_dict["P3_p1"], cal_dict["P3_p2"],
         cal_dict["P1_az"], cal_dict["P2_az"], cal_dict["P3_az"]) = result

    # Print results
    if verbose:
        Imodel_a, Imodel_b, Imodel_c = model_step_3(
            (cal_dict["P1_p1"], cal_dict["P1_p2"], cal_dict["P2_p1"],
             cal_dict["P2_p2"], cal_dict["P3_p1"], cal_dict["P3_p2"],
             cal_dict["P1_az"], cal_dict["P2_az"], cal_dict["P3_az"]),
            *args[:-3])
        Imodel = np.concatenate((Imodel_a, Imodel_b, Imodel_c))
        Iexp = np.concatenate((cal_dict["I_step_3a"], cal_dict["I_step_3b"],
                               cal_dict["I_step_3c"]))
        angles = np.concatenate((cal_dict["Angles_step_3a"],
                                 cal_dict["Angles_step_3b"] + 180 * degrees,
                                 cal_dict["Angles_step_3c"] + 360 * degrees))
        utils.plot_experiment_residuals_1D(
            angles,
            Iexp,
            Imodel,
            title='Step 3',
            xlabel='Angle last pol. (deg)')
        print('Preliminary analysis:')
        print('   - P1 p1         : {:.3f}'.format(cal_dict["P1_p1"]))
        print('   - P1 p2         : {:.3f}'.format(cal_dict["P1_p2"]))
        print('   - P1 theta_0    : {:.1f} deg'.format(
            cal_dict["P1_az"] / degrees))
        print('   - P2 p1         : {:.3f}'.format(cal_dict["P2_p1"]))
        print('   - P2 p2         : {:.3f}'.format(cal_dict["P2_p2"]))
        print('   - P2 theta_0    : {:.1f} deg'.format(
            cal_dict["P2_az"] / degrees))
        print('   - P3 p1         : {:.3f}'.format(cal_dict["P3_p1"]))
        print('   - P3 p2         : {:.3f}'.format(cal_dict["P3_p2"]))
        print('   - P3 theta_0    : {:.1f} deg'.format(
            cal_dict["P3_az"] / degrees))

    # Output
    return cal_dict


# def model_step_4a(par, th1, Mp1, Mp2, Mrc, S, p1, p2, R):
#     # Crear objetos
#     Mp1 = Mueller().diattenuator_perfect(azimuth=par[1])
#     Mp2 = Mueller().diattenuator_perfect(azimuth=par[2])
#     Mrc = Mueller().quarter_waveplate(azimuth=(45-7.7) * degrees)
#     Mr2 = Mueller().quarter_waveplate(azimuth=th1 - par[0])
#     # Calcular
#     Sf = Mp2 * (Mr2 * (Mrc * (Mp1 * S)))
#     return Sf.parameters.intensity()

# def model_step_4a(par, th1, Mp1, Mp2, Mrc, S, p1, p2, R):
#     # Crear objetos
#     Mr2 = Mueller().diattenuator_retarder_linear(
#         p1=p1, p2=p2, R=R, azimuth=th1 - par[0])
#     Mrc_rot = Mrc.rotate(angle=-par[1], keep=True)
#     # Calcular
#     Sf = Mp2 * (Mr2 * (Mrc * (Mp1 * S)))
#     return Sf.parameters.intensity()
#
#
# def error_step_4a(par, th1, Mp1, Mp2, Mrc, S, p1, p2, R, Iexp):
#     Imodel = model_step_4a(par, th1, Mp1, Mp2, Mrc, S, p1, p2, R)
#     diferencia = Imodel - Iexp
#     return diferencia
#
#
# def make_step_4a(cal_dict, pol=None, verbose=False, fit=True):
#     """Function that performs step 2a of calibration: origin of R1_temp."""
#     # If no polarimeter, take data from dictionary
#     if pol is None:
#         Iexp = cal_dict["I_step_4a"]
#         Angles_def = cal_dict["Angles_step_4a"]
#         angle_P1 = cal_dict["P1_az_step_4a"]
#
#     else:
#         # Calculate angle of P1
#         angle_P1 = cal_dict["P1_az"] / degrees
#         Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
#         Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
#         Angle_P1_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
#         utils.percentage_complete()
#
#         for ind, angle in enumerate(cal_dict["angles_1"]):
#             theta = np.array([angle_P1, 0, angle, 0])
#             angles_aux = pol.motor.Move_Absolute(
#                 pos=theta, waiting='busy', units="deg")
#             Angles_def[ind] = angles_aux[2] * degrees
#             Angle_P1_def[ind] = angles_aux[0] * degrees
#             Iexp[ind] = pol.daca.Get_Signal()
#             utils.percentage_complete(ind, cal_dict["N_measures_1D"])
#
#         cal_dict["I_step_4a"] = Iexp
#         cal_dict["Angles_step_4a"] = Angles_def
#         cal_dict["P1_az_step_4a"] = Angle_P1_def
#         angle_P1 = Angle_P1_def
#
#     # Test objects
#     S = Stokes().general_azimuth_ellipticity(
#         intensity=cal_dict["S0"],
#         azimuth=cal_dict["illum_az"],
#         ellipticity=cal_dict["illum_el"],
#         degree_pol=cal_dict["illum_pol_degree"])
#     Mp1 = Mueller().diattenuator_retarder_linear(
#         p1=cal_dict["P1_p1"],
#         p2=cal_dict["P1_p2"],
#         R=cal_dict["P1_R"],
#         azimuth=angle_P1 - cal_dict["P1_az"])
#     Mp2 = Mueller().diattenuator_linear(
#         p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])
#     Mrc = Mueller().diattenuator_retarder_linear(
#         p1=cal_dict["Rc_p1"],
#         p2=cal_dict["Rc_p2"],
#         R=cal_dict["Rc_R"],
#         # azimuth=45 * degrees - cal_dict["Rc_offset"])
#         azimuth=45 * degrees)
#
#     # Fit
#     args = (Angles_def, Mp1, Mp2, Mrc, S, cal_dict["R2_p1"], cal_dict["R2_p2"],
#             cal_dict["R2_R"], Iexp)
#     if fit:
#         par0 = [cal_dict["R2_az"], cal_dict["Rc_offset"]]
#         bounds = ((0, -cal_dict["Rc_offset_max"]), (180 * degrees, cal_dict["Rc_offset_max"]))
#         result = least_squares(error_step_4a, par0, args=args, bounds=bounds)
#         cal_dict["R2_az"] = result.x[0]
#         cal_dict["Rc_offset"] = result.x[1]
#
#     # Plot results, 1D
#     if verbose:
#         par0 = [cal_dict["R2_az"], (np.random.rand(1)[0] - 1) * 2*cal_dict["Rc_offset_max"]]
#         Imodel = model_step_4a(par0, *args[:-1])
#         utils.plot_experiment_residuals_1D(
#             Angles_def, Iexp, Imodel, title='Step 4a', xlabel='P1 angle (deg)')
#         # Print results
#         print('Preliminarr analysis first iteration:')
#         print('   - R2 azimuth          : {:.1f} deg'.format(
#             cal_dict["R2_az"] / degrees))
#         print('   - Max Intensity       : {:.3f} a.u.'.format(Iexp.max()))
#         print(np.array(par0) / degrees)
#
#     # Save data
#     if pol is not None:
#         filename = "Step_4a_{}".format(datetime.date.today())
#         np.savez(
#             filename + '.npz',
#             angles_1=Angles_def,
#             Iexp=Iexp,
#             P1_az_step_4a=cal_dict["P1_az_step_4a"])
#
#     # Output
#     return cal_dict

def make_step_4a(cal_dict, pol=None, verbose=False, fit=True):
    """Function that performs step 2a of calibration: origin of R1_temp."""
    # Act only when necessary
    if pol is not None:
        Iexp = np.zeros(cal_dict["N_measures_2D"], dtype=float)
        Angles_1_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
        Angles_2_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
        utils.percentage_complete()

        for ind1, angle1 in enumerate(cal_dict["angles_2Y"]):
            for ind2, angle2 in enumerate(cal_dict["angles_2X"]):
                theta = np.array([angle1, 0, angle2, 0])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg")
                Angles_1_def[ind1, ind2] = angles_aux[0] * degrees
                Angles_2_def[ind1, ind2] = angles_aux[2] * degrees
                Iexp[ind1, ind2] = pol.daca.Get_Signal()
                utils.percentage_complete([ind1, ind2],
                                          cal_dict["N_measures_2D"])

        cal_dict["I_step_4a"] = Iexp
        cal_dict["Angles_1_step_4a"] = Angles_1_def
        cal_dict["Angles_2_step_4a"] = Angles_2_def

        # Save data
        filename = "Step_4a_{}".format(datetime.date.today())
        np.savez(
            filename + cal_dict["extra_name"] + '.npz',
            angles_1=Angles_1_def,
            angles_2=Angles_2_def,
            Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict


# def model_step_4b(par, th1, th2, Mp2, S, th0r1, p1, p2, th0p1):
#     # Crear objetos
#     Mr2 = Mueller().diattenuator_retarder_linear(
#         p1=par[0], p2=par[1], R=par[2], azimuth=th1 - th0r1)
#     Mp1 = Mueller().diattenuator_retarder_linear(
#         p1=p1, p2=p2, R=par[3], azimuth=th2 - th0p1)
#     # Calcular
#     Sf = Mp2 * (Mr2 * (Mp1 * S))
#     return Sf.parameters.intensity()
#
#
# def error_step_4b(par, th1, th2, Mp2, S, th0r1, p1, p2, th0p1, Iexp):
#     Imodel = model_step_4b(par, th1, th2, Mp2, S, th0r1, p1, p2, th0p1)
#     diferencia = Imodel - Iexp
# #     return diferencia.flatten()
#
#
# def make_step_4b(cal_dict, pol=None, verbose=False, fit=True):
#     """Function that performs step 2a of calibration: origin of R1_temp."""
#     # If no polarimeter, take data from dictionary
#     if pol is  None:
#         Iexp = cal_dict["I_step_4b"]
#         Angles_1_def = cal_dict["Angles_1_step_4b"]
#         Angles_2_def = cal_dict["Angles_2_step_4b"]
#
#     else:
#         # Calculate angle of P1
#         Iexp = np.zeros(cal_dict["N_measures_2D"], dtype=float)
#         Angles_1_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
#         Angles_2_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
#         utils.percentage_complete()
#
#         for ind1, angle1 in enumerate(cal_dict["angles_2Y"]):
#             for ind2, angle2 in enumerate(cal_dict["angles_2X"]):
#                 theta = np.array([0, 0, angle1, angle2])
#                 angles_aux = pol.motor.Move_Absolute(
#                     pos=theta, waiting='busy', units="deg")
#                 Angles_1_def[ind1, ind2] = angles_aux[2] * degrees
#                 Angles_2_def[ind1, ind2] = angles_aux[3] * degrees
#                 Iexp[ind1, ind2] = pol.daca.Get_Signal()
#                 utils.percentage_complete([ind1, ind2],
#                                           cal_dict["N_measures_2D"])
#
#         cal_dict["I_step_4b"] = Iexp
#         cal_dict["Angles_1_step_4b"] = Angles_1_def
#         cal_dict["Angles_2_step_4b"] = Angles_2_def
#
#     # Test objectsb
#     S = Stokes().general_azimuth_ellipticity(
#         intensity=cal_dict["S0"],
#         azimuth=cal_dict["illum_az"],
#         ellipticity=cal_dict["illum_el"],
#         degree_pol=cal_dict["illum_pol_degree"])
#     Mp2 = Mueller().diattenuator_linear(
#         p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])
#
#     # Fit
#     args = (Angles_1_def, Angles_2_def, Mp2, S, cal_dict["R2_az"],
#             cal_dict["P1_p1"], cal_dict["P1_p2"], cal_dict["P1_az"], Iexp)
#     if fit:
#         par0 = [
#             cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_R"],
#             cal_dict["P1_R"]
#         ]
#         bounds = ([0, 0, 0, 0], [1, 1, 180 * degrees, 360 * degrees])
#         result = least_squares(error_step_4b, par0, args=args, bounds=bounds)
#         cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_R"], cal_dict[
#             "P1_R"] = result.x
#
#     # Plot results, 1D
#     if verbose:
#         Imodel = model_step_4b([
#             cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_R"],
#             cal_dict["P1_R"]
#         ], *args[:-1])
#         utils.plot_experiment_residuals_2D(
#             Angles_1_def,
#             Angles_2_def,
#             Iexp,
#             Imodel,
#             title='Step 4b',
#             xlabel='P2 angle (deg)',
#             ylabel="R2 angle (deg)")
#         # Print results
#         print('Preliminarr analysis first iteration:')
#         print('   - R2 p1          : {:.1f}'.format(cal_dict["R2_p1"]))
#         print('   - R2 p2          : {:.1f}'.format(cal_dict["R2_p2"]))
#         print('   - R2 retardance  : {:.1f} deg'.format(
#             cal_dict["R2_R"] / degrees))
#         print('   - P1 retardance  : {:.1f} deg'.format(
#             cal_dict["P1_R"] / degrees))
#         print('   - Max Intensity  : {:.3f} a.u.'.format(Iexp.max()))
#
#     # Save data
#     if pol is not None:
#         filename = "Step_4b_{}".format(datetime.date.today())
#         np.savez(
#             filename + '.npz',
#             angles_1=Angles_1_def,
#             angles_2=Angles_2_def,
#             Iexp=Iexp)
#
#     # Output
#     return cal_dict


def make_step_4b(cal_dict, pol=None, verbose=False, fit=True):
    """Function that performs step 2a of calibration: origin of R1_temp."""
    # If no polarimeter, take data from dictionary
    if pol is not None:
        # Prealocate
        Iexp = np.zeros(cal_dict["N_measures_2D"], dtype=float)
        Angles_1_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
        Angles_2_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
        utils.percentage_complete()

        # Measure
        for ind1, angle1 in enumerate(cal_dict["angles_2Y"]):
            for ind2, angle2 in enumerate(cal_dict["angles_2X"]):
                theta = np.array([0, 0, angle1, angle2])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg")
                Angles_1_def[ind1, ind2] = angles_aux[2] * degrees
                Angles_2_def[ind1, ind2] = angles_aux[3] * degrees
                Iexp[ind1, ind2] = pol.daca.Get_Signal()
                utils.percentage_complete([ind1, ind2],
                                          cal_dict["N_measures_2D"])

        # Save data in calibration dictionary
        cal_dict["I_step_4b"] = Iexp
        cal_dict["Angles_1_step_4b"] = Angles_1_def
        cal_dict["Angles_2_step_4b"] = Angles_2_def

        # Save data in file
        filename = "Step_4b_{}".format(datetime.date.today())
        np.savez(
            filename + cal_dict["extra_name"] + '.npz',
            angles_1=Angles_1_def,
            angles_2=Angles_2_def,
            Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def analysis_step_4(cal_dict, verbose=False, fit=True):
    """Makes the first analysis of step 3: measurement of R2."""
    # Take data from dictionary
    Iexp_A = cal_dict["I_step_4a"]
    Angles_1_A = cal_dict["Angles_1_step_4a"]
    Angles_2_A = cal_dict["Angles_2_step_4a"]
    Iexp_B = cal_dict["I_step_4b"]
    Angles_1_B = cal_dict["Angles_1_step_4b"]
    Angles_2_B = cal_dict["Angles_2_step_4b"]

    # Test objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])

    # Fit
    args = (Angles_1_A, Angles_2_A, Angles_1_B, Angles_2_B, S, Mp2, cal_dict["P1_p1"], cal_dict["P1_p2"], cal_dict["P1_az"], cal_dict["Rc_p1"], cal_dict["Rc_p2"], Iexp_A, Iexp_B)
    if fit:
        bounds = ([0, 0, 0, 0*degrees, 0, 0*degrees, -cal_dict["Rc_offset_max"], -cal_dict["P1_offset_max"]], [360*degrees, 1, 1, 180 * degrees, 180 * degrees, 180*degrees, cal_dict["Rc_offset_max"], cal_dict["P1_offset_max"]])
        # PSO
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_2D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_2D, verbose=verbose, n_processes=PSO_cores_2D, fun=error_step_4, args=args)
        # least squares
        result = least_squares(error_step_4, result, args=args, bounds=bounds, ftol=tol, xtol=tol, gtol=tol)
        result = result.x

        cal_dict["P1_R"], cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_R"], cal_dict["R2_az"], cal_dict["Rc_R"], cal_dict["Rc_offset"], cal_dict["P1_offset"] = result

    # Plot results, 1D
    if verbose:
        par = [cal_dict["P1_R"],
            cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_R"], cal_dict["R2_az"], cal_dict["Rc_R"], cal_dict["Rc_offset"], cal_dict["P1_offset"] ]
        # par = [281*degrees, 1, 1, ]
        # par = result
        Imodel_A = model_step_4(par, Angles_1_A, Angles_2_A, S, Mp2, cal_dict["P1_p1"], cal_dict["P1_p2"], cal_dict["P1_az"], cal_dict["Rc_p1"], cal_dict["Rc_p2"], "a")
        utils.plot_experiment_residuals_2D(
            Angles_1_A,
            Angles_2_A,
            Iexp_A,
            Imodel_A,
            title='Step 4a',
            xlabel='P1 angle (deg)',
            ylabel="R2 angle (deg)")

        Imodel_B = model_step_4(par, Angles_1_B, Angles_2_B, S, Mp2, cal_dict["P1_p1"], cal_dict["P1_p2"], cal_dict["P1_az"], cal_dict["Rc_p1"], cal_dict["Rc_p2"], "b")
        utils.plot_experiment_residuals_2D(
            Angles_1_B,
            Angles_2_B,
            Iexp_B,
            Imodel_B,
            title='Step 4b',
            xlabel='R2 angle (deg)',
            ylabel="P2 angle (deg)")
        # Print results
        print('Preliminar analysis first iteration:')
        print('   - P1 Retardance     : {:.1f}'.format(cal_dict["P1_R"]))
        print('   - R2 p1             : {:.1f}'.format(cal_dict["R2_p1"]))
        print('   - R2 p2             : {:.1f}'.format(cal_dict["R2_p2"]))
        print('   - R2 retardance     : {:.1f} deg'.format(
            cal_dict["R2_R"] / degrees))
        print('   - R2 azimuth        : {:.1f} deg'.format(
            cal_dict["R2_az"] / degrees))
        print('   - Rc retardance     : {:.1f} deg'.format(
            cal_dict["Rc_R"] / degrees))
        print('   - Rc offset         : {:.1f} deg'.format(
            cal_dict["Rc_offset"] / degrees))
        print('   - P1 offset         : {:.1f} deg'.format(
            cal_dict["P1_offset"] / degrees))
        print('   - Max Intensity (a) : {:.3f} a.u.'.format(Iexp_A.max()))
        print('   - Max Intensity (b): {:.3f} a.u.'.format(Iexp_B.max()))

    # Output
    return cal_dict

def model_step_4(par, th1, th2, S, Mp2, p1, p2, az, Rc_p1, Rc_p2, substep):
    # Elements
    Mp1 = Mueller().diattenuator_retarder_linear(
        p1=p1,
        p2=p2,
        R=par[0],
        azimuth=-az-par[7])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=par[1],
        p2=par[2],
        R=par[3],
        azimuth=-par[4])
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=Rc_p1, p2=Rc_p2, R=par[5], azimuth=45 * degrees - par[6])
    # Rotate and calculate
    if substep == "a":
        Mp1 = Mp1.rotate(angle=th1, keep=True)
        Mr2 = Mr2.rotate(angle=th2, keep=True)
        Sf = Mp2 * Mr2 * Mrc * Mp1 * S
    else:
        Mr2 = Mr2.rotate(angle=th1, keep=True)
        Mp2 = Mp2.rotate(angle=th2, keep=True)
        Sf = Mp2 * Mr2 * (Mp1 * S)
    return Sf.parameters.intensity()

def error_step_4(par, th1_A, th2_A, th1_B, th2_B, S, Mp2, p1, p2, az, Rc_p1, Rc_p2, Iexp_A, Iexp_B):
    Imodel_A = model_step_4(par, th1_A, th2_A, S, Mp2, p1, p2, az, Rc_p1, Rc_p2, "a")
    diferencia_A = (Imodel_A - Iexp_A).flatten()

    Imodel_B = model_step_4(par, th1_B, th2_B, S, Mp2, p1, p2, az, Rc_p1, Rc_p2, "b")
    diferencia_B = (Imodel_B - Iexp_B).flatten()
    return np.concatenate((diferencia_A, diferencia_B))



def make_step_5(cal_dict, pol, substep):
    """Function that performs step 5 of calibration: calibration of R1."""
    # Calculate angle of P1
    angle_P1 = 0 if substep == "a" else 45
    Iexp = np.zeros(cal_dict["N_measures_2D"], dtype=float)
    Angles_1_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
    Angles_2_def = np.zeros(cal_dict["N_measures_2D"], dtype=float)
    utils.percentage_complete()

    for ind1, angle1 in enumerate(cal_dict["angles_2Y"]):
        for ind2, angle2 in enumerate(cal_dict["angles_2X"]):
            theta = np.array([angle_P1, 0, angle1, angle2])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_1_def[ind1, ind2] = angles_aux[2] * degrees
            Angles_2_def[ind1, ind2] = angles_aux[3] * degrees
            Iexp[ind1, ind2] = pol.daca.Get_Signal()
            utils.percentage_complete([ind1, ind2], cal_dict["N_measures_2D"])

    cal_dict["I_step_5" + substep] = Iexp
    cal_dict["Angles_1_step_5" + substep] = Angles_1_def
    cal_dict["Angles_2_step_5" + substep] = Angles_2_def

    # Save data
    filename = "Step_5{}_{}".format(substep, datetime.date.today())
    np.savez(
        filename + cal_dict["extra_name"] + '.npz',
        angles_1=Angles_1_def,
        angles_2=Angles_2_def,
        Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict


def model_step_5(par, th1, th2, Mp1, Mp2, Mr2, S):
    # Crear objetos
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=par[0], p2=par[1], R=par[2], azimuth=-par[3])
    Mr2_rot = Mr2.rotate(angle=th1, keep=True)
    Mp2_rot = Mp2.rotate(angle=th2, keep=True)
    # Calcular
    Sf = Mp2_rot * Mr2_rot * (Mr1 * (Mp1 * S))
    return Sf.parameters.intensity()


def error_step_5(par, th1_A, th2_A, th1_B, th2_B, Mp1, Mp2, Mr2, S, Iexp_A,
                 Iexp_B):
    Imodel_A = model_step_5(par, th1_A, th2_A, Mp1, Mp2, Mr2, S)
    diferencia_A = (Imodel_A - Iexp_A).flatten()
    Mp1_45 = Mp1.rotate(angle=45 * degrees, keep=True)
    Imodel_B = model_step_5(par, th1_B, th2_B, Mp1_45, Mp2, Mr2, S)
    diferencia_B = (Imodel_B - Iexp_B).flatten()
    return np.concatenate((diferencia_B, diferencia_B))


def analysis_step_5(cal_dict, verbose=False, fit=True):
    """Function that performs step 2a of calibration: origin of R1_temp."""
    # Take data from dictionary
    Iexp_A = cal_dict["I_step_5a"]
    Angles_1_A = cal_dict["Angles_1_step_5a"]
    Angles_2_A = cal_dict["Angles_2_step_5a"]
    Iexp_B = cal_dict["I_step_5b"]
    Angles_1_B = cal_dict["Angles_1_step_5b"]
    Angles_2_B = cal_dict["Angles_2_step_5b"]

    # Test objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    Mp1 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["P1_p1"],
        p2=cal_dict["P1_p2"],
        R=cal_dict["P1_R"],
        azimuth=-cal_dict["P1_az"])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"],
        p2=cal_dict["R2_p2"],
        R=cal_dict["R2_R"],
        azimuth=-cal_dict["R2_az"])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])

    # Fit
    args = (Angles_1_A, Angles_2_A, Angles_1_B, Angles_2_B, Mp1, Mp2, Mr2, S, Iexp_A, Iexp_B)
    if fit:
        bounds = ([0.8, 0.8, 80*degrees, 0], [1.05, 1.05, 100 * degrees, 180 * degrees])
        # PSO
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_2D, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO_ind, iters=PSO_iter_2D, verbose=verbose, n_processes=PSO_cores_2D, fun=error_step_5, args=args)
        # Least squares
        result = least_squares(error_step_5, result, args=args, bounds=bounds, ftol=tol, xtol=tol, gtol=tol)
        result = result.x

        cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_R"], cal_dict["R1_az"] = result

    # Plot results, 1D
    if verbose:
        Imodel_A = model_step_5([
            cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_R"],
            cal_dict["R1_az"]
        ], Angles_1_A, Angles_2_A, Mp1, Mp2, Mr2, S)
        utils.plot_experiment_residuals_2D(
            Angles_1_A,
            Angles_2_A,
            Iexp_A,
            Imodel_A,
            title='Step 5a',
            xlabel='P2 angle (deg)',
            ylabel="R2 angle (deg)")
        Imodel_B = model_step_5(
            [
                cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_R"],
                cal_dict["R1_az"]
            ],
            Angles_1_B,
            Angles_2_B,
            Mp1.rotate(angle=45 * degrees),
            Mp2,
            Mr2,
            S)
        utils.plot_experiment_residuals_2D(
            Angles_1_B,
            Angles_2_B,
            Iexp_B,
            Imodel_B,
            title='Step 5b',
            xlabel='P2 angle (deg)',
            ylabel="R2 angle (deg)")
        # Print results
        print('Preliminarr analysis first iteration:')
        print('   - R1 p1               : {:.1f}'.format(cal_dict["R1_p1"]))
        print('   - R1 p2               : {:.1f}'.format(cal_dict["R1_p2"]))
        print('   - R1 retardance       : {:.1f} deg'.format(
            cal_dict["R1_R"] / degrees))
        print('   - R1 azimuth          : {:.1f} deg'.format(
            cal_dict["R1_az"] / degrees))
        print('   - Max Intensity (0º)  : {:.3f} a.u.'.format(Iexp_A.max()))
        print('   - Max Intensity (45º) : {:.3f} a.u.'.format(Iexp_B.max()))

    # Output
    return cal_dict


def make_step_6(cal_dict,
                pol=None,
                method="intensity",
                verbose=False,
                tol=1e-9):
    """Function that performs step 2a of calibration: origin of R1_temp."""
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_6"]
        Angles_def = cal_dict["Angles_step_6"]

    else:
        # Calculate angles
        # angles = calculate_polarimetry_angles(cal_dict["N_measures_pol"], type="random")
        angles = np.random.rand(cal_dict["N_measures_pol"], 4) * 180
        pos_ini = pol.motor.Get_Position(units='deg')
        Angles_def = utils.sort_positions(angles, pos_ini)

        # Prealocate
        Iexp = np.zeros(cal_dict["N_measures_pol"])
        utils.percentage_complete()

        # Measure
        for ind in range(cal_dict["N_measures_pol"]):
            theta = Angles_def[ind, :]
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind, :] = angles_aux * degrees
            time.sleep(0.5)
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_pol"])

        cal_dict["I_step_6"] = Iexp
        cal_dict["Angles_step_6"] = Angles_def

    

    # Calculate model and final error
    Imodel = model_step_6(cal_dict, Angles_def)
    if method.lower() == "intensity":
        error = np.linalg.norm(Imodel - Iexp) / (Iexp.size * cal_dict["S0"])
    else:
        Ms = Msystem_From_Dict(cal_dict)
        M = Calculate_Mueller_Matrix_0D(cal_dict["I_step_6"],
                                        cal_dict["Angles_step_6"], Ms)
        error = np.linalg.norm(M.M - Mueller().vacuum().M) / 16
    cal_dict["Error_step_6"] = error

    # Plot results, 1D
    if verbose:
        utils.plot_experiment_residuals_1D(
            np.arange(Iexp.size),
            Iexp,
            Imodel,
            title='Step 6',
            xlabel='Measurement')
        # Print results
        print('Preliminarr analysis first iteration:')
        print('   - Error          : {:.3f}'.format(cal_dict["Error_step_6"]))
        print('   - Max Intensity  : {:.3f} a.u.'.format(Iexp.max()))
        if method.lower() != "intensity":
            print(M)
            print(np.round(M.M/M.M[0,0], decimals = 4))

    # Save data
    if pol is not None:
        filename = "Step_6_{}".format(datetime.date.today())
        np.savez(filename + cal_dict["extra_name"] + '.npz', angles=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    

    # Output
    return cal_dict


def model_step_6(cal_dict, Angles_def):
    """Model of the step 6 (performed just to see intensity errors)."""
    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    Mp1 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["P1_p1"],
        p2=cal_dict["P1_p2"],
        R=cal_dict["P1_R"],
        azimuth=Angles_def[:, 0] - cal_dict["P1_az"])
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R1_p1"],
        p2=cal_dict["R1_p2"],
        R=cal_dict["R1_R"],
        azimuth=Angles_def[:, 1] - cal_dict["R1_az"])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"],
        p2=cal_dict["R2_p2"],
        R=cal_dict["R2_R"],
        azimuth=Angles_def[:, 2] - cal_dict["R2_az"])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"],
        p2=cal_dict["P2_p2"],
        azimuth=Angles_def[:, 3] - cal_dict["P2_az"])
    # Calculate
    Sf = Mp2 * (Mr2 * (Mr1 * (Mp1 * S)))
    return Sf.parameters.intensity()


def Load_Cal_Files(folder=None, filename=None):
    # Ir al directorio
    if folder:
        old_folder = os.getcwd()
        os.chdir(folder)
    # If filename is defined, individual steps will be loaded
    if filename and filename != "Calibration_measurements.npz":
        cal_dict = generate_initial_cal_dict(method="random", experiment=False)
        data = np.load("Step_0f_" + filename + ".npz")
        cal_dict["S0"] = np.mean(data["Iexp"][:, 0] / data["Iexp"][:, 1])
        if sign_turn != 1:
            print("Rotation of motors corrected artificially")
        try:
            cal_dict['P_filter'] = data["P_filter"]
            cal_dict['P_no_filter'] = data["P_no_filter"]
        except:
            data = np.load("Calibration_measurements.npz")
            cal_dict['P_filter'] = data["P_filter"]
            cal_dict['P_no_filter'] = data["P_no_filter"]

        data = np.load("Step_1_" + filename + ".npz")
        cal_dict["I_step_1"] = data["Iexp"]
        cal_dict["Angles_step_1"] = sign_turn * data["angles_1"]

        data = np.load("Step_2a_" + filename + ".npz")
        cal_dict["I_step_2a"] = data["Iexp"]
        cal_dict["Angles_step_2a"] = sign_turn * data["angles_1"]

        data = np.load("Step_2b_" + filename + ".npz")
        cal_dict["I_step_2b"] = data["Iexp"]
        cal_dict["Angles_step_2b"] = sign_turn * data["angles_1"]

        data = np.load("Step_3a_" + filename + ".npz")
        cal_dict["I_step_3a"] = data["Iexp"]
        cal_dict["Angles_step_3a"] = sign_turn * data["angles_1"]

        data = np.load("Step_3b_" + filename + ".npz")
        cal_dict["I_step_3b"] = data["Iexp"]
        cal_dict["Angles_step_3b"] = sign_turn * data["angles_1"]

        data = np.load("Step_3c_" + filename + ".npz")
        cal_dict["I_step_3c"] = data["Iexp"]
        cal_dict["Angles_step_3c"] = sign_turn * data["angles_1"]

        data = np.load("Step_4a_" + filename + ".npz")
        cal_dict["I_step_4a"] = data["Iexp"]
        cal_dict["Angles_1_step_4a"] = data["angles_1"]
        cal_dict["Angles_2_step_4a"] = sign_turn * data["angles_2"]

        data = np.load("Step_4b_" + filename + ".npz")
        cal_dict["I_step_4b"] = data["Iexp"]
        cal_dict["Angles_1_step_4b"] = sign_turn * data["angles_1"]
        cal_dict["Angles_2_step_4b"] = sign_turn * data["angles_2"]

        data = np.load("Step_5a_" + filename + ".npz")
        cal_dict["I_step_5a"] = data["Iexp"]
        cal_dict["Angles_1_step_5a"] = sign_turn * data["angles_1"]
        cal_dict["Angles_2_step_5a"] = sign_turn * data["angles_2"]

        data = np.load("Step_5b_" + filename + ".npz")
        cal_dict["I_step_5b"] = data["Iexp"]
        cal_dict["Angles_1_step_5b"] = sign_turn * data["angles_1"]
        cal_dict["Angles_2_step_5b"] = sign_turn * data["angles_2"]

        data = np.load("Step_6_" + filename + ".npz")
        cal_dict["I_step_6"] = data["Iexp"]
        cal_dict["Angles_step_6"] = data["angles"]
        cal_dict["Angles_step_6"][:, 2:] *= sign_turn

    # If filename is None, use the general name
    else:
        data = np.load("Calibration_measurements.npz")
        cal_dict = dict(data)

    # Volver del directorio
    if folder and old_folder:
        os.chdir(old_folder)

    return cal_dict


def Process_Single_Calibration(cal_dict=None,
                               folder=None,
                               filename=None,
                               method="iterative",
                               type_error="intensity",
                               N_iterations=20,
                               save_name=None,
                               verbose=True,
                               plot_all=False):
    """Function to process all the data and obtain final vlaues."""
    if verbose:
        start_time = time.time()

    # Load data if required
    if cal_dict is None:
        cal_dict = Load_Cal_Files(folder=folder, filename=filename)

    # TODO: Remove this when fixed
    # try:
    #     trans = cal_dict['P_filter'] / cal_dict['P_no_filter']
    #     print("S0 and I_step_2b being artificially corrected by filter transmission ({}).".format(trans))
    # except:
    #     trans = 1
    #cal_dict["S0"] = cal_dict["S0"] / trans
    #cal_dict["I_step_2b"] = cal_dict["I_step_2b"] /trans

    # Iterative
    if method.lower() == "iterative":
        # Draw function
        def draw_fig():
            fig = plt.figure(figsize=(15, 15))
            ax = fig.add_subplot(3, 3, 1)
            ax.plot(error[:ind + 1])
            ax.set_ylabel("Normalized RMS error")
            ax.set_title("Normalized RMS error")

            ax = fig.add_subplot(3, 3, 2)
            ax.plot(els[:ind + 1] / degrees)
            ax.set_ylabel("Ellipticity angle (deg)")
            ax.set_title("Ilum ellipticity")

            ax = fig.add_subplot(3, 3, 3)
            ax.plot(offset[:ind + 1] / degrees)
            ax.set_ylabel("Angle (deg)")
            ax.set_title("Rc offset")

            ax = fig.add_subplot(3, 3, 4)
            ax.plot(p1s[:, :ind + 1].T)
            ax.set_ylabel("Field trans.")
            ax.set_title("Polarizers p1")
            ax.legend(("P1", "P2", "P3", "Pc"))

            ax = fig.add_subplot(3, 3, 5)
            ax.plot(p2s[:, :ind + 1].T)
            ax.set_ylabel("Field trans.")
            ax.set_title("Polarizers p2")
            ax.legend(("P1", "P2", "P3", "Pc"))

            ax = fig.add_subplot(3, 3, 6)
            ax.plot(ps[:, :ind + 1].T)
            ax.set_ylabel("Field trans.")
            ax.set_title("Retarders")
            ax.legend(("R1 p1", "R1 p2", "R2 p1", "R2 p2", "Rc p1", "R2 p2"))

            ax = fig.add_subplot(3, 3, 7)
            ax.plot(azs[:, :ind + 1].T / degrees)
            ax.set_ylabel("Azimuth (deg)")
            ax.set_xlabel("Iteration")
            ax.set_title("Initial angles")
            ax.legend(("Illum", "P1", "P1 temp", "P2", "P3", "R1", "R2"))

            ax = fig.add_subplot(3, 3, 8)
            ax.plot(rets[:-1, :ind + 1].T / degrees)
            ax.set_ylabel("Retardance (deg)")
            ax.set_xlabel("Iteration")
            ax.set_title("L/4 retardances")
            ax.legend(("R1", "R2", "Rc"))

            ax = fig.add_subplot(3, 3, 9)
            ax.plot(rets[-1, :ind + 1] / degrees)
            ax.set_ylabel("Retardance (deg)")
            ax.set_xlabel("Iteration")
            ax.set_title("P1 retardance")

        # Prealocate
        error = np.zeros(N_iterations)
        offset = np.zeros(N_iterations)
        p1s = np.zeros((4, N_iterations))
        p2s = np.zeros((4, N_iterations))
        ps = np.zeros((6, N_iterations))
        azs = np.zeros((7, N_iterations))
        els = np.zeros(N_iterations)
        rets = np.zeros((4, N_iterations))
        fig = plt.figure(figsize=(15, 15))
        plt.ion()

        # Iterate
        for ind in range(N_iterations):
            print("Inicial", cal_dict["P1_az"])
            cal_dict = make_step_1(cal_dict, verbose=plot_all)
            cal_dict = make_step_2a(cal_dict, verbose=plot_all)
            cal_dict = make_step_2b(cal_dict, verbose=plot_all)
            cal_dict = analysis_step_3(cal_dict)
            cal_dict = analysis_step_4(cal_dict, verbose=plot_all)
            cal_dict = analysis_step_5(cal_dict, verbose=plot_all)
            cal_dict = make_step_6(cal_dict, method=type_error)
            print("Despues de los ajustes", cal_dict["P1_az"])

            # Save data
            error[ind] = cal_dict["Error_step_6"]
            azs[:, ind] = [
                cal_dict["illum_az"], cal_dict["P1_az"],
                cal_dict["P1_az_temp"], cal_dict["P2_az"], cal_dict["P3_az"],
                cal_dict["R1_az"], cal_dict["R2_az"]
            ]
            els[ind] = cal_dict["illum_el"]
            p1s[:, ind] = [
                cal_dict["P1_p1"], cal_dict["P2_p1"], cal_dict["P3_p1"],
                cal_dict["Pc_p1"]
            ]
            p2s[:, ind] = [
                cal_dict["P1_p2"], cal_dict["P2_p2"], cal_dict["P3_p2"],
                cal_dict["Pc_p2"]
            ]
            ps[:, ind] = [
                cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R2_p1"],
                cal_dict["R2_p2"], cal_dict["Rc_p1"], cal_dict["Rc_p2"]
            ]
            rets[:, ind] = [
                cal_dict["R1_R"], cal_dict["R2_R"], cal_dict["Rc_R"],
                cal_dict["P1_R"]
            ]
            offset[ind] = cal_dict["Rc_offset"]

            # Plot
            if verbose:
                print("Plotting iteration {}. Error = {}".format(
                    ind, error[ind]))
            # if ind > 0:
            # drawnow(draw_fig)
            # draw_fig()
        if verbose and not plot_all:
            draw_steps_fit_error(cal_dict)
        if verbose and N_iterations > 1:
            draw_fig()

    # Simultaneous
    else:
        bound_up = np.array([
            180 * degrees, 45 * degrees, 1, 0.3, 360 * degrees, 180 * degrees,
            180 * degrees, 1.1, 1.1, 100 * degrees, 180 * degrees, 1.1, 1.1,
            100 * degrees, 180 * degrees, 1.1, 0.3, 180 * degrees, 1.1, 1.1,
            180 * degrees, 1.1, 0.2, 1.1, 1.1, 120 * degrees, 35 * degrees
        ])
        bound_down = np.array([
            0 * degrees, -45 * degrees, 0.5, 0, 0 * degrees, 0 * degrees,
            0 * degrees, 0.8, 0.8, 80 * degrees, 0 * degrees, 0.8, 0.8,
            80 * degrees, 0 * degrees, 0.5, 0, 0 * degrees, 0.3, 0,
            0 * degrees, 0.3, 0, 0.8, 0.8, 60 * degrees, -35 * degrees
        ])

        # print(bound_up>bound_down)

        # bound_up = np.array([
        #     180 * degrees, 45 * degrees, 1, 0.1, 360 * degrees, 180 * degrees,
        #     180 * degrees, 1, 1, 100 * degrees, 180 * degrees, 1, 1,
        #     100 * degrees, 180 * degrees, 1, 0.1, 180 * degrees, 1, 0.1,
        #     180 * degrees, 1, 0.1, 1, 1, 100 * degrees, cal_dict["Rc_offset_max"]
        # ])
        # bound_down = np.array([
        #     0, -45 * degrees, 0.8, 0, 0, 0,
        #     0, 0.9, 0.9, 80 * degrees, 0 * degrees, 0.9, 0.9,
        #     80 * degrees, 0 * degrees, 0.85, 0, 0 * degrees, 0.85, 0,
        #     0 * degrees, 0.7, 0, 0.9, 0.9, 80 * degrees, -cal_dict["Rc_offset_max"]
        # ])
        bounds = (bound_down, bound_up)

        # PSO
        optimizer = GlobalBestPSO(
            n_particles=PSO_part_global, dimensions=bound_up.size, options=options_global, bounds=bounds)
        cost, result = optimizer.optimize(opt_func_PSO, iters=PSO_iter_global, n_processes=PSO_cores_global, cal_dict=cal_dict, type_error="intensity", single=True)
        # Least squares
        args = (cal_dict, "intensity")
        result = least_squares(error_global, result, bounds=bounds, args=args)
        result = result.x

        (cal_dict["illum_az"], cal_dict["illum_el"],
         cal_dict["P1_p1"], cal_dict["P1_p2"],
         cal_dict["P1_R"], cal_dict["P1_az"], cal_dict["P1_az_temp"],
         cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_R"],
         cal_dict["R1_az"], cal_dict["R2_p1"], cal_dict["R2_p2"],
         cal_dict["R2_R"], cal_dict["R2_az"], cal_dict["P2_p1"],
         cal_dict["P2_p2"], cal_dict["P2_az"], cal_dict["P3_p1"],
         cal_dict["P3_p2"], cal_dict["P3_az"], cal_dict["Pc_p1"],
         cal_dict["Pc_p2"], cal_dict["Rc_p1"], cal_dict["Rc_p2"],
         cal_dict["Rc_R"], cal_dict["Rc_offset"]) = result
        cal_dict["P1_offset"] = 0

        cal_dict = make_step_6(cal_dict, method=type_error)
        cal_dict["illum_el_sign"] = np.sign(cal_dict["illum_el"])
        cal_dict["illum_el_max"] = np.abs(cal_dict["illum_el"])

        if verbose:
            draw_steps_fit_error(cal_dict, method=method)
            print("Computed error is: {}".format(cal_dict["Error_step_6"]))

    print("Antes de grabar", cal_dict["P1_az"])
    if save_name:
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)
        print("Current folder:   ", os.getcwd())
        np.savez(save_name, **cal_dict)
        print("Saved file:   ", save_name)
        if folder and old_folder:
            os.chdir(old_folder)
    if verbose:
        print("Ellapsed time is {} s".format(time.time() - start_time))

    return cal_dict


def Process_Multiple_Calibrations(cal_dict=None,
                                  folder=None,
                                  filename=None,
                                  method="iterative",
                                  type_error="intensity",
                                  N_iterations=20,
                                  save_name="Calibration.npz",
                                  N_cal=10,
                                  verbose=True):
    """Function to perform several analysis of the calibration with different random start values."""
    if verbose:
        start_time = time.time()

    # Loop iterations
    error = np.zeros(N_cal)
    list_cals = []
    lowest_error = 1e6

    for ind in range(N_cal):
        new_cal = Process_Single_Calibration(
            cal_dict=None,
            folder=folder,
            filename=filename,
            method=method,
            type_error=type_error,
            N_iterations=N_iterations,
            save_name=None,
            verbose=False)
        error[ind] = new_cal["Error_step_6"]
        if error[ind] < lowest_error:
            lowest_error = error[ind]
            cal_dict = new_cal
        if verbose:
            print("Computed error of iteration {} is: {}".format(
                ind, new_cal["Error_step_6"]))

    if save_name:
        if folder:
            old_folder = os.chdir(folder)
        np.savez(save_name, **cal_dict)
        if folder and old_folder:
            os.chdir(old_folder)
    if verbose:
        draw_steps_fit_error(cal_dict)
        print("Ellapsed time is {} s".format(time.time() - start_time))

    return cal_dict


def draw_steps_fit_error(cal_dict, method="intensity"):
    """Draw the figures with the measured intensities and the calculated through the fit."""
    make_step_1(cal_dict, verbose=True, fit=False)
    make_step_2a(cal_dict, verbose=True, fit=False)
    make_step_2b(cal_dict, verbose=True, fit=False)
    analysis_step_3(cal_dict, verbose=True, fit=False)
    analysis_step_4(cal_dict, verbose=True, fit=False)
    analysis_step_5(cal_dict, verbose=True, fit=False)
    make_step_6(cal_dict, method=method, verbose=True)


def model_global(par, cal_dict):
    """Function to calculate the global error of the calibration."""
    # Rename parameters
    (illum_az, illum_el, P1_p1, P1_p2, P1_R, P1_az,
     P1_az_temp, R1_p1, R1_p2, R1_R, R1_az, R2_p1, R2_p2, R2_R, R2_az, P2_p1,
     P2_p2, P2_az, P3_p1, P3_p2, P3_az, Pc_p1, Pc_p2, Rc_p1, Rc_p2, Rc_R,
     dif_con) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=illum_az,
        ellipticity=illum_el,
        degree_pol=1)
    Mp1_temp = Mueller().diattenuator_linear(
        p1=P1_p1, p2=P1_p2, azimuth=-P1_az_temp)
    Mp1 = Mueller().diattenuator_retarder_linear(
        p1=P1_p1, p2=P1_p2, R=P1_R, azimuth=-P1_az)
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=R1_p1, p2=R1_p2, R=R1_R, azimuth=-R1_az)
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=R2_p1, p2=R2_p2, R=R2_R, azimuth=-R2_az)
    Mp2 = Mueller().diattenuator_linear(p1=P2_p1, p2=P2_p2, azimuth=-P2_az)
    Mp3 = Mueller().diattenuator_linear(p1=P3_p1, p2=P3_p2, azimuth=-P3_az)
    Mpc = Mueller().diattenuator_linear(p1=Pc_p1, p2=Pc_p2, azimuth=0)
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=Rc_p1, p2=Rc_p2, R=Rc_R, azimuth=45*degrees-dif_con)

    # Step 1
    Mp1_rot = Mp1_temp.rotate(angle=cal_dict["Angles_step_1"], keep=True)
    Sf = Mp1_rot * (Mpc * S)
    I_1 = Sf.parameters.intensity()

    # Step 2a
    Mp1_rot = Mp1_temp.rotate(angle=cal_dict["Angles_step_2a"], keep=True)
    Sf = Mp1_rot * S
    I_2a = Sf.parameters.intensity()

    # Step 2b
    Mp1_rot = Mp1_temp.rotate(angle=cal_dict["Angles_step_2b"], keep=True)
    Sf = Mp1_rot * (Mrc * S)
    I_2b = Sf.parameters.intensity()

    # Step 3a
    Mp1_rot = Mp1_temp.rotate(angle=cal_dict["Angles_step_3a"], keep=True)
    Sf = Mp1_rot * (Mp3 * S)
    I_3a = Sf.parameters.intensity()

    # Step 3b
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_3b"], keep=True)
    Sf = Mp2_rot * (Mp3 * S)
    I_3b = Sf.parameters.intensity()

    # Step 3c
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_3c"], keep=True)
    Sf = Mp2_rot * (Mp1 * S)
    I_3c = Sf.parameters.intensity()

    # Step 4a
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_2_step_4a"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["Angles_1_step_4a"], keep=True)
    Sf = Mp2 * (Mr2_rot * (Mrc * (Mp1_rot * S)))
    I_4a = Sf.parameters.intensity()

    # Step 4b
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_1_step_4b"], keep=True)
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_2_step_4b"], keep=True)
    Sf = Mp2_rot * (Mr2_rot * (Mp1 * S))
    I_4b = Sf.parameters.intensity()

    # Step 5a
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_1_step_5a"], keep=True)
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_2_step_5a"], keep=True)
    Sf = (Mp2_rot * Mr2_rot) * (Mr1 * (Mp1 * S))
    I_5a = Sf.parameters.intensity()

    # Step 5b
    Mp1_rot = Mp1.rotate(angle=45 * degrees, keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_1_step_5b"], keep=True)
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_2_step_5b"], keep=True)
    Sf = (Mp2_rot * Mr2_rot) * (Mr1 * (Mp1_rot * S))
    I_5b = Sf.parameters.intensity()

    # Step 6
    Mp1_rot = Mp1.rotate(angle=cal_dict["Angles_step_6"][:, 0], keep=True)
    Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_6"][:, 1], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_6"][:, 2], keep=True)
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_6"][:, 3], keep=True)
    Sf = Mp2_rot * (Mr2_rot * (Mr1_rot * (Mp1_rot * S)))
    I_6 = Sf.parameters.intensity()

    return I_1, I_2a, I_2b, I_3a, I_3b, I_3c, I_4a, I_4b, I_5a, I_5b, I_6


def error_global(par, cal_dict, type_error="intensity", single=False):
    """Function to determine the global error."""

    aux_dict = {}
    (aux_dict["illum_az"], aux_dict["illum_el"],
     aux_dict["P1_p1"], aux_dict["P1_p2"],
     aux_dict["P1_R"], aux_dict["P1_az"], aux_dict["P1_az_temp"],
     aux_dict["R1_p1"], aux_dict["R1_p2"], aux_dict["R1_R"],
     aux_dict["R1_az"], aux_dict["R2_p1"], aux_dict["R2_p2"],
     aux_dict["R2_R"], aux_dict["R2_az"], aux_dict["P2_p1"],
     aux_dict["P2_p2"], aux_dict["P2_az"], aux_dict["P3_p1"],
     aux_dict["P3_p2"], aux_dict["P3_az"], aux_dict["Pc_p1"],
     aux_dict["Pc_p2"], aux_dict["Rc_p1"], aux_dict["Rc_p2"],
     aux_dict["Rc_R"], aux_dict["Rc_offset"]) = par

    if type_error == "mueller":
        Ms = Msystem_From_Dict(aux_dict)
        M = Calculate_Mueller_Matrix_0D(cal_dict["I_step_6"],
                                        cal_dict["Angles_step_6"], Ms)
        Error = np.linalg.norm(M.M - Mueller().vacuum().M) / 16

    elif type_error == "mixed":
        Ms = Msystem_From_Dict(aux_dict)
        M = Calculate_Mueller_Matrix_0D(cal_dict["I_step_6"],
                                        cal_dict["Angles_step_6"], Ms)
        E_Mueller = np.squeeze(M.M - Mueller().vacuum().M).flatten() / 16

        Imodel = model_global(par, cal_dict)
        s1 = Imodel[0].size
        s2 = Imodel[6].size
        s3 = Imodel[-1].size

        E_1 = (cal_dict["I_step_1"] - Imodel[0]) / s1
        E_2a = (cal_dict["I_step_2a"] - Imodel[1]) / s1
        E_2b = (cal_dict["I_step_2b"] - Imodel[2]) / s1
        E_3a = (cal_dict["I_step_3a"] - Imodel[3]) / s1
        E_3b = (cal_dict["I_step_3b"] - Imodel[4]) / s1
        E_3c = (cal_dict["I_step_3c"] - Imodel[5]) / s1
        E_4a = (cal_dict["I_step_4a"] - Imodel[6]).flatten() / s2
        E_4b = (cal_dict["I_step_4b"] - Imodel[7]).flatten() / s2
        E_5a = (cal_dict["I_step_5a"] - Imodel[8]).flatten() / s2
        E_5b = (cal_dict["I_step_5b"] - Imodel[9]).flatten() / s2

        Error = np.concatenate((E_Mueller, E_1, E_2a, E_2b, E_3a, E_3b, E_3c, E_4a, E_4b,
                                E_5a, E_5b))

        if single:
            Error = np.linalg.norm(Error) / Error.size

    else:

        Imodel = model_global(par, cal_dict)
        s1 = Imodel[0].size
        s2 = Imodel[6].size
        s3 = Imodel[-1].size

        E_1 = (cal_dict["I_step_1"] - Imodel[0]) / s1
        E_2a = (cal_dict["I_step_2a"] - Imodel[1]) / s1
        E_2b = (cal_dict["I_step_2b"] - Imodel[2]) / s1
        E_3a = (cal_dict["I_step_3a"] - Imodel[3]) / s1
        E_3b = (cal_dict["I_step_3b"] - Imodel[4]) / s1
        E_3c = (cal_dict["I_step_3c"] - Imodel[5]) / s1
        E_4a = (cal_dict["I_step_4a"] - Imodel[6]).flatten() / s2
        E_4b = (cal_dict["I_step_4b"] - Imodel[7]).flatten() / s2
        E_5a = (cal_dict["I_step_5a"] - Imodel[8]).flatten() / s2
        E_5b = (cal_dict["I_step_5b"] - Imodel[9]).flatten() / s2
        E_6 = (cal_dict["I_step_6"] - Imodel[10]) / s3

        Error = np.concatenate((E_1, E_2a, E_2b, E_3a, E_3b, E_3c, E_4a, E_4b,
                                E_5a, E_5b, E_6))
        if single:
            Error = np.linalg.norm(Error) / Error.size

    return Error

def opt_func_PSO_ind(Transitions, fun, args):
    """Function needed as interface between PSO algorithm and error_global."""
    num_particles = Transitions.shape[0]  # number of particles
    error = [np.linalg.norm(fun(Transitions[i,:], *args)) for i in range(num_particles)]
    return error

def opt_func_PSO(Transitions, cal_dict, type_error="intensity", single=False):
    """Function needed as interface between PSO algorithm and error_global."""
    num_particles = Transitions.shape[0]  # number of particles
    error = [error_global(Transitions[i,:], cal_dict=cal_dict, type_error=type_error, single=single) for i in range(num_particles)]
    return error


def generate_initial_cal_dict(method="fixed", experiment=True, extra_name=""):
    """Function to generate the initial cal_dict. It can be used both for the experiment and the fit part. It supports different methods."""
    # Experimental values
    cal_dict = {}
    if experiment:
        cal_dict["type"] = 'Experiment'
        cal_dict["N_measures_1D"] = 91
        cal_dict["N_measures_2D"] = [19, 19]
        cal_dict["N_measures_pol"] = 144
        cal_dict["max_angle_1D"] = 180
        cal_dict["max_angle_2D"] = 180
        cal_dict["P1_az_step_4a"] = np.nan
        cal_dict["angles_1"] = np.linspace(0, cal_dict["max_angle_1D"],
                                           cal_dict["N_measures_1D"])
        cal_dict["angles_2X"] = np.linspace(0, cal_dict["max_angle_2D"],
                                            cal_dict["N_measures_2D"][1])
        cal_dict["angles_2Y"] = np.linspace(0, cal_dict["max_angle_2D"],
                                            cal_dict["N_measures_2D"][0])
        cal_dict['P_no_filter'] = 1  # mW
        cal_dict['P_filter'] = 1  # mW
        cal_dict['extra_name'] = extra_name  # mW

    cal_dict["Rc_offset_max"] = 35 * degrees
    cal_dict["P1_offset_max"] = 1 * degrees
    if method == "fixed":
        cal_dict["illum_pol_degree"] = 1
        cal_dict["illum_az"] = 0 * degrees
        cal_dict["illum_el_sign"] = 1
        cal_dict["illum_el_max"] = 45 * degrees
        cal_dict["P1_p1"] = 0.975
        cal_dict["P1_p2"] = 0.05
        cal_dict["P1_az"] = 90 * degrees
        cal_dict["P1_az_temp"] = 90 * degrees
        cal_dict["P1_az_step_4a"] = np.nan
        cal_dict["P1_R"] = 90 * degrees
        cal_dict["P2_p1"] = 0.975
        cal_dict["P2_p2"] = 0.05
        cal_dict["P2_az"] = 90 * degrees
        cal_dict["P3_p1"] = 0.975
        cal_dict["P3_p2"] = 0.05
        cal_dict["P3_az"] = 90 * degrees
        cal_dict["Pc_p1"] = 0.4
        cal_dict["Pc_p2"] = 0.04
        cal_dict["R1_p1"] = 0.99
        cal_dict["R1_p2"] = 0.99
        cal_dict["R1_az"] = 90 * degrees
        cal_dict["R1_R"] = 83 * degrees
        cal_dict["R2_p1"] = 0.99
        cal_dict["R2_p2"] = 0.99
        cal_dict["R2_az"] = 90 * degrees
        cal_dict["R2_R"] = 83 * degrees
        cal_dict["Rc_p1"] = 0.99
        cal_dict["Rc_p2"] = 0.99
        cal_dict["Rc_R"] = 90 * degrees
        cal_dict["Rc_offset"] = 0 * degrees

    else:
        azs = np.random.rand(7) * 180 * degrees
        signs = np.sign(np.random.rand(1) - 0.5)
        els = np.random.rand(1) * 5 * degrees + 40 * degrees
        p1s = np.random.rand(10) * 0.1 + 0.9
        p2s = np.random.rand(4) * 0.1
        rets = np.random.rand(3) * 20 * degrees + 80 * degrees

        cal_dict["illum_pol_degree"] = 1
        cal_dict["illum_az"] = azs[0]
        cal_dict["illum_el_max"] = els[0]
        cal_dict["illum_el_sign"] = signs[0]
        cal_dict["P1_p1"] = p1s[0]
        cal_dict["P1_p2"] = p2s[0]
        cal_dict["P1_az"] = azs[1]
        cal_dict["P1_az_temp"] = azs[2]
        cal_dict["P1_R"] = np.random.rand(1)[0] * 360 * degrees
        cal_dict["P2_p1"] = p1s[1]
        cal_dict["P2_p2"] = p2s[1]
        cal_dict["P2_az"] = azs[3]
        cal_dict["P3_p1"] = p1s[2]
        cal_dict["P3_p2"] = p2s[2]
        cal_dict["P3_az"] = azs[4]
        cal_dict["Pc_p1"] = p1s[3]
        cal_dict["Pc_p2"] = p2s[3]
        cal_dict["R1_p1"] = p1s[4]
        cal_dict["R1_p2"] = p1s[5]
        cal_dict["R1_az"] = azs[5]
        cal_dict["R1_R"] = rets[0]
        cal_dict["R2_p1"] = p1s[6]
        cal_dict["R2_p2"] = p1s[7]
        cal_dict["R2_az"] = azs[6]
        cal_dict["R2_R"] = rets[1]
        cal_dict["Rc_p1"] = p1s[8]
        cal_dict["Rc_p2"] = p1s[9]
        cal_dict["Rc_R"] = rets[2]
        cal_dict["Rc_offset"] = np.random.rand(
            1)[0] * 2 * cal_dict["Rc_offset_max"] - cal_dict["Rc_offset_max"]

    cal_dict["illum_el"] = cal_dict["illum_el_sign"] * cal_dict["illum_el_max"]

    return cal_dict

def simulate_experiment(folder=None):
    """Function that simulates a polarimeter calibration experiment, generating the same files in order to make the fit."""
    # Go to folder
    if folder is not None:
        old_folder = os.getcwd()
        os.chdir(folder)

    # Generate experiment objects
    cal_dict = generate_initial_cal_dict(method="random")
    cal_dict['P_filter'] = 1.
    cal_dict['P_no_filter'] = 1.
    cal_dict['S0'], cal_dict['S0_error'] = 2, 0
    S, Mp1, Mr1, Mr2, Mp2 = Msystem_From_Dict(cal_dict)
    Mp3 = Mueller().diattenuator_linear(p1=cal_dict["P3_p1"], p2=cal_dict["P3_p2"], azimuth=cal_dict["P3_az"])
    Mpc = Mueller().diattenuator_linear(p1=cal_dict["Pc_p1"], p2=cal_dict["Pc_p2"])
    Mrc = Mueller().diattenuator_retarder_linear(p1=cal_dict["Rc_p1"], p2=cal_dict["Rc_p2"], R=cal_dict["Rc_R"], azimuth=45*degrees-cal_dict["Rc_offset"])
    Mp1_aux = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["P1_p1"],
        p2=cal_dict["P1_p2"],
        R=cal_dict["P1_R"],
        azimuth=-cal_dict["P1_az_temp"])

    # Extract common variables
    angles = cal_dict["angles_1"] * degrees
    anglesx = cal_dict["angles_2X"] * degrees
    anglesy = cal_dict["angles_2Y"] * degrees
    anglesX, anglesY = np.meshgrid(anglesx, anglesy)
    angles6 = np.random.rand(cal_dict["N_measures_pol"], 4) * 180 * degrees

    # Generate angles and intensities
    Sfin = Mp1_aux.rotate(angle=angles, keep=True) * Mpc * S
    cal_dict["I_step_1"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_1"] = angles

    Sfin = Mp1_aux.rotate(angle=angles, keep=True) * S
    cal_dict["I_step_2a"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_2a"] = angles

    Sfin = Mp1_aux.rotate(angle=angles, keep=True) * Mrc * S
    cal_dict["I_step_2b"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_2b"] = angles

    Sfin = Mp1_aux.rotate(angle=angles, keep=True) * Mp3 * S
    cal_dict["I_step_3a"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_3a"] = angles

    Sfin = Mp2.rotate(angle=angles, keep=True) * Mp3 * S
    cal_dict["I_step_3b"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_3b"] = angles

    Sfin = Mp2.rotate(angle=angles, keep=True) * Mp1 * S
    cal_dict["I_step_3c"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_3c"] = angles

    Sfin = Mp2 * Mr2.rotate(angle=anglesY, keep=True) * Mrc * Mp1.rotate(angle=anglesX, keep=True) * S
    cal_dict["I_step_4a"] = Sfin.parameters.intensity()
    cal_dict["Angles_1_step_4a"] = anglesX
    cal_dict["Angles_2_step_4a"] = anglesY

    Sfin = Mp2.rotate(angle=anglesY, keep=True) * Mr2.rotate(angle=anglesX, keep=True) * Mp1 * S
    cal_dict["I_step_4b"] = Sfin.parameters.intensity()
    cal_dict["Angles_1_step_4b"] = anglesX
    cal_dict["Angles_2_step_4b"] = anglesY

    Sfin = Mp2.rotate(angle=anglesY, keep=True) * Mr2.rotate(angle=anglesX, keep=True) * Mr1 * Mp1 * S
    cal_dict["I_step_5a"] = Sfin.parameters.intensity()
    cal_dict["Angles_1_step_5a"] = anglesX
    cal_dict["Angles_2_step_5a"] = anglesY

    Sfin = Mp2.rotate(angle=anglesY, keep=True) * Mr2.rotate(angle=anglesX, keep=True) * Mr1 * Mp1.rotate(angle=45*degrees, keep=True) * S
    cal_dict["I_step_5b"] = Sfin.parameters.intensity()
    cal_dict["Angles_1_step_5b"] = anglesX
    cal_dict["Angles_2_step_5b"] = anglesY

    Sfin = Mp2.rotate(angle=angles6[:, 3], keep=True) * Mr2.rotate(angle=angles6[:, 2], keep=True) * Mr1.rotate(angle=angles6[:, 1], keep=True) * Mp1.rotate(angle=angles6[:, 0], keep=True) * S
    cal_dict["I_step_6"] = Sfin.parameters.intensity()
    cal_dict["Angles_step_6"] = angles6

    # Save files
    np.savez('Calibration_measurements.npz', **cal_dict)

    # Return to original folder
    if folder is not None:
        os.chdir(old_folder)



def Measure_Motor_Drift(pol, Nangles=37, Nrandom=15, wait_time=0.7):
    """Function to measure the drift of the beam due to the rotation of the motors. It is only important in camera configuration.

    Args:
        pol (Polarimeter): Polarimeter object.
        Nangles (int): Number of measured angles between 0 and 360 degrees. Default: 37.
        wait_time (float): Wait time between movement and camera measurement in seconds. Default: 0.7.
    """    
    # Prepare for experiment
    x = np.linspace(0, 360, Nangles)
    indC = [0, 0, 0, 0]
    limits = [Nangles, Nangles, Nangles, Nangles]
    try:
        pol.camera.Start_Live()
    except:
        pass


    # Main program
    for indM in range(4):
        for indA, angle in enumerate(x):
            # Move the motors
            angles = np.zeros(4)
            angles[indM] = angle
            pol.motor.Move_Absolute(pos=angles, units="deg")
            # Get the camera image
            image = pol.camera.Get_Image(wait_time=wait_time)
            # Save the image
            np.savez("Motor_Drift_M{}_A{}.npz".format(indM, indA), image=image, angles=angles*degrees, angle=angle*degrees, indM=indM)
            # Update advance
            indC[indM] = indA
            utils.percentage_complete(indC[indM],limits[indM])
        print("\n")

        # Return to [0,0,0,0] just in case
        pol.motor.Move_Absolute(pos=[0,0,0,0])

    # Repeat for random numbers (check)
    for indR in range(Nrandom):
        # Move the motors
        angles = np.random.rand(4) * 360
        pol.motor.Move_Absolute(pos=angles, units="deg")
        # Get the camera image
        image = pol.camera.Get_Image(wait_time=wait_time)
        # Save the image
        np.savez("Motor_Drift_R{}.npz".format(indR), image=image, angles=angles*degrees)
        # Update advance
        indC[indM] = indA
        utils.percentage_complete(indR, Nrandom)

def Analyze_Motor_Drift(Nangles=37, Nrandom=15):
    """Function to analyze the stored images of the motor drift on the camera.

    Args:
        Nangles (int, optional): Number of angles in each motor. Defaults to 37.
        Nrandom (int, optional): Number of check random values. Defaults to 15.
    """    
    # Prealocate
    Nangles_fit = 360*5+1
    pos = np.zeros((2, 4, Nangles))
    angles = np.linspace(0, 360*degrees, Nangles)
    angles_fit = np.linspace(0, 360*degrees, Nangles_fit)

    # Reference image. I make an average but should be equal.
    dis = np.zeros((8,2))
    # First image as reference
    data = np.load("Motor_Drift_M0_A0.npz")
    im0_prov = data["image"][::-1, ::-1]
    im0 = np.zeros_like(im0_prov)
    for indM in range(4):
        for indA, ang in enumerate([0,Nangles-1]):
            # First image as reference
            data = np.load("Motor_Drift_M{}_A{}.npz".format(indM,ang))
            im = data["image"]
            convolution = fftconvolve(im, im0_prov, mode='same')
            dis[indM+4*indA,:] = np.array(np.unravel_index(np.argmax(convolution), convolution.shape))
            im0 = im0 + im0_prov/8
    print(dis)
    dis0 = np.mean(dis, axis=0)
    disErr = np.std(dis, axis=0)
    print("The centroid position (usimg M=0 A=0) as reference is:")
    print("  - Average:   x = {:.1f} pix.;   y = {:.1f} pix.".format(*dis0))
    print("  - Error:     x = {:.1f} pix.;   y = {:.1f} pix.".format(*disErr))

    # Analysis loop
    for indM in range(4):
        for indA in range(1,Nangles-1):
            data = np.load("Motor_Drift_M{}_A{}.npz".format(indM, indA))
            im = data["image"]
            convolution = fftconvolve(im, im0, mode='same')
            dis = np.array(np.unravel_index(np.argmax(convolution), convolution.shape))
            pos[:, indM, indA] = dis - dis0
            utils.percentage_complete([indM, indA], [4, Nangles])

    # Fit
    Offset = np.zeros((2, 4))
    Amp = np.zeros((2, 4))
    Phase = np.zeros((2, 4))
    for indM in range(4):
        for indX in range(2):
            maxi = np.max(np.abs(pos[indX, indM, :])) + 1e-16
            bounds = ([-maxi, 0, 0], [maxi, 1.2*maxi, 360 * degrees])
            args = (angles, pos[indX, indM, :])
            # PSO
            optimizer = GlobalBestPSO(
                n_particles=15, dimensions=len(bounds[0]), options=options_individual, bounds=bounds)
            cost, result = optimizer.optimize(opt_func_PSO_ind, iters=10, verbose=False, n_processes=None, fun=error_cos, args=args)
            # Least squares
            result = least_squares(
                error_cos, result, bounds=bounds, ftol=tol, xtol=tol, gtol=tol, args=args)
            Offset[indX, indM], Amp[indX, indM], Phase[indX, indM] = result.x
            utils.percentage_complete([indM, indX], [4, 2])

    # Plot
    legend = ("M0", "M1", "M2", "M3")
    ylabels = ("Y (pixels)", "X (pixels)")
    colors = ("b", "g", "k", "r")
    fits = np.zeros((2, 4, Nangles_fit))
    plt.figure(figsize=(6,2))
    for indX in range(2):
        plt.subplot(1,2,indX+1)
        for indM in range(4):
            plt.plot(angles/degrees, pos[indX,indM,:], "+", color=colors[indM])
            fit = model_cos([Offset[indX, indM], Amp[indX, indM], Phase[indX, indM]], angles_fit)
            fits[indX, indM, :] = fit
            plt.plot(angles_fit/degrees, fit, "--", color=colors[indM])
        plt.xlabel("Angle (deg)")
        plt.ylabel(ylabels[indX])
        plt.legend()

    plt.figure()
    for indM in range(4):
        plt.plot(pos[1,indM,:], pos[0,indM,:], "+", color=colors[indM])
        plt.plot(fits[1, indM, :], fits[0, indM,:], "--", color=colors[indM])
    plt.legend(legend)
    plt.xlabel("X (pixels)")
    plt.ylabel("Y (pixels)")

    # Error check
    errorX = np.zeros(Nrandom)
    errorY = np.zeros(Nrandom)
    error = np.zeros(Nrandom)
    for indR in range(Nrandom):
        data = np.load("Motor_Drift_M0_A{}.npz".format(indR))
        im = data["image"]
        angles_case = data["angles"]
        convolution = fftconvolve(im, im0[::-1,::-1], mode='same')
        dis = np.array(np.unravel_index(np.argmax(convolution), convolution.shape)) - dis0
        # print("dis",dis)
        expected = calculated_drift(angles_case, Offset, Amp, Phase)
        # print("expected",expected)
        dif = dis - expected
        errorY[indR], errorX[indR] = dif
        error[indR] = np.linalg.norm(dif)
        utils.percentage_complete(indR, Nrandom)

    plt.figure()
    plt.plot(error)
    plt.plot(errorX)
    plt.plot(errorY)
    plt.ylabel("Error (pixels)")
    plt.legend(("Total error", "Error in X", "Error in Y"))

    # Save result
    np.savez("Pol_Motor_Drift.npz", x=pos[1,:,:], y=pos[0,:,:], angles=angles, Amp_fit=Amp, Phase_fit=Phase, Offset_fit=Offset, error=np.mean(error))



def calculated_drift(angles, Offset, Amp, Phase):
    y = np.sum(Offset[0,:] + Amp[0,:] * np.cos(angles - Phase[0,:]))
    x = np.sum(Offset[1,:] + Amp[1,:] * np.cos(angles - Phase[1,:]))
    return np.array([y,x])




def __END():
    pass
