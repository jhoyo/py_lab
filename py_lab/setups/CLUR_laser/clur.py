from py_lab.motor import Motor_Multiple, UNITS_POS, UNITS_VEL
from py_lab.power_meter import Power_Meter
from py_lab.camera import Camera
from py_lab.config import CONF_CLUR, CONF_NEWPORT_8742, CONF_ESP_300,CONF_INTEL
from py_lab.utils import PSG_states_2_angles, PSG_angles_2_states

import numpy as np
from scipy.optimize import least_squares
from math import ceil
import pickle
import os
from time import sleep
import matplotlib.pyplot as plt

UNITS_POWER = {'W': 1, 'mW': 1e-3, 'uW': 1e-6, 'kW': 1e3}
UNITS_ANGLES = {'rad': 1, 'deg': np.pi/180}

KEYS_POINT = ["type", "name", "power", "number_pulses", "pos"]
KEYS_LINE = ["type", "name", "power", "length", "angle", "velocity", "pos", "acel_length"]
KEYS_RECTANGLE = ["type", "name", "power", "length", "width", "separation", "angle", "velocity", "pos", "acel_length"]
KEYS_ORIGIN = ["type", "pos"]
UNITS_KEY = {}
UNITS_KEY["type"] = ""
UNITS_KEY["name"] = ""
UNITS_KEY["power"] = " W"
UNITS_KEY["number_pulses"] = " pulses"
UNITS_KEY["pos"] = " mm"
UNITS_KEY["length"] = " mm"
UNITS_KEY["angle"] = " rad"
UNITS_KEY["velocity"] = " mm/s"
UNITS_KEY["acel_length"] = " mm"
UNITS_KEY["width"] = " mm"
UNITS_KEY["separation"] = " mm"



class CLUR(object):
    """
    Class for the CLUR laser setup.

    Objects:
        cam (Camera): Camera object for viewing the laser on the sample.
        stage (Motor_Multiple): Motor of the sample XYZ stage.
        gimbal (Motor_Multiple): Motor of the gimbal to control the angle of the sample respect to the laser.
        rotator (Motor_Multiple): Rotational motors to control the polarization optics rotation angle.
        pm (PowerMeter): Power meter object to measure the laser power.

    Atributes:
        power_calibration_parameters (array): Parameters of the power calibration.
        power_calibration_power_ref (float): Reference power for the power calibration.

        polarization_calibration_angle0_H (float): Initial angle for the half-waveplate.
        polarization_calibration_angle0_Q (float): Initial angle for the quarter-waveplate.

        power_ref (float): Reference power of the laser.
        sample_positions (list): List of the positions of the sample to calculate planarity.
        list_irradiations (list): List of the irradiations performed. Each element is a dictionary with the relevant data.
        sample_name (str): Name of the sample.
        irradiation_index (int): Index of the irradiation in the list.
    """

    ####################
    ## BASIC METHODS
    ####################

    def __init__(self, cam = "DCC1645C", stage = 'ESP300', gimbal = 'Newport8742', rotator = 'InteliDrives', pm = 'Ophir', load_sample = False):
        """Initialize the object.
            """
        # Objects
        self.cam = None if cam is None else Camera(cam)
        self.stage = None if stage is None else Motor_Multiple(stage, N=3)
        self.gimbal = None if gimbal is None else Motor_Multiple(gimbal, N=3)
        self.rotator = None if rotator is None else Motor_Multiple(name="InteliDrives", N=3)
        self.pm = None if pm is None else Power_Meter(pm)

        # Calibrations
        self.Load_Power_Cal()
        self.Load_Polarization_Cal()

        # Sample
        if load_sample:
            self.Load_Last_Irradiation()
        else:
            self.New_Sample()

        # Other variables
        self.power_ref = np.load(CONF_CLUR["cal_folder"] + "\\" + CONF_CLUR["power_ref_file"]) ["power_ref"]
        self.sample_positions = []
        self.list_irradiations = []
        
    def __del__(self):
        """When deleting object"""
        self.Close()
        
    def Load_Power_Cal(self):
        """Loads the calibration power."""
        cal = np.load(CONF_CLUR["cal_folder"] + "\\" + CONF_CLUR["power_cal_file"])         
        self.power_calibration_parameters = cal['par']
        self.power_calibration_power_ref = cal['I_ref']
        
    def Load_Polarization_Cal(self):
        """Loads the calibration polarization."""
        cal = np.load(CONF_CLUR["cal_folder"] + "\\" + CONF_CLUR["polarization_cal_file"])         
        self.polarization_calibration_angle0_H = cal['angle_0_H']
        self.polarization_calibration_angle0_Q = cal['angle_0_Q']
        
    def Load_Planarity_Cal(self): 
        """Loads the planarity positions for calibration"""
        cal = np.load(CONF_CLUR["cal_folder"] + "\\" + CONF_CLUR["planar_cal_file"])
        self.sample_positions = cal['sample_positions']

    def Load_Last_Irradiation(self):
        """Loads the last irradiation list."""
        self.Load_Sample(folder=CONF_CLUR["cal_folder"], name=CONF_CLUR["sample_list_file"])
        
    def Open(self):
        """Open the communication with the devices.
            """
        if self.cam is not None: 
            self.cam.Open()
        if self.stage is not None:
            self.stage.Open(invert=False, axis=[1, 2, 3], port = CONF_ESP_300['port'])
        if self.gimbal is not None:
            self.gimbal.Open(invert=False, axis=[1, 2, 3])
        if self.rotator is not None:
            self.rotator.Open(invert=False, axis=[0, 1, 2], port = CONF_INTEL['ports'])

    def Close(self):
        """Close the communication with the devices.
            """
        # Set the final state of the setup to max power and horizontal polarization
        if self.rotator is not None:
            self.Set_Power(power=1e9, units='W')
            self.Set_Polarization(azimuth=0, ellipticity=0)
            self.rotator.Close()
        # Close devices
        if self.cam is not None:
            self.cam.Close()
        if self.stage is not None:
            self.stage.Close()
        if self.gimbal is not None:
            self.gimbal.Close()
        

    ####################
    ## ENERGY CONTROL
    ####################

    def Get_Power_Reference(self, Nmeaurements=32, use_background=True, verbose=False):
        """Set the power reference.

        Args:
            * Nmeasurements (int): Number of measurements to average. Default: 32.
            * use_background (bool): Use the reference power. Default: True.
            * verbose (bool): If True, prints the power reference. Default: False.
            """
        self.power_ref = self.pm.Get_Power(units="W", Nmeasures=Nmeaurements, average=True, use_background=use_background, verbose=verbose)
        np.savez(CONF_CLUR['cal_folder'] + "\\" + CONF_CLUR['power_ref_file'], power_ref = self.power_ref)
        

    def Set_Power(self, power, units='W', verbose=False):
        """Set the power of the laser.

        Args:
            * power (float): Power to set.
            * units (str): Units of the power. Default: 'W'.
            * verbose (bool): If True, prints the power percentage. Default: False.
            """
        """Obtienes la intensidad"""
        # Transform units
        if units in UNITS_POWER:
            power = power * UNITS_POWER[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))
        # Load calibration        
        par = self.power_calibration_parameters
        # Intensity correction
        cte = self.power_calibration_power_ref / self.power_ref
        power = cte * power
        # Check power below lower limit
        if power < par[0]: 
            angle = par[2] + np.pi/4
            print("WARNING: requested power lower than limit: {:.3f} W".format(par[0] / cte))
        # Check power over upper limit
        elif power > par[0] + par[1]: 
            angle = par[2]
            print("WARNING: power upper than limit: {:.3f} W".format((par[0] + par[1]) / cte))
        # Rest of the cases, calculate angle
        else:
            angle = par[2] + .5 * np.arccos(np.sqrt((power - par[0]) / par[1]))
        # Move to position
        self.rotator.Move_Absolute(pos=angle, axis=2, units='rad', waiting="busy")
        # Verbose
        if verbose:
            power = 100 * power / (par[0] + par[1])
            print("Power: {:.2f}%".format(power))


    def Calculate_Power(self, units='W'):
        """Calculate the power of the laser.

        Args: 
            * units (str): Units of the power. Default: 'W'.

        Returns:
            * power (float): Power in the units specified.
            """
        # Get angle
        angle = self.rotator.Get_Position(units='rad')[2]
        # Load calibration
        par = self.power_calibration_parameters
        # Calculate power
        power = par[0] + par[1] * np.cos(2 * (angle - par[2]))
        # Transform units
        if units in UNITS_POWER:
            power = power / UNITS_POWER[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))
        return power


    ####################
    ## POLARIZATION CONTROL
    ####################

    def Set_Polarization(self, azimuth=0, ellipticity=0, busy=True):
        """Set the polarization of the laser.

        Args:
            * azimuth (float): Azimuth angle in degrees. Default: 0.
            * ellipticity (float): Ellipticity angle in degrees. Default: 0.
            * busy (bool): If True, waits until the movement is done. Default: True.
            """
        # Calculate rotation angles from parameters
        angle_P, angle_R = PSG_states_2_angles(azimuth=azimuth, ellipticity=ellipticity)
        angle_H = angle_P / 2 + self.polarization_calibration_angle0_H
        angle_Q = angle_R + self.polarization_calibration_angle0_Q
        print(angle_H/UNITS_ANGLES["deg"], angle_Q/UNITS_ANGLES["deg"])
        # Rotate
        self.rotator.Move_Absolute(pos=[angle_Q, angle_H, None], units='rad', waiting='busy')

    def Calculate_Polarization(self, units='rad'):
        """Calculate the polarization of the laser.

        Args: 
            * units (str): Units of the angles. Default: 'rad'.

        Returns:
            * azimuth (float): Azimuth angle in degrees.
            * ellipticity (float): Ellipticity angle in degrees.
            """
        # Get angles
        angles = self.rotator.Get_Position(units='rad')
        angle_H = angles[1] - self.polarization_calibration_angle0_H
        angle_Q = angles[2] - self.polarization_calibration_angle0_Q
        # Calculate parameters
        azimuth, ellipticity = PSG_angles_2_states(angle_H, angle_Q)
        # Transform units
        if units in UNITS_ANGLES:
            azimuth = azimuth / UNITS_ANGLES[units]
            ellipticity = ellipticity / UNITS_ANGLES[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))
        return azimuth, ellipticity


    ####################
    ## SAMPLE CONTROL
    ####################
    def Store_Sample_Position(self, pos=None, tol=1e-2):
        """Store the stated or current position of the sample to calculate planarity.

        Args:
            pos (list): Position to store. If None, it takes the current position. Default: None.
        """
        # Get position if not stated
        if pos is None:
            self.sapos = self.stage.Get_Position()
            
        # If there are still no positions, store it
        if len(self.sample_positions) < 1:
            self.sample_positions.append(pos)
            return
        
        # If X-Y position is present, replace it
        positions = np.array(self.sample_positions)
        
        if positions.ndim == 1:
            positions = positions[np.newaxis, :]
        dif = np.sqrt((pos[0] - positions[:,0])**2 + (pos[1] - positions[:,1])**2)
        if np.min(dif) < tol:
            print("Position replaced")
            ind = np.argmin(dif)
            self.sample_positions[ind] = pos
        else:
            self.sample_positions.append(pos)
            
        np.savez(CONF_CLUR['cal_folder'] + "\\" + CONF_CLUR['planar_cal_file'], sample_positions = self.sample_positions)
            
    def Print_Sample_Positions(self):
        """Print the stored positions of the sample.
        """
        if self.sample_positions == []: 
            print("No hay posiciones gardadas.")
            return
        print("Las posiciones guardadas son: ")
        for i, pos  in enumerate(self.sample_positions): 
            print("\nPos ", i, ": ", pos)
        
        return 
    
    
    def Clear_Sample_Positions(self, sel = []):
        """Store the stated or current position of the sample to calculate planarity.

        Args:
            sel (list): Positions to clear. Default: None.
        """
        if type(sel) in (float, int):
            sel = [sel]
        sel.sort(reverse = True)
        for i in sel: 
            self.sample_positions.pop(i)
            
        np.savez(CONF_CLUR['cal_folder'] + "\\" + CONF_CLUR['planar_cal_file'], sample_positions = self.sample_positions)
        
        return 
    
    def Clear_Sample_Positions_All(self):
        """Clear all of the stored positions of the sample.
        """
        self.sample_positions = []
        np.savez(CONF_CLUR['cal_folder'] + "\\" + CONF_CLUR['planar_cal_file'], sample_positions = self.sample_positions)
        
        return                                                       

    def Find_Sample_Position(self, dist=0.5, px = [1,1], units='mm', direction="centered", Npoints=10, go_pos = True, verbose=False, draw_std=False,store = True,  draw =False):
        """Find the focused Z position of the sample.

        Args:
            * dist (float): Distance to move in each axis. Default: 0.5.
            * units (str): Units of the distance. Default: 'mm'.
            * direction (str): Direction of the movement. Options: 'centered', 'positive', 'negative'. Default: 'centered'.
            * Npoints (int): Number of points to measure. Default: 10.
            * verbose (bool): If True, prints the position. Default: False.
            * go_pos (bool): If True, go to best position. Default: True.
            * store (bool): If True, save best position. Default: True.
            * draw_std (bool): If True, plots the std vs z position. Default: False.
            * draw (bool): If True, plots the image in best position. Default: False.
            """
        # Safety
        if direction not in ["centered", "up", "down"]:
            raise ValueError('Direction {} is not valid.'.format(direction))
        [dist] = adjust_units([dist], units, UNITS_POS)
        units = 'mm'
        
        # Stop video to avoid overwhelming the camera
        self.cam.Stop_Live()

        # Get array of positions
        pos = self.stage.Get_Position(units=units)
        Imax = px[0]*px[1]*256*2 # For limits in gaussian fitting
        # Imax = 1e10 # For limits in pàrabola fitting
        if direction == "centered":
            extra = np.linspace(-dist/2, dist/2, Npoints)
            limits = [[0, pos[2]-dist/2, 0], [Imax, pos[2]+dist/2, 0]]
        elif direction == "positive":
            extra = np.linspace(0, dist, Npoints)
            limits = [[0, pos[2], 0], [1e5, pos[2]+dist, 0]]
        elif direction == "negative":
            extra = np.linspace(-dist, 0, Npoints)
            limits = [[0, pos[2]-dist, 0], [1e5, pos[2], 0]]
        positions = np.array([pos + np.array([0, 0, extra[ind]]) for ind in range(Npoints)])
        limits = np.array(limits)

        # Get images and store std
        stds = np.zeros(Npoints)
        zetas = np.zeros(Npoints)
        self.cam.Start_Live(view=False)
        for ind, position in enumerate(positions):
            self.stage.Move_Absolute(pos=position, units=units, waiting='busy')
            sleep(CONF_CLUR["time_to_measure"])
            
            # stds[ind] = self.cam.Get_Image().std()
            std_individual = focus_measurement(self.cam.Get_Image(), px = px)
            
            stds[ind] = std_individual
            zetas[ind] = position[2]
        
        # PARABOLA
        # limits[0][2] = np.min(stds) * 0.8
        # limits[1][2] = np.max(stds) * 1.2
        
        # param0 = limits[0,:] + (limits[1,:] - limits[0,:]) * param0
        # result = least_squares(fun=error_parabola, x0=param0, args=(zetas[stds>0], stds[stds>0]), bounds=limits)
                
        # GAUSSIAN
        limits[0][2] = 0
        limits[1][2] = 0.1
        param0 = np.random.rand(3)* np.array((5000,dist/2, 0.1)) + np.array((0,pos[2], 0))
        
        result = least_squares(fun=error_gaussian, x0=param0, args=(zetas[stds>0], stds[stds>0]), bounds=limits)
        
        
        best_pos = result.x[1]
        # Store position
        if store: 
            self.Store_Sample_Position(pos=[pos[0], pos[1], best_pos])
            

        # Verbose
        if verbose:
            print("Z fit position: {:.3f} mm".format(best_pos))

        # Draw
        if draw_std:
            plt.plot(zetas, stds, 'ko', label="Experimental")
            
            # plt.plot(zetas, parabola(result.x, zetas), "b", label="Fit")
            
            plt.plot(zetas, gaussian(result.x, zetas), "b", label="Fit")
            plt.ylim([stds.min()*0.8, stds.max()*1.2])
            
            plt.legend()
            plt.plot([best_pos, best_pos], [0, result.x[0]], "r--")
            plt.xlabel('Z position (mm)')
            plt.ylabel('Std')
            plt.show()
        
        # Go to best position
        if go_pos or draw:        
            self.stage.Move_Absolute(pos=[pos[0], pos[1], best_pos], units=units, waiting='busy')
        
        # Capture best position image
        if draw: 
            self.cam.Start_Live(view=True)
            
        return best_pos


    def Set_Sample_Planar(self, verbose=False):
        """Set the sample to be planar.

        - Fit the points to plane Ax + By + Cz + D = 0.
        - Calculate three sets of valid positions (x, y, z):
            * (0, 0, z0)
            * (1, 0, z_x)
            * (0, 1, z_y)
        - Calculate angles from arctan(z_x - z0) and arctan(z_y - z0).
        
        Args:
            verbose (bool): If True, prints plane parameters. Default: False.
        """
        # Safety
        if len(self.sample_positions) < 3:
            raise ValueError('At least 3 positions are required.')
        # Extract coordinates 
        coor = np.array(self.sample_positions)   
        
        param0 = np.array([1,1,1,1])
        result = least_squares(fun=error_ajusteplano, x0 = param0, args=(coor[:,0], coor[:,1], coor[:,2]), xtol = 1e-10)
        # print(result.x)
        # # Fit to plane
        # def fun_plane(param, x, y, z):
        #     return param[0] * x + param[1] * y + param[2] * z + param[3]

        # param0 = np.array([0, 0, 0, 0])
        # result = least_squares(fun=fun_plane, 
        #                        x0=param0, 
        #                        args=(coor[:,0], coor[:,1], coor[:,2]))
        
        
        # Calculate angles
        z0 = -result.x[3] / result.x[2]
        z_x = -(result.x[0] + result.x[3]) / result.x[2]
        z_y = -(result.x[1] + result.x[3]) / result.x[2]
        Theta_x = np.arctan(z_x - z0)
        Theta_y = np.arctan(z_y - z0)
        # Calculate motor movement
        dx = -CONF_NEWPORT_8742["d_screw"]*np.tan(Theta_x) # displacement in x axis in mm
        dy = -CONF_NEWPORT_8742["d_screw"]*np.tan(Theta_y) # displacement in y axis in mm
        # dx = -CONF_NEWPORT_8742["d_screw"] * (z_x - z0) # displacement in x axis in mm
        # dy = -CONF_NEWPORT_8742["d_screw"] * (z_y - z0) # displacement in y axis in mm
        dz = -(dx + dy) # in order to avoid brownian movement in one directon
        # Move motor
        print("Mov. del gimbal: [", dz, dx+dz, dy+dz, ']')
        # self.gimbal.Move_Relative([-dz, -dx-dz, -dy-dz], units='mm', busy = True)
        
        # self.Clear_Sample_Positions_All()

    def Set_ROI_In_Beam(self, width=100, height=None):
        """Sets the camera ROI to the beam area.
        
        Args:
            * width (int): Width of the ROI. If None, defaults to height. Default: 100.
            * height (int): Height of the ROI. If None defaults to height. Default: None.
        """
        # Select height and width
        if height is None and width is None:
            raise ValueError('At least one of height or width must be stated.')
        elif height is None:
            height = width
        elif width is None:
            width = height

        # Calculate ROI
        img = self.cam.Get_Image()
        pos_x, pos_y = get_center(img)
        pos_x = int(np.max([0, pos_x - width/2]))
        pos_y = int(np.max([0, pos_y - height/2]))

        # Set ROI
        self.cam.Set_ROI(width=width, height=height, x=pos_x, y=pos_y)

        
        

    ####################
    ## SAMPLE MANAGEMENT
    ####################

    def New_Sample(self, name="Sample"):
        """Start a new sample.

        Args:
            * name (str): Name of the sample. Default: 'Sample'.
            """
        self.list_irradiations = []
        self.sample_name = name
        self.irradiation_index = 0

    def Save_Sample(self, folder=None, name=None):
        """Save the sample information.

        Args:
            * folder (str): Folder to save the sample. If None, uses the default folder. Default: None.
            """
        # Go to folder
        old_folder = os.getcwd()
        if folder is None:
            folder = CONF_CLUR["data_folder"]
        if folder != old_folder:
            try:
                os.chdir(folder)
            except:
                raise ValueError('Folder {} does not exist.'.format(folder))
        
        # Save info
        if name is None:
            name = self.sample_name
        with open(name + '.pkl', 'wb') as f:
            pickle.dump(self.list_irradiations, f)

        # Return to original folder
        os.chdir(old_folder)

    def Load_Sample(self, folder=None, name = ""):
        """Load a sample information.

        Args:
            * folder (str): Folder to load the sample. If None, uses the default folder. Default: None.
            * name (str): Name of the sample.
            """
        # Go to folder
        if folder is None:
            folder = CONF_CLUR["data_folder"]        
        old_folder = os.getcwd()
        if folder != old_folder:
            try:
                os.chdir(folder)
            except:
                raise ValueError('Folder {} does not exist.'.format(folder))
            
        # Correct name
        if len(name) > 4 and name[-4:] == ".pkl":
            name = name[:-4]
            
        # Load info
        with open(name + '.pkl', 'rb') as f:
            self.list_irradiations = pickle.load(f)
        self.irradiation_index = len(self.list_irradiations)

        # Return to original folder
        os.chdir(old_folder)


    ####################
    ## PROCESSING
    ####################

    def Process_Point(self, Npulses, pos=None, units_length='mm', power=None, units_power="W", name="Point_XXX", add_info_to_list=True, verbose=False):
        """Process a line in the sample.

        Args:
            * Npulses (int): Number of pulses of the point.
            * pos (list): Initial position of the line. If None, uses the current position. Default: None.
            * units_length (str): Units of length, pos and acel_length. Default: 'mm'.
            * power (float): Power to deliver. If None, uses the current power. Default: None.
            * units_power (str): Units of power. Default: 'W'.
            * name (str): Name of the irradiation. Substitues XXX string by the number of irradiation. Default: 'Point_XXX'.
            * add_info_to_list (bool): If True, adds the irradiation information to the list. Default: True.
            * verbose (bool): If True, prints the power percentage. Default: False.
            """
        # Info dictionary
        self.irradiation_index = self.irradiation_index + 1
        dict_irrad = {}
        dict_irrad["type"] = "Point"
        dict_irrad["index"] = self.irradiation_index
        dict_irrad["name"] = name.replace("XXX", "{:03d}".format(self.irradiation_index))

        # Safety and transform units
        if pos is None:
            pos = self.stage.Get_Position(units=units_length)
        else:
            pos = np.array(pos)
            # Move to real initial position
            self.stage.Move_Absolute(pos=pos, units=units_length, waiting='busy')
        if power is not None:
            self.Set_Power(power=power, units=units_power)
        else:
            power = self.Calculate_Power(units=units_power)

        [pos] = adjust_units([pos], units_length, UNITS_POS)
        units_length = 'mm'
        [power] = adjust_units([power], units_power, UNITS_POWER)
        units_power = 'W'

        # Save some data
        dict_irrad["number_pulses"] = Npulses
        dict_irrad["pos"] = pos
        dict_irrad["power"] = power
        
        # Close shutter just in case
        self.stage.Close_Shutter()

        # Calculate irradiation time
        Itime = Npulses / CONF_CLUR["rep_rate"]

        # Open shutter and close after irradiation time
        self.stage.Open_Shutter(wait_time=Itime)
        self.stage.Close_Shutter()

        # Save irradiation in list
        if add_info_to_list:
            self.list_irradiations.append(dict_irrad)
            self.Save_Sample(folder=CONF_CLUR["cal_folder"], name=CONF_CLUR["sample_list_file"])

        # Verbose
        if verbose:
            print_point_irradiation(dict_irrad)


    def Process_Line(self, length, pos=None, acel_length=0, units_length='mm', angle=0, units_angle="rad", velocity=None, units_vel="mm/s", power=None, units_power="W", return_to_init=False, name="Line_XXX", add_info_to_list=True, verbose=False):
        """Process a line in the sample.

        Args:
            * length (float): Length of the line.
            * pos (list): Initial position of the line. If None, uses the current position. Default: None.
            * acel_length (float): Safety length to accelerte the motor before starting to irradiate. Default: 0.
            * units_length (str): Units of length, pos and acel_length. Default: 'mm'.
            * angle (float): Angle of the line. Default: 0.
            * units_angle (str): Units of angle. Default: 'rad'.
            * velocity (float): Process velocity. If None, uses the current velocity. Default: None.
            * units_vel (str): Units of velocity. Default: 'mm/s'.
            * power (float): Power to deliver. If None, uses the current power. Default: None.
            * units_power (str): Units of power. Default: 'W'.
            * return_to_init (bool): If True, returns to the initial position. Default: False.
            * name (str): Name of the irradiation. Substitues XXX string by the number of irradiation. Default: 'Line_XXX'.
            * add_info_to_list (bool): If True, adds the irradiation information to the list. Default: True.
            * verbose (bool): If True, prints the power percentage. Default: False.
            """
        # Info dictionary
        self.irradiation_index = self.irradiation_index + 1
        dict_irrad = {}
        dict_irrad["type"] = "Line"
        dict_irrad["index"] = self.irradiation_index
        dict_irrad["name"] = name.replace("XXX", "{:03d}".format(self.irradiation_index))

        # Safety and transform units
        if pos is not None:
            pos = np.array(pos)
            

        [angle] = adjust_units([angle], units_angle, UNITS_ANGLES)
        units_angle = 'rad'
        [length, acel_length, pos] = adjust_units([length, acel_length, pos], units_length, UNITS_POS)
        units_length = 'mm'
        [velocity] = adjust_units([velocity], units_vel, UNITS_VEL)
        units_vel = 'mm/s'
        if velocity is None:
            velocity = self.stage.Get_Velocity(units=units_vel)
        velocity = velocity[0] * np.cos(angle) + velocity[1] * np.sin(angle)
        [power] = adjust_units([power], units_power, UNITS_POWER)
        units_power = 'W'
        
        # Save some data
        dict_irrad["length"] = length
        dict_irrad["angle"] = angle
        dict_irrad["acel_length"] = acel_length
        dict_irrad["velocity"] = velocity
        
        # Close shutter just in case
        self.stage.Close_Shutter()
        
        # Save initial position and get Z position
        pos_current = self.stage.Get_Position(units=units_length)
        if pos is None:
            pos = pos_current
        dict_irrad["pos"] = pos

        # Calculate lengths due to angle
        length_x = length * np.cos(angle)
        length_y = length * np.sin(angle)
        acel_length_x = acel_length * np.cos(angle)
        acel_length_y = acel_length * np.sin(angle)
        vel_x = velocity * np.cos(angle)
        vel_y = velocity * np.sin(angle)
        length_vect = np.array([length_x, length_y, 0])
        acel_length_vect = np.array([acel_length_x, acel_length_y, 0])
        vel_vect = np.array([vel_x, vel_y, 0])

        # Move to real initial position
        pos_initial = self.stage.Move_Absolute(pos=pos-acel_length_vect, units=units_length, waiting='busy')

        # Set power
        if power is not None:
            self.Set_Power(power=power, units=units_power)
        else:
            power = self.Calculate_Power(units=units_power)
        dict_irrad["power"] = power

        # Start moving along the line
        self.stage.Set_Velocity(vel=vel_vect, units=units_vel)
        self.stage.Move_Absolute(pos=pos+length_vect+acel_length_vect, units=units_length, move_time=None, waiting='NO')

        # Open shutter when the acceleration length is done
        cond = True
        while cond:
            pos_current = self.stage.Get_Position(units=units_length)
            cond1 = np.linalg.norm(pos_current - pos_initial) < np.linalg.norm(acel_length_vect) 
            cond2 = self.stage.Is_Moving()
            cond = cond1 and cond2
        self.stage.Open_Shutter()

        # Close shutter when the line is done
        cond = True
        while cond:
            pos_current = self.stage.Get_Position(units=units_length)
            cond1 = np.linalg.norm(pos_current - pos) < np.linalg.norm(length_vect) 
            cond2 = self.stage.Is_Moving()
            cond = cond1 and cond2
        self.stage.Close_Shutter()

        # Return to initial position
        if return_to_init:
            self.stage.Move_Absolute(pos=pos, units=units_length, waiting='busy')

        # Save irradiation in list
        if add_info_to_list:
            self.list_irradiations.append(dict_irrad)
            self.Save_Sample(folder=CONF_CLUR["cal_folder"], name=CONF_CLUR["sample_list_file"])

        # Verbose
        if verbose:
            print(print_line_irradiation(dict_irrad))
            
            
    def Process_Rectangle(self, length, width, sep, pos=None, acel_length=0, units_length='mm', angle=0, units_angle="rad", velocity=None, units_vel="mm/s", power=None, units_power="W", return_to_init=False, name="Rectangle_XXX", add_info_to_list=True, verbose=False):
        """Process a rectangle in the sample by writing several lines one below the other.

        Args:
            * length (float): Length of each line of the rectangle.
            * width (float): width of the rectangle.
            * sep (float): Separation between lines of the rectangle.
            * pos (list): Position of the first corner of the rectangle. If None, uses the current position. Default: None.
            * acel_length (float): Safety length to accelerte the motor before starting to irradiate lines. Default: 0.
            * units_length (str): Units of length, pos and acel_length. Default: 'mm'.
            * angle (float): Angle of the lines. Default: 0.
            * units_angle (str): Units of angle. Default: 'rad'.
            * velocity (float): Line process velocity. If None, uses the current velocity. Default: None.
            * units_vel (str): Units of velocity. Default: 'mm/s'.
            * power (float): Power to deliver. If None, uses the current power. Default: None.
            * units_power (str): Units of power. Default: 'W'.
            * return_to_init (bool): If True, returns to the initial position. Default: False.
            * name (str): Name of the irradiation. Substitues XXX string by the number of irradiation. Default: 'Rectangle_XXX'.
            * add_info_to_list (bool): If True, adds the irradiation information to the list. Default: True.
            * verbose (bool): If True, prints the power percentage. Default: False.
            """
        # Info dictionary
        self.irradiation_index = self.irradiation_index + 1
        dict_irrad = {}
        dict_irrad["type"] = "Rectangle"
        dict_irrad["index"] = self.irradiation_index
        dict_irrad["name"] = name.replace("XXX", "{:03d}".format(self.irradiation_index))

        # Safety and transform units
        if pos is None:
            pos = self.stage.Get_Position(units=units_length)
        else:
            pos = np.array(pos)
        if velocity is None:
            velocity = self.stage.Get_Velocity(units=units_vel)

        [angle] = adjust_units([angle], units_angle, UNITS_ANGLES)
        units_angle = 'rad'
        [length, acel_length, pos, width, sep] = adjust_units([length, acel_length, pos, width, sep], units_length, UNITS_POS)
        units_length = 'mm'
        [velocity] = adjust_units([velocity], units_vel, UNITS_VEL)
        units_vel = 'mm/s'
        [power] = adjust_units([power], units_power, UNITS_POWER)
        units_power = 'W'
        
        # Save some data
        dict_irrad["length"] = length
        dict_irrad["width"] = width
        dict_irrad["angle"] = angle
        dict_irrad["acel_length"] = acel_length
        dict_irrad["velocity"] = velocity
        dict_irrad["pos"] = pos
        dict_irrad["separation"] = sep

        # Set power
        if power is not None:
            self.Set_Power(power=power, units=units_power)
        else:
            power = self.Calculate_Power(units=units_power)
        dict_irrad["power"] = power

        # Calculate number of lines and separation vector
        Nlines = int(ceil(width / sep))
        sep_vect = np.array([sep * np.sin(angle), -np.cos(angle) * sep, 0])
        dict_irrad["lines"] = Nlines

        # Irradiate lines
        for ind in range(Nlines):
            position = pos + ind * sep_vect
            self.Process_Line(length=length, pos=position, acel_length=acel_length, units_length='mm', angle=angle, units_angle="rad", velocity=velocity, units_vel="mm/s", power=None, return_to_init=False, add_info_to_list=False, verbose=False)
            print("{} de {}".format(ind,Nlines), end='\r')

        # Return to initial position
        if return_to_init:
            self.stage.Move_Absolute(pos=pos, units=units_length, waiting='busy')

        # Save irradiation in list
        if add_info_to_list:
            self.list_irradiations.append(dict_irrad)
            self.Save_Sample(folder=CONF_CLUR["cal_folder"], name=CONF_CLUR["sample_list_file"])

        # Verbose
        if verbose:
            print(print_square_irradiation(dict_irrad))

    def Process_Origin(self):
        """Irradiates a clearly identifiable pattern in the sample."""
        # Process two lines in L shape
        self.Process_Line(length=0.5, pos=None, acel_length=0, units_length='mm', angle=0, units_angle="rad", velocity=[0.1,0.1], units_vel="mm/s", power=0.1, return_to_init=True, add_info_to_list=False, verbose=False)
        self.Process_Line(length=0.5, pos=None, acel_length=0, units_length='mm', angle=90, units_angle="deg", velocity=[0.1,0.1], units_vel="mm/s", power=0.1, return_to_init=True, add_info_to_list=False, verbose=False)

        # Set this position as position reference
        self.stage.Set_Reference()

        # Save to irradiations list
        dict_irrad = {}
        dict_irrad["type"] = "Origin"
        dict_irrad["pos"] = self.stage.Get_Position(units='mm')



##################
## FIT FUNCTIONS
##################

def parabola(param, x):
    return -param[0] * (x - param[1])**2 + param[2]

def error_parabola(param, x, y):
    return y - parabola(param, x)

def gaussian(param, x):
    return param[0] * np.exp(-(x-param[1])**2/param[2]**2)

def error_gaussian(param, x, y):
    return y - gaussian(param, x)

def model_ajusteplano(par, x, y): 
    """Funcion z = -(Ax +By+D)/C with parameters."""
    z = -(par[0]*x +par[1]*y + par[3])/par[2]
    return z
        
def error_ajusteplano(par, x, y, z_exp): 
    """Function that serves as optimization for z = -(Ax +By+D)/C."""
    z_model = model_ajusteplano(par, x, y)
    dif = np.abs((z_exp - z_model))
    return dif


##################
## AUXILIARY FUNCTIONS
##################

def print_point_irradiation(dict_irrad):
    for key in KEYS_POINT:
        print(key, ": ", dict_irrad[key], UNITS_KEY[key])

def print_line_irradiation(dict_irrad):
    for key in KEYS_LINE:
        print(key, ": ", dict_irrad[key], UNITS_KEY[key])

def print_square_irradiation(dict_irrad):
    for key in KEYS_RECTANGLE:
        print(key, ": ", dict_irrad[key], UNITS_KEY[key])

def adjust_units(vars, units, dict_units):
    """Function to transform an array of variables to the correct units.
    
    Args:
        vars (list): List of variables.
        units (string or None): Units of the variables. If None, no transformation is performed.
        dict units (dict): Dictionary with the units transformation.
        
    Returns:
        List of transformed variables.
    """
    if units is not None:
        if units in dict_units:
            for ind in range(len(vars)):
                if vars[ind] is not None:
                    vars[ind] = vars[ind] * dict_units[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))

    return vars

def get_center(im):
    """Gets the 1st moment tenter of an image.

    Args:
        im (np.ndarray): Image array.

    Returns:
        pos_x, pos_y (float): Center of the image.
    """

    x = np.sum(im*np.indices(im.shape)[0])/np.sum(im)
    y = np.sum(im*np.indices(im.shape)[1])/np.sum(im)
    pos_x, pos_y = int(np.round(x)), int(np.round(y))
    # print(pos_x, pos_y)
    return (pos_x, pos_y)

def focus_measurement(im, px = [1,1]): 
    """Function to localize the focus.
    
    Args:
        im (array): Image with the focus.
        
    Returns:
        (x,y) positions in the image 
    """
    # Get center
    im = np.where(im < CONF_CLUR['cam_integration_threshold'], 0, im)
    if im.max() == 0: 
        return 0
    
    pos_x, pos_y = get_center(im)
    
    # Calcular los índices del subarray
    start_row = max(0, pos_x - px[0]//2)
    end_row = min(im.shape[0], pos_x + px[0]//2 + px[0] % 2)
    start_col = max(0, pos_y - px[1]//2)
    end_col = min(im.shape[1], pos_y + px[1]//2 + px[1] % 2)

    # Obtener el subarray
    sub_im = im[start_row:end_row, start_col:end_col]

    return np.sum(sub_im)
