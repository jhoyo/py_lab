"""This file contains useful functions to calibrate and operate SLM setups."""

from copy import deepcopy
import os
import time
from IPython.display import clear_output

import numpy as np

from scipy.signal import find_peaks
from scipy.optimize import least_squares, minimize, curve_fit
from datetime import datetime
import shutil
import matplotlib.pyplot as plt
from matplotlib import cm

from diffractio.scalar_masks_XY import Scalar_mask_XY
from py_lab.utils import (degrees, Load_Image, Process_Window, Unwrap,
                          Filter_Curves, Make_Non_Negative,
                          PSG_states_2_angles, PSG_angles_2_states,
                          PSA_states_2_angles, PSA_angles_2_states,calculate_polarimetry_angles)
from py_pol.jones_vector import Jones_vector, degrees
from py_pol.jones_matrix import Jones_matrix
from py_pol.mueller import Mueller, Stokes
from py_lab.config import CONF_SLM1, PSG_az, PSG_el, PSA_az, PSA_el, angle_P1, angle_P2, angle_Q1, angle_Q2,CONF_SLM2#, Mueller_indices
from py_lab.motor import Motor_Multiple
from py_lab.setups.polarimeter_utils import Calculate_Mueller, Msystem_From_Dict

COLORS = plt.rcParams['axes.prop_cycle'].by_key()['color']
LINESTYLES = ['solid', 'dashed', 'dotted', 'dashdot']

# Element angles
# angles_0 = CONF_SLM1['angles_0']
# azimuth_source = CONF_SLM1['azimuth_source']
# ellipticity_source = CONF_SLM1['ellipticity_source']
# def_mirrors = CONF_SLM1['mirrors']

angles_0 = CONF_SLM2['angles_0']
azimuth_source = CONF_SLM2['azimuth_source']
ellipticity_source = CONF_SLM2['ellipticity_source']
def_mirrors = CONF_SLM2['mirrors']



# Pre-allocated lists
list_angles = []
list_ind = []
list_ort = []
no_elements = -np.ones(4)

# Constants
r3 = np.sqrt(3)

###################################
# PHASE CALIBRATION
###################################


def Phase_Calibration_Measurement(slm,
                                  cam,
                                  filename="Biprisma_V_{}.npz",
                                  levels=None,
                                  Naverage=1,
                                  wait_time=1e-2,
                                  return_vars=False):
    """Function to perform the measurements to calculate the global phase of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        cam (Camera): Object of the camera.
        filename (str): Template for filenames. Must contain at least a {}. Default: "Biprisma_V_{}.npz".
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.

    Returns (only if return_vars is True):
        images (list): List of arrays of the images.
    """
    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)
    else:
        levels = np.array(levels)
        if np.max(levels) > 1:
            levels = levels / np.max(levels)

    # Main loop
    images = []
    for ind, val in enumerate(levels):
        # Print advancement
        clear_output(wait=False)
        print('Image {} of {}'.format(ind + 1, levels.size))

        # Salvarla de otras maneras
        if return_vars:
            images.append(
                Phase_Calibration_Single_Measurement(
                    slm=slm,
                    cam=cam,
                    level=val,
                    return_vars=True,
                    Naverage=Naverage,
                    wait_time=wait_time))
        else:
            filename_def = filename.format(ind)
            Phase_Calibration_Single_Measurement(
                slm=slm,
                cam=cam,
                level=val,
                filename=filename_def,
                Naverage=Naverage,
                wait_time=wait_time)

    # Return
    if return_vars:
        return images
    

def Phase_Calibration_Movement(slm,
                                  cam,
                                  filename="Biprisma_V_{}.npz",
                                  levels=None,
                                  Naverage=1,
                                  wait_time=1e-2,
                                  return_vars=False):
    """Function to perform the measurements to calculate the global phase of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        cam (Camera): Object of the camera.
        filename (str): Template for filenames. Must contain at least a {}. Default: "Biprisma_V_{}.npz".
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.

    Returns (only if return_vars is True):
        images (list): List of arrays of the images.
    """
    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)
    else:
        levels = np.array(levels)
        if np.max(levels) > 1:
            levels = levels / np.max(levels)

    # Main loop
    images = []
    for ind, val in enumerate(levels):
        # Print advancement
        clear_output(wait=False)
        print('Image {} of {}'.format(ind + 1, levels.size))

        # Salvarla de otras maneras
        if return_vars:
            images.append(
                Phase_Calibration_Single_Measurement(
                    slm=slm,
                    cam=cam,
                    level=val,
                    return_vars=True,
                    Naverage=Naverage,
                    wait_time=wait_time))
        else:
            filename_def = filename.format(ind)
            filename_ref = "Biprisma_V_{}_0.npz".format(ind) 
            Phase_Calibration_Single_Measurement(
                slm=slm,
                cam=cam,
                level=0,
                filename=filename_ref,
                Naverage=Naverage,
                wait_time=wait_time)
    
            Phase_Calibration_Single_Measurement(
                slm=slm,
                cam=cam,
                level=val,
                filename=filename_def,
                Naverage=Naverage,
                wait_time=wait_time)

    # Return
    if return_vars:
        return images


def Phase_Calibration_Single_Measurement(slm,
                                         cam,
                                         level,
                                         filename=None,
                                         Naverage=1,
                                         wait_time=1e-2,
                                         return_vars=False):
    """Function to perform a single phase measurement SLM.

    Args:
        slm (SLM): Object of the SLM.
        cam (Camera): Object of the camera.
        level (number): Level of the SLM to measure.
        filename (str or None): Name of the files to save. If None, no file is created. Default: None.
        Naverage (int): Number of measurements to average. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.

    Returns (only if return_vars is True):
        image (np.ndarray): Array of the image.
    """
    # Ceate some variables
    gray_double = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)
    level_min = 0
    # Crear la mascara con el SLM
    #level_max = np.sqrt(level)
    level_max = level
    #gray_double.gray_scale(
    #   num_levels=2, levelMin=level_min, levelMax=level_max) #Calibración Biprisma: Máscara de dos niveles.
    gray_double.one_level(level=level_max) #Calibración interferómetro: Máscara un solo nivel.

    # Mandarla al SLM
    slm.Send_Image(image=gray_double, kind='amplitude', norm=1)

    # Tomar la imagen
    for indM in range(Naverage):
        if indM == 0:
            time.sleep(wait_time)
            image = cam.Get_Image()
        else:
            time.sleep(wait_time)
            image = image + cam.Get_Image()
    image = image / Naverage

    # Salvarla de otras maneras
    if filename:
        np.savez(filename, image=image, Naverage=Naverage, wait_time=wait_time)

    # Return
    if return_vars:
        return image

def fitting_0(x, a, b,c):
    return a*x**3 + b*x**2 + c*x 



def Phase_Calibration_Analysis(levels,
                               files,
                               var_name='image',
                               path=None,
                               method='correlation_peaks',
                               unwrap=np.pi,
                               threshold=0.5,
                               window=('center', 'left', 15, 'full'),
                               peak_height=None,
                               peak_distance=None,
                               fit_order=3,
                               weighted=True,
                               draw=False,
                               print_completion=False,
                               filename='Phase_calibration.npz',
                               jones_calibration=None,
                               method_jones="hoyo",
                               mirrors=def_mirrors,
                               angles=no_elements,
                               substract_zero=True):
    """Function that loads some interference fringes data produced to measure the phase shift produced by the SLM. For now, the fringes must be vertical. First image will be used as reference (so its calculated phase will be 0).

    Args:
        levels (iterable): List of gray levels tested.
        files (iterable): List with file names where the data is stored.
        var_name (string): If data is saved in npz files, var_name is the keyword to access the data. Default: 'image'.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        unwrap (float or None): If not None, the phase is unwrapped using py_lab.utils function Unwrap with this variable as step. Default: np.pi.
        threshold (float): Threshold of Unwrap function. Default: 0.5.
        method (str): Method to calculate the phase shift. Default: 'correlation_peaks'.
            fitshift: Measures peak positions. The fringes period and shift are calculated from the slope and intercept of the linear fit respectively.
            correlation_peaks: Calculates the correlation curve and then calculates the peak displacement of maxima and minima.
        window (list or tuple): If the method uses a window, this variable specifies it. It must be a 4-element list like this: [row start, column start, row span, column span]. One or more of the elements can be iterables of the same length. In that case, several windows will be used. Default: ('center', 'left', 15, 'full').
            Starts (iterable of int or str): If int, it is the index. Some strings are allowed: 'top', 'bottom', 'center', 'left', 'right'.
            Span (int or str): If int, it is the number of rows/collumns. Some strings are also accepted: 'full', 'half'.
        peak_height (float): Peak height for scipy.find_peaks. Default: None.
        peak_distance (int): Minimum pixels between peaks for scipy.find_peaks. Default: None.
        fit_order (int): Polynomial fit order. Default: 3.
        weighted (bool): If true and more than 1 window is provided, the averages and errors are weighted respect to the errors given by the linear regressions. Default: True.
        draw (bool): If True, draws the calculated phase. Default: False.
        print_completion (bool): If True, prints the completion of the program. Default: False.
        filename (str): Filename to save the data. Default: 'Phase_calibration.npz'.
        jones_calibration (str): Filename of the Jones calibration. Default: None.
        method_jones (str): Method for calculating the phase. Default: "hoyo".
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively.
        angles (list): List of element angles. If negative, the element is not present. Default: -np.ones(4).
        substract_zero (bool): If True, the origin of angles is substracted. Default: True.

    Returns:
        phase (np.ndarray): Calculated phase shift. Averaged if several windows are set.
        phase_error (np.ndarray or None): If more than one window is used, the mean error is calculated.
        phase_fit (np.ndarray): Fit phase.
        period (np.ndarray): Period of the fringes.
    """
    start_time = time.time()
    # Go to path
    #if path:
    #    old_path = os.getcwd()
    #    os.chdir(path)

    # Load first image to take shape
    image = Load_Image(files[0], var_name=[var_name])
    shape = image.shape

    # Process the window
    if method in ('fitshift', 'correlation_peaks'):
        p_window = Process_Window(window, shape)
        #print(p_window)
        Lw = len(p_window)
    else:
        Lw = 1

    # Loop through files
    levels = np.array(levels)
    phase = np.zeros((levels.size, Lw))
    phase_error = np.ones((levels.size, Lw))
    period = np.zeros((levels.size, Lw))
    period_error = np.ones((levels.size, Lw))
    intercept_0 = np.zeros(Lw)
    intercept_0_error = np.zeros(Lw)
    norm_factor = None
    image_win_0 = []
    for indV, V in enumerate(levels):
        # Completion
        if print_completion:
            clear_output(wait=True)
            print('Iteration {} out of {}.'.format(indV, levels.size))
        # Load data
        image = Load_Image(files[indV], var_name=[var_name])

        # Switch method
        if method in ('fitshift', 'correlation_peaks'):
            # Iterate inside window
            for ind, win in enumerate(p_window):
                # Extract window
                image_win = image[win[0]:win[1], win[2]:win[3]]
                image_win = np.mean(image_win, axis=0)
                # In correlation method, calculate the correlation
                if method == 'correlation_peaks':
                    image_win = (image_win - image_win.min()) / \
                        (image_win.max() - image_win.min())
                    if norm_factor is None:
                        ones = np.ones_like(image_win)
                        norm_factor = np.correlate(ones, ones, 'same')
                    if indV == 0:
                        image_win_0.append(image_win)
                    image_win = np.correlate(image_win, image_win_0[ind],
                                             'same') / norm_factor
                # plt.figure(figsize=(12, 4))
                # plt.plot(image_win)
                # plt.draw()
                # Find maxima and minima
                maxS, _ = find_peaks(
                    image_win, height=peak_height, distance=peak_distance)
                minS, _ = find_peaks(
                    -image_win, height=peak_height, distance=peak_distance)
                # Correct a little and sort
                if maxS[0] == 0:
                    maxS = maxS[1:]
                if maxS[-1] == image_win.size - 1:
                    maxS = maxS[:-1]
                if minS[0] == 0:
                    minS = minS[1:]
                if minS[-1] == image_win.size - 1:
                    minS = minS[:-1]
                if maxS[0] < minS[0]:
                    maxS = maxS[1:]
                indices = np.sort(np.concatenate((maxS, minS)))
                # plt.plot(indices, image_win[indices], 'bo')

                # Fit and extract param
                x = np.arange(indices.size)
                #print(x)
                coef, cov_matrix = np.polyfit(x, indices, 1, cov=True)
                period[indV, ind] = coef[0] * 2
                period_error[indV, ind] = np.sqrt(cov_matrix[0, 0]) * 2
                if indV == 0:
                    intercept_0[ind] = coef[1]
                    intercept_0_error[ind] = cov_matrix[1, 1]
                else:
                    phase_now = 2 * np.pi * \
                        ((coef[1] - intercept_0[ind]) / period[0, ind])
                    phase[indV, ind] = phase_now
                    phase_error[indV, ind] = 2 * np.pi * np.sqrt(
                        cov_matrix[1, 1] *
                        (phase_now / coef[1])**2 + period_error[0, ind] *
                        (phase_now / coef[0])**2 + intercept_0_error[ind] *
                        (phase_now / intercept_0[ind])**2)
                # plt.figure(figsize=(8, 8))
                # plt.plot(x, indices, '+')
                # plt.plot(x, coef[0] * x + coef[1])
                difer = np.linalg.norm(coef[0] * x + coef[1] -
                                       indices) / x.size
                # print(period[ind], intercept_0[ind]/degrees, difer)

    # Unwrap
    if unwrap is not None:
        phase = Unwrap(
            phase, step=unwrap, axis=0, threshold=threshold, progresive=False)

    # Average to calculate phase
    if weighted and (Lw > 1):
        phase_av = np.average(phase, axis=1, weights=phase_error)
        period_mean = np.average(period, axis=1, weights=period_error)

        _, phase_aux = np.meshgrid(np.ones(Lw), phase_av)
        _, period_aux = np.meshgrid(np.ones(Lw), period_mean)
        phase_error = np.sqrt(
            np.average((phase - phase_aux)**2, weights=phase_error,
                       axis=1)) / np.sqrt(Lw)
        phase_error[0] = 0
        period_error = np.sqrt(
            np.average((period - period_aux)**2, weights=period_error,
                       axis=1)) / np.sqrt(Lw)
    else:
        phase_av = np.mean(phase, axis=1)
        period_mean = np.mean(period, axis=1)

        phase_error = np.std(phase, axis=1) / np.sqrt(Lw)
        period_error = np.std(period, axis=1) / np.sqrt(Lw)

    # Fit
    coef = np.polyfit(levels, phase_av, fit_order)
    coef, cov = curve_fit(fitting_0,levels,phase_av)
    phase_fit = fitting_0(levels,*coef)
    #phase_fit = np.zeros_like(phase_av)
    #for ind, val in enumerate(coef):
    #    phase_fit += levels**(coef.size - ind - 1) * val

    # Substract Jones contribution
    if jones_calibration is not None:
        # Load calibration
        data = np.load("Analysis_calibration.npz", allow_pickle=True)
        components = data["components_" + method_jones]
        # Create matrices
        Jslm = Components_To_Matrix(components)
        E0 = Jones_vector().general_azimuth_ellipticity(
            azimuth=azimuth_source,
            ellipticity=ellipticity_source)
        # Mirrors
        J_mirror = Jones_matrix().mirror()
        if mirrors[0]:
            Jslm = Jslm * J_mirror
        if mirrors[1]:
            Jslm = J_mirror * Jslm
        # 0 angle
        if substract_zero:
            cond = angles > 0
            angles[cond] = (angles[cond] - angles_0[cond]) % np.pi
        # Calculation
        Intensity, phase_jones = Calculate_Transmission(angles, Jslm, E0=E0)
        # Compensation
        phase_av = phase_av - phase_jones
        phase_av = phase_av - phase_av[0]

    # Draw if required
    if draw:
        plt.figure(figsize=(12, 9))
        plt.plot(levels, phase_av / degrees, 'b', linewidth=0.5)
        plt.plot(levels, phase_fit / degrees, 'r--', linewidth=2)
        """""
        if Lw > 1:
            if levels.size <= 10:
                plt.errorbar(
                    levels,
                    phase_av / degrees,
                    phase_error / degrees,
                    fmt='ob')
            else:
                plt.errorbar(
                    levels[::5],
                    phase_av[::5] / degrees,
                    phase_error[::5] / degrees,
                    fmt='ob')
        """""
        plt.xlabel('SLM gray level')
        plt.ylabel('Phase (deg)')
        plt.title('Phase shift ({} method)'.format(method))
        print('Mean period is {:.2f} +- {:.2f} pixels'.format(
            np.mean(period_mean), np.mean(period_error)))

    # Save phase file
    if filename:
        print("File {} saved in {}.".format(filename, os.getcwd()))
        np.savez(
            filename,
            phase_av=phase_av,
            phase_error=phase_error,
            period=period,
            phase_fit=phase_fit)

    # Return to original folder
    #if path:
    #    os.chdir(old_path)

    # Print time
    print('Elapsed time is {:.1f} s'.format(time.time() - start_time))

    return phase_av, phase_error, phase_fit, period



def Phase_Calibration_Analysis_Movement(levels,
                               files,
                               files_t,
                               var_name='image',
                               path=None,
                               method='correlation_peaks',
                               unwrap=np.pi,
                               threshold=0.5,
                               window=('center', 'left', 15, 'full'),
                               peak_height=None,
                               peak_distance=None,
                               fit_order=3,
                               weighted=True,
                               draw=False,
                               print_completion=False,
                               filename='Phase_calibration.npz',
                               jones_calibration=None,
                               method_jones="hoyo",
                               mirrors=def_mirrors,
                               angles=no_elements,
                               substract_zero=True):
    """Function that loads some interference fringes data produced to measure the phase shift produced by the SLM. For now, the fringes must be vertical. First image will be used as reference (so its calculated phase will be 0).

    Args:
        levels (iterable): List of gray levels tested.
        files (iterable): List with file names where the data is stored.
        var_name (string): If data is saved in npz files, var_name is the keyword to access the data. Default: 'image'.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        unwrap (float or None): If not None, the phase is unwrapped using py_lab.utils function Unwrap with this variable as step. Default: np.pi.
        threshold (float): Threshold of Unwrap function. Default: 0.5.
        method (str): Method to calculate the phase shift. Default: 'correlation_peaks'.
            fitshift: Measures peak positions. The fringes period and shift are calculated from the slope and intercept of the linear fit respectively.
            correlation_peaks: Calculates the correlation curve and then calculates the peak displacement of maxima and minima.
        window (list or tuple): If the method uses a window, this variable specifies it. It must be a 4-element list like this: [row start, column start, row span, column span]. One or more of the elements can be iterables of the same length. In that case, several windows will be used. Default: ('center', 'left', 15, 'full').
            Starts (iterable of int or str): If int, it is the index. Some strings are allowed: 'top', 'bottom', 'center', 'left', 'right'.
            Span (int or str): If int, it is the number of rows/collumns. Some strings are also accepted: 'full', 'half'.
        peak_height (float): Peak height for scipy.find_peaks. Default: None.
        peak_distance (int): Minimum pixels between peaks for scipy.find_peaks. Default: None.
        fit_order (int): Polynomial fit order. Default: 3.
        weighted (bool): If true and more than 1 window is provided, the averages and errors are weighted respect to the errors given by the linear regressions. Default: True.
        draw (bool): If True, draws the calculated phase. Default: False.
        print_completion (bool): If True, prints the completion of the program. Default: False.
        filename (str): Filename to save the data. Default: 'Phase_calibration.npz'.
        jones_calibration (str): Filename of the Jones calibration. Default: None.
        method_jones (str): Method for calculating the phase. Default: "hoyo".
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively.
        angles (list): List of element angles. If negative, the element is not present. Default: -np.ones(4).
        substract_zero (bool): If True, the origin of angles is substracted. Default: True.

    Returns:
        phase (np.ndarray): Calculated phase shift. Averaged if several windows are set.
        phase_error (np.ndarray or None): If more than one window is used, the mean error is calculated.
        phase_fit (np.ndarray): Fit phase.
        period (np.ndarray): Period of the fringes.
    """
    start_time = time.time()
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Load first image to take shape
    image = Load_Image(files[0], var_name=[var_name])
    shape = image.shape

    # Process the window
    if method in ('fitshift', 'correlation_peaks'):
        p_window = Process_Window(window, shape)
        #print(p_window)
        Lw = len(p_window)
        #print(Lw)
    else:
        Lw = 1

    # Loop through files
    levels = np.array(levels)
    phase = np.zeros((levels.size, Lw))
    phase_error = np.ones((levels.size, Lw))
    period = np.zeros((levels.size, Lw))
    period_error = np.ones((levels.size, Lw))
    intercept_0 = np.zeros(Lw)
    intercept_0_error = np.zeros(Lw)
    norm_factor = None
    image_win_0 = []
    for indV, V in enumerate(levels):
        # Completion
        if print_completion:
            clear_output(wait=True)
            print('Iteration {} out of {}.'.format(indV, levels.size))
        # Load data
        image = Load_Image(files[indV], var_name=[var_name])
        image_0 = Load_Image(files_t[indV], var_name=[var_name])
        # Switch method
        if method in ('fitshift', 'correlation_peaks'):
            # Iterate inside window
            for ind, win in enumerate(p_window):
                # Extract window
                image_win = image[win[0]:win[1], win[2]:win[3]]
                image_win = np.mean(image_win, axis=0)

                image_win_0 = image_0[win[0]:win[1], win[2]:win[3]]
                image_win_0 = np.mean(image_win_0, axis=0)
                # In correlation method, calculate the correlation
                if method == 'correlation_peaks':
                    image_win = (image_win - image_win.min()) / \
                        (image_win.max() - image_win.min())
                    image_win_0 = (image_win_0 - image_win_0.min()) / \
                        (image_win_0.max() - image_win_0.min())
                    if norm_factor is None:
                        ones = np.ones_like(image_win)
                        norm_factor = np.correlate(ones, ones, 'same')
                    image_win = np.correlate(image_win, image_win_0,
                                             'same') / norm_factor
                # plt.figure(figsize=(12, 4))
                # plt.plot(image_win)
                # plt.draw()
                # Find maxima and minima
                maxS, _ = find_peaks(
                    image_win, height=peak_height, distance=peak_distance)
                minS, _ = find_peaks(
                    -image_win, height=peak_height, distance=peak_distance)
                # Correct a little and sort
                if maxS[0] == 0:
                    maxS = maxS[1:]
                if maxS[-1] == image_win.size - 1:
                    maxS = maxS[:-1]
                if minS[0] == 0:
                    minS = minS[1:]
                if minS[-1] == image_win.size - 1:
                    minS = minS[:-1]
                if maxS[0] < minS[0]:
                    maxS = maxS[1:]
                indices = np.sort(np.concatenate((maxS, minS)))
                # plt.plot(indices, image_win[indices], 'bo')

                # Fit and extract param
                x = np.arange(indices.size)
                coef, cov_matrix = np.polyfit(x, indices, 1, cov=True)
                period[indV, ind] = coef[0] * 2
                period_error[indV, ind] = np.sqrt(cov_matrix[0, 0]) * 2
                if indV == 0:
                    intercept_0[ind] = coef[1]
                    intercept_0_error[ind] = cov_matrix[1, 1]
                else:
                    phase_now = 2 * np.pi * \
                        ((coef[1] - intercept_0[ind]) / period[0, ind])
                    phase[indV, ind] = phase_now
                    phase_error[indV, ind] = 2 * np.pi * np.sqrt(
                        cov_matrix[1, 1] *
                        (phase_now / coef[1])**2 + period_error[0, ind] *
                        (phase_now / coef[0])**2 + intercept_0_error[ind] *
                        (phase_now / intercept_0[ind])**2)
                # plt.figure(figsize=(8, 8))
                # plt.plot(x, indices, '+')
                # plt.plot(x, coef[0] * x + coef[1])
                difer = np.linalg.norm(coef[0] * x + coef[1] -
                                       indices) / x.size
                # print(period[ind], intercept_0[ind]/degrees, difer)

    # Unwrap
    if unwrap is not None:
        phase = Unwrap(
            phase, step=unwrap, axis=0, threshold=threshold, progresive=False)

    # Average to calculate phase
    if weighted and (Lw > 1):
        phase_av = np.average(phase, axis=1, weights=phase_error)
        period_mean = np.average(period, axis=1, weights=period_error)

        _, phase_aux = np.meshgrid(np.ones(Lw), phase_av)
        _, period_aux = np.meshgrid(np.ones(Lw), period_mean)
        phase_error = np.sqrt(
            np.average((phase - phase_aux)**2, weights=phase_error,
                       axis=1)) / np.sqrt(Lw)
        phase_error[0] = 0
        period_error = np.sqrt(
            np.average((period - period_aux)**2, weights=period_error,
                       axis=1)) / np.sqrt(Lw)
    else:
        phase_av = np.mean(phase, axis=1)
        period_mean = np.mean(period, axis=1)

        phase_error = np.std(phase, axis=1) / np.sqrt(Lw)
        period_error = np.std(period, axis=1) / np.sqrt(Lw)

    # Fit
    coef = np.polyfit(levels, phase_av, fit_order)
    coef, cov = curve_fit(fitting_0,levels,phase_av)
    phase_fit = fitting_0(levels,*coef)
    #phase_fit = np.zeros_like(phase_av)
    #for ind, val in enumerate(coef):
    #    phase_fit += levels**(coef.size - ind - 1) * val

    # Substract Jones contribution
    if jones_calibration is not None:
        # Load calibration
        data = np.load("Analysis_calibration.npz", allow_pickle=True)
        components = data["components_" + method_jones]
        # Create matrices
        Jslm = Components_To_Matrix(components)
        E0 = Jones_vector().general_azimuth_ellipticity(
            azimuth=azimuth_source,
            ellipticity=ellipticity_source)
        # Mirrors
        J_mirror = Jones_matrix().mirror()
        if mirrors[0]:
            Jslm = Jslm * J_mirror
        if mirrors[1]:
            Jslm = J_mirror * Jslm
        # 0 angle
        if substract_zero:
            cond = angles > 0
            angles[cond] = (angles[cond] - angles_0[cond]) % np.pi
        # Calculation
        Intensity, phase_jones = Calculate_Transmission(angles, Jslm, E0=E0)
        # Compensation
        phase_av = phase_av - phase_jones
        phase_av = phase_av - phase_av[0]

    # Draw if required
    if draw:
        plt.figure(figsize=(12, 9))
        plt.plot(levels, phase_av / degrees, 'b', linewidth=0.5)
        plt.plot(levels, phase_fit / degrees, 'r--', linewidth=2)
        """""
        if Lw > 1:
            if levels.size <= 10:
                plt.errorbar(
                    levels,
                    phase_av / degrees,
                    phase_error / degrees,
                    fmt='ob')
            else:
                plt.errorbar(
                    levels[::5],
                    phase_av[::5] / degrees,
                    phase_error[::5] / degrees,
                    fmt='ob')
        """""
        plt.xlabel('SLM gray level')
        plt.ylabel('Phase (deg)')
        plt.title('Phase shift ({} method)'.format(method))
        print('Mean period is {:.2f} +- {:.2f} pixels'.format(
            np.mean(period_mean), np.mean(period_error)))

    # Save phase file
    if filename:
        print("File {} saved in {}.".format(filename, os.getcwd()))
        np.savez(
            filename,
            phase_av=phase_av,
            phase_error=phase_error,
            period=period,
            phase_fit=phase_fit)

    # Return to original folder
    if path:
        os.chdir(old_path)

    # Print time
    print('Elapsed time is {:.1f} s'.format(time.time() - start_time))

    return phase_av, phase_error, phase_fit, period


def Extract_Global_Phase(phase,
                         angles,
                         calibration,
                         draw=False,
                         filename='Phase_calibration.npz'):
    """Function to extract the global phase from a phase measurement.

    Args:
        phase (np.ndarray): Measured phase.
        angles (tuple or list): List with the 4 angles used in the measurement.
        calibration (string): File with the calibration.
        draw (bool): If True, plots the measurement. Default: True.

    Returns:
        global_phase (np.ndarray): Extracted global phase.
    """
    # Load the calibrated data
    datos = np.load(calibration)
    comp = datos["components_hoyo_ext"]

    # Create polarization elements
    Jslm = Jones_matrix().from_components(comp)
    E0 = Jones_vector().general_azimuth_ellipticity(
        azimuth=azimuth_source, ellipticity=ellipticity_source)
    if angles[0] is None:
        P1 = Jones_matrix().vacuum()
    else:
        P1 = Jones_matrix().diattenuator_perfect(azimuth=angles[0] -
                                                 angles_0[0])
    if angles[1] is None:
        Q1 = Jones_matrix().vacuum()
    else:
        Q1 = Jones_matrix().quarter_waveplate(azimuth=angles[1] - angles_0[1])
    if angles[2] is None:
        Q2 = Jones_matrix().vacuum()
    else:
        Q2 = Jones_matrix().quarter_waveplate(azimuth=angles[2] - angles_0[2])
    if angles[3] is None:
        P2 = Jones_matrix().vacuum()
    else:
        P2 = Jones_matrix().quarter_waveplate(azimuth=angles[3] - angles_0[3])

    # Calculate the transmission
    E = P2 * Q2 * Jslm * Q1 * P1 * E0
    phase_th = E.parameters.global_phase()
    phase_global = phase - phase_th

    if draw:
        plt.figure(figsize=(10, 10))
        plt.plot(phase / degrees)
        plt.plot(phase_th / degrees)
        plt.plot(phase_global / degrees, linewidth=2)
        plt.xlabel('Level')
        plt.ylabel('Phase (deg)')
        plt.legend(('Measured', 'Jones matrix', 'Global'))

    if filename:
        np.savez(filename, phase_av=phase_global)

    return phase_global


###################################
# JONES MATRIX CALIBRATION
###################################


def Calculate_Angles(ind,
                     ort=0,
                     mirrors=def_mirrors,
                     check_repeat=True,
                     return_angles=False,
                     add_zero=True):
    """Function to calculate the required angles to perform the calibration of the Jones matrix of the SLM.

    Args:
        ind (int): Index of measurement.
        ort (ind): Index of complementary measurement (1) or signal measurement (0). Default: 0.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        check_repeat (bool): If True, checks if the angles have been already done and calculates the corresponding index. Default: True.
        return_angles (bool): If True, returns calculated angles instead of index, ind_rep and ort_rep. Default: False.
        add_zero (bool): If True, the zero angle (angle of rotation that produces azimuth=0) is added. Default: True.

    Return:
        index (int): Corresponding index in the list of all measurements for the repeated measurement.
        ind_rep (int): Index of the repeated measurement.
        ort_rep (int): Complementarity index of the repeated measurement.
        angles (np.ndarray): List of angles (a_P1, a_Q1, a_Q2, a_P2) in radians, not in degrees.
    """
    # PSG
    psg_az = PSG_az[ind]
    psg_el = PSG_el[ind]
   
    if mirrors[0]:
        psg_az = 180 * degrees - psg_az if psg_az != 0 else 0
        psg_el = -psg_el
    a_P1, a_Q1 = PSG_states_2_angles(azimuth=psg_az, ellipticity=psg_el)
    # a_P1 = int(a_P1 / degrees) + angles_0[0]
    # a_Q1 = int(a_Q1 / degrees) + angles_0[1]
    
    a_P1 = int(round((a_P1 + angles_0[0] * add_zero) / degrees))
    a_Q1 = int(round((a_Q1 + angles_0[1] * add_zero) / degrees))

    # PSA
    psa_az = PSA_az[ind]
    psa_el = PSA_el[ind]
    if mirrors[1]:
        psa_az = 180 * degrees - psa_az if psa_az != 0 else 0
        psa_el = -psa_el
    if ort > 0:
        psa_az = (90 * degrees + psa_az) % (180 * degrees)
        psa_el = -psa_el
    a_P2, a_Q2 = PSA_states_2_angles(azimuth=psa_az, ellipticity=psa_el)
    # a_P2 = int(a_P2 / degrees) + angles_0[3]
    # a_Q2 = int(a_Q2 / degrees) + angles_0[2]
    a_P2 = int(round((a_P2 + angles_0[3] * add_zero) / degrees))
    a_Q2 = int(round((a_Q2 + angles_0[2] * add_zero) / degrees))

    # Check that the measurement has not been already performed
    angles = [a_P1, a_Q1, a_Q2, a_P2]
    if check_repeat and (angles in list_angles):
        index = list_angles.index(angles)
        ind_rep = list_ind[index]
        ort_rep = list_ort[index]
        rep = ind_rep * 2 + ort_rep
        print(
            'This measurement has been already performed: indM = {}, ort = {}.'.
            format(ind_rep, ort_rep))
    else:
        list_angles.append(angles)
        list_ind.append(ind)
        list_ort.append(ort)
        index, ind_rep, ort_rep = (None, None, None)
        # Check angle change
        if len(list_angles) < 2:
            previous_angles = np.random.rand(4)
            # p_P1, p_Q1, p_Q2, p_P2 = np.random.rand(4)
        else:
            previous_angles = list_angles[-2]
            # ind_p = ind - 1 + ort
            # ort_p = 1 - ort
            # if mirrors[0]:
            #     p_P1 = angle_P1_mirror[ind_p] + angles_0[0]
            #     p_Q1 = angle_Q1_mirror[ind_p] + angles_0[1]
            # else:
            #     p_P1 = angle_P1[ind_p] + angles_0[0]
            #     p_Q1 = angle_Q1[ind_p] + angles_0[1]
            # if mirrors[1]:
            #     p_Q2 = angle_Q2_mirror[ind_p] + angles_0[2]
            #     p_P2 = (angle_P2_mirror[ind_p] +
            #             ort_p * 90) % 180 + angles_0[3]
            # else:
            #     p_Q2 = angle_Q2[ind_p] + angles_0[2]
            #     p_P2 = (angle_P2[ind_p] + ort_p * 90) % 180 + angles_0[3]

        s1 = ' (N)' * (a_P1 != previous_angles[0])
        s2 = ' (N)' * (a_Q1 != previous_angles[1])
        s3 = ' (N)' * (a_Q2 != previous_angles[2])
        s4 = ' (N)' * (a_P2 != previous_angles[3])
        #print('stop')
        if not return_angles:
            print(
                'Angles for ind = {}, ort = {}:\n  - P1 = {} deg{}.\n  - Q1 = {} deg{}.\n  - Q2 = {} deg{}.\n  - P2 = {} deg{}.'.
                format(ind, ort, a_P1, s1, a_Q1, s2, a_Q2, s3, a_P2, s4))

    # Return
    if return_angles:
        return np.array(angles) * degrees
    else:
        return index, ind_rep, ort_rep



def Calculate_Angles_Polarimeter(file_angles,
                     ind,
                     ort=0,
                     mirrors=def_mirrors,
                     check_repeat=True,
                     return_angles=False,
                     add_zero=True):
    """Function to calculate the required angles to perform the calibration of the Jones matrix of the SLM.

    Args:
        ind (int): Index of measurement.
        ort (ind): Index of complementary measurement (1) or signal measurement (0). Default: 0.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        check_repeat (bool): If True, checks if the angles have been already done and calculates the corresponding index. Default: True.
        return_angles (bool): If True, returns calculated angles instead of index, ind_rep and ort_rep. Default: False.
        add_zero (bool): If True, the zero angle (angle of rotation that produces azimuth=0) is added. Default: True.

    Return:
        index (int): Corresponding index in the list of all measurements for the repeated measurement.
        ind_rep (int): Index of the repeated measurement.
        ort_rep (int): Complementarity index of the repeated measurement.
        angles (np.ndarray): List of angles (a_P1, a_Q1, a_Q2, a_P2) in radians, not in degrees.
    """
    # PSG
    data = np.load(file_angles)

    psg_az = data['PSG_az'][ind]
    psg_el = data['PSG_el'][ind]
   
    if mirrors[0]:
        psg_az = 180 * degrees - psg_az if psg_az != 0 else 0
        psg_el = -psg_el
    a_P1, a_Q1 = PSG_states_2_angles(azimuth=psg_az, ellipticity=psg_el)
    # a_P1 = int(a_P1 / degrees) + angles_0[0]
    # a_Q1 = int(a_Q1 / degrees) + angles_0[1]
    
    a_P1 = int(round((a_P1 + angles_0[0] * add_zero) / degrees))
    a_Q1 = int(round((a_Q1 + angles_0[1] * add_zero) / degrees))

    # PSA
    psa_az = data['PSA_az'][ind]
    psa_el = data['PSA_el'][ind]
    if mirrors[1]:
        psa_az = 180 * degrees - psa_az if psa_az != 0 else 0
        psa_el = -psa_el
    if ort > 0:
        psa_az = (90 * degrees + psa_az) % (180 * degrees)
        psa_el = -psa_el
    a_P2, a_Q2 = PSA_states_2_angles(azimuth=psa_az, ellipticity=psa_el)
    # a_P2 = int(a_P2 / degrees) + angles_0[3]
    # a_Q2 = int(a_Q2 / degrees) + angles_0[2]
    a_P2 = int(round((a_P2 + angles_0[3] * add_zero) / degrees))
    a_Q2 = int(round((a_Q2 + angles_0[2] * add_zero) / degrees))

    # Check that the measurement has not been already performed
    angles = [a_P1, a_Q1, a_Q2, a_P2]
    if check_repeat and (angles in list_angles):
        index = list_angles.index(angles)
        ind_rep = list_ind[index]
        ort_rep = list_ort[index]
        rep = ind_rep * 2 + ort_rep
        print(
            'This measurement has been already performed: indM = {}, ort = {}.'.
            format(ind_rep, ort_rep))
    else:
        list_angles.append(angles)
        list_ind.append(ind)
        list_ort.append(ort)
        index, ind_rep, ort_rep = (None, None, None)
        # Check angle change
        if len(list_angles) < 2:
            previous_angles = np.random.rand(4)
            # p_P1, p_Q1, p_Q2, p_P2 = np.random.rand(4)
        else:
            previous_angles = list_angles[-2]
            # ind_p = ind - 1 + ort
            # ort_p = 1 - ort
            # if mirrors[0]:
            #     p_P1 = angle_P1_mirror[ind_p] + angles_0[0]
            #     p_Q1 = angle_Q1_mirror[ind_p] + angles_0[1]
            # else:
            #     p_P1 = angle_P1[ind_p] + angles_0[0]
            #     p_Q1 = angle_Q1[ind_p] + angles_0[1]
            # if mirrors[1]:
            #     p_Q2 = angle_Q2_mirror[ind_p] + angles_0[2]
            #     p_P2 = (angle_P2_mirror[ind_p] +
            #             ort_p * 90) % 180 + angles_0[3]
            # else:
            #     p_Q2 = angle_Q2[ind_p] + angles_0[2]
            #     p_P2 = (angle_P2[ind_p] + ort_p * 90) % 180 + angles_0[3]

        s1 = ' (N)' * (a_P1 != previous_angles[0])
        s2 = ' (N)' * (a_Q1 != previous_angles[1])
        s3 = ' (N)' * (a_Q2 != previous_angles[2])
        s4 = ' (N)' * (a_P2 != previous_angles[3])
        #print('stop')
        if not return_angles:
            print(
                'Angles for ind = {}, ort = {}:\n  - P1 = {} deg{}.\n  - Q1 = {} deg{}.\n  - Q2 = {} deg{}.\n  - P2 = {} deg{}.'.
                format(ind, ort, a_P1, s1, a_Q1, s2, a_Q2, s3, a_P2, s4))

    # Return
    if return_angles:
        return np.array(angles) * degrees
    else:
        return index, ind_rep, ort_rep

def Correct_Angles_By_Mirrors(angles, mirrors):
    """Function to correct the PSG and PSA elements angles if mirrors are present.

    Args:
        angles (np.ndarray): Initial angles, size 4
        mirrors (list): List of 2 elements containing if the PSG and PSA mirrors are present.

    Returns:
        np.ndarray: Result
    """
    # PSG
    if mirrors[0]:
        az, el = PSG_angles_2_states(angles[0],angles[1])
        #np.where(az!=0,180 * degrees - az,0)
        az = 180 * degrees - az if az != 0 else 0
        el = -el
        angles[0:2] = PSG_states_2_angles(az, el) 
        
    # PSA
    if mirrors[1]:
        az, el = PSA_angles_2_states(angles[2:])
        az = 180 * degrees - az if az != 0 else 0
        el = -el
        angles[-1], angles[-2] = PSA_states_2_angles(az, el)

    return angles


def Intensity_Measurement(slm,
                          daca,
                          levels=None,
                          Naverage=1,
                          wait_time=1e-2,
                          filename='Intensity.npz',
                          want_repeat=True,
                          draw=True):
    """Function to perform one of the measurements to calculate the Jones matrix of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        filename (str): Filename to save the result.
        want_repeat (bool): If True, you can repeat the measure that has just been carried out.
        draw (bool): If True, plots the measurement. Default: True.
    """
    # Prealocate
    gray_level = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)

    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)

    # Measurement
    power = np.zeros(levels.size)
    error = np.zeros(levels.size)
    for indV, value in enumerate(levels):
        # Create the mask
        gray_level.gray_scale(
            num_levels=1, levelMin=np.sqrt(value), levelMax=np.sqrt(value))
        slm.Send_Image(image=gray_level, kind='intensity', norm=1)
        if indV == 0:
            time.sleep(1)

        # Average intensity
        measures = np.zeros(Naverage)
        for indMM in range(Naverage):
            time.sleep(wait_time)
            measures[indMM] = daca.Get_Signal()

        # Calculate mean value and error
        power[indV] = np.mean(measures)
        #print(power)
        error[indV] = np.std(measures) / np.sqrt(Naverage)

    # Save file
    now = datetime.now()
    string_time = now.strftime("Date: %Y_%m_%d_%H_%M_%S.")
    np.savez(
        filename,
        power=power,
        error=error,
        Naverage=Naverage,
        wait_time=wait_time,
        levels=levels,
        time=string_time)

    # Draw
    if draw:
        plt.figure()
        plt.plot(levels * 255, power)
        plt.xlabel('Levels')
        plt.ylabel('Intensity (a.u.)')
        if Naverage > 1:
            plt.errorbar(
                levels[::5] * 255, power[::5], yerr=error[::5], fmt='b+')
        plt.show()

    if want_repeat:
        string = input(
            'Write REPEAT to repeat the measurement, otherwise to store it.')
        if string.lower() == 'repeat':
            Intensity_Measurement(
                slm=slm,
                daca=daca,
                levels=levels,
                Naverage=Naverage,
                wait_time=wait_time,
                filename=filename,
                want_repeat=want_repeat,
                draw=draw)

    # End of iteration
    print('\n')


def Jones_Calibration_Single_Measurement(slm,
                                         daca,
                                         ind,
                                         ort=None,
                                         filename=None,
                                         levels=None,
                                         Naverage=1,
                                         wait_time=1e-2,
                                         want_repeat=True,
                                         draw=True):
    """Function to perform one of the measurements to calculate the Jones matrix of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        ind (int): Index of measurement.
        ort (ind): Index of complementary measurement (1) or signal measurement (0). Default: 0.
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        want_repeat (bool): If True, you can repeat the measure that has just been carried out.
        draw (bool): If True, plots the measurement. Default: True.
    """
    #filename = "Calibracion_Ind_{}_Ort_{}.npz".format(ind, ort)
    filename = filename + "_{}.npz".format(ind)
    Intensity_Measurement(
        slm=slm,
        daca=daca,
        levels=levels,
        Naverage=Naverage,
        wait_time=wait_time,
        filename=filename,
        want_repeat=want_repeat,
        draw=draw)


def Jones_Calibration_Measurement(slm,
                                  daca,
                                  mirrors=def_mirrors,
                                  levels=None,
                                  Naverage=1,
                                  wait_time=1e-2,
                                  restart=True,
                                  want_repeat=True,
                                  draw=True):
    """Function to perform the measurements to calculate the Jones matrix of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        restart (bool): If True, restart the measurements from the begining. Default: True.
        want_repeat (bool): If True, you can repeat the measure that has just been carried out.
        draw (bool): If True, plots the measurement. Default: True.
        return_vars (bool): If True, intensity and error are returned. Default: False.
    """
    # Prealocate
    gray_level = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)

    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)

    # Restart
    if restart:
        Clear_Angles()

    # Loop
    for indM in range(PSG_az.size):
        for ort in range(2):
            # Calculate angles and wait for the user to put them
            clear_output(wait=False)
            index, ind_rep, ort_rep = Calculate_Angles(
                ind=indM, ort=ort, mirrors=mirrors)
            letras = input('Press Enter when PSG and PSA are ready, write SKIP to avoid measurement.')

            # Skip if required
            if letras.lower() in ("saltar", "skip"):
                print("Medida no realizada")

            elif ind_rep is not None:
                #if not letras.lower() in ('skip'):
                # Copy file
                    shutil.copyfile(
                        "Calibracion_Ind_{}_Ort_{}.npz".format(ind_rep, ort_rep),
                        "Calibracion_Ind_{}_Ort_{}.npz".format(indM, ort))


            # Measurement
            else:
                Jones_Calibration_Single_Measurement(
                    slm=slm,
                    daca=daca,
                    ind=indM,
                    ort=ort,
                    levels=levels,
                    Naverage=Naverage,
                    wait_time=wait_time,
                    want_repeat=want_repeat,
                    draw=draw)

    print('Fin!!!')



def Jones_Calibration_Motorized(slm,
                                  daca,
                                  motors,
                                  angles='perfect',
                                  Nmeasures=144,
                                  filename='Intensity',
                                  mirrors=def_mirrors,
                                  levels=None,
                                  Naverage=1,
                                  wait_time=1e-2,
                                  restart=True,
                                  want_repeat=False,
                                  make_orthogonal=True,
                                  draw=True):
    """Function to perform the measurements to calculate the Jones matrix of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        motors (Rotary motors): Object of rotary motors.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        restart (bool): If True, restart the measurements from the begining. Default: True.
        want_repeat (bool): If True, you can repeat the measure that has just been carried out.
        make_orthogonal (bool): If True, the orthogonal measurement is performed for normalization with retarder methods. Default: True.
        draw (bool): If True, plots the measurement. Default: True.
        return_vars (bool): If True, intensity and error are returned. Default: False.
    """
    #gray_level = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)
    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)

    # Restart
    if restart:
        Clear_Angles()

    list_angles = calculate_polarimetry_angles(Nmeasures, type=angles)

    for i in range(len(list_angles)):
            # Calculate angles and wait for the user to put them
            clear_output(wait=False)
            angles = (list_angles[i] + angles_0)/degrees
            print('Medida {} Ángulos: {}'.format(i,angles))
            motors.Move_Absolute(pos=[angles[0],angles[1],angles[2],angles[3]], units='deg', refered=True, move_time=0.1, verbose=False, waiting='busy')

            Jones_Calibration_Single_Measurement(
                slm=slm,
                daca=daca,
                ind=i,
                filename=filename,
                levels=levels,
                Naverage=Naverage,
                wait_time=wait_time,
                want_repeat=want_repeat,
                draw=draw)

    print('Fin!!!')
    return list_angles


def Jones_Calibration_Motorized_backup(slm,
                                  daca,
                                  motors,
                                  mirrors=def_mirrors,
                                  levels=None,
                                  Naverage=1,
                                  wait_time=1e-2,
                                  restart=True,
                                  want_repeat=False,
                                  make_orthogonal=True,
                                  draw=True):
    """Function to perform the measurements to calculate the Jones matrix of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        motors (Rotary motors): Object of rotary motors.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        restart (bool): If True, restart the measurements from the begining. Default: True.
        want_repeat (bool): If True, you can repeat the measure that has just been carried out.
        make_orthogonal (bool): If True, the orthogonal measurement is performed for normalization with retarder methods. Default: True.
        draw (bool): If True, plots the measurement. Default: True.
        return_vars (bool): If True, intensity and error are returned. Default: False.
    """
    #gray_level = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)
    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)

    # Restart
    if restart:
        Clear_Angles()

    # Measure the orthogonal
    Nort = 2 if make_orthogonal else 1

    # Loop
    list_angles = np.zeros((4,PSA_az.size))
    for indM in range(PSG_az.size):
        for ort in range(Nort):
            # Calculate angles and wait for the user to put them
            clear_output(wait=False)
            angles = Calculate_Angles(
                ind=indM, ort=ort, mirrors=mirrors,return_angles=True)
            list_angles[:,indM] = angles
            angles = angles/degrees
            print('Medida {}_{} Ángulos: {}'.format(indM,ort,angles))
            motors.Move_Absolute(pos=[angles[0],angles[1],angles[2],angles[3]], units='deg', refered=True, move_time=0.1, verbose=False, waiting='busy')

            Jones_Calibration_Single_Measurement(
                slm=slm,
                daca=daca,
                ind=indM,
                ort=ort,
                levels=levels,
                Naverage=Naverage,
                wait_time=wait_time,
                want_repeat=want_repeat,
                draw=draw)

    print('Fin!!!')
    return list_angles


def Jones_Calibration_Analysis(files=None,
                               path=None,
                               use_ort=True,
                               method='all',
                               norm=1,
                               mirrors=def_mirrors,
                               draw=False,
                               draw_errors=False,
                               analyze_matrices=False,
                               I=None,
                               DI=None,
                               filename='Analysis_calibration.npz',
                               correct_source=True):
    """Function that processes the data from the calibration to determine the Jones matrix of the SLM. Several methods are used.

    Args:
        files (iterable): List with file names where the data is stored. Default: None
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        use_ort (bool): If True, normalizes the intensity using the orthogonal measurements. Used only for pure retarders models (Moreno, Baiheng, Amaya).
        method (str): Method to calculate the element matrix. Accepted methods are 'moreno', 'baiheng', 'amaya', 'hoyo', 'hoyo ext', 'mueller', 'all' Default: 'all'.
        norm (float): Intensity normalization factor. Default: 1.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        draw (bool): If True, draws the calculated intensities and matrix elements. Default: False.
        draw_errors (bool): If True, draws the calculated errors. Default: False.
        print_completion (bool): If True, prints the completion of the program. Default: False.
        analyze_matrices (bool): If True, it decomposes the Jones matrices in polarizers and retarders and calculates their parameters. Default: False.
        I (list): If not None, takes this intensities instead of loading them from files. Default: None.
        DI (list): If not None, takes this intensity errors instead of loading them from files. Default: None.
        filename (str): Filename to save the data. Default: 'Analysis_calibration.npz'.
        correct_source (bool): If True the intensity measurements are corrected by ellipticity of the ilumination. Default: True

    Returns:
        phase (numpy.ndarray): Calculated phase shift. Averaged if several windows are set.
        phase_std (np.ndarray or None): If more than one window is used, the std is provided to have some idea of the error.
        period (float): Average period (if more than one window is used) for the reference image.
    """
    start_time = time.time()
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Light source
    if correct_source:
        E_source = Jones_vector().general_azimuth_ellipticity(
            azimuth=azimuth_source, ellipticity=ellipticity_source, intensity=2)
    else:
        E_source = Jones_vector().circular_light(intensity=2)


    # Loop through files
    I = []
    Idirect = []
    DI = []
    DIdirect = []
    load_levels = True
    Naverage = 0
    Imax = 0
    for ind, name in enumerate(files):
        # Load the measurement
        loaded = np.load(name)
        data = loaded['power']
        error = loaded['error']
        data[data<0] = 0.001
        if Naverage == 0:
            Naverage = loaded['Naverage']
        # Filter data
        data = Filter_Curves(data, method='widepeaks')
        # Correct source ellipticity
        if use_ort:
            indM = int(ind / 2)
            indO = int(indM * 2 != ind)
        else:
            indM = ind
            indO = 0
        psg_az = PSG_az[indM]
        psg_el = PSG_el[indM]
        if mirrors[0]:
            psg_az = 180 * degrees - psg_az if psg_az != 0 else 0
            psg_el = -psg_el
        a_P1, _ = PSG_states_2_angles(azimuth=psg_az, ellipticity=psg_el)
        Jp = Jones_matrix().diattenuator_perfect(azimuth=a_P1)
        E_in = Jp * E_source
        cte = E_in.parameters.intensity()
        data = data / cte
        error = error / cte

        # Check if it is the measurement or the complementary orthogonal to calculate total energy
        if indO == 0:
            I_aux = data
            DI_aux = error
            Idirect.append(data)
            DIdirect.append(error)
            if np.max(data) > Imax:
                Imax = np.max(data)
        else:
            I_aux = I_aux / (data + I_aux)
            DI_aux = np.sqrt(error**2 +
                             (DI_aux**2) * (2 * I_aux + data)) / (data + I_aux)
            I.append(I_aux)
            DI.append(DI_aux)


        # Load levels
        if load_levels:
            try:
                levels = data['levels']
            except:
                levels = np.arange(256)
            load_levels = False
    # Normalize direct intensities
    for ind in range(len(Idirect)):
        Idirect[ind] = Idirect[ind] / (Imax * norm)
        #print(Idirect)
        DIdirect[ind] = DIdirect[ind] / (Imax * norm)

    # Use direct intensities as renormalized ones if the orthogonal states were not measured
    if not use_ort:
        I = Idirect
        DI = DIdirect

    if draw:
        # Datos
        Iplot = np.transpose(np.array(I))
        DIplot = np.transpose(np.array(DI))
        Idirect_plot = np.transpose(np.array(Idirect))
        DIdirect_plot = np.transpose(np.array(DIdirect))
        Lscatter = levels[::10]
        # Plot direct intensities
        plt.figure(figsize=(20, 6))
        plt.subplot(1, 2, 1)
        plt.plot(levels, Idirect_plot)
        plt.xlabel('SLM levels')
        plt.ylabel('Intensity (a.u.)')
        plt.title('Direct intensities')
        plt.legend(range(len(Idirect)))
        if draw_errors:
            for ind, Ielem in enumerate(Idirect):
                color = COLORS[ind % len(COLORS)]
                linestyle = LINESTYLES[int(ind / len(COLORS))]
                Iscatter = Ielem[::10]
                DIscatter = DIdirect[ind][::10]
                plt.errorbar(
                    x=Lscatter,
                    y=Iscatter,
                    yerr=DIscatter,
                    color=color,
                    linestyle=linestyle)
        # Plot renormalized intensities
        if use_ort:
            plt.subplot(1, 2, 2)
            plt.plot(levels, Iplot)
            plt.xlabel('SLM levels')
            plt.ylabel('Intensity (a.u.)')
            plt.title('Renormalized intensities')
            plt.legend(range(len(I)))
            if draw_errors:
                for ind, Ielem in enumerate(I):
                    color = COLORS[ind % len(COLORS)]
                    linestyle = LINESTYLES[int(ind / len(COLORS))]
                    Iscatter = Ielem[::10]
                    DIscatter = DI[ind][::10]
                    plt.errorbar(
                        x=Lscatter,
                        y=Iscatter,
                        yerr=DIscatter,
                        color=color,
                        linestyle=linestyle)
        


    # Calculate matrix elements
    # if method != "all":
    empty_list = []
    for ind in range(8):
        empty_list.append(levels + np.nan)

    if method in ("all", "hoyo_ext"):
        comp_hoyo_ext, error_hoyo_ext = Metodo_Hoyo_Ext(Idirect, DIdirect)
    else:
        comp_hoyo_ext = empty_list
        error_hoyo_ext = empty_list

    if method in ("all", "hoyo"):
        comp_hoyo, error_hoyo = Metodo_Hoyo(Idirect, DIdirect)
    else:
        comp_hoyo = empty_list
        error_hoyo = empty_list

    if method in ("all", "moreno"):
        comp_moreno, error_moreno, param = Metodo_Moreno(I, DI, give_param=True)
        signs = [
            np.sign(param[0]),
            np.sign(param[1]),
            np.sign(param[2]),
            np.sign(param[3])
        ]
    else:
        comp_moreno = empty_list
        error_moreno = empty_list
        signs = [1, 1, 1, 1]

    #if method in ("all", "baiheng"):
    #    comp_baiheng, error_baiheng = Metodo_Baiheng(I, DI=DI, signs=signs)
    #else:
    #    comp_baiheng = empty_list
    #    error_baiheng = empty_list

    # if method in ("all", "amaya"):
    #     comp_amaya, error_amaya = Metodo_Amaya(I)
    # else:
    #     comp_amaya = empty_list
    #     comp_amaya = empty_list

    comp_amaya = empty_list
    comp_baiheng = empty_list

    if method in ("mueller"):
        comp_mueller,components_before, error_mueller = Metodo_Mueller(Idirect,DIdirect)
        #La matriz de Mueller real es el doble de la calculada
        #for ind in range(len(comp_mueller)):
        #    comp_mueller[ind] = 2*comp_mueller[ind]

    # Draw
    if draw:
        # Jones
        titles = ('|J0|', '|J1|', '|J2|', '|J3|', 'det', 'd1', 'd2', 'd3')
        leg = ('Hoyo ext.', 'Hoyo', 'Moreno'
        # 'Baiheng', 'Amaya'
        )
        ylabels = ['Error'] * 5 + ['Error (rad)'] * 3
        colors = ('b', 'g', 'k', 'r', 'c')
        dots = ('ob', 'og', 'ok', 'or', 'oc')
        plt.figure(figsize=(24, 12))
        for ind in range(8):
            plt.subplot(2, 4, ind + 1)
            plt.plot(levels, comp_hoyo_ext[ind], colors[0])
            plt.plot(levels, comp_hoyo[ind], colors[1])
            plt.plot(levels, comp_moreno[ind], colors[2])
            #plt.plot(levels, comp_baiheng[ind], colors[3])
            #plt.plot(levels, comp_amaya[ind], colors[4])
            plt.title(titles[ind])
            if ind > 4:
                plt.ylabel('Phase (rad)')
            else:
                plt.ylabel('Abs. value')
                plt.ylim(0,1)
            plt.legend(leg)
            if draw_errors:
                _, _, ymin, ymax = plt.axis()
                plt.errorbar(
                    x=levels[::10],
                    y=comp_hoyo_ext[ind][::10],
                    yerr=error_hoyo_ext[ind][::10],
                    fmt=dots[0])
                plt.errorbar(
                    x=levels[2::10],
                    y=comp_hoyo[ind][2::10],
                    yerr=error_hoyo[ind][2::10],
                    fmt=dots[1])
                plt.errorbar(
                    x=levels[2::10],
                    y=comp_moreno[ind][4::10],
                    yerr=error_moreno[ind][4::10],
                    fmt=dots[2])
                # plt.errorbar(
                #     x=levels[6::10],
                #     y=comp_baiheng[ind][6::10],
                #     yerr=error_baiheng[ind][6::10],
                #     fmt=dots[3])
                # plt.errorbar(
                #     x=levels[8::10],
                #     y=comp_amaya[ind][8::10],
                #     yerr=error_amaya[ind][8::10],
                #     fmt=dots[4])
                # plt.ylim([ymin, ymax])

        # Mueller
        if method in ("mueller"):
            titles = ("M00", "M01", "M02", "M03", "M10", "M11", "M12", "M13", "M20", "M21", "M22", "M23", "M30", "M31", "M32", "M33")
            plt.figure(figsize=(24, 24))
            for ind, elem in enumerate(comp_mueller):
                plt.subplot(4, 4, ind + 1)
                plt.plot(levels, elem, 'b',label='Corrected')
                plt.plot(levels,components_before[ind],'g',label='Direct')
                plt.title(titles[ind])
                plt.ylabel('Value')
                plt.ylim([-1.1, 1.1])
                plt.legend()
                if draw_errors:
                    plt.errorbar(
                        x=levels[::10],
                        y=elem[::10],
                        yerr=error_mueller[ind][::10],
                        fmt='ob')

    # Analize matrices
    if analyze_matrices:
        param_hoyo_ext = Analyze_Matrix(comp_hoyo_ext)
        param_hoyo = Analyze_Matrix(comp_hoyo)
        param_moreno = Analyze_Matrix(comp_moreno)
        #param_baiheng = Analyze_Matrix(comp_baiheng)
        #param_amaya = Analyze_Matrix(comp_amaya)
        #param_mueller = Analyze_Matrix(comp_mueller,type='mueller')
        R = [
            param_hoyo_ext["R"] / degrees,
            param_hoyo["R"] / degrees,
            #param_mueller["R"] / degrees,
            param_moreno["R"] / degrees
            #param_baiheng["R"] / degrees,
            #param_amaya["R"]
        ]
        az_R = [
            param_hoyo_ext["azimuth R"] / degrees,
            param_hoyo["azimuth R"] / degrees,
            #param_mueller["azimuth R"] / degrees,
            param_moreno["azimuth R"] / degrees
            #param_baiheng["azimuth R"] / degrees,
            # param_amaya["azimuth R"],

        ]
        el_R = [
            param_hoyo_ext["ellipticity R"] / degrees,
            param_hoyo["ellipticity R"] / degrees,
            #param_mueller["ellipticity R"] / degrees,
            param_moreno["ellipticity R"] / degrees
            #param_baiheng["ellipticity R"] / degrees,
            # param_amaya["ellipticity R"],

        ]
        p1 = [param_hoyo_ext["p1"], param_hoyo["p1"]]
        #param_mueller["p1"]]
        p2 = [param_hoyo_ext["p2"], param_hoyo["p2"]]
        #param_mueller["p2"]]
        az_D = [
            param_hoyo_ext["azimuth D"] / degrees,
            param_hoyo["azimuth D"] / degrees,
            #param_mueller["azimuth D"] / degrees
        ]
        el_D = [
            param_hoyo_ext["ellipticity D"] / degrees,
            param_hoyo["ellipticity D"] / degrees,
            #param_mueller["ellipticity D"] / degrees
        ]
        #depol = [param_mueller['depol']]
        #elements = [depol, R, az_R, el_R, p1, p2, az_D, el_D]
        elements = [R, az_R, el_R, p1, p2, az_D, el_D]
        print('Maximum field transmission is : ',
              "- Hoyo_ext:   {}".format(param_hoyo_ext["p1"].max()),
              "- Hoyo:       {}".format(param_hoyo["p1"].max()))
              #"- Mueller:     {}".format(param_mueller["p1"].max()), sep="\n")

        # Print the info
        if draw:
            titles = ('Retardance', 'Azimuth R', 'Ellipticity R',
                      'Max. field trans.', 'Min. field trans.', 'Azimuth D',
                      'Ellipticity D')
            ylabels = ('Depol. index', 'Retardance (deg)', 'Azimuth  (deg)',
                       'Ellipticity (deg)', 'Field trans.', 'Field trans.',
                       'Azimuth (deg)', 'Ellipticity (deg)')
            plt.figure(figsize=(20, 10))
            for ind, elem in enumerate(elements):
                if elem is not None:
                    elem = np.transpose(np.array(elem))
                    plt.subplot(2, 4, ind + 1)
                    plt.plot(levels, elem)
                    plt.title(titles[ind])
                    plt.xlabel('SLM level')
                    plt.ylabel(ylabels[ind])
                    plt.legend((
                        'Hoyo ext.',
                        'Hoyo',
                        'Moreno',
                        'Mueller',
                        #'Baiheng',
                        # 'Amaya',
                    ))
                if ind == 0:
                    plt.legend(frameon=False)
    else:
        param_hoyo_ext = None
        param_hoyo = None
        param_moreno = None
        # param_baiheng = None
        # param_amaya = None
        #param_mueller = None

    # Save calibration file
    np.savez(
        filename,
        components_hoyo_ext=comp_hoyo_ext,
        components_hoyo=comp_hoyo,
        components_moreno=comp_moreno,
        # components_amaya=comp_amaya,
        # components_baiheng=comp_baiheng,
        #components_mueller=comp_mueller,
        error_hoyo_ext=error_hoyo_ext,
        error_hoyo=error_hoyo,
        error_moreno=error_moreno,
        #error_baiheng=error_baiheng,
        #error_amaya=error_amaya,
        #error_mueller=error_mueller,
        param_hoyo_ext=param_hoyo_ext,
        param_hoyo=param_hoyo,
        param_moreno=param_moreno,
        # param_baiheng=param_baiheng,
        # param_amaya=param_amaya,
        #param_mueller=param_mueller,
        normalization_factor=norm * Imax)

    # Go to older path
    if path:
        os.chdir(old_path)

    # Return
    components = [
        comp_hoyo_ext, comp_hoyo, comp_moreno, #comp_baiheng, comp_amaya, 
        #comp_mueller
    ]
    errors = [
        error_hoyo_ext, error_hoyo, error_moreno, #error_baiheng,
        #error_amaya,
        #error_mueller
    ]
    ret = [components, errors]
    if analyze_matrices:
        parameters = [
            param_hoyo_ext, param_hoyo, param_moreno, #param_baiheng, param_amaya, 
            #param_mueller
        ]
        ret.append(parameters)
    print('Ellapsed time is {:.1f} s'.format(time.time() - start_time))
    return ret


def Mueller_Calibration_Motorized(slm,
                                  daca,
                                  motors,
                                  angles="Mueller_angles.npz",
                                  mirrors=def_mirrors,
                                  levels=None,
                                  Naverage=1,
                                  wait_time=1e-2,
                                  draw=True):
    """Function to perform the measurements to calculate the Mueller matrix of the SLM using the optimal method.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        motors (Rotary motors): Object of rotary motors.
        angles (np.array or str): Array with the angles of the PSG and PSA elements. If angles is str, it is the filename with the values. Default: 'Mueller_angles.npz'.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        restart (bool): If True, restart the measurements from the begining. Default: True.
        want_repeat (bool): If True, you can repeat the measure that has just been carried out.
        make_orthogonal (bool): If True, the orthogonal measurement is performed for normalization with retarder methods. Default: True.
        draw (bool): If True, plots the measurement. Default: True.
        return_vars (bool): If True, intensity and error are returned. Default: False.
    """
    # Load angles
    if isinstance(angles, str):
        data = np.load(angles)
        angles = data["angles"]

    # Loop
    for indM in range(angles.size / 4):
        # Calculate angles when mirrors are present
        clear_output(wait=False)
        anglesM = Correct_Angles_By_Mirrors(angles=angles[4*indM:4*indM+3], mirrors=mirrors) / degrees
        print('Ángulos: - P1 = {:.2f} º\n - Q1 = {:.2f} º\n - Q2 = {:.2f} º\n - P2 = {:.2f} º\n'.format(*anglesM))

        motors.Move_Absolute(pos=anglesM, units='deg', refered=True, move_time=0.1, verbose=False, waiting='busy')

        Intensity_Measurement(slm,
                          daca,
                          levels=levels,
                          Naverage=Naverage,
                          wait_time=wait_time,
                          filename='Mueller_Optimal_{}.npz'.format(indM),
                          want_repeat=False,
                          draw=draw)

    print('Fin!!!')


def Mueller_Calibration_Analysis(files=None,
                                angles="Mueller_angles.npz",
                               path=None,
                               norm=1,
                               mirrors=def_mirrors,
                               draw=False,
                               draw_errors=False,
                               analyze_matrices=False,
                               I=None,
                               DI=None,
                               filename='Analysis_calibration.npz',
                               correct_source=True):
    """Function that processes the data from the calibration to determine the Jones matrix of the SLM. Several methods are used.

    Args:
        files (iterable): List with file names where the data is stored. Default: None.
        angles (np.array or str): Array with the angles of the PSG and PSA elements. If angles is str, it is the filename with the values. Default: 'Mueller_angles.npz'.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        norm (float): Intensity normalization factor. Default: 1.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        draw (bool): If True, draws the calculated intensities and matrix elements. Default: False.
        draw_errors (bool): If True, draws the calculated errors. Default: False.
        print_completion (bool): If True, prints the completion of the program. Default: False.
        analyze_matrices (bool): If True, it decomposes the Jones matrices in polarizers and retarders and calculates their parameters. Default: False.
        I (list): If not None, takes this intensities instead of loading them from files. Default: None.
        DI (list): If not None, takes this intensity errors instead of loading them from files. Default: None.
        filename (str): Filename to save the data. Default: 'Analysis_calibration.npz'.
        correct_source (bool): If True the intensity measurements are corrected by ellipticity of the ilumination. Default: True

    Returns:
        phase (numpy.ndarray): Calculated phase shift. Averaged if several windows are set.
        phase_std (np.ndarray or None): If more than one window is used, the std is provided to have some idea of the error.
        period (float): Average period (if more than one window is used) for the reference image.
    """
    start_time = time.time()
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Light source
    if correct_source:
        E_source = Jones_vector().general_azimuth_ellipticity(
            azimuth=azimuth_source, ellipticity=ellipticity_source, intensity=2)
    else:
        E_source = Jones_vector().circular_light(intensity=2)

    # Load angles
    if isinstance(angles, str):
        data = np.load(angles)
        angles = data["angles"]

    # Loop through files
    I = []
    DI = []
    load_levels = True
    Naverage = 0
    Imax = 0
    for ind, name in enumerate(files):
        # Load the measurement
        loaded = np.load(name)
        data = loaded['power']
        error = loaded['error']
        #data[data<0] = 0.001
        if Naverage == 0:
            Naverage = loaded['Naverage']
        # Filter data
        data = Filter_Curves(data, method='widepeaks')
        # Correct source ellipticity
        #anglesM = Correct_Angles_By_Mirrors(angles=angles[4*ind : 4*ind+3], mirrors=mirrors)
        anglesM = Correct_Angles_By_Mirrors(angles=angles[:,ind], mirrors=mirrors)
        Jp = Jones_matrix().diattenuator_perfect(azimuth=anglesM[0])
        E_in = Jp * E_source
        cte = E_in.parameters.intensity()
        data = data / cte
        I.append(data)
        DI.append(error / cte)
        Imax = max(Imax, data.max())

        # Load levels
        if load_levels:
            try:
                levels = data['levels']
            except:
                levels = np.arange(256)
            load_levels = False

    # Normalize direct intensities
    for ind in range(len(I)):
        I[ind] = I[ind] / (Imax * norm)
        DI[ind] = DI[ind] / (Imax * norm)

    if draw:
        # Datos
        Iplot = np.transpose(np.array(I))
        DIplot = np.transpose(np.array(DI))
        Lscatter = levels[::10]
        # Plot direct intensities
        plt.figure(figsize=(10, 6))
        plt.plot(levels, Iplot)
        plt.xlabel('SLM levels')
        plt.ylabel('Intensity (a.u.)')
        plt.title('Direct intensities')
        plt.legend(range(len(I)))
        if draw_errors:
            for ind, Ielem in enumerate(Iplot):
                color = COLORS[ind % len(COLORS)]
                linestyle = LINESTYLES[int(ind / len(COLORS))]
                Iscatter = Ielem[::10]
                DIscatter = DIplot[ind][::10]
                plt.errorbar(
                    x=Lscatter,
                    y=Iscatter,
                    yerr=DIscatter,
                    color=color,
                    linestyle=linestyle)
    
    # Calculate matrix elements
    comp_mueller, error_mueller = Metodo_Mueller_Optimal(I, angles, DI) #Con cálculo intrínseco de la matriz W
    #comp_mueller,error_mueller = Metodo_Mueller(I, DI) #Importando la matriz W del config

    # Draw
    if draw:
        titles = ("M00", "M01", "M02", "M03", "M10", "M11", "M12", "M13", "M20", "M21", "M22", "M23", "M30", "M31", "M32", "M33")
        plt.figure(figsize=(24, 24))
        for ind, elem in enumerate(comp_mueller):
            plt.subplot(4, 4, ind + 1)
            plt.plot(levels, elem,label='Hoyo')
            plt.title(titles[ind])
            plt.ylabel('Value')
            plt.ylim([-1.1, 1.1])
            plt.legend()
            if draw_errors:
                plt.errorbar(
                    x=levels[::10],
                    y=elem[::10],
                    yerr=error_mueller[ind][::10],
                    fmt='ob')

    # Analize matrices
    if analyze_matrices:
        param_mueller = Analyze_Matrix(comp_mueller,type='mueller')
        R = param_mueller["R"] / degrees
        az_R = param_mueller["azimuth R"]
        el_R = param_mueller["ellipticity R"] / degrees
        p1 = param_mueller["p1"]
        p2 = param_mueller["p2"]
        az_D = param_mueller["azimuth D"] / degrees
        el_D = param_mueller["ellipticity D"] / degrees
        depol = param_mueller['depol']
        elements = [depol, R, az_R, el_R, p1, p2, az_D, el_D]
        print("Maximum field transmission is : {}".format(p1.max()))

        # Print the info
        if draw:
            titles = ('Depol. index', 'Retardance', 'Azimuth R', 'Ellipticity R',
                      'Max. field trans.', 'Min. field trans.', 'Azimuth D',
                      'Ellipticity D')
            ylabels = ('Depol. index', 'Retardance (deg)', 'Azimuth  (deg)',
                       'Ellipticity (deg)', 'Field trans.', 'Field trans.',
                       'Azimuth (deg)', 'Ellipticity (deg)')
            plt.figure(figsize=(20, 10))
            for ind, elem in enumerate(elements):
                elem = np.transpose(np.array(elem))
                plt.subplot(2, 4, ind + 1)
                plt.plot(levels, elem)
                plt.title(titles[ind])
                plt.xlabel('SLM level')
                plt.ylabel(ylabels[ind])
                if ind == 0:
                    plt.legend(frameon=False)
    else:
        param_mueller = None

    # Save calibration file
    np.savez(
        filename,
        components_mueller_optimal=comp_mueller,
        error_mueller_optimal=error_mueller,
        param_mueller_optimal=param_mueller,
        normalization_factor=norm * Imax)

    # Go to older path
    if path:
        os.chdir(old_path)

    # Return
    ret = [comp_mueller, error_mueller]
    if analyze_matrices:
        ret.append(param_mueller)
    print('Ellapsed time is {:.1f} s'.format(time.time() - start_time))
    return ret



def Mueller_Calibration_Analysis_Pluto(files=None,
                               angles="Mueller_angles.npz",
                               path=None,
                               norm=1,
                               mirrors=def_mirrors,
                               draw=False,
                               draw_errors=False,
                               analyze_matrices=False,
                               I=None,
                               DI=None,
                               filename='Analysis_calibration.npz',
                               method="all"):
    """Function that processes the data from the calibration to determine the Jones matrix of the SLM. Several methods are used.

    Args:
        files (iterable): List with file names where the data is stored. Default: None.
        angles (np.array or str): Array with the angles of the PSG and PSA elements. If angles is str, it is the filename with the values. Default: 'Mueller_angles.npz'.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        norm (float): Intensity normalization factor. Default: 1.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        draw (bool): If True, draws the calculated intensities and matrix elements. Default: False.
        draw_errors (bool): If True, draws the calculated errors. Default: False.
        print_completion (bool): If True, prints the completion of the program. Default: False.
        analyze_matrices (bool): If True, it decomposes the Jones matrices in polarizers and retarders and calculates their parameters. Default: False.
        I (list): If not None, takes this intensities instead of loading them from files. Default: None.
        DI (list): If not None, takes this intensity errors instead of loading them from files. Default: None.
        filename (str): Filename to save the data. Default: 'Analysis_calibration.npz'.
        correct_source (bool): If True the intensity measurements are corrected by ellipticity of the ilumination. Default: True

    Returns:
        phase (numpy.ndarray): Calculated phase shift. Averaged if several windows are set.
        phase_std (np.ndarray or None): If more than one window is used, the std is provided to have some idea of the error.
        period (float): Average period (if more than one window is used) for the reference image.
    """
    start_time = time.time()
    # Go to path
    #if path:
    #   old_path = os.getcwd()
    #   os.chdir(path)

    # Load angles
    if isinstance(angles, str):
        data = np.load(angles)
        angles = data["angles"]

    # Loop through files
    I = np.zeros((len(files),256))
    DI = np.zeros((len(files),256))
    load_levels = True
    Naverage = 0
    Imax = 0
    for ind, name in enumerate(files):
        # Load the measurement
        loaded = np.load(name)
        data = loaded['power']
        error = loaded['error']
       
        if Naverage == 0:
            Naverage = loaded['Naverage']
        # Filter data
        data = Filter_Curves(data, method='widepeaks')
        I[ind] = data
        DI[ind] = error
        Imax = max(Imax, data.max())


        # Load levels
        if load_levels:
            try:
                levels = data['levels']
            except:
                levels = np.arange(256)
            load_levels = False

    # Normalize direct intensities
    for ind in range(len(I)):
        I[ind] = I[ind] / (Imax)# * norm)
        DI[ind] = DI[ind] / (Imax)# * norm)
        #print('I',ind,np.mean(DI[ind]))
    if draw:
        # Datos
        Iplot = np.transpose(np.array(I))
        #DIplot = np.transpose(np.array(DI))
        Lscatter = levels[::5]
        # Plot direct intensities
        plt.figure(figsize=(10, 6))
        plt.plot(levels, Iplot)
        plt.xlabel('SLM levels')
        plt.ylabel('Intensity (a.u.)')
        plt.title('Direct intensities')
        #plt.legend(range(len(I)))
        if draw_errors:
            for ind, Ielem in enumerate(I):
                #color = COLORS[ind % len(COLORS)]
                #linestyle = LINESTYLES[int(ind / len(COLORS))]
                Iscatter = I[ind][::5]
                DIscatter = DI[ind][::5]
                plt.errorbar(
                    x=Lscatter,
                    y=Iscatter,
                    yerr=DIscatter
                    )
    
    if "Tetraedro" in method:
        comp, error = Metodo_Mueller_Optimal(I, angles, DI) #Cálculo de la matriz W.
        
    if "Icosaedro" in method:
        comp, error = Metodo_Mueller_Optimal(I, angles, DI) #Cálculo de la matriz W.
      
    # Draw
    if draw:
        titles = ("M00", "M01", "M02", "M03", "M10", "M11", "M12", "M13", "M20", "M21", "M22", "M23", "M30", "M31", "M32", "M33")
        plt.figure(figsize=(24, 24))
        x = np.arange(16)
        com_saved = np.zeros((16,256))
        for ind, elem in enumerate(comp):
            plt.subplot(4, 4, ind + 1)
            if "Tetraedro" in method:
                com_saved[ind] = comp[ind]/norm
                plt.plot(levels,comp[ind]/norm,label='Tetraedro')
                if draw_errors:
                    plt.errorbar(x=levels[::10],y=comp[ind][::10],yerr=error[ind][::10],markersize=3)

            if "Icosaedro" in method:
                com_saved[ind] = comp[ind]/norm
                plt.plot(levels,comp[ind]/norm,label='Icosaedro')
                if draw_errors:
                    plt.errorbar(x=levels[::10],y=comp[ind][::10],yerr=error[ind][::10],markersize=3)

           
            plt.title(titles[ind])
            plt.ylabel('Value')
            plt.ylim([-1.1, 1.1])
            plt.legend()

            if draw_errors:
                plt.figure()
                plt.plot(x,error.mean(axis=1)*100,'o')
                plt.legend(loc='upper left')
                plt.xlabel('M_component')
                plt.ylabel('Error (%)')

    # Analize matrices
    if analyze_matrices:
        param_mueller = Analyze_Matrix(comp,type='mueller')
        R = param_mueller["R"] / degrees
        az_R = param_mueller["azimuth R"]
        el_R = param_mueller["ellipticity R"] / degrees
        p1 = param_mueller["p1"]
        p2 = param_mueller["p2"]
        az_D = param_mueller["azimuth D"] / degrees
        el_D = param_mueller["ellipticity D"] / degrees
        depol = param_mueller['depol']
        elements = [depol, R, az_R, el_R, p1, p2, az_D, el_D]
        print("Maximum field transmission is : {}".format(p1.max()))

        # Print the info
        if draw:
            titles = ('Depol. index', 'Retardance', 'Azimuth R', 'Ellipticity R',
                      'Max. field trans.', 'Min. field trans.', 'Azimuth D',
                      'Ellipticity D')
            ylabels = ('Depol. index', 'Retardance (deg)', 'Azimuth  (deg)',
                       'Ellipticity (deg)', 'Field trans.', 'Field trans.',
                       'Azimuth (deg)', 'Ellipticity (deg)')
            plt.figure(figsize=(20, 10))
            for ind, elem in enumerate(elements):
                elem = np.transpose(np.array(elem))
                plt.subplot(2, 4, ind + 1)
                plt.plot(levels, elem)
                plt.title(titles[ind])
                plt.xlabel('SLM level')
                plt.ylabel(ylabels[ind])
                if ind == 0:
                    plt.legend(frameon=False)
    else:
        param_mueller = None

    # Save calibration file
    np.savez(
        filename,
        components_mueller_optimal=com_saved,
        error_mueller_optimal=error,
        param_mueller_optimal=param_mueller,
        normalization_factor=norm * Imax)

    # Go to older path
    #if path:
    #    os.chdir(old_path)

    # Return
    ret = [com_saved]#, error_mueller]
    if analyze_matrices:
        ret.append(param_mueller)
    print('Ellapsed time is {:.1f} s'.format(time.time() - start_time))
    return ret


def Metodo_Mueller_Optimal_backup(I,angles, DI=None):
    """Function to extract the Mueller matrix parameters of the Mueller optimal method.

    Args:
        I (tuple or list): List of measured intensities (at least 16 elements).
        angles (np.ndarray): Angles (radians) of the PSG and PSA.
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.

    Returns:
        components (list): List of M00, M01, M02, M03, M10, M11, M12, M13, M20, M21, M22, M23, M30, M31, M32, M33.
        errors (list or None): If DI is not None, component error.
    """
    # Calculate components
    system = [
    Stokes().circular_light(),
    Mueller().diattenuator_perfect(),
    Mueller().quarter_waveplate(),
    Mueller().quarter_waveplate(),
    Mueller().diattenuator_perfect()]

    angles_matrix = np.zeros((4,16)) 
    for ind,elem in enumerate(Mueller_indices):
        angles_matrix[:,ind] = angles[:,elem]

    W = Calculate_W(angles_matrix,system=system) #angulos en radianes siempre
    System_Inv = np.linalg.inv(W)
    components = []
    for row in System_Inv:
        comp = np.zeros_like(I[0])
        for ind, elem in enumerate(row):
            comp += elem * I[Mueller_indices[ind]]
        components.append(comp)
    # Calculate errors
    if DI is not None:
        errors = []
        for row in System_Inv:
            comp = np.zeros_like(I[1])
            for ind, elem in enumerate(row):
                comp += (elem * DI[Mueller_indices[ind]])**2
            errors.append(np.sqrt(comp))


    M = Mueller().from_components(components)

    D = np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2)/M.M[0,0]
    P = np.sqrt(M.M[1,0]**2 + M.M[2,0]**2 + M.M[3,0]**2)/M.M[0,0]
    P1 = np.sqrt(M.M[0,0]*(1+P))
    P1_max = P1.max()
    print('D = {:.4f}, P = {:.4f}, P1_max = {:.4f}'.format(D.mean(),P.mean(),P1_max))

    k = 1/P1_max**2 # Para forzar a que la transmisión máxima sea 1.
    M = M*k

    #Comprobación
    D = np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2)/M.M[0,0]
    P = np.sqrt(M.M[1,0]**2 + M.M[2,0]**2 + M.M[3,0]**2)/M.M[0,0]
    P1 = np.sqrt(M.M[0,0]*(1+P))
    P1_max = P1.max()
    print('P1_max =',P1_max.max())

    #M.analysis.filter_physical_conditions(tol=0,ignore_cond=[6,7]) #ignore_cond va con índices (número anterior a la condición).
    components = M.parameters.components()

    # Return
    ret = [components]
    if DI is not None:
        ret.append(errors)
    if len(ret) == 1:
        ret = ret[0]

    return ret


def Metodo_Mueller_Optimal(I,angles, DI=None,system=None):
    """Function to extract the Mueller matrix parameters of the Mueller optimal method.

    Args:
        I (tuple or list): List of measured intensities (at least 16 elements).
        angles (np.ndarray): Angles (radians) of the PSG and PSA.
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.

    Returns:
        components (list): List of M00, M01, M02, M03, M10, M11, M12, M13, M20, M21, M22, M23, M30, M31, M32, M33.
        errors (list or None): If DI is not None, component error.
    """
    # Calculate components
    if system is None:
        print('in')
        cal_dict = np.load('Polarimeter.npz')
        S, P1, R1, R2, P2 = Msystem_From_Dict(cal_dict,initial_angles=False)
        system = [S, P1, R1, R2, P2]
    

    #system = [Stokes().circular_light(),Mueller().diattenuator_perfect(),Mueller().quarter_waveplate(),Mueller().quarter_waveplate(),Mueller().diattenuator_perfect()]

    #angles_matrix = np.zeros((4,16)) 
    #for ind,elem in enumerate(Mueller_indices):
    #    angles_matrix[:,ind] = angles[:,elem]

    M,_ = Calculate_Mueller(I,angles,system=system,filter=False) #angulos en radianes siempre
    
    if DI is not None:
        _,wi = Calculate_Mueller(I,angles,system=system,filter=True) #angulos en radianes siempre
        """"
        wi2 = np.zeros((len(wi[0,:,0]),len(wi[0,0,:])))
        for i in range(len(wi[0,:,0])):
            for j in range(len(wi[0,0,:])):
                wi2[i,j] = wi[0,i,j]**2

        DI2 = np.zeros((len(DI[:,0]),len(DI[0,:])))
        for k in range(len(DI[:,0])):
            for l in range(len(DI[0,:])):
                DI2[k,l] = DI[k,l]**2
        #M_error = np.sqrt(wi2[:,:] @ DI2) #Error cuadrático
        """""
        M_error = np.sqrt(wi[0,:,:]**2 @ DI **2) #Error cuadrático
        #M_error = np.abs(wi[0,:,:]) @ DI #Error absoluto

    
    D = np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2)/M.M[0,0]
    P = np.sqrt(M.M[1,0]**2 + M.M[2,0]**2 + M.M[3,0]**2)/M.M[0,0]
    P1 = np.sqrt(M.M[0,0]*(1+D))
    P1_max = P1.max()
    #print('D = {:.4f}, P = {:.4f}, P1_max = {:.4f}'.format(D.mean(),P.mean(),P1_max))

    #k = 1/P1_max**2 # Para forzar a que la transmisión máxima sea 1.
    #M = M*k
    #print(k)

    #Comprobación
    D = np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2)/M.M[0,0]
    P = np.sqrt(M.M[1,0]**2 + M.M[2,0]**2 + M.M[3,0]**2)/M.M[0,0]
    P1 = np.sqrt(M.M[0,0]*(1+D))
    P1_max = P1.max()
    #print('P1_max =',P1_max.max())

    M.M = M.M/(P1_max**2)

    #M.analysis.filter_physical_conditions(tol=1e-20) #ignore_cond va con índices (número anterior a la condición).
    #print(M.checks.is_physical(draw=True,give_all=True))
    #M.analysis.filter_physical_conditions(tol=0,ignore_cond=[5,6]) #ignore_cond va con índices (número anterior a la condición).
    components = M.parameters.components()
    
    # Return
    ret = [components]
    if DI is not None:
        #error_components = M_error.parameters.components()
        ret.append(M_error)
    if len(ret) == 1:
        ret = ret[0]

    return ret


def Metodo_Mueller(I, DI=None):
    """Function to extract the Mueller matrix parameters of the Mueller Hoyo method.

    Args:
        I (tuple or list): List of measured intensities (at least 16 elements).
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.

    Returns:
        components (list): List of M00, M01, M02, M03, M10, M11, M12, M13, M20, M21, M22, M23, M30, M31, M32, M33.
        errors (list or None): If DI is not None, component error.
    """
    # Calculate components
    System_Inv = np.linalg.inv(System_Mueller)
    components = []
    for row in System_Inv:
        comp = np.zeros_like(I[0])
        for ind, elem in enumerate(row):
            comp += elem * I[Mueller_indices[ind]]
        components.append(comp)

    # Calculate errors
    if DI is not None:
        errors = []
        for row in System_Inv:
            comp = np.zeros_like(I[1])
            for ind, elem in enumerate(row):
                comp += (elem * DI[Mueller_indices[ind]])**2
            errors.append(np.sqrt(comp))

    # Filtering
    M = Mueller().from_components(components)
   
    #Normalization
    D = np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2)/M.M[0,0]
    print('D = ',D.mean())
    P = np.sqrt(M.M[1,0]**2 + M.M[2,0]**2 + M.M[3,0]**2)/M.M[0,0]
    print('P = ',P.mean())
    P1 = np.sqrt(M.M[0,0]*(1+P))
    P1_max = P1.max()
    k = 1/P1_max**2 # Para forzar a que la transmisión máxima sea 1.
    M = M*k

    #print('Maximo:',P1_max)
    #V = np.where(P1 == P1.max())
    #k = (1-np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2))/M.M[0,0]
    #k= M.M.max()
    #print(k.max())
    #print(M.M[0,0].max())
    #k1 = M.M[2,2].max()/M.M[0,0].max()
    #M.M[2,2] = M.M[2,2]/k1
    
    D = np.sqrt(M.M[0,1]**2 + M.M[0,2]**2 + M.M[0,3]**2)/M.M[0,0]
    P1 = np.sqrt(M.M[0,0]*(1+P))
    P1_max = P1.max()
    print(P1_max.max())

    #components_before = M.parameters.components()
    #components_before = deepcopy(components_before)
    conditions = M.checks.is_physical(give_all=True)
    #M.analysis.filter_physical_conditions(tol=0,ignore_cond=[None])
    components = M.parameters.components()

    # Return
    #ret = [components,components_before]
    ret = [components]
    if DI is not None:
        ret.append(errors)
    if len(ret) == 1:
        ret = ret[0]

    return ret


def Metodo_Hoyo_Ext(I, DI=None):
    """Function to extract the Jones parameters of the Extended Hoyo method.

    Args:
        I (tuple or list): List of measured intensities (at least 16 elements).
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.

    Returns:
        components (list): List of J0, J1, J2, J3, det, d1, d2, d3.
        errors (list or None): If DI is not None, component error.
    """
    # Calculate components
    J0 = np.sqrt(I[0])
    J1 = np.sqrt(I[3])
    J2 = np.sqrt(I[1])
    J3 = np.sqrt(I[2])
    d1 = np.arctan2(I[4] - I[10], I[5] - I[11]) % (2 * np.pi)
    d2 = np.arctan2(I[6] - I[12], I[7] - I[13]) % (2 * np.pi)
    d3 = (np.arctan2(I[8] - I[14], I[9] - I[15]) + d2) % (2 * np.pi)
    determinant = np.abs(J0 * J3 * np.exp(1j * d3) -
                         J1 * J2 * np.exp(1j * (d1 + d2)))
    components = [J0, J1, J2, J3, determinant, d1, d2, d3]

    # Calculate errors
    if DI is not None:
        DJ0 = np.sqrt(DI[0]) / np.sqrt(I[0])
        DJ1 = np.sqrt(DI[3]) / np.sqrt(I[3])
        DJ2 = np.sqrt(DI[1]) / np.sqrt(I[1])
        DJ3 = np.sqrt(DI[2]) / np.sqrt(I[2])

        u = (I[4] - I[10]) / (I[5] - I[11])
        f1 = DI[4] / (I[5] - I[11])
        f2 = DI[10] / (I[5] - I[11])
        f3 = u * DI[5] / (I[5] - I[11])
        f4 = u * DI[11] / (I[5] - I[11])
        Dd1 = 1 / (1 + u**2) * np.sqrt(f1**2 + f2**2 + f3**2 + f4**2)

        u = (I[6] - I[12]) / (I[7] - I[13])
        f1 = DI[6] / (I[7] - I[13])
        f2 = DI[12] / (I[7] - I[13])
        f3 = u * DI[7] / (I[7] - I[13])
        f4 = u * DI[13] / (I[7] - I[13])
        Dd2 = 1 / (1 + u**2) * np.sqrt(f1**2 + f2**2 + f3**2 + f4**2)

        u = (I[8] - I[14]) / (I[9] - I[15])
        f1 = DI[8] / (I[9] - I[15])
        f2 = DI[14] / (I[9] - I[15])
        f3 = u * DI[9] / (I[9] - I[15])
        f4 = u * DI[15] / (I[9] - I[15])
        Dd3 = np.sqrt((f1**2 + f2**2 + f3**2 + f4**2) / (1 + u**2) + Dd2**2)

        u = J0 * J3 * np.exp(1j * d3)
        v = J1 * J2 * np.exp(1j * (d1 + d2))
        Ddet2 = (DJ0 * u / J0)**2 + (DJ3 * u / J3)**2 + (Dd3 * u * 1j)**2 + \
            (DJ1 * v / J1)**2 + (DJ2 * v / J2)**2 + \
            (Dd1 * v * 1j)**2 + (Dd2 * v * 1j)**2
        Ddet = np.sqrt(np.abs(Ddet2))
        errors = [DJ0, DJ1, DJ2, DJ3, Ddet, Dd1, Dd2, Dd3]

    # Return
    ret = [components]
    if DI is not None:
        ret.append(errors)
    if len(ret) == 1:
        ret = ret[0]

    return ret


def Metodo_Hoyo(I, DI=None):
    """Function to extract the Jones parameters of the Extended Hoyo method.

    Args:
        I (tuple or list): List of measured intensities (at least 16 elements).
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.

    Returns:
        components (list): List of J0, J1, J2, J3, det, d1, d2, d3.
        errors (list or None): If DI is not None, component error.
    """
    # Calculate components
    J0 = np.sqrt(I[0])
    J1 = np.sqrt(I[3])
    J2 = np.sqrt(I[1])
    J3 = np.sqrt(I[2])
    d1 = np.arctan2(2 * I[4] - I[0] - I[3],
                    2 * I[5] - I[0] - I[3]) % (2 * np.pi)
    d2 = np.arctan2(2 * I[6] - I[0] - I[1],
                    2 * I[7] - I[0] - I[1]) % (2 * np.pi)
    d3 = (np.arctan2(2 * I[8] - I[1] - I[2], 2 * I[9] - I[1] - I[2]) + d2) % (
        2 * np.pi)
    determinant = np.abs(J0 * J3 * np.exp(1j * d3) -
                         J1 * J2 * np.exp(1j * (d1 + d2)))
    components = [J0, J1, J2, J3, determinant, d1, d2, d3]

    # Calculate errors
    if DI is not None:
        DJ0 = np.sqrt(DI[0]) / np.sqrt(I[0])
        DJ1 = np.sqrt(DI[3]) / np.sqrt(I[3])
        DJ2 = np.sqrt(DI[1]) / np.sqrt(I[1])
        DJ3 = np.sqrt(DI[2]) / np.sqrt(I[2])

        u = (2 * I[4] - I[0] - I[3]) / (2 * I[5] - I[0] - I[3])
        f1 = 2 * DI[4] / (2 * I[5] - I[0] - I[3])
        f2 = DI[0] / (2 * I[5] - I[0] - I[3])
        f3 = DI[3] / (2 * I[5] - I[0] - I[3])
        f4 = 2 * u * DI[5] / (2 * I[5] - I[0] - I[3])
        f5 = u * DI[0] / (2 * I[5] - I[0] - I[3])
        f6 = u * DI[3] / (2 * I[5] - I[0] - I[3])
        Dd1 = 1 / (
            1 + u**2) * np.sqrt(f1**2 + f2**2 + f3**2 + f4**2 + f5**2 + f6**2)

        u = (2 * I[6] - I[0] - I[3]) / (2 * I[7] - I[0] - I[3])
        f1 = 2 * DI[6] / (2 * I[7] - I[0] - I[3])
        f2 = DI[0] / (2 * I[7] - I[0] - I[3])
        f3 = DI[3] / (2 * I[7] - I[0] - I[3])
        f4 = 2 * u * DI[7] / (2 * I[7] - I[0] - I[3])
        f5 = u * DI[0] / (2 * I[7] - I[0] - I[3])
        f6 = u * DI[3] / (2 * I[7] - I[0] - I[3])
        Dd2 = 1 / (
            1 + u**2) * np.sqrt(f1**2 + f2**2 + f3**2 + f4**2 + f5**2 + f6**2)

        u = (2 * I[8] - I[0] - I[3]) / (2 * I[9] - I[0] - I[3])
        f1 = 2 * DI[8] / (2 * I[9] - I[0] - I[3])
        f2 = DI[0] / (2 * I[9] - I[0] - I[3])
        f3 = DI[3] / (2 * I[9] - I[0] - I[3])
        f4 = 2 * u * DI[9] / (2 * I[9] - I[0] - I[3])
        f5 = u * DI[0] / (2 * I[9] - I[0] - I[3])
        f6 = u * DI[3] / (2 * I[9] - I[0] - I[3])
        Dd3 = np.sqrt(f1**2 + f2**2 + f3**2 + f4**2 + f5**2 + f6**2)
        Dd3 = np.sqrt(Dd3**2 / (1 + u**2) + Dd2**2)

        u = J0 * J3 * np.exp(1j * d3)
        v = J1 * J2 * np.exp(1j * (d1 + d2))
        Ddet2 = (DJ0 * u / J0)**2 + (DJ3 * u / J3)**2 + (Dd3 * u * 1j)**2 + \
            (DJ1 * v / J1)**2 + (DJ2 * v / J2)**2 + \
            (Dd1 * v * 1j)**2 + (Dd2 * v * 1j)**2
        Ddet = np.sqrt(np.abs(Ddet2))
        errors = [DJ0, DJ1, DJ2, DJ3, Ddet, Dd1, Dd2, Dd3]

    # Return
    ret = [components]
    if DI is not None:
        ret.append(errors)
    if len(ret) == 1:
        ret = ret[0]

    return ret


def Metodo_Moreno(I, DI=None, give_param=False):
    """Function to extract the Jones parameters of the Moreno method.

    Args:
        I (tuple or list): List of measured intensities (at least 16 elements).
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.
        give_param (bool): If True, the retarder parameters x, y, z and w are returned. Default: False.

    Returns:
        components (list): List of J0, J1, J2, J3, det, d1, d2, d3.
        errors (list or None): If DI is not None, component error.
        x, y, z, w (np.ndarray): If give_param is True, the retarder parameters are returned.
    """
    # Choose depending on the method
    if len(I) > 7:
        Inew = [I[0], I[1], I[12], I[14], I[5], I[9], I[7]]
    else:
        Inew = I

    # Calculate retarder coefficients
    x2 = Inew[0] / (1 + ((Inew[6] - Inew[5]) / (Inew[2] - Inew[3]))**2)
    y2 = Inew[0] / (1 + ((Inew[2] - Inew[3]) / (Inew[6] - Inew[5]))**2)
    z2 = Inew[1] / (1 + ((Inew[2] - Inew[3]) / (Inew[4] - Inew[6]))**2)
    w2 = Inew[1] / (1 + ((Inew[4] - Inew[6]) / (Inew[2] - Inew[3]))**2)
    sz = np.sign(Inew[4] - Inew[6])
    sw = np.sign(Inew[2] - Inew[3])
    sy = np.sign(Inew[6] - Inew[5]) / sw
    x = np.sqrt(x2)
    y = np.sqrt(y2) * sy
    z = np.sqrt(z2) * sz
    w = np.sqrt(w2) * sw

    # Calcualte Jones matrix coefficients
    components = Ret_Parameters_To_Jones_Parameters(x, y, z, w)

    # Calculate errors
    if DI is not None:
        J0, J1, J2, J3, determinant, d1, d2, d3 = components
        if len(DI) > 7:
            DInew = [DI[0], DI[1], DI[12], DI[14], DI[5], DI[9], DI[7]]
        else:
            DInew = DI

        u = (Inew[6] - Inew[5]) / (Inew[2] - Inew[3])
        Du2 = u**2 * ((DInew[6] / (Inew[6] - Inew[5]))**2 +
                      (DInew[5] / (Inew[6] - Inew[5]))**2 +
                      (DInew[2] / (Inew[2] - Inew[3]))**2 +
                      (DInew[3] / (Inew[2] - Inew[3]))**2)
        Dx2 = np.sqrt((DInew[0] * x2 / Inew[0])**2 +
                      Du2 * (2 * u * x2 / (1 + u**2))**2)
        Dx = Dx2 / (2 * x)

        u = (Inew[2] - Inew[3]) / (Inew[6] - Inew[5])
        Du2 = u**2 * ((DInew[2] / (Inew[2] - Inew[3]))**2 +
                      (DInew[3] / (Inew[2] - Inew[3]))**2 +
                      (DInew[6] / (Inew[6] - Inew[5]))**2 +
                      (DInew[5] / (Inew[6] - Inew[5]))**2)
        Dy2 = np.sqrt((DInew[0] * y2 / Inew[0])**2 +
                      Du2 * (2 * u * y2 / (1 + u**2))**2)
        Dy = Dy2 / (2 * y)

        u = (Inew[2] - Inew[3]) / (Inew[4] - Inew[6])
        Du2 = u**2 * ((DInew[2] / (Inew[2] - Inew[3]))**2 +
                      (DInew[3] / (Inew[2] - Inew[3]))**2 +
                      (DInew[4] / (Inew[4] - Inew[6]))**2 +
                      (DInew[6] / (Inew[4] - Inew[6]))**2)
        Dz2 = np.sqrt((DInew[1] * z2 / Inew[1])**2 +
                      Du2 * (2 * u * z2 / (1 + u**2))**2)
        Dz = Dz2 / (2 * z)

        u = (Inew[4] - Inew[6]) / (Inew[2] - Inew[3])
        Du2 = u**2 * ((DInew[4] / (Inew[4] - Inew[6]))**2 +
                      (DInew[6] / (Inew[4] - Inew[6]))**2 +
                      (DInew[2] / (Inew[2] - Inew[3]))**2 +
                      (DInew[3] / (Inew[2] - Inew[3]))**2)
        Dw2 = np.sqrt((DInew[1] * w2 / Inew[1])**2 +
                      Du2 * (2 * u * w2 / (1 + u**2))**2)
        Dw = Dw2 / (2 * w)

        DJ0 = np.sqrt(Dx2**2 + Dy2**2) / (2 * J0)
        DJ3 = DJ0
        DJ1 = np.sqrt(Dz2**2 + Dw2**2) / (2 * J1)
        DJ2 = DJ1

        Dd0 = np.sqrt((Dy / x)**2 + (Dx * y / x2)**2) / (1 + y2 / x2)
        Dd3 = np.sqrt(2) * Dd0
        Dd1 = np.sqrt((Dw / z)**2 + (Dz * w / z2)**2) / (1 + w2 / z2)
        Dd1 = np.sqrt(Dd1**2 + Dd0**2)
        Dd2 = Dd1

        u = J0 * J3 * np.exp(1j * d3)
        v = J1 * J2 * np.exp(1j * (d1 + d2))
        Ddet2 = (DJ0 * u / J0)**2 + (DJ3 * u / J3)**2 + (Dd3 * u * 1j)**2 + \
            (DJ1 * v / J1)**2 + (DJ2 * v / J2)**2 + \
            (Dd1 * v * 1j)**2 + (Dd2 * v * 1j)**2
        Ddet = np.sqrt(np.abs(Ddet2))
        errors = [DJ0, DJ1, DJ2, DJ3, Ddet, Dd1, Dd2, Dd3]

    # Return
    ret = [components]
    if DI is not None:
        ret.append(errors)
    if give_param:
        ret.append((x, y, z, w))
    if len(ret) == 1:
        ret = ret[0]
    return ret


def Metodo_Baiheng(I, signs=(+1, +1, -1, -1), DI=None, give_param=False):
    """Function to extract the Jones parameters of the Baiheng method.

    Args:
        I (iterable): List of measured intensities (at least 16 elements).
        signs (iterable): List of signs (4 elements). Default: (+1, +1, -1, -1).
        DI (tuple, list or None): List of intensity error. If None, no errors are calculated.
        give_param (bool): If True, the retarder parameters x, y, z and w are returned. Default: False.

    Returns:
        components (list): List of J0, J1, J2, J3, det, d1, d2, d3.
        errors (list or None): If DI is not None, component error.
    """
    # Choose depending on the method
    if len(I) > 3:
        Inew = [I[0], I[16], I[17]]
    else:
        Inew = I

    # Calculate retarder coefficients
    x2 = (Inew[0] + Inew[1] + Inew[2] - 1) / 2
    y2 = (Inew[0] - Inew[1] - Inew[2] + 1) / 2
    z2 = (-Inew[0] + Inew[1] - Inew[2] + 1) / 2
    w2 = (-Inew[0] - Inew[1] + Inew[2] + 1) / 2
    # Correct negative values
    x2[x2 < 0] = 0
    y2[y2 < 0] = 0
    z2[z2 < 0] = 0
    w2[w2 < 0] = 0

    sx, sy, sz, sw = signs
    x = np.sqrt(x2) * sx
    y = np.sqrt(y2) * sy
    z = np.sqrt(z2) * sz
    w = np.sqrt(w2) * sw

    # Calcualte Jones matrix coefficients
    components = Ret_Parameters_To_Jones_Parameters(x, y, z, w)

    # Calculate errors
    if DI is not None:
        J0, J1, J2, J3, determinant, d1, d2, d3 = components
        if len(DI) > 3:
            DInew = [DI[0], DI[16], DI[17]]
        else:
            DInew = DI

        Du2 = np.sqrt(DInew[0]**2 + DInew[1]**2 + DInew[2]**2)
        Dx = Du2 / (2 * x)
        Dy = Du2 / (2 * y)
        Dz = Du2 / (2 * z)
        Dw = Du2 / (2 * w)

        DJ0 = np.sqrt(2 * Du2**2) / (2 * J0)
        DJ3 = DJ0
        DJ1 = np.sqrt(2 * Du2**2) / (2 * J1)
        DJ2 = DJ1

        Dd0 = np.sqrt((Dy / x)**2 + (Dx * y / x2)**2) / (1 + y2 / x2)
        Dd3 = np.sqrt(2) * Dd0
        Dd1 = np.sqrt((Dw / z)**2 + (Dz * w / z2)**2) / (1 + w2 / z2)
        Dd1 = np.sqrt(Dd1**2 + Dd0**2)
        Dd2 = Dd1

        u = J0 * J3 * np.exp(1j * d3)
        v = J1 * J2 * np.exp(1j * (d1 + d2))
        Ddet2 = (DJ0 * u / J0)**2 + (DJ3 * u / J3)**2 + (Dd3 * u * 1j)**2 + \
            (DJ1 * v / J1)**2 + (DJ2 * v / J2)**2 + \
            (Dd1 * v * 1j)**2 + (Dd2 * v * 1j)**2
        Ddet = np.sqrt(np.abs(Ddet2))
        errors = [DJ0, DJ1, DJ2, DJ3, Ddet, Dd1, Dd2, Dd3]

    # Return
    ret = [components]
    if DI is not None:
        ret.append(errors)
    if give_param:
        ret.append((x, y, z, w))
    if len(ret) == 1:
        ret = ret[0]
    return ret


def Metodo_Amaya(I, param0=None, tol=1e-12, give_seed=False, give_param=False):
    """Function to extract the Jones parameters of the Amaya method.

    Args:
        I (iterable): List of measured intensities (at least 16 elements).
        param0 (str or np.ndarray): Initial seed for least squares fit. If None, creates it randomly. Default: None.
        tol (float): Tolerance for least_squares method. Default: 1e-12.
        give_seed (bool): If True, the seed param0 is returned (useful if param0 is None). Default: False.
        give_param (bool): If True, the retarder parameters x, y, z and w are returned. Default: False.

    Returns:
        components (list): List of J0, J1, J2, J3, det, d1, d2, d3.
        errors (list or None): If DI is not None, component error.
    """
    # Choose depending on the method
    if len(I) > 8:
        Inew = [I[2], I[15], I[12], I[18], I[17], I[7], I[19], I[20]]
    else:
        Inew = I
    N = Inew[0].size

    # Perform the fit
    if param0 is None:
        param0 = 2 * np.random.rand(N * 4) - 1
    bounds = (-1, 1)
    result = least_squares(
        fun=Error_Metodo_Amaya,
        x0=param0,
        bounds=bounds,
        args=[Inew],
        xtol=tol,
        gtol=tol,
        ftol=tol)

    # Extract information from the fit
    x, y, z, w = (result.x[:N], result.x[N:2 * N], result.x[2 * N:3 * N],
                  result.x[3 * N:])
    components = Ret_Parameters_To_Jones_Parameters(x, y, z, w)
    (J0, J1, J2, J3, determinant, d1, d2, d3) = components

    # Calculate error
    J = result.jac
    cov = np.linalg.inv(J.T.dot(J))
    var = np.sqrt(np.diagonal(cov))
    Dx, Dy, Dz, Dw = (var[:N], var[N:2 * N], var[2 * N:3 * N], var[3 * N:])

    DJ0 = 2 * np.sqrt((x * Dx)**2 + (y * Dy)**2) / (2 * J0)
    DJ3 = DJ0
    DJ1 = 2 * np.sqrt((z * Dz)**2 + (w * Dw)**2) / (2 * J1)
    DJ2 = DJ1

    Dd0 = np.sqrt((Dy / x)**2 + (Dx * y / x**2)**2) / (1 + y**2 / x**2)
    Dd3 = np.sqrt(2) * Dd0
    Dd1 = np.sqrt((Dw / z)**2 + (Dz * w / z**2)**2) / (1 + w**2 / z**2)
    Dd1 = np.sqrt(Dd1**2 + Dd0**2)
    Dd2 = Dd1

    u = J0 * J3 * np.exp(1j * d3)
    v = J1 * J2 * np.exp(1j * (d1 + d2))
    Ddet2 = (DJ0 * u / J0)**2 + (DJ3 * u / J3)**2 + (Dd3 * u * 1j)**2 + \
        (DJ1 * v / J1)**2 + (DJ2 * v / J2)**2 + \
        (Dd1 * v * 1j)**2 + (Dd2 * v * 1j)**2
    Ddet = np.sqrt(np.abs(Ddet2))
    errors = [DJ0, DJ1, DJ2, DJ3, Ddet, Dd1, Dd2, Dd3]

    # Return
    ret = [components, errors]
    if give_seed:
        ret.append(param0)
    if give_param:
        ret.append((x, y, z, w))
    return ret


def Calculate_Intensities_Amaya(param):
    """Function to compute the intensities obtained by the Amaya method to calculate the Jones matrix of the SLM. The error of this function (see below) will be given to the Least_squares method.

    Args:
        param (np.ndarray): Array of param: (X, Y, Z, W) concatenated. For this reason, the array must be of size 4*N.

    Returns:
        I (list): List of the eight calculated intensities.
    """
    # Transform param in X, Y, Z and W
    N = int(param.size / 4)
    X, Y, Z, W = (param[:N], param[N:2 * N], param[2 * N:3 * N], param[3 * N:])

    # Calculate intensities
    I = []
    I.append(X**2 + Y**2)
    I.append(0.5 + W * Y - X * Z)
    I.append(0.5 + W * X - Y * Z)
    I.append(0.5 - X * Y - W * Z)
    I.append(X**2 + Y**2)
    I.append(0.5 + W * Y + X * Z)
    I.append((2 * X - Y - r3 * W)**2 / 8 + (X + 2 * Y + r3 * Z)**2 / 8)
    I.append((2 * X + Y - r3 * W)**2 / 8 + (-X + 2 * Y + r3 * Z)**2 / 8)

    return I


def Error_Metodo_Amaya(param, I):
    """Calculates the difference between experimental and calculated errors. This function is given to the Least_squares function to find the correct parameters.

    Args:
        param (np.ndarray): Array of param: (X, Y, Z, W) concatenated. For this reason, the array must be of size 4*N.
        I (list): List of experimental intensity measurements.

    Returns
        DI (np.ndarray): Difference in intensities.
    """
    I_fit = Calculate_Intensities_Amaya(param)
    DI = []

    if len(I) > 8:
        DI.append(I[2] - I_fit[0])
        DI.append(I[15] - I_fit[1])
        DI.append(I[12] - I_fit[2])
        DI.append(I[18] - I_fit[3])
        DI.append(I[17] - I_fit[4])
        DI.append(I[7] - I_fit[5])
        DI.append(I[19] - I_fit[6])
        DI.append(I[20] - I_fit[7])

    else:
        DI.append(I[0] - I_fit[0])
        DI.append(I[1] - I_fit[1])
        DI.append(I[2] - I_fit[2])
        DI.append(I[3] - I_fit[3])
        DI.append(I[4] - I_fit[4])
        DI.append(I[5] - I_fit[5])
        DI.append(I[6] - I_fit[6])
        DI.append(I[7] - I_fit[7])

    return np.concatenate(DI)


###################################
# OPTIMIZATION AND VERIFICATION
###################################


def Check_Calibration_Analysis(path=None, N=256, method='retarder', DI=0):
    """Function to check that Jones_Calibration_Analysis works correctly. It simulates the intensity measurements and then performs the analysis with a known sample."""
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Create SLM matrix
    if method.lower() == 'retarder':
        X = np.linspace(0.8, 0.5, N)
        Y = np.linspace(0.2, 0.6, N)
        Z = np.linspace(0.2, 0.0, N)
        W = np.sqrt(1 - X**2 - Y**2 - Z**2)
        components = [X - 1j * Y, Z - 1j * W, -Z - 1j * W, X + 1j * Y]
        Jslm = Jones_matrix().from_components(components)
    else:
        az_p, az_r, R = np.random.rand(3) * 180 * degrees
        el_p, el_r = np.random.rand(2) * 90 * degrees - 45 * degrees
        p2 = np.linspace(1, 0, N)
        Jp = Jones_matrix().diattenuator_azimuth_ellipticity(
            azimuth=az_p, ellipticity=el_p, p1=1, p2=p2)
        Jr = Jones_matrix().retarder_azimuth_ellipticity(
            azimuth=az_r, ellipticity=el_r, R=R)
        Jslm = Jp * Jr
        components = Jslm.parameters.components()

    # Draw matrix elements
    Jslm.remove_global_phase()
    J0 = np.abs(components[0])
    J1 = np.abs(components[1])
    J2 = np.abs(components[2])
    J3 = np.abs(components[3])
    d0 = np.angle(components[0]) % (2 * np.pi)
    d1 = np.angle(components[1]) % (2 * np.pi)
    d2 = np.angle(components[2]) % (2 * np.pi)
    d3 = np.angle(components[3]) % (2 * np.pi)
    det = np.abs(J0 * J3 * np.exp(1j * d3) - J1 * J2 * np.exp(1j * (d1 + d2)))
    elements = [J0, J1, J2, J3, det, d1, d2, d3]
    titles = ('|J0|', '|J1|', '|J2|', '|J3|', 'det', 'd1', 'd2', 'd3')
    plt.figure(figsize=(10, 20))
    for ind, elem in enumerate(elements):
        plt.subplot(4, 2, ind + 1)
        plt.plot(elem)
        plt.title(titles[ind])
        plt.xlabel('SLM level')
        if ind > 4:
            plt.ylabel('Phase (rad)')
        else:
            plt.ylabel('Abs. value')

    # Create polarization objects
    E0 = Jones_vector().circular_light(intensity=2)
    P1 = Jones_matrix()
    P2 = Jones_matrix()
    Q1 = Jones_matrix()
    Q2 = Jones_matrix()

    # Perform the measurement
    for ind in range(angle_P1.size):
        for ort in range(2):
            # Set elements at their angles
            P1.diattenuator_linear(azimuth=angle_P1[ind] * degrees)
            P2.diattenuator_linear(azimuth=angle_P2[ind] * degrees +
                                   ort * 90 * degrees)
            Q1.quarter_waveplate(azimuth=angle_Q1[ind] * degrees)
            Q2.quarter_waveplate(azimuth=angle_Q2[ind] * degrees)

            # Calculate transmission
            Efinal = P2 * Q2 * Jslm * Q1 * P1 * E0
            Ifinal = Efinal.parameters.intensity(
            ) + DI * np.random.normal(size=N)

            # Save the file
            filename = "Calibracion_Ind_{}_Ort_{}.npz".format(ind, ort)
            np.savez(
                filename,
                power=Ifinal,
                error=Ifinal * 1e-6,
                indM=ind,
                ort=ort,
                Naverage=-1)

    # Go to path
    if path:
        os.chdir(old_path)


def Check_Calibration_Method(type='retarder', N=200, mirrors=def_mirrors):
    """Function to check that Jones_Calibration_Analysis works correctly. It simulates the intensity measurements and then performs the analysis with a known sample.

    Args:
        type (str): If 'retarder', the test SLM is a pure retarder. If not, a general Jones matrix whose determinant is linearly decreasing is used.
        N (int): Number of levels.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
    """
    N = int(N)
    # Create SLM matrix
    # az_r = np.random.rand(N) * 180 * degrees
    # R = np.random.rand(N) * 180 * degrees
    # el_r = np.random.rand(N) * 90 * degrees - 45 * degrees
    x = np.linspace(0, 1, N)
    levels = np.arange(N)
    az_r = x**2 * 180 * degrees
    R = x * 180 * degrees
    el_r = np.sqrt(x) * 90 * degrees - 45 * degrees
    Jr = Jones_matrix().retarder_azimuth_ellipticity(
        azimuth=az_r, ellipticity=el_r, R=R)
    if type == 'retarder':
        Jslm = Jr
    else:
        az_p = np.random.rand(N) * 180 * degrees
        el_p = np.random.rand(N) * 90 * degrees - 45 * degrees
        p1 = np.linspace(1, 0.5, N)
        p2 = np.linspace(1, 0, N)
        Jp = Jones_matrix().diattenuator_azimuth_ellipticity(
            azimuth=az_p, ellipticity=el_p, p1=p1, p2=p2)
        Jslm = Jr * Jp
    Jslm.remove_global_phase()
    comp = Jslm.parameters.components()
    det_slm = np.abs(Jslm.parameters.det())
    pi2 = 2 * np.pi
    comp_theor = [
        np.abs(comp[0]),
        np.abs(comp[1]),
        np.abs(comp[2]),
        np.abs(comp[3]), det_slm,
        np.angle(comp[1]) % pi2,
        np.angle(comp[2]) % pi2,
        np.angle(comp[3]) % pi2
    ]
    # Mirrors
    J_mirror = Jones_matrix().mirror()
    if mirrors[0]:
        Jslm = Jslm * J_mirror
    if mirrors[1]:
        Jslm = J_mirror * Jslm

    # Perform the measurement
    I = []
    for ind in range(angle_P1.size):
        # Calculate
        angles = Calculate_Angles(
            ind,
            ort=0,
            mirrors=mirrors,
            check_repeat=False,
            return_angles=True,
            add_zero=False)
        Ifinal, _ = Calculate_Transmission(angles, Jslm)
        I.append(Ifinal)

    # Calculate methods
    comp_hoyo_ext = Metodo_Hoyo_Ext(I)

    # Hoyo
    comp_hoyo = Metodo_Hoyo(I)

    # Moreno
    comp_moreno, param = Metodo_Moreno(I, give_param=True)
    signs = (np.sign(param[0]), np.sign(param[1]), np.sign(param[2]),
             np.sign(param[3]))

    # Baiheng
    # comp_baiheng, param_baiheng = Metodo_Baiheng(
    #     I, signs=signs, give_param=True)

    # Amaya
    # comp_amaya, _ = Metodo_Amaya(I)

    # Draw
    titles = ('|J0|', '|J1|', '|J2|', '|J3|', 'det', 'd1', 'd2', 'd3')
    leg = ('Experimental', 'Hoyo ext.', 'Hoyo', 'Moreno', 'Baiheng', 'Amaya')
    ylabels = ['Error'] * 5 + ['Error (rad)'] * 3
    plt.figure(figsize=(24, 12))
    for ind in range(8):
        plt.subplot(2, 4, ind + 1)
        plt.plot(levels, comp_theor[ind], linewidth=3)
        plt.plot(levels, comp_hoyo_ext[ind])
        plt.plot(levels[::10], comp_hoyo[ind][::10], '+')
        plt.plot(levels, comp_moreno[ind])
        # plt.plot(levels, comp_baiheng[ind], '--')
        # plt.plot(levels, comp_amaya[ind])
        plt.title(titles[ind])
        if ind > 4:
            plt.ylabel('Phase (rad)')
        else:
            plt.ylabel('Abs. value')
        plt.legend(leg)


def Check_Calibration_Analysis_aux_old(type='retarder', N=200):
    """Function to check that Jones_Calibration_Analysis works correctly. It simulates the intensity measurements and then performs the analysis with a known sample.

    Args:
        type (str): If 'retarder', the test SLM is a pure retarder. If not, a general Jones matrix whose determinant is linearly decreasing is used.
        N (int): Number of levels.
    """
    N = int(N)
    # Create SLM matrix
    # az_r = np.random.rand(N) * 180 * degrees
    # R = np.random.rand(N) * 180 * degrees
    # el_r = np.random.rand(N) * 90 * degrees - 45 * degrees
    x = np.linspace(0, 1, N)
    az_r = x**2 * 180 * degrees
    R = x * 180 * degrees
    el_r = np.sqrt(x) * 90 * degrees - 45 * degrees
    Jr = Jones_matrix().retarder_azimuth_ellipticity(
        azimuth=az_r, ellipticity=el_r, R=R)
    if type == 'retarder':
        Jslm = Jr
    else:
        az_p = np.random.rand(N) * 180 * degrees
        el_p = np.random.rand(N) * 90 * degrees - 45 * degrees
        p1 = np.linspace(1, 0.5, N)
        p2 = np.linspace(1, 0, N)
        Jp = Jones_matrix().diattenuator_azimuth_ellipticity(
            azimuth=az_p, ellipticity=el_p, p1=p1, p2=p2)
        Jslm = Jr * Jp
    Jslm.remove_global_phase()
    comp = Jslm.parameters.components()
    det_slm = np.abs(Jslm.parameters.det())
    pi2 = 2 * np.pi
    comp_theor = [
        np.abs(comp[0]),
        np.abs(comp[1]),
        np.abs(comp[2]),
        np.abs(comp[3]), det_slm,
        np.angle(comp[1]) % pi2,
        np.angle(comp[2]) % pi2,
        np.angle(comp[3]) % pi2
    ]

    # Create polarization objects
    E0 = Jones_vector().circular_light(intensity=2)
    P1 = Jones_matrix()
    P2 = Jones_matrix()
    Q1 = Jones_matrix()
    Q2 = Jones_matrix()

    angle_P1 = np.array([
        0, 0, 90, 90, 45, 45, 0, 0, 45, 45, 135, 135, 0, 0, 135, 135, 45, 45,
        45, 90, 90
    ])
    angle_Q1 = np.array([
        0, 0, 90, 90, 0, 45, 0, 0, 0, 45, 0, 135, 0, 0, 0, 135, 0, 45, 45, 90,
        90
    ])
    angle_Q2 = np.array([
        0, 90, 90, 0, 0, 0, 0, 45, 90, 90, 0, 0, 0, 135, 90, 90, 0, 45, 0, 60,
        60
    ])
    angle_P2 = np.array([
        0, 90, 90, 0, 0, 0, 45, 45, 90, 90, 0, 0, 135, 135, 90, 90, 45, 45,
        135, 30, 90
    ])

    # Perform the measurement
    I = []
    for ind in range(angle_P1.size):
        # Set elements at their angles
        P1.diattenuator_linear(azimuth=angle_P1[ind] * degrees)
        P2.diattenuator_linear(azimuth=angle_P2[ind] * degrees)
        Q1.quarter_waveplate(azimuth=angle_Q1[ind] * degrees)
        Q2.quarter_waveplate(azimuth=angle_Q2[ind] * degrees)

        # Calculate transmission
        Efinal = P2 * Q2 * Jslm * Q1 * P1 * E0
        Ifinal = Efinal.parameters.intensity()
        I.append(Ifinal)

    # Calculate methods
    comp_hoyo_ext = Metodo_Hoyo_Ext(I)

    # Hoyo
    comp_hoyo = Metodo_Hoyo(I)

    # Moreno
    comp_moreno, param = Metodo_Moreno(I, give_param=True)
    signs = (np.sign(param[0]), np.sign(param[1]), np.sign(param[2]),
             np.sign(param[3]))

    # Baiheng
    # comp_baiheng, param_baiheng = Metodo_Baiheng(
    # I, signs=signs, give_param=True)

    # Amaya
    # comp_amaya, _ = Metodo_Amaya(I)

    # Draw
    titles = ('|J0|', '|J1|', '|J2|', '|J3|', 'det', 'd1', 'd2', 'd3')
    leg = ('Hoyo ext.', 'Hoyo', 'Moreno', 'Baiheng', 'Amaya')
    ylabels = ['Error'] * 5 + ['Error (rad)'] * 3
    plt.figure(figsize=(24, 12))
    for ind in range(8):
        plt.subplot(2, 4, ind + 1)
        plt.plot(comp_hoyo_ext[ind] - comp_theor[ind])
        plt.plot(comp_hoyo[ind] - comp_theor[ind])
        plt.plot(comp_moreno[ind] - comp_theor[ind])
        plt.plot(comp_baiheng[ind] - comp_theor[ind])
        plt.plot(comp_amaya[ind] - comp_theor[ind])
        plt.title(titles[ind])
        if ind > 4:
            plt.ylabel('Phase (rad)')
        else:
            plt.ylabel('Abs. value')
        plt.legend(leg)


def Compare_Measured_Theorical(angles,
                               file_comp="Analysis_calibration.npz",
                               Im=None,
                               DIm=None,
                               file_phase="Phase_calibration.npz",
                               Pm=None,
                               DPm=None,
                               path=None,
                               mirrors=def_mirrors,
                               norm=None,
                               title='',
                               draw=False,
                               print_errors=False):
    """Function to plot an intensity or phase measurement and compare it with the model.

    Args:
        angles (np.ndarray): Current angles.
        file_comp (string): File where the measured Jones matrices components of the SLM are stored. Default: "Analysis_calibration.npz".
        Im (np.ndarray or None): Measured intensity. If None, intensity comparison is not performed. Default: None.
        DIm (np.ndarray or None): Measured intensity error. Default: None.
        file_phase (string): File where the measured global phase of the Jones matrices of the SLM are stored. If None,  phase comparison is not performed. Default: "Phase_calibration.npz".
        Pm (np.ndarray or None): Measured phase. If None, phase comparison is not performed. Default: None.
        DPm (np.ndarray or None): Measured phase error. Default: None.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        method (string): Method to calculate the the configuration. Default: 'all'.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        norm (str or None): If not None, it is normalized to the chosen variable. Defauult: None.
        title (str): Title label. Default: ''.
        draw (bool): If True, the comparison is plotted. Default: False.
        print_errors (bool): If True, the errors are printed. Default: False.
    """
    # Safety
    if (Im is None) and ((Pm is None) or (file_phase is None)):
        print('Input parameters wrong')

    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Load Jones matrix components
    loaded = np.load(file_comp)
    components_hoyo_ext = loaded['components_hoyo_ext']
    components_hoyo = loaded['components_hoyo']
    components_moreno = loaded['components_moreno']
    #components_amaya = loaded['components_amaya']
    #components_baiheng = loaded['components_baiheng']
    #components_mueller = loaded['components_mueller']
    # Create matrices
    J_hoyo_ext = Components_To_Matrix(components_hoyo_ext)
    J_hoyo = Components_To_Matrix(components_hoyo)
    J_moreno = Components_To_Matrix(components_moreno)
    #J_baiheng = Components_To_Matrix(components_baiheng)
    #J_amaya = Components_To_Matrix(components_amaya)
    #M_mueller = Components_To_Matrix(components_mueller,type='mueller')
    E0 = Jones_vector().general_azimuth_ellipticity(
        azimuth=azimuth_source,
        ellipticity=ellipticity_source)

    # Mirrors
    J_mirror = Jones_matrix().mirror()
    if mirrors[0]:
        J_hoyo_ext = J_hoyo_ext * J_mirror
        J_hoyo = J_hoyo * J_mirror
        J_moreno = J_moreno * J_mirror
        J_baiheng = J_baiheng * J_mirror
        J_amaya = J_amaya * J_mirror
        M_mueller = M_mueller * J_mirror
    if mirrors[1]:
        J_hoyo_ext = J_mirror * J_hoyo_ext
        J_hoyo = J_mirror * J_hoyo
        J_moreno = J_mirror * J_moreno
        J_baiheng = J_mirror * J_baiheng
        J_amaya = J_mirror * J_amaya
        M_mueller = J_mirror * M_mueller


    # Add global phase
    if (file_phase is not None) and (Pm is not None):
        loaded = np.load(file_phase)
        phase = loaded['phase_av']
        if J_hoyo_ext is not None:
            J_hoyo_ext.add_global_phase(phase)
        if J_hoyo is not None:
            J_hoyo.add_global_phase(phase)
        if J_moreno is not None:
            J_moreno.add_global_phase(phase)
        if J_baiheng is not None:
            J_baiheng.add_global_phase(phase)
        if J_amaya is not None:
            J_amaya.add_global_phase(phase)
        if M_mueller is not None:
            M_mueller.add_global_phase(phase)

    # Calculation
    I_hoyo_ext, P_hoyo_ext = Calculate_Transmission(angles, J_hoyo_ext, E0=E0)
    I_hoyo, P_hoyo = Calculate_Transmission(angles, J_hoyo, E0=E0)
    I_moreno, P_moreno = Calculate_Transmission(angles, J_moreno, E0=E0)
    #I_baiheng, P_baiheng = Calculate_Transmission(angles, J_baiheng, E0=E0)
    #I_amaya, P_amaya = Calculate_Transmission(angles, J_amaya, E0=E0)
    #I_mueller, P_mueller = Calculate_Transmission(angles, M_mueller, E0=E0)

    k_hoyo_ext =(Im.mean()/I_hoyo_ext.mean())
    I_hoyo_ext = k_hoyo_ext*I_hoyo_ext

    k_hoyo = (Im.mean()/I_hoyo.mean())
    I_hoyo = k_hoyo*I_hoyo

    k_moreno = (Im.mean()/I_moreno.mean())
    I_moreno = k_moreno*I_moreno

    #k_baiheng = (Im/I_baiheng).mean()
    #I_baiheng = k_baiheng*I_baiheng

    #k_mueller = (Im/I_mueller).mean()
    #I_mueller = k_mueller*I_mueller

    # if norm is None:
    #     cte_norm = 0.5
    # elif isinstance(norm, (int, float)):
    #     cte_norm = norm
    # elif isinstance(norm, str):
    #     It = eval("I_" + norm)
    #     cte_norm = np.sum(Im * It) / np.sum(Im**2)
    # else:
    #     cte_norm = 0.5
    # Im *= cte_norm

    # Plot
    levels = np.arange(I_hoyo_ext.size)
    cond2 = (Im is not None) and (Pm is not None) and (file_phase is not None)
    if draw:
        if cond2:
            plt.figure(figsize=(16, 8))
            plt.subplot(1, 2, 1)
        else:
            plt.figure(figsize=(8, 8))

    if draw and (Im is not None):
        plt.plot(levels, Im, linewidth=4)
        plt.plot(levels, I_hoyo_ext, linewidth=1)
        plt.plot(levels, I_hoyo, linewidth=1)
        #plt.plot(levels, I_moreno, linewidth=1)
        #plt.plot(levels, I_baiheng, linewidth=1)
        #plt.plot(levels, I_amaya, linewidth=1)
        #plt.plot(levels, I_mueller, linewidth=1)
        plt.xlabel('SLM level')
        plt.ylabel('Intensity (a.u.)')
        plt.ylim(0,1)
        plt.title(title + str(angles / degrees))
        plt.legend(('Experimental', 'Hoyo ext', 'Hoyo', 'Moreno', 'Baiheng',
                    'Amaya','Mueller'))
        # plt.legend(('Experimental', 'Hoyo', 'Moreno'))
        if DIm is not None:
            plt.errorbar(x=levels[::10], y=Im[::10], yerr=DIm[::10], color='b')

    if print_errors and (Im is not None):
        mean = np.mean(Im) * Im.size
        err = np.sqrt(np.sum((Im - I_hoyo_ext)**2)) / mean
        print("The RMS error of Hoyo ext. method is: ", err)
        err = np.sqrt(np.sum((Im - I_hoyo)**2)) / mean
        print("The RMS error of Hoyo method is: ", err)
        err = np.sqrt(np.sum((Im - I_moreno)**2)) / mean
        print("The RMS error of Moreno method is: ", err)
        err = np.sqrt(np.sum((Im - I_baiheng)**2)) / mean
        print("The RMS error of Baiheng method is: ", err)
        err = np.sqrt(np.sum((Im - I_amaya)**2)) / mean
        print("The RMS error of Amaya method is: ", err)
        err = np.sqrt(np.sum((Im - I_mueller)**2)) / mean
        print("The RMS error of Mueller method is: ", err)

    if draw and (Pm is not None) and (file_phase is not None):
        if cond2:
            plt.subplot(1, 2, 2)
        Pm = (Pm) % (2 * np.pi)
        plt.plot(levels, Pm / degrees, linewidth=2)
        plt.plot(levels, P_hoyo_ext / degrees)
        plt.plot(levels, P_hoyo / degrees)
        plt.plot(levels, P_moreno / degrees)
        #plt.plot(levels, P_baiheng / degrees)
        #plt.plot(levels, P_amaya / degrees)
        #plt.plot(levels, P_mueller / degrees)
        plt.xlabel('SLM level')
        plt.ylabel('Phase (º)')
        plt.title(title)
        plt.legend(('Experimental', 'Hoyo ext', 'Hoyo', 'Moreno', 'Baiheng',
                    'Amaya','Mueller'))
        # plt.legend(('Experimental', 'Hoyo', 'Moreno'))
        if DPm is not None:
            plt.errorbar(x=levels[::10], y=Pm[::10], yerr=DPm[::10], color='b')

    # Go to older path
    if path:
        os.chdir(old_path)
    # Return
    Is = [Im, I_hoyo_ext, I_hoyo, I_moreno]
    Ps = [Pm, P_hoyo_ext, P_hoyo, P_moreno]
    return Is, Ps



def  Compare_Measured_Theorical_Mueller(angles,
                               Im=None,
                               DIm=None,
                               M_comp=None,
                               file_phase="Phase_calibration.npz",
                               Pm=None,
                               DPm=None,
                               mirrors=def_mirrors,
                               norm=None,
                               title='',
                               method="Icosaedro",
                               method_transmission='ideal',
                               filename_calibration=None,
                               draw=False,
                               print_errors=False):
    """Function to plot an intensity or phase measurement and compare it with the model.

    Args:
        angles (np.ndarray): Current angles.
        Im (np.ndarray or None): Measured intensity. If None, intensity comparison is not performed. Default: None.
        DIm (np.ndarray or None): Measured intensity error. Default: None.
        Mcomp (string): Filenames of the Mueller matrix componentes measured. Default: None.
        file_phase (string): File where the measured global phase of the Jones matrices of the SLM are stored. If None,  phase comparison is not performed. Default: "Phase_calibration.npz".
        Pm (np.ndarray or None): Measured phase. If None, phase comparison is not performed. Default: None.
        DPm (np.ndarray or None): Measured phase error. Default: None.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        norm (str or None): If 'Each' a certain constant is used for each intensity. If 'Mean' a mean normalization constant is used. If 'None' the intensities are not normalized. Default: None.
        title (str): Title label. Default: ''.
        method (string): Method to calculate the the configuration. Default: 'Icosaedro'.
        method_transmission (string): Method to calculate the transmision of PSA*M*PSG. Default: 'ideal'.
        filename_calibration (string): Filename with the calibration of the polarizing plates of the PSG and PSA. Default: None. 
        draw (bool): If True, the comparison is plotted. Default: False.
        print_errors (bool): If True, the errors are printed. Default: False.
    """
    # Safety
    if (Im is None) and ((Pm is None) or (file_phase is None)):
        print('Input parameters wrong')
    
    if len(method) == 1:
        M_comp_tetraedro = M_comp
        M_comp_icosaedro = M_comp
        
    else:
        M_comp_tetraedro = M_comp[0]
        M_comp_icosaedro = M_comp[1]


    if "Icosaedro" in method:
        components_icosaedro = np.load('M_Icosaedro_fix.npz')['components_mueller_optimal']
        M_icosaedro = Components_To_Matrix(components_icosaedro,type='mueller')
        I_icosaedro, P_icosaedro = Calculate_Transmission(angles, M_icosaedro,method_transmission=method_transmission,filename=filename_calibration)
        
        if np.any(I_icosaedro<0):
            I_icosaedro = I_icosaedro - I_icosaedro.min()

       
        if norm == "Mean":
            k = 1.4226409375245386 #az corrected
            #k = 1.851388573911534 #az=0
        elif norm == "Each":
            k = (Im.max()/I_icosaedro.max()) 
        else: 
            k = 1


        Im = Im/k
        dif = np.sqrt(np.mean(I_icosaedro/I_icosaedro.max() - Im/Im.max())**2)
      
    if "Tetraedro" in method:
        components_tetraedro = np.load('M_tetraedro_fix.npz')['components_mueller_optimal'] #PLUTO NIR
        #components_tetraedro = np.load(M_comp_tetraedro)['components_mueller_optimal']
        M_tetraedro = Components_To_Matrix(components_tetraedro,type='mueller')
        I_tetraedro, P_tetraedro = Calculate_Transmission(angles, M_tetraedro,method_transmission=method_transmission,filename=filename_calibration)
        #I_tetraedro[I_tetraedro<0] = 0.001

        if norm == "Mean":
            k = 2.0270318548600947#2.015678295138102 
        elif norm == "Each":
            k = (Im.mean()/I_tetraedro.mean()) 
        else: 
            k = 1

        
        Im = Im/k
        dif = np.mean(np.abs(I_tetraedro - Im))
           
    
    # Add global phase
    if (file_phase is not None) and (Pm is not None):
        loaded = np.load(file_phase)
        phase = loaded['phase_av']
        if M_icosaedro is not None:
            M_icosaedro.add_global_phase(phase)

    # Plot
    levels = np.arange(Im.size)
    
    cond2 = (Im is not None) and (Pm is not None) and (file_phase is not None)
    if draw:
        if cond2:
            plt.figure(figsize=(16, 8))
            plt.subplot(1, 2, 1)
        else:
            plt.figure(figsize=(8, 8))

    if draw and (Im is not None):
    
        plt.plot(levels, Im, linewidth=4,label='Experimental')
        if "Icosaedro" in method:
            plt.plot(levels, I_icosaedro, linewidth=1,label='Icosaedro' + ' ({:.3f}'.format(dif*100) + '%)')
            
        if "Tetraedro" in method:
            plt.plot(levels, I_tetraedro, linewidth=1,label='Tetraedro' + ' ({:.3f}'.format(dif*100) + '%)')
        
        plt.xlabel('SLM level')
        plt.ylabel('Intensity (a.u.)')
        plt.title(title + str(angles / degrees)+ str(k))
        plt.ylim(0,0.22)
        plt.legend()
        if DIm is not None:
            plt.errorbar(x=levels[::10], y=Im[::10], yerr=DIm[::10], color='b')

    if print_errors and (Im is not None):
        mean = np.mean(Im) * Im.size
        err = np.sqrt(np.sum((Im - I_icosaedro)**2)) / mean
        print("The RMS error of Mueller method is: ", err)

    if draw and (Pm is not None) and (file_phase is not None):
        if cond2:
            plt.subplot(1, 2, 2)
        Pm = (Pm) % (2 * np.pi)
        plt.plot(levels, Pm / degrees, linewidth=2)
        plt.plot(levels, P_icosaedro / degrees)
        plt.xlabel('SLM level')
        plt.ylabel('Phase (º)')
        plt.title(title)
        plt.legend(('Experimental', 'Mueller'))
        # plt.legend(('Experimental', 'Hoyo', 'Moreno'))
        if DPm is not None:
            plt.errorbar(x=levels[::10], y=Pm[::10], yerr=DPm[::10], color='b')

    # Return
    Is = [Im]
    Ps = [Pm]
    
    return Is, Ps, k,dif




def Check_Calibration_Positions(files,
                                file_comp,
                                path=None,
                                mirrors=def_mirrors):
    """Function to plot all the intensity measurements performed during the calibration and compare them with the models.

    Args:
        files (list): List of filenames containing the intensity measurements.
        file_comp (string): File where the measured Jones matrices components of the SLM are stored.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
    """
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Extract normalization factor
    loaded = np.load(file_comp)
    norm = loaded['normalization_factor']
    E0 = Jones_vector().general_azimuth_ellipticity(
        azimuth=azimuth_source,
        ellipticity=ellipticity_source,
        intensity=2)

    for ind, name in enumerate(files):
        # Load the measurement
        loaded = np.load(name)
        data = loaded['power']
        error = loaded['error']
        # Filter data
        data = Filter_Curves(data, method='widepeaks')
        # Calculate angles
        indM = int(ind / 2)
        indO = int(indM * 2 != ind)
        indM = ind
        indO = 0
        # print('{} - {}'.format(indM, indO))
        angles = Calculate_Angles(
            indM,
            indO,
            mirrors=def_mirrors,
            check_repeat=False,
            return_angles=True,
            add_zero=False)
        # Correct source ellipticity
        Jp = Jones_matrix().diattenuator_perfect(azimuth=angles[0])
        E_in = Jp * E0
        cte = E_in.parameters.intensity()
        cte_arb = 1
        data = data / (cte_arb * cte * norm)
        error = error / (cte_arb * cte * norm)
        #print('Max intensity is: ', data.max())

        # Compare
        
        Compare_Measured_Theorical(
            angles,
            file_comp,
            Im=data,
            norm=None,  # DIm=error,
            mirrors=def_mirrors,
            title='M = {}, O = {} '.format(indM, indO),
            draw=True)

        # Go to older path
        if path:
            os.chdir(old_path)



def Check_Calibration_Positions_Mueller(intensities,
                                files_M,
                                method="all",
                                mirrors=def_mirrors,
                                norm=None,
                                method_transmission='ideal',
                                filename_calibration=None):
    """Function to plot all the intensity measurements performed during the calibration and compare them with the models.

    Args:
        intensities (list): List of filenames containing the intensity measurements.
        file_M (string): File where the measured Mueller matrices components of the SLM are stored.
        method (string):
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        norm (str or None): If 'Each' a certain constant is used for each intensity. If 'Mean' a mean normalization constant is used. If 'None' the intensities are not normalized. Default: None.
        method_transmission (string): Method to calculate the transmision of PSA*M*PSG. Default: 'ideal'.
        filename_calibration (string): Filename with the calibration of the polarizing plates of the PSG and PSA. Default: None. 
    """
    #Creamos una lista para calcular la constate de normalización media entre todas las medidas realizadas.
    if norm == 'Each':
        print('Para normalizar con una cte media, primero calculala utilizando en esta misma función: norm=Each. La función (Check_Calibration_Positions_Pluto_I) te devolverá una lista con la norma de cada medida, sobre la cual podrás hacer la media')
        list_cte = []
    else:
        list_cte = None

    if "Icosaedro" in method:
        Nmeasures  = 144
    elif "Tetraedro" in method:
        Nmeasures = 16
    
    list_angles = calculate_polarimetry_angles(Nmeasures)
    # angles_random = np.load('Angles_random.npz')['angles']
    # list_angles = np.vstack((angles,angles_random))
    list_error = np.zeros(Nmeasures)
    for ind, name in enumerate(intensities):
        loaded = np.load(name)
        data = loaded['power']
        # Filter data
        data = Filter_Curves(data, method='widepeaks')
        # Calculate angles
        #angles = Calculate_Angles(ind,mirrors=def_mirrors,check_repeat=False,return_angles=True,add_zero=False)
        angles = list_angles[ind]
       
        # Compare
        Is, Ps,cte,error= Compare_Measured_Theorical_Mueller(
            angles,
            Im=data,
            M_comp=files_M,
            norm=norm, 
            mirrors=def_mirrors,
            title='M = {} '.format(ind),
            method=method,
            method_transmission=method_transmission,
            filename_calibration=filename_calibration,
            draw=True)
        
        if norm == 'Each':
            list_cte.append(cte)
        list_error[ind] = error
    
    return list_cte,list_error

def Check_Position(file_int='Intensity.npz',
                   file_phase="Phase.npz",
                   path=None,
                   angles=no_elements,
                   substract_zero=True,
                   norm=None,
                   phase_sign=1,
                   mirrors=def_mirrors,
                   draw=False,
                   print_errors=False):
    """File to check the calculated intensity and phase and compare it with the measurements.

    Args:
        file_int (string): File of the measured intensity. Default: 'Intensity.npz'.
        file_phase (string): File structure of the measured phase. Default: 'Phase.npz'.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        method (string): Method to calculate the the configuration. Default: 'all'.
        angles (np.ndarray): Angles of the elements. If negative, the element is not present. Default: [-1, -1, -1, -1].
        substract_zero (bool): If True, the origin of angles is substracted. Default: True.
        norm (str or None): If not None, it is normalized to the chosen variable. Defauult: None.
        phase_sign (+1 or -1): Global phase sign. Default: 1.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        draw (bool): If True, the comparison is plotted. Default: False.
        print_errors (bool): If True, the errors are printed. Default: False.
    """
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Load intensity
    if file_int is not None:
        loaded = np.load(file_int, allow_pickle=True)
        I = loaded['power']
    else:
        I = None

    # Load phase
    if file_phase is not None:
        data = np.load(file_phase, allow_pickle=True)
        phase = phase_sign * data["phase_av"]
    else:
        phase = None

    if substract_zero:
        cond = angles > 0
        if np.any(cond):
            angles[cond] = (angles[cond] - angles_0[cond]) % np.pi

    # Compare
    Is, Ps = Compare_Measured_Theorical(
        angles,
        Im=I,
        Pm=phase,
        norm=norm,  # DIm=error,
        mirrors=mirrors,
        title='Angles: ',
        draw=draw,
        print_errors=print_errors)

    # Go to older path
    if path:
        os.chdir(old_path)

    return Is, Ps


def Check_Current_Position(slm,
                           daca,
                           angles,
                           file_comp,
                           file_phase=None,
                           path=None,
                           levels=None,
                           Naverage=1,
                           phase_sign=1,
                           mirrors=def_mirrors):
    """Function to measure the current intensity or phase and compare it with the model.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        angles (np.ndarray): Current angles.
        file_comp (string): File where the measured Jones matrices components of the SLM are stored.
        file_phase (string): File where the measured global phase of the Jones matrices of the SLM are stored. If None, no phase comparison is performed. Default: None.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        method (string): Method to calculate the the configuration. Default: 'all'.
        phase_fit (bool): If True, the fit flobal phase is used. If False, the measured global phase is used. Default: False.
        phase_sign (+1 or -1): Global phase sign. Default: 1.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].

    Returns:
        intensity (np.ndarray): Measured intensity.
        phase (np.ndarray or None): Measured phase.
    """

    # Measure intensity
    gray_level = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)
    intensity = np.zeros(levels.size)
    error = np.zeros(levels.size)
    for indV, value in enumerate(levels):
        # Create the mask
        gray_level.gray_scale(
            num_levels=1, levelMin=np.sqrt(value), levelMax=np.sqrt(value))
        slm.Send_Image(image=gray_level, kind='intensity', norm=1)
        # Average intensity
        measures = np.zeros(Naverage)
        for indMM in range(Naverage):
            time.sleep(wait_time)
            measures[indMM] = daca.Get_Signal()
        # Calculate mean value and error
        intensity[indV] = np.mean(measures) * norm
        error[indV] = np.std(measures) * norm / np.sqrt(Naverage)


def Find_Positions(file_comp,
                   file_phase=None,
                   path=None,
                   method='all',
                   phase_fit=False,
                   mf_amplitude=None,
                   mf_phase=None,
                   phase_sign=1,
                   mirrors=def_mirrors,
                   Noptimizations=10,
                   verbose=False,
                   draw=False,
                   filename='Found_angles.npz'):
    """Function to find the angle positions of PSG and PSA to produce amplitude and phase modulation.

    Args:
        file_comp (string): File where the measured Jones matrices components of the SLM are stored.
        file_phase (string): File where the measured global phase of the Jones matrices of the SLM are stored. If None, no global phase is added. Default: None.
        path (string or None): If not None, the function will look for the files at this path location. Default: None.
        method (string): Method to calculate the the configuration. Default: 'all'.
        phase_fit (bool): If True, the fit flobal phase is used. If False, the measured global phase is used. Default: False.
        mf_amplitude (function): Merit function for amplitude modulation. If None, this optimization is not performed. Default: None.
        mf_phase (function): Merit function for amplitude modulation. If None, this optimization is not performed. Default: None.
        phase_sign (+1 or -1): Global phase sign. Default: 1.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        Noptimizations (int): Number of optimizations that will be performed for each case. The best among them will be selected. Default: 5.
        verbose (bool): If True, prints the determined angles. Default: True.
        draw (bool): If True, plots the measurement. Default: True.
        filename (str): Filename to save the data. Default: 'Found_angles.npz'.

    Returns:
        result (dict): Dictionary whose fields are each one a Get_Best_Optimization results dictionary.
    """
    start_time = time.time()
    # Go to path
    if path:
        old_path = os.getcwd()
        os.chdir(path)

    # Load components
    loaded = np.load(file_comp)
    components_hoyo_ext = loaded['components_hoyo_ext']
    components_hoyo = loaded['components_hoyo']
    components_moreno = loaded['components_moreno']
    components_amaya = loaded['components_amaya']
    components_baiheng = loaded['components_baiheng']
    # Create matrices
    J_hoyo_ext = Components_To_Matrix(components_hoyo_ext)
    J_hoyo = Components_To_Matrix(components_hoyo)
    J_moreno = Components_To_Matrix(components_moreno)
    J_baiheng = Components_To_Matrix(components_baiheng)
    J_amaya = Components_To_Matrix(components_amaya)

    # Add global phase
    if file_phase is not None:
        loaded = np.load(file_phase)
        if phase_fit:
            phase = phase_sign * loaded['phase_fit']
        else:
            phase = phase_sign * loaded['phase_av']
        if J_hoyo_ext is not None:
            J_hoyo_ext.add_global_phase(phase)
        if J_hoyo is not None:
            J_hoyo.add_global_phase(phase)
        if J_moreno is not None:
            J_moreno.add_global_phase(phase)
        if J_baiheng is not None:
            J_baiheng.add_global_phase(phase)
        if J_amaya is not None:
            J_amaya.add_global_phase(phase)

    # Make the optimizations
    results = {}
    if method == 'all' or method == 'hoyo_ext':
        if mf_amplitude:
            title = 'Hoyo ext. amplitude modulation '
            string = 'Best angles for amplitude modulation with method Hoyo ext.:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["amp_hoyo_ext"] = Get_Best_Optimization(
                J_hoyo_ext,
                mf=mf_amplitude,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)
        if mf_phase:
            title = 'Hoyo ext. phase modulation '
            string = 'Best angles for phase modulation with method Hoyo ext.:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["phase_hoyo_ext"] = Get_Best_Optimization(
                J_hoyo_ext,
                mf=mf_phase,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)

    if method == 'all' or method == 'hoyo':
        if mf_amplitude:
            title = 'Hoyo amplitude modulation '
            string = 'Best angles for amplitude modulation with method Hoyo:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["amp_hoyo"] = Get_Best_Optimization(
                J_hoyo,
                mf=mf_amplitude,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)
        if mf_phase:
            title = 'Hoyo phase modulation '
            string = 'Best angles for phase modulation with method Hoyo:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["phase_hoyo"] = Get_Best_Optimization(
                J_hoyo,
                mf=mf_phase,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)

    if method == 'all' or method == 'moreno':
        if mf_amplitude:
            title = 'Moreno amplitude modulation '
            string = 'Best angles for amplitude modulation with method Moreno:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["amp_moreno"] = Get_Best_Optimization(
                J_moreno,
                mf=mf_amplitude,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)
        if mf_phase:
            title = 'Moreno phase modulation '
            string = 'Best angles for phase modulation with method Moreno:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["phase_moreno"] = Get_Best_Optimization(
                J_moreno,
                mf=mf_phase,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)

    if method == 'all' or method == 'baiheng':
        if mf_amplitude:
            title = 'Baiheng amplitude modulation '
            string = 'Best angles for amplitude modulation with method Baiheng:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["amp_baiheng"] = Get_Best_Optimization(
                J_baiheng,
                mf=mf_amplitude,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)
        if mf_phase:
            title = 'Baiheng phase modulation '
            string = 'Best angles for phase modulation with method Baiheng:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["phase_baiheng"] = Get_Best_Optimization(
                J_baiheng,
                mf=mf_phase,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)

    if method == 'all' or method == 'amaya':
        if mf_amplitude:
            title = 'Amaya amplitude modulation '
            string = 'Best angles for amplitude modulation with method Amaya:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["amp_amaya"] = Get_Best_Optimization(
                J_amaya,
                mf=mf_amplitude,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)
        if mf_phase:
            title = 'Amaya phase modulation '
            string = 'Best angles for phase modulation with method Amaya:\n - P1: {:.0f} º.\n - Q1: {:.0f} º.\n - Q2: {:.0f} º.\n - P2: {:.0f} º.\n'
            results["phase_amaya"] = Get_Best_Optimization(
                J_amaya,
                mf=mf_phase,
                x0=None,
                mirrors=mirrors,
                Noptimizations=Noptimizations,
                verbose=verbose,
                draw=draw,
                title=title,
                string=string)

    # Save result
    if filename:
        np.savez(filename, results=results)

    # Return to original folder
    if path:
        os.chdir(old_path)

    return results


def Get_Best_Optimization(Jslm,
                          mf,
                          x0=None,
                          mirrors=def_mirrors,
                          Noptimizations=10,
                          verbose=False,
                          draw=False,
                          title='',
                          string='{} {} {} {}'):
    """Function that performs several optimizations and picks the best one.

    Args:
        Jslm (Jones_matrix): Jones matrix of the SLM.
        mf (function): Merit function.
        x0 (np.ndarray or None): Initial guess for optimization. If None, calculates x0 randomly. Default: None.
        mirrors ((bool, bool)): List or tuple of two elements which states if there is a mirror between the PSG and the SLM and between the SLM and the PSA respectively. Default: CONF_SLM1['mirrors'].
        Noptimizations (int): Number of optimizations that will be performed for each case. The best among them will be selected. Default: 10.
        verbose (bool): If True, prints the determined angles. Default: True.
        draw (bool): If True, plots the measurement. Default: True.
        title (str): Title of figure if Draw is True. Default: ''.
        string (str): Print string if verbose is True. Must contain four {}. Default: '{} {} {} {}'.

    Returns:
        result (dict): Dictionary with the result:
            amplitude (np.ndarray): Amplitude distribution.
            phase (np.ndarray): Phase distribution.
            angles (1x4 np.ndarray): Calculated angles for P1, Q1, Q2 and P2.
            cost (float): Cost obtained.
            seed (1x4 np.ndarray): Initial seed used to calculate the angles.
            N (int): Number of optimizations performed to obtain the result.
    """
    # Prepare
    best_cost = 1e16
    best_sol = None
    best_x0 = None
    if x0 is not None:
        Noptimizations = 1

    # Loop of optimizations
    for ind in range(Noptimizations):
        sol, cost, x0_result = Perform_Single_Optimization(Jslm, mf, x0)
        if cost < best_cost:
            best_cost = cost
            best_sol = sol
            best_x0 = x0_result

    # Result
    amp, phase = Calculate_Transmission(best_sol, Jslm)
    result = {}
    result["amplitude"] = amp
    result["phase"] = phase
    result["cost"] = best_cost
    result["seed"] = best_x0
    result["N"] = Noptimizations

    # Transform if mirrors are present
    PSG_az, PSG_el = PSG_angles_2_states(best_sol[2], best_sol[3])
    PSA_az, PSA_el = PSA_angles_2_states(best_sol[0], best_sol[1])
    if mirrors[0]:
        a_P1, a_Q1 = PSG_states_2_angles(180 * degrees - PSG_az, -PSG_el)
    else:
        a_P1, a_Q1 = best_sol[:2]
    if mirrors[1]:
        a_P2, a_Q2 = PSA_states_2_angles(180 * degrees - PSA_az, -PSA_el)
    else:
        a_Q2, a_P2 = best_sol[:2]
    best_sol = np.array([a_P1, a_Q1, a_Q2, a_P2]) + angles_0

    result["angles"] = best_sol
    # Print
    if verbose:
        print_sol = best_sol / degrees
        print('Best cost in {} optimizations is {:.3f}'.format(
            Noptimizations, best_cost))
        print(string.format(print_sol[0], print_sol[1], print_sol[2],
                            print_sol[3]))

    # Draw
    if draw:
        plt.figure(figsize=(16, 4))
        plt.subplot(1, 2, 1)
        plt.plot(amp)
        plt.title(title + 'Amplitude')
        plt.subplot(1, 2, 2)
        plt.plot(phase / degrees)
        plt.title(title + 'Phase (deg)')

    return result


def Perform_Single_Optimization(Jslm, mf, x0=None, tol=1e-11):
    """Function that performs a single optimization.

    Args:
        Jslm (Jones_matrix): Jones matrix of the SLM.
        mf (function): Merit function.
        x0 (np.ndarray or None): Initial guess for optimization. If None, calculates x0 randomly. Default: None.
        tol (float): Tolerance for least_squares. Default: 1e-11.

    Returns:
        x (np.ndarray): Solution.
        cost (float): Final cost.
    """
    # Preparations
    bounds = (np.zeros(4), np.ones(4) * 180 * degrees)
    if x0 is None:
        x0 = np.random.rand(4) * 180 * degrees

    # Optimization
    result = least_squares(
        fun=mf,
        x0=x0,
        bounds=bounds,
        args=[Jslm],
        ftol=tol,
        xtol=tol,
        gtol=tol)

    return result.x, result.cost, x0


def Calculate_Transmission(angles, Jslm, states=False, E0=None,method_transmission='ideal',filename='Polarimeter.npz'):
    """Function that calculates the transmission (amplitude and phase) of the PSG + SLM + PSA system.

    Args:
        angles (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm (Jones_matrix): Jones matrix of the SLM.
        states (bool): If True, angles is [PSG_az, PSG_el, PSA_az, PSA_el] instead of rotation angles. Default: False.
        E0 (Jones_vector or None). Source Jones vector. If None, circular polarization is used.
        method_transmission (string): Method 'ideal' or 'real' if you want to calculte the transmission using the ideal polarization plates or the real ones. Default: 'ideal'.
        filename (string): name of the filename with the mueller matrix of the polarization plates. Default. 'Polarimeter.npz'.

    Returns:
        amplitude (np.ndarray): Solution.
        phase (np.ndarray): Solution.
    """

    # Initial angles
    if states:
        a_P1, a_Q1 = PSG_states_2_angles(
            azimuth=angles[0], ellipticity=angles[1])
        a_P2, a_Q2 = PSA_states_2_angles(
            azimuth=angles[2], ellipticity=angles[3])
        angles = [a_P1, a_Q1, a_Q2, a_P2]

    if method_transmission == 'ideal':
        # Create objects
        if E0 is None:
            E0 = Stokes().circular_light(intensity=2)
        if angles[0] < 0:
            P1 = Stokes().vacuum()
        else:
            P1 = Mueller().diattenuator_perfect(azimuth=angles[0])
        if angles[1] < 0:
            Q1 = Mueller().vacuum()
        else:
            Q1 = Mueller().quarter_waveplate(azimuth=angles[1])
        if angles[2] < 0:
            Q2 = Mueller().vacuum()
        else:
            Q2 = Mueller().quarter_waveplate(azimuth=angles[2])
        if angles[3] < 0:
            P2 = Mueller().vacuum()
        else:
            P2 = Mueller().diattenuator_perfect(azimuth=angles[3])
        #print(P1, Q1, Q2, P2, sep='\n')

    elif method_transmission == 'real':
        
        cal_dict = np.load(filename)
        
        E0, P1, Q1, Q2, P2 = Msystem_From_Dict(cal_dict,initial_angles=False)
        
        P1.rotate(angles[0])
        Q1.rotate(angles[1])
        Q2.rotate(angles[2])
        P2.rotate(angles[3])

    # Calculate final state
    PSA = P2 * Q2
    PSG = Q1 * P1 * E0
    #if Jslm._type.lower() == "mueller":
    #    PSG = Stokes().from_Jones(PSG)
    #    PSA = Mueller().from_Jones(PSA)
    Efinal = PSA * (Jslm * PSG)

    # Extract phase and amplitude
    phase = Efinal.parameters.global_phase(out_number=False)
    phase -= phase[0]
    phase = phase % (2 * np.pi)
    amp = Efinal.parameters.intensity()
    #print((Efinal.M[0]))
    #amp = np.abs(amp)**2
    # phase = np.unwrap(phase)

    # plt.figure(figsize=(8, 4))
    # plt.subplot(1, 2, 1)
    # plt.plot(phase)
    # plt.subplot(1, 2, 2)
    # plt.plot(amp)

    # Ex, Ey = Efinal.parameters.components()
    # p1 = np.angle(Ex)
    # p2 = np.angle(Ey)
    # plt.figure(figsize=(8, 4))
    # plt.subplot(1, 2, 1)
    # plt.plot(p1)
    # plt.subplot(1, 2, 2)
    # plt.plot(p2)

    return amp, phase


def Merit_Function_Amplitude(angles, Jslm):
    """Merit function for amplitude modulation.

    Args:
        angles (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm (Jones_matrix): Jones matrix of the SLM.

    Returns:
        error (np.ndarray): Error cost. Due to how least_squares works, must have at least than 4 elements.
    """
    # Calculate the amplitude and phase curves
    amp, phase = Calculate_Transmission(angles, Jslm)

    # Calculate the cost
    dif_phase = np.max(phase) - np.min(phase)  # The most important parameter
    zero_level = np.min(amp)  # The lower the better
    dif_amp = np.max(amp) - zero_level  # The bigger the better
    lineality = np.std(np.diff(amp))  # The lower the better
    const = np.mean(np.abs(np.diff(phase)))  # The lower the better

    error = np.array([
        15 * dif_phase, zero_level, 0.2 / dif_amp, 5 * lineality, 0.2 * const
    ])
    return error


def Error_Function_Amplitude(angles, Jslm):
    """Error function for amplitude modulation.

    Args:
        angles (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm (Jones_matrix): Jones matrix of the SLM.

    Returns:
        error (np.ndarray): Error.
    """
    # Calculate the amplitude and phase curves
    amp, phase = Calculate_Transmission(angles, Jslm)

    # Calculate the target functions
    phase_t = np.ones_like(phase) * np.mean(phase)
    amp_t1 = np.linspace(0, np.max(amp), phase.size)
    amp_t2 = np.max(amp) - amp_t1

    # Calculate errors
    phase_error = phase_t - phase
    amp_error_1 = (amp_t1 - amp) / np.max(amp)**2
    amp_error_2 = (amp_t2 - amp) / np.max(amp)**2
    if np.linalg.norm(amp_error_1) > np.linalg.norm(amp_error_2):
        error = np.concatenate((amp_error_2, phase_error))
    else:
        error = np.concatenate((amp_error_1, phase_error))
    return error


def Error_Function_Phase(angles, Jslm):
    """Error function for phase modulation.

    Args:
        angles (np.ndarray): Rotation angles of P1, Q1, Q2 and P2.
        Jslm (Jones_matrix): Jones matrix of the SLM.

    Returns:
        error (np.ndarray): Error.
    """
    # Calculate the amplitude and phase curves
    amp, phase = Calculate_Transmission(angles, Jslm)

    # Calculate the target functions
    amp_t = np.ones_like(amp) * np.mean(amp)
    phase_t1 = np.linspace(np.min(phase), np.max(phase), phase.size)
    phase_t2 = np.max(phase) - phase_t1

    # Calculate errors
    amp_error = (amp_t - amp) / np.mean(amp)**2
    phase_error_1 = (phase_t1 - phase) / (np.max(phase) - np.min(phase))**2
    phase_error_2 = (phase_t2 - phase) / (np.max(phase) - np.min(phase))**2
    if np.linalg.norm(phase_error_1) > np.linalg.norm(phase_error_2):
        error = np.concatenate((amp_error, phase_error_2))
    else:
        error = np.concatenate((amp_error, phase_error_1))
    return error


def Amplitude_Transmission_Measurement(slm,
                                       daca,
                                       filename=None,
                                       levels=None,
                                       Naverage=1,
                                       wait_time=1e-2,
                                       draw=True,
                                       return_vars=False):
    """Function to perform one of the measurements to calculate the Jones matrix of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        daca (DACA): Object of the data acquisition card.
        filename (str or None): Name of the files to save. If None, the filename is automaticly created. Default: None.
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        draw (bool): If True, plots the measurement. Default: True.
        return_vars (bool): If True, intensity and error are returned. Default: False.

    Returns (only if return_vars is True):
        power (np.ndarray): Array of intensity (Voltage).
        error (np.ndarray): Array of deviation for each level.
    """
    # Prealocate
    gray_level = Scalar_mask_XY(x=slm.x, y=slm.y, wavelength=slm.wavelength)

    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)

    # Measurement
    power = np.zeros(levels.size)
    error = np.zeros(levels.size)
    for indV, value in enumerate(levels):
        # Create the mask
        gray_level.gray_scale(
            num_levels=1, levelMin=np.sqrt(value), levelMax=np.sqrt(value))
        slm.Send_Image(image=gray_level, kind='intensity', norm=1)

        # Average intensity
        measures = np.zeros(Naverage)
        for indMM in range(Naverage):
            time.sleep(wait_time)
            measures[indMM] = daca.Get_Signal()

        # Calculate mean value and error
        power[indV] = np.mean(measures)
        error[indV] = np.std(measures)

    # Save file
    now = datetime.now()
    string_time = now.strftime("Date: %Y_%m_%d_%H_%M_%S.")
    if filename is None:
        filename = 'Amplitude_Trans_{}.npz'.format(string_time)
    np.savez(
        filename,
        power=power,
        error=error,
        Naverage=Naverage,
        wait_time=wait_time,
        levels=levels,
        time=string_time)

    # Draw
    if draw:
        plt.figure()
        plt.plot(levels * 255, power)
        plt.xlabel('Levels')
        plt.ylabel('Intensity (a.u.)')
        if Naverage > 1:
            plt.errorbar(
                levels[::5] * 255, power[::5], yerr=error[::5], fmt='b+')
        plt.show()

    if return_vars:
        return power, error


def Verify_SLM_Configuration(slm,
                             obj_measure,
                             kind='amplitude',
                             filename=None,
                             file_theory=None,
                             model_theory='amp_hoyo_ext',
                             kind_theory='amplitude',
                             levels=None,
                             Naverage=1,
                             wait_time=1e-2,
                             draw=True,
                             return_vars=False,
                             skip_measurement=False):
    """Function to to verify a configuration of the SLM of the SLM.

    Args:
        slm (SLM): Object of the SLM.
        obj_measure (DACA or Camera): Object for measurement: data acquisition card for amplitude and camera for phase.
        kind (str): Type of verification, amplitude or phase. Default: 'amplitude'.
        filename (str or None): Name of the files to save. If None, no file is created. Default: None.
        file_theory (str or None): Name of the file where the theoretical curves are stored. If None, no comparison is performed. Default: None.
        model_theory (str or None): Model used to calculate the angles between hoyo_ext, hoyo, moreno, baiheng and amaya. Default: hoyo_ext.
        levels (iterable or None): Levels of the SLM to measure. If None, loads the full dynamic range of the SLM. Default: None.
        Naverage (int): Number of measurements for each gray level. Default: 1.
        wait_time (float): Seconds to wait between measurements (in seconds). Default: 10 ms.
        draw (bool): If True, plots the measurement. Default: True.
        return_vars (bool): If True, intensity and error are returned. Default: False.
        skip_measurement (bool): If True, measurement is not performed (only in phase calibration). Default: False.

    Returns (only if return_vars is True):
        result (np.ndarray): Array of intensity (amplitude) or phase (phase) for each gray level.
        error (np.ndarray): Array of deviation for each gray level.
        theory (np.ndarray): Array of the theoretical values.
    """
    # Create levels
    if levels is None:
        levels = np.linspace(0, 1, 2**slm.dynamic_range)
    else:
        levels = np.array(levels)
    # levels = np.array([0, 50, 100, 150, 200, 255])

    # Load theory data
    theory = None
    if file_theory is not None:
        try:
            loaded = np.load(file_theory, allow_pickle=True)
            data = loaded["results"]
            data = np.take(data, 0)
            theory = data[model_theory]
            theory = theory[kind]
            levels_theory = np.linspace(0, 1, theory.size)
        except:
            print('Unable to load theory data')

    # Perform the measurement
    if kind == 'amplitude':
        result, error = Amplitude_Transmission_Measurement(
            slm,
            obj_measure,
            levels=levels,
            Naverage=Naverage,
            wait_time=wait_time,
            return_vars=True,
            draw=False)
    else:
        # Make the measurement
        if not skip_measurement:
            Phase_Calibration_Measurement(
                slm=slm,
                cam=obj_measure,
                levels=levels,
                filename="Verification_{}.npz",
                Naverage=Naverage,
                wait_time=wait_time,
                return_vars=False)
        # Analyze images
        files = []
        for ind in range(2**slm.dynamic_range):
            files.append("Verification_{}.npz".format(ind))
        # window = (np.linspace(40, 1940, 20, dtype=int), 'left', 15, 'full')
        window = (np.linspace(40, 1940, 20, dtype=int), 'left', 30, 'full')
        result, error, fit, period = Phase_Calibration_Analysis(
            levels=levels,
            files=files,
            window=window,
            draw=True,
            peak_distance=100,
            filename=None)

    # Normalize amplitude data
    if (kind == 'amplitude') and (theory is not None):
        result = result * np.max(theory) / np.max(result)

    # Print
    if draw:
        plt.figure()
        plt.plot(levels, result)
        if Naverage > 1:
            plt.errorbar(levels[::5], result[::5], yerr=error[::5], fmt='b+')
        if theory is not None:
            plt.plot(levels_theory, theory)
        plt.xlabel('Levels')
        if kind == 'amplitude':
            plt.ylabel('Intensity (a.u.)')
        else:
            plt.ylabel('Phase (rad)')
        plt.show()

    # Save file
    if filename:
        np.savez(
            filename,
            result=result,
            error=error,
            theory=theory,
            kind=kind,
            model_theory=model_theory,
            Naverage=Naverage,
            wait_time=wait_time,
            levels=levels,
            time=string_time)

    # Return
    if return_vars:
        return result, error, theory


########################
# UTILS
########################


def Ret_Parameters_To_Jones_Parameters(x, y, z, w):
    """Function to calculate the Jones parameters from the x, y, z and w retarder parameters.

    Args:
        x, y, z, w (np.ndarray): Retarder parameters. All arrays must be of the same shape.

    Returns:
        J0, J1, J2, J3, det, d1, d2, d3 (np.ndarray): Absolute value of Jones parameters (Ji) and their complex phase (di). Also the absolute value of the determinant (det) is given.
    """
    J0c = x - 1j * y
    J1c = z - 1j * w
    J2c = -z - 1j * w
    J3c = x + 1j * y
    J0 = np.abs(J0c)
    J1 = np.abs(J1c)
    J2 = np.abs(J2c)
    J3 = np.abs(J3c)
    d0 = np.angle(J0c)
    d1 = (np.angle(J1c) - d0) % (2 * np.pi)
    d2 = (np.angle(J2c) - d0) % (2 * np.pi)
    d3 = (np.angle(J3c) - d0) % (2 * np.pi)

    determinant = np.abs(J0 * J3 * np.exp(1j * d3) -
                         J1 * J2 * np.exp(1j * (d1 + d2)))
    return [J0, J1, J2, J3, determinant, d1, d2, d3]


def Clear_Angles():
    """Function to clear the performed measurements."""
    list_angles.clear()
    list_ind.clear()
    list_ort.clear()


def Components_To_Matrix(comp, type="jones"):
    """Transforms the list of components into the py_pol object of the Jones matrix.

    Args:
        comp(list): List containing J0, J1, J2, J3, det, d1, d2 and d3.
        type (str): "jones" or "mueller". Default: "jones".

    Returns:
        J (Jones_matrix): Jones matrix.
    """
    if type == "jones":
        J0, J1, J2, J3, det, d1, d2, d3 = comp
        pypol_comp = [
            J0, J1 * np.exp(1j * d1), J2 * np.exp(1j * d2), J3 * np.exp(1j * d3)
        ]
        J = Jones_matrix().from_components(pypol_comp)
    else:
        J = Mueller().from_components(comp)
    return J


def Analyze_Matrix(comp, type="jones"):
    """Decomposes the Jones matrix into a retarder and polarizer and calculates their parameters.

    Args:
        comp (list): List containing J0, J1, J2, J3, det, d1, d2 and d3.
        type (str): "jones" or "mueller". Default: "jones".

    Returns:
        param (dict): Dictionary of parameters (given by py_pol).
    """
    if np.any(np.isnan(np.array(comp))):
        param = {
        "R": comp[0] + np.nan,
        "azimuth R": comp[0] + np.nan,
        "ellipticity R": comp[0] + np.nan,
        "p1": comp[0] + np.nan,
        "p2": comp[0] + np.nan,
        "azimuth D": comp[0] + np.nan,
        "ellipticity D": comp[0] + np.nan,
        }

    else:
        J = Components_To_Matrix(comp, type)
        if type == 'jones':
            _, _, param = J.analysis.decompose_pure(all_info=True)

        else:
            _, _, _,param = J.analysis.decompose_polar(give_all=True)
            param["depol"] = J.parameters.depolarization_index()

    return param

def Calculate_W(angles, system=None):
    """Calculate the W matrix for a set of angles.

    Args:
        angles (np.ndarray): Array of angles.
        system (list): List of PSG, PSA and illumination Mueller/Stokes objects. Default: None.

    Returns:
        np.ndarray: W matrix
    """
    # Default values  
    angles = np.reshape(angles, (4,16))
    S, P1, R1, R2, P2 = system
    """""
    if angles:
        S, P1, R1, R2, P2 = system
    else:
        S = Stokes().circular_light()
        P1 = Mueller().diattenuator_perfect()
        R1 = Mueller().quarter_waveplate()
        R2 = Mueller().quarter_waveplate()
        P2 = Mueller().diattenuator_perfect()
    """""

    # Calculate W
    Mp1_rot = P1.rotate(angle=angles[0,:], keep=True)
    Mr1_rot = R1.rotate(angle=angles[1,:], keep=True)
    Mr2_rot = R2.rotate(angle=angles[2,:], keep=True)
    Mp2_rot = P2.rotate(angle=angles[3,:], keep=True)

    PSG = Mr1_rot * (Mp1_rot * S)
    PSA = Mp2_rot * Mr2_rot
    a = PSA.M[0,:,:]
    a = a.T[:, :, np.newaxis]
    g = PSG.M.T[:, np.newaxis, :]
    w = a @ g # Similar to matmul
    w = w.reshape((w.shape[0], 16))
    
    return w
