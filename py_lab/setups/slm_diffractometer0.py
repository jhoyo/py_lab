"""This file contains functions for diffractometry.

It is used in the SLM-motor-laser-camera set-up"""


import os
import numpy as np
import imageio
from pprint import pprint
import screeninfo
import serial.tools.list_ports
import serial
import cv2
import time
from diffractio.scalar_sources_XY import Scalar_source_XY
from diffractio.utils_drawing import draw_several_fields
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_fields_XYZ import Scalar_field_XYZ
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib
from diffractio import degrees, mm, plt, sp, um, np
from diffractio.scalar_masks_XY import Scalar_mask_XY

from py_lab.camera import Camera
from py_lab.motor_linear import Motor
from py_lab.slm import SLM
from py_lab.utils import List_COM_Ports


# Camara
ms = 1.
seconds = 1000 * ms

# Motores
s = 1


def images_z(devices, mask, positions, z0, filename, remove_background=True, has_diffractio=False,  has_video=True, has_images=False, has_npz=False, show_images=False, fps=1):
    """Function to perform the measurements to calculate the global phase of the SLM.
    - Generates a gif video.
    - Generates npz files with the images.
    - generates functions

    Args:
        devices=(slm, cam,motor)
            slm (SLM): SLM.
            cam (Camera): Object of the camera.
            motor (Motor): Object of the motor
        mask (slm): mask to execeute
        positions (np.array): positions in mm from the z0 image
        z0 (float): motor position of the image after 4f.
        filename (str): Template for filenames.
        remove_background (bool): If True, removes_background
        has_diffractio (bool): If True returns scalar_fields_XYZ structure with image
        has_video (bool): If True, makes a gif file
        has_images (bool): If True, keeps png images for the gif file.
        has_npz (bool): if True, generates npz files of images

    Returns (only if return_vars is True):
        images (list): List of arrays of the images.
    """
    slm, cam, motor = devices

    mask_null = Scalar_mask_XY(x=mask.x, y=mask.y, wavelength=mask.wavelength)
    mask_null.u = np.zeros_like(mask.u, dtype=float)

    if has_diffractio:
        u_field = Scalar_field_XYZ(
            x=mask.x, y=mask.y, z=positions, wavelength=mask.wavelength)

    names = []

    for i, z in enumerate(positions):
        motor.Move_Absolute(pos=z0 - z, units='mm',
                            verbose=False, move_time=None)

        if remove_background:
            slm.Send_Image(mask_null, norm=1, kind='intensity')
            time.sleep(0.3)
            imageb = cam.Get_Image()
            slm.Send_Image(mask, norm=1, kind='intensity')
            time.sleep(0.3)
            imagem = cam.Get_Image()
            image = imagem - imageb
            image[image < 0] = 0
        else:
            image = cam.Get_Image()

        if has_diffractio:
            u_field.u[:, :, i] = np.sqrt(image.transpose())

        if np.sign(z) >= 0:
            filename_npz = '{}_{:.1f}.npz'.format(filename, abs(z))
            names.append(filename_npz)
            np.savez(filename_npz)
            plt.imshow(image, origin='lower')
            plt.axis('off')
            plt.title('{} z = {:.1f} mm'.format(filename, z))
            plt.savefig('{}_{:.1f}.png'.format(filename, abs(z)))
            if show_images:
                plt.show()

        print('{:.1f}'.format(z), end='\r')

    if has_video == True:
        gif_path = "{}.gif".format(filename)
        with imageio.get_writer(gif_path, mode='I', fps=fps) as writer:
            for i, z in enumerate(positions):
                if np.sign(z) >= 0:
                    writer.append_data(imageio.imread(
                        '{}_{:.1f}.png'.format(filename, abs(z))))

    if has_images is False:
        for i, z in enumerate(positions):
            if np.sign(z) >= 0:
                os.remove('{}_{:.1f}.png'.format(filename, abs(z)))

    if has_npz is False:
        for i, z in enumerate(positions):
            if np.sign(z) >= 0:
                os.remove('{}_{:.1f}.npz'.format(filename, abs(z)))

    print('¡Finished!')

    if has_diffractio:
        return names, u_field
    else:
        return names, None
