import numpy as np
import matplotlib.pyplot as plt
from py_pol.mueller import Mueller, Stokes, degrees
from py_lab.config import CONF_POLARIMETER,CONF_POLARIMETER_633


def Calculate_Mueller(I, angles, system, system_aux=None, filter=True):
    """Function to calculate the Mueller matrix from the intensity measurements.

    Args:
        I (np.array): First dimension is the corresponding to different measurements.
        angles (Nx4 np.array): Angles array.
        system (list) List with the Stokes and Mueller objects for illumination, P1, R1, R2 and P2.
        system_aux (list): List with Mueller objects for BS (ref), BS (trans), MO (optional). Default: None.
        filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.

    Returns:
        result (Mueller): Result.
    """
    # Get shapes and reshape for usefullness
    Ndim = I.ndim
    shape = None
    if Ndim == 1:
        I = I[:, np.newaxis]
        N = I.size
        Np = 1
    elif Ndim > 1:
        N, shape = I.shape[0], I.shape[1:]
        Np = np.prod(shape)
        I = I.reshape(1, N, Np)

    # Calculate Mueller matrix
    S, P1, R1, R2, P2 = system

    Mp1_rot = P1.rotate(angle=angles[:, 0], keep=True)
    Mr1_rot = R1.rotate(angle=angles[:, 1], keep=True)
    Mr2_rot = R2.rotate(angle=angles[:, 2], keep=True)
    Mp2_rot = P2.rotate(angle=angles[:, 3], keep=True)

    PSG = Mr1_rot * (Mp1_rot * S)
    PSA = Mp2_rot * Mr2_rot
    a = PSA.M[0,:,:]
    a = a.T[:, :, np.newaxis]
    g = PSG.M.T[:, np.newaxis, :]
    w = a @ g # Similar to matmul
    w = w.reshape((1,w.shape[0], 16))
    wt = w.T        ## Problema .T no deja usar .axis
    wt = np.transpose(wt,axes=[2,0,1])
    w = wt @ w
    wi = np.linalg.inv(w) @ wt
    #wi = np.linalg.inv(w) 
    m = np.squeeze(wi @ I)
    M = Mueller().from_components(m)
    if shape:
        M.shape = shape

    # Filter errors
    if filter:
        M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER_633["filter_tol"])

    # TODO: Reflection
    if system_aux:
        pass

    return M



def Msystem_From_Dict(cal_dict, initial_angles=True):
    """Function to calculate the system Mueller matrices from the calibration dictionary.

    Args:
        cal_dict (dict): Calibration dictionary.

    Returns:
        Ms (list): List with Sillum, Mp1, Mr1, Mr2 and Mp2 py_pol objects at azimuth 0.
    """

    # Create objects
    S = Stokes("Sillum").general_azimuth_ellipticity(
        intensity=cal_dict["S0"], #0.55717404
        azimuth=cal_dict["illum_az"], #140.91*degrees,
        ellipticity=cal_dict["illum_el"],#-34.13*degrees,
        degree_pol=cal_dict["illum_pol_degree"]) #0.999)
    #print(cal_dict["S0"],cal_dict["illum_az"],cal_dict["illum_el"],cal_dict["illum_pol_degree"])

    az = -cal_dict["P1_az"] if initial_angles else 0#0.02799216
    Mp1 = Mueller("Mp1").diattenuator_linear(
        p1= cal_dict["P1_p1"], 
        p2= cal_dict["P1_p2"],
        azimuth=az)
    az = -cal_dict["R1_az"] if initial_angles else 0#-0.01229507
    Mr1 = Mueller("Mr1").diattenuator_retarder_linear(
        p1=cal_dict["R1_p1"],
        p2=cal_dict["R1_p2"],
        R=cal_dict["R1_Ret"], 
        azimuth=az)
    az = -cal_dict["R2_az"] if initial_angles else  0#-0.02744035
    Mr2 = Mueller("Mr2").diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"],
        p2=cal_dict["R2_p2"],
        R=cal_dict["R2_Ret"], 
        azimuth=az)
    az = -cal_dict["P2_az"] if initial_angles else 0#-0.02693901
    Mp2 = Mueller("Mp2").diattenuator_linear(
        p1=cal_dict["P2_p1"], 
        p2=cal_dict["P2_p2"],
        azimuth=az) 

    Ms = [S, Mp1, Mr1, Mr2, Mp2]
    return Ms

    
## DEPERCATED

def Calculate_Mueller_Matrix_0D(I, angles, Ms, tol=1e-9):
    """Function to calculate the Mueller matrix of a mssive sample without spatial resolution.

    Args:
        I (np.ndarray): Nx1 array of measured intensities.
        angles (np.ndarray): Nx4 array of angles.
        ref (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
        Ms (list): List with Sillum, Mp1, Mr1, Mr2 and Mp2 py_pol objects at azimuth 0.
        tol (float): Tolerance for error filtering. Default: 1e-9.

    Returns:
        M (Mueller): Mueller matrix of sample.
    """
    # Calculate rotated matrices
    S = Ms[0]
    Mp1 = Ms[1].rotate(angles[:, 0], keep=True)
    Mr1 = Ms[2].rotate(angles[:, 1], keep=True)
    Mr2 = Ms[3].rotate(angles[:, 2], keep=True)
    Mp2 = Ms[4].rotate(angles[:, 3], keep=True)

    # Calculate products
    PSG = (Mr1 * (Mp1 * S))
    PSA = Mp2 * Mr2

    # Calculate W matrix
    N = I.size
    PSG = PSG.M.T.reshape((N, 1, 4))
    PSA = PSA.M[0, :, :].T.reshape((N, 4, 1))
    # W = np.matmul(PSG, PSA).reshape((N, 16))
    W = (PSA @ PSG).reshape((N, 16))

    # Invert
    I = I.reshape((N, 1))
    Wt = W.T
    # Wi = np.matmul(np.linalg.inv(np.matmul(Wt, W)), Wt)
    # M = np.matmul(Wi, I)
    Wi = np.linalg.inv((Wt @ W)) @ Wt
    M = Wi @ I
    # Object
    M = Mueller().from_components(M[:, 0])
    # M.analysis.filter_physical_conditions(tol=tol)

    return M

def Draw_Mueller_Matrix(M=[], clim = "global", norm = True, text = False): 

    """Function to draw Matrix Mueller in 2D. 

    Args:
        M (np.darray): 16xMxN array of measured Mueller matrix
        clim (str): Clim between -1 and 1 ("global"), or between min and max ("individual"). Default: "global"
        norm (bool): if True, this function divides all elements from Mueller matrix by M_00_max 
        text (bool): Write in the plot the min and max value

    Returns:
    """
    # Create figure:
    fig, plots = plt.subplots(4,4, figsize = (12,8))

    for i in range(16): 
        plt.subplot(4,4,i+1)

        # Mueller matrix normalization
        if norm == True: 
            im = plt.imshow(M[i,:,:]/M[0,:,:].max(), cmap="seismic")
        else: 
            im = plt.imshow(M[i,:,:], cmap="seismic")

        plt.xticks([])
        plt.yticks([])
        plt.title("M_{}{}".format((i//4),(i%4)))

        # Clim for Mueller elements: 
        if clim.lower() == "global": 
            plt.clim(-1,1)
            x = len(M[i,:,0])
            y = len(M[i,0,:])
            if text == True & norm == True: 
                plt.text(0.8*x,0.65*y,"Min: {}\nMax: {}\nMean abs: {}\nMean: {}".format( np.round(M[i,:,:].min()/M[0,:,:].max(),2) , np.round(M[i,:,:].max()/M[0,:,:].max(),2) , np.round(np.abs(M[i,:,:]).mean()/M[0,:,:].max(),2) , np.round(M[i,:,:].mean()/M[0,:,:].max(),2)), fontsize = 7)
            elif text == True: 
                plt.text(0.8*x,0.65*y,"Min: {}\nMax: {}".format(np.round(M[i,:,:].min(),2) , np.round(M[i,:,:].max(),2) , np.round(np.abs(M[i,:,:]).mean(),2) , np.round(M[i,:,:].mean(),2)), fontsize = 7)
        elif clim.lower() == "individual":
            # plt.clim(M[i,:,:].min(),M[i,:,:].max())
            plt.colorbar()
        
        
    plt.tight_layout()
    if clim.lower() == "global":
        plt.colorbar(im,ax=plots.ravel().tolist())


    return 