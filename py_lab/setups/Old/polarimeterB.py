import numpy as np
import os
import datetime

from py_pol.mueller import Mueller, Stokes, degrees

from py_lab.motor import Motor_Multiple
from py_lab.daca import DACA
from py_lab.camera import Camera
from py_lab.utils import sort_positions
from py_lab.config import degrees, CONF_INTEL, CONF_T7, CONF_POLARIMETER_633
from py_lab.setups.polarimeter_utils import Calculate_Mueller, Calculate_Mueller_Matrix_0D


class Polarimeter(object):
    """New class for polarimeter. It will include control of the rotary motors of PSG and PSA elements, the data acquisition card for measuring photodiode signal (bulk configuation) and a camera (spatial resolution configuration).

    Args:
        calibration (bool): Load default calibration. Default: True.
        use_daca (bool): If True, measurement signal is get through DACA (photodiode). If False, the camera is used instead. Default: True.

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        camera (Camera): Camera object.
        daca (DACA): Data acquisition card object.
    """

    def __init__(self, calibration=True, use_daca=True):

        self.motor = Motor_Multiple(name="InteliDrives", N=4)
        self.daca = DACA(name="T7")

        if use_daca is True:
            self.camera = None
        else:
           self.camera = Camera(name="ImagingSource2", M=1, wavelength=None)
        self.norm = 1
        self.I_Distribution = None

        # Load calibration info
        if calibration:
            self.Load_Calibration(filename=CONF_POLARIMETER_633['cal_file'], BS_ref=CONF_POLARIMETER_633['BS_ref'], BS_trans=CONF_POLARIMETER_633['BS_trans'], ref_mirror=CONF_POLARIMETER_633['ref_mirror'], folder=CONF_POLARIMETER_633["cal_folder"])

        # Just in case some default data must be stored
        else:
            self.angles_0 = np.zeros(4)
            self.cal_dict = {}
            self.S = None
            self.P1 = None
            self.R1 = None
            self.R2 = None
            self.P2 = None
            self.BS_ref_inv = None
            self.BS_ref = None
            self.BS_trans = None
            self.BS_trans_inv = None
            self.ref_mirror = None


    def Load_Calibration(self, filename=None, BS_ref=None, BS_trans=None, ref_mirror=None, folder=None):
        """Load some or all calibration files. If any of the filename parameters is None, that calibration will be ignored.

        Args:
            filename (str or None): Filename of the main calibration. Default: None.
            BS_ref (str or None): Filename of the beam splitter in reflection configuration. Default: None.
            BS_trans (str or None): Filename of the main calibration. Default: None.
            ref_mirror (str or None): Filename of the main calibration. Default: None.
            folder (str or None): Filename of the main calibration. Default: None.
        """

        # Go to calibration folder
        if folder:
            try:
                old_folder = os.getcwd()
                os.chdir(folder)
            except:
                print("Folder {} nonexistent".format(folder))

        # Main calibration
        if filename:
            try:
                data = np.load(filename)
                for key in data.keys():
                    print(key, data[key], data[key]/degrees)

                self.angles_0 = np.array([data["P1_az"], data["R1_az"], data["R2_az"], data["P2_az"]])
                self.S = Stokes().general_azimuth_ellipticity(azimuth=data["illum_az"], ellipticity=data["illum_el"], degree_pol=data["illum_pol_degree"])
                self.P1 = Mueller().diattenuator_retarder_linear(p1=data["P1_p1"], p2=data["P1_p2"], R=data["P1_R"], azimuth=-data["P1_az"])
                self.R1 = Mueller().diattenuator_retarder_linear(p1=data["R1_p1"], p2=data["R1_p2"], R=data["R1_R"], azimuth=-data["R1_az"])
                self.R2 = Mueller().diattenuator_retarder_linear(p1=data["R2_p1"], p2=data["R2_p2"], R=data["R2_R"], azimuth=-data["R2_az"])
                self.P2 = Mueller().diattenuator_linear(p1=data["P2_p1"], p2=data["P2_p2"], azimuth=-data["P2_az"])

                self.cal_dict = data

            except:
                print("File {} nonexistent".format(filename))

        # Reflection configuration
        if BS_ref:
            try:
                data = np.load(BS_ref)
                comp = data["Mcomp"]
                self.BS_ref = Mueller().from_components(comp)
                self.BS_ref_inv = self.BS_ref.inverse(keep=True)
            except:
                self.BS_ref_inv = None
                self.BS_ref = None
                print("File {} nonexistent".format(BS_ref))

        if BS_trans:
            try:
                data = np.load(BS_trans)
                comp = data["Mcomp"]
                self.BS_trans = Mueller().from_components(comp)
                self.BS_trans_inv = self.BS_trans.inverse(keep=True)
            except:
                self.BS_trans_inv = None
                self.BS_trans = None
                print("File {} nonexistent".format(BS_trans))

        if ref_mirror:
            try:
                data = np.load(ref_mirror)
                comp = data["Mcomp"]
                self.ref_mirror = Mueller().from_components(comp)
            except:
                print("File {} nonexistent".format(ref_mirror))

        if folder:
            try:
                os.chdir(old_folder)
            except:
                print("Unable to return to original folder")

        #return self.BS_ref_inv, self.BS_trans_inv


    def Open(self):
        self.motor.Open(port='COM3', axis=[0,1,2,3])
        #self.motor.Home()
        self.daca.Open(AIN=CONF_T7["AIN"], AIN_ref=CONF_T7["AIN_ref"])
        if self.camera is not None:
            self.camera.Open()
            ## Parámetros de la Cámara
            self.camera.Set_Property_Auto(name="Exposure", value=0)
            self.camera.Set_Property(name="Exposure", value=0.5)
            self.camera.Set_Property_Auto(name="Gain", value=0)
            self.camera.Set_Property(name="Framerate", value=3)
            self.camera.Set_Property_Auto(name="Resolution", value=0)
            self.camera.Set_Property(name="Resolution", value=[2560,1920])

        # Origin angles reference TODO: NOT WORKING
        # self.motor.Clear_Reference()
        # self.motor.Move_Absolute(self.angles_0, units="rad")
        # self.motor.Set_Reference()

    # def Open_Camera(self):
    #     self.camera.Open()

    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=self.angles_0, units="rad")




    def Close(self):
        """Close all devices."""
        self.motor.Close()
        if self.camera is not None:
            self.camera.Close()



    def Measure_Intensity(self,dim=0):
        """ Get the intensity when motors are at home.
            Args:
                dim(int): If dim = 0 measured with Photodiode, instead use the camera.
                default = 0
                ROI: If ROI True define the region of interest and just get that intensity
        """
        if dim == 0:
            I = self.daca.Get_Signal()
        else:
            I = np.zeros((self.camera.Get_Property(name="Resolution")))
            I = self.camera.Get_Image(is_background=False,rest_background=True,draw=True)
            I[I == 0] = 1 ## Avoid problem with division by 0
        self.I_Distribution = I



    def Measure_Mueller_Matrix(self, dim=0, I=None, angles="random", N=200, filter=True, reflection=False, is_ref=False, save_folder=None, filename=None, verbose=True, downscale=False):
        """Function to measure the Mueller matrix of a mssive sample without spatial resolution.

        If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.

        Args:
            dim (int): If dim is 0 measured with PHD, other wise with camera. Default:0
            I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
            angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'random'.
                "random": Random angles.
                'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
            N (int): Number of measurements used (when I is None). Default: 200.
            filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
            reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
            is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
            save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
            filename (str or None): If not None, the filename where the measurement is stored. Default: None.
            verbose (bool): If True, the information is printed after performing the measurement. Default: True.
            downscale (bool): If True, resize the image with an interpolation. Default: False

        Returns:
            result (Mueller): Measured Mueller matrix.
        """
        # Checks
        if isinstance(angles, np.ndarray) and angles.ndim != 2:
            raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
        elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
            raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
        if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
            raise ValueError("BS in reflection missing")
        elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
            raise ValueError("BS in transmission missing")
        elif reflection and is_ref and self.ref_mirror is None:
            raise ValueError("Reference mirror missing")

        if not is_ref and self.norm == 1:
            print("WARNING: Normalization not performed")

        # Make measurement
        if I is None:
            # Generate angles
            if angles.lower() == "random":
                angles = np.random.rand(N, 4) * 180*degrees
            elif angles.lower() == "linspace":
                Nx = np.floor(N**0.25)
                N = Nx**4
                angles_x = np.linspace(0, 180*degrees, Nx)
                angles = np.zeros((N, 4))
                ind = 0
                for a1 in angles_x:
                    for a2 in angles_x:
                        for a3 in angles_x:
                            for a4 in angles_x:
                                angles[ind, :] = [a1, a2, a3, a4]
                                ind += 1
            angles = sort_positions(angles, self.motor.Get_Position(units="rad"))

            # Measure
            if dim == 0:
                I = np.zeros(N)
            else:
                width, height, x, y = self.camera.Get_ROI()
                Resolution=[height, width]
                I = np.zeros(shape=[N]+Resolution)


            angles_def = np.zeros((N, 4))
            for ind, angles_m in enumerate(angles):
                angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad")
                if dim == 0:
                    I[ind] = self.daca.Get_Signal()
                else:
                    I[ind,:,:] = self.camera.Get_Image()

            # Correccion no 0
            I[I == 0] = 1
            # Normalizacion espejo referencia
            Inorm = I if is_ref else I / self.norm
            # Normalizacion distribucion intensidad
            if dim > 0 and not is_ref:
                Inorm = np.divide(Inorm, self.I_Distribution) * np.max(self.I_Distribution)
        else:
            angles_def = angles
        # Calculate Mueller matrix
        system = [self.S, self.P1, self.R1, self.R2, self.P2]
        M = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=filter and not is_ref)

        # Substract the effect of the BS
        if reflection and not is_ref:
            M = self.BS_trans_inv * M * self.BS_ref_inv

        # Reference
        if is_ref:
            # Normalize
            Mref = (self.BS_trans * self.ref_mirror * self.BS_ref).M[:,:,0] if reflection else np.identity(4)

            self.norm = np.max(M.M[0,0,:]) / Mref[0,0]
            print(np.max(M.M[0,0,:]), Mref[0,0])
            shape = M.shape
            M = M / self.norm

            if dim > 0:
                M = M * np.max(self.I_Distribution) / self.I_Distribution


            M.shape = shape
            # Filter if required (as it was not done previously)
            # if filter:
            #     M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER["filter_tol"])

        # Save file
        if save_folder:
            old_path = os.getcwd()
            os.cd(save_folder)

        if filename:
            if filename.lower() == "auto":
                if dim == 0:
                    filename = "Mueller_0D_{}".format(datetime.date.today())
                else:
                    filename = "Mueller_{}".format(datetime.date.today())
            np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

        if save_folder:
            os.chdir(old_path)



        # if downscale:
        #     image =  Self.camera.Get_Image(is_background=False,rest_background=True,draw=False)
        #     res = cv2.resize(image,dsize=(100,100),interpolation=cv2.INTER_LANCZOS4))
        # Print
        if verbose:
            if dim == 0:
                M.name = filename or "Sample"
                print("The Mueller matrix is:\n", M)
                M.analysis.decompose_polar(tol=1e-8, verbose=True)
                if is_ref:
                    error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
                    print("The error is:   ", error)
            else:
                M.parameters.components(draw=True)
        return M


    def Measure_Mueller_Matrix_Ok(self, dim=0, I=None, angles="random", N=200, filter=True, reflection=False, is_ref=False, save_folder=None, filename=None, verbose=True, downscale=False):
        """Function to measure the Mueller matrix of a mssive sample without spatial resolution.

        If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.

        Args:
            dim (int): If dim is 0 measured with PHD, other wise with camera. Default:0
            I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
            angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'random'.
                "random": Random angles.
                'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
            N (int): Number of measurements used (when I is None). Default: 200.
            filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
            reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
            is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
            save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
            filename (str or None): If not None, the filename where the measurement is stored. Default: None.
            verbose (bool): If True, the information is printed after performing the measurement. Default: True.
            downscale (bool): If True, resize the image with an interpolation. Default: False

        Returns:
            result (Mueller): Measured Mueller matrix.
        """
        # Checks
        if isinstance(angles, np.ndarray) and angles.ndim != 2:
            raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
        elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
            raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
        if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
            raise ValueError("BS in reflection missing")
        elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
            raise ValueError("BS in transmission missing")
        elif reflection and is_ref and self.ref_mirror is None:
            raise ValueError("Reference mirror missing")

        if not is_ref and self.norm == 1:
            print("WARNING: Normalization not performed")

        # Make measurement
        if I is None:
            # Generate angles
            if angles.lower() == "random":
                angles = np.random.rand(N, 4) * 180*degrees
            elif angles.lower() == "linspace":
                Nx = np.floor(N**0.25)
                N = Nx**4
                angles_x = np.linspace(0, 180*degrees, Nx)
                angles = np.zeros((N, 4))
                ind = 0
                for a1 in angles_x:
                    for a2 in angles_x:
                        for a3 in angles_x:
                            for a4 in angles_x:
                                angles[ind, :] = [a1, a2, a3, a4]
                                ind += 1
            angles = sort_positions(angles, self.motor.Get_Position(units="rad"))

            # Measure
            if dim == 0:
                I = np.zeros(N)
            else:
                width, height, x, y = self.camera.Get_ROI()
                Resolution=[height, width]
                I = np.zeros(shape=[N]+Resolution)


            angles_def = np.zeros((N, 4))
            for ind, angles_m in enumerate(angles):
                angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad")
                if dim == 0:
                    I[ind] = self.daca.Get_Signal()
                else:
                    I[ind,:,:] = self.camera.Get_Image()

            # Esto Son normalizaciones
            if not is_ref:
                Inorm = I #/ self.norm
            else:
                Inorm = I
        else:
            angles_def = angles

        # Calculate Mueller matrix
        system = [self.S, self.P1, self.R1, self.R2, self.P2]
        M = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=filter and not is_ref)

        # Substract the effect of the BS
        if reflection and not is_ref:
            M = self.BS_trans_inv * M * self.BS_ref_inv

        # Reference
        if is_ref:
            # Normalize
            Mref = (self.BS_trans * self.ref_mirror * self.BS_ref).M[:,:,0] if reflection else np.identity(4)

            self.norm = np.max(M.M[0,0,:]) / Mref[0,0]
            shape = M.shape
            M = M / self.norm
            M.shape = shape
            # Filter if required (as it was not done previously)
            if filter:
                M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER_633["filter_tol"])

        # Save file
        if save_folder:
            old_path = os.getcwd()
            os.cd(save_folder)

        if filename:
            if filename.lower() == "auto":
                if dim == 0:
                    filename = "Mueller_0D_{}".format(datetime.date.today())
                else:
                    filename = "Mueller_{}".format(datetime.date.today())
            np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

        if save_folder:
            os.chdir(old_path)



        # if downscale:
        #     image =  Self.camera.Get_Image(is_background=False,rest_background=True,draw=False)
        #     res = cv2.resize(image,dsize=(100,100),interpolation=cv2.INTER_LANCZOS4))
        # Print
        if verbose:
            if dim == 0:
                M.name = filename or "Sample"
                print("The Mueller matrix is:\n", M)
                M.analysis.decompose_polar(tol=1e-8, verbose=True)
                if is_ref:
                    error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
                    print("The error is:   ", error)
            else:
                M.parameters.components(draw=True)
        return M

    def Measure_Mueller_Matrix_JdH(self, dim=0, I=None, angles="random", N=200, filter=True, reflection=False, is_ref=False, save_folder=None, filename=None, verbose=True, downscale=False):
        """Function to measure the Mueller matrix of a mssive sample without spatial resolution.

        If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.

        Args:
            dim (int): If dim is 0 measured with PHD, other wise with camera. Default:0
            I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
            angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'random'.
                "random": Random angles.
                'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
            N (int): Number of measurements used (when I is None). Default: 200.
            filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
            reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
            is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
            save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
            filename (str or None): If not None, the filename where the measurement is stored. Default: None.
            verbose (bool): If True, the information is printed after performing the measurement. Default: True.
            downscale (bool): If True, resize the image with an interpolation. Default: False

        Returns:
            result (Mueller): Measured Mueller matrix.
        """
        # Checks
        if isinstance(angles, np.ndarray) and angles.ndim != 2:
            raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
        elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
            raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
        if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
            raise ValueError("BS in reflection missing")
        elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
            raise ValueError("BS in transmission missing")
        elif reflection and is_ref and self.ref_mirror is None:
            raise ValueError("Reference mirror missing")

        if not is_ref and self.norm == 1:
            print("WARNING: Normalization not performed")

        # Make measurement
        if I is None:
            # Generate angles
            if angles.lower() == "random":
                angles = np.random.rand(N, 4) * 180*degrees
            elif angles.lower() == "linspace":
                Nx = np.floor(N**0.25)
                N = Nx**4
                angles_x = np.linspace(0, 180*degrees, Nx)
                angles = np.zeros((N, 4))
                ind = 0
                for a1 in angles_x:
                    for a2 in angles_x:
                        for a3 in angles_x:
                            for a4 in angles_x:
                                angles[ind, :] = [a1, a2, a3, a4]
                                ind += 1
            angles = sort_positions(angles, self.motor.Get_Position(units="rad"))

            # Measure
            if dim == 0:
                I = np.zeros(N)
            else:
                width, height, x, y = self.camera.Get_ROI()
                Resolution=[height, width]
                I = np.zeros(shape=[N]+Resolution)


            angles_def = np.zeros((N, 4))
            cte = 0
            for ind, angles_m in enumerate(angles):
                angles_def[ind, :] = self.motor.Move_Absolute(angles_m + self.angles_0, units="rad")
                # angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad")
                if dim == 0:
                    I[ind] = self.daca.Get_Signal()
                else:
                    I[ind,:,:] = self.camera.Get_Image()

                # print(I[ind])
                Sfin = self.P2.rotate(angles_def[ind, -1], keep=True) * self.R2.rotate(angles_def[ind, -2], keep=True) * self.R1.rotate(angles_def[ind, -3], keep=True) * self.P1.rotate(angles_def[ind, -4], keep=True) * self.S
                Imodel = Sfin.parameters.intensity()
                if cte == 0:
                    cte = I[ind] / Imodel
                # print(Imodel * cte, "\n")

            # Esto Son normalizaciones
            if not is_ref:
                Inorm = I / self.norm
            else:
                Inorm = I
        else:
            angles_def = angles

        # Calculate Mueller matrix
        system = [self.S, self.P1, self.R1, self.R2, self.P2]
        # M = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=filter and not is_ref)
        M = Calculate_Mueller_Matrix_0D(I=Inorm, angles=angles_def, Ms=system)
        print(M)

        # Substract the effect of the BS
        if reflection and not is_ref:
            M = self.BS_trans_inv * M * self.BS_ref_inv

        # Reference
        if is_ref:
            # Normalize
            Mref = (self.BS_trans * self.ref_mirror * self.BS_ref).M[:,:,0] if reflection else np.identity(4)

            self.norm = np.max(M.M[0,0,:]) / Mref[0,0]
            shape = M.shape
            M = M / self.norm
            M.shape = shape
            # Filter if required (as it was not done previously)
            # if filter:
            #     M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER["filter_tol"])

        # Save file
        if save_folder:
            old_path = os.getcwd()
            os.cd(save_folder)

        if filename:
            if filename.lower() == "auto":
                if dim == 0:
                    filename = "Mueller_0D_{}".format(datetime.date.today())
                else:
                    filename = "Mueller_{}".format(datetime.date.today())
            np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

        if save_folder:
            os.chdir(old_path)



        # if downscale:
        #     image =  Self.camera.Get_Image(is_background=False,rest_background=True,draw=False)
        #     res = cv2.resize(image,dsize=(100,100),interpolation=cv2.INTER_LANCZOS4))
        # Print
        if verbose:
            if dim == 0:
                M.name = filename or "Sample"
                print("The Mueller matrix is:\n", M)
                M.analysis.decompose_polar(tol=1e-8, verbose=True)
                if is_ref:
                    error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
                    print("The error is:   ", error)
            else:
                M.parameters.components(draw=True)
        return M
