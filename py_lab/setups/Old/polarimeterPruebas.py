import numpy as np
import os
import datetime

from py_pol.mueller import Mueller, Stokes, degrees

from py_lab.motor import Motor_Multiple
from py_lab.daca import DACA
from py_lab.camera import Camera
from py_lab.utils import sort_positions
from py_lab.config import degrees, CONF_DT_50, CONF_U6, CONF_POLARIMETER
from py_lab.setups.polarimeter_utils import Calculate_Mueller


class Polarimeter(object):
    """New class for polarimeter. It will include control of the rotary motors of PSG and PSA elements, the data acquisition card for measuring photodiode signal (bulk configuation) and a camera (spatial resolution configuration).

    Args:
        calibration (bool): Load default calibration. Default: True.
        use_daca (bool): If True, measurement signal is get through DACA (photodiode). If False, the camera is used instead. Default: True.

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        camera (Camera): Camera object.
        daca (DACA): Data acquisition card object.
    """

    def __init__(self, calibration=True, use_daca=True):
        self.motor = Motor_Multiple(name="DT50", N=4)
        self.daca = DACA(name="U6")
        self.camera = Camera(name="uEye", M=1, wavelength=None)
        self.norm = 1

        # Load calibration info
        if calibration:
            self.Load_Calibration(filename=CONF_POLARIMETER['cal_file'], BS_ref=CONF_POLARIMETER['BS_ref'], BS_trans=CONF_POLARIMETER['BS_trans'], ref_mirror=CONF_POLARIMETER['ref_mirror'], folder=CONF_POLARIMETER["cal_folder"])

        # Just in case some default data must be stored
        else:
            self.angles_0 = np.zeros(4)
            self.cal_dict = {}
            self.S = None
            self.P1 = None
            self.R1 = None
            self.R2 = None
            self.P2 = None
            self.BS_ref_inv = None
            self.BS_ref = None
            self.BS_trans = None
            self.BS_trans_inv = None
            self.ref_mirror = None


    def Load_Calibration(self, filename=None, BS_ref=None, BS_trans=None, ref_mirror=None, folder=None):
        """Load some or all calibration files. If any of the filename parameters is None, that calibration will be ignored.

        Args:
            filename (str or None): Filename of the main calibration. Default: None.
            BS_ref (str or None): Filename of the beam splitter in reflection configuration. Default: None.
            BS_trans (str or None): Filename of the main calibration. Default: None.
            ref_mirror (str or None): Filename of the main calibration. Default: None.
            folder (str or None): Filename of the main calibration. Default: None.
        """

        # Go to calibration folder
        if folder:
            try:
                old_folder = os.getcwd()
                os.chdir(folder)
            except:
                print("Folder {} nonexistent".format(folder))

        # Main calibration
        if filename:
            try:
                data = np.load(filename)

                self.angles_0 = np.array([data["P1_az"], data["R1_az"], data["R2_az"], data["P2_az"]])
                self.S = Stokes().general_azimuth_ellipticity(azimuth=data["illum_az"], ellipticity=data["illum_el"], degree_pol=data["illum_pol_degree"])
                self.P1 = Mueller().diattenuator_retarder_linear(p1=data["P1_p1"], p2=data["P1_p2"], R=data["P1_R"], azimuth=-data["P1_az"])
                self.R1 = Mueller().diattenuator_retarder_linear(p1=data["R1_p1"], p2=data["R1_p2"], R=data["R1_R"], azimuth=-data["R1_az"])
                self.R2 = Mueller().diattenuator_retarder_linear(p1=data["R2_p1"], p2=data["R2_p2"], R=data["R2_R"], azimuth=-data["R2_az"])
                self.P2 = Mueller().diattenuator_linear(p1=data["P2_p1"], p2=data["P2_p2"], azimuth=-data["P2_az"])

                self.cal_dict = data

            except:
                print("File {} nonexistent".format(filename))

        # Reflection configuration
        if BS_ref:
            try:
                data = np.load(BS_ref)
                comp = data["Mcomp"]
                self.BS_ref = Mueller().from_components(comp)
                self.BS_ref_inv = self.BS_ref.inverse(keep=True)
            except:
                self.BS_ref_inv = None
                self.BS_ref = None
                print("File {} nonexistent".format(BS_ref))

        if BS_trans:
            try:
                data = np.load(BS_trans)
                comp = data["Mcomp"]
                self.BS_trans = Mueller().from_components(comp)
                self.BS_trans_inv = self.BS_trans.inverse(keep=True)
            except:
                self.BS_trans_inv = None
                self.BS_trans = None
                print("File {} nonexistent".format(BS_trans))

        if ref_mirror:
            try:
                data = np.load(ref_mirror)
                comp = data["Mcomp"]
                self.ref_mirror = Mueller().from_components(comp)
            except:
                print("File {} nonexistent".format(ref_mirror))

        if folder:
            try:
                os.chdir(old_folder)
            except:
                print("Unable to return to original folder")

        return self.BS_ref_inv, self.BS_trans_inv


    def Open(self):
        self.motor.Open(port=CONF_DT_50["ports"], invert=CONF_DT_50["invert"])
        self.motor.Home()
        self.daca.Open(AIN=CONF_U6["AIN"], AIN_ref=CONF_U6["AIN_ref"])
        if self.camera is not None:
            self.camera.Open()

    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=self.angles_0, units="rad")

    def Close(self):
        """Close all devices."""
        self.motor.Close()
        if self.camera is not None:
            self.camera.Close()

    def Measure_Mueller_Matrix(self, I=None, angles="random", N=200, filter=True, reflection=False, is_ref=False, save_folder=None, filename=None, verbose=True):
        """Function to measure the Mueller matrix of a mssive sample without spatial resolution.

        If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.

        Args:
            I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
            angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'random'.
                "random": Random angles.
                'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
            N (int): Number of measurements used (when I is None). Default: 200.
            filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
            reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
            is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
            save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
            filename (str or None): If not None, the filename where the measurement is stored. Default: None.
            verbose (bool): If True, the information is printed after performing the measurement. Default: True.

        Returns:
            result (Mueller): Measured Mueller matrix.
        """
        # Checks
        if isinstance(angles, np.ndarray) and angles.ndim != 2:
            raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
        elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
            raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
        if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
            raise ValueError("BS in reflection missing")
        elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
            raise ValueError("BS in transmission missing")
        elif reflection and is_ref and self.ref_mirror is None:
            raise ValueError("Reference mirror missing")

        if not is_ref and self.norm == 1:
            print("WARNING: Normalization not performed")

        # Make measurement
        if I is None:
            # Generate angles
            if angles.lower() == "random":
                angles = np.random.rand(N, 4) * 180*degrees
            elif angles.lower() == "linspace":
                Nx = np.floor(N**0.25)
                N = Nx**4
                angles_x = np.linspace(0, 180*degrees, Nx)
                angles = np.zeros((N, 4))
                ind = 0
                for a1 in angles_x:
                    for a2 in angles_x:
                        for a3 in angles_x:
                            for a4 in angles_x:
                                angles[ind, :] = [a1, a2, a3, a4]
                                ind += 1
            angles = sort_positions(angles, self.motor.Get_Position(units="rad"))

            # Measure
            if dim == 0:
                I = np.zeros(N)
            else:
                width, height, x, y = cam.Get_ROI(verbose=True)
                Resolution=[width,height]
                I = np.zeros(shape=[N]+Resolution)


            angles_def = np.zeros((N, 4))
            for ind, angles_m in enumerate(angles):
                angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad")
                if dim == 0:
                    I[ind] = self.daca.Get_Signal()
                else:
                    I[ind,:,:] = self.camera.Get_image()

            if not is_ref:
                Inorm = I / self.norm
            else:
                Inorm = I
        else:
            angles_def = angles

        # Calculate Mueller matrix
        system = [self.S, self.P1, self.R1, self.R2, self.P2]
        M = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=filter and not is_ref)

        # Substract the effect of the BS
        if reflection and not is_ref:
            M = self.BS_trans_inv * M * self.BS_ref_inv

        # Reference
        if is_ref:
            # Normalize
            Mref = (self.BS_trans * self.ref_mirror * self.BS_ref).M[:,:,0] if reflection else np.identity(4)

            self.norm = M.M[0,0,0] / Mref[0,0]
            M = M / self.norm
            # Filter if required (as it was not done previously)
            # if filter:
            #     M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER["filter_tol"])

        # Save file
        if save_folder:
            old_path = os.getcwd()
            os.cd(save_folder)

        if filename:
            if filename.lower() == "auto":
                filename = "Mueller_0D_{}".format(datetime.date.today())
            np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

        if save_folder:
            os.chdir(old_path)

        # Print
        if verbose:
            M.name = filename or "Sample"
            print("The Mueller matrix is:\n", M)
            M.analysis.decompose_polar(tol=1e-8, verbose=True)
            if is_ref:
                error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
                print("The error is:   ", error)

        return M
