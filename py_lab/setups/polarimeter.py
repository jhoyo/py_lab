import numpy as np
import os
import datetime
from tqdm import tqdm
from py_pol.mueller import Mueller, Stokes, degrees
import time
import py_lab.utils as utils                                # Utils py-lab
from py_lab.motor import Motor_Multiple
from py_lab.daca import DACA
from py_lab.spectrometer import Spectrometer
from py_lab.camera import Camera
from py_lab.utils import sort_positions, calculate_polarimetry_angles
from py_lab.config import degrees, CONF_DT_50, CONF_U6, CONF_POLARIMETER, CONF_INTEL, CONF_T7, CONF_ZABER, CONF_AVANTES
from py_lab.setups.polarimeter_utils import Calculate_Mueller, Calculate_Mueller_Matrix_0D, Msystem_From_Dict
from py_lab.utils import Rotation_Poincare, PSG_angles_2_states, PSA_angles_2_states, PSG_states_2_angles, PSA_states_2_angles



class Polarimeter(object):
    """New class for polarimeter. It will include control of the rotary motors of PSG and PSA elements, the data acquisition card for measuring photodiode signal (bulk configuation) and a camera (spatial resolution configuration).

    Args:
        calibration (string): Load all calibration files ("full"), only the corresponding to illumination, PSG and PSA ("minimal") or none ("none"). Default: "full".
        conf_dict (dict): Dictionary with the calibration configuration. Default: CONF_POLARIMETER.
        name_motor (string): Name of the rotating motors used. Default: "DT50".
        name_daca (string): Name of the data acquisition card used. Default: "U6".
        name_camera (string or None): Name of the camera used. Default: None.

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        camera (Camera): Camera object.
        daca (DACA): Data acquisition card object.
        norm (float): Intensity normalization constant.
        I_Distribution (np.ndarray): Intensity normalization array.
    """

    def __init__(self, calibration="full", conf_dict=CONF_POLARIMETER, name_motor="DT50", name_daca="U6", name_spectrometer="Avantes", name_camera=None):

        
        self.motor = Motor_Multiple(name=name_motor, N=4)
        self.daca = DACA(name=name_daca)
        self.spectrometer = Spectrometer(name=name_spectrometer)
        if name_camera:
           self.camera = Camera(name=name_camera, M=1, wavelength=None)
        else:
            self.camera = None
        self.norm = 1
        self.I_Distribution = None
        self.I_Distribution_norm = 0
        self.I_Threshold = None

        self.name_motor = name_motor 
        self.name_daca = name_daca
        self.name_spectrometer = name_spectrometer

        # Default values
        self.BS_trans_inv = Mueller().vacuum()
        self.BS_trans = Mueller().vacuum()
        self.BS_ref_inv = Mueller().vacuum()
        self.BS_ref = Mueller().vacuum()
        self.ref_mirror = Mueller().vacuum()
        self.objective_direct = Mueller().vacuum()
        self.objective_return = Mueller().vacuum()
        self.objective_direct_inv = Mueller().vacuum()
        self.objective_return_inv = Mueller().vacuum()

        # # Load calibration info
        if calibration.lower() == "full":
             self.Load_Calibration(filename=conf_dict['cal_file'], BS_ref=conf_dict['BS_ref'], BS_trans=conf_dict['BS_trans'], ref_mirror=conf_dict['ref_mirror'], folder=conf_dict["cal_folder"], objective = conf_dict['objective_trans'])
        # elif calibration.lower() == "minimal":
        #     self.Load_Calibration(filename=conf_dict['cal_file'], folder=conf_dict["cal_folder"])

        # # Just in case some default data must be stored
        # else:
        #     self.angles_0 = np.zeros(4)
        #     self.cal_dict = {}
        #     self.S = None
        #     self.P1 = None
        #     self.R1 = None
        #     self.R2 = None
        #     self.P2 = None
        #     self.BS_ref_inv = None
        #     self.BS_ref = None
        #     self.BS_trans = None
        #     self.BS_trans_inv = None
        #     self.ref_mirror = None


    def Load_Calibration(self, filename=None, BS_ref=None, BS_trans=None, ref_mirror=None, objective=None, folder=None):
        """Load some or all calibration files. If any of the filename parameters is None, that calibration will be ignored.

        Args:
            filename (str or None): Filename of the main calibration. Default: None.
            BS_ref (str or None): Filename of the beam splitter in reflection configuration. Default: None.
            BS_trans (str or None): Filename of the main calibration. Default: None.
            ref_mirror (str or None): Filename of the main calibration. Default: None.
            folder (str or None): Filename of the main calibration. Default: None.
        """

        # Go to calibration folder
        if folder:
            try:
                old_folder = os.getcwd()
                os.chdir(folder)
            except:
                print("Folder {} nonexistent".format(folder))

        # Main calibration
        if filename:
            try:
                cal_dict = np.load(filename)

                self.angles_0 = np.array([cal_dict["P1_az"], cal_dict["R1_az"], cal_dict["R2_az"], cal_dict["P2_az"]])
                # self.S = Stokes().general_azimuth_ellipticity(azimuth=data["illum_az"], ellipticity=data["illum_el"], degree_pol=data["illum_pol_degree"])
                # self.P1 = Mueller().diattenuator_retarder_linear(p1=data["P1_p1"], p2=data["P1_p2"], R=data["P1_R"], azimuth=-data["P1_az"])
                # self.R1 = Mueller().diattenuator_retarder_linear(p1=data["R1_p1"], p2=data["R1_p2"], R=data["R1_R"], azimuth=-data["R1_az"])
                # self.R2 = Mueller().diattenuator_retarder_linear(p1=data["R2_p1"], p2=data["R2_p2"], R=data["R2_R"], azimuth=-data["R2_az"])
                # self.P2 = Mueller().diattenuator_linear(p1=data["P2_p1"], p2=data["P2_p2"], azimuth=-data["P2_az"])
                self.S, self.P1, self.R1, self.R2, self.P2 = Msystem_From_Dict(cal_dict, initial_angles=True)

                self.cal_dict = cal_dict

            except:
                loc = os.getcwd()
                print("File {} at location {} nonexistent".format(filename, loc))
                return

        # Reflection configuration
        if BS_ref:
            try:
                data = np.load(BS_ref)
                comp = data["Mcomp"]
                self.BS_ref = Mueller().from_components(comp)
                self.BS_ref_inv = self.BS_ref.inverse(keep=True)
            except:
                self.BS_ref_inv = Mueller().vacuum()
                self.BS_ref = Mueller().vacuum()
                print("File {} nonexistent".format(BS_ref))


        if BS_trans:
            try:
                data = np.load(BS_trans)
                comp = data["Mcomp"]
                self.BS_trans = Mueller().from_components(comp)
                self.BS_trans_inv = self.BS_trans.inverse(keep=True)
            except:
                self.BS_trans_inv = Mueller().vacuum()
                self.BS_trans = Mueller().vacuum()
                print("File {} nonexistent".format(BS_trans))

        if ref_mirror:
            try:
                data = np.load(ref_mirror)
                comp = data["Mcomp"]
                self.ref_mirror = Mueller().from_components(comp)
            except:
                self.ref_mirror = Mueller().vacuum()
                print("File {} nonexistent".format(ref_mirror))


        if objective:
            try:
                data = np.load(objective)
                comp = data["Mcomp"]
                self.objective_direct = Mueller().from_components(comp)
                self.objective_return = self.objective_direct.reciprocal()
                self.objective_direct_inv = self.objective_direct.inverse()
                self.objective_return_inv = self.objective_return.inverse()
            except:
                self.objective_direct = Mueller().vacuum()
                self.objective_return = Mueller().vacuum()
                self.objective_direct_inv = Mueller().vacuum()
                self.objective_return_inv = Mueller().vacuum()
                print("File {} nonexistent".format(ref_mirror))


        if folder:
            try:
                os.chdir(old_folder)
            except:
                print("Unable to return to original folder")

        # return self.BS_ref_inv, self.BS_trans_inv


    def Open(self):

        if self.name_motor == "DT50":
            config_motor = CONF_DT_50
        
        elif self.name_motor == "InteliDrives":
            config_motor = CONF_INTEL

        elif self.name_motor == "Zaber":
            config_motor = CONF_ZABER

        if self.name_daca == "U6":
            config_daca = CONF_U6
        
        elif self.name_daca == "T7":
            config_daca = CONF_T7
        
        if self.name_spectrometer == "Avantes":
            config_spectrometer = CONF_AVANTES
        
        self.motor.Open(port=config_motor["ports"], invert=config_motor["invert"],axis=config_motor["axes"])
        self.motor.Home()
        print('\n')
        self.daca.Open(AIN=config_daca["AIN"], AIN_ref=config_daca["AIN_ref"])
        self.spectrometer.Open()
        self.spectrometer.Get_Wavelength()
        self.spectrometer.Get_Number_Px()
        self.spectrometer.Set_Parameters(exposure = config_spectrometer["exposure"], N_average= config_spectrometer["N_average"])   

        if self.camera is not None:
            self.camera.Open()
            ## Parámetros de la Cámara
            self.camera.Set_Property_Auto(name="Exposure", value=0)
            self.camera.Set_Property(name="Exposure", value=0.5)
            self.camera.Set_Property_Auto(name="Gain", value=0)
            self.camera.Set_Property(name="Framerate", value=3)
            # self.camera.Set_Property_Auto(name="Resolution", value=0)
            # self.camera.Set_Property(name="Resolution", value=[2560,1920])
            self.camera.Set_ROI()

        # Origin angles reference TODO: NOT WORKING
        # self.motor.Clear_Reference()
        # self.motor.Move_Absolute(self.angles_0, units="rad")
        # self.motor.Set_Reference()

    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=self.angles_0, units="rad")




    def Close(self):
        """Close all devices."""
        self.motor.Close()
        self.spectrometer.Close()
        if self.camera is not None:
            self.camera.Close()


    def Set_Property(self):
        """Method to set some properties of the camera whic changes the normalization constant. 
        TODO"""
        pass



    def Measure_Intensity(self,dim=0, binning = (1,1), threshold=5):
        """ Get the intensity when motors are at home.
            Args:
                dim(int): If dim = 0 measured with Photodiode, instead use the camera.
                default = 0
                ROI: If ROI True define the region of interest and just get that intensity
        """
        if dim == 0:
            I = self.daca.Get_Signal()
        else:
            I = np.zeros((self.camera.Get_Property(name="Resolution")))
            I = self.camera.Get_Image(is_background=False,rest_background=True, binning = binning, draw=True)
            I[I == 0] = 1 ## Avoid problem with division by 0
        self.I_Distribution = I
        self.I_Threshold = I < threshold



    def Measure_Mueller_Matrix(self, dim=0, I=None, angles="perfect", Nmeasures=144, Naverage=1, filter=True, reflection=False, is_ref=False, iterable=None, iterable_function=None, iterable_arg=None, save_folder=None, filename=None, add_angle_0=False, binning = (1,1), tol=CONF_POLARIMETER["filter_tol"], rotation = None, verbose=True):
        """Function to measure the Mueller matrix of a mssive sample without spatial resolution.

        If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.

        Args:
            dim (int): If dim is 0 measured with PHD, other wise with camera. Default:0
            I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
            angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'perfect'.
                "perfect": PSG and PSA states distributed in the poincare sphere as a regular solid. Algorithm calculates the nearest lower number of measurements to match two regular solids.
                "spiral": PSG and PSA states distributed in the poincare sphere as a spiral with the same number of states in each case. N will be reduced to a number in the form of M^2 where M is an integer.
                "random": Random angles.
                'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
            Nmeasures (int): Number of measurements used (when I is None). Default: 144.
            naverage (int): Number of times each measurement is repeated. The intensity is then averaged. Default: 1.
            filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
            reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
            is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
            downscale (bool): If True, resize the image with an interpolation. Default: False. TODO
            iterable (iterable): Iterable to run while measuring. Default: None.
            iterable_function (function ref): Function to execute as the iterable is running. Default: None.
            iterable_arg (tuple or list): List of other arguments required by iterable_function. Default: None.
            save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
            filename (str or None): If not None, the filename where the measurement is stored. Default: None.
            verbose (bool): If True, the information is printed after performing the measurement. Default: True.
            rotation (np.ndarray or None): If not None, add an array [lat,lon]. Default: None.

        Returns:
            result (Mueller): Measured Mueller matrix.
        """
        # Checks
        if isinstance(angles, np.ndarray) and angles.ndim != 2:
            raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
        elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
            raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
        if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
            raise ValueError("BS in reflection missing")
        elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
            raise ValueError("BS in transmission missing")
        elif reflection and is_ref and self.ref_mirror is None:
            raise ValueError("Reference mirror missing")

        if not is_ref and self.norm == 1:
            print("WARNING: Normalization not performed")

        # Make measurement
        if I is None:
            # Generate angles
            angles = calculate_polarimetry_angles(Nmeasures, type=angles)
            
            if rotation is not None:
                if len(rotation)>1:
                    angles_new = np.asarray(angles)
                    PSG_az,PSG_el = PSG_angles_2_states(angles[:,0],angles[:,1])
                    PSA_az,PSA_el = PSA_angles_2_states(angles[:,2],angles[:,3])
                    PSG_az_new,PSG_el_new = Rotation_Poincare(PSG_az,PSG_el,rotation[0],rotation[1])
                    PSA_az_new,PSA_el_new = Rotation_Poincare(PSA_az,PSA_el,rotation[0],rotation[1])
                    angles_new[:,0],angles_new[:,1] = PSG_states_2_angles(PSG_az_new,PSG_el_new)
                    angles_new[:,3],angles_new[:,2] = PSA_states_2_angles(PSA_az_new,PSA_el_new)
                else:
                    print("Add a correct array: [lat, lon]")
            
            # if angles.lower() == "random":
            #     angles = np.random.rand(Nmeasures, 4) * 180*degrees
            # elif angles.lower() == "linspace":
            #     Nx = np.floor(Nmeasures**0.25)
            #     Nmeasures = Nx**4
            #     angles_x = np.linspace(0, 180*degrees, Nx)
            #     angles = np.zeros((Nmeasures, 4))
            #     ind = 0
            #     for a1 in angles_x:
            #         for a2 in angles_x:
            #             for a3 in angles_x:
            #                 for a4 in angles_x:
            #                     angles[ind, :] = [a1, a2, a3, a4]
            #                     ind += 1
            angles = sort_positions(angles, self.motor.Get_Position(units="rad"))
            

            # Prepare for measurement
            if iterable is not None:
                Niter = len(iterable)
            else:
                iterable = np.array([0])
                Niter = 0
            if dim == 0 and Niter == 0:
                I = np.zeros(Nmeasures)
            elif dim == 0 and Niter > 0:
                I = np.zeros((Nmeasures, Niter))
                #dim = 1
            elif dim == 2:
                width, height, x, y = self.camera.Get_ROI()
                if height % binning[0] != 0: 
                    binning[0] = 1
                    print("WARNING: in X dimension, binning is not divisible")
                if width % binning[1] != 0: 
                    binning[1] = 1
                    print("WARNING: in Y dimension, binning is not divisible")
                Resolution=[height//binning[0], width//binning[1]]
                I = np.zeros(shape=[Nmeasures]+Resolution)

            if Niter>0:
                has_slm = True
            else:
                has_slm = False

            # Measure
            angles_def = np.zeros((Nmeasures, 4))
            # Loop in measurements
            for ind, angles_m in enumerate(angles): 
                print(ind,end='\r') 
                angles_def[ind, :] = angles_m
                if add_angle_0:
                    angles_m = angles_m + self.angles_0
                # angles_def[ind, :] = angles_m
                # self.motor.Move_Absolute(angles_m, units="rad") 
                self.motor.Move_Absolute(angles_m, units="rad")
                #angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad") #No se tiene en cuenta el ángulo de 0.
                time.sleep(0.3)
                # Loop in iterable
                
                for indI, elem in enumerate(iterable):
                    if iterable_function:
                        iterable_function(elem, *iterable_arg)
                    # Loop in average
                    # Iaverage = np.zeros(shape=[Naverage]+Resolution)
                    Iaverage = np.zeros(shape=[Naverage])

                    for indA in range(Naverage):
                        if dim == 0:
                            Iaverage[indA] = self.daca.Get_Signal()
                        else:
                            Iaverage[indA,:,:] = self.camera.Get_Image(binning=binning)
                            time.sleep(0.5)
                    if dim == 0 and not has_slm:
                        I[ind] = np.mean(Iaverage)
                    elif dim == 1 or has_slm:
                        I[ind,indI] = np.mean(Iaverage, axis=0)
                    else:
                        I[ind,:,:] = np.mean(Iaverage, axis=0)
            
            # Correccion no 0
            if dim > 0:
                I[I == 0] = 1
              
            # Normalizacion distribucion intensidad
            if dim > 0: # and not is_ref:
                if self.I_Distribution_norm == 0:
                    Inorm = np.divide(I, self.I_Distribution) * np.mean(self.I_Distribution[np.logical_not(self.I_Threshold)])
                else:
                    Inorm = np.divide(I, self.I_Distribution) * self.I_Distribution_norm
            else: 
                Inorm = I
        else:
            angles_def = angles
            
            Inorm = I
        # Calculate Mueller matrix
        system = [self.S, self.P1, self.R1, self.R2, self.P2]
        
        M,_ = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=True)
        if self.I_Threshold is not None:
            M[self.I_Threshold] = Mueller().from_components(np.zeros(16))
        
        # Substract the effect of the BS
        if reflection and not is_ref:
            M = self.BS_trans_inv * M * self.BS_ref_inv
        if reflection and dim == 2: 
            M = self.objective_return_inv * M * self.objective_direct_inv
        
        # Reference
        if is_ref:
            if dim == 2:
                # Normalize
                Mref = self.ref_mirror.M[:,:,0] if reflection else np.identity(4)
                self.I_Distribution_norm = np.mean(self.I_Distribution[np.logical_not(self.I_Threshold)])
        
                if self.I_Threshold is not None:
                    comp = M.parameters.components()
                    self.norm = np.mean(comp[0][np.logical_not(self.I_Threshold)]) / Mref[0,0]
                else:
                    self.norm = np.mean(M.M[0,0,:]) / Mref[0,0]
                print(np.max(M.M[0,0,:]), Mref[0,0])
                shape = M.shape
            elif dim == 0:
                self.norm = M.M[0,0,0]
                Mref = np.identity(4)

            else:
                print("WARNING: NOT IMPLEMENTED")


            
            # Filter if required (as it was not done previously)

        # Normalize to mirror reflectivity
        M = M / self.norm
        # Filter physical conditions
        if filter:
            M.analysis.filter_physical_conditions(tol=tol)


        # Save file
        if save_folder:
            old_path = os.getcwd()
            os.cd(save_folder)

        if filename:
            if filename.lower() == "auto":
                if dim == 0:
                    filename = "Mueller_0D_{}".format(datetime.date.today())
                else:
                    filename = "Mueller_{}".format(datetime.date.today())
            np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

        if save_folder:
            os.chdir(old_path)



        # if downscale:
        #     image =  Self.camera.Get_Image(is_background=False,rest_background=True,draw=False)
        #     res = cv2.resize(image,dsize=(100,100),interpolation=cv2.INTER_LANCZOS4))
        # Print
        if verbose:
            if dim == 0:
                M.name = filename or "Sample"
                print("The Mueller matrix is:\n", M)
                M.analysis.decompose_polar(tol=1e-8, verbose=True)
                if is_ref:
                    error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
                    print("The error is:   ", error)
            else:
                M.parameters.components(draw=True)
        return M


    # def Measure_Mueller_Matrix_Ok(self, dim=0, I=None, angles="random", N=200, filter=True, reflection=False, is_ref=False, save_folder=None, filename=None, verbose=True, downscale=False):
    #     """Function to measure the Mueller matrix of a mssive sample without spatial resolution.
    #
    #     If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.
    #
    #     Args:
    #         dim (int): If dim is 0 measured with PHD, other wise with camera. Default:0
    #         I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
    #         angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'random'.
    #             "random": Random angles.
    #             'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
    #         N (int): Number of measurements used (when I is None). Default: 200.
    #         filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
    #         reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
    #         is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
    #         save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
    #         filename (str or None): If not None, the filename where the measurement is stored. Default: None.
    #         verbose (bool): If True, the information is printed after performing the measurement. Default: True.
    #         downscale (bool): If True, resize the image with an interpolation. Default: False
    #
    #     Returns:
    #         result (Mueller): Measured Mueller matrix.
    #     """
    #     # Checks
    #     if isinstance(angles, np.ndarray) and angles.ndim != 2:
    #         raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
    #     elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
    #         raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
    #     if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
    #         raise ValueError("BS in reflection missing")
    #     elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
    #         raise ValueError("BS in transmission missing")
    #     elif reflection and is_ref and self.ref_mirror is None:
    #         raise ValueError("Reference mirror missing")
    #
    #     if not is_ref and self.norm == 1:
    #         print("WARNING: Normalization not performed")
    #
    #     # Make measurement
    #     if I is None:
    #         # Generate angles
    #         if angles.lower() == "random":
    #             angles = np.random.rand(N, 4) * 180*degrees
    #         elif angles.lower() == "linspace":
    #             Nx = np.floor(N**0.25)
    #             N = Nx**4
    #             angles_x = np.linspace(0, 180*degrees, Nx)
    #             angles = np.zeros((N, 4))
    #             ind = 0
    #             for a1 in angles_x:
    #                 for a2 in angles_x:
    #                     for a3 in angles_x:
    #                         for a4 in angles_x:
    #                             angles[ind, :] = [a1, a2, a3, a4]
    #                             ind += 1
    #         angles = sort_positions(angles, self.motor.Get_Position(units="rad"))
    #
    #         # Measure
    #         if dim == 0:
    #             I = np.zeros(N)
    #         else:
    #             width, height, x, y = self.camera.Get_ROI()
    #             Resolution=[height, width]
    #             I = np.zeros(shape=[N]+Resolution)
    #
    #
    #         angles_def = np.zeros((N, 4))
    #         for ind, angles_m in enumerate(angles):
    #             angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad")
    #             if dim == 0:
    #                 I[ind] = self.daca.Get_Signal()
    #             else:
    #                 I[ind,:,:] = self.camera.Get_Image()
    #
    #         # Esto Son normalizaciones
    #         if not is_ref:
    #             Inorm = I / self.norm
    #         else:
    #             Inorm = I
    #     else:
    #         angles_def = angles
    #         Inorm = I
    #
    #     # Calculate Mueller matrix
    #     system = [self.S, self.P1, self.R1, self.R2, self.P2]
    #     M = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=filter and not is_ref)
    #
    #     # Substract the effect of the BS
    #     if reflection and not is_ref:
    #         M = self.BS_trans_inv * M * self.BS_ref_inv
    #
    #     # Reference
    #     if is_ref:
    #         # Normalize
    #         Mref = (self.BS_trans * self.ref_mirror * self.BS_ref).M[:,:,0] if reflection else np.identity(4)
    #
    #         self.norm = np.max(M.M[0,0,:]) / Mref[0,0]
    #         shape = M.shape
    #         M = M / self.norm
    #         M.shape = shape
    #         # Filter if required (as it was not done previously)
    #         # if filter:
    #         #     M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER["filter_tol"])
    #
    #     # Save file
    #     if save_folder:
    #         old_path = os.getcwd()
    #         os.cd(save_folder)
    #
    #     if filename:
    #         if filename.lower() == "auto":
    #             if dim == 0:
    #                 filename = "Mueller_0D_{}".format(datetime.date.today())
    #             else:
    #                 filename = "Mueller_{}".format(datetime.date.today())
    #         np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())
    #
    #     if save_folder:
    #         os.chdir(old_path)
    #
    #
    #
    #     # if downscale:
    #     #     image =  Self.camera.Get_Image(is_background=False,rest_background=True,draw=False)
    #     #     res = cv2.resize(image,dsize=(100,100),interpolation=cv2.INTER_LANCZOS4))
    #     # Print
    #     if verbose:
    #         if dim == 0:
    #             M.name = filename or "Sample"
    #             print("The Mueller matrix is:\n", M)
    #             M.analysis.decompose_polar(tol=1e-8, verbose=True)
    #             if is_ref:
    #                 error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
    #                 print("The error is:   ", error)
    #         else:
    #             M.parameters.components(draw=True)
    #     return M
    #
    # def Measure_Mueller_Matrix_JdH(self, dim=0, I=None, angles="random", N=200, filter=True, reflection=False, is_ref=False, save_folder=None, filename=None, verbose=True, downscale=False):
    #     """Function to measure the Mueller matrix of a mssive sample without spatial resolution.
    #
    #     If a file is saved, the file stores also the real angles and intensities measured, so the measurement can be used afterwards with a different calibration.
    #
    #     Args:
    #         dim (int): If dim is 0 measured with PHD, other wise with camera. Default:0
    #         I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
    #         angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'random'.
    #             "random": Random angles.
    #             'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
    #         N (int): Number of measurements used (when I is None). Default: 200.
    #         filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
    #         reflection (bool): If True, polarimeter working on reflection, so the calibration data of the 50/50 beam splitter is used. Default: False.
    #         is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
    #         save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
    #         filename (str or None): If not None, the filename where the measurement is stored. Default: None.
    #         verbose (bool): If True, the information is printed after performing the measurement. Default: True.
    #         downscale (bool): If True, resize the image with an interpolation. Default: False
    #
    #     Returns:
    #         result (Mueller): Measured Mueller matrix.
    #     """
    #     # Checks
    #     if isinstance(angles, np.ndarray) and angles.ndim != 2:
    #         raise ValueError("Angles of shape {} must have 2 dimensions".format(angles.shape))
    #     elif isinstance(angles, np.ndarray) and angles.shape[1] != 4:
    #         raise ValueError("Shape of angles array is {} instead of 4xN".format(angles.shape))
    #     if reflection and ((self.BS_ref_inv is None )or (self.BS_ref is None)):
    #         raise ValueError("BS in reflection missing")
    #     elif reflection and ((self.BS_trans_inv is None) or (self.BS_trans is None)):
    #         raise ValueError("BS in transmission missing")
    #     elif reflection and is_ref and self.ref_mirror is None:
    #         raise ValueError("Reference mirror missing")
    #
    #     if not is_ref and self.norm == 1:
    #         print("WARNING: Normalization not performed")
    #
    #     # Make measurement
    #     if I is None:
    #         # Generate angles
    #         if angles.lower() == "random":
    #             angles = np.random.rand(N, 4) * 180*degrees
    #         elif angles.lower() == "linspace":
    #             Nx = np.floor(N**0.25)
    #             N = Nx**4
    #             angles_x = np.linspace(0, 180*degrees, Nx)
    #             angles = np.zeros((N, 4))
    #             ind = 0
    #             for a1 in angles_x:
    #                 for a2 in angles_x:
    #                     for a3 in angles_x:
    #                         for a4 in angles_x:
    #                             angles[ind, :] = [a1, a2, a3, a4]
    #                             ind += 1
    #         angles = sort_positions(angles, self.motor.Get_Position(units="rad"))
    #
    #         # Measure
    #         if dim == 0:
    #             I = np.zeros(N)
    #         else:
    #             width, height, x, y = self.camera.Get_ROI()
    #             Resolution=[height, width]
    #             I = np.zeros(shape=[N]+Resolution)
    #
    #
    #         angles_def = np.zeros((N, 4))
    #         cte = 0
    #         for ind, angles_m in enumerate(angles):
    #             angles_def[ind, :] = self.motor.Move_Absolute(angles_m + self.angles_0, units="rad")
    #             # angles_def[ind, :] = self.motor.Move_Absolute(angles_m, units="rad")
    #             if dim == 0:
    #                 I[ind] = self.daca.Get_Signal()
    #             else:
    #                 I[ind,:,:] = self.camera.Get_Image()
    #
    #             # print(I[ind])
    #             Sfin = self.P2.rotate(angles_def[ind, -1], keep=True) * self.R2.rotate(angles_def[ind, -2], keep=True) * self.R1.rotate(angles_def[ind, -3], keep=True) * self.P1.rotate(angles_def[ind, -4], keep=True) * self.S
    #             Imodel = Sfin.parameters.intensity()
    #             if cte == 0:
    #                 cte = I[ind] / Imodel
    #             # print(Imodel * cte, "\n")
    #
    #         # Esto Son normalizaciones
    #         if not is_ref:
    #             Inorm = I / self.norm
    #         else:
    #             Inorm = I
    #     else:
    #         angles_def = angles
    #
    #     # Calculate Mueller matrix
    #     system = [self.S, self.P1, self.R1, self.R2, self.P2]
    #     # M = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=filter and not is_ref)
    #     M = Calculate_Mueller_Matrix_0D(I=Inorm, angles=angles_def, Ms=system)
    #     print(M)
    #
    #     # Substract the effect of the BS
    #     if reflection and not is_ref:
    #         M = self.BS_trans_inv * M * self.BS_ref_inv
    #
    #     # Reference
    #     if is_ref:
    #         # Normalize
    #         Mref = (self.BS_trans * self.ref_mirror * self.BS_ref).M[:,:,0] if reflection else np.identity(4)
    #
    #         self.norm = np.max(M.M[0,0,:]) / Mref[0,0]
    #         shape = M.shape
    #         M = M / self.norm
    #         M.shape = shape
    #         # Filter if required (as it was not done previously)
    #         # if filter:
    #         #     M.analysis.filter_physical_conditions(tol=CONF_POLARIMETER["filter_tol"])
    #
    #     # Save file
    #     if save_folder:
    #         old_path = os.getcwd()
    #         os.cd(save_folder)
    #
    #     if filename:
    #         if filename.lower() == "auto":
    #             if dim == 0:
    #                 filename = "Mueller_0D_{}".format(datetime.date.today())
    #             else:
    #                 filename = "Mueller_{}".format(datetime.date.today())
    #         np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())
    #
    #     if save_folder:
    #         os.chdir(old_path)
    #
    #
    #
    #     # if downscale:
    #     #     image =  Self.camera.Get_Image(is_background=False,rest_background=True,draw=False)
    #     #     res = cv2.resize(image,dsize=(100,100),interpolation=cv2.INTER_LANCZOS4))
    #     # Print
    #     if verbose:
    #         if dim == 0:
    #             M.name = filename or "Sample"
    #             print("The Mueller matrix is:\n", M)
    #             M.analysis.decompose_polar(tol=1e-8, verbose=True)
    #             if is_ref:
    #                 error = np.linalg.norm(M.M[:,:,0] - Mref)/(16 * Mref[0,0])
    #                 print("The error is:   ", error)
    #         else:
    #             M.parameters.components(draw=True)
    #     return M

    # def Measure_0D_Mueller_Matrix(self, I=None, angles="perfect", Nmeasures=144, 
    #                                is_ref=False, save_folder=None, filename="Sample", 
    #                                add_angle_0=False, rotation = None, verbose=True):
            
    #         """Function to measure the Mueller matrix in 0D.
            
    #         Args:
    #             I (np.ndarray or None): Nx1 array of measured intensities. If None, the intensities are measured. Default: None.
    #             angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'perfect'.
    #                 "perfect": PSG and PSA states distributed in the poincare sphere as a regular solid. Algorithm calculates the nearest lower number of measurements to match two regular solids.
    #                 "spiral": PSG and PSA states distributed in the poincare sphere as a spiral with the same number of states in each case. N will be reduced to a number in the form of M^2 where M is an integer.
    #                 "random": Random angles.
    #                 'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
    #             Nmeasures (int): Number of measurements used (when I is None). Default: 144.
    #             is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
    #             save_folder (str or None): If not None, it specifies the folder where the measurement will be saved. Default: None.
    #             filename (str or None): If not None, the filename where the measurement is stored. Default: None.
    #             add_angle_0 (bool): If True, add original angles in the motor positions. Default: False
    #             rotation (np.ndarray or None): If not None, add an array [lat,lon]. Default: None.
    #             verbose (bool): If True, the information is printed after performing the measurement. Default: True.

    #         Returns:
    #             result (Mueller): Measured Mueller matrix.
    #         """
            
    #         if not is_ref and self.norm == 1:
    #             print("WARNING: Normalization not performed")

    #         # Make measurement
    #         if I is None:
    #             # Generate angles
    #             angles = calculate_polarimetry_angles(Nmeasures, type=angles)
                
    #             if rotation is not None:
    #                 if len(rotation)>1:
    #                     angles_new = np.asarray(angles)
    #                     PSG_az,PSG_el = PSG_angles_2_states(angles[:,0],angles[:,1])
    #                     PSA_az,PSA_el = PSA_angles_2_states(angles[:,3],angles[:,2])
    #                     PSG_az_new,PSG_el_new = Rotation_Poincare(PSG_az,PSG_el,rotation[0])
    #                     PSA_az_new,PSA_el_new = Rotation_Poincare(PSA_az,PSA_el,rotation[1])
    #                     angles_new[:,0],angles_new[:,1] = PSG_states_2_angles(PSG_az_new,PSG_el_new)
    #                     angles_new[:,3],angles_new[:,2] = PSA_states_2_angles(PSA_az_new,PSA_el_new)
    #             else:
    #                 print("Add a correct array: [rot_x, rot_y, rot_z]")
                    
    #             I = np.zeros(Nmeasures)
    #             # Measure
    #             angles_def = np.zeros((Nmeasures, 4))
    #             # Loop in measurements
    #             for ind, angles_m in enumerate(angles): 
                                        
    #                 if add_angle_0:
    #                     angles_m = angles_m + self.angles_0
    #                 angles_def[ind, :] = angles_m
                    
    #                 angles_aux = self.motor.Move_Absolute(pos=angles_m, waiting='busy', units="rad")

    #                 angles_def[ind] = angles_aux 
    #                 I[ind] = self.daca.Get_Signal()
                    
    #                 utils.percentage_complete(ind + 1, Nmeasures)
                    
    #             Inorm = I
                
    #         else:
                
    #             angles_def = angles
    #             Inorm = I

    #         # Calculate Mueller matrix
    #         system = [self.S, self.P1, self.R1, self.R2, self.P2]
            
    #         M,_ = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=False)
            
    #         # If reference, calculate the norm
    #         if is_ref:
    #             self.norm = M.M[0,0,0]
                
    #         if not is_ref: 
    #             M = M / self.norm
            
    #         # Save file
    #         if save_folder:
    #             old_path = os.getcwd()
    #             os.cd(save_folder)

    #         if filename:
    #             np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

    #         if save_folder:
    #             os.chdir(old_path)

    #         if verbose:
    #             M.name = filename or "Sample"
    #             print("The Mueller matrix is:\n", M)
    #             M.analysis.decompose_polar(tol=1e-8, verbose=True)
                    
    #         return M
    

    def Measure_ND_Mueller_Matrix(self, I=None, angles="perfect", Nmeasures=144, 
                                   dim=0, is_ref=False, filename="Sample", 
                                   add_angle_0=False, rotation = None, filter = False, verbose=True):
            
            """New function to measure the Mueller matrix 
            
            Args:
                I (np.ndarray or None) If None, the intensities are measured. Default: None.
                angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'perfect'.
                    "perfect": PSG and PSA states distributed in the poincare sphere as a regular solid. Algorithm calculates the nearest lower number of measurements to match two regular solids.
                    "spiral": PSG and PSA states distributed in the poincare sphere as a spiral with the same number of states in each case. N will be reduced to a number in the form of M^2 where M is an integer.
                    "random": Random angles.
                    'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
                Nmeasures (int): Number of measurements used (when I is None). Default: 144.
                dim (int): Measurement dimensions (0 - photodetector, 2 - camera). Default: 0.
                is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
                filename (str or None): If not None, the filename where the measurement is stored. Default: None.
                add_angle_0 (bool): If True, add original angles in the motor positions. Default: False
                rotation (1x3 np.ndarray): Measurement rotation angles. Default: None
                filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.
                verbose (bool): If True, the information is printed after performing the measurement. Default: True.

            Returns:
                result (Mueller): Measured Mueller matrix.
            """
            
            # Normalization not performed
            if not is_ref and self.norm == 1:
                print("WARNING: Normalization not performed")

            # Make measurement
            if I is None:
                
                # Generate angles
                angles = calculate_polarimetry_angles(Nmeasures, type=angles, rotation=rotation)

                # Resolution of camera
                if dim == 0:
                    I = np.zeros(Nmeasures)
                elif dim == 2: 
                    width, height, _, _ = self.camera.Get_ROI()
                    Resolution=[height, width]
                    I = np.zeros(shape=[Nmeasures]+Resolution)

                # Measurement

                angles_def = np.zeros((Nmeasures, 4))

                for ind, angles_m in enumerate(angles): 
                                        
                    if add_angle_0:
                        angles_m = angles_m + self.angles_0
                    
                    angles_def[ind] = self.motor.Move_Absolute(pos=angles_m, waiting='busy', units="rad")

                    if dim == 0:
                            I[ind] = self.daca.Get_Signal()
                    else:
                        I[ind,:,:] = self.camera.Get_Image()
                    
                    utils.percentage_complete(ind + 1, Nmeasures)
                
                # Correccion no 0
                if dim > 0:
                    I[I == 0] = 1    

                Inorm = I
                
            else:                
                angles_def = angles
                Inorm = I

            # Calculate Mueller matrix
            system = [self.S, self.P1, self.R1, self.R2, self.P2]
            
            M,_ = Calculate_Mueller(I=Inorm, angles=angles_def, system=system, filter=False)
            
            if is_ref:
                if dim == 2:
                    # Normalize
                    Mref =  np.identity(4)            
                    self.norm = np.mean(M.M[0,0,:]) / Mref[0,0]
                elif dim == 0:
                    Mref = np.identity(4)
                    self.norm = M.M[0,0,0]
                
            if not is_ref: 
                M = M / self.norm
            
            # Filter physical conditions
            if filter:
                M.analysis.filter_physical_conditions()

            if filename:
                if filename.lower() == "auto":
                    if dim == 0:
                        filename = "Mueller_0D_{}".format(datetime.date.today())
                    else:
                        filename = "Mueller_{}".format(datetime.date.today())
            np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

            if verbose:
                if dim == 0:
                    M.name = filename or "Sample"
                    print("The Mueller matrix is:\n", M)
                    M.analysis.decompose_polar(verbose=True)
                else:
                    M.parameters.components(draw=True)
        
            return M

