import numpy as np
import os
import datetime
from tqdm import tqdm
import matplotlib.pyplot as plt
import time

from py_pol.mueller import Mueller, Stokes, degrees

from py_lab.motor import Motor_Multiple
from py_lab.daca import DACA
from py_lab.camera import Camera
from py_lab.utils import sort_positions,PSA_states_2_angles,PSG_states_2_angles,PSG_angles_2_states
from py_lab.config import degrees, CONF_INTEL, CONF_T7, CONF_POLARIMETER_633


"""
Posibles fuentes de error del experimento:
- Los polarizadores y retardadores no son perfectos, calculamos el estado generado con sus matrices reales y no las ideales. OK
- Calcular el azimuth, elipticidad y grado de despolarización del haz de salida y comparar con el generado.
- Comprobar la posición de los motores con los ángulos a los que se tiene que mover.
- Hacer varias medidas con los fotodiodos (N_averange) -- Puede que reduzca el error porque las medidas no son estables. OK

"""

class Stokes_lens(object):
    """Class for Stokes lens. It will include control of the rotary motors of PSG and PSA elements, the data acquisition card for measuring photodiode signal (bulk configuation) and a camera (spatial resolution configuration).

    Args:
        calibration (bool): Load default calibration. Default: True.
        use_daca (bool): If True, measurement signal is get through DACA (photodiode). If False, the camera is used instead. Default: True.

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        camera (Camera): Camera object.
        daca (DACA): Data acquisition card object.
    """

    def __init__(self, calibration=True, use_daca=True):

        self.motor = Motor_Multiple(name="InteliDrives", N=4)
        self.daca = DACA(name="T7")

        if use_daca is True:
            self.camera = None
        else:
           self.camera = Camera(name="ImagingSource2", M=1, wavelength=None)
        self.norm = 1
        self.I_Distribution = None

        # Load calibration info
        if calibration:
            self.Load_Calibration(filename=CONF_POLARIMETER_633['cal_file'], BS_ref=CONF_POLARIMETER_633['BS_ref'], BS_trans=CONF_POLARIMETER_633['BS_trans'], ref_mirror=CONF_POLARIMETER_633['ref_mirror'], folder=CONF_POLARIMETER_633["cal_folder"])

        # Just in case some default data must be stored
        else:
            self.angles_0 = np.zeros(4)
            self.cal_dict = {}
            self.S = None
            self.P1 = None
            self.R1 = None
            self.R2 = None
            self.P2 = None
            self.BS_ref_inv = None
            self.BS_ref = None
            self.BS_trans = None
            self.BS_trans_inv = None
            self.ref_mirror = None


    def Load_Calibration(self, filename=None, BS_ref=None, BS_trans=None, ref_mirror=None, folder=None):
        """Load some or all calibration files. If any of the filename parameters is None, that calibration will be ignored.

        Args:
            filename (str or None): Filename of the main calibration. Default: None.
            BS_ref (str or None): Filename of the beam splitter in reflection configuration. Default: None.
            BS_trans (str or None): Filename of the main calibration. Default: None.
            ref_mirror (str or None): Filename of the main calibration. Default: None.
            folder (str or None): Filename of the main calibration. Default: None.
        """

        # Go to calibration folder
        if folder:
            try:
                old_folder = os.getcwd()
                os.chdir(folder)
            except:
                print("Folder {} nonexistent".format(folder))

        # Main calibration
        if filename:
            try:
                data = np.load(filename)
                #for key in data.keys():
                    #print(key, data[key], data[key]/degrees)

                self.angles_0 = np.array([data["P1_az"], data["R1_az"], data["R2_az"], data["P2_az"]])
                self.S = Stokes().general_azimuth_ellipticity(azimuth=data["illum_az"], ellipticity=data["illum_el"], degree_pol=data["illum_pol_degree"])
                self.P1 = Mueller().diattenuator_retarder_linear(p1=data["P1_p1"], p2=data["P1_p2"], R=data["P1_R"], azimuth=-data["P1_az"])
                self.R1 = Mueller().diattenuator_retarder_linear(p1=data["R1_p1"], p2=data["R1_p2"], R=data["R1_R"], azimuth=-data["R1_az"])
                self.R2 = Mueller().diattenuator_retarder_linear(p1=data["R2_p1"], p2=data["R2_p2"], R=data["R2_R"], azimuth=-data["R2_az"])
                self.P2 = Mueller().diattenuator_linear(p1=data["P2_p1"], p2=data["P2_p2"], azimuth=-data["P2_az"])

                self.cal_dict = data

            except:
                print("File {} nonexistent".format(filename))

        # Reflection configuration
        if BS_ref:
            try:
                data = np.load(BS_ref)
                comp = data["Mcomp"]
                self.BS_ref = Mueller().from_components(comp)
                self.BS_ref_inv = self.BS_ref.inverse(keep=True)
            except:
                self.BS_ref_inv = None
                self.BS_ref = None
                print("File {} nonexistent".format(BS_ref))

        if BS_trans:
            try:
                data = np.load(BS_trans)
                comp = data["Mcomp"]
                self.BS_trans = Mueller().from_components(comp)
                self.BS_trans_inv = self.BS_trans.inverse(keep=True)
            except:
                self.BS_trans_inv = None
                self.BS_trans = None
                print("File {} nonexistent".format(BS_trans))

        if ref_mirror:
            try:
                data = np.load(ref_mirror)
                comp = data["Mcomp"]
                self.ref_mirror = Mueller().from_components(comp)
            except:
                print("File {} nonexistent".format(ref_mirror))

        if folder:
            try:
                os.chdir(old_folder)
            except:
                print("Unable to return to original folder")

        #return self.BS_ref_inv, self.BS_trans_inv


    def Open(self):
        self.motor.Open(port='COM3', axis=[0,1,2,3])
        #self.motor.Home()
        self.daca.Open(AIN=CONF_T7["AIN"], AIN_ref=CONF_T7["AIN_ref"])
        if self.camera is not None:
            self.camera.Open()
            ## Parámetros de la Cámara
            self.camera.Set_Property(name="Resolution", value="Y800 (1920x1080)")   # Cambiar la resolucion a una manejable para el video en vivo
            self.camera.Set_Property_Auto(name="Exposure", value=7)                     # Cambiar el tiempo de exposicion a manual
            self.camera.Set_Property(name="Gain", value=2)                       # Cambiar la ganancia a un valor fijo
            #self.camera.Set_Property_Auto(name="Exposure", value=0)
            #self.camera.Set_Property(name="Exposure", value=0.5)
            #self.camera.Set_Property_Auto(name="Gain", value=0)
            #self.camera.Set_Property(name="Framerate", value=3)
            #self.camera.Set_Property_Auto(name="Resolution", value=0)
            #self.camera.Set_Property(name="Resolution", value=[2560,1920])

        # Origin angles reference TODO: NOT WORKING
        # self.motor.Clear_Reference()
        # self.motor.Move_Absolute(self.angles_0, units="rad")
        # self.motor.Set_Reference()

    # def Open_Camera(self):
    #     self.camera.Open()

    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=self.angles_0, units="rad")




    def Close(self):
        """Close all devices."""
        self.motor.Close()
        if self.camera is not None:
            self.camera.Close()



    def analizador_polarizacion(self,N,Mp1,Mr1,Silum,kind="ideal",normalize=True,filename=None,motor_position=False):
        """""
        Once polarization states have been created with the state generator, we experimentally determine the polarization of the output beam.


        Args:
            N (int): Root of the number of generated states. The total number of generated states will be N**2.
            Mr1
            kind (str):
            filename (str or None): If not None, stored generated Stokes vector. Default: None.
            normalize (bool): If True, real stokes vector generated is normalized. Default: True
            motor_position (bool): If True, motors position are printed. Default: False.

        Return:
            I (np.ndarray): Analized state intensities of each Stokes lens sector.
            S_G (Stokes): Stokes vector of each generated state.

        """""

        azimuth_G = np.linspace(0,179.8*degrees,N)
        ellipticity_G = np.linspace(-45*degrees,45*degrees,N)
        mesh = np.meshgrid(azimuth_G,ellipticity_G) #(N,N)
        P1, Q1 = PSG_states_2_angles(mesh[0],mesh[1]) #Matrices (N,N)

        if kind == "ideal":
            S_G = Stokes().general_azimuth_ellipticity(mesh[0],mesh[1])

        elif kind == "real":
            # Calcular estado real generado
            Mp1_rot = Mp1.rotate(P1, keep=True)
            Mr1_rot = Mr1.rotate(Q1, keep=True)
            S_G = Mr1_rot * Mp1_rot * Silum
            # Normalizar
            if normalize:
                S_G.normalize()

        #Estados de polarización de los 6 sectores
        azimuth_A = np.array([0, 1.57079633,0.78539816, 2.35619449,1.57079633,1.57079633])
        ellipticity_A = np.array([0,0,0,0,45*degrees,-45*degrees])
        #Pasamos de estados a ángulos de giro
        P2,Q2 = PSA_states_2_angles(azimuth_A,ellipticity_A)


        I = np.zeros((N,N,6))
        for azimuth_G in tqdm(range(N),desc="Proceso"):
            for ellip_G in range(0,N):
                for ind_A in range(0,6):
                    angles = np.zeros(4)
                    angles[:] = [P1[azimuth_G,ellip_G],Q1[azimuth_G,ellip_G],Q2[ind_A],P2[ind_A]]
                    angles = angles[:] + np.array([55,82,157,135]) * degrees #Sumamos los ángulos de cero.
                    self.motor.Move_Absolute(angles, units="rad",verbose=motor_position)
                    I[azimuth_G,ellip_G,ind_A] = self.daca.Get_Signal(use_ref=True, rest_background=True, verbose=False, return_ref=False,Naverage=20,Twait=0.1)

        if filename:
            np.savez(filename, S=S_G.M,date=datetime.date.today())

        return I,S_G


    def analizador_polarizacion_backup(N,filename=None):
        """""
        Once polarization states have been created with the state generator, we experimentally determine the polarization of the output beam.


        Args:
            N (int): Root of the number of generated states. The total number of generated states will be N**2.
            filename (str or None): If not None, stored generated Stokes vector. Default: None.

        Return:
            I (np.ndarray): Analized state intensities of each Stokes lens sector.
            S_G (Stokes): Stokes vector of each generated state.

        """""

        azimuth_G = np.linspace(0,179.8*degrees,N)
        ellipticity_G = np.linspace(-45*degrees,45*degrees,N)
        mesh = np.meshgrid(azimuth_G,ellipticity_G) #(N,N)
        S_G = Stokes().general_azimuth_ellipticity(mesh[0],mesh[1])


        P1, Q1 = PSG_states_2_angles(mesh[0],mesh[1]) #Matrices (N,N)

        #Estados de polarización de los 6 sectores
        azimuth_A = np.array([0, 1.57079633,0.78539816, 2.35619449,1.57079633,1.57079633])
        ellipticity_A = np.array([0,0,0,0,45*degrees,-45*degrees])
        #Pasamos de estados a ángulos de giro
        P2,Q2 = PSA_states_2_angles(azimuth_A,ellipticity_A)


        I = np.zeros((N,N,6))
        for azimuth_G in tqdm(range(N),desc="Proceso"):
            for ellip_G in range(0,N):
                for ind_A in range(0,6):
                    angles = np.zeros(4)
                    angles[:] = [P1[azimuth_G,ellip_G],Q1[azimuth_G,ellip_G],Q2[ind_A],P2[ind_A]]
                    angles = angles[:] + np.array([55,82,157,135]) * degrees #Sumamos los ángulos de cero.
                    pol.motor.Move_Absolute(angles, units="rad")
                    I[azimuth_G,ellip_G,ind_A] = pol.daca.Get_Signal(use_ref=True, rest_background=True, verbose=False, return_ref=False)

        if filename:
            np.savez(filename, S=S_G.M,date=datetime.date.today())

        return I,S_G



    def intensities_2_stokes(self,I,norm=True,filter_physical=True,filename=None):
        """""
        Transforms intensities of each Stokes lens sector to its Stokes vector.

        Args:
            I(np.ndarray): Intensities of each Stokes lens sector.
            norm (bool): If True, normalize Stokes vector. Default: True.
            filter_physical (True): If True, filters experimental errors by forcing the Stokes vector to fulfill the conditions necessary for a vector to be real light. Default: True.
            filename (str or None): If not None, stored analized Stokes vector. Default: None.

        Return:
            S_A(Stokes): Stokes vector of each analized state.

        """""
        S = np.zeros((4,len(I[0]),len(I[1])))
        for azimuth_A in range(len(I[0])):
            for ellip_A in range(len(I[1])):

                S0 = (I[azimuth_A,ellip_A,0] + I[azimuth_A,ellip_A,1]+ I[azimuth_A,ellip_A,2] + I[azimuth_A,ellip_A,3] + I[azimuth_A,ellip_A,4] + I[azimuth_A,ellip_A,5])/3
                S1 = I[azimuth_A,ellip_A,0] - I[azimuth_A,ellip_A,1]
                S2 = I[azimuth_A,ellip_A,2] - I[azimuth_A,ellip_A,3]
                S3 = I[azimuth_A,ellip_A,4] - I[azimuth_A,ellip_A,5]

                S[:,azimuth_A,ellip_A] = [S0,S1,S2,S3]

        S_A = Stokes().from_matrix(S)

        if norm:
            S_A.normalize()

        if filter_physical:
            S_A.analysis.filter_physical_conditions(tol=0)

        if filename:
            np.savez(filename, S=S_A.M, Intensities=I, date=datetime.date.today())

        return S_A

    def intensities_2_stokes_backup(I,norm=True,filter_physical=True,filename=None):
        """""
        Transforms intensities of each Stokes lens sector to its Stokes vector.

        Args:
            I(np.ndarray): Intensities of each Stokes lens sector.
            norm (bool): If True, normalize Stokes vector. Default: True.
            filter_physical (True): If True, filters experimental errors by forcing the Stokes vector to fulfill the conditions necessary for a vector to be real light. Default: True.
            filename (str or None): If not None, stored analized Stokes vector. Default: None.

        Return:
            S_A(Stokes): Stokes vector of each analized state.

        """""
        S = np.zeros((4,len(I[0]),len(I[1])))
        for azimuth_A in range(len(I[0])):
            for ellip_A in range(len(I[1])):

                S0 = (I[azimuth_A,ellip_A,0] + I[azimuth_A,ellip_A,1]+ I[azimuth_A,ellip_A,2] + I[azimuth_A,ellip_A,3] + I[azimuth_A,ellip_A,4] + I[azimuth_A,ellip_A,5])/3
                S1 = I[azimuth_A,ellip_A,0] - I[azimuth_A,ellip_A,1]
                S2 = I[azimuth_A,ellip_A,2] - I[azimuth_A,ellip_A,3]
                S3 = I[azimuth_A,ellip_A,4] - I[azimuth_A,ellip_A,5]

                S[:,azimuth_A,ellip_A] = [S0,S1,S2,S3]

        S_A = Stokes().from_matrix(S)

        if norm:
            S_A.normalize()

        if filter_physical:
            S_A.analysis.filter_physical_conditions(tol=0)

        if filename:
            np.savez(filename, S=S_A.M, Intensities=I, date=datetime.date.today())

        return S_A


    def compute_error(self,S_G,S_A,percentage=True):
        """
        Calculate the error between generated and analized state.

        Args:
            S_G (Stokes vector): Stokes vector of generated state.
            S_A (Stokes vector): Stokes vector of analized vector.

        Returns:
            error (np.ndarray): Diference between generated and analized state

        """
        if percentage:
            error = np.linalg.norm((S_G.M - S_A.M),axis=0)*100

        else:
            error = np.linalg.norm((S_G.M - S_A.M),axis=0)

        return error


    def analizador_polarizacion_device(self,N,N_averange,Mp1,Mr1,Silum,kind="ideal",normalize=True,filename=None,motor_position=False):
        """""
        Once polarization states have been created with the state generator, we experimentally determine the polarization of the output beam.


        Args:
            N (int): Root of the number of generated states. The total number of generated states will be N**2.
            Mr1
            kind (str):
            filename (str or None): If not None, stored generated Stokes vector. Default: None.
            normalize (bool): If True, real stokes vector generated is normalized. Default: True
            motor_position (bool): If True, motors position are printed. Default: False.

        Return:
            I (np.ndarray): Analized state intensities of each Stokes lens sector.
            S_G (Stokes): Stokes vector of each generated state.

        """""

        list_azimuth_G = np.linspace(0,179.8*degrees,N)
        list_ellipticity_G = np.linspace(-45*degrees,45*degrees,N)
        mesh = np.meshgrid(list_azimuth_G,list_ellipticity_G) #(N,N)
        P1, Q1 = PSG_states_2_angles(mesh[0],mesh[1]) #Matrices (N,N)
        #print(P1,Q1)

        if kind == "ideal":
            S_G = Stokes().general_azimuth_ellipticity(mesh[0],mesh[1])

        elif kind == "real":
            # Calcular estado real generado
            Mp1_rot = Mp1.rotate(P1, keep=True)
            Mr1_rot = Mr1.rotate(Q1, keep=True)
            S_G = Mr1_rot * Mp1_rot * Silum
            # Normalizar
            if normalize:
                S_G.normalize()

        for azimuth_G in tqdm(range(N),desc="Proceso"):
            for ellip_G in range(0,N):
                angles = np.zeros(4)
                angles[:] = [P1[azimuth_G,ellip_G],Q1[azimuth_G,ellip_G],0,0]
                angles = angles[:] + np.array([55,82,0,0]) * degrees #Sumamos los ángulos de cero.
                self.motor.Move_Absolute(angles, units="rad",verbose=motor_position)
                SG = PSG_angles_2_states(P1[azimuth_G,ellip_G],Q1[azimuth_G,ellip_G],output='stokes')
                for i in range(N_averange):
                    image = self.camera.Get_Image(draw=False)
                    image_cut = image[300:800,700:1200]
                    #plt.imshow(image_cut)
                    time.sleep(2)
                    np.savez('Measure' + str(azimuth_G) + str(ellip_G) + str(i) + '.npz',image=image_cut,S_G=SG.M)

        if filename:
            np.savez(filename, S=S_G.M,date=datetime.date.today())


        return list_azimuth_G,list_ellipticity_G,S_G


def calculate_Wi(matrices, order=None):
    """Function that calculates the Wi matrix respect to the measured Mueller matrices.

    Args:
        matrices (iterable of Mueller objects): List of Mueller matrices.
        order (iterable of ints or None): If not None, list of indices representing the order of the elements of the Stokes sectors.

    Returns:
        result (np.ndarray): Array of the Wi matrix."""
    # Get size and default order
    N = len(matrices)
    if order is None:
        order = range(N)

    # Calculate matrix
    W = np.zeros((N, 4))
    for ind, O in enumerate(order):
        M = matrices[O]
        W[ind, :] = M.M[0,:,0]

    # Calculate inverse
    if N == 4:
        Wi = np.linalg.norm(W)
    else:
        Wt = W.T
        Wi = np.linalg.inv(Wt @ W) @ Wt

    return Wi
