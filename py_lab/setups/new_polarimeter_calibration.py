# MODULOS GENERALES
import datetime
import os
import time
# MÓDULOS PYTHON 
import numpy as np                                          # Numpy
import matplotlib.pyplot as plt                             # Matplotlib
from pyswarms.single.global_best import GlobalBestPSO       # PSO
from scipy.optimize import least_squares                    # Least squares
# MÓDULOS PY-POL
from py_pol.mueller import Mueller                          # Mueller
from py_pol.stokes import Stokes                            # Stokes
# MÓDULOS PYLAB
import py_lab.utils as utils                                # Utils py-lab
from py_lab.config import degrees, CONF_DT_50, CONF_U6      # Configurations
from py_lab.setups.polarimeter_utils import Calculate_Mueller, Msystem_From_Dict, Calculate_Mueller_Matrix_0D

# PARÁMETROS PSO 
options_individual = {'c1': 0.5, 'c2': 0.3, 'w':0.9}
options_global = {'c1': 0.5, 'c2': 0.3, 'w':0.9}
PSO_part_global = 25
PSO_iter_global = 20
PSO_cores_global = None
tol = 1e-15

sign_turn = 1    # Due to motor 2 and 3 rotating counter-clockwise. Fix in the future

# DEFINICIONES DE AJUSTES DE FUNCIONES

def model_cos2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2 
    return Imodel


def error_cos2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2(par, angle)
    dif = Iexp - Imodel
    return dif



def model_cos2_2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(2*(angle - par[2]))**2
    return Imodel


def error_cos2_2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2_2(par, angle)
    dif = Iexp - Imodel
    return dif

# DEFINICIONES DE PSO

def opt_func_PSO_ind(Transitions, fun, args):
    """Function needed as interface between PSO algorithm and error_global."""
    num_particles = Transitions.shape[0]  # number of particles
    error = [np.linalg.norm(fun(Transitions[i,:], *args)) for i in range(num_particles)]
    return error

def opt_func_PSO(Transitions, cal_dict, single=False):
    """Function needed as interface between PSO algorithm and error_global."""
    num_particles = Transitions.shape[0]  # number of particles
    error = [error_global(Transitions[i,:], cal_dict=cal_dict, single=single) for i in range(num_particles)]
    return error


# DEFINICIONES DEL DICCIONARIO DE CALIBRACIÓN


def generate_initial_cal_dict(method="fixed", experiment=True):
    
    """Function to generate the initial cal_dict. It can be used both for the experiment and the fit part. It supports different methods."""
    
    # Experimental values
    cal_dict = {}
    if experiment:
        cal_dict["type"] = 'Experiment'
        cal_dict["N_measures_1D"] = 91
        cal_dict["max_angle_1D"] = 360
        cal_dict["angles_1"] = np.linspace(0, cal_dict["max_angle_1D"], cal_dict["N_measures_1D"])
        cal_dict['P_no_filter'] = 1  
        cal_dict['P_filter'] = 1  
    if method == "fixed":
        cal_dict["illum_pol_degree"] = 1
        cal_dict["illum_az"] = 0 * degrees
        cal_dict["illum_el_sign"] = 1
        cal_dict["illum_el_max"] = 45 * degrees
        cal_dict["illum_el"] = cal_dict["illum_el_sign"] * cal_dict["illum_el_max"]
        cal_dict["P1_p1"] = 1
        cal_dict["P1_p2"] = 0
        cal_dict["P1_az"] = 0 * degrees
        cal_dict["P2_p1"] = 1
        cal_dict["P2_p2"] = 0
        cal_dict["P2_az"] = 0 * degrees
        cal_dict["Pc_p1"] = 1
        cal_dict["Pc_p2"] = 0
        cal_dict["R1_p1"] = 1
        cal_dict["R1_p2"] = 1
        cal_dict["R1_az"] = 0 * degrees
        cal_dict["R1_Ret"] = 90 * degrees
        cal_dict["R2_p1"] = 1
        cal_dict["R2_p2"] = 1
        cal_dict["R2_az"] = 0 * degrees
        cal_dict["R2_Ret"] = 90 * degrees
        cal_dict["Rc_p1"] = 1
        cal_dict["Rc_p2"] = 1
        cal_dict["Rc_Ret"] = 90 * degrees
        cal_dict["Rc_offset"] = 0 * degrees
    else:
        azs = np.random.rand(7) * 180 * degrees
        signs = np.sign(np.random.rand(1) - 0.5)
        els = np.random.rand(1) * 5 * degrees + 40 * degrees
        p1s = np.random.rand(10) * 0.1 + 0.9
        p2s = np.random.rand(4) * 0.1
        rets = np.random.rand(3) * 20 * degrees + 80 * degrees

        cal_dict["illum_pol_degree"] = 1
        cal_dict["illum_az"] = azs[0]
        cal_dict["illum_el_max"] = els[0]
        cal_dict["illum_el_sign"] = signs[0]
        cal_dict["P1_p1"] = p1s[0]
        cal_dict["P1_p2"] = p2s[0]
        cal_dict["P1_az"] = azs[1]
        cal_dict["P1_R"] = np.random.rand(1)[0] * 360 * degrees
        cal_dict["P2_p1"] = p1s[1]
        cal_dict["P2_p2"] = p2s[1]
        cal_dict["P2_az"] = azs[3]
        cal_dict["P3_p1"] = p1s[2]
        cal_dict["P3_p2"] = p2s[2]
        cal_dict["R1_p1"] = p1s[4]
        cal_dict["R1_p2"] = p1s[5]
        cal_dict["R1_az"] = azs[5]
        cal_dict["R1_Ret"] = rets[0]
        cal_dict["R2_p1"] = p1s[6]
        cal_dict["R2_p2"] = p1s[7]
        cal_dict["R2_az"] = azs[6]
        cal_dict["R2_Ret"] = rets[1]
    return cal_dict

def Load_Cal_Files(folder=None, filename=None):
    
    """Function to generate the initial cal_dict from calibration step files."""
    
    # Ir al directorio
    if folder:
        old_folder = os.getcwd()
        os.chdir(folder)
    # If filename is defined, individual steps will be loaded
    if filename and filename != "Calibration_measurements.npz":
        cal_dict = generate_initial_cal_dict(method="fixed", experiment=False)
        data = np.load("Step_1c_" + filename + ".npz")
        cal_dict["S0"] = np.mean(data["Iexp"][:, 0] / data["Iexp"][:, 1])
        if sign_turn != 1:
            print("Rotation of motors corrected artificially")
        try:
            cal_dict['P_filter'] = data["P_filter"]
            cal_dict['P_no_filter'] = data["P_no_filter"]
        except:
            data = np.load("Calibration_measurements.npz")
            cal_dict['P_filter'] = data["P_filter"]
            cal_dict['P_no_filter'] = data["P_no_filter"]

        data = np.load("Step_2a_" + filename + ".npz")
        cal_dict["I_step_2a"] = data["Iexp"]
        cal_dict["Angles_step_2a"] = sign_turn * data["angles_1"]

        data = np.load("Step_2b_" + filename + ".npz")
        cal_dict["I_step_2b"] = data["Iexp"]
        cal_dict["Angles_step_2b"] = sign_turn * data["angles_1"]

        data = np.load("Step_2c_" + filename + ".npz")
        cal_dict["I_step_2c"] = data["Iexp"]
        cal_dict["Angles_step_2c"] = sign_turn * data["angles_1"]

        data = np.load("Step_3a_" + filename + ".npz")
        cal_dict["I_step_3a"] = data["Iexp"]
        cal_dict["Angles_step_3a"] = sign_turn * data["angles_1"]

        data = np.load("Step_3b_" + filename + ".npz")
        cal_dict["I_step_3b"] = data["Iexp"]
        cal_dict["Angles_step_3b"] = sign_turn * data["angles_1"]

        data = np.load("Step_4_" + filename + ".npz")
        cal_dict["I_step_4"] = data["Iexp"]
        cal_dict["Angles_step_4"] = sign_turn * data["angles_1"]

        data = np.load("Step_5a_" + filename + ".npz")
        cal_dict["I_step_5a"] = data["Iexp"]
        cal_dict["Angles_step_5a"] = sign_turn * data["angles_1"]

        data = np.load("Step_5b_" + filename + ".npz")
        cal_dict["I_step_5b"] = data["Iexp"]
        cal_dict["Angles_step_5b"] = sign_turn * data["angles_1"]

        data = np.load("Step_6_" + filename + ".npz")
        cal_dict["I_step_6"] = data["Iexp"]
        cal_dict["Angles_step_6"] = sign_turn * data["angles_1"]


        cal_dict["type"] = 'Experiment'
        cal_dict["N_measures_1D"] = 91
        cal_dict["max_angle_1D"] = 360
        cal_dict["angles_1"] = np.linspace(0, cal_dict["max_angle_1D"], cal_dict["N_measures_1D"])
    # If filename is None, use the general name
    else:
        data = np.load("Calibration_measurements.npz")
        cal_dict = dict(data)

    # Volver del directorio
    if folder and old_folder:
        os.chdir(old_folder)

    return cal_dict


def Print_Calibration_Parameters(cal_dict): 
    print("Parámetros de Calibración: ")

    print("Datos de la fuente: ")
    print("\tIntensidad: {:.2f}".format(cal_dict['S0']))
    print("\tAzimuth: {:.2f}°".format(cal_dict['illum_az']/degrees))
    print("\tElipticidad: {:.2f}°".format(cal_dict['illum_el']/degrees))
    print("\tGrado de polarización: {:.2f}".format(cal_dict['illum_pol_degree']))

    print("Datos de P1, P2 y Pc: ")
    print("\tP1:\tp1: {:.2f}\tp2: {:.2f}\taz: {:.2f}°".format(cal_dict['P1_p1'],cal_dict['P1_p2'],cal_dict['P1_az']/degrees))
    print("\tP2:\tp1: {:.2f}\tp2: {:.2f}\taz: {:.2f}°".format(cal_dict['P2_p1'],cal_dict['P2_p2'],cal_dict['P2_az']/degrees))
    print("\tPc:\tp1: {:.2f}\tp2: {:.2f}".format(cal_dict['Pc_p1'],cal_dict['Pc_p2']))
    
    print("Datos de R1, R2 y Rc: ")
    print("\tR1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R1_p1'],cal_dict['R1_p2'],cal_dict['R1_Ret']/degrees, cal_dict['R1_az']/degrees))
    print("\tR2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R2_p1'],cal_dict['R2_p2'],cal_dict['R2_Ret']/degrees, cal_dict['R2_az']/degrees))
    print("\tRc:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\toffset: {:.2f}°".format(cal_dict['Rc_p1'],cal_dict['Rc_p2'],cal_dict['Rc_Ret']/degrees, cal_dict['Rc_offset']/degrees))

    return

# DEFINICIONES DE LOS PASOS DE CLAIBRACIÓN DEL POLARÍMETRO

def make_step_0a(pol, angles_def=None, Iexp=None):
    
    """Step 0a: Cruzar los polarizadores, para luego encontrar los ejes de las láminas de cuarto de onda de ref."""

    # Get angles and powers
    if angles_def is None and Iexp is None:
        angles_def, Iexp = utils.Tomar_Medidas(pol.daca)
    else: 
        angles_def, Iexp = angles_def, Iexp
    # Fit
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0*degrees], [0.2,Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x

    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(angles_def, Iexp, Imodel, title='Step 0a')
    

    # Print results
    Imax = par1[1] + par1[0]
    el = np.arctan(par1[0] / Imax)
    par1[2] = np.round(par1[2]/degrees, 10) % 180
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(Imax))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Ellipt. laser : {:.1f} deg'.format(el/degrees))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]))
    print("\nThe correct angle for P_aux is {:.2f} deg".format(par1[2]+90))

    return angles_def, Iexp

def make_step_0b(pol, angles_def=None, Iexp=None):
    
    """Step 0b: Giro breve de la segunda lámina de cuarto de onda"""

    # Get angles and powers
    if angles_def is None or Iexp is None:
        angles_def, Iexp = utils.Tomar_Medidas(pol.daca)
    else: 
        angles_def, Iexp = angles_def, Iexp
    print(angles_def)
    print(Iexp)
    # Fit
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0*degrees], [0.3,Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    print(bounds)
    print(par0)
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x

    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(angles_def, Iexp, Imodel, title='Step 0b')
    
    # Print results
    Imax = par1[1] + par1[0]
    el = np.arctan(par1[0] / Imax)
    par1[2] = np.round(par1[2]/degrees, 10) % 180
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(Imax))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Ellipt. laser : {:.1f} deg'.format(el/degrees))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]))
    print("\nThe correct angle for Q_aux is {:.2f} deg".format(par1[2]))
    
    return angles_def, Iexp

def make_step_1a(pol, angles_def=None, Iexp=None):
    
    """Step 1a: find angle of P0"""
    
    # Get angles and powers
    if angles_def is None or Iexp is None:
        angles_def, Iexp = utils.Tomar_Medidas(pol.daca)
    print(Iexp)

    # Fit
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180*degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x

    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 1a')
    # Print results
    Imax = par1[1] + par1[0]
    el = np.arctan(par1[0] / Imax)
    par1[2] = np.round(par1[2]/degrees, 10) % 180
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(Imax))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Ellipt. laser : {:.1f} deg'.format(el/degrees))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]))
    print("\nThe correct angle for P0 is {:.2f} deg".format(par1[2]))

    return angles_def, Iexp

def make_step_1b_1(pol, num_data=91, max_angle=360, motor_num=3, Iexp = None, angles = None):
    
    """First part of step 1b: Polarizador auxiliar cruzado con P0"""
    # Create array of angles that will be used
    var_angle = np.linspace(0, max_angle, num_data) * degrees
    # Initialize data
    angles_def = np.zeros(num_data)
    Iexp = np.zeros(num_data)
    if pol is None: 
        Iexp = Iexp
        angles_def = angles
        plt.plot(Iexp)
    else:
    # Make the loop
        utils.percentage_complete()
        for ind, angle in enumerate(var_angle):
            angles_def[ind] = pol.motor.Move_Absolute(pos=angle, units="rad", axis=motor_num)[motor_num] 
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind + 1, num_data)
        
    # Fit to the function
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    print(bounds)
    print(par0)
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(angles_def, Iexp, Imodel, title='Step 1b part 1')
    angle_P = (par1[2] + 90*degrees)
    # Print results
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
    print("\nThe correct angle for P_aux is {:.2f} deg".format(angle_P/degrees))

    return angle_P, Iexp, angles_def

def make_step_1b_2(pol, angle_P=None, motor_num=3, units="deg", angles_def=None, Iexp=None):
    
    """Second part of step 1b_2: find angle of Q0"""
    
    # Get angles and powers
    if angles_def is None or Iexp is None:
        pol.motor.Move_Absolute(pos=angle_P, units=units, axis=motor_num)[motor_num]
        angles_def, Iexp = utils.Tomar_Medidas(pol.daca)
    print(Iexp)
    print(angles_def)
    # Fit
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    result = least_squares(error_cos2_2, par0, args=(angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    
    # Plot result
    Imodel = model_cos2_2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 1b part 2')
    # Print results
    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Maximum angle : {:.2f} deg or {:.2f} deg'.format(par1[2]/degrees, par1[2]/degrees + 90))
    print("\nThe correct angle for Q0 is {:.2f} deg".format(par1[2]/degrees))

    return angles_def, Iexp

def make_step_1b_3(pol, num_data=91, max_angle=360, motor_num=3):
    
    """Third part of step 1b: We make a loop with one motor with a polarizer to check the circularity of the illumination"""
    
    # Create array of angles that will be used
    var_angle = np.linspace(0, max_angle, num_data) * degrees
    # Initialize data
    angles_def = np.zeros(num_data)
    Iexp = np.zeros(num_data)

    # Make the loop
    utils.percentage_complete()
    for ind, angle in enumerate(var_angle):
        angles_def[ind] = pol.motor.Move_Absolute(pos=angle, units="rad", axis=motor_num)[motor_num]
        Iexp[ind] = pol.daca.Get_Signal()
        utils.percentage_complete(ind + 1, num_data)

    # Fit to the function
    print(Iexp)
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    # Plot result
    Imodel = model_cos2(par1, angles_def)
    utils.plot_experiment_residuals_1D(
        angles_def, Iexp, Imodel, title='Step 1b part 3')
    # Print results
    Imax = par1[1] + par1[0]
    el = np.arctan(par1[0] / Imax)

    print('The values obtained are:')
    print('   - Imax       : {:.4f} V'.format(Imax))
    print('   - Imin       : {:.4f} V'.format(par1[0]))
    print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
    print('   - Ellipt. fuente : {:.1f} deg'.format(el/degrees))

    return el/degrees

def make_step_1c(pol, cal_dict):
    
    """Function to make step 1c: photodetector stability."""
    
    # Make the measurement
    Iexp = np.zeros([cal_dict["Nmeasurements"], 2])
    utils.percentage_complete()
    for ind in range(cal_dict["Nmeasurements"]):
        Iexp[ind, :] = pol.daca.Get_Signal(return_ref=True, use_ref=False)
        time.sleep(cal_dict["Twait"])
        utils.percentage_complete(ind + 1, cal_dict["Nmeasurements"])

    # Correct if filter is present
    try:
        trans = cal_dict['P_filter'] / cal_dict['P_no_filter']
        Iexp[:, 0] /= trans
    except:
        cal_dict['P_filter'] = 1
        cal_dict['P_no_filter'] = 1

    # Make stadistics
    ratio_individual = Iexp[:, 0] / Iexp[:, 1]
    mean = np.mean(Iexp, axis=0)
    error = np.std(Iexp, axis=0)
    ratio2 = np.mean(ratio_individual)
    ratio_error2 = np.std(ratio_individual)
    error_correction = np.sqrt((error[0] / mean[0])**2 +
                               2 * (error[1] / mean[1])**2)
    # Fake plot data
    t = range(cal_dict["Nmeasurements"])
    meanCh1y = np.ones(cal_dict["Nmeasurements"]) * mean[0]
    meanCh1yUp = np.ones(cal_dict["Nmeasurements"]) * (mean[0] + error[0])
    meanCh1yDown = np.ones(cal_dict["Nmeasurements"]) * (mean[0] - error[0])
    meanCh2y = np.ones(cal_dict["Nmeasurements"]) * mean[1]
    meanCh2yUp = np.ones(cal_dict["Nmeasurements"]) * (mean[1] + error[1])
    meanCh2yDown = np.ones(cal_dict["Nmeasurements"]) * (mean[1] - error[1])
    meanRy2 = np.ones(cal_dict["Nmeasurements"]) * ratio2
    meanRyUp2 = np.ones(cal_dict["Nmeasurements"]) * (ratio2 + ratio_error2)
    meanRyDown2 = np.ones(cal_dict["Nmeasurements"]) * (ratio2 - ratio_error2)
    # Plot it
    plt.figure(figsize=(24, 4))
    plt.subplot(1, 3, 1)
    plt.plot(t, Iexp[:, 0] / meanCh1y, 'k')
    plt.plot(t, meanCh1y / meanCh1y, 'b')
    plt.plot(t, meanCh1yUp / meanCh1y, 'r--')
    plt.plot(t, meanCh1yDown / meanCh1y, 'r--')
    plt.title('Normalized PhD 1 (Signal)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 2)
    plt.plot(t, Iexp[:, 1] / meanCh2y, 'k')
    plt.plot(t, meanCh2y / meanCh2y, 'b')
    plt.plot(t, meanCh2yUp / meanCh2y, 'r--')
    plt.plot(t, meanCh2yDown / meanCh2y, 'r--')
    plt.title('Normalized PhD 2 (Reference)')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    plt.subplot(1, 3, 3)
    plt.plot(t, ratio_individual / meanRy2, 'k')
    plt.plot(t, meanRy2 / meanRy2, 'b')
    plt.plot(t, meanRyUp2 / meanRy2, 'r--')
    plt.plot(t, meanRyDown2 / meanRy2, 'r--')
    plt.title('Ratio PhD1 / PhD2')
    plt.xlabel('Time (s)')
    plt.ylabel('Intensity (V)')
    # Print result
    print('The resuts are:')
    print('   - Signal channel          : {:.4f} +- {:.4f} V.'.format(
        mean[0], error[0]))
    print('   - Reference channel       : {:.4f} +- {:.4f} V.'.format(
        mean[1], error[1]))
    print('   - Ratio error             : {:.2f} %.'.format(
        ratio_error2 * 100))
    print('   - Error in corrected I    : {:.2f} %.'.format(
        error_correction * 100))

    # Save data
    filename = "Step_1c_{}".format(datetime.date.today())
    np.savez(
        filename + '.npz',
        Nmeasurements=cal_dict["Nmeasurements"],
        Twait=cal_dict["Twait"],
        Iexp=Iexp,
        mean=mean,
        error=error,
        P_filter=cal_dict['P_filter'],
        P_no_filter=cal_dict['P_no_filter'],
        error_correction=error_correction,
        **CONF_DT_50, **CONF_U6)
    # Output
    return ratio2, ratio_error2

def make_step_2a(cal_dict, pol=None, verbose = True):

    
    """Function that performs step 2a of calibration: azimuth of P1."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_2a"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_2a"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([angle, 0, 0, 0])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind] = angles_aux[0] * degrees
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_2a"] = Iexp
        cal_dict["Angles_step_2a"] = Angles_def

    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    print(bounds)
    print(par0)
    result = least_squares(error_cos2, par0, args=(Angles_def, Iexp), bounds=bounds)
    par1 = result.x
    # Plot result
    if verbose: 
        Imodel = model_cos2(par1, Angles_def)
        utils.plot_experiment_residuals_1D(
        Angles_def, Iexp, Imodel, title='Step 2a')
    
    cal_dict['P1_az'] = (np.round(par1[2]/degrees,10) % 180)*degrees
    if verbose:
    # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for P1 is {:.2f} deg".format(cal_dict['P1_az']/degrees))

    # Save data
    if pol is not None:
        filename = "Step_2a_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)


    # Output
    return cal_dict

def make_step_2b(cal_dict, pol=None, verbose = True):
    
    """Function that performs step 2b of calibration: azimuth of P2."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_2b"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_2b"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([0, 0, 0, angle])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind] = angles_aux[3] * degrees
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_2b"] = Iexp
        cal_dict["Angles_step_2b"] = Angles_def

    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(Angles_def, Iexp), bounds=bounds)
    par1 = result.x
    # Plot result
    if verbose: 
        Imodel = model_cos2(par1, Angles_def)
        utils.plot_experiment_residuals_1D(
        Angles_def, Iexp, Imodel, title='Step 2b')
    
    cal_dict['P2_az'] = (np.round(par1[2]/degrees,10) % 180)*degrees
    if verbose:
    # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for P2 is {:.2f} deg".format(cal_dict['P2_az']/degrees))

    # Save data
    if pol is not None:
        filename = "Step_2b_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def make_step_2c(cal_dict, pol=None, verbose = True):
    
    
    """Function that performs step 2c of calibration: intensities of P1-P2."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_2c"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_2c"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([cal_dict["P1_az"]/degrees, 0, 0, angle])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="deg")
            Angles_def[ind] = angles_aux[3] * degrees
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_2c"] = Iexp
        cal_dict["Angles_step_2c"] = Angles_def

    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(Angles_def, Iexp), bounds=bounds)
    par1 = result.x
    # Plot result
    if verbose:
        Imodel = model_cos2(par1, Angles_def)
        utils.plot_experiment_residuals_1D(
        Angles_def, Iexp, Imodel, title='Step 2c')
    if verbose:
    # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for next part is {:.2f} deg".format(np.round(par1[2]/degrees,10)%180))

    # Save data
    if pol is not None:
        filename = "Step_2c_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def make_step_3a(cal_dict, pol=None, verbose = True):
    
    """Function that performs step 3a of calibration: azimuth of Q2."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_3a"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_3a"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([cal_dict['P1_az'], 0, angle*degrees, cal_dict['P2_az']])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="rad")
            Angles_def[ind] = angles_aux[2]
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_3a"] = Iexp
        cal_dict["Angles_step_3a"] = Angles_def

    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2_2, par0, args=(Angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    # Plot result
    if verbose: 
        Imodel = model_cos2_2(par1, Angles_def)
        utils.plot_experiment_residuals_1D(
        Angles_def, Iexp, Imodel, title='Step 3a')

    cal_dict['R2_az'] = (np.round(par1[2]/degrees,10)%90)*degrees
    #cal_dict['R2_az'] = par1[2]
    if verbose:
        # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for Q2 is {:.2f} deg".format(cal_dict['R2_az']/degrees))

    # Save data
    if pol is not None:
        filename = "Step_3a_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def make_step_3b(cal_dict, pol=None, verbose = True):
    
    """Function that performs step 3b of calibration: real azimuth of Q2."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_3b"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_3b"]
        
    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]*degrees + cal_dict['R2_az']):
            theta = np.array([cal_dict['P1_az'], 0, angle, cal_dict['P2_az']])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="rad")
            Angles_def[ind] = angles_aux[2]
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_3b"] = Iexp
        cal_dict["Angles_step_3b"] = Angles_def
    
    
    Angles_fit = cal_dict["angles_1"]*degrees

    
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    # bounds = ([0, 0, 0], [np.abs(Imax)*0.5, Imax, 180 * degrees])
    # par0 = [np.abs(Imin), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(Angles_fit, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    
    # Plot result
    if verbose:
        Imodel = model_cos2(par1, Angles_fit)
        utils.plot_experiment_residuals_1D(
        Angles_fit, Iexp, Imodel, title='Step 3b')

    if np.abs(par1[2]/degrees - 45) <= 20 and cal_dict['R2_az'] <= 90 * degrees:
        cal_dict['R2_az'] = cal_dict['R2_az'] + 90 * degrees
    if verbose:
        # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for Q2 is {:.2f} deg".format(cal_dict['R2_az']/degrees))

    # Save data
    if pol is not None:
        filename = "Step_3b_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def Calculate_Stokes(I, angles, system, filter = True):
    """Function to calculate the Mueller matrix from the intensity measurements.

    Args:
        I (np.array): First dimension is the corresponding to different measurements.
        angles (Nx2 np.array): Angles array.
        system (list) List with the Stokes and Mueller objects for illumination, R2 and P2.
        filter (bool): If True, the Mueller matrix is filtered to be physcally realizable. Default: True.

    Returns:
        result (Stokes): Result.
    """
    
    R2, P2 = system
    Mr2_rot = R2.rotate(angle=angles[1], keep=True)
    Mp2_rot = P2.rotate(angle=angles[0], keep=True)
    
    PSA = Mp2_rot * Mr2_rot
    # a = PSA.M[0,:,:].T
    # I = I[:, np.newaxis]
    # #a = a.T[:, :]
    # # print(I.shape)
    # s = np.linalg.lstsq(a, I, rcond=None)[0]
    
    # a = np.linalg.pinv(PSA.M[0,:,:].T)
    # s = np.dot(a,I)
    a = PSA.M[0,:,:].T
    
    at = a.T
    ai = np.linalg.inv(at @ a) @ at
    s = np.dot(ai,I[:, np.newaxis])

    S = Stokes()
    S.from_components(components=s)
    if filter: 
        S.analysis.filter_physical_conditions(tol=tol)

    return S


def make_step_4(cal_dict, pol=None, verbose = True):

    
    """Function that performs step 4 of calibration: illumination."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_4"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_4"]

    else:
        Iexp = np.zeros(20, dtype=float)
        Angles_def = utils.PSA_states_2_angles(S = utils.S_20)
        utils.percentage_complete()

        for ind in range(len(Angles_def[0])):
            theta = np.array((0, 0, Angles_def[1][ind]+cal_dict['R2_az'], Angles_def[0][ind] + cal_dict['P2_az']))
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="rad")
            #print(angles_aux)
            Angles_def[1][ind] = angles_aux[2]
            Angles_def[0][ind] = angles_aux[3]
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, 20)

        cal_dict["I_step_4"] = Iexp
        cal_dict["Angles_step_4"] = Angles_def

    
    
        
    Mp2 = Mueller().diattenuator_linear(p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])
    
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict["R2_az"])

    
    system = (Mr2, Mp2)
    
    

    S = Calculate_Stokes(I = Iexp, angles = Angles_def, system = system, filter = True)

    cal_dict["illum_az"] = S.parameters.azimuth(use_nan = False)
    cal_dict["illum_el"] = S.parameters.ellipticity_angle()
    cal_dict["illum_pol_degree"] = np.round(S.parameters.degree_polarization(),10)
    if verbose:
        print(S.parameters.azimuth(use_nan = False)/degrees)
        print(S.parameters.ellipticity_angle()/degrees)
        print(np.round(S.parameters.degree_polarization(),10))
        Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_4"][0], keep=True)
        Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_4"][1], keep=True)
        Sf = Mp2_rot * Mr2_rot * S
        I_4 = Sf.parameters.intensity()
        utils.plot_experiment_residuals_1D(np.linspace(1,20,20)*degrees, cal_dict["I_step_4"], I_4, title='Step 4')
        
    # Save data
    if pol is not None:
        filename = "Step_4_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def make_step_5a(cal_dict, pol=None, verbose = True):
    
    """Function that performs step 5a of calibration: azimuth of Q1."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_5a"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_5a"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()
        for ind, angle in enumerate(cal_dict["angles_1"]):
            theta = np.array([cal_dict['P1_az'], angle*degrees, 0, cal_dict['P2_az']])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="rad")
            Angles_def[ind] = angles_aux[1] 
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])

        cal_dict["I_step_5a"] = Iexp
        cal_dict["Angles_step_5a"] = Angles_def

    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2_2, par0, args=(Angles_def, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    # Plot result
    if verbose: 
        Imodel = model_cos2_2(par1, Angles_def)
        utils.plot_experiment_residuals_1D(
        Angles_def, Iexp, Imodel, title='Step 5a')

    cal_dict['R1_az'] = (np.round(par1[2]/degrees,10)%90)*degrees
    if verbose:
        # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for Q1 is {:.2f} deg".format(cal_dict['R1_az']/degrees))

    # Save data
    if pol is not None:
        filename = "Step_5a_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict

def make_step_5b(cal_dict, pol=None, verbose = True):
    
    """Function that performs step 3b of calibration: real azimuth of Q1."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_5b"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = cal_dict["Angles_step_5b"]

    else:
        Iexp = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        Angles_def = np.zeros(cal_dict["N_measures_1D"], dtype=float)
        utils.percentage_complete()

        for ind, angle in enumerate(cal_dict["angles_1"]*degrees + cal_dict['R1_az']):
            theta = np.array([cal_dict['P1_az'], angle, 0, cal_dict['P2_az']])
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="rad")
            Angles_def[ind] = angles_aux[1]
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, cal_dict["N_measures_1D"])
        cal_dict["I_step_5b"] = Iexp
        cal_dict["Angles_step_5b"] = Angles_def
    
    
    Angles_fit = cal_dict["angles_1"]*degrees

    
    (Imax, Imin) = (np.max(Iexp), np.min(Iexp))
    # bounds = ([0, 0, 0], [np.abs(Imax)*0.5, Imax, 180 * degrees])
    # par0 = [np.abs(Imin), Imax - np.abs(Imin), np.random.rand() * 180*degrees]
    bounds = ([0, 0, 0], [np.abs(Imin), Imax, 180 * degrees])
    par0 = [np.max((0,Imin)), Imax - Imin, np.random.rand() * 180*degrees]
    result = least_squares(error_cos2, par0, args=(Angles_fit, Iexp), bounds=bounds, ftol = tol)
    par1 = result.x
    # Plot result
    if verbose: 
        Imodel = model_cos2(par1, Angles_fit)
        utils.plot_experiment_residuals_1D(
        Angles_fit, Iexp, Imodel, title='Step 5b')

    if np.abs(par1[2]/degrees - 45) <= 20 and cal_dict['R1_az']<=90*degrees:
        cal_dict['R1_az'] = cal_dict['R1_az'] + 90*degrees
    if verbose:
        # Print results
        print('The values obtained are:')
        print('   - Imax       : {:.4f} V'.format(par1[1] + par1[0]))
        print('   - Imin       : {:.4f} V'.format(par1[0]))
        print('   - Maximum angle : {:.2f} deg'.format(par1[2]/degrees))
        print("\nThe correct angle for Q1 is {:.2f} deg".format(cal_dict['R1_az']/degrees))

    # Save data
    if pol is not None:
        filename = "Step_5b_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict


def make_step_6(cal_dict, pol=None, Nmeasures=144, angles='perfect', verbose = True):
    
    """Function that performs step 6 of calibration: Air Mueller matrix."""
    
    # If no polarimeter, take data from dictionary
    if pol is None:
        Iexp = cal_dict["I_step_6"]
        # if np.min(Iexp)<0: 
        #     Iexp = Iexp - np.min(Iexp)
        Angles_def = np.array(cal_dict["Angles_step_6"])

    else:
        Iexp = np.zeros(Nmeasures, dtype=float)
        Angles_def = utils.calculate_polarimetry_angles(Nmeasures, type=angles)
        
        utils.percentage_complete()

        for ind, angle in enumerate(Angles_def):
            theta_0 = np.array((cal_dict['P1_az'],cal_dict['R1_az'],cal_dict['R2_az'],cal_dict['P2_az']))
            theta = theta_0 + angle
            angles_aux = pol.motor.Move_Absolute(
                pos=theta, waiting='busy', units="rad")
            Angles_def[ind] = angles_aux 
            Iexp[ind] = pol.daca.Get_Signal()
            utils.percentage_complete(ind, 144)

        cal_dict["I_step_6"] = Iexp
        cal_dict["Angles_step_6"] = Angles_def
    
    Mp1 = Mueller().diattenuator_linear(p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict["P1_az"])
    
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R1_p1"], p2=cal_dict["R1_p2"], R=cal_dict["R1_Ret"], azimuth=-cal_dict["R1_az"])
    
    Mp2 = Mueller().diattenuator_linear(p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict["P2_az"])
    
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict["R2_az"])
    
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    system = [S, Mp1, Mr1, Mr2, Mp2]

    # if pol is None: 
    #      Angles_def[:,0] = Angles_def[:,0] + cal_dict['P1_az']
    #      Angles_def[:,1] = Angles_def[:,1] + cal_dict['R1_az']
    #      Angles_def[:,2] = Angles_def[:,2] + cal_dict['R2_az']
    #      Angles_def[:,3] = Angles_def[:,3] + cal_dict['P2_az']
    
    M,_ = Calculate_Mueller(I=Iexp, angles=Angles_def, system=system, filter=False)
    error = np.linalg.norm(M.M - Mueller().vacuum().M) / 16
    cal_dict["Error_step_6"] = error
    if verbose:       
        M.name = "Sample"
        print("The Mueller matrix is:\n", M)
        M.analysis.decompose_polar(tol=1e-20, verbose=True)
    # Save data
    if pol is not None:
        filename = "Step_6_{}".format(datetime.date.today())
        np.savez(filename + '.npz', angles_1=Angles_def, Iexp=Iexp, **CONF_DT_50, **CONF_U6)

    # Output
    return cal_dict, M

## ANÁLISIS SIMULTÁNEO DE LA CALIBRACIÓN

def Process_Simultaneous_Calibration(cal_dict=None, folder=None, filename=None, save_name=None, verbose=True, use_filter = True):
    
    """Function to process all the data and obtain final vlaues."""
    
    if verbose:
        start_time = time.time()

    # Load data if required
    if cal_dict is None:
        cal_dict = Load_Cal_Files(folder=folder, filename=filename)
    
    if not use_filter:  
        cal_dict['P_filter'] = 1 
        cal_dict['P_no_filter'] = 1 
    
    bound_up = np.array([
        180 * degrees,      # Azimuth de la iluminación 
        45 * degrees,       # Elipticidad de la iluminación 
        1,                  # Grado de pol. de la iluminación 
        1,                  # P1-p1 
        1,                  # P1-p2 
        1,                  # R1-p1 
        1,                  # R1-p2 
        180 * degrees,      # R1-retardancia 
        1,                  # R2-p1 
        1,                  # R2-p2 
        180 * degrees,      # R2-retardancia 
        1,                  # P2-p1 
        1,                  # P2-p2 
        1,                  # Pc-p1 
        1,                  # Pc-p2 
        1,                  # Rc-p1 
        1,                  # Rc-p2 
        180 * degrees,      # Rc-retardancia 
        45 * degrees        # Rc-offset 
    ])
    bound_down = np.array([
        0 * degrees,        # Azimuth de la iluminación 
        -45 * degrees,      # Elipticidad de la iluminación 
        0,                  # Grado de pol. de la iluminación
        0,                  # P1-p1 
        0,                  # P1-p2 
        0,                  # R1-p1 
        0,                  # R1-p2 
        0 * degrees,        # R1-retardancia 
        0,                  # R2-p1 
        0,                  # R2-p2 
        0 * degrees,       # R2-retardancia 
        0,                  # P2-p1 
        0,                  # P2-p2 
        0,                  # Pc-p1 
        0,                  # Pc-p2 
        0,                  # Rc-p1 
        0,                  # Rc-p2 
        0 * degrees,       # Rc-retardancia 
        -45 * degrees       # Rc-offset 
    ])

    bounds = (bound_down, bound_up)  
   

    # PSO
    optimizer = GlobalBestPSO(n_particles=PSO_part_global, dimensions=bound_up.size, options=options_global, bounds=bounds)
    cost, result = optimizer.optimize(opt_func_PSO, iters=PSO_iter_global, n_processes=PSO_cores_global, cal_dict=cal_dict, single=True)
    print('PSO Realizado')
    # Least squares
    args = [cal_dict]
    
    result = least_squares(error_global, result, bounds=bounds, args=args, ftol = tol)

    result = result.x
    print('Least squares realizado')

    (cal_dict["illum_az"], cal_dict["illum_el"], cal_dict["illum_pol_degree"],
         cal_dict["P1_p1"], cal_dict["P1_p2"], 
         cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_Ret"],
         cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_Ret"], 
         cal_dict["P2_p1"], cal_dict["P2_p2"], 
         cal_dict["Pc_p1"], cal_dict["Pc_p2"], 
         cal_dict["Rc_p1"], cal_dict["Rc_p2"], cal_dict["Rc_Ret"], cal_dict["Rc_offset"]) = result

    if save_name:
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)
        print("Current folder:   ", os.getcwd())
        np.savez(save_name, **cal_dict)
        print("Saved file:   ", save_name)
        if folder and old_folder:
            os.chdir(old_folder)
    if verbose:
        print("Ellapsed time is {} s".format(time.time() - start_time))

    return cal_dict


def model_global(par, cal_dict):
    
    """Function to calculate the global error of the calibration."""
    
    # Rename parameters
    (illum_az, illum_el, illum_pol_degree, P1_p1, P1_p2, R1_p1, R1_p2, R1_Ret, R2_p1, R2_p2, R2_Ret, P2_p1,
     P2_p2, Pc_p1, Pc_p2, Rc_p1, Rc_p2, Rc_Ret, dif_con) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict['S0'],
        azimuth=illum_az,
        ellipticity=illum_el,
        degree_pol=illum_pol_degree)
    
    if P1_p1<P1_p2:
        P1_p1, P1_p2 = P1_p2, P1_p1
    if P2_p1<P2_p2:
        P2_p1, P2_p2 = P2_p2, P2_p1    
    if Pc_p1<Pc_p2:
        Pc_p1, Pc_p2 = Pc_p2, Pc_p1
    
    
    Mp1 = Mueller().diattenuator_linear(
        p1=P1_p1, p2=P1_p2, azimuth=-cal_dict['P1_az'])
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=P2_p1, p2=P2_p2, azimuth=-cal_dict['P2_az'])
    Mpc = Mueller().diattenuator_linear(
        p1=Pc_p1, p2=Pc_p2, azimuth=0*degrees)
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=Rc_p1, p2=Rc_p2, R=Rc_Ret, azimuth=45*degrees - dif_con)

    # Step 2a
    Mp1_rot = Mp1.rotate(angle=cal_dict["angles_1"]*degrees, keep=True)
    Sf = Mp1_rot * Mpc * S
    I_2a = Sf.parameters.intensity()

    # Step 2b
    Mp2_rot = Mp2.rotate(angle=cal_dict["angles_1"]*degrees, keep=True)
    Sf = Mp2_rot * Mpc * S
    I_2b = Sf.parameters.intensity()
    
    # Step 2c
    Mp2_rot = Mp2.rotate(angle=cal_dict["angles_1"]*degrees + cal_dict['P2_az'], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Sf = Mp2_rot * Mp1_rot * S
    I_2c = Sf.parameters.intensity()

    # Step 3a
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["angles_1"]*degrees, keep=True)
    Sf = Mp2_rot *(Mr2_rot * (Mp1_rot * S))
    I_3a = Sf.parameters.intensity()

    # Step 3b
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["angles_1"]*degrees+cal_dict['R2_az'], keep=True)
    Sf = Mp2_rot *(Mr2_rot *(Mrc * (Mp1_rot * S)))
    I_3b = Sf.parameters.intensity()

    # Step 4
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_4"][0], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_4"][1], keep=True)
    Sf = Mp2_rot * (Mr2_rot * S)
    I_4 = Sf.parameters.intensity()

    # Step 5a
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr1_rot = Mr1.rotate(angle=cal_dict["angles_1"]*degrees, keep=True)
    Sf = Mp2_rot *(Mr1_rot * (Mp1_rot * S))
    I_5a = Sf.parameters.intensity()

    # Step 5b
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr1_rot = Mr1.rotate(angle=cal_dict["angles_1"]*degrees+cal_dict['R1_az'], keep=True)
    Sf = Mp2_rot *(Mrc *(Mr1_rot * (Mp1_rot * S)))
    I_5b = Sf.parameters.intensity()

    # # Step 6
    # Mp1_rot = Mp1.rotate(angle=cal_dict["Angles_step_6"][:, 0]+cal_dict['P1_az'], keep=True)
    # Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_6"][:, 1]+cal_dict['R1_az'], keep=True)
    # Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_6"][:, 2]+cal_dict['R2_az'], keep=True)
    # Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_6"][:, 3]+cal_dict['P2_az'], keep=True)
    # Sf = Mp2_rot * (Mr2_rot * (Mr1_rot * (Mp1_rot * S)))
    # I_6 = Sf.parameters.intensity()

    return [I_2a, I_2b, I_2c, I_3a, I_3b, I_4, I_5a, I_5b]#, I_6


def error_global(par, cal_dict, single=False):
    
    """Function to determine the global error."""

    Imodel = model_global(par, cal_dict)
    E_2a = (cal_dict["I_step_2a"] - Imodel[0])
    E_2b = (cal_dict["I_step_2b"] - Imodel[1])
    E_2c = (cal_dict["I_step_2c"] - Imodel[2])
    E_3a = (cal_dict["I_step_3a"] - Imodel[3])
    E_3b = (cal_dict["I_step_3b"] - Imodel[4])
    E_4 = (cal_dict["I_step_4"] - Imodel[5])
    E_5a = (cal_dict["I_step_5a"] - Imodel[6])
    E_5b = (cal_dict["I_step_5b"] - Imodel[7])    

    Error = np.concatenate((E_2a, E_2b, E_2c, E_3a, E_3b, E_4, E_5a, E_5b))#, E_6))
    if single:
        Error = np.linalg.norm(Error) / Error.size
    return Error

# ANÁLISIS ITERATIVO DE LA CALIBRACIÓN

def model_step_2(par, cal_dict):
    
    # Rename parameters
    # (P1_p1, P1_p2, P2_p1, P2_p2, Pc_p1, Pc_p2) = par
    (P1_p1, P1_p2, P2_p1, P2_p2, Pc_p1, Pc_p2) = par
    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    Mp1 = Mueller().diattenuator_linear(
        p1=P1_p1, p2=P1_p2, azimuth=-cal_dict['P1_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=P2_p1, p2=P2_p2, azimuth=-cal_dict['P2_az'])
    Mpc = Mueller().diattenuator_linear(
        p1=Pc_p1, p2=Pc_p2, azimuth=0*degrees)
    # Step 2a
    Mp1_rot = Mp1.rotate(angle=cal_dict["Angles_step_2a"], keep=True)
    Sf =  (Mp1_rot * (Mpc * S))
    I_2a = Sf.parameters.intensity()
    
    # Step 2b
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_2b"], keep=True)
    Sf = (Mp2_rot * (Mpc * S))
    I_2b = Sf.parameters.intensity()
    
    # Step 2c
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_2c"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Sf = (Mp2_rot * (Mp1_rot * S))
    I_2c = Sf.parameters.intensity()

    return I_2a, I_2b, I_2c
    

def error_step_2(par, cal_dict):
    
    """Function to determine the global error."""

    I_2a, I_2b, I_2c = model_step_2(par, cal_dict)
    E_2a = (cal_dict["I_step_2a"] - I_2a) /91
    E_2b = (cal_dict["I_step_2b"] - I_2b) /91
    E_2c = (cal_dict["I_step_2c"] - I_2c) /91

    Error = np.concatenate((E_2a, E_2b, E_2c))

    return Error

def analysis_step_2(cal_dict, verbose = False):
    bound_up = np.array([
        1,                  # P1-p1 
        1,                  # P1-p2 
        1,                  # P2-p1 
        1,                  # P2-p2 
        1,                  # Pc-p1 
        1,                  # Pc-p2 
    ])
    bound_down = np.array([
        0,                  # P1-p1 
        0,                  # P1-p2 
        0,                  # P2-p1 
        0,                  # P2-p2 
        0,                  # Pc-p1 
        0,                  # Pc-p2 
    ])

    bounds = (bound_down, bound_up)

    # Least squares
    args = [cal_dict]
    
    par0 = (cal_dict["P1_p1"], cal_dict["P1_p2"],
          cal_dict["P2_p1"], cal_dict["P2_p2"], 
        cal_dict["Pc_p1"], cal_dict["Pc_p2"])
    result = least_squares(error_step_2, par0, bounds=bounds, args=args, ftol= tol)
    result = result.x
    
    [cal_dict["P1_p1"], cal_dict["P1_p2"],
          cal_dict["P2_p1"], cal_dict["P2_p2"], 
          cal_dict["Pc_p1"], cal_dict["Pc_p2"]] = result
    
    if verbose: 
        print(np.round(result,2))
        I_2a, I_2b, I_2c = model_step_2(result, cal_dict)
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_2a"], I_2a, title='Step 2a')
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_2b"], I_2b, title='Step 2b')
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_2c"], I_2c, title='Step 2c')
    return cal_dict


def  model_step_3a(par, cal_dict):
    
    """Function to calculate the global error of the calibration."""
    
    # Rename parameters
    (R2_p1, R2_p2, R2_Ret) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])

    # Step 3a
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_3a"], keep=True)
    Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S)))
    I_3a = Sf.parameters.intensity()
    return I_3a

def error_step_3a(par, cal_dict):
    
    """Function to determine the global error."""

    Imodel = model_step_3a(par, cal_dict)
    E_3a = (cal_dict["I_step_3a"] - Imodel)
    Error = np.array(E_3a)
    return Error


def analysis_step_3a(cal_dict, verbose = False, arg = None):
    """Function to process all the data and obtain final vlaues."""

    bound_up = np.array([
        1,                  # R2-p1 
        1,                  # R2-p2 
        360 * degrees       # R2-retardancia 
    ])
    bound_down = np.array([
        0,                  # R2-p1 
        0,                  # R2-p2 
        0 * degrees         # R2-retardancia 
    ])

    bounds = (bound_down, bound_up)

    # Least squares
    args = [cal_dict]
    par0 = [cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_Ret"]]
    result = least_squares(error_step_3a, par0, bounds=bounds, args=args, ftol = tol)
    result = result.x
    
    # if result[1]>result[0]:
    #     result[0],result[1] = result[1],result[0]
    
    [cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_Ret"]] = result
    
    if verbose: 
        
        I_3a = model_step_3a(result, cal_dict)
        result[2] = result[2]/degrees
        print(np.round(result,2))
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_3a"], I_3a, title='Step 3a')
        
    return cal_dict


def model_step_3b(par, cal_dict):
    
    """Function to calculate the global error of the calibration."""
    
    # Rename parameters
    (Rc_p1, Rc_p2, Rc_Ret, dif_con) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict['R2_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=Rc_p1, p2=Rc_p2, R=Rc_Ret, azimuth= 45*degrees - dif_con)
    
    # Step 3b
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_3b"], keep=True)
    Sf = Mp2_rot *(Mr2_rot *(Mrc * (Mp1_rot * S)))
    I_3b = Sf.parameters.intensity()

    return I_3b


def error_step_3b(par, cal_dict):
    
    """Function to determine the global error."""

    I_3b = model_step_3b(par, cal_dict)
    
    E_3b = (cal_dict["I_step_3b"] - I_3b)

    Error = np.array(E_3b)
    
    return Error


def analysis_step_3b(cal_dict, verbose = False):
    """Function to process all the data and obtain final vlaues."""
    
    bound_up = np.array([
        1,                  # Rc-p1 
        1,                  # Rc-p2 
        180 * degrees,      # Rc-retardancia 
        45 * degrees        # Rc-offset 
    ])
    bound_down = np.array([
        0,                  # Rc-p1 
        0,                  # Rc-p2 
        0 * degrees,       # Rc-retardancia 
        -45 * degrees       # Rc-offset 
    ])

    bounds = (bound_down, bound_up)

    # Least squares
    args = [cal_dict]
    par0 = [cal_dict["Rc_p1"], cal_dict["Rc_p2"], cal_dict["Rc_Ret"], cal_dict["Rc_offset"]]
    result = least_squares(error_step_3b, par0, bounds=bounds, args=args, ftol = tol)
    result = result.x
    
    (cal_dict["Rc_p1"], cal_dict["Rc_p2"], cal_dict["Rc_Ret"], cal_dict["Rc_offset"]) = result
    
    if verbose: 
        I_3b = model_step_3b(result, cal_dict)
        result[2] = result[2]/degrees
        result[3] = result[3]/degrees
        print(np.round(result,2))
        
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_3b"], I_3b, title='Step 3b')


    return cal_dict

def  model_step_3(par, cal_dict):
    
    """Function to calculate the global error of the calibration."""
    
    # Rename parameters
    (R2_p1, R2_p2, R2_Ret, R2_az, Rc_p1, Rc_p2, Rc_Ret, dif_con) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-R2_az)
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=Rc_p1, p2=Rc_p2, R=Rc_Ret, azimuth= 135*degrees - dif_con)

    # Step 3a
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_3a"], keep=True)
    Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S)))
    I_3a = Sf.parameters.intensity()

    # Step 3b
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_3b"], keep=True)
    Sf = Mp2_rot *(Mr2_rot *(Mrc * (Mp1_rot * S)))
    I_3b = Sf.parameters.intensity()

    return I_3a, I_3b

def error_step_3(par, cal_dict):
    
    """Function to determine the global error."""

    I_3a, I_3b = model_step_3(par, cal_dict)
    E_3a = (cal_dict["I_step_3a"] - I_3a)
    E_3b = (cal_dict["I_step_3b"] - I_3b)
    Error = np.concatenate((E_3a, E_3b))
    return Error


def analysis_step_3(cal_dict, verbose = False):
    """Function to process all the data and obtain final vlaues."""
    
    bound_up = np.array([
        1,                  # R2-p1 
        1,                  # R2-p2 
        180 * degrees,       # R2-retardancia 
        180*degrees,
        1,                  # Rc-p1 
        1,                  # Rc-p2 
        360 * degrees,      # Rc-retardancia 
        45 * degrees        # Rc-offset
    ])
    bound_down = np.array([
        0,                  # R2-p1 
        0,                  # R2-p2 
        0 * degrees,         # R2-retardancia 
        0*degrees,
        0,                  # Rc-p1 
        0,                  # Rc-p2 
        0 * degrees,      # Rc-retardancia 
        -45 * degrees        # Rc-offset
    ])

    bounds = (bound_down, bound_up)

    # Least squares
    args = [cal_dict]
    par0 = [cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_Ret"], cal_dict['R2_az'], cal_dict["Rc_p1"], cal_dict["Rc_p2"], cal_dict["Rc_Ret"], cal_dict["Rc_offset"]]
    result = least_squares(error_step_3, par0, bounds=bounds, args=args, ftol = tol)
    result = result.x
    
    [cal_dict["R2_p1"], cal_dict["R2_p2"], cal_dict["R2_Ret"], cal_dict['R2_az'], cal_dict["Rc_p1"], cal_dict["Rc_p2"], cal_dict["Rc_Ret"], cal_dict["Rc_offset"]] = result
    
    if verbose: 
        
        I_3a, I_3b = model_step_3(result, cal_dict)
        result[2] = result[2]/degrees
        result[3] = result[3]/degrees
        result[6] = result[6]/degrees
        result[7] = result[7]/degrees
        print(np.round(result,2))
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_3a"], I_3a, title='Step 3a')
        utils.plot_experiment_residuals_1D(cal_dict['angles_1']*degrees, cal_dict["I_step_3b"], I_3b, title='Step 3b')
        
    return cal_dict


def model_step_4(par, cal_dict):
    
    # Rename parameters
    (illum_az, illum_el, illum_pol_degree) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict['S0'],
        azimuth=illum_az,
        ellipticity=illum_el,
        degree_pol=illum_pol_degree)
    
    
    Mr2 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict['R2_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
    
    # Step 4
    Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_4"][0], keep=True)
    Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_4"][1], keep=True)
    Sf = Mp2_rot * Mr2_rot * S
    I_4 = Sf.parameters.intensity()

    return I_4


def error_step_4(par, cal_dict):
    
    """Function to determine the global error."""

    
    Imodel = model_step_4(par, cal_dict)
    E_4 = (cal_dict["I_step_4"] - Imodel)
    Error = np.array(E_4)
    
    return Error

def analysis_step_4(cal_dict, verbose = False):

    bound_up = np.array([
        180 * degrees,      # Azimuth de la iluminación 
        45 * degrees,       # Elipticidad de la iluminación 
        1,                  # Grado de pol. de la iluminación 
    ])
    bound_down = np.array([
        0 * degrees,        # Azimuth de la iluminación 
        -45 * degrees,      # Elipticidad de la iluminación 
        0,                  # Grado de pol. de la iluminación
    ])

    bounds = (bound_down, bound_up)
    
    # Least squares
    args = [cal_dict]
    par0 = [cal_dict["illum_az"], cal_dict["illum_el"], cal_dict["illum_pol_degree"]]
    
    result = least_squares(error_step_4, par0, bounds=bounds, args=args, ftol = tol)
    result = result.x
    ( cal_dict["illum_az"], cal_dict["illum_el"], cal_dict["illum_pol_degree"]) = result
    
    if verbose: 
        
        I_4 = model_step_4(result, cal_dict)
        result[0] = result[0]/degrees
        result[1] = result[1]/degrees
        print(np.round(result,2))
        utils.plot_experiment_residuals_1D(np.linspace(1,20,20)*degrees, cal_dict["I_step_4"], I_4, title='Step 4')
    return cal_dict


def model_step_5a(par, cal_dict):
    
   # Rename parameters
    (R1_p1, R1_p2, R1_Ret) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])

    # Step 5a
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_5a"], keep=True)
    Sf = Mp2_rot * Mr1_rot * Mp1_rot * S
    I_5a = Sf.parameters.intensity()

    return I_5a

def error_step_5a(par, cal_dict):
    
    """Function to determine the global error."""

    Imodel = model_step_5a(par, cal_dict)
    
    E_5a = (cal_dict["I_step_5a"] - Imodel) 
    
    Error = E_5a

    return Error

def analysis_step_5a(cal_dict, verbose = False):


    bound_up = np.array([
        1,                  # R1-p1 
        1,                  # R1-p2 
        180 * degrees       # R1-retardancia 
    ])
    bound_down = np.array([
        0,                  # R1-p1 
        0,                  # R1-p2 
        0 * degrees         # R1-retardancia 
    ])

    bounds = (bound_down, bound_up)

    # Least squares
    args = [cal_dict]
    par0 = [cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_Ret"]]
    result = least_squares(error_step_5a, par0, bounds=bounds, args=args, ftol = tol)
    result = result.x

    # if result[1]>result[0]:
    #     result[0],result[1] = result[1],result[0]
        
    (cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R1_Ret"]) = result
    
    if verbose: 
        I_5a = model_step_5a(result, cal_dict)
        result[2] = result[2]/degrees
        print(np.round(result,2))
        utils.plot_experiment_residuals_1D(cal_dict['angles_1'], cal_dict["I_step_5a"], I_5a, title='Step 5a')
    return cal_dict


def model_step_5b(par, cal_dict):
    
   # Rename parameters
    (Rc_p1, Rc_p2, Rc_Ret, dif_con) = par

    # Create objects
    S = Stokes().general_azimuth_ellipticity(
        intensity=cal_dict["S0"],
        azimuth=cal_dict["illum_az"],
        ellipticity=cal_dict["illum_el"],
        degree_pol=cal_dict["illum_pol_degree"])
    
    Mp1 = Mueller().diattenuator_linear(
        p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], azimuth=-cal_dict['P1_az'])
    Mr1 = Mueller().diattenuator_retarder_linear(
        p1=cal_dict["R1_p1"], p2=cal_dict["R1_p2"], R=cal_dict["R1_Ret"], azimuth=-cal_dict['R1_az'])
    Mp2 = Mueller().diattenuator_linear(
        p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], azimuth=-cal_dict['P2_az'])
    Mrc = Mueller().diattenuator_retarder_linear(
        p1=Rc_p1, p2=Rc_p2, R=Rc_Ret, azimuth=45*degrees - dif_con)

    # Step 5b
    Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
    Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
    Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_5b"], keep=True)
    Sf = Mp2_rot * Mrc *Mr1_rot * Mp1_rot * S
    I_5b = Sf.parameters.intensity()

    return I_5b


def error_step_5b(par, cal_dict):
    
    """Function to determine the global error."""

    Imodel = model_step_5b(par, cal_dict)
    
    E_5b = (cal_dict["I_step_5b"] - Imodel)
    
    Error = E_5b

    return Error

def analysis_step_5b(cal_dict, verbose = False):

    bound_up = np.array([
        1,                  # Rc-p1 
        1,                  # Rc-p2 
        180 * degrees,      # Rc-retardancia
        45 * degrees,      # Rc-offset
    ])
    bound_down = np.array([
        0,                  # Rc-p1 
        0,                  # Rc-p2 
        0 * degrees,      # Rc-retardancia
        -45 * degrees,      # Rc-offset
    ])

    bounds = (bound_down, bound_up)

    # Least squares
    args = [cal_dict]
    par0 = [cal_dict["Rc_p1"],cal_dict["Rc_p2"],cal_dict["Rc_Ret"],cal_dict["Rc_offset"]]
    result = least_squares(error_step_5b, par0, bounds=bounds, args=args, ftol = tol)
    result = result.x

    (cal_dict["Rc_p1"],cal_dict["Rc_p2"],cal_dict["Rc_Ret"],cal_dict["Rc_offset"]) = result

    if verbose: 
        I_5b = model_step_5b(result, cal_dict)
        result[2] = result[2]/degrees
        result[3] = result[3]/degrees
        print(np.round(result,2))
        utils.plot_experiment_residuals_1D(cal_dict['angles_1'], cal_dict["I_step_5b"], I_5b, title='Step 5b')
    return cal_dict


def Process_Iterative_Calibration(cal_dict=None, folder=None, filename=None, save_name=None, verbose=True, use_filter = True, N_iterations=20):
    
    """Function to process all the data and obtain final vlaues."""
    
    if verbose:
        start_time = time.time()

    # Load data if required
    if cal_dict is None:
        cal_dict = Load_Cal_Files(folder=folder, filename=filename)
    
    if not use_filter:  
        cal_dict['P_filter'] = 1 
        cal_dict['P_no_filter'] = 1 
        
    # Draw function
    def draw_fig():
        fig = plt.figure(figsize=(15, 15))
        ax = fig.add_subplot(3, 3, 1)
        ax.plot(error[:ind + 1])
        ax.set_ylabel("Normalized RMS error")
        ax.set_title("Normalized RMS error")

        ax = fig.add_subplot(3, 3, 2)
        ax.plot(els[:ind + 1] / degrees)
        ax.set_ylabel("Ellipticity angle (deg)")
        ax.set_title("Ilum ellipticity")

        ax = fig.add_subplot(3, 3, 3)
        ax.plot(offset[:ind + 1] / degrees)
        ax.set_ylabel("Angle (deg)")
        ax.set_title("Rc offset")

        ax = fig.add_subplot(3, 3, 4)
        ax.plot(p1s[:, :ind + 1].T)
        ax.set_ylabel("Field trans.")
        ax.set_title("Polarizers p1")
        ax.legend(("P1", "P2", "Pc"))

        ax = fig.add_subplot(3, 3, 5)
        ax.plot(p2s[:, :ind + 1].T)
        ax.set_ylabel("Field trans.")
        ax.set_title("Polarizers p2")
        ax.legend(("P1", "P2", "Pc"))

        ax = fig.add_subplot(3, 3, 6)
        ax.plot(ps[:, :ind + 1].T)
        ax.set_ylabel("Field trans.")
        ax.set_title("Retarders")
        ax.legend(("R1 p1", "R1 p2", "R2 p1", "R2 p2", "Rc p1", "R2 p2"))

        ax = fig.add_subplot(3, 3, 7)
        ax.plot(azs[:, :ind + 1].T / degrees)
        ax.set_ylabel("Azimuth (deg)")
        ax.set_xlabel("Iteration")
        ax.set_title("Initial angles")
        ax.legend(("Illum", "P1", "P2", "R1", "R2"))

        ax = fig.add_subplot(3, 3, 8)
        ax.plot(rets[:-1, :ind + 1].T / degrees)
        ax.set_ylabel("Retardance (deg)")
        ax.set_xlabel("Iteration")
        ax.set_title("L/4 retardances")
        ax.legend(("R1", "R2", "Rc"))

        ax = fig.add_subplot(3, 3, 9)
        ax.plot(rets[-1, :ind + 1] / degrees)
        ax.set_ylabel("Retardance (deg)")
        ax.set_xlabel("Iteration")
        ax.set_title("P1 retardance")

    # Prealocate
    error = np.zeros(N_iterations)
    offset = np.zeros(N_iterations)
    p1s = np.zeros((3, N_iterations))
    p2s = np.zeros((3, N_iterations))
    ps = np.zeros((6, N_iterations))
    azs = np.zeros((5, N_iterations))
    els = np.zeros(N_iterations)
    rets = np.zeros((3, N_iterations))
    fig = plt.figure(figsize=(15, 15))
    plt.ion()

    # Iterate
    for ind in range(N_iterations):
        
        cal_dict = analysis_step_2(cal_dict)
        cal_dict = analysis_step_3a(cal_dict)
        cal_dict = analysis_step_3b(cal_dict)
        cal_dict = make_step_4(cal_dict, pol=None, verbose = False)
        cal_dict = analysis_step_5a(cal_dict)
        cal_dict = analysis_step_5b(cal_dict)
        
        # Save data
        
        azs[:, ind] = [
            cal_dict["illum_az"], cal_dict["P1_az"],
            cal_dict["P2_az"], 
            cal_dict["R1_az"], cal_dict["R2_az"]
        ]
        els[ind] = cal_dict["illum_el"]
        p1s[:, ind] = [
            cal_dict["P1_p1"], cal_dict["P2_p1"], 
            cal_dict["Pc_p1"]
        ]
        p2s[:, ind] = [
            cal_dict["P1_p2"], cal_dict["P2_p2"], 
            cal_dict["Pc_p2"]
        ]
        ps[:, ind] = [
            cal_dict["R1_p1"], cal_dict["R1_p2"], cal_dict["R2_p1"],
            cal_dict["R2_p2"], cal_dict["Rc_p1"], cal_dict["Rc_p2"]
        ]
        rets[:, ind] = [
            cal_dict["R1_Ret"], cal_dict["R2_Ret"], cal_dict["Rc_Ret"],
        ]
        offset[ind] = cal_dict["Rc_offset"]

        print("Proceso: {} %".format((ind+1)*100/N_iterations), end='\r')
    if verbose and N_iterations > 1:
        draw_fig()

    
    if save_name:
        if folder:
            old_folder = os.getcwd()
            os.chdir(folder)
        print("Current folder:   ", os.getcwd())
        np.savez(save_name, **cal_dict)
        print("Saved file:   ", save_name)
        if folder and old_folder:
            os.chdir(old_folder)
    if verbose:
        print("Ellapsed time is {} s".format(time.time() - start_time))

    return cal_dict
def __END():
    pass
