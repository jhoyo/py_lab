"""
Configuration file. Standard py_lab units are mm.
"""

import numpy as np

degrees = np.pi / 180
number_types = (float, int)

#######################
## SLMs
#######################

# Configuration Params for Holoeye 2500 SLM
CONF_HOLOEYE2500 = {}
CONF_HOLOEYE2500['resolution'] = np.array([1024, 768])
CONF_HOLOEYE2500['pixel_size'] = np.array([19, 19]) * 1e-3
CONF_HOLOEYE2500["dynamic_range"] = 8
CONF_HOLOEYE2500["framerate"] = 72
CONF_HOLOEYE2500["wavelength"] = 632.8e-9

CONF_HOLOEYE2500['callibration_table'] = None
CONF_HOLOEYE2500['ID_screen'] = 1

# Configuration Params for Holoeye PLUTO SLM
CONF_PLUTO = {}
CONF_PLUTO['resolution'] = np.array([1920, 1080])
CONF_PLUTO['pixel_size'] = np.array([8, 8]) * 1e-3
CONF_PLUTO["framerate"] = 60  # Hz
CONF_PLUTO["dynamic_range"] = 8
CONF_PLUTO["wavelength"] = 632.8e-9


CONF_PLUTO['callibration_table'] = None
CONF_PLUTO['ID_screen'] = 1
CONF_PLUTO['f1'] = 16589481.30  #10792738.4
CONF_PLUTO['f2'] = 16715629.50  #11096772.2
CONF_PLUTO['theta'] = 2.47308

# Configuration Params for Holoeye PLUTO SLM (puesto 3)
CONF_PLUTO2 = {}
CONF_PLUTO2['resolution'] = np.array([1920, 1080])
CONF_PLUTO2['pixel_size'] = np.array([8, 8]) * 1e-3
CONF_PLUTO2["framerate"] = 60  # Hz
CONF_PLUTO2["dynamic_range"] = 8
CONF_PLUTO2["wavelength"] = 632.8e-9

CONF_PLUTO2['callibration_table'] = None
CONF_PLUTO2['ID_screen'] = 1

# Configuration Params for kopin SLM 
CONF_KOPIN = {}
CONF_KOPIN['resolution'] = np.array([300, 225])
CONF_KOPIN['pixel_size'] = np.array([11, 11]) * 1e-3
CONF_KOPIN["framerate"] = 60  # Hz
CONF_KOPIN["dynamic_range"] = 8
CONF_KOPIN["wavelength"] = 632.8e-9

CONF_KOPIN['callibration_table'] = None
CONF_KOPIN['ID_screen'] = 1

##########################
## CAMERAS
##########################

# Configuration Params for ImagingSource Camera 1
CONF_IMAGING_SOURCE = {}
CONF_IMAGING_SOURCE["name"] = "DMx 72BUC02 14210296"
CONF_IMAGING_SOURCE['resolution'] = np.array([2592, 1944])
CONF_IMAGING_SOURCE['resolution_string'] = "Y800 (2592x1944)"
CONF_IMAGING_SOURCE['pixel_size'] = np.array([2.2, 2.2]) * 1e-3
CONF_IMAGING_SOURCE["dynamic_range"] = 8
CONF_IMAGING_SOURCE["wait_time"] = 0.2
CONF_IMAGING_SOURCE["wavelength"] = 633e-6
CONF_IMAGING_SOURCE["resolutions"] = {
    "Y800 (2592x1944)": np.array([2592, 1944]),
    "Y800 (2560x1920)": np.array([2560, 1920]),
    "Y800 (2560x1440)": np.array([2560, 1440]),
    "Y800 (2048x1536)": np.array([2048, 1536]),
    "Y800 (1920x1080)": np.array([1920, 1080]),
    "Y800 (1600x1200)": np.array([1600, 1200]),
    "Y800 (1296x972)": np.array([1296, 972]),
    "Y800 (1280x1024)": np.array([1280, 1024]),
    "Y800 (1280x960)": np.array([1280, 960]),
    "Y800 (1280x720)": np.array([1280, 720]),
    "Y800 (1024x768)": np.array([1024, 768]),
    "Y800 (640x480)": np.array([640, 480])
}
CONF_IMAGING_SOURCE["framerates"] = {
    "Y800 (2592x1944)": 7,
    "Y800 (2560x1920)": 7,
    "Y800 (2560x1440)": 7,
    "Y800 (2048x1536)": 7,
    "Y800 (1920x1080)": 16,
    "Y800 (1600x1200)": 16,
    "Y800 (1296x972)": 16,
    "Y800 (1280x1024)": 16,
    "Y800 (1280x960)": 16,
    "Y800 (1280x720)": 16,
    "Y800 (1024x768)": 16,
    "Y800 (640x480)": 60
}
CONF_IMAGING_SOURCE["wait_time_param"] = 2

# Configuration Params for ImagingSource Camera 2
CONF_IMAGING_SOURCE_2 = {}
CONF_IMAGING_SOURCE_2["name"] = "DMx 72BUC02 14210300"
CONF_IMAGING_SOURCE_2['resolution'] = np.array([2592, 1944])
CONF_IMAGING_SOURCE_2['resolution_string'] = "Y800 (2592x1944)"
CONF_IMAGING_SOURCE_2['pixel_size'] = np.array([2.2, 2.2]) * 1e-3
CONF_IMAGING_SOURCE_2["dynamic_range"] = 8
CONF_IMAGING_SOURCE_2["wait_time"] = 0.2
CONF_IMAGING_SOURCE_2["wavelength"] = 633e-6
CONF_IMAGING_SOURCE_2["resolutions"] = {
    "Y800 (2592x1944)": np.array([2592, 1944], dtype=int),
    "Y800 (2560x1920)": np.array([2560, 1920], dtype=int),
    "Y800 (2560x1440)": np.array([2560, 1440], dtype=int),
    "Y800 (2048x1536)": np.array([2048, 1536], dtype=int),
    "Y800 (1920x1080)": np.array([1920, 1080], dtype=int),
    "Y800 (1600x1200)": np.array([1600, 1200], dtype=int),
    "Y800 (1296x972)": np.array([1296, 972], dtype=int),
    "Y800 (1280x1024)": np.array([1280, 1024], dtype=int),
    "Y800 (1280x960)": np.array([1280, 960], dtype=int),
    "Y800 (1280x720)": np.array([1280, 720], dtype=int),
    "Y800 (1024x768)": np.array([1024, 768], dtype=int),
    "Y800 (640x480)": np.array([640, 480], dtype=int)
}
CONF_IMAGING_SOURCE_2["framerates"] = {
    "Y800 (2592x1944)": 7,
    "Y800 (2560x1920)": 7,
    "Y800 (2560x1440)": 7,
    "Y800 (2048x1536)": 7,
    "Y800 (1920x1080)": 16,
    "Y800 (1600x1200)": 16,
    "Y800 (1296x972)": 16,
    "Y800 (1280x1024)": 16,
    "Y800 (1280x960)": 16,
    "Y800 (1280x720)": 16,
    "Y800 (1024x768)": 16,
    "Y800 (640x480)": 60
}
CONF_IMAGING_SOURCE_2["wait_time_param"] = 2


# Configuration Params for Thorlabs DC1650C Camera
CONF_DC1645C = {}
CONF_DC1645C["name"] = "CONF_DC1645C"
CONF_DC1645C['resolution'] = np.array([1280, 1024], dtype=int)
CONF_DC1645C['pixel_size'] = np.array([3.6, 3.6]) * 1e-3
CONF_DC1645C["dynamic_range"] = 8
CONF_DC1645C["wait_time"] = 0.1
CONF_DC1645C["wavelength"] = 800e-6
CONF_DC1645C["framerate"] = 25
CONF_DC1645C["wait_time_param"] = 5
CONF_DC1645C["default_framerate"] = 2

# Configuration Params for uEye Cameras
CONF_UEYE = {}
CONF_UEYE["name"] = "uEye"
CONF_UEYE["model"] = "UI-1485LE-M-GL"
CONF_UEYE["HIDS"] = 0
CONF_UEYE["nBitsPerPixel"] = 24
CONF_UEYE['resolution'] = np.array([2560, 1920], dtype=int)
CONF_UEYE['pixel_size'] = np.array([2.2, 2.2]) * 1e-3
CONF_UEYE["dynamic_range"] = 8
CONF_UEYE["wait_time"] = 0.1
CONF_UEYE["wavelength"] = 850e-6
CONF_UEYE["framerate"] = 6
CONF_UEYE["wait_time_param"] = 5
CONF_UEYE["min_fps"] = 0.36
CONF_UEYE["max_fps"] = 6.31
CONF_UEYE["max_exp"] = 1590
CONF_UEYE["const_exp"] = 6790
CONF_UEYE["const_fps"] = 6.5
CONF_UEYE["verbose"] = True

CONF_UEYE_SLM = {}
CONF_UEYE_SLM["name"] = "uEye_SLM"
CONF_UEYE_SLM["model"] = "UI-1640LE-C-HQ"
CONF_UEYE_SLM["HIDS"] = 0
CONF_UEYE_SLM["nBitsPerPixel"] = 32
CONF_UEYE_SLM['resolution'] = np.array([1280, 1024], dtype=int)
CONF_UEYE_SLM['pixel_size'] = np.array([3.6, 3.6]) * 1e-3
CONF_UEYE_SLM["dynamic_range"] = 10
CONF_UEYE_SLM["wavelength"] = 633e-6
CONF_UEYE_SLM["wait_time"] = 0.1
CONF_UEYE_SLM["framerate"] = 6
CONF_UEYE_SLM["wait_time_param"] = 5
CONF_UEYE_SLM["min_fps"] = 0.36
CONF_UEYE_SLM["max_fps"] = 6.31
CONF_UEYE_SLM["max_exp"] = 1590
CONF_UEYE_SLM["const_exp"] = 6790
CONF_UEYE_SLM["const_fps"] = 6.5
CONF_UEYE_SLM["verbose"] = True

CONF_UEYE_LCD = {}
CONF_UEYE_LCD["name"] = "uEye_LCD"
CONF_UEYE_LCD["model"] = "UI-1492LE-M"
CONF_UEYE_LCD["HIDS"] = 0
CONF_UEYE_LCD["nBitsPerPixel"] = 32
CONF_UEYE_LCD['resolution'] = np.array([3840 , 2748], dtype=int)
CONF_UEYE_LCD['pixel_size'] = np.array([1.67, 1.67]) * 1e-3
CONF_UEYE_LCD["dynamic_range"] = 8
CONF_UEYE_LCD["wavelength"] = 633e-6
CONF_UEYE_LCD["wait_time"] = 0.1
CONF_UEYE_LCD["pixelclock"] = 36
CONF_UEYE_LCD["framerate"] = 3.2
CONF_UEYE_LCD["wait_time_param"] = 5
CONF_UEYE_LCD["min_fps"] = 0.49
CONF_UEYE_LCD["max_fps"] = 3.2
CONF_UEYE_LCD["max_exp"] = 312
CONF_UEYE_LCD["verbose"] = True


# Configuration params for ELIS1024 linear cameras
CONF_ELIS1024 = {}
CONF_ELIS1024["name"] = "ELIS1024"
CONF_ELIS1024["resolution"] = np.array([1024], dtype=int)
CONF_ELIS1024['pixel_size'] = np.array([7.8, 125]) * 1e-3
CONF_ELIS1024["wavelength"] = 632.8e-9
CONF_ELIS1024["framerate"] = 0
CONF_ELIS1024["wait_time_param"] = 0
CONF_ELIS1024["verbose"] = True
CONF_ELIS1024["integration"] = np.array([1, 2, 4, 8])

#######################
## MOTORS
#######################

# Configuration Params for SMC100 Motor
CONF_SMC_100 = {}
CONF_SMC_100['port'] = 'COM3'
CONF_SMC_100['range'] = np.array([-120, 120])
CONF_SMC_100["pos_units"] = 'mm'
CONF_SMC_100["vel_units"] = 'mm/s'

# Configuration Params for SMC100 Motor
CONF_ESP_300 = {}
CONF_ESP_300['port'] = 'COM4'
CONF_ESP_300['axes'] = [1, 2, 3]
CONF_ESP_300["pos_units"] = 'mm'
CONF_ESP_300["vel_units"] = 'mm/s'
CONF_ESP_300["right_limit_axis_0"] = 39.0 # mm, limite por restriccion fisica, no de motor
CONF_ESP_300["left_limit"] = 0.1 # mm, to avoid noises when moving to 0
CONF_ESP_300["sign"] = [+1, +1, -1]
CONF_ESP_300["home_vel_high"] = 1
CONF_ESP_300["home_vel_low"] = 0.2
CONF_ESP_300["home_vel_high_axis_2"] = 0.5
CONF_ESP_300["home_vel_low_axis_2"] = 0.1

# Configuration Params for DT-50 Rotatory Motor
CONF_DT_50 = {}
CONF_DT_50["N"] = 1
CONF_DT_50["ports"] = [3, 5, 4, 6]
CONF_DT_50["baudrate"] = 115200
CONF_DT_50["dll_lib"] = __file__[:-9] + 'drivers\motors\dt50\PI_GCS2_DLL_x64'
CONF_DT_50["velocity"] = 500
CONF_DT_50["axes"] = ['1']
CONF_DT_50["sleeping_time"] = 1e-3
CONF_DT_50["pos_units"] = 'deg'
CONF_DT_50["vel_units"] = 'deg/s'
CONF_DT_50["invert"] = [False, True, True, False]

# Configuration Params for Newport 8742 picomotor controler
CONF_NEWPORT_8742 = {}
CONF_NEWPORT_8742["axes"] = [1, 2, 3]
CONF_NEWPORT_8742["idVendor"] = 0x104d
CONF_NEWPORT_8742["idProduct"] = 0x4000
CONF_NEWPORT_8742["config"] = None
CONF_NEWPORT_8742["ep_out"] = 0x2
CONF_NEWPORT_8742["ep_in"] = 0x81
CONF_NEWPORT_8742["end_char"] = "\r"
CONF_NEWPORT_8742["step_2_mm"] = 30e-6 # 35.6e-6
CONF_NEWPORT_8742["step_2_rad"] = 7.28e-7 # 7e-7
CONF_NEWPORT_8742["step_2_deg"] = 4.17e-5 # CONF_NEWPORT_8742["step_2_rad"] * 180 / np.pi
CONF_NEWPORT_8742["pos_units"] = 'mm'
CONF_NEWPORT_8742["vel_units"] = 'mm/s'
CONF_NEWPORT_8742["screw_pitch"] = 0.635 # mm separation between two screw peaks. Distance advanced in a complete 360º turning of one screw
CONF_NEWPORT_8742["d_screw"] = 41.25 # 40.66 / 41.25  # mm. Revisar este dato.

# Configuration Params for InteLiDrives RTGA-28-60D with Copley Controlls Stepnet STP-075-07
CONF_INTEL = {}
CONF_INTEL["axes"] = [0, 1, 2]
CONF_INTEL['ports'] = 'COM3'
CONF_INTEL['counts_per_rev'] = 4e4
CONF_INTEL['counts_per_deg'] = CONF_INTEL['counts_per_rev'] / 360
CONF_INTEL['vel_const'] = CONF_INTEL['counts_per_deg'] * \
    10  # Velocity units are 0.1 counts / sec
CONF_INTEL['ac_const'] = CONF_INTEL['counts_per_deg'] / \
    10  # Velocity units are 10 counts / sec**2
CONF_INTEL["pos_units"] = 'deg'
CONF_INTEL["vel_units"] = 'deg/s'
CONF_INTEL["max_vel"] = 200000  # Units:  0.1 Counts/s
CONF_INTEL["max_acel"] = 2000  # Units: 10 Counts/s**2
CONF_INTEL["default_baudrate"] = 9600
CONF_INTEL["working_baudrate"] = 115200
CONF_INTEL["max_baudrate"] = 115200
CONF_INTEL["timeout"] = 2
CONF_INTEL["write_timeout"] = 0.5
CONF_INTEL["invert"] = np.array([True,False,False])

# Configuration Params for InteLiDrives RTGA-28-60D with Copley Controlls Stepnet STP-075-07
CONF_INTEL_SINGLE = {}
CONF_INTEL_SINGLE["axes"] = 0
CONF_INTEL_SINGLE['port'] = 'COM6'
CONF_INTEL_SINGLE['counts_per_rev'] = 4e4
CONF_INTEL_SINGLE['counts_per_deg'] = CONF_INTEL['counts_per_rev'] / 360
CONF_INTEL_SINGLE['vel_const'] = CONF_INTEL['counts_per_deg'] * \
    10  # Velocity units are 0.1 counts / sec
CONF_INTEL_SINGLE['ac_const'] = CONF_INTEL['counts_per_deg'] / \
    10  # Velocity units are 10 counts / sec**2
CONF_INTEL_SINGLE["pos_units"] = 'deg'
CONF_INTEL_SINGLE["vel_units"] = 'deg/s'
CONF_INTEL_SINGLE["max_vel"] = 200000  # Units:  0.1 Counts/s
CONF_INTEL_SINGLE["max_acel"] = 2000  # Units: 10 Counts/s**2
CONF_INTEL_SINGLE["default_baudrate"] = 9600
CONF_INTEL_SINGLE["working_baudrate"] = 115200
CONF_INTEL_SINGLE["max_baudrate"] = 115200
CONF_INTEL_SINGLE["timeout"] = 2
CONF_INTEL_SINGLE["write_timeout"] = 0.5
CONF_INTEL_SINGLE["invert"] = False

# Configuration Params for M-511-DD Motor
CONF_M511 = {}
CONF_M511["pos_units"] = 'mm'
CONF_M511["vel_units"] = 'mm/s'

# Configuration Params for GRBL Motor
CONF_GRBL = {}
CONF_GRBL["pos_units"] = 'mm'
CONF_GRBL["vel_units"] = 'mm/s'
CONF_GRBL["port"] = 'COM4'
CONF_GRBL["invert"] = True
CONF_GRBL["home_vel_fast"] = 750/60
CONF_GRBL["home_vel_slow"] = 50/60
CONF_GRBL["home_dist"] = 390
CONF_GRBL["home_debounce"] = -5

# Configuration Params for Zaber Motor
CONF_ZABER = {}
CONF_ZABER["pos_units"] = 'deg'
CONF_ZABER["vel_units"] = 'deg/s'
CONF_ZABER["port"] = "COM6"#"/dev/ttyACM0"
CONF_ZABER["invert"] = False
CONF_ZABER["home_vel_fast"] = 250
CONF_ZABER["home_vel_slow"] = 250
CONF_ZABER["max_vel"] = 1000   # buscar números
CONF_ZABER["max_acel"] = 1000  # buscar números
CONF_ZABER["axes"] = [1,2,3,4]



###############################
## SOURCES
###############################

CONF_PIC_CTL_PD = {}
CONF_PIC_CTL_PD["port"] = 'COM3'

CONF_PIC_CTL_7V = {}
CONF_PIC_CTL_7V["port"] = 'COM3'

###############################
## SETUPS
###############################

# SLM setups
CONF_SLM1 = {}
# CONF_SLM1['angles_0'] = np.array([127, 175 - 90, 91 + 90, 73]) # Old
CONF_SLM1['angles_0'] = np.array([127, 85, 90, 72]) * degrees
CONF_SLM1['azimuth_source'] = 27 * degrees
CONF_SLM1['ellipticity_source'] = 40.9 * degrees
CONF_SLM1['mirrors'] = [True, False]

CONF_SLM2 = {}
#Alineamiento polarímetro
CONF_SLM2['angles_0'] = np.array([55,82,157,135]) * degrees # (sumando los 4 grados del P_ref)
CONF_SLM2['azimuth_source'] = 0 * degrees
CONF_SLM2['ellipticity_source'] =  42.4 * degrees
CONF_SLM2['mirrors'] = [False, False]
#Alineamiento manual
#CONF_SLM2['angles_0'] = np.array([83, 64, 0, 20]) * degrees
#CONF_SLM2['azimuth_source'] = 143 * degrees
#CONF_SLM2['ellipticity_source'] = 29.7 * degrees


# Port of Labjack cards
CONF_U3 = {}
CONF_U3['AIN'] = [2]
CONF_U3['AIN_ref'] = 1
CONF_U3['max_signal'] = 9.9

CONF_U3_LCD = {}
CONF_U3_LCD['AIN'] = [2]
CONF_U3_LCD['AIN_ref'] = 0
CONF_U3_LCD['max_signal'] = 9.9

CONF_U6 = {}
CONF_U6['AIN'] = [0]
CONF_U6['AIN_ref'] = 2
CONF_U6['res_index'] = 8
CONF_U6['gain_index'] = 0
CONF_U6['sett_factor'] = 4
CONF_U6['normalization'] = 1  # e-6 / 0.8144
CONF_U6['max_signal'] = 9.9

CONF_T7 = {}
CONF_T7['AIN'] = [2]  # AIN1
CONF_T7['AIN_ref'] = 4  # AIN2
CONF_T7['max_signal'] = 9.9

# Spectrometers
CONF_AVANTES = {}
CONF_AVANTES['Npixels'] = 3648
CONF_AVANTES['model'] = 'AvaSpec-ULS3648-USB2'
CONF_AVANTES['serial'] = '1701126U1'
CONF_AVANTES['address'] = 'SDK::C:\Program Files (x86)\AvaSpecX64-DLL_9.14.0.0/avaspecx64.dll'
CONF_AVANTES["exposure"] = 25
CONF_AVANTES["N_average"] = 5

#  Liquid crystal

CONF_LCC25 = {}
CONF_LCC25['port'] = "COM4"
CONF_LCC25['wait_time'] = 0.06

# Power meters
CONF_SOLO2 = {}
CONF_SOLO2['port'] = 'COM5'
CONF_SOLO2['Twait'] = 1e-1

CONF_OPHIR = {}
CONF_OPHIR['Twait'] = 1e-1
CONF_OPHIR['retries'] = 1e2

# Polarimeter

CONF_POLARIMETER = {}
CONF_POLARIMETER['cal_file'] = "Polarimeter.npz"
CONF_POLARIMETER["cal_folder"] = __file__[:-9] + r"calibrations\polarimeter"
CONF_POLARIMETER['BS_ref'] = "Beam_Splitter_Ref.npz"
CONF_POLARIMETER['BS_trans'] = "Beam_Splitter_Trans.npz"
CONF_POLARIMETER['ref_mirror'] = "Ref_Mirror.npz"
CONF_POLARIMETER['objective_trans'] = "Objective_Trans.npz"
CONF_POLARIMETER["filter_tol"] = 0


CONF_POLARIMETER_633 = {}
CONF_POLARIMETER_633['cal_file'] = "Polarimeter.npz"
CONF_POLARIMETER_633["cal_folder"] = __file__[:-9] + r"calibrations\polarimeter633"
CONF_POLARIMETER_633['BS_ref'] = None
CONF_POLARIMETER_633['BS_trans'] = None
CONF_POLARIMETER_633['ref_mirror'] = None
CONF_POLARIMETER_633['objective_trans'] = None
CONF_POLARIMETER_633["filter_tol"] = 1e-10


CONF_POLARIMETER_ZABER = {}
CONF_POLARIMETER_ZABER['cal_file'] = "Polarimeter.npz"
CONF_POLARIMETER_ZABER["cal_folder"] = __file__[:-9] + r"calibrations\polarimeter_zaber"
CONF_POLARIMETER_ZABER['BS_ref'] = None
CONF_POLARIMETER_ZABER['BS_trans'] = None
CONF_POLARIMETER_ZABER['ref_mirror'] = None
CONF_POLARIMETER_ZABER['objective_trans'] = None
CONF_POLARIMETER_ZABER["filter_tol"] = 1e-10

# CLUR laser
CONF_CLUR = {}
CONF_CLUR['power_cal_file'] = 'Power_Calibration.npz'
CONF_CLUR['polarization_cal_file'] = 'Polarization_Calibration.npz'
CONF_CLUR['planar_cal_file'] = 'Planarity_Calibration.npz'
CONF_CLUR['power_ref_file'] = 'Power_Ref.npz'
CONF_CLUR['sample_list_file'] = 'Sample'
CONF_CLUR['rep_rate'] = 1e3 # Hz
CONF_CLUR['time_to_measure'] = 0.2 # Seconds to stabilize after measuring
CONF_CLUR['cam_integration_threshold'] = 5 # Seconds to stabilize after measuring
CONF_CLUR['cal_folder'] = r'C:\Users\CLUR AOCG\Bitbucket\py_lab\py_lab\calibrations\clur'
CONF_CLUR['data_folder'] = r"C:\Users\CLUR AOCG\Bitbucket\py_lab\py_lab\setups\CLUR_laser\Data"

###########################################
## CONSTANTS
###########################################

# Polarization theory

"""""
#Tetraedro Method
PSA_az = np.array([0.,         0.95531662, 2.04845464, 2.04845464, 0.,         0.95531662,
 2.04845464, 2.04845464, 0.,         0.95531662, 2.04845464, 2.04845464,
 0.,         0.95531662, 2.04845464, 2.04845464      
])
PSA_el = np.array([0.,          0.,          0.47765831, -0.47765831,  0.,          0.,
  0.47765831, -0.47765831,  0.,          0.,          0.47765831, -0.47765831,
  0.,          0.,          0.47765831, -0.47765831,
]) 
PSG_az = np.array([0.,        0.,         0.,         0.,         0.95531662, 0.95531662,
 0.95531662, 0.95531662, 2.04845464, 2.04845464, 2.04845464, 2.04845464,
 2.04845464, 2.04845464, 2.04845464, 2.04845464
]) 
PSG_el = np.array([ 0.,          0.,          0.,          0.,          0.,          0.,
  0.,          0.,         0.47765831,  0.47765831,  0.47765831,  0.47765831,
 -0.47765831, -0.47765831, -0.47765831, -0.47765831    
]) 


angle_P1 = np.array([
        0, 0, 0, 0, 54.73561032, 54.73561032, 54.73561032, 54.73561032,
        90, 90, 90, 90, 144.73561032, 144.73561032, 144.73561032, 144.73561032
])*degrees
angle_Q1 = np.array([
        0.        ,   0.        ,   0.        ,   0.        ,
        54.73561032,  54.73561032,  54.73561032,  54.73561032,
       117.36780516, 117.36780516, 117.36780516, 117.36780516,
       117.36780516, 117.36780516, 117.36780516, 117.36780516
])*degrees
angle_Q2 = np.array([
         0.        ,  54.73561032, 117.36780516, 117.36780516,
         0.        ,  54.73561032, 117.36780516, 117.36780516,
         0.        ,  54.73561032, 117.36780516, 117.36780516,
         0.        ,  54.73561032, 117.36780516, 117.36780516
])*degrees
angle_P2 = np.array([
         0.        ,  54.73561032, 144.73561032,  90.        ,
         0.        ,  54.73561032, 144.73561032,  90.        ,
         0.        ,  54.73561032, 144.73561032,  90.        ,
         0.        ,  54.73561032, 144.73561032,  90.        
])*degrees

Mueller_indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] #Tetraedro Method
"""""


#Icosaedro Method
PSG_az = np.array([0.78539816, 0.78539816, 0.78539816, 0.78539816, 0.78539816,
       0.78539816, 0.78539816, 0.78539816, 0.78539816, 0.78539816,
       0.78539816, 0.78539816, 2.35619449, 2.35619449, 2.35619449,
       2.35619449, 2.35619449, 2.35619449, 2.35619449, 2.35619449,
       2.35619449, 2.35619449, 2.35619449, 2.35619449, 0.78539816,
       0.78539816, 0.78539816, 0.78539816, 0.78539816, 0.78539816,
       0.78539816, 0.78539816, 0.78539816, 0.78539816, 0.78539816,
       0.78539816, 2.35619449, 2.35619449, 2.35619449, 2.35619449,
       2.35619449, 2.35619449, 2.35619449, 2.35619449, 2.35619449,
       2.35619449, 2.35619449, 2.35619449, 0.50861098, 0.50861098,
       0.50861098, 0.50861098, 0.50861098, 0.50861098, 0.50861098,
       0.50861098, 0.50861098, 0.50861098, 0.50861098, 0.50861098,
       1.06218534, 1.06218534, 1.06218534, 1.06218534, 1.06218534,
       1.06218534, 1.06218534, 1.06218534, 1.06218534, 1.06218534,
       1.06218534, 1.06218534, 2.63298167, 2.63298167, 2.63298167,
       2.63298167, 2.63298167, 2.63298167, 2.63298167, 2.63298167,
       2.63298167, 2.63298167, 2.63298167, 2.63298167, 2.07940731,
       2.07940731, 2.07940731, 2.07940731, 2.07940731, 2.07940731,
       2.07940731, 2.07940731, 2.07940731, 2.07940731, 2.07940731,
       2.07940731, 0.        , 0.        , 0.        , 0.        ,
       0.        , 0.        , 0.        , 0.        , 0.        ,
       0.        , 0.        , 0.        , 1.57079633, 1.57079633,
       1.57079633, 1.57079633, 1.57079633, 1.57079633, 1.57079633,
       1.57079633, 1.57079633, 1.57079633, 1.57079633, 1.57079633,
       0.        , 0.        , 0.        , 0.        , 0.        ,
       0.        , 0.        , 0.        , 0.        , 0.        ,
       0.        , 0.        , 1.57079633, 1.57079633, 1.57079633,
       1.57079633, 1.57079633, 1.57079633, 1.57079633, 1.57079633,
       1.57079633, 1.57079633, 1.57079633, 1.57079633]) 

PSG_el = np.array([0.50861098,  0.50861098,  0.50861098,  0.50861098,  0.50861098,
        0.50861098,  0.50861098,  0.50861098,  0.50861098,  0.50861098,
        0.50861098,  0.50861098,  0.50861098,  0.50861098,  0.50861098,
        0.50861098,  0.50861098,  0.50861098,  0.50861098,  0.50861098,
        0.50861098,  0.50861098,  0.50861098,  0.50861098, -0.50861098,
       -0.50861098, -0.50861098, -0.50861098, -0.50861098, -0.50861098,
       -0.50861098, -0.50861098, -0.50861098, -0.50861098, -0.50861098,
       -0.50861098, -0.50861098, -0.50861098, -0.50861098, -0.50861098,
       -0.50861098, -0.50861098, -0.50861098, -0.50861098, -0.50861098,
       -0.50861098, -0.50861098, -0.50861098,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.        ,  0.        ,  0.        ,  0.        ,
        0.        ,  0.27678718,  0.27678718,  0.27678718,  0.27678718,
        0.27678718,  0.27678718,  0.27678718,  0.27678718,  0.27678718,
        0.27678718,  0.27678718,  0.27678718,  0.27678718,  0.27678718,
        0.27678718,  0.27678718,  0.27678718,  0.27678718,  0.27678718,
        0.27678718,  0.27678718,  0.27678718,  0.27678718,  0.27678718,
       -0.27678718, -0.27678718, -0.27678718, -0.27678718, -0.27678718,
       -0.27678718, -0.27678718, -0.27678718, -0.27678718, -0.27678718,
       -0.27678718, -0.27678718, -0.27678718, -0.27678718, -0.27678718,
       -0.27678718, -0.27678718, -0.27678718, -0.27678718, -0.27678718,
       -0.27678718, -0.27678718, -0.27678718, -0.27678718]) 
       
PSA_az = np.array([
    0.78539816, 2.35619449, 0.78539816, 2.35619449, 0.50861098,
       1.06218534, 2.63298167, 2.07940731, 0.        , 1.57079633,
       0.        , 1.57079633, 0.78539816, 2.35619449, 0.78539816,
       2.35619449, 0.50861098, 1.06218534, 2.63298167, 2.07940731,
       0.        , 1.57079633, 0.        , 1.57079633, 0.78539816,
       2.35619449, 0.78539816, 2.35619449, 0.50861098, 1.06218534,
       2.63298167, 2.07940731, 0.        , 1.57079633, 0.        ,
       1.57079633, 0.78539816, 2.35619449, 0.78539816, 2.35619449,
       0.50861098, 1.06218534, 2.63298167, 2.07940731, 0.        ,
       1.57079633, 0.        , 1.57079633, 0.78539816, 2.35619449,
       0.78539816, 2.35619449, 0.50861098, 1.06218534, 2.63298167,
       2.07940731, 0.        , 1.57079633, 0.        , 1.57079633,
       0.78539816, 2.35619449, 0.78539816, 2.35619449, 0.50861098,
       1.06218534, 2.63298167, 2.07940731, 0.        , 1.57079633,
       0.        , 1.57079633, 0.78539816, 2.35619449, 0.78539816,
       2.35619449, 0.50861098, 1.06218534, 2.63298167, 2.07940731,
       0.        , 1.57079633, 0.        , 1.57079633, 0.78539816,
       2.35619449, 0.78539816, 2.35619449, 0.50861098, 1.06218534,
       2.63298167, 2.07940731, 0.        , 1.57079633, 0.        ,
       1.57079633, 0.78539816, 2.35619449, 0.78539816, 2.35619449,
       0.50861098, 1.06218534, 2.63298167, 2.07940731, 0.        ,
       1.57079633, 0.        , 1.57079633, 0.78539816, 2.35619449,
       0.78539816, 2.35619449, 0.50861098, 1.06218534, 2.63298167,
       2.07940731, 0.        , 1.57079633, 0.        , 1.57079633,
       0.78539816, 2.35619449, 0.78539816, 2.35619449, 0.50861098,
       1.06218534, 2.63298167, 2.07940731, 0.        , 1.57079633,
       0.        , 1.57079633, 0.78539816, 2.35619449, 0.78539816,
       2.35619449, 0.50861098, 1.06218534, 2.63298167, 2.07940731,
       0.        , 1.57079633, 0.        , 1.57079633]) 

PSA_el = np.array([0.50861098,  0.50861098, -0.50861098, -0.50861098,  0.,
        0.        ,  0.        ,  0.        ,  0.27678718,  0.27678718,
       -0.27678718, -0.27678718,  0.50861098,  0.50861098, -0.50861098,
       -0.50861098,  0.        ,  0.        ,  0.        ,  0.        ,
        0.27678718,  0.27678718, -0.27678718, -0.27678718,  0.50861098,
        0.50861098, -0.50861098, -0.50861098,  0.        ,  0.        ,
        0.        ,  0.        ,  0.27678718,  0.27678718, -0.27678718,
       -0.27678718,  0.50861098,  0.50861098, -0.50861098, -0.50861098,
        0.        ,  0.        ,  0.        ,  0.        ,  0.27678718,
        0.27678718, -0.27678718, -0.27678718,  0.50861098,  0.50861098,
       -0.50861098, -0.50861098,  0.        ,  0.        ,  0.        ,
        0.        ,  0.27678718,  0.27678718, -0.27678718, -0.27678718,
        0.50861098,  0.50861098, -0.50861098, -0.50861098,  0.        ,
        0.        ,  0.        ,  0.        ,  0.27678718,  0.27678718,
       -0.27678718, -0.27678718,  0.50861098,  0.50861098, -0.50861098,
       -0.50861098,  0.        ,  0.        ,  0.        ,  0.        ,
        0.27678718,  0.27678718, -0.27678718, -0.27678718,  0.50861098,
        0.50861098, -0.50861098, -0.50861098,  0.        ,  0.        ,
        0.        ,  0.        ,  0.27678718,  0.27678718, -0.27678718,
       -0.27678718,  0.50861098,  0.50861098, -0.50861098, -0.50861098,
        0.        ,  0.        ,  0.        ,  0.        ,  0.27678718,
        0.27678718, -0.27678718, -0.27678718,  0.50861098,  0.50861098,
       -0.50861098, -0.50861098,  0.        ,  0.        ,  0.        ,
        0.        ,  0.27678718,  0.27678718, -0.27678718, -0.27678718,
        0.50861098,  0.50861098, -0.50861098, -0.50861098,  0.        ,
        0.        ,  0.        ,  0.        ,  0.27678718,  0.27678718,
       -0.27678718, -0.27678718,  0.50861098,  0.50861098, -0.50861098,
       -0.50861098,  0.        ,  0.        ,  0.        ,  0.        ,
        0.27678718,  0.27678718, -0.27678718, -0.27678718]) 


angle_P1 = np.array([15.85873721,  15.85873721,  15.85873721,  15.85873721,
        15.85873721,  15.85873721,  15.85873721,  15.85873721,
        15.85873721,  15.85873721,  15.85873721,  15.85873721,
       105.85873721, 105.85873721, 105.85873721, 105.85873721,
       105.85873721, 105.85873721, 105.85873721, 105.85873721,
       105.85873721, 105.85873721, 105.85873721, 105.85873721,
        74.14126279,  74.14126279,  74.14126279,  74.14126279,
        74.14126279,  74.14126279,  74.14126279,  74.14126279,
        74.14126279,  74.14126279,  74.14126279,  74.14126279,
       164.14126279, 164.14126279, 164.14126279, 164.14126279,
       164.14126279, 164.14126279, 164.14126279, 164.14126279,
       164.14126279, 164.14126279, 164.14126279, 164.14126279,
        29.14126279,  29.14126279,  29.14126279,  29.14126279,
        29.14126279,  29.14126279,  29.14126279,  29.14126279,
        29.14126279,  29.14126279,  29.14126279,  29.14126279,
        60.85873721,  60.85873721,  60.85873721,  60.85873721,
        60.85873721,  60.85873721,  60.85873721,  60.85873721,
        60.85873721,  60.85873721,  60.85873721,  60.85873721,
       150.85873721, 150.85873721, 150.85873721, 150.85873721,
       150.85873721, 150.85873721, 150.85873721, 150.85873721,
       150.85873721, 150.85873721, 150.85873721, 150.85873721,
       119.14126279, 119.14126279, 119.14126279, 119.14126279,
       119.14126279, 119.14126279, 119.14126279, 119.14126279,
       119.14126279, 119.14126279, 119.14126279, 119.14126279,
       164.14126279, 164.14126279, 164.14126279, 164.14126279,
       164.14126279, 164.14126279, 164.14126279, 164.14126279,
       164.14126279, 164.14126279, 164.14126279, 164.14126279,
        74.14126279,  74.14126279,  74.14126279,  74.14126279,
        74.14126279,  74.14126279,  74.14126279,  74.14126279,
        74.14126279,  74.14126279,  74.14126279,  74.14126279,
        15.85873721,  15.85873721,  15.85873721,  15.85873721,
        15.85873721,  15.85873721,  15.85873721,  15.85873721,
        15.85873721,  15.85873721,  15.85873721,  15.85873721,
       105.85873721, 105.85873721, 105.85873721, 105.85873721,
       105.85873721, 105.85873721, 105.85873721, 105.85873721,
       105.85873721, 105.85873721, 105.85873721, 105.85873721])* degrees

angle_Q1 = np.array([45.        ,  45.        ,  45.        ,  45.,
        45.        ,  45.        ,  45.        ,  45.        ,
        45.        ,  45.        ,  45.        ,  45.        ,
       135.        , 135.        , 135.        , 135.        ,
       135.        , 135.        , 135.        , 135.        ,
       135.        , 135.        , 135.        , 135.        ,
        45.        ,  45.        ,  45.        ,  45.        ,
        45.        ,  45.        ,  45.        ,  45.        ,
        45.        ,  45.        ,  45.        ,  45.        ,
       135.        , 135.        , 135.        , 135.        ,
       135.        , 135.        , 135.        , 135.        ,
       135.        , 135.        , 135.        , 135.        ,
        29.14126279,  29.14126279,  29.14126279,  29.14126279,
        29.14126279,  29.14126279,  29.14126279,  29.14126279,
        29.14126279,  29.14126279,  29.14126279,  29.14126279,
        60.85873721,  60.85873721,  60.85873721,  60.85873721,
        60.85873721,  60.85873721,  60.85873721,  60.85873721,
        60.85873721,  60.85873721,  60.85873721,  60.85873721,
       150.85873721, 150.85873721, 150.85873721, 150.85873721,
       150.85873721, 150.85873721, 150.85873721, 150.85873721,
       150.85873721, 150.85873721, 150.85873721, 150.85873721,
       119.14126279, 119.14126279, 119.14126279, 119.14126279,
       119.14126279, 119.14126279, 119.14126279, 119.14126279,
       119.14126279, 119.14126279, 119.14126279, 119.14126279,
         0.        ,   0.        ,   0.        ,   0.        ,
         0.        ,   0.        ,   0.        ,   0.        ,
         0.        ,   0.        ,   0.        ,   0.        ,
        90.        ,  90.        ,  90.        ,  90.        ,
        90.        ,  90.        ,  90.        ,  90.        ,
        90.        ,  90.        ,  90.        ,  90.        ,
         0.        ,   0.        ,   0.        ,   0.        ,
         0.        ,   0.        ,   0.        ,   0.        ,
         0.        ,   0.        ,   0.        ,   0.        ,
        90.        ,  90.        ,  90.        ,  90.        ,
        90.        ,  90.        ,  90.        ,  90.        ,
        90.        ,  90.        ,  90.        ,  90.])* degrees

angle_Q2 = np.array([45.        , 135.        ,  45.        , 135.,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.        ,
        45.        , 135.        ,  45.        , 135.        ,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
         0.        ,  90.        ,   0.        ,  90.])* degrees

angle_P2 = np.array([74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279,
        74.14126279, 164.14126279,  15.85873721, 105.85873721,
        29.14126279,  60.85873721, 150.85873721, 119.14126279,
        15.85873721, 105.85873721, 164.14126279,  74.14126279])* degrees


"""
#Método Hoyo Mueller
PSA_az = np.array([
    0, 90, 90, 0, 0, 0, 0, 45, 90, 90, 0, 0, 0, 135, 90, 90, 0, 45, 0, 60, 60, 45, 0, 45
]) * degrees
PSA_el = np.array([
    0, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0,
    - 45, 0, 0, 0, 45, 0, -45, -30, 30, 0, 45, 0
]) * degrees
PSG_az = np.array([
    0, 0, 90, 90, 0, 45, 0, 0, 0, 45, 0, 135, 0, 0, 0, 135, 0, 45, 45, 90, 90, 45, 45, 135
]) * degrees
PSG_el = np.array([
    0, 0, 0, 0, -45, 0, 0, 0, -45, 0, 45, 0, 0, 0, 45, 0,
    - 45, 0, 0, 0, 0, 45, 45, -45
]) * degrees

angle_P1 = np.array([
    0, 0, 90, 90, 45, 45, 0, 0, 45, 45, 135, 135, 0, 0, 135, 135, 45, 45, 45, 90, 90, 0, 0, 0
])* degrees
angle_Q1 = np.array([
    0, 0, 90, 90, 0, 45, 0, 0, 0, 45, 0, 135, 0, 0, 0, 135, 0, 45, 45, 90, 90, 45, 45, 135
])* degrees
angle_Q2 = np.array([
    0, 90, 90, 0, 0, 0, 0, 45, 90, 90, 0, 0, 0, 135, 90, 90, 0, 45, 0, 60, 60, 45, 0, 45
])* degrees
angle_P2 = np.array([
    0, 90, 90, 0, 0, 0, 45, 45, 90, 90, 0, 0, 135, 135, 90, 90, 45, 45, 135, 30, 90, 45, 45, 45
])* degrees

Mueller_indices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 17, 18, 21, 22, 23] #Método Hoyo Mueller
"""



"""
#
# angle_P1_mirror = np.array([
#     0, 0, 90, 90, 135, 135, 0, 0, 135, 135, 45, 45, 0, 0, 45, 45, 135, 135,
#     135, 90, 90
# ])
# angle_Q1_mirror = np.array([
#     0, 0, 90, 90, 0, 135, 0, 0, 0, 135, 0, 45, 0, 0, 0, 45, 0, 135, 135, 90, 90
# ])
# angle_Q2_mirror = np.array([
#     0, 90, 90, 0, 0, 0, 0, 135, 90, 90, 0, 0, 0, 45, 90, 90, 0, 135, 0, 120,
#     120
# ])
# angle_P2_mirror = np.array([
#     0, 90, 90, 0, 0, 0, 135, 135, 90, 90, 0, 0, 45, 45, 90, 90, 135, 135, 45,
#     150, 90
# ])

System_Mueller = np.array([
    [ 1,  1,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,], #I0
    [ 1,  1,  0,  0, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,], #I1
    [ 1, -1,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,], #I2
    [ 1, -1,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,], #I3
    [ 1,  0,  0, -1,  1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,], #I4
    [ 1,  0,  1,  0,  1,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,], #I5
    [ 1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  0,  0,], #I6
    [ 1,  1,  0,  0,  0,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,], #I7
    [ 1,  0,  0, -1, -1,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,], #I8
    [ 1,  0,  1,  0, -1,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,], #I9
    [ 1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0, -1,], #I16
    [ 1,  0,  1,  0,  0,  0,  0,  0,  1,  0,  1,  0,  0,  0,  0,  0,], #I17
    [ 1,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0, -1,  0,], #I18
    [ 1,  0,  0,  1,  0,  0,  0,  0,  1,  0,  0,  1,  0,  0,  0,  0,], #I21
    [ 1,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  1,], #I22
    [ 1,  0,  0, -1,  0,  0,  0,  0,  1,  0,  0, -1,  0,  0,  0,  0,], #I23
]) / 4 
"""