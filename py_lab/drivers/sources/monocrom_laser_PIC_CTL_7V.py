#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Control for laser:
MONOCROM  Láser Monocrom PIC_CTL_7V

It is controlled by serial port

There is only one function:
R_PCO_xxx where xxx= 0, 1, ... 1023


"""

# # FUNCIONA

import time

import serial
import serial.tools.list_ports


class PIC_CTL_7V():
    def __init__(self):
        self.serial = None

    def __del__(self):
        self.close()

    def close(self):
        self.set_power(power=0)
        self.serial.close()

    def get_port_list(self):
        list = serial.tools.list_ports.comports()
        connected = []
        for element in list:
            connected.append(element.device)
        print("Connected COM ports: " + str(connected))
        return list

    def open_laser(self, port):
        self.serial = serial.Serial(port=port, baudrate=9600, parity=serial.PARITY_NONE,
                                    stopbits=serial.STOPBITS_ONE, bytesize=serial.EIGHTBITS,  xonxoff=False)

    def set_power(self, power=512, verbose=False):
        """Set the power of the laser"""
        if power > 1023:
            power = 1023
        elif power < 0:
            power = 0

        power = int(power)

        code = "R_PCO_"

        texto = code + str(power) + "\r"

        if verbose:
            print(texto)

        try:
            self.serial.write(texto.encode("utf-8"))  # works
        except self.serial.SerialException:
            print('Port is not available')
        except self.serial.portNotOpenError:
            print('Attempting to use a port that is not open')
            print('End of script')

        if verbose:
            out = self.serial.read_all()
            time.sleep(.25)
            print(out)
