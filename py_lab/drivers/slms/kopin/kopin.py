import time

#import devices_ctrl

from diffractio import np, plt, sp
from diffractio import um, mm, degrees
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.utils_optics import field_parameters


IMAGE_SZ = 67500  # tamaño del vector imagen: 300x225 píxels activos (visibles)
Hp = 300  # Número de píxels activos en horizontal (num px por fila)
Vp = 225  # Número de píxels activos en vertical (num filas)

seconds = 1.
pixels = pixel_size = 11 * um
num_pixels = np.array((Hp, Vp), dtype=int)
num_pixels_min = np.min(num_pixels)
num_pixels_max = np.max(num_pixels)
num_pixels_total = num_pixels[0] * num_pixels[1]

slm_size = np.array(num_pixels * pixel_size)
slm_size_min = np.min(slm_size)
slm_size_max = np.max(slm_size)

x = np.linspace(-pixel_size * num_pixels[0] / 2,
                pixel_size * num_pixels[0] / 2, num_pixels[0])
y = np.linspace(-pixel_size * num_pixels[1] / 2,
                pixel_size * num_pixels[1] / 2, num_pixels[1])
wavelength = 0.6328 * um


class Kopin(object):
    """Class for working kopin SLM.

    Parameters:
        x (numpy.array): linear array with equidistant positions. The number of data is preferibly :math:`2^n` .
        y (numpy.array): linear array wit equidistant positions for y values
        wavelength (float): wavelength of the incident field
        info (str): String with info about the simulation

    Attributes:
        self.x (numpy.array): linear array with equidistant positions. The number of data is preferibly :math:`2^n` .
        self.y (numpy.array): linear array wit equidistant positions for y values
        self.wavelength (float): wavelength of the incident field.
        self.u (numpy.array): (x,z) complex field
        self.info (str): String with info about the simulation
    """

    def __init__(self,
                 device='CYDI_E6625C05E7143E25',
                 wavelength=None,
                 info="",
                 verbose=True):
        """_summary_

        Args:
            wavelength (_type_, optional): _description_. Defaults to None.
            info (str, optional): _description_. Defaults to "".
        """
        self.x = x
        self.y = y
        self.wavelength = wavelength

        self.mask = Scalar_mask_XY(x, y, wavelength, info)

        dvcs_obj = devices_ctrl.Devices()
        dvcs = dvcs_obj.update_dvcs()

        if verbose:
            print(dvcs)

        dvc = dvcs[device]
        dvc.ser.open()

        self.dvcs = dvcs
        self.dvc = dvc
        self.led_on(duration=3 * seconds)

    def led_on(self, duration=3 * seconds):
        """_summary_

        Args:
            duration (_type_): _description_ 
        """

        self.dvc.set_led_status(True)
        time.sleep(duration)
        self.dvc.set_led_status(False)

    def send_image(self, is_diffractio=True, kind='intensity', duration=0 * seconds, test=0, has_draw=False):
        """Send an image to the kopin SLM.

        Args:
            kind (str, optional): (intensity, amplitude, phase). Defaults to 'intensity'.
            duration (float, optional): time the image is placed at SLM, if 0 it is not deleted. Defaults to 0*seconds.
            test (int, optional): (1-5) Send a predefined image. Defaults to 0.
            has_draw (bool, optional): Draw the image to send. Defaults to False.
        """
        """""
        match test:
            case 1:
                print("circle")
                self.mask.circle(r0=(0, 0), radius=(slm_size_max / 2, slm_size_min / 2))
            
            case 2:
                print("square")
                self.mask.square(r0=(0,0), size=(2100,2100))

            case 3:
                print("chess small")
                self.mask.grating_2D_chess(r0=(0,0), period=80*pixels, fill_factor=0.5, angle=0*degrees)

            case 4:
                print("chess small")
                self.mask.grating_2D_chess(r0=(0,0), period=Hp/2*pixels, fill_factor=0.5, angle=0*degrees)
            
            case 4:
                print("ring")
                self.mask.ring(r0=(0,0), radius1=50*pixels, radius2=100*pixels, angle=0 * degrees)

            case 5:
                print("square as a ")
                u1=self.mask.duplicate(clear=True)
                u1.square(r0=(0, 0), size= 200*pixels)

                u2=self.mask.duplicate(clear=True)
                u2.square(r0=(0, 0), size=100*pixels)

                u3=self.mask.duplicate(clear=True)
                u3.u = u2.u-u1.u
                self.mask = u3
        """""
        if is_diffractio:
            amplitude, intensity, phase = field_parameters(self.mask.u)

            if kind == 'intensity':
                image_to_send = intensity

            elif kind == 'amplitude':
                image_to_send = amplitude

            elif kind == 'phase':
                # TODO: comprobar que funciona bien y está entre 0 y 255 cuando -pi, pi
                print(phase.min(), phase.max())

                phase = phase
                image_to_send = phase / (2 * np.pi)
                print(image_to_send.min(), image_to_send.max())
    
        else:
            image_to_send = np.abs(self.mask.u)

        intensity_integers = (image_to_send * 255).astype(np.uint8)

        self.dvc.load_image(intensity_integers)

        if has_draw is not False:
            plt.figure()
            plt.imshow(image_to_send)
            plt.colorbar()
            print(image_to_send.min(), image_to_send.max())

        if duration > 0 * seconds:
            time.sleep(duration)
            intensity_zeros = np.zeros_like(image_to_send).astype(np.uint8)
            self.dvc.load_image(intensity_zeros)

    def close(self):
        """Cierra el puerto en serie
        """
        self.dvc.ser.close()