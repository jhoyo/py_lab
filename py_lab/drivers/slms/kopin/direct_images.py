import numpy as np

IMAGE_SZ = 67500  # tamaño del vector imagen: 300x225 píxels activos (visibles)
Hp = 300  # Número de píxels activos en horizontal (num px por fila)
Vp = 225  # Número de píxels activos en vertical (num filas)


def linearize_img(image):
    """Convierte un array bidimensional imagen en un array lineal por filas

    Args:
        image (np.array(225,300)): imagen

    Returns:
        np.array(67500): array
    """
    #return image.flatten('C').tobytes()    # 'C' row, 'F' column
    return image.flatten('C')  # 'C' row, 'F' column


def build_img_generic_1():
    """Imagen de prueba genérica. Devuelve un array numpy bidimensional con la imagen

    Returns:
        np.array(225,300): imagen
    """
    image = np.zeros((Vp, Hp), dtype=np.uint8)
    for v in range(Vp):
        for h in range(Hp):
            # la imagen será un marco de 1 píxel con una cruz en el centro:

            # ponemos a 1 el primer y el último píxel de cada columna
            if (h == 0 or h == (Hp - 1)):
                image[v, h] = 0xFF

            # ponemos a 1 la primera y la última fila
            if v == 0 or v == (Vp - 1):
                image[v, h] = 0xFF

            # ponemos una cruz centrada
            if h == 150 or v == 112:
                image[v, h] = 0xFF

    return image


def test_image_direct_1():
    """Imagen de prueba 1:   1 0 1 1 1 0 1 0 1 ..... 0 1 0 1 0 1 0 .... 1 1 0 0 1

    Returns:
        np.array(225,300): imagen
    """

    image = bytearray(IMAGE_SZ)
    for indx in range(IMAGE_SZ):
        if indx % 2 == 0:
            image[indx] = 0xFF

    image[0] = 0xFF
    image[1] = 0x00
    image[2] = 0xFF
    image[3] = 0xFF
    image[4] = 0xFF
    image[67495] = 0xFF
    image[67496] = 0xFF
    image[67497] = 0x00
    image[67498] = 0xFF
    image[67499] = 0x00

    return image


def test_image_direct_2():
    """Imagen de prueba 2:  

    Returns:
        np.array(225,300): imagen
    """

    image = bytearray(IMAGE_SZ)
    n_pixel = 0  # Índice para recorrer los píxeles de la imagen, de 0 a IMAGE_SZ-1

    for V in range(225):  # Cada imagen tiene 225 filas
        for H in range(300):  # Cada fila tiene 300 píxels
            # ponemos a 1 el primer y el último píxel de cada fila
            if H == 0 or H == 299:
                image[n_pixel] = 0xFF
            else:
                image[n_pixel] = 0x00
            n_pixel += 1
    return image


def test_image_direct_3():
    """Imagen de prueba 3:  escala de grises

    Returns:
        np.array(225,300): imagen
    """
    image = bytearray(IMAGE_SZ)
    n_pixel = 0  # Índice para recorrer los píxeles de la imagen, de 0 a IMAGE_SZ-1

    for V in range(225):  # Cada imagen tiene 225 filas
        for H in range(300):  # Cada fila tiene 300 píxels
            # ponemos a 1 el primer y el último píxel de cada fila
            image[n_pixel] = V

            n_pixel += 1
    return image
