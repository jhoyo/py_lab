"""
Este módulo gestiona el conjunto de dispositivos CyDi

"""
# import serial
from serial.tools import list_ports
import comms_cydi   # Proporciona las funciones de comunicación con cada dispositivo

class Devices(object):
    SERIAL_CYDY =   'CYDI_' # cadena a buscar el comienzo del string Serial de USB  

    # Poner los números PID/VID en hexadecimal como string sin 0x. Ej: 0x4001 --> "4001"
    # Usaremos estos números para filtrar el campo hwid con grep
    PID =   "4001"    # 0x4001
    VID =   "2E8A"    # 0x2E8A - Raspberry Pi vendor ID
    
    def __init__(self):
        # lista todos los dispositivos conectados, contiene la referencia al objeto serial
        self._present_dvcs_tmp = {}   # {serial_number : objeto_serial}
        self.present_dvcs = {}        # {serial_number : objeto_serial}

    # Para crear un demonio: https://stackoverflow.com/questions/21050671/how-to-check-if-device-is-connected-pyserial
  
    def update_dvcs(self):
        """
        Listo todos los puertos serial que hay y filtro aquellos cuyo serial_number comience por el valor de
        SERIAL_CYDY, los resultantes serán dispositivos CyDi.
        Devuelve un diccionario con los dispositivos CyDi conectados al PC. Cada entrada del diccionario se
        corresponderá con un dispositivo, siendo la clave su serial_number y el valor su objeto serial. El puerto 
        serial de todos los dispositivos estará cerrado.
        """
        all_ports = list_ports.grep(self.VID + ':' + self.PID) # lista todos los puertos con el par "VID:PID"
        
        self._present_dvcs_tmp.clear()   # {serial_number : objeto_serial} Dispositivos CyDi actualmente conectados, temporal
        for port in all_ports:
            if str(port.serial_number).startswith(self.SERIAL_CYDY):
                # Se trata de un dispositivo CyDi

                # Recorremos los dispositivos que se encontraron en la última invocación a esta función para seguir 
                # usando el mismo objeto serial en aquellos dispositivos que sigan conectados
                new_dvc = True  
                for key in self.present_dvcs.keys():
                    if key == str(port.serial_number):
                        # El dispositivo ya estaba la última vez que se invocó update_dvcs()

                        # Añadimos el dispositivo a la lista temporal de dispositivos conectados
                        self._present_dvcs_tmp[key] = self.present_dvcs[key]

                        new_dvc = False  # El dispositivo ya estaba en la última invocación a update_dvcs()
                        break
                if new_dvc:
                    # El dispositivo no estaba la última vez que se invocó update_dvcs()
                    ser_obj = comms_cydi.SerialComms(port.device)   # creamos un objeto serial
                    # Añadimos el dispositivo a la lista temporal de dispositivos conectados
                    self._present_dvcs_tmp[str(port.serial_number)] = ser_obj

        self.present_dvcs = self._present_dvcs_tmp.copy()
        return self.present_dvcs

    def get_dvcs(self):
        """
        Devuelve los dispositivos encontrados en la última invocación a update_dvcs() sin realizar una nueva búsqueda 
        """
        return self.present_dvcs