"""
Módulo que gestiona las comunicaciones a través del bus USB-CDC.
Gestiona la comunicación con dispositivos de forma individual
"""
import serial
from serial.tools import list_ports
import time


class SerialComms(object):

    # Opcodes de las instrucciones serial
    TEST = 0x05
    ACK = b'\x06'  # TODO poner esto bien
    NACK = 0x07
    TERMINADOR_MSG = 0x10

    LOAD_IMAGE = 0x20  # Enviamos la imagen a la placa

    LED_ON = 0x40  # Activamos el led de la placa
    LED_OFF = 0x41  # Desactivamos el led de la placa

    # Estados del resultado de una transacción serial
    OK = 0  # La conexión al puerto ha sido correcta en la transacción
    NOT_OK = -1  # No ha sido posible la conexión al puerto en la transacción

    # Constructor. Crea el objeto puerto serie asociado al puerto pasado como parámetro.
    # El puerto queda cerrado. Se abrirá en cada transacción y a continuación se volverá a cerrar
    def __init__(self, port):
        self.ser = serial.Serial()  # Creamos una instancia Serial sin abrir
        self.ser.baudrate = 12_000_000
        self.ser.bytesize = 8
        self.ser.parity = 'N'
        self.ser.stopbits = 1
        self.ser.timeout = 0.3  # tiempo en segundos
        self.ser.port = port  # Puerto en el PC, COMx, tty

    def _transaccion(self, msg_out, n_bytes_in):
        """
        Realizamos una transacción de comunicación, enviamos un mensaje y si no_bytes_in  0
            esperamos a recibir un mensaje de respuesta. El puerto debe estar cerrado antes de
            ser invocada la función y quedará cerrado cuando salgamos de ella

        Parámetros:
            msg_out     : array de bytes con mensaje a enviar
            n_bytes_in  : número de bytes que debe tener la respuesta recibida

        Devuelve: 
            El array de bytes con la respuesta recibida si se ha podido establecer la comunicación
            None si no se ha podido establecer la comunicación.
            TODO tener cuenta que también devuelve None si no se espera ninguna respuesta. No existirá
            esta ambiguedad si siempre se solicita al dispositivo devolver como mínimo el ACK
        """
        if isinstance(msg_out, str):
            # write sólo acepta datos de tipo bytes o compatible (bytearray)
            msg_out = msg_out.encode('utf-8')
        try:
            # with self.ser as s:
            #   El puerto se abre y cierra automáticamente
            # s.write(msg_out)
            # response = s.read(n_bytes_in)
            #
            # En esta versión los puertos están siempre abiertos
            self.ser.write(msg_out)
            response = self.ser.read(n_bytes_in)
        except Exception as e:
            # Transacción fallida, no existe el objeto serial o el puerto está abierto por otro programa
            print(e)
            return None
        else:
            if n_bytes_in > 0:
                # Esperamos alguna respuesta
                return response
            else:
                return None

    def test(self):
        """
        Comprueba si puede mantener comunicación con el dispositivo.

        Argumentos:
            Ninguno

        Devuelve:
            True:  se establece la comunicación correctamente
            False: el dispositivo responde, pero no con ACK
            None:  el dispositivo no responde
        """
        response = self._transaccion([self.TEST], 1)
        if (response):
            # Hemos recibido alguna respuesta
            if (response == self.ACK
                ):  # **** PONER LAS DEMAS COMPROBACIONES CON ACK ASÍ ****
                # hemos recibido ACK
                return True
            else:
                # Hemos recibido una respuesta pero no es ACK
                return False
        else:
            # No hemos recibido respuesta
            return None

    def get_device_vers(self):
        """Solicitamos la versión del software del dispositivo.
        # TODO comprobar y hacer crear también en la placa

        Returns:
            str:Devuelve un string con el número de versión
        """

        return self._transaccion([self.GET_DVC_VERS], LENGHT_INFO)

    def set_led_status(self, led_status):
        """
        Activa o desactiva el led del dispositivo.

        Argumentos:
            led_status (bool): true o false para activar o desactivar respectivamente el led

        Devuelve:
            Nada
        """

        if led_status:
            op = self.LED_ON
        else:
            op = self.LED_OFF
        response = self._transaccion([op], 1)
        if (response):
            # Hemos recibido alguna respuesta
            if (response == self.ACK
                ):  # **** PONER LAS DEMAS COMPROBACIONES CON ACK ASÍ ****
                return True
            else:
                # Hemos recibido una respuesta pero no es ACK
                return False
        else:
            # No hemos recibido respuesta
            return None

    def load_image(self, image):
        """
        Enviamos una imagen a la posición 1 de la placa

        Parámetros:
            image: array lineal numpy de 67500 píxels, elementos de tipo uint8
        """
        # Solicitamos a la placa el envío de la imagen
        response = self._transaccion([self.LOAD_IMAGE], 1)

        if (response == self.ACK):
            # Hemos recibido confirmación, podemos enviar la imagen
            # la imagen tiene 67500 píxeles activos, la envío en 18 trozos de 3750 bytes

            indx_rd = 0  # índice de la imagen a partir de la cual enviaremos la siguiente porción
            chunk_sz = 3750  # tamaño de cada uno de los 18 trozos

            for n_chunk in range(18):  # 18 trozos en total, de 0 a 17
                # Enviamos la matriz imagen por filas, cada fila tendrá H = 300pixels
                response = self._transaccion(
                    image.flatten('C')[indx_rd:indx_rd + chunk_sz], 1)
                indx_rd += chunk_sz
                if response != self.ACK:
                    return response  # No hemos recibido ACK, salimos de la función
            return response

    def load_array(self, array):
        """
        Enviamos una imagen a la posición 1 de la placa

        Parámetros:
            image: array lineal numpy de 67500 píxels, elementos de tipo uint8
        """
        # Solicitamos a la placa el envío de la imagen
        response = self._transaccion([self.LOAD_IMAGE], 1)

        if (response == self.ACK):
            # Hemos recibido confirmación, podemos enviar la imagen
            # la imagen tiene 67500 píxeles activos, la envío en 18 trozos de 3750 bytes

            indx_rd = 0  # índice de la imagen a partir de la cual enviaremos la siguiente porción
            chunk_sz = 3750  # tamaño de cada uno de los 18 trozos

            for n_chunk in range(18):  # 18 trozos en total, de 0 a 17
                # Enviamos la matriz imagen por filas, cada fila tendrá H = 300pixels
                response = self._transaccion(array[indx_rd:indx_rd + chunk_sz],
                                             1)
                indx_rd += chunk_sz
                if response != self.ACK:
                    return response  # No hemos recibido ACK, salimos de la función
            return response