# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Communications with ELIS1024 DSP made by Isidoro
"""

import time

import numpy as np
import serial
import serial.tools.list_ports as port_list


def get_port_list():
    list = serial.tools.list_ports.comports()
    connected = []
    for element in list:
        connected.append(element.device)
    print("Connected COM ports: " + str(connected))
    return list


class ELIS1024(object):
    def __init__(self,  sleep_time=0.2):
        self.port = None
        self.sleep_time = sleep_time

    def __str__(self):
        """Represents main data of the atributes."""

        if self.port.is_open:
            print("El DSP port {} is opened".format(self.port_name))
        else:
            print("DSP is not opened")

        return ""

    def __del__(self):
        del self

    def __send_message__(self, message, kind='opcode'):
        """Send a message without parameters:
        Arguments:
            message (int or hex): message. The message can be just a data o several.
            kind (str): 'opcode' of 'hex'

        """

        self.port.reset_output_buffer()

        if kind == 'opcode':
            if isinstance(message, 'int'):
                message = [message]

            self.port.write(bytes(message))

        elif kind == 'hex':
            self.port.write(bytes.fromhex(message))

    def open_serial(self, port='/dev/ttyUSB0'):
        try:
            # Declaración del puerto serie usado en PC
            port = serial.Serial(port)
            self.port = port
            print(port.is_open)
            port.name = 'serial-ELIS1024'  # Nombramos al objeto serial creado
            port.baudrate = 812500  # 812500
            port.parity = serial.PARITY_NONE
            port.stopbits = serial.STOPBITS_ONE
            port.bytesize = serial.EIGHTBITS
            port.timeout = 1
            print("port {} opened".format(port.name))
        except:
            print("cuidado, no puedes abrir el puerto")
            port = None
            self.port = port
            return

        return port

    def image1(self, t_integracion_s):
        """Lee la imagen actual del array 1 con el tiempo de integración pasado como parámetro.

            Arguments:
                t_integracion_s (float): tiempo de integración en segundos. Debe estar comprendido entre 200ns y 203.568ms. Para tiempos de integración inferiores a 0.4094ms utilizamos incrementos de 200ns, para tiempos superiores a 0.4094ms utilizamos incrementos de 3.2us.

            Returns:
                image: array de 1024 elementos. Cada elemento es un entero en el rango 0 .. 4095 que contine la lectura realizada por el ADC de cada pixel
        """

        opcode = 65  # ('0x41') opcode de la operación 'grabarPulsos'
        EOF = 26  # '1A' en hexadecimal

        self.port.reset_output_buffer()
        self.port.reset_input_buffer()

        image = np.zeros(1024)  # Reservamos memoria para el array imagen

        # El tiempo de integracion debe estar comprendido entre 200ns y 203.568ms
        if (t_integracion_s > 203.568e-3):
            t_integracion_s = 203.568e-3  # max =  3.2 um
        if (t_integracion_s < 200E-9):
            t_integracion_s = 200E-9  # min =  200 ns

        # para tiempos de integración inferiores a 0.4094ms utilizamos incrementos de 200ns.
        if (t_integracion_s <= 0.4094e-3):
            wordDSP_time = round(t_integracion_s / 200E-9)
        else:  # para tiempos de integración superiores a 0.4094ms utilizamos incrementos de 3.2us.
            wordDSP_time = round(t_integracion_s / 3.2E-6) + 1920

        arrayHex_time = [int(np.fix(wordDSP_time / 256)),
                         int(np.mod(wordDSP_time, 256))]

        message = [opcode, arrayHex_time[0], arrayHex_time[1]]

        b = self.port.write(bytes(message))
        time.sleep(0.1)
        text = self.port.read_all()
        num_data_received = len(text)

        if num_data_received == 2049:
            i1 = 0
            for i2 in range(0, 2048, 2):
                image[i1] = 16 * text[i2] + text[i2 + 1]
                i1 = i1 + 1

        return image, num_data_received, text

    def image2(self, t_integracion_s):
        """Lee la imagen actual del array 1 con el tiempo de integración pasado como parámetro.
            Arguments:
                t_integracion_s (float): tiempo de integración en segundos. Debe estar comprendido entre 200ns y 203.568ms. Para tiempos de integración inferiores a 0.4094ms utilizamos incrementos de 200ns, para tiempos superiores a 0.4094ms utilizamos incrementos de 3.2us.

            Returns:
                image: array de 1024 elementos. Cada elemento es un entero en el rango 0 .. 4095 que contine la lectura realizada por el ADC de cada pixel.
        """

        opcode = 66  # ('0x41') opcode de la operación 'grabarPulsos'
        EOF = 26  # '1A' en hexadecimal

        self.port.reset_output_buffer()
        self.port.reset_input_buffer()

        image = np.zeros(1024)  # Reservamos memoria para el array imagen

        # El tiempo de integraci�n debe estar comprendido entre 200ns y 203.568ms
        if (t_integracion_s > 203.568e-3):
            t_integracion_s = 203.568e-3  # max =  3.2 um
        if (t_integracion_s < 200E-9):
            t_integracion_s = 200E-9  # min =  200 ns

        # para tiempos de integración inferiores a 0.4094ms utilizamos incrementos de 200ns.
        if (t_integracion_s <= 0.4094e-3):
            wordDSP_time = round(t_integracion_s / 200E-9)
        else:  # para tiempos de integración superiores a 0.4094ms utilizamos incrementos de 3.2us.
            wordDSP_time = round(t_integracion_s / 3.2E-6) + 1920

        arrayHex_time = [int(np.fix(wordDSP_time / 256)),
                         int(np.mod(wordDSP_time, 256))]

        message = [opcode, arrayHex_time[0], arrayHex_time[1]]

        b = self.port.write(bytes(message))
        time.sleep(0.1)
        text = self.port.read_all()
        num_data_received = len(text)
        # print(count)

        if num_data_received == 2049:
            i1 = 0
            for i2 in range(0, 2048, 2):
                image[i1] = 16 * text[i2] + text[i2 + 1]
                i1 = i1 + 1

        return image, num_data_received, text

    def close(self):
        self.port.close()
        self.port = None
