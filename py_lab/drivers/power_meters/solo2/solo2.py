import serial
from time import sleep
import re

matcher_wavelength = re.compile("Active Wavelength (\d+)")
matcher_scale = re.compile("Current Scale (\w+)")

class Solo2(object):

    def __init__(self, port):
        """:param port: Serial port connected to the controller."""
        # self.lock = threading.Lock()
        self.ser = serial.Serial(
            port=port,
            baudrate=115200,
            bytesize=8,
            timeout=1,
            parity='N',
            stopbits=1)

    def __del__(self):
        self.close()

    def close(self):
        self.ser.close()

    def read(self):
        """ Serial read with EOL character removed."""
        str = self.ser.readline()
        # return str[0:-2].decode()
        return str.decode()

    def write(self, string, param=[]):
        """ Serial write.

        The * start character and EOL character is automatically appended"""
        string = "*" + string
        for par in param:
            string = string + " " + str(par)
        string = string + "\r"
        self.ser.write(string.encode())

    def query(self, string):
        """write a command and read the reply.
        """
        self.write(string)
        return self.read()

    def version(self):
        return self.query('VER')
    
    def get_power(self):
        power = self.query("CVU")
        print(power)
        return float(power[16:])
    
    def set_correction_wavelength(self, wavelength):
        self.write("SWA", [wavelength])

    def set_scale(self, scale):
        self.write("SSA", [scale])

    def print_scales(self):
        numbers = ["1", "3", "10", "30", "100", "300"]
        letters = ["p", "n", "u", "m", "", "k"]
        lista = []
        for letter in letters:
            for number in numbers:
                lista.append(number + letter)

        return lista
    
    def set_multipliers(self, mult1=None, mult2=None):
        if mult1 is not None:
            self.write("SMU", [1, mult1])
        if mult2 is not None:
            self.write("SMU", [2, mult2])
            
    def set_offsets(self, off1=None, off2=None):
        if off1 is not None:
            self.write("SOU", [1, off1])
        if off2 is not None:
            self.write("SOU", [2, off2])
            
    def set_trigger_mode(self, cond):
        if cond:
            self.write("SCA", [1])
        else:
            self.write("SCA", [0])
            
            
    def set_background_mode(self, mode):
        self.write("EOA", [mode])
        
    def get_status(self):
        return self.query("STA")
    
    def get_scale(self):
        status = self.get_status()
        result = matcher_scale.findall(status)
        return result[0]
    
    def get_wavelength(self):
        status = self.get_status()
        result = matcher_wavelength.findall(status)
        return result[0]
        
