
import win32gui
import win32com.client
import time
import traceback
from py_lab.config import CONF_OPHIR

class Ophir(object):    

    def __init__(self):
        # Create object
        self.object = win32com.client.Dispatch("OphirLMMeasurement.CoLMMeasurement")
        # Stop & Close all devices
        self.object.StopAllStreams() 
        self.object.CloseAll()
        self.handle = None
        self.is_live = False
        # Scan for connected Devices
        DeviceList = self.object.ScanUSB()
        print(DeviceList)
        for Device in DeviceList:   	# if any device is connected
            DeviceHandle = self.object.OpenUSBDevice(Device)	# open first deviceç
            print(DeviceHandle)
            exists = self.object.IsSensorExists(DeviceHandle, 0)
            print(exists)
            if exists:
                print('\n----------Data for S/N {0} ---------------'.format(Device))
                self.handle = DeviceHandle
                
        if self.handle is None:
            raise ValueError("No device found")
            
    def get_range(self):
        ranges = self.object.GetRanges(self.handle, 0)
        return ranges
    
    def set_range(self, new_range):
        self.object.SetRange(self.handle, 0, new_range)
        
    def start_live(self):
        if not self.is_live:
            self.object.StartStream(self.handle, 0)
        self.is_live = True
        time.sleep(1)
        
    def get_data(self):
        data = [[]]
        ind = 0
        while len(data[0]) <= 0 and ind < CONF_OPHIR['retries']:
            data = self.object.GetData(self.handle, 0)
            ind += 1
            
        if len(data[0]) > 0:
            return data # Reading, Time stamp, Status
        else:
            raise ValueError("No data readed")
        
    def stop_live(self):
        if self.is_live:
            self.object.StopAllStreams()
        self.is_live = False
        time.sleep(1)
        
    def close(self):
        self.stop_live()
        self.object.CloseAll()