# !/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys


def show_devices_usb():
    os.system('cat /sys/bus/usb/devices/*/product')
    os.system('usb-devices')


def getDeviceList(get_names=True):
    """
    very important: it obtains the number of sudo usb devices connected
    This name is used at UsbTmcDriver for conection
    """

    dirList = os.listdir("/dev")
    devices = list()
    names = list()

    for fname in dirList:
        #if (fname.startswith("usbtmc")):
        device = "/dev/{}".format(fname)
        devices.append(device)
        if get_names is True:
            handle = UsbTmcDriver(device)
            name_trial = handle.get_name()
            names.append(name_trial)
        else:
            names.append("")
    return devices, names


def closeDevices():
    devices, names = getDeviceList(get_names=False)
    for device in devices:
        handle = UsbTmcDriver(device)
        del handle


def getDeviceFromList(string="DS1302CA"):
    devices, names = getDeviceList()
    print(devices, names)
    has_device = False
    for device_trial in devices:
        print(device_trial)
        handle_trial = UsbTmcDriver(device_trial)
        name_trial = handle_trial.get_name()
        print(device_trial, handle_trial, name_trial)
        handle_trial.close()
        if string in name_trial:
            # "Rigol Technologies,DS1302CA,DS1AA134900317,04.02.00"
            device = device_trial
            has_device = True
            return device

    if has_device is False:
        print("No {} connected".format(string))
        return None


class UsbTmcDriver(object):
    """Simple implementation of a USBTMC device driver, in the style of visa.h"""
    def __init__(self, device, name=None):
        self.device = device
        try:
            self.FILE = os.open(device, os.O_RDWR)
        except OSError as e:
            if str(e).find('No such file or directory') != -1:
                print("ERROR: Path to USB device does not exist (" + device +
                      ")")
                sys.exit()
            elif str(e).find("Permission denied:"):
                print("Getting permission to change permissions of " + device)
                os.system("gksudo chmod 777 " + device)
                try:
                    self.FILE = os.open(device, os.O_RDWR)
                    print("Changed permissions and opened device")
                except:
                    print("Error opening device!")
                    sys.exit()
            else:
                print("Can't open path to device, have you ran: \n" +
                      "sudo chmod 777 " + device)
                sys.exit()

        self.verbose = True
        # print("device name at init = {}".format(self.get_name()))

    # def __del__(self):
    #     os.close(self.FILE)

    def write(self, command):
        command = command.encode()
        os.write(self.FILE, command)

    def read(self, length=8000):
        return os.read(self.FILE, length)

    def query(self, command, length=8000):
        self.write(command)
        # sleep(0.01)
        result = self.read(length)
        if self.verbose is True:
            print("query     = {}".format(command))
            print("   result = {}".format(result))
        return result

    def get_name(self):
        self.write("*IDN?")
        return self.read(300)

    def send_reset(self):
        self.write("*RST")

    def close(self):
        os.close(self.FILE)


# show_devices_usb()
