# !/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is a copy of https://github.com/sbrinkmann/PyOscilloskop/blob/master/src/usbtmc.py

from diffractio import plt, np, sp
from .driver_usbtmc import UsbTmcDriver
from .utils import nearest

V = 1.
mV = V / 1000.
s = 1.
ms = s / 1000.
us = ms / 1000.
ns = us / 1000.


class osciloscopioRigoDS1302CA(UsbTmcDriver):
    def __init__(self, device=None):
        super(self.__class__, self).__init__(device)

    # def __del__(self):
    #     self.unclock()
    #     # os.close(self.FILE)

    def set_temporal_scale(self, scale):
        values = np.array([
            1 * ns, 2 * ns, 5 * ns, 10 * ns, 20 * ns, 50 * ns, 100 * ns,
            200 * ns, 500 * ns, 1 * us, 2 * us, 5 * us, 10 * us, 20 * us,
            50 * us, 100 * us, 200 * us, 500 * us, 1 * ms, 2 * ms, 5 * ms,
            10 * ms, 100 * ms, 200 * ms, 500 * ms, 1 * s, 2 * s, 5 * s, 10 * s,
            20 * s, 50 * s
        ])
        i_menor, _, _ = nearest(values, scale)

        final_scale = values[i_menor]
        self.write(":TIM:SCAL {}".format(final_scale))

    def set_voltage_scale(self, channel, scale):
        values = np.array([
            0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1, 2, 5,
            10
        ])
        i_menor, _, _ = nearest(values, scale)

        final_scale = values[i_menor]

        self.write(":CHAN{}:PROB {}".format(channel, final_scale))
        print("prob: " +
              str(self.query_to_data(":CHAN{}:PROB?", channel, size_data=20)))
        self.write(":CHAN{}:SCAL {}".format(channel, final_scale))

    def unclock(self):
        self.write(":KEY:LOCK DIS")

    def set_filter(self, channel, is_on=False):
        if is_on is True:
            status = 'ON'
        else:
            status = 'OFF'

        self.write("::CHAN{}:FILT {}".format(channel, status))

    def get_memory_depth(self, channel):

        memory_depth = self.query_to_data(":CHAN{}:MEMD?",
                                          channel,
                                          size_data=20)

        return memory_depth

    def get_data(self, channel=1):
        self.write(":WAV:DATA? CHAN{}".format(channel))

        rawdata = self.read(18000)
        # rawdata.append(self.read(8000))
        rawdata = rawdata[10:-2]
        data = np.frombuffer(rawdata, 'B')
        print(data)
        print("Data_raw: min={}, max={}".format(data.min(), data.max()))
        return data

    def get_data_processed(self, channel):
        data = self.get_data(channel)
        param = self.get_parameters(channel)
        param['num_data'] = len(data)
        # Get the voltage scale and get the timescale

        # Walk through the data, and map it to actual voltages

        print("Data: min={}, max={}".format(255 - data.min(),
                                            255 - data.max()))
        # First invert the data
        data = (200 - data) / 200.

        # Now, we know from experimentation that the scope display range is actually
        # 55-255.  So shift by 130 - the voltage offset in counts, then scale to
        # get the actual voltage.

        # The number of ticks is 8
        data_processed = (8 * data -
                          4 * param['voltscale']) / param['voltscale']

        print("min = {}, max = {}".format(data_processed.min(),
                                          data_processed.max()))
        print("range={}".format(data_processed.max() - data_processed.min()))

        # The number of ticks is 12
        time = sp.linspace(-6 * param['timescale'], 6 * param['timescale'],
                           param['num_data'])

        return time, data_processed, param

    def query_to_data(self, command, channel='', size_data=20):
        """ejecutes a query and convert it to a float"""
        if "{}" in command:
            try:
                data = float(self.query(command.format(channel), size_data))
            except:
                data = "*****"
        else:
            try:
                data = float(self.query(command, size_data))
            except:
                data = "*****"
        return data

    def get_parameters(self, channel=1):
        # self.write(":WAV:DATA? CHAN{}".format(channel))

        timescale = self.query_to_data(":TIM:SCAL?", size_data=20)
        timeoffset = self.query_to_data(":TIM:OFFS?", size_data=20)

        voltscale = self.query_to_data(":CHAN{}:SCAL?", channel, size_data=20)
        voltoffset = self.query_to_data(":CHAN{}:OFFS?", channel, size_data=20)

        vpp = self.query_to_data(":MEAS:VPP? {}", channel, size_data=20)
        vmax = self.query_to_data(":MEAS:VMAX? {}", channel, size_data=20)
        vmin = self.query_to_data(":MEAS:VMIN? {}", channel, size_data=20)
        vamplitude = self.query_to_data(":MEAS:VAMP? {}",
                                        channel,
                                        size_data=20)
        vaverage = self.query_to_data(":MEAS:VAV? {}", channel, size_data=20)
        vrms = self.query_to_data(":MEAS:VRMS? {}", channel, size_data=20)

        vtop = self.query_to_data(":MEAS:VTOP? {}", channel, size_data=20)
        vbas = self.query_to_data(":MEAS:VBAS? {}", channel, size_data=20)
        overshoot = self.query_to_data(":MEAS:OVER? {}", channel, size_data=20)
        frequency = self.query_to_data(":MEAS:FREQ? {}", channel, size_data=20)
        period = self.query_to_data(":MEAS:PER? {}", channel, size_data=20)
        pwid = self.query_to_data(":MEAS:PWID? {}", channel, size_data=20)
        nwid = self.query_to_data(":MEAS:NWID? {}", channel, size_data=20)
        pdut = self.query_to_data(":MEAS:PDUT? {}", channel, size_data=20)
        ndut = self.query_to_data(":MEAS:NDUT? {}", channel, size_data=20)

        parameters = dict(channel=channel,
                          voltscale=voltscale,
                          voltoffset=voltoffset,
                          timescale=timescale,
                          timeoffset=timeoffset,
                          vpp=vpp,
                          vmax=vmax,
                          vmin=vmin,
                          vamplitude=vamplitude,
                          vaverage=vaverage,
                          vrms=vrms,
                          vtop=vtop,
                          vbas=vbas,
                          overshoot=overshoot,
                          frequency=frequency,
                          period=period,
                          pwid=pwid,
                          nwid=nwid,
                          pdut=pdut,
                          ndut=ndut)

        if self.verbose is True:
            self.show_parameters(parameters)

        return parameters

    def show_parameters(self, parameters):
        for key, value in parameters.items():
            print("{}={}".format(key, value))

    def save_parameters(self, parameters, filename=''):
        output = open(filename, 'w')
        hickle.dump(parameters, filename, mode='w')  # , compression='gzip'
        output.close()

    def save_data_processed(self, channel=1, filename='data_temp.txt'):
        time, data_processed, param = self.get_data_processed(self, channel)
        np.savetxt(filename, np.transpose([time, data_processed]))
        return time, data_processed, param

    def draw(self, time, data, filename=''):
        # format scale
        if (time[-1] < 1e-4):
            time = time / us
            t_unit = "us"
        elif (time[-1] < 1):
            time = time / ms
            t_unit = "ms"
        else:
            t_unit = "s"

        # Plot the data
        plt.figure()
        plt.plot(time, data, 'k', lw=2)
        plt.title("Oscilloscope Channel 1")
        plt.ylabel("Voltage (V)")
        plt.xlabel("Time ({})".format(t_unit))
        plt.xlim(time[0], time[-1])
        plt.grid(True)
        if not filename == '':
            plt.savefig(filename, dpi=300, bbox_inches='tight', pad_inches=0.1)
