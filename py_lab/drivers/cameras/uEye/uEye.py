from pyueye import ueye
from py_lab.config import CONF_UEYE
import numpy as np
import time

# error_codes = {ueye.IS_INVALID_EXPOSURE_TIME: "Invalid exposure time",
#                ueye.IS_INVALID_CAMERA_HANDLE: "Invalid camera handle",
#                ueye.IS_INVALID_MEMORY_POINTER: "Invalid memory pointer",
#                ueye.IS_INVALID_PARAMETER: "Invalid parameter",
#                ueye.IS_IO_REQUEST_FAILED: "IO request failed",
#                ueye.IS_NO_ACTIVE_IMG_MEM: "No active IMG memory",
#                ueye.IS_NO_USB20: "No USB2",
#                ueye.IS_NO_SUCCESS: "No success",
#                ueye.IS_NOT_CALIBRATED: "Not calibrated",
#                ueye.IS_NOT_SUPPORTED: "Not supported",
#                ueye.IS_OUT_OF_MEMORY: "Out of memory",
#                ueye.IS_TIMED_OUT: "Timed out",
#                ueye.IS_SUCCESS: "Success",
#                ueye.IS_CANT_OPEN_DEVICE: "Cannot open device",
#                ueye.IS_ALL_DEVICES_BUSY: "All device busy",
#                ueye.IS_TRANSFER_ERROR: "Transfer error"}

error_codes = {
    ueye.IS_NO_SUCCESS	: "	General error message	",
    ueye.IS_SUCCESS	: "	Function executed successfully	",
    ueye.IS_INVALID_CAMERA_HANDLE	: "	Invalid camera handle. Most of the uEye SDK functions expect the camera handle as the first parameter.	",
    ueye.IS_IO_REQUEST_FAILED	: "	An IO request from the uEye driver failed. Possibly the versions of the ueye_api.dll (API) and the driver file (ueye_usb.sys or ueye_eth.sys) do not match.	",
    ueye.IS_CANT_OPEN_DEVICE	: "	An attempt to initialize or select the camera failed (no camera connected or initialization error).	",
    ueye.IS_CANT_OPEN_REGISTRY	: "	Error opening a Windows registry key	",
    ueye.IS_CANT_READ_REGISTRY	: "	Error reading settings from the Windows registry	",
    ueye.IS_NO_IMAGE_MEM_ALLOCATED	: "	The driver could not allocate memory.	",
    ueye.IS_CANT_CLEANUP_MEMORY	: "	The driver could not release the allocated memory.	",
    ueye.IS_CANT_COMMUNICATE_WITH_DRIVER	: "	Communication with the driver failed because no driver has been loaded.	",
    ueye.IS_FUNCTION_NOT_SUPPORTED_YET	: "	The function is not supported yet.	",
    ueye.IS_INVALID_IMAGE_SIZE	: "	Invalid image size	",
    ueye.IS_INVALID_CAPTURE_MODE	: "	The function can not be executed in the current camera operating mode (free run, trigger or standby).	",
    ueye.IS_INVALID_MEMORY_POINTER	: "	Invalid pointer or invalid memory ID	",
    ueye.IS_FILE_WRITE_OPEN_ERROR	: "	File cannot be opened for writing or reading.	",
    ueye.IS_FILE_READ_OPEN_ERROR	: "	The file cannot be opened.	",
    ueye.IS_FILE_READ_INVALID_BMP_ID	: "	The specified file is not a valid bitmap file.	",
    ueye.IS_FILE_READ_INVALID_BMP_SIZE	: "	The bitmap size is not correct (bitmap too large).	",
    ueye.IS_NO_ACTIVE_IMG_MEM	: "	No active image memory available. You must set the memory to active using the is_SetImageMem() function or create a sequence using the is_AddToSequence() function.	",
    ueye.IS_SEQUENCE_LIST_EMPTY	: "	The sequence list is empty and cannot be deleted.	",
    ueye.IS_CANT_ADD_TO_SEQUENCE	: "	The image memory is already included in the sequence and cannot be added again.	",
    ueye.IS_SEQUENCE_BUF_ALREADY_LOCKED	: "	The memory could not be locked. The pointer to the buffer is invalid.	",
    ueye.IS_INVALID_DEVICE_ID	: "	The device ID is invalid. Valid IDs start from 1 for USB cameras, and from 1001 for GigE cameras.	",
    ueye.IS_INVALID_BOARD_ID	: "	The board ID is invalid. Valid IDs range from 1 through 255.	",
    ueye.IS_ALL_DEVICES_BUSY	: "	All cameras are in use.	",
    ueye.IS_TIMED_OUT	: "	A timeout occurred. An image capturing process could not be terminated within the allowable period.	",
    ueye.IS_NULL_POINTER	: "	Invalid array	",
    ueye.IS_INVALID_PARAMETER	: "	One of the submitted parameters is outside the valid range or is not supported for this sensor or is not available in this mode.	",
    ueye.IS_OUT_OF_MEMORY	: "	No memory could be allocated.	",
    ueye.IS_ACCESS_VIOLATION	: "	An access violation has occurred.	",
    ueye.IS_NO_USB20	: "	The camera is connected to a port which does not support the USB 2.0 high-speed standard. Cameras without a memory board cannot be operated on a USB 1.1 port.	",
    ueye.IS_CAPTURE_RUNNING	: "	A capturing operation is in progress and must be terminated first.	",
    ueye.IS_IMAGE_NOT_PRESENT	: "	The requested image is not available in the camera memory or is no longer valid.	",
    ueye.IS_TRIGGER_ACTIVATED	: "	The function cannot be used because the camera is waiting for a trigger signal.	",
    ueye.IS_CRC_ERROR	: "	A CRC error-correction problem occurred while reading the settings.	",
    ueye.IS_NOT_YET_RELEASED	: "	This function has not been enabled yet in this version.	",
    ueye.IS_NOT_CALIBRATED	: "	The camera does not contain any calibration data.	",
    ueye.IS_WAITING_FOR_KERNEL	: "	The system is waiting for the kernel driver to respond.	",
    ueye.IS_NOT_SUPPORTED	: "	The camera model used here does not support this function or setting.	",
    ueye.IS_TRIGGER_NOT_ACTIVATED	: "	The function is not possible as trigger is disabled.	",
    ueye.IS_OPERATION_ABORTED	: "	The operation was cancelled.	",
    ueye.IS_BAD_STRUCTURE_SIZE	: "	An internal structure has an incorrect size.	",
    ueye.IS_INVALID_BUFFER_SIZE	: "	The image memory has an inappropriate size to store the image in the desired format.	",
    ueye.IS_INVALID_PIXEL_CLOCK	: "	This setting is not available for the currently set pixel clock frequency.	",
    ueye.IS_INVALID_EXPOSURE_TIME	: "	This setting is not available for the currently set exposure time.	",
    ueye.IS_AUTO_EXPOSURE_RUNNING	: "	This setting cannot be changed while automatic exposure time control is enabled.	",
    ueye.IS_CANNOT_CREATE_BB_SURF	: "	The BackBuffer surface cannot be created.	",
    ueye.IS_CANNOT_CREATE_BB_MIX	: "	The BackBuffer mix surface cannot be created.	",
    ueye.IS_BB_OVLMEM_NULL	: "	The BackBuffer overlay memory cannot be locked.	",
    ueye.IS_CANNOT_CREATE_BB_OVL	: "	The BackBuffer overlay memory cannot be created.	",
    ueye.IS_NOT_SUPP_IN_OVL_SURF_MODE	: "	Not supported in BackBuffer Overlay mode.	",
    ueye.IS_INVALID_SURFACE	: "	Back buffer surface invalid.	",
    ueye.IS_SURFACE_LOST	: "	Back buffer surface not found.	",
    ueye.IS_RELEASE_BB_OVL_DC	: "	Error releasing the overlay device context.	",
    ueye.IS_BB_TIMER_NOT_CREATED	: "	The back buffer timer could not be created.	",
    ueye.IS_BB_OVL_NOT_EN	: "	The back buffer overlay was not enabled.	",
    ueye.IS_ONLY_IN_BB_MODE	: "	Only possible in BackBuffer mode.	",
    ueye.IS_INVALID_COLOR_FORMAT	: "	Invalid color format	",
    ueye.IS_INVALID_WB_BINNING_MODE	: "	Mono binning/mono sub-sampling do not support automatic white balance.	",
    ueye.IS_INVALID_I2C_DEVICE_ADDRESS	: "	Invalid I2C device address	",
    ueye.IS_COULD_NOT_CONVERT	: "	The current image could not be processed.	",
    ueye.IS_TRANSFER_ERROR	: "	Transfer error. Frequent transfer errors can mostly be avoided by reducing the pixel rate.	",
    ueye.IS_PARAMETER_SET_NOT_PRESENT	: "	Parameter set is not present.	",
    ueye.IS_INVALID_CAMERA_TYPE	: "	The camera type defined in the .ini file does not match the current camera model.	",
    ueye.IS_INVALID_HOST_IP_HIBYTE	: "	Invalid HIBYTE of host address	",
    ueye.IS_CM_NOT_SUPP_IN_CURR_DISPLAYMODE	: "	The color mode is not supported in the current display mode.	",
    ueye.IS_NO_IR_FILTER	: "	No IR filter available	",
    ueye.IS_STARTER_FW_UPLOAD_NEEDED	: "	The camera's starter firmware is not compatible with the driver and needs to be updated.	",
    ueye.IS_DR_LIBRARY_NOT_FOUND	: "	The DirectRenderer library could not be found.	",
    ueye.IS_DR_DEVICE_OUT_OF_MEMORY	: "	Not enough graphics memory available.	",
    ueye.IS_DR_CANNOT_CREATE_SURFACE	: "	The image surface or overlay surface could not be created.	",
    ueye.IS_DR_CANNOT_CREATE_VERTEX_BUFFER	: "	The vertex buffer could not be created.	",
    ueye.IS_DR_CANNOT_CREATE_TEXTURE	: "	The texture could not be created.	",
    ueye.IS_DR_CANNOT_LOCK_OVERLAY_SURFACE	: "	The overlay surface could not be locked.	",
    ueye.IS_DR_CANNOT_UNLOCK_OVERLAY_SURFACE	: "	The overlay surface could not be unlocked.	",
    ueye.IS_DR_CANNOT_GET_OVERLAY_DC	: "	Could not get the device context handle for the overlay.	",
    ueye.IS_DR_CANNOT_RELEASE_OVERLAY_DC	: "	Could not release the device context handle for the overlay.	",
    ueye.IS_DR_DEVICE_CAPS_INSUFFICIENT	: "	Function is not supported by the graphics hardware.	",
    ueye.IS_INCOMPATIBLE_SETTING	: "	Because of other incompatible settings the function is not possible.	",
    ueye.IS_DR_NOT_ALLOWED_WHILE_DC_IS_ACTIVE	: "	A device context handle is still open in the application.	",
    ueye.IS_DEVICE_ALREADY_PAIRED	: "	The device is already in use by the system or is being used by another system. (Camera was opened and paired to a device).	",
    ueye.IS_SUBNETMASK_MISMATCH	: "	The subnet mask of the camera and PC network card are different.	",
    ueye.IS_SUBNET_MISMATCH	: "	The subnet of the camera and PC network card are different.	",
    ueye.IS_INVALID_IP_CONFIGURATION	: "	The configuration of the IP address is invalid.	",
    ueye.IS_DEVICE_NOT_COMPATIBLE	: "	The device is not compatible to the drivers.	",
    ueye.IS_NETWORK_FRAME_SIZE_INCOMPATIBLE	: "	The settings for the image size of the camera are not compatible to the PC network card.	",
    ueye.IS_NETWORK_CONFIGURATION_INVALID	: "	The configuration of the network card is invalid.	",
    ueye.IS_ERROR_CPU_IDLE_STATES_CONFIGURATION	: "	The configuration of the CPU idle has failed.	",
    ueye.IS_DEVICE_BUSY	: "	The camera is busy and cannot transfer the requested image.	",
    ueye.IS_SENSOR_INITIALIZATION_FAILED	: "	The initialization of the sensor failed.	",
    ueye.IS_IMAGE_BUFFER_NOT_DWORD_ALIGNED	: "	The image buffer is not DWORD aligned.	",
    ueye.IS_SEQ_BUFFER_IS_LOCKED	: "	The image memory is locked.	",
    ueye.IS_FILE_PATH_DOES_NOT_EXIST	: "	The file path does not exist.	",
    ueye.IS_INVALID_WINDOW_HANDLE	: "	Invalid Window handle	",
    ueye.IS_INVALID_IMAGE_PARAMETER	: "	Invalid image parameter (position or size)	",
    212: "No camera present."
}


def check(error_code):
    """
    Check an error code, and raise an error if adequate.
    """
    if error_code != ueye.IS_SUCCESS:
        raise uEyeException(error_code)

class uEyeException(Exception):
    def __init__(self, error_code):
        self.error_code = error_code

    def __str__(self):
        if self.error_code in error_codes.keys():
                return error_codes[self.error_code]
        else:
            for att, val in ueye.__dict__.items():
                if att[0:2] == "IS" and val == self.error_code \
                   and ("FAILED" in att or
                        "INVALID" in att or
                        "ERROR" in att or
                        "NOT" in att):
                    return "Err: {} ({} ?)".format(str(self.error_code),
                                                   att)
            return "Err: " + str(self.error_code) + ". Check https://es.ids-imaging.com/manuals/ids-software-suite/ueye-manual/4.96.1/en/sdk_fehlermeldungen.html for more information"


class uEye(object):
    """Class for uEye cameras."""
    def __init__(self, HIDS=CONF_UEYE["HIDS"], nBitsPerPixel=CONF_UEYE["nBitsPerPixel"]):
        self.cam = ueye.HIDS(HIDS)
        # Camera data dictionary
        self.sInfo = ueye.SENSORINFO()
        self.cInfo = ueye.CAMINFO()
        self.pcImageMemory = ueye.c_mem_p()
        self.MemID = ueye.int()
        self.pitch = ueye.INT()
        self.rectAOI = ueye.IS_RECT()
        self.nBitsPerPixel = ueye.INT(nBitsPerPixel)
        self.channels = 3
        self.m_nColorMode = ueye.INT()
        self.sInfo = ueye.SENSORINFO()

    def open(self, resolution):
        # Starts the driver and establishes the connection to the camera
        check(ueye.is_InitCamera(self.cam, None))

        # Reads out the data hard-coded in the non-volatile camera memory and writes it to the data structure that cInfo points to
        check(ueye.is_GetCameraInfo(self.cam, self.cInfo))

        # You can query additional information about the sensor type used in the camera
        check(ueye.is_GetSensorInfo(self.cam, self.sInfo))
        # print(self.sInfo)

        # Reset the camera
        check(ueye.is_ResetToDefault(self.cam))

        # Set display mode to DIB
        check(ueye.is_SetDisplayMode(self.cam, ueye.IS_SET_DM_DIB))

        self.m_nColorMode = ueye.IS_CM_MONO8
        self.nBitsPerPixel = ueye.INT(8)
        self.bytes_per_pixel = int(self.nBitsPerPixel / 8)

        # St AOI and allocate memory
        self.set_aoi(width=resolution[0], height=resolution[1])

        # Set the desired color mode
        check(ueye.is_SetColorMode(self.cam, self.m_nColorMode))

        # Store pixelclock
        self.pixelclock = self.get_pixelclock()

        # Get minimum and maximum gains
        Master = ueye.c_int(100)
        value = ueye.is_SetHWGainFactor(self.cam, ueye.IS_INQUIRE_MASTER_GAIN_FACTOR, Master)
        print("Gain must be between 1 and ", float(value)/100)


    def allocate_memory(self):
        # Allocates an image memory for an image having its dimensions defined by width and height and its color depth defined by nBitsPerPixel
        check(ueye.is_AllocImageMem(self.cam, self.width, self.height, self.nBitsPerPixel, self.pcImageMemory, self.MemID))

        # Makes the specified image memory the active memory
        check(ueye.is_SetImageMem(self.cam, self.pcImageMemory, self.MemID))

    def start_live(self):
        # Activates the camera's live video mode (free run mode)
        check(ueye.is_CaptureVideo(self.cam, ueye.IS_DONT_WAIT))

    def stop_live(self):
        # Activates the camera's live video mode (free run mode)
        check(ueye.is_StopLiveVideo(self.cam, ueye.IS_FORCE_VIDEO_STOP))

    def set_aoi(self, width=CONF_UEYE['resolution'][0], height=CONF_UEYE['resolution'][1], x=0, y=0):
        # Can be used to set the size and position of an "area of interest"(AOI) within an image
        rect_aoi = ueye.IS_RECT()
        rect_aoi.s32X = ueye.int(x)
        rect_aoi.s32Y = ueye.int(y)
        rect_aoi.s32Width = ueye.int(width)
        rect_aoi.s32Height = ueye.int(height)

        check(ueye.is_AOI(self.cam, ueye.IS_AOI_IMAGE_SET_AOI, rect_aoi, ueye.sizeof(rect_aoi)))

        self.rectAOI = rect_aoi
        self.width = ueye.int(width)
        self.height = ueye.int(height)

        # Allocate memory
        self.allocate_memory()

    def get_aoi(self, return_rect=False):
        rect_aoi = ueye.IS_RECT()
        ueye.is_AOI(self.cam, ueye.IS_AOI_IMAGE_GET_AOI, rect_aoi, ueye.sizeof(rect_aoi))

        if return_rect:
            return rect_aoi
        else:
            return rect_aoi.s32X.value, rect_aoi.s32Y.value, rect_aoi.s32Width.value, rect_aoi.s32Height.value


    def get_image(self):
        # Enables the queue mode for existing image memory sequences
        check(ueye.is_InquireImageMem(self.cam, self.pcImageMemory, self.MemID, self.width, self.height, self.nBitsPerPixel, self.pitch))

        # In order to display the image in an OpenCV window we need to extract the data of our image memory
        array = ueye.get_data(self.pcImageMemory, self.width, self.height, self.nBitsPerPixel, self.pitch, copy=False)

        # ...reshape it in an numpy array...
        frame = np.reshape(array,(self.height.value, self.width.value, self.bytes_per_pixel))

        return frame

    def get_fps_range(self):
        """
        Get the current fps available range.
        Returns
        =======
        fps_range: 2x1 array
            range of available fps
        """
        mini = ueye.c_double()
        maxi = ueye.c_double()
        interv = ueye.c_double()
        check(ueye.is_GetFrameTimeRange(
                self.cam,
                mini, maxi, interv))
        return [float(1/maxi), float(1/mini)]

    def set_fps(self, fps):
        """
        Set the fps.
        Returns
        =======
        fps: number
            Real fps, can be slightly different than the asked one.
        """
        # Limits
        if fps > CONF_UEYE["max_fps"]:
            if CONF_UEYE["verbose"]:
                print("Framerate {} higher than maximum ({}). Value changed to maximum.".format(fps, CONF_UEYE["max_fps"]))
            fps = CONF_UEYE["max_fps"]
        elif fps < CONF_UEYE["min_fps"]:
            if CONF_UEYE["verbose"]:
                print("Framerate {} lower than minimum ({}). Value changed to minimum.".format(fps, CONF_UEYE["min_fps"]))
            fps = CONF_UEYE["min_fps"]

        # Change pixelclock if required
        new_pc = CONF_UEYE["const_fps"] * fps
        if new_pc > self.pixelclock or new_pc < self.pixelclock / 2:
            self.set_pixelclock(new_pc)

        # Change fps
        fps = ueye.c_double(fps)
        new_fps = ueye.c_double()
        check(ueye.is_SetFrameRate(self.cam, fps, new_fps))
        self.current_fps = float(new_fps)
        return new_fps.value

    def get_fps(self):
        """
        Get the current fps.
        Returns
        =======
        fps: number
            Current fps.
        """
        fps = ueye.c_double()
        check(ueye.is_GetFramesPerSecond(self.cam, fps))
        return fps.value

    def set_pixelclock(self, pixelclock):
        """
        Set the current pixelclock.
        Params
        =======
        pixelclock: number
            Current pixelclock.
        """
        # get pixelclock range
        pixelclock = int(pixelclock)
        pcrange = (ueye.c_uint*3)()
        check(ueye.is_PixelClock(self.cam, ueye.IS_PIXELCLOCK_CMD_GET_RANGE,
                                 pcrange, 12))
        pcmin, pcmax, pcincr = pcrange
        if pixelclock < pcmin:
            pixelclock = pcmin.value
        elif pixelclock > pcmax:
            pixelclock = pcmax.value

        # Set pixelclock
        self.pixelclock = pixelclock
        pixelclock = ueye.c_uint(pixelclock)
        check(ueye.is_PixelClock(self.cam, ueye.IS_PIXELCLOCK_CMD_SET,
                                 pixelclock, 4))
        return pixelclock.value

    def get_pixelclock(self):
        """
        Get the current pixelclock.
        Returns
        =======
        pixelclock: number
            Current pixelclock.
        """
        pixelclock = ueye.c_uint()
        check(ueye.is_PixelClock(self.cam, ueye.IS_PIXELCLOCK_CMD_GET,
                                 pixelclock, 4))
        return pixelclock.value

    def set_exposure(self, exposure):
        """
        Set the exposure.
        Returns
        =======
        exposure: number
            Real exposure, can be slightly different than the asked one.
        """
        # Limits
        if exposure > CONF_UEYE["max_exp"]:
            if CONF_UEYE["verbose"]:
                print("Exposure {} higher than maximum ({}). Value changed to maximum.".format(exposure, CONF_UEYE["max_exp"]))
            exposure = CONF_UEYE["max_exp"]

        # Change pixelclock if required
        new_pc = CONF_UEYE["const_exp"] / exposure
        if new_pc < self.pixelclock:
            self.set_pixelclock(new_pc)

        # Set exposure
        new_exposure = ueye.c_double(exposure)
        check(ueye.is_Exposure(self.cam,
                               ueye.IS_EXPOSURE_CMD_SET_EXPOSURE,
                               new_exposure, 8))
        time.sleep(CONF_UEYE["wait_time_param"])
        return new_exposure.value

    def get_exposure(self):
        """
        Get the current exposure.
        Returns
        =======
        exposure: number
            Current exposure.
        """
        exposure = ueye.c_double()
        check(ueye.is_Exposure(self.cam, ueye.IS_EXPOSURE_CMD_GET_EXPOSURE, exposure,  8))
        return exposure.value

    def set_exposure_auto(self, toggle):
        """
        Set auto expose to on/off.
        Params
        =======
        toggle: integer
            1 activate the auto gain, 0 deactivate it
        """
        value = ueye.c_double(toggle)
        value_to_return = ueye.c_double()
        check(ueye.is_SetAutoParameter(self.cam,
                                       ueye.IS_SET_ENABLE_AUTO_SHUTTER,
                                       value,
                                       value_to_return))

    def set_gain_auto(self, toggle):
        """
        Set/unset auto gain.
        Params
        ======
        toggle: integer
            1 activate the auto gain, 0 deactivate it
        """
        value = ueye.c_double(toggle)
        value_to_return = ueye.c_double()
        check(ueye.is_SetAutoParameter(self.cam,
                                       ueye.IS_SET_ENABLE_AUTO_GAIN,
                                       value,
                                       value_to_return))

    def set_gain(self, value):
        """
        Set master gain
        Params:
        =======
        value: integer"""
        Master = ueye.c_int(int(value * 100))
        value = ueye.is_SetHWGainFactor(self.cam, ueye.IS_SET_MASTER_GAIN_FACTOR, Master)

    def set_gain_linear(self, value):
        """
        Set master gain
        Params:
        =======
        value: integer"""
        Master = ueye.c_int(int(value))
        value = ueye.is_SetHWGainFactor(self.cam, ueye.IS_INQUIRE_MASTER_GAIN_FACTOR, Master)
        Master = ueye.c_int(value)
        value = ueye.is_SetHWGainFactor(self.cam, ueye.IS_SET_MASTER_GAIN_FACTOR, Master)

    def get_gain(self):
        """
        Get master gain
        Returns
        =======
        gain: number
            Current gain."""
        Master = ueye.c_int()
        value = ueye.is_SetHWGainFactor(self.cam, ueye.IS_GET_MASTER_GAIN_FACTOR, Master)
        return float(value)/100

    def close(self):
        ueye.is_FreeImageMem(self.cam, self.pcImageMemory, self.MemID)
        ueye.is_ExitCamera(self.cam)
