import usb.core
from usb.util import dispose_resources

DEFAULT_READ_LENGTH = 150


class USB(object):
    """Class implemented to adress devices controlled by USB."""

    def __init__(self,
                 idVendor,
                 idProduct,
                 config,
                 ep_out,
                 ep_in,
                 end_char="\r",
                 timeout=None,
                 nbytes=DEFAULT_READ_LENGTH,
                 verbose=False):
        """Initialization.

        Args:
            idVendor, idProduct (int): Device USB identifiers. Can be found using the function in py_lab.utils List_USB_Devices().
            config (int or None): USB configuration. If None, the first found is used.
            ep_out (int): Out endpoint adress.
            ep_in (int): In endpoint adress.
            end_char (str): Enc char of orders received by the device. Default: \r.
            timeout (int or None): Timeout in ms. If None, the device built-in default value is used. Default: None.
            nbytes (int): Bytes to read after a command is sent. Default: 150.
            verbose (bool): If True, prints the information of the device. Default: False.

        Returns:
            self (USB).
        """
        # Set the object
        self._object = usb.core.find(idVendor=idVendor, idProduct=idProduct)
        self._object.set_configuration(config)

        #  Store info
        self.end_char = end_char
        self.ep_out = ep_out
        self.ep_in = ep_in
        self.timeout = timeout
        self.nbytes = nbytes

        # Rest
        if verbose:
            print(self._object)

    def send_command(self,
                     command,
                     axis=None,
                     var=None,
                     return_type=float,
                     length=DEFAULT_READ_LENGTH):
        """Method to send a command and return the device response.

        Args:
            command (str): Command string.
            axis (None or int): If not None, it prepends the axis number to the command. Default: None.
            var (None or int): If not none, it appends the variable to the command. Default: None.
            return_type (bool): If query, this determines the return type. Default: float.
            length (int): Max number of bytes readed. Default: DEFAULT_READ_LENGTH.

        Returns:
            res (str): Response.
        """
        # Adaptar comando
        is_query = command[-1] == "?"
        if axis:
            command = str(axis) + command
        if var is not None and not is_query:
            command += str(var)
        if self.end_char:
            command += self.end_char

        # Escribir
        l = self._object.write(self.ep_out, command, self.timeout)
        if l != len(command):
            raise ValueError("El comando no se ha transmitido correctamente.")

        # Leer
        if is_query:
            res_bytes = self._object.read(self.ep_in, self.nbytes,
                                          self.timeout)
            res = ''.join([chr(x) for x in res_bytes])
            if return_type == bool:
                return bool(int(res))
            else:
                return return_type(res)

    def close(self):
        """Releases the object and returns it to the state before it was claimed.

        TODO: Parece que no funciona"""
        self._object.reset()
        dispose_resources(self._object)
