"""
Control of motors driven by GRBL drivers.

It is controlled by serial port and pyserial module.

Parameters
https://github.com/gnea/grbl/wiki/Grbl-v1.1-Configuration

G codes
http://www.linuxcnc.org/docs/2.5/html/gcode/gcode.html

M codes
http://www.linuxcnc.org/docs/2.5/html/gcode/m-code.html

Other codes
http://www.linuxcnc.org/docs/2.5/html/gcode/other-code.html#sec:F-feed-rate

GRBL codes
https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands

Quick chart of all symbols and variables
https://www.sainsmart.com/blogs/news/grbl-v1-1-quick-reference

Detailed description of everything
https://github-wiki-see.page/m/gnea/grbl/wiki/Grbl-v1.1-Interface

* $ Help
* $$ (grbl settings)
* $# (view # parameters)
* $G (view parser state)
* $I (view build info)
* $N (view startup blocks)
* $x=value (save Grbl setting)
* $Nx=line (save startup block)
* $C (check gcode mode)
* $X (kill alarm lock)
* $H (run homing cycle)
* ~ (cycle start)
* ! (feed hold)
* ? (current status)
* ctrl-x (reset Grbl)

If nothing works
https://github.com/Spark-Concepts/xPRO/wiki/5.-Troubleshooting#settings-for-most-common-machines
"""


import time
import re

import numpy as np
import serial
import serial.tools.list_ports

from py_lab.config import CONF_GRBL

max_vel = 2000
min_vel = 6
max_travel = 410

matcherPos = re.compile("MPos:(.+?),(.+?),(.+?)\|")
matcherVel = re.compile("FS:(.+?),")
matcherStatus = re.compile("<(.+?)\|")

class GRBL():
    #######################
    ## RESERVED METHODS
    #######################

    def __init__(self):
        self.serial = None
        self.sleep_time = 0.2
        self.vel = max_vel

    def __del__(self):
        self.close()

    ########################
    ## PROPERTIES
    #######################

    # @property
    # def pos_min(self):
    #     return self._pos_min
    # @pos_min.setter
    # def pos_min(self, value):
    #     self._pos_min = value

    # @property
    # def pos_max(self):
    #     return self._pos_max
    # @pos.setter
    # def pos_max(self, value):
    #     self._pos_max = value

    @property
    def vel(self):
        return self._vel
    @vel.setter
    def vel(self, value):
        value = value
        self._vel = max(min_vel, min(max_vel, value))

    ########################
    ## INTERNAL METHODS
    #######################


    def close(self):
        self.serial.close()
        del self

    def get_port_list(self):
        list = serial.tools.list_ports.comports()
        connected = []
        for element in list:
            connected.append(element.device)
        print("Connected COM ports: " + str(connected))
        return list

    def send_command(self, command, data=None, verbose=False):
        if data is None: 
            text = "{}\r".format(command)
        else:
            text = "{}={}\r".format(command, data)

        if verbose:
            print("Input text: {}".format(text))

        try:
            self.serial.write(text.encode("utf-8"))
            time.sleep(self.sleep_time)
            out = self.serial.read_all()
            time.sleep(self.sleep_time)
            out2 = out.replace(b'\r', b'\n')
            # out2 = out2.replace(b'\t', b'\n')
            out3 = out2.decode()
            out4 = out3.split("\n")
        except self.serial.SerialException:
            print('Port is not available')
        except self.serial.portNotOpenError:
            print('Attempting to use a port that is not open')
            print('End of script')

        if verbose:
            print("Output text:{}\n".format(out2.decode()))

        return out4

    ########################
    ## EXTERNAL METHODS
    #######################

    def enable(self):
        self.send_command("$X") # Enable

    def open(self, port):
        self.port = port
        self.serial = serial.Serial(
            port=port, baudrate=115200, parity='N', stopbits=1, bytesize=8,  xonxoff=False)

        # self.serial.reset_input_buffer()
        # self.serial.reset_output_buffer()
        time.sleep(.5)

        self.set_hard_limits(1)
        # self.set_soft_limits(1)

        self.enable()
        # self.send_command("$130={}".format(max_travel)) # max movement, maybe does not work

    def home(self):
        self.send_command("$H") # Enable

    def get_position(self):
        string_list = self.send_command("?")
        for string in string_list:
            res = matcherPos.findall(string)
            if len(res) > 0:
                return float(res[0][0])
        return None

    def get_vel(self):
        string_list = self.send_command("?")
        for string in string_list:
            res = matcherVel.findall(string)
            if len(res) > 0:
                return float(res[0])

    def get_status(self):
        string_list = self.send_command("?")
        for string in string_list:
            res = matcherStatus.findall(string)
            if len(res) > 0:
                return res[0]


    def move_absolute(self, pos, vel=0, check_limits=True):
        if check_limits and (pos > CONF_GRBL["home_dist"] or pos < 1):
            raise ValueError("Position {} out of motor limits (1, {})".format(pos, CONF_GRBL["home_dist"]))
        else:
            if self.get_status() == "Alarm":
                self.enable()
            if vel != 0:
                self.vel = vel
            com = "G21G1G55X{}F{}".format(pos, self.vel)
            self.send_command(com)


    def move_relative(self, dist, vel=0, check_limits=True):
        pos = self.get_position() + dist
        if check_limits and (pos > CONF_GRBL["home_dist"] or pos < 1):
            raise ValueError("Position {} out of motor limits (1, {})".format(pos, CONF_GRBL["home_dist"]))
        else:
            if self.get_status() == "Alarm":
                self.enable()
            if vel != 0:
                self.vel = vel
            com = "$J=G21G91X{}F{}".format(dist, self.vel)
            self.send_command(com)

    def set_parameter(self, param, value):
        self.send_command("${}={}".format(param, value))

    def get_parameters(self):
        return self.send_command("$$")

    def get_parameter(self, param):
        matcherParam = re.compile("\${}=(.+)".format(param))
        string_list = self.get_parameters()
        for string in string_list:
            res = matcherParam.findall(string)
            if len(res) > 0:
                return float(res[0])

    def feed_hold(self):
        self.send_command("!")

    def soft_reset(self):
        self.send_command(chr(int(0x18)))

    def hard_reset(self):
        self.close()
        self.open(self.port)

    def set_hard_limits(self, state):
        self.set_parameter(21, state)

    def set_soft_limits(self, state):
        self.set_parameter(20, state)



        
    ####################
    ## OTHER METHODS
    ####################

    def help_grbl(self):
        self.send_command("$", verbose=True)

    def grbl_settings(self):
        self.send_command("$$", verbose=True)

    def current_status(self):
        self.send_command("?", verbose=True)

    def view_parameters(self):
        self.send_command("$#", verbose=True)

    def view_parser_state(self):
        self.send_command("$G", verbose=True)

    def view_build_info(self):
        self.send_command("$I", verbose=True)

    def view_startup_blocks(self):
        self.send_command("$N", verbose=True)

    def check_gcode_mode(self):
        self.send_command("$C", verbose=True)

    def cycle_start(self):
        self.send_command("~", verbose=True)
