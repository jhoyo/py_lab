"""
Driver for InteLiDrives rotatory motors controlled using Copley Controlls Stepnet STP-075-07. Designed to be simple as many other things can be implemented.

Author: Jesus del Hoyo.

----------------------------------------
"""

from  serial import Serial
from time import sleep, time
import numpy as np
from py_lab.config import CONF_INTEL

dict_errors = {}
dict_errors["1"] = "Too much data passed with command"
dict_errors["3"] = "Unknown command code"
dict_errors["4"] = "Not enough data was supplied with the command"
dict_errors["5"] = "Too much data was supplied with the command"
dict_errors["9"] = "Unknown parameter ID"
dict_errors["10"] = "Data value out of range"
dict_errors["11"] = "Attempt to modify read-only parameter"
dict_errors["14"] = "Unknown axis state"
dict_errors["15"] = "Parameter doesn’t exist on requested page"
dict_errors["16"] = "Illegal serial port forwarding"
dict_errors["18"] = "Illegal attempt to start a move while currently moving"
dict_errors["19"] = "Illegal velocity limit for move"
dict_errors["20"] = "Illegal acceleration limit for move"
dict_errors["21"] = "Illegal deceleration limit for move"
dict_errors["22"] = "Illegal jerk limit for move"
dict_errors["25"] = "Invalid trajectory mode"
dict_errors["27"] = "Command is not allowed while CVM is running"
dict_errors["31"] = "Invalid node ID for serial port forwarding"
dict_errors["32"] = "CAN Network communications failure"
dict_errors["33"] = "ASCII command parsing error"
dict_errors["36"] = "Bad axis letter specified"
dict_errors["46"] = "Error sending command to encoder"
dict_errors["48"] = "Unable to calculate filter"


class InteliDrives(object):
    """ Driver for InteLiDrives rotatory moors.
    """

    def __init__(self, port):
        """:param port: Serial port connected to the controller."""
        # self.lock = threading.Lock()
        self.ser = Serial(
            port=port,
            baudrate=CONF_INTEL["default_baudrate"],
            bytesize=8,
            timeout=CONF_INTEL["timeout"],
            write_timeout=CONF_INTEL["write_timeout"],
            parity='N',
            stopbits=1)

    def __del__(self):
        self.close()


    def read(self):
        """ Serial read with EOL character removed."""
        string = self.ser.read_until(b"\r")
        try:
            string = string.decode()
        except:
            pass
        # string = self.ser.readline().decode()
        if len(string) > 1:
            return string[:-1]
        else:
            return string

    def write(self, string, axis=None, value=None, return_read=False):
        """ Serial write. The value and EOL character are automatically appended and the axis number preppended.

        Args:
            string (str): Command.
            axis (None, 'all' or int): Axis number (if required). If 'all', the command is passed to all axes. Default: None.
            value (number or iterable): Value to add to the command (example: position to be set). If axis='all', itis possible to use here an iterable, so eax motor uses a different value. If None, nothing is added. Default: None.
            return_read (bool or None): This function performs a read to check there is no error. If this arg is True, the readed value is returned. If None, no read is performed. Default: False.

        Returns:
            string (str, float or np.ndarray): Readed string / value.
        """
        # Case all, use recurrency
        is_string = False
        if axis == "all":
            readed = [0] * len(CONF_INTEL["axes"])
            for ind, ax in enumerate(CONF_INTEL["axes"]):
                if type(value) in (list, tuple, np.ndarray):
                    if len(value) < len(CONF_INTEL["axes"]):
                        raise ValueError("Length of value {} is lower than number of axes ({})".format(len(value), len(CONF_INTEL["axes"])))
                    readed[ind] = self.write(string=string, axis=ax, value=value[ind], return_read=return_read)
                else:
                    readed[ind] = self.write(string=string, axis=ax, value=value, return_read=return_read)
                is_string = is_string or isinstance(readed[ind], str)
            if not is_string:
                readed = np.array(readed)


        # Rest of cases
        else:
            # Write command
            string = (str(axis) + " " if axis is not None else "") + string + (" " + str(round(value)) if value is not None else "") + "\r"
            self.ser.write(string.encode())

            # Check there is no error
            if return_read is not None:
                readed = self.read()
                if readed == "":
                    raise ValueError("Communication failed")
                elif len(readed) > 1 and readed[0] == "e":
                    raise ValueError(dict_errors[readed[2:]])
                elif len(readed) > 1 and readed[0] == "v":
                    readed = 0 if len(readed) < 3 else float(readed[2:])

        # Return
        if return_read:
            return readed
        else:
            return None

    def close(self, axis="all", disable_motors=True, close_serial=True):
        """Disables the motors and closes serial port communications.

        Args:
            disable_motors (bool): If True, the motors are disabled. Default: True.
            close_serial (bool): If True, the serial port connection is closed. Default: True.
        """
        if disable_motors:
            # self.write(string="r0x24", axis="all", value=0)
            self.write(string="s r0xab", axis=axis, value=0)
        if close_serial:
            self.set_baudrate(CONF_INTEL["default_baudrate"], axis=axis)
            self.ser.close()


    def open(self, set_max=True, axis="all"):
        """Ready the motors to work.

        Args:
            set_max (bool): Set maximum accelerations and decelerations.
        """
        try:
            self.ser.open()
        except:
            pass
        # try:
        #     self.set_baudrate(CONF_INTEL["working_baudrate"])
        # except:
        #     self.set_baudrate(CONF_INTEL["working_baudrate"])
        self.set_baudrate(CONF_INTEL["working_baudrate"], axis=axis)
        self.enable(axis=axis)
        self.set_mode(axis=axis, mode='servo')
        self.write(string="s r0xc6", axis=axis, value=0)   # Home offset
        self.write(string="s r0xc2", axis=axis, value=514) # Home method: home switch positive
        # Set maximums
        if set_max:
            self.set_max_acceleration(value=CONF_INTEL["max_acel"], axis=axis)
            self.set_max_decceleration(value=CONF_INTEL["max_acel"], axis=axis)
            self.set_abort_decceleration(value=CONF_INTEL["max_acel"], axis=axis)

    def enable(self, axis='all'):
        """ Enables axis.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write(string="s r0xab", axis=axis, value=1)


    def reset(self, axis='all'):
        """ Reset controller.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write("r", axis=axis, return_read=None)
        self.ser.baudrate = 9600
        sleep(1)
        self.open()

    def set_baudrate(self, new_baud=CONF_INTEL["working_baudrate"], axis="all"):
        """Change serial port baudrate (the higher the better). Maximum is 115,200.

        Args:
            new_baud (int): New baudrate. Default: CONF_INTEL["working_baudrate"].
        """
        new_baud = int(min(CONF_INTEL["max_baudrate"], new_baud))
        string = "s r0x90 " + str(new_baud) + "\r"
        self.ser.write(string.encode())
        sleep(1)
        self.ser.baudrate = new_baud
        self.read()

    def set_mode(self, axis="all", mode="servo"):
        """ Sets movement type between servo and stepper.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.
            mode (str): 'servo' or 'stepepr'. Default: 'servo'.
        """
        if mode.lower() == "servo":
            self.write(string="s r0x24", axis=axis, value=21)
        else:
            self.write(string="s r0x24", axis=axis, value=31)


    def abort(self, axis='all'):
        """ Aborts motor motion.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write(string="t 0", axis=axis)

    def home(self, axis='all'):
        """ Homes motor motion.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.
        """
        # print(self.write(string="t 2", axis=axis, return_read=True))
        self.write(string="t 2", axis=axis)

    def get_velocity(self, axis='all'):
        """ Gets set motor velocity in position mode.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            result (float or np.ndarray). Units: 0.1 counts/s.
        """
        return self.write(string="g r0xcb", axis=axis, return_read=True)

    def get_current_velocity(self, axis='all'):
        """ Gets current motor velocity.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            result (float or np.ndarray). Units: 0.1 counts/s.

        ERROR: Seems to give random numbers until stopped.
        """
        return self.write(string="g r0x18", axis=axis, return_read=True)

    def set_max_acceleration(self, value, axis='all'):
        """ Sets motor maximum acceleration in position mode.

        Args:
            value (float): New acceleration (units: 10 counts/s**2)
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write(string="s r0xcc", value=value, axis=axis)

    def set_max_decceleration(self, value, axis='all'):
        """ Sets motor maximum decceleration in position mode.

        Args:
            value (float): New acceleration (units: 10 counts/s**2)
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write(string="s r0xcd", value=value, axis=axis)

    def set_abort_decceleration(self, value, axis='all'):
        """ Sets motor abort decceleration in position mode.

        Args:
            value (float): New acceleration (units: 10 counts/s**2)
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write(string="s r0xcf", value=value, axis=axis)

    def set_jerk(self, value, axis='all'):
        """ Sets motor abort decceleration in position mode.

        Args:
            value (float): New acceleration (units: 100 counts/s**3)
            axis ('all' or int): Axis number. Default: 'all'.
        """
        self.write(string="s r0xce", value=value, axis=axis)

    def set_velocity(self, value, axis='all'):
        """ Sets motor velocity in position mode.

        Args:
            value (float): New acceleration (units: 0.1 counts/s)
            axis ('all' or int): Axis number. Default: 'all'.
        """
        if isinstance(value, np.ndarray):
            value = np.min(CONF_INTEL["max_vel"], np.abs(value))
        elif isinstance(value, (tuple, list)):
            value = np.min(CONF_INTEL["max_vel"], np.abs(np.array(value)))
        else:
            value = min(CONF_INTEL["max_vel"], abs(value))
        self.write(string="s r0xcb", value=value, axis=axis)

    def get_max_acceleration(self, axis='all'):
        """ Gets motor maximum acceleration in position mode.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            result (float or np.ndarray). Units: 10 counts/s**2.
        """
        return self.write(string="g r0xcc", axis=axis, return_read=True)

    def get_max_decceleration(self, axis='all'):
        """ Gets motor maximum decceleration in position mode.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            result (float or np.ndarray). Units: 10 counts/s**2.
        """
        return self.write(string="g r0xcd", axis=axis, return_read=True)

    def get_abort_decceleration(self, axis='all'):
        """ Gets motor abort decceleration in position mode.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            result (float or np.ndarray). Units: 10 counts/s**2.
        """
        return self.write(string="g r0xcf", axis=axis, return_read=True)

    def get_jerk(self, axis='all'):
        """ Gets motor jerk in position mode.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            result (float or np.ndarray). Units: 100 counts/s**3.
        """
        return self.write(string="g r0xce", axis=axis, return_read=True)

    def move_absolute(self, value, axis='all', mode="s-curve"):
        """ Move to a fixed position.

        Args:
            value (float): New position (units: counts).
            axis ('all' or int): Axis number. Default: 'all'.
            mode (str): Choose between s-curve (better but not modifiable while moving) or trapezoidal (modifiable while moving but less stable). Default: 's-curve'.
        """
        # Choose mode
        if mode == "s-curve":
            self.write(string="s r0xc8", value=1, axis=axis)
        else:
            self.write(string="s r0xc8", value=0, axis=axis)

        # Set target position
        self.write(string="s r0xca", value=value, axis=axis)

        # Move
        self.write(string="t 1", axis=axis)

    def move_relative(self, value, axis='all', mode="s-curve"):
        """ Movement relative to the current position.

        Args:
            value (float): Distance (units: counts).
            axis ('all' or int): Axis number. Default: 'all'.
            mode (str): Choose between s-curve (better but not modifiable while moving) or trapezoidal (modifiable while moving but less stable). Default: 's-curve'.
        """
        # Choose mode
        if mode == "s-curve":
            self.write(string="s r0xc8", value=257, axis=axis)
        else:
            self.write(string="s r0xc8", value=256, axis=axis)

        # Set target position
        self.write(string="s r0xca", value=value, axis=axis)

        # Move
        self.write(string="t 1", axis=axis)

    def move_continuous(self, value=1, axis='all'):
        """ Continuous movement until aborted or velocity is set to 0.

        Args:
            value (int): Direction (+1 or -1). Default: 1.
            axis ('all' or int): Axis number. Default: 'all'.
        """
        # Choose mode
        self.write(string="s r0xc8", value=2, axis=axis)

        # Set target position
        self.write(string="s r0xca", value=value, axis=axis)

        # Move
        self.write(string="t 1", axis=axis)

    def get_position(self, axis='all'):
        """ Get current position. Can be done while moving.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            (float): Current position. Units: counts.
        """
        # Choose mode
        return self.write(string="g r0x32", axis=axis, return_read=True)

    def get_current_velocity(self, axis='all'):
        """ Get current velocity. Can be done while moving.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            (float or np.ndarray): Current velocity. Units: 0.1 counts/s.
        """
        # Choose mode
        return self.write(string="g r0x18", axis=axis, return_read=True)

    def get_commanded_velocity(self, axis='all'):
        """ Get commanded velocity. Can be done while moving.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            (float or np.ndarray): Commanded velocity. Units: 0.1 counts/s.
        """
        # Choose mode
        return self.write(string="g r0x2c", axis=axis, return_read=True)

    def is_moving(self, axis='all'):
        """ Checks if the motor is moving.

        Args:
            axis ('all' or int): Axis number. Default: 'all'.

        Returns:
            (bool or np.ndarray): Result.

        Notes: The result given by the module seems to be different than intuitive: 0 for moving and 1 for stopped.
        """
        # Choose mode
        return (self.write(string="g r0xc9", axis=axis, return_read=True) % 2) != 1
