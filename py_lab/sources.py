"""
Light sources
"""

from py_lab.drivers.sources.monocrom_laser_PIC_CTL_PD import PIC_CTL_PD
from py_lab.config import CONF_PIC_CTL_PD

from py_lab.drivers.sources.monocrom_laser_PIC_CTL_7V import PIC_CTL_7V
from py_lab.config import CONF_PIC_CTL_7V

# TODO: Warning de saturacion

class Source(object):
    """General class for light sources.

    Args:
        name (string): Name of the source.

    Atributes:
        name (string): Name of the source.
        _object (variable): Source object. Its class depends on which device is being used.

    Supported devices:
        * Monocrom PIC_CTL_PD.
        * Monocrom PIC_CTL_7V
    """

    def __init__(self, name="PIC_CTL_PD"):
        """Initialize the object."""
        self.name = name

    def __del__(self):
        """Delete callback method"""
        self.Close()

    def Open(self, port=None):
        """Open the object.

        Args:
            port (str): Port name.

        Supported devices:
            * Monocrom PIC_CTL_PD.
        """
        if self.name == "PIC_CTL_PD":
            self._object = PIC_CTL_PD()
            self._object.open_laser(port or CONF_PIC_CTL_PD["port"])
        if self.name == "PIC_CTL_7V":
            self._object = PIC_CTL_7V()
            self._object.open_laser(port or CONF_PIC_CTL_7V["port"])

    def Set_Power(self, power):
        """Sets the power of the source.

        Args:
            power (number): Power.

        Supported devices:
            * Monocrom PIC_CTL_PD.
            * Monocrom PIC_CTL_7V.
        """
        self._object.set_power(power)


    def Close(self):
        """Closes the connection with the object.

        Supported devices:
            * Monocrom PIC_CTL_PD.
            * Monocrom PIC_CTL_7V."""
        if self.name == "PIC_CTL_PD":
            self._object.close()
        if self.name == "PIC_CTL_7V":
            self._object.close()