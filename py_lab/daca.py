"""
Data acquisition card.
"""
import time

import u3
import u6
import LabJackPython
import numpy as np
from labjack import ljm

from py_lab.config import CONF_U3, CONF_U3_LCD, CONF_T7, CONF_U6

# TODO: Warning de saturacion


def Close_All_DACA():
    """Close all cards.

    Supported devices:
        * U3 (LabJack U3).
    """
    LabJackPython.Close()


class DACA(object):
    """General class for Data Acquisition Cards.

    Args:
        name (string): Name of the card.

    Atributes:
        name (string): Name of the card.
        AIN (list): List of the ID of used ports.
        AIN_ref (int or None): ID of the port used as signal reference.
        n_AIN (int): Number of used AIN ports.
        background (np.ndarray): Array of stored background signals.
        background_ref (float): Stored background for reference signal.
        _object (variable): Card object. Its class depends on which device is being used.

    LabJack:
        _module (object): Stores the correct module (U3, U6, etc.)

    Supported devices:
        * U3 (LabJack U3).
        * U6 (LabJack U6).
        * T7 (LabJack T7).
    """

    def __init__(self, name="U3"):
        """Initialize the object."""
        self.name = name

    def Open(self, AIN=None, AIN_ref=None, verbose=False):
        """Open the object.

        Args:
            AIN (int, tuple or list of ints): ID number of the ports we will read from. If None, default values will be taken. Default: None.
            AIN_ref (int): ID number of the port used as reference to future measurements. If None, default values will be taken. Default: None.
            verbose (bool): If True, prints some information about the card. Default: False.

        Supported devices:
            * U3 U6 T7.
        """
        if self.name == "U3":
            self._object = u3.U3()
            self._module = u3
            self.AIN = CONF_U3['AIN'] if AIN is None else AIN
            self.AIN_ref = CONF_U3['AIN_ref'] if AIN_ref is None else AIN_ref
            self.max_signal = CONF_U3['max_signal']

            if verbose:
                cal_data = self._object.getCalibrationData()
                print("\n Callibration data:\n", cal_data)
                print("\n Measured signal:")
                self.Get_Signal(verbose=True)

        elif self.name == "U6":
            self._object = u6.U6()
            self._module = u6
            self.AIN = CONF_U6['AIN'] if AIN is None else AIN
            self.AIN_ref = CONF_U6['AIN_ref'] if AIN_ref is None else AIN_ref
            self.max_signal = CONF_U6['max_signal']

            if verbose:
                cal_data = self._object.getCalibrationData()
                print("\n Callibration data:\n", cal_data)
                print("\n Measured signal:")
                self.Get_Signal(verbose=True)

        elif self.name in ("T7"):
            self.AIN = CONF_T7['AIN'] if AIN is None else AIN
            self.AIN_ref = CONF_T7['AIN_ref'] if AIN_ref is None else AIN_ref
            self.max_signal = CONF_T7['max_signal']

            # T7 device, Any connection, Any identifier
            self._object = ljm.openS("T7", "ANY", "ANY")

            if verbose:
                info = ljm.getHandleInfo(self._object)
                print(
                    "Opened a LabJack with Device type: %i, Connection type: %i,\n"
                    "Serial number: %i" % (info[0], info[1], info[2]))
                
        if self.name == "U3_LCD":
            self._object = u3.U3()
            self._module = u3
            self.AIN = CONF_U3_LCD['AIN'] if AIN is None else AIN
            self.AIN_ref = CONF_U3_LCD['AIN_ref'] if AIN_ref is None else AIN_ref
            self.max_signal = CONF_U3_LCD['max_signal']

            if verbose:
                cal_data = self._object.getCalibrationData()
                print("\n Callibration data:\n", cal_data)
                print("\n Measured signal:")
                self.Get_Signal(verbose=True)

    # Comunes para todas las LabJack
        if isinstance(self.AIN, int):
            self.AIN = [self.AIN]

        if isinstance(self.AIN, (list, tuple)):
            self.AIN = list(self.AIN)
            if self.AIN_ref is not None and self.AIN_ref in self.AIN:
                self.AIN.remove(self.AIN_ref)

        else:
            raise ValueError(
                "{} is not int, list or tuple of ints.".format(AIN))

        self.n_AIN = len(self.AIN)
        self.Clear_Background()

    def Get_Signal(self,
                   is_background=False,
                   rest_background=True,
                   use_ref=True,
                   return_single=True,
                   Naverage=1,
                   Twait=0,
                   return_ref=False,
                   verbose=False):
        """Measure the signal.

        Args:
            is_background (bool): If True, stores the measured signal as background to substract it in next measurements. Default: False.
            rest_background (bool): If True, background is substracted from the measurement. Default: True.
            use_ref (bool): If True, signal is divided by the reference. Default: True.
            return_single (bool): If True and only one signal photodiode is used, the result is returned as a number instead of a 1 element array. Default: True.
            Naverage (int): Number of measurements performed to take a single value. Default: 1.
            Twait (float): If Naverage > 1, this is the time between measurements in seconds. Default: 0.
            return_ref (bool): If True, also returns the reference signal.
            verbose (bool): If True, prints the measured signal. Default: False.

        Returns:
            signal (np.ndarray): Measured signal.
            ref (float, only if return_ref is True): Reference measurement.

        Supported devices:
            * U3 U6 T7.
        """
        # Average several measurements
        if Naverage > 1:
            signal = np.zeros((Naverage, self.n_AIN))
            ref = np.zeros(Naverage)

            # Loop in average
            for ind in range(Naverage):
                signal[ind, :], ref[ind] = self.Get_Signal(is_background=is_background, rest_background=rest_background, use_ref=use_ref, return_single=True, Naverage=1, return_ref=True, verbose=False)

                if Twait > 0:
                    time.sleep(Twait)

            # Average
            # print(signal, ref, sep="\n")
            signal = np.mean(signal, axis=0)
            ref = np.mean(ref)

        # Take a single measurement
        else:
            signal = np.zeros(self.n_AIN)

            if self.name in ("U3", "U3_LCD"):
                # Get the measurements
                for ind, AIN in enumerate(self.AIN):
                    ain1bits = self._object.getFeedback(self._module.AIN(AIN))
                    ainValue = self._object.binaryToCalibratedAnalogVoltage(
                        ain1bits[0], isLowVoltage=False, channelNumber=0)
                    signal[ind] = ainValue

                # Normalize using reference
                if self.AIN_ref is not None:
                    ain1bits = self._object.getFeedback(
                        self._module.AIN(self.AIN_ref))
                    ref = self._object.binaryToCalibratedAnalogVoltage(
                        ain1bits[0], isLowVoltage=False, channelNumber=0)
                else:
                    ref = None

            elif self.name == "U6":
                # Get the measurements
                for ind, AIN in enumerate(self.AIN):
                    signal[ind] = self._object.getAIN(
                        AIN,
                        resolutionIndex=CONF_U6['res_index'],
                        gainIndex=CONF_U6['gain_index'],
                        settlingFactor=CONF_U6['sett_factor'])
                    # cmd = u6.AIN24AR(
                    #     AIN,
                    #     ResolutionIndex=CONF_U6['res_index'],
                    #     GainIndex=CONF_U6['gain_index'],
                    #     SettlingFactor=CONF_U6['sett_factor'])
                    # res = self._object.getFeedback(cmd)
                    # ain1bits = res[0]['AIN'] * CONF_U6['normalization']
                    # signal[ind] = self._object.binaryToCalibratedAnalogVoltage(0,
                    #     ain1bits)

                # Normalize using reference
                if self.AIN_ref is not None:
                    ref = self._object.getAIN(
                        self.AIN_ref,
                        resolutionIndex=CONF_U6['res_index'],
                        gainIndex=CONF_U6['gain_index'],
                        settlingFactor=CONF_U6['sett_factor'])
                    # cmd = u6.AIN24AR(
                    #     self.AIN_ref,
                    #     ResolutionIndex=CONF_U6['res_index'],
                    #     GainIndex=CONF_U6['gain_index'],
                    #     SettlingFactor=CONF_U6['sett_factor'])
                    # res = self._object.getFeedback(cmd)
                    # ain1bits = res[0]['AIN'] * CONF_U6['normalization']
                    # ref = self._object.binaryToCalibratedAnalogVoltage(0,
                    #     ain1bits)
                else:
                    ref = None

            elif self.name in ("T7"):
                dataType = ljm.constants.FLOAT32

                for ind, AIN in enumerate(self.AIN):
                    ainValue = ljm.eReadAddress(self._object, AIN, dataType)
                    signal[ind] = ainValue

                if self.AIN_ref is not None:
                    ref = ljm.eReadAddress(self._object, self.AIN_ref, dataType)

                else:
                    ref = None

            # Check saturation
            cond1 = np.any(signal > self.max_signal)
            cond2 = ref is not None and ref > self.max_signal
            if cond1 or cond2:
                print("WARNING: Saturation threshold reached.")

            # Save background
            if is_background:
                self.background = signal
                self.background_ref = ref

            # Substract background
            elif rest_background:
                signal -= self.background
                if ref is not None:
                    ref -= self.background_ref

            # Normalize
            if not is_background and use_ref and ref is not None:
                signal /= ref

        if signal.size == 1 and return_single:
            signal = signal[0]

        # Print if required
        if verbose:
            if return_ref or (ref is not None and is_background):
                print("Reference: ", ref)
            print("Signal: ", signal)

        # Return
        if return_ref:
            return signal, ref
        else:
            return signal

    def Clear_Background(self):
        """Clears the background."""
        self.background = np.zeros(self.n_AIN)
        self.background_ref = 0

    def Get_Background(self, return_ref=False):
        """Gets the last measured background.

        Args:
            return_ref (bool): If True, also returns the reference background.

        Returns:
            signal (np.ndarray): Array of signal backgrounds.
            ref (float): Reference background.
        """
        return self.background, self.background_ref
