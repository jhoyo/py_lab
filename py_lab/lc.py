"""
Liquid crystals
"""
import time
import numpy as np

from py_lab.drivers.optics.lcc25.thorlabs_lcd import LCD
from py_lab.config import CONF_LCC25

class LC(object):
    """General class for liquid crystals.

    Args:
        name (string): Name of the device.

    Atributes:
        name (string): Name of the device.
        mode (int): New mode: 0 for modulation between voltage 1 and 2, 11-2 to select that voltage.
        _object (variable): Device object. Its class depends on which device is being used.

    LCC25 atributes:
        mode (int): Output mode: 0 for modulation, 1 for Voltage 1 and 2 for voltage 2. Default: 1.


    Supported devices:
        * LCC25.
    """

    def __init__(self, name="LCC25"):
        """Initialize the object."""
        self.name = name
        self.mode = 1


    def Open(self, port=None, verbose=False):
        """Open the object.

        Args:
            port (string): Device port name. If None, it uses the default port given by the configuration file. Default: None.
            verbose (bool): If True, the result is printed. Default: False.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            self._object = LCD()
            self._object.open(port or CONF_LCC25['port'])
            self._object.set_mode(mode=self.mode)
            if verbose:
                print("Device " + self._object.idn()[1] + " open")


    def Close(self):
        """Close the object.

        Supported devices:
            * LCC25.
        """
        self.Disable()
        if self.name == "LCC25":
            self._object.close()

    def Get_Voltage(self, verbose=False):
        """Query for the current voltage.

        Args:
            verbose (bool): If True, the result is printed. Default: False.

        Returns:
            voltage (float): Result in volts.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            if self.mode == 0:
                volt = np.array([float(self._object.get_voltage(channel=1)[1]), float(self._object.get_voltage(channel=2)[1])])
            else:
                volt = float(self._object.get_voltage(channel=self.mode)[1])

        if verbose:
            print("The current voltage is: {} V".format(volt))

        return volt

    def Set_Voltage(self, volt, verbose=False):
        """Sets the current voltage.

        Args:
            volt (float): New voltage in volts.
            verbose (bool): If True, the result is printed. Default: False.

        Returns:
            voltage (float): Result in volts.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            if self.mode == 0:
                if not isinstance(volt, (list, tuple, np.ndarray)):
                    volt = np.ones(2) * volt
                self._object.set_voltage(volt[0], channel=1)
                self._object.set_voltage(volt[1], channel=2)
            else:
                self._object.set_voltage(volt, channel=self.mode)
            time.sleep(CONF_LCC25['wait_time'])

        return self.Get_Voltage(verbose=verbose)

    def Enable(self):
        """Send the voltage to the liquid crystal.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            self._object.set_enable(1)

    def Disable(self):
        """Stops sending the voltage to the liquid crystal.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            self._object.set_enable(0)

    def Is_Enabled(self, verbose=False):
        """Queries if we are sending the voltage to the liquid crystal.

        Args:
            verbose (bool): If True, the result is printed. Default: False.

        Returns:
            result (bool): 

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            cond = self._object.get_enable()[1] == "1"

        if verbose and cond:
            print("Output is enabled")
        elif verbose:
            print("Output is disabled")

    def Set_Mode(self, mode):
        """Sets the current operation mode.

        Args:
            mode (int): New mode: 0 for modulation between voltage 1 and 2, 11-2 to select that voltage.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            if mode in (0, 1, 2):
                self.mode = mode
                self._object.set_mode(mode=mode)
                time.sleep(CONF_LCC25['wait_time'])
            else:
                raise ValueError("Mode must be 0, 1 or 2, not {}".format(mode))

    def Get_Mode(self, verbose=False):
        """Sets the current operation mode.

        Args:
            verbose (bool): If True, the result is printed. Default: False.

        Returns:
            result (int): 

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            mode = int(self._object.get_mode()[1])

        if verbose:
            print("The current mode is ", mode)

        return mode

    def Get_Frequency(self, verbose=False):
        """Query for the current frequency of modulation between voltage 1 and 2.

        Args:
            verbose (bool): If True, the result is printed. Default: False.

        Returns:
            freq (float): Result in Hz.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            freq = float(self._object.get_freq()[1])

        if verbose:
            print("The current frequency is: {:.2f} Hz".format(freq))

        return freq

    def Set_Frequency(self, freq, verbose=False):
        """Sets the current voltage.

        Args:
            freq (float): New frequency in Hertzs.
            verbose (bool): If True, the result is printed. Default: False.

        Returns:
            freq (float): Result in Hz.

        Supported devices:
            * LCC25.
        """
        if self.name == "LCC25":
            self._object.set_freq(freq)
            time.sleep(CONF_LCC25['wait_time'])

        return self.Get_Frequency(verbose=verbose)


