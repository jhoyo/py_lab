import serial
import numpy as np
import time
from pipython import GCSDevice

from py_lab.config import (CONF_SMC_100, CONF_ESP_300, CONF_DT_50, CONF_NEWPORT_8742, 
                           CONF_M511, CONF_INTEL, CONF_GRBL, CONF_ZABER)
from py_lab.utils import select_movement, adapt_to_multiple, select_pos, transform_axis_variable
from py_lab.drivers.motors.smc100.smc100 import SMC100
from py_lab.drivers.motors.esp300.newportESP import ESP
from py_lab.drivers.comunication import USB
from py_lab.drivers.motors.dt50.pyPICommands import pyPICommands
from py_lab.drivers.motors.intelidrives.intelidrives import InteliDrives
from py_lab.drivers.motors.grbl.grbl import GRBL


from zaber_motion import Units
from zaber_motion.ascii import Connection
from zaber_motion import Measurement
                      

# Default values
DEFAULT_MOVE_TIME = 1  # 1 second
DEFAULT_SLEEPING_TIME = 1e-3
DEFAULT_ACCEL_TIME = 0.1
DEFAULT_MIN_MOVE_TIME = 0.1
DEFAULT_SHUTTER_TIME = 5e-3

UNITS_POS = {'mm': 1, 'm': 1e3, 'um': 1e-3, 'deg': 1, 'rad': np.pi / 180, 'pulses': 1}
UNITS_VEL = {
    'mm/s': 1,
    'm/s': 1e3,
    'um/s': 1e-3,
    'deg/s': 1,
    'rad/s': np.pi / 180
}
UNITS_ACEL = {
    'mm/s2': 1,
    'm/s2': 1e3,
    'um/s2': 1e-3,
    'deg/s2': 1,
    'rad/s2': np.pi / 180
}

# TODO
# Check speed
# Check movement range


class Motor(object):
    """
    General class for motors.

    Args:
        name (string): Name of the motor. Available motors are: SMC100.

    Atributes:
        name (string): Name of the motor.
        range (np.array): Min and max absolute positions.
        sign (+1 or -1): Sign to invert the stage movement direction if desired.
        stored_pos (dict): Dictionary of saved references.
        ref (float): Position of 0 reference. Positions will be calculated respect to this position if refered variable of methods is True. Units: mm or deg.
        pos_units (str): Default position units.
        vel_units (str): Default velocity units.
        _object (variable): Motor object. Its class depends on which device is being used.

    ESP300 atributes:
        vel_max (float): Maximumm velocity.
        ac_max (float): Maximumm acceleration.
        min_limit (float): Minimum position limit.
        max_limit (float): Maximum position limit.

    Supported devices:
        * SMC100.
        * ESP300.
        * DT50.
        * Newport8742.
        * M-511.DD.
        * InteliDrives.
        * GRBL.
        * Zaber.
    """

    def __init__(self, name):
        """Initialize the object.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * Zaber
            """
        self.name = name

        if name == "SMC100":
            self.range = CONF_SMC_100["range"]
            self.pos_units = CONF_SMC_100["pos_units"]
            self.vel_units = CONF_SMC_100["vel_units"]

        elif name == "ESP300":
            self.pos_units = CONF_ESP_300["pos_units"]
            self.vel_units = CONF_ESP_300["vel_units"]

        elif name == "DT50":
            self.pos_units = CONF_DT_50["pos_units"]
            self.vel_units = CONF_DT_50["vel_units"]

        elif name == "Newport8742":
            self.pos_units = CONF_NEWPORT_8742["pos_units"]
            self.vel_units = CONF_NEWPORT_8742["vel_units"]

        elif self.name == 'M-511.DD':
            self.pos_units = CONF_M511["pos_units"]
            self.vel_units = CONF_M511["vel_units"]
            
        elif self.name in ('InteliDrives', "InteliDrivesSingle"):
            self.pos_units = CONF_INTEL["pos_units"]
            self.vel_units = CONF_INTEL["vel_units"]

        elif self.name == 'GRBL':
            self.pos_units = CONF_GRBL["pos_units"]
            self.vel_units = CONF_GRBL["vel_units"]
        
        elif self.name == 'Zaber':

            self.port = CONF_ZABER["port"]
            self.pos_units = CONF_ZABER["pos_units"]
            self.vel_units = CONF_ZABER["vel_units"]
            self.current_vel = 100
            self.current_acc = 10
            self.wait_until_idle = False


        else:
            raise ValueError('{} is not a valid motor name.'.format(name))

        self.stored_pos = {}
        self.ref = 0
        self.Clear_Reference()

    def __enter__(self):
        pass

    def __exit__(self):
        """Delete object last function.

        Supported devices:
            * All.
        """
        self.Close()

    def Open(self, port=None, invert=None, axis=1, parent_object=None):
        """Open the object.

        Args:
            port (string): Device port name. If None, it uses the default port given by the configuration file. Default: None.
            invert (bool): If True, the movement direction is inverted (switches the sign of positions).

        ESP300, Newport8742 and InteliDrives args:
            parent_object (object): These devices control multiple axes, so they are intended to be opened through Motor_Multiple class. For that reason, port is the Motor_Multiple parent object.
            axis (int): Axis of the stage. Valid values depend on the model. Default: 1.

        InteliDrives args:
            axis (int): Axis of the stage (only 0, 1 or 2 valid). Default: 1.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        if self.name == "SMC100":
            self._object = SMC100(1, port or CONF_SMC_100['port'], silent=True)

        elif self.name == "ESP300":
            self._object = parent_object.axis(axis)
            self._object.on()
            self._object.setUnits(2)
            self.vel_max = self._object.maxVelocity
            self.ac_max = self._object.maxAcceleration

        elif self.name == "DT50":
            port = port or CONF_DT_50["ports"][0]
            self._object = pyPICommands(CONF_DT_50["dll_lib"], 'PI_')
            # print(self._object)
            connected = self._object.ConnectRS232(port, CONF_DT_50["baudrate"])
            # print(connected)
            # try:
            if not connected:
                print('Cannot connect to COM %d, %d Baud' %
                      (port, CONF_DT_50["baudrate"]))
                return None
                # exit()
            print("Connected to " + self._object.qIDN())

            # except Exception as exc:
            #     ERR_NUM = self._object.GetError()
            #     print('GCS ERROR %d: %s' %
            #           (ERR_NUM, self._object.TranslateError(ERR_NUM)))
            #     self._object.CloseConnection()
            #     raise exc

            self._object.VEL({"1": CONF_DT_50["velocity"]})
            ref_ok = self._object.qFRF(' '.join(CONF_DT_50["axes"]))
            for axis_to_ref in CONF_DT_50["axes"]:
                self._object.SVO({axis_to_ref: 1})  # switch on servo
                if ref_ok[axis_to_ref] != 1:
                    print('referencConnectioning axis {}, PORT: {}'.format(
                        axis_to_ref, port))
                    self._object.FNL(axis_to_ref)

            ref_ok = self._object.FRF(' '.join(CONF_DT_50["axes"]))
            axes_are_referencing = True
            while axes_are_referencing:
                time.sleep(CONF_DT_50["sleeping_time"])
                ref = self._object.IsMoving(' '.join(CONF_DT_50["axes"]))
                axes_are_referencing = sum(ref.values()) > 0

        elif self.name == "Newport8742":
            self._object = parent_object
            self.axis = axis

        elif self.name == 'M-511.DD':
            self._object = GCSDevice('C-843.21')
            self._object.ConnectPciBoard(board=1)
            print('connected: {}'.format(self._object.qIDN().strip()))

        elif self.name == 'InteliDrives':
            # self._object = InteliDrives(CONF_INTEL['port'])
            self._object = parent_object
            self.axis = axis
            # self._object.open()
            
        elif self.name == 'InteliDrivesSingle':
            self._object = InteliDrives(CONF_INTEL_SINGLE['port'])
            self.axis = CONF_INTEL_SINGLE["axes"]
            self._object.open(axis=self.axis)

        elif self.name == 'GRBL':
            self._object = GRBL()
            self._object.open(port or CONF_GRBL["port"])
            if invert is None:
                invert = CONF_GRBL["invert"]

        
        elif self.name == 'Zaber':
            if port is None:
                port = self.port

            # La conexión con los motores la hacemos en la clase 'Motor_Multiple'.
            # if self._object is None:
            #     # connection
            #     self._object=Connection.open_serial_port(port)
                
            #     # Habilitar alertas
            #     self._object.enable_alerts()
            
            #La asignación de los ejes de los motores depende del controlador, en nuestro caso tenemos dos controladores. Cambiamos el 'device_ind' en función de si corresponde a los dos primeros ejes o los dos segundos.
            if axis <= 2:
                device_ind = 0
            elif axis == 3:
                device_ind = 1
                axis = 1
            else:
                device_ind = 1
                axis = 2
    
            
            self._object = parent_object
            device_list = self._object.detect_devices()
            #print("Found {} devices".format(len(device_list)))
            #device = device_list[1]
            axis_hdl = device_list[device_ind].get_axis(axis)
            self.axis = axis_hdl
            #self.axis_number=axis        
            
            if invert is None:
                invert = CONF_ZABER["invert"]

        if invert:
            self.sign = -1
        else:
            self.sign = 1

    def Close(self):
        """Close the object.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        if self.name == "SMC100":
            self._object.close()

        elif self.name == "ESP300":
            self._object.off()

        elif self.name == "DT50":
            self._object.CloseConnection()

        elif self.name == "M-511.DD":
            self._object.CloseConnection()

        elif self.name == "InteliDrives":
            self._object.close(close_serial=False)

        elif self.name == "InteliDrivesSingle":
            self._object.close(axis=self.axis)

        elif self.name == "GRBL":
            self._object.close()

        elif self.name == "Zaber":
            self._object.close()


    def Test_Connection(self):
        """Test the connection.

        Supported devices:
            * SMC100.
            * ESP300.
            * M-511.DD.
            * GRBL.
        """
        if self.name == "SMC100":
            self._object.reset_and_configure()
            if self._object.get_status()[0] == 0:
                print('Stage SMC100 ready')

        elif self.name == "ESP300":
            print("Connected to " + self._object.id)

        elif self.name == 'M-511.DD':
            if self._object.HasqVER():
                print('version info: {}'.format(self._object.qVER().strip()))

        elif self.name == "GRBL":
            print("Status: ", self._object.get_status())
            
        elif self.name == "Zaber":
            device_list = self._object.detect_devices()
            print("Found {} devices".format(len(device_list)))
            device = device_list[0]
            print(device)
            print("id  = {}".format(device.device_id))
            print("s/n = {}".format(device.serial_number))
        else:
            print("This is not a valid method for motor {}.".format(self.name))

    def Home(self, busy=True, index=None):
        """Home the stage.
        
        ESP300 args:
            index (int or None): Axis index. Default: None.

        InteliDrives and DT50 args:
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        self.Clear_Reference()
        if self.name == "SMC100":
            self._object.home()

        elif self.name == "ESP300":
            # Look for axis limits
            if index == 2:
                self._object.home_velocity_low(CONF_ESP_300["home_vel_low_axis_2"])
                self._object.home_velocity_high(CONF_ESP_300["home_vel_high_axis_2"])
                self._object.home_search(3)  # Search for max position always
            else:
                self._object.home_velocity_low(CONF_ESP_300["home_vel_low"])
                self._object.home_velocity_high(CONF_ESP_300["home_vel_high"])
                self._object.home_search(4)  # Search for minimum position always
            if busy:
                self.Wait_Movement()
            # Set limits
            if index == 0:
                range = [CONF_ESP_300["left_limit"], CONF_ESP_300["right_limit_axis_0"]]
            elif index == 1:
                range = self._object.travel_limits()
                range = [range["left"] + CONF_ESP_300["left_limit"] , range["right"] - CONF_ESP_300["left_limit"]]
            else:
                range = self._object.travel_limits()
                range = [-range["right"] + CONF_ESP_300["left_limit"] , -CONF_ESP_300["left_limit"]]
            range = np.array(range)
            self._object.range = range
            # Move to a valid position
            if index == 2:
                self._object.move_to(-CONF_ESP_300["left_limit"]*2)
            else:
                self._object.move_to(CONF_ESP_300["left_limit"]*2)
            # Print limits
            print("Axis ", index, "limits: ")
            print("- Minimum: ", np.abs(range).min())
            print("- Maximum: ", np.abs(range).max())
            # Invert
            self.Set_Invert(CONF_ESP_300["sign"][index])

        elif self.name in ("DT50", "Newport8742"):
            print(self._object)
            self.Move_Absolute(pos=0, busy=busy, verbose=False, move_time=1)

        elif self.name == 'M-511.DD':
            self._object.CST('1','M-511.DD')
            self._object.INI('1')
            self._object.REF('1')
            time.sleep(4)


        elif self.name in ('InteliDrives', "InteliDrivesSingle"):
            self._object.home(axis=self.axis)

        elif self.name == "GRBL":
            # self._object.home()
            # print("Method currently not working")
            
            # Move to the limit
            self.Set_Velocity(CONF_GRBL["home_vel_fast"])
            self._object.move_relative(dist=-CONF_GRBL["home_dist"], check_limits=False)
            self.Wait_Movement()
            # Reset
            self.Reset()
            # Look again for the limit slowly
            self.Set_Velocity(CONF_GRBL["home_vel_slow"])
            self._object.move_relative(dist=2*CONF_GRBL["home_debounce"], check_limits=False)
            self.Wait_Movement()
            # Reset again
            self.Reset()

        elif self.name == 'InteliDrives':
            self._object.home(axis=self.axis)
            

        elif self.name == 'Zaber':
            self.axis.home()


    def Reset(self):
        """Reset the motor in case hard limits are found.

        Supported devices:
            * GRBL.
        """
        if self.name == "GRBL":
            # Reset
            self._object.hard_reset()
            # Move back
            self._object.set_hard_limits(0)
            self.Set_Velocity(CONF_GRBL["home_vel_fast"])
            self.Move_Relative(dist=CONF_GRBL["home_debounce"], move_time=0)
            self._object.set_hard_limits(1)

        else:
            print("Method Reset not supported for this motor.")


    def Invert(self):
        """Invert the stage direction.

        Supported devices:
            * All.
        """
        self.sign = -self.sign
        self.ref = -self.ref
        for key in self.stored_pos:
            self.stored_pos[key] *= -1

    def Set_Invert(self, sign):
        """Invert the stage direction.

        Supported devices:
            * All.
        """
        self.sign = sign
        self.ref = sign * self.ref
        for key in self.stored_pos:
            self.stored_pos[key] *= -1

    def Set_Reference(self, pos=None, units=None, verbose=False):
        """Sets the reference (0 value position).

        Args:
            pos (None or float): If None, the current position is set as the position reference. If float, that number is used as position reference. Default: None.
            units (str): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            verbose (bool): If True and pos is None, the current position is printed. Default: False.

        Returns:
            pos (float): Current position.

        Supported devices:
            * All.
        """
        if pos is None:
            self.ref = self.Get_Position(
                 units=None, refered=False,verbose=False)
        else:
            if not units:
                units = self.pos_units
            if units in UNITS_POS:
                self.ref = self.sign * pos * UNITS_POS[units]
            else:
                self.ref = self.sign * pos

        return self.Get_Position(units=units, verbose=True, refered=True)

    def Clear_Reference(self, verbose=True):
        """Clears the reference.

        Supported devices:
            * All
        """
        self.ref = 0

    def Get_Position(self, units=None, refered=True, verbose=False):
        """Get the current position.

        Args:
            units (str): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            refered (bool): If True, the current position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the current position is printed. Default: True.

        Returns:
            pos (float): Current position.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        if not units:
            units = self.pos_units
        pos = None
        shift = refered * self.ref

        if self.name == "SMC100":
            pos = self._object.get_position_mm()

        elif self.name == "ESP300":
            pos = self._object.position

        elif self.name == "DT50":
            pos = self._object.qPOS('1')['1']

        elif self.name == 'M-511.DD':
            dic = self._object.qPOS('1')
            dic = self._object.qPOS('1')
            dic = self._object.qPOS('1')
            pos = dic.get('1')

        elif self.name == "Newport8742":
            pos = self._object.send_command(
                "TP?", axis=self.axis) * CONF_NEWPORT_8742["step_2_mm"]

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            pos = self._object.get_position(axis=self.axis) / CONF_INTEL['counts_per_deg']

        elif self.name == "GRBL":
            pos =  self._object.get_position() or 0


        elif self.name == "Zaber":
            
            if units == 'deg':
                u = Units.ANGLE_DEGREES
            elif units == 'rad':
                u = Units.ANGLE_RADIANS
            elif units == 'pulses':
                u = Units.NATIVE
            
            pos=self.axis.get_position(u)
            
            
            
        if units in UNITS_POS:
            pos = self.sign * (pos - shift) * UNITS_POS[units]

        else:
            raise ValueError('{} is not a valid unit.'.format(units))
        

        if verbose:
            print('Current position is {} {}'.format(pos, units), end='\r')
        return pos

    def Move_Relative(self,
                      dist,
                      units=None,
                      move_time=DEFAULT_MOVE_TIME,
                      refered=True,
                      verbose=False,
                      busy=False):
        """Move stage relative to the current position.

        Args:
            dist (float): Distance to move.
            units (str): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            refered (bool): If True, the current position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        ESP300 args:
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.

        Returns:
            pos (float): Final position.

        Notes:
            * SMC100 drivers keep Python busy until the movement ends.
            * Newport8742 has 3 motors and setting a move don't get Python busy. However, if a new command is issued before the movement ends, it is ignored. For that reason, we lock Python until its movement has finished.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        # Order to not move when having multiple axes
        if dist == None:
            return
        
        if not units:
            units = self.pos_units

        # Auto calculate velocity
        if move_time:
            old_vel = self.Get_Velocity()
            if dist != 0.0:
                new_vel = np.abs(dist) / max(move_time, DEFAULT_MIN_MOVE_TIME)
                self.Set_Velocity(vel=new_vel, units=(units + '/s'))


        # Corrections
        if units in UNITS_POS:
            if self.name == "DT50":
                dist *= UNITS_POS[units]
            else:
                dist *= self.sign / UNITS_POS[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))
        # Move
        if self.name == "SMC100":
            self._object.move_relative_mm(dist)

        elif self.name == "ESP300":
            self._object.move_by(dist, False)

        elif self.name == "DT50":
            new_pos = self.Get_Position(units=units, refered=False) + dist
            final_pos = self.Move_Absolute(
                new_pos,
                units=units,
                move_time=None,
                busy=busy,
                refered=False,
                verbose=False)

        elif self.name == 'M-511.DD':
            self._object.MVR(('1',),dist)
            #time.sleep(DEFAULT_MOVE_TIME)

        elif self.name == "Newport8742":
            busy = True
            self._object.send_command(
                "PR",
                axis=self.axis,
                var=int(dist / CONF_NEWPORT_8742["step_2_mm"]))
        
        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            self._object.move_relative(axis=self.axis, value=dist*CONF_INTEL['counts_per_deg'])

        elif self.name == "GRBL":
            self._object.move_relative(dist)
            
        elif self.name == "Zaber":
            if busy:
                old_wait = self.wait_until_idle
                self.Wait_Movement(value=True, verbose=False)

            if self.pos_units == 'deg':
                self.axis.move_relative(dist, Units.ANGLE_DEGREES, velocity=self.current_vel, velocity_unit=Units.ANGULAR_VELOCITY_DEGREES_PER_SECOND,
                                        wait_until_idle = self.wait_until_idle)
            elif self.pos_units == 'rad':
                self.axis.move_relative(dist, Units.ANGLE_RADIANS, velocity=self.current_vel, velocity_unit=Units.ANGULAR_VELOCITY_RADIANS_PER_SECOND,
                                        wait_until_idle = self.wait_until_idle)  
            if busy:
                self.Wait_Movement(value=old_wait, verbose=False)
        
               
        if busy:
            self.Wait_Movement(value=True, verbose=False)

        if move_time and (self.name in ( "SMC100", 'Zaber') or busy):
            self.Set_Velocity(vel=old_vel)


        return self.Get_Position(units=units, refered=refered, verbose=verbose)

    def Move_Absolute(self,
                      pos=0,
                      units=None,
                      move_time=DEFAULT_MOVE_TIME,
                      refered=True,
                      verbose=False,
                      busy=True):
        """Move stage to a given position.

        Args:
            pos (float): Position to move to. Default: 0.
            units (str): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            refered (bool): If True, the absolute position is calculated respect to the stored reference. Default: True.
            verbose (bool): If True, the final position is printed. Default: False.

        ESP300 args:
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.

        Returns:
            pos (float): Final position.

        Notes:
            * SMC100 drivers keep Python busy until the movement ends.
            * Newport8742 has 3 motors and setting a move don't get Python busy. However, if a new command is issued before the movement ends, it is ignored. For that reason, we lock Python until its movement has finished.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        # Order to not move when having multiple axes
        if pos == None:
            return
        
        if not units:
            units = self.pos_units

        # Auto calculate velocity
        if move_time:
            old_vel = self.Get_Velocity()
            dist = np.abs(
                self.Get_Position(units=units, refered=refered) - pos)
            if dist > 0.01:
                new_vel = dist / max(move_time, DEFAULT_MIN_MOVE_TIME)
                #print('Velocity',new_vel)
                self.Set_Velocity(vel=new_vel, units=(units + '/s'))

        # Corrections
        if units in UNITS_POS:
            shift = refered * self.ref
            pos = self.sign * (pos / UNITS_POS[units] - shift)
        else:
            raise ValueError('{} is not a valid unit.'.format(units))

        # Move
        if self.name == "SMC100":
            self._object.move_absolute_mm(self.sign * (pos + shift))

        elif self.name == "ESP300":
            self._object.move_to(pos, False)

        elif self.name == "DT50":
            self._object.MOV({"1": pos})

        elif self.name == 'M-511.DD':
            self._object.MOV(('1',),pos)

        elif self.name == "Newport8742":
            busy = True
            self._object.send_command(
                "PA",
                var=int(pos / CONF_NEWPORT_8742["step_2_mm"]),
                axis=self.axis)

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            self._object.move_absolute(axis=self.axis, value=pos*CONF_INTEL['counts_per_deg'])

        elif self.name == "GRBL":
            self._object.move_absolute(pos)

        elif self.name == "Zaber":
            
            if self.pos_units == 'deg':
                self.axis.move_absolute(pos, Units.ANGLE_DEGREES, 
                                        velocity=self.current_vel, velocity_unit=Units.ANGULAR_VELOCITY_DEGREES_PER_SECOND,
                                        wait_until_idle = self.wait_until_idle)
            elif self.pos_units == 'rad':
                self.axis.move_absolute(dist, Units.ANGLE_RADIANS, velocity=self.current_vel, velocity_unit=Units.ANGULAR_VELOCITY_RADIANS_PER_SECOND,
                                        wait_until_idle = self.wait_until_idle)  
        if busy:
            self.Wait_Movement(verbose=False)

        if move_time and (self.name == "SMC100" or busy):
            self.Set_Velocity(vel=old_vel)

        return self.Get_Position(units=units, verbose=verbose)

    def Get_Velocity(self, units=None, verbose=False):
        """Get the current (current or stored?) stage velocity.

        Args:
            units (str): Position units to choose between m/s, mm/s and um/s, or deg/s and rad/s. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the velocity is printed. Default: False.

        Returns:
            vel (float): Current velocity.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        if not units:
            units = self.vel_units
        vel = None

        if self.name == "SMC100":
            vel = self._object.get_velocity()

        elif self.name == "ESP300":
            vel = self._object.velocity

        elif self.name == "DT50":
            vel = self._object.qVEL('1')['1']

        elif self.name == 'M-511.DD':
            d = self._object.qVEL('1')
            d = self._object.qVEL('1')
            d = self._object.qVEL('1')
            vel = d.get('1')

        elif self.name == "Newport8742":
            vel = self._object.send_command(
                "VA?", axis=self.axis) * CONF_NEWPORT_8742["step_2_mm"]

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            vel = self._object.get_velocity(axis=self.axis) / CONF_INTEL['vel_const']

        elif self.name == "GRBL":
            vel = self._object.vel / 60 # mm/min -> mm/s

        elif self.name == "Zaber":
            vel = self.current_vel
             

            
        # Units
        try: 
            vel *= UNITS_VEL[units]
        except:
            raise ValueError('{} is not a valid unit.'.format(units))

        if verbose:
            print('Current velocity is {} {}'.format(vel, units))
        return vel

    def Set_Velocity(self, vel, units=None, verbose=False):
        """Get the current stage velocity.

        Args:
            vel (float): New velocity.
            units (str): Position units to choose between m/s, mm/s and um/s, or deg/s and rad/s. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            vel (float): Set velocity.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        # Units
        if not units:
            units = self.vel_units
        if units in UNITS_VEL:
            vel /= UNITS_VEL[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))

        if self.name == "SMC100":
            self._object.set_velocity(vel)

        elif self.name == "ESP300":
            if vel > self.vel_max:
                print('WARNING: {} speed reduced from {} to {} {}.'.format(
                    self.name, vel, self.vel_max, self.vel_units))
                vel = self.vel_max
            self._object.setVelocity(vel)
            ac = vel / DEFAULT_ACCEL_TIME
            self._object.setAcceleration(min(ac, self.ac_max))
            self._object.setDeceleration(min(ac, self.ac_max))

        elif self.name == "DT50":
            self._object.VEL({"1": vel})

        elif self.name == "M-511.DD":
            self._object.VEL({"1": vel})
            #self._object.VEL({"1": vel})
            #self._object.VEL({"1": vel})

        elif self.name == "Newport8742":
            self._object.send_command(
                "VA",
                axis=self.axis,
                var=int(vel / CONF_NEWPORT_8742["step_2_mm"]))

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            vel = self._object.set_velocity(axis=self.axis, value=vel*CONF_INTEL['vel_const'])

        elif self.name == "GRBL":
            self._object.vel = vel * 60 # mm/s -> mm/min
            
        elif self.name == "Zaber":
            self.current_vel = vel
            
        

        return self.Get_Velocity(units=units, verbose=verbose)

    def Save_Position(self,
                      name="default",
                      pos=None,
                      units=None,
                      refered=True,
                      verbose=False):
        """Saves a position in the stored positions dictionary.

        Args:
            name (str): Name of the reference. Default: 'default'.
            pos (float or None): Position to save. If None, it is the current position. Default: None.
            units (str): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            refered (bool): If True, the absolute position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the current position is printed. Default: False.

        Returns:
            pos (float): Saved position.

        Supported devices:
            * All.
        """
        if not units:
            units = self.pos_units

        if pos is None:
            self.stored_pos[name] = self.Get_Position(
                verbose=verbose, refered=False)
        else:
            if units == 'um':
                pos *= 1e-3
            elif units == 'm':
                pos *= 1e3
            if refered:
                pos += self.ref
            self.stored_pos[name] = pos
        return self.stored_pos[name]

    def Clear_Position(self, name='default'):
        """Saves the current position in the references library.

        Args:
            name (str): Name of the reference. Default: 'default'.

        Supported devices:
            * All.
        """
        self.stored_pos.pop(name, 0)

    def Move_To_Position(self,
                         name="default",
                         move_time=DEFAULT_MOVE_TIME,
                         units=None,
                         refered=True,
                         verbose=False,
                         busy=True):
        """Move stage to a stored position.

        Args:
            name (str): Name of the reference. Default: 'default'.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            units (str): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            refered (bool): If True, the absolute position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        ESP300 args:
            busy (bool): If True, the program will wait for the completion of the movement before continuing. Default: True.

        Returns:
            pos (float): Final position.

        Supported devices:
            * All.
        """
        if name in self.stored_pos:
            pos = self.Move_Absolute(
                pos=self.stored_pos[name],
                move_time=move_time,
                units=None,
                verbose=False,
                refered=False,
                busy=busy)
            if verbose:
                self.Get_Position(units=units, refered=refered, verbose=True)
        else:
            raise ValueError(
                'There is no saved reference of name {}'.format(name))
            pos = None
        return pos

    def Is_Moving(self, verbose=False):
        """Sees if the motor is moving or not.

        Args:
            verbose (bool): If True, the current status is printed. Default: False.

        Returns:
            moving (bool): Result.

        Supported devices:
            * SMC100.
            * ESP300.
            * DT50.
            * Newport8742.
            * InteliDrives.
            * GRBL.
        """
        if self.name == "SMC100":
            moving = False

        elif self.name == "ESP300":
            moving = self._object.moving

        elif self.name == "DT50":
            moving = self._object.IsMoving(' '.join(CONF_DT_50["axes"]))
            moving = sum(moving.values()) > 0

        elif self.name == "Newport8742":
            moving = not self._object.send_command(
                "MD?", axis=self.axis, return_type=bool)

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            moving = self._object.is_moving(axis=self.axis)

        elif self.name == "GRBL":
            vel = self._object.get_vel()
            moving = (vel is not None) and (vel > 1e-6)
        
        elif self.name == "Zaber":
            vel=self.axis.settings.get('vel', unit=Units.ANGULAR_VELOCITY_DEGREES_PER_SECOND)
            moving = np.abs(vel) > 1e-6


        if verbose:
            if moving:
                print("Motor is moving")
            else:
                print("Motor is not moving")

        return moving

    def Wait_Movement(self, value=True, verbose=False):
        """Gets the python kernel busy until the movement has ended.

        Args:
            value (bool): If True, wait untile movement is finished.
            verbose (bool): If True, the final position is printed. Default: False.

        Supported devices:
            * ESP300.
            * DT50.
            * Newport8742.
            * M-511.DD.
            * InteliDrives.
            * GRBL.
        """
        if self.name in ("ESP300", "DT50", "Newport8742", "GRBL","Zaber"):
            if value:
                moving = self.Is_Moving()
                while moving:
                    if verbose:
                        self.Get_Position(verbose=True)
                    time.sleep(DEFAULT_SLEEPING_TIME)
                    moving = self.Is_Moving(verbose=False)

        elif self.name == "M-511.DD":
            #time.sleep(4)
            if value:
                moving = 1
            else:
                moving = 0
            while moving:
                pos_ant = self.Get_Position(verbose=False)
                time.sleep(1e-2)
                pos = self.Get_Position(verbose=False)
                if np.abs(pos-pos_ant)>1e-6:
                    #print('Moviendo')
                    moving = 1
                else:
                    moving = 0
        # elif self.name == "Zaber":
        #     self.wait_until_idle=value



    def Stop_Motion(self):
        """Decelerates and stops the motion of the current axis.

        Supported devices:
            * Newport8742.
            * InteliDrives.
            * GRBL.
        """
        if self.name == "Newport8742":
            self._object.send_command("ST", axis=self.axis)

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            self._object.abort(axis=self.axis)

        elif self.name == "GRBL":
            self._object.feed_hold()
            
        elif self.name == "Gerber":
            self._object.stop()

    def Get_Acceleration(self, units=None, verbose=False):
        """Get the aceleration.

        Args:
            units (str): Position units to choose between m/s2, mm/s2 and um/s2, or deg/s2 and rad/s2. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            ac (float): Current acceleration.

        Supported devices:
            * Newport8742.
            * InteliDrives.
            * GRBL.
        """
        if self.name == "Newport8742":
            ac = self._object.send_command("AC?", axis=self.axis)

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            ac = self._object.get_max_acceleration(axis=self.axis) / CONF_INTEL['ac_const']

        elif self.name == "GRBL":
            ac = self._object.get_parameter(120)

        else:
            ac = np.nan

        # Units
        if units in UNITS_ACEL:
            ac *= UNITS_ACEL[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))

        if verbose:
            print('Current acceleration is {} {}'.format(ac, units))
        return ac

    def Set_Acceleration(self, ac, units=None, verbose=False):
        """Get the aceleration.

        Args:
            ac (number): Acceleration.
            units (str): Position units to choose between m/s2, mm/s2 and um/s2, or deg/s2 and rad/s2. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            ac (float): Set acceleration.

        Supported devices:
            * Newport8742.
            * InteliDrives.
            * GRBL.
        """
        # Units
        if units in UNITS_ACEL:
            ac *= UNITS_ACEL[units]
        else:
            raise ValueError('{} is not a valid unit.'.format(units))

        # Set
        if self.name == "Newport8742":
            self._object.send_command("AC", axis=self.axis, var=ac)

        elif self.name in ("InteliDrives", "InteliDrivesSingle"):
            self._object.set_max_accceleration(axis=self.axis, value=ac*CONF_INTEL['ac_const'])
            self._object.set_max_decceleration(axis=self.axis, value=ac*CONF_INTEL['ac_const'])

        elif self.name == "GRBL":
            ac = self._object.set_parameter(120, ac)

        return self.Get_Acceleration(units=units, verbose=verbose)


class Motor_Multiple(object):
    """
    General class for multiple motors.

    NOTE: Most method accepts lists/np.ndarrays or single elements. If a single element is provided, it is used for all motors.

    Args:
        name (string or list of strings): Name of the motors. Available motors are: DT50, ESP300, Newport8742.
        N (int): Number of motors. This parameter is replaced by len(name) if it is a list. Default: 1.

    Atributes:
        name (list): Name of the motors.
        N (int): Number of motors
        range (np.ndarray): Min and max absolute positions.
        sign (np.ndarray): Sign to invert the stage movement direction if desired.
        stored_pos (dict): Dictionary of saved references.
        ref (np.ndarray): Position of 0 reference. Positions will be calculated respect to this position if refered variable of methods is True. Units: mm or deg.
        pos_units (list): Default position units.
        vel_units (list): Default velocity units.
        _object (list): Motor objects. Its class depends on which device is being used. used.

    ESP300 and Newport8742 atributes (Some methods are global and must be given to their respective objects):
        _esp (object): Parent object of the ESP300.
        _8742 (object): Parent object of the Newport8742.

    Supported devices:
        * ESP300.
        * DT50.
        * Newport8742.
        * InteliDrives.
        * Zaber.
    """

    def __init__(self, name, N=1):
        """Initialize the object.

        Supported devices:
            * DT50.
            * ESP300.
            * Newport8742.
            * InteliDrives.
            * Zaber.
        """
        if isinstance(name, str):
            name = [name] * N
        else:
            N = len(name)

        self.N = N
        self.name = name
        self.stored_pos = {}
        self.ref = np.zeros(N)
        self.pos_units = []
        self.vel_units = []

        for n in name:
            if n == "Newport8742":
                self.pos_units.append(CONF_SMC_100["pos_units"])
                self.vel_units.append(CONF_SMC_100["vel_units"])

            elif n == "ESP300":
                self.pos_units.append(CONF_ESP_300["pos_units"])
                self.vel_units.append(CONF_ESP_300["vel_units"])

            elif n == "DT50":
                self.pos_units.append(CONF_DT_50["pos_units"])
                self.vel_units.append(CONF_DT_50["vel_units"])

            elif n == "Newport8742":
                self.pos_units.append(CONF_NEWPORT_8742["pos_units"])
                self.vel_units.append(CONF_NEWPORT_8742["vel_units"])

            elif n == "InteliDrives":
                self.pos_units.append(CONF_INTEL["pos_units"])
                self.vel_units.append(CONF_INTEL["vel_units"])

            elif n == "Zaber":
                self.pos_units.append(CONF_INTEL["pos_units"])
                self.vel_units.append(CONF_INTEL["vel_units"])

            else:
                raise ValueError(
                    "{} is not a valid Motor_Multiple name.".format(name))

        self._object = []
        for Name in name:
            self._object.append(Motor(Name))

        self.Clear_Reference()


    def __enter__(self):
        pass

    def __exit__(self):
        """Delete object last function.

        Supported devices:
            * all
        """
        self.Close()

    def Open(self, port=None, invert=False, axis=1):
        """Open the object.

        Args:
            port (str or list): List of device port names. If None, it is taken from the configuration file. Default: None.
            invert (bool, list or None): If True, the movement direction is inverted (switches the sign of positions). Default: False.

        ESP300 args:
            axis (int): Axis of the stage (only 1, 2 or 3 valid). Default: 1.

        Supported devices:
            * DT50.
            * ESP300.
            * Newport8742.
            * InteliDrives.
        """
                
        # Dimensions check
        if isinstance(port, str) or isinstance(port, int):
            port = [port] * self.N
        if isinstance(invert, bool):
            invert = [invert] * self.N
        if isinstance(axis, (str, int)):
            axis = [axis] * self.N
        if port and ((len(port) != self.N) or (len(invert) != self.N)):
            raise ValueError(
                "The number of provided ports ({}) or signs ({}) does not correspond with the number of motors ({})".
                format(len(port), len(invert), self.N))

        # Act
        self._esp = None
        self._8742 = None
        self._intel = None
        self._zaber = None
        indDT = 0

        for ind, obj in enumerate(self._object):                      
            if obj.name == "ESP300":
                if self._esp is None:
                    self._esp = ESP(port[ind])
                obj.Open(
                    parent_object=self._esp,
                    invert=invert[ind],
                    axis=(axis or CONF_ESP_300['axes'])[ind])

            elif obj.name == "DT50":
                if port:
                    obj.Open(port=port[ind], invert=invert[ind])
                else:
                    obj.Open(
                        port=CONF_DT_50["ports"][indDT],
                        invert=(invert or CONF_DT_50["invert"])[ind],
                        axis=axis[ind])
                    indDT += 1

            elif obj.name == "Newport8742": # Falta [ind]?
                if self._8742 is None:
                    self._8742 = USB(
                        idVendor=CONF_NEWPORT_8742["idVendor"],
                        idProduct=CONF_NEWPORT_8742["idProduct"],
                        config=CONF_NEWPORT_8742["config"],
                        ep_out=CONF_NEWPORT_8742["ep_out"],
                        ep_in=CONF_NEWPORT_8742["ep_in"],
                        end_char=CONF_NEWPORT_8742["end_char"])
                obj.Open(
                    parent_object=self._8742,
                    invert=invert[ind],
                    axis=(axis or CONF_NEWPORT_8742['axes'])[ind])

            elif obj.name == "InteliDrives":
                if self._intel is None:
                    self._intel = InteliDrives(CONF_INTEL['ports'])
                    self._intel.open()
                obj.Open(
                    parent_object=self._intel,
                    #invert=(invert or CONF_INTEL["invert"])[ind],
                    invert=CONF_INTEL["invert"][ind],
                    axis=(axis or CONF_INTEL['axes'])[ind])
                self.sign = np.ones(self.N) - 2 * CONF_INTEL["invert"]
                
            elif obj.name == 'Zaber':

                if port is not None:
                    port = port[0]
                if self._zaber is None:
                    self._zaber=Connection.open_serial_port(port or CONF_ZABER['port'])

                obj.Open(parent_object=self._zaber,
                    invert=False,
                    axis=(axis)[ind] or CONF_ZABER["axes"])

            else:
                obj.Open(port=port[ind], invert=invert[ind], axis=axis[ind])

    def Close(self):
        """Close the object.

        Supported devices:
            * DT50.
            * ESP300.
            * Newport8742.
            * InteliDrives.
        """
        # Close individuals
        esp = False
        new8742 = False
        intel = False
        for obj in self._object:
            obj.Close()

        # Close globals
        if self._esp:
            self._esp.ser.close()
        if self._8742:
            self._8742.close()
        if self._intel:
            self._intel.close(disable_motors=False)

    def Test_Connection(self):
        """Test the connections.

        Supported devices:
            * DT50.
            * ESP300.
            * Newport8742.
        """
        print_8742 = True
        for obj in self._object:
            if obj.name == "Newport8742":
                if print_8742:
                    print(self._8742.send_command("*IDN?", return_type=str))
                    print_8742 = False

            else:
                obj.Test_Connection()

    def Home(self, mode=1, seconds_wait=3, waiting='sequential', verbose=False):
        """Home the stages.

        Args:
            verbose (bool): If True, the final position is printed. Default: False.
            seconds_wait (int): Waiting time until the movementis completed. Default: 3

        ESP300, InteliDrives and DT50 args:
            waiting (str): If 'busy', all movements will be simultaneous and the program will wait to the end of all movements. If 'sequential', the program will execute one movement after the other and will wait till the end of the last one. Otherwise, all movements will be performed simultaneously and the program will continue without waiting. Default: 'sequential'.

        Supported devices:
            * DT50.
            * ESP300.
            * Newport8742.
            * InteliDrives.
        """
        busy = True if waiting == 'sequential' else False
        self.Clear_Reference()
        for ind, obj in enumerate(self._object):
            obj.Home(busy=busy, index=ind)
        if waiting == 'busy':
            self.Wait_Movement(seconds_wait=seconds_wait)
        if verbose:
            self.Get_Position(refered=False, verbose=verbose)

    def Invert(self, axis=None):
        """Invert the stage direction.

        Args:
            axis (None, int or iterable): Number of motor to invert. If None, all will be inverted. Default: None.

        Supported devices:
            * All.
        """
        if axis is None:
            self.sign = -self.sign
            for obj in self._object:
                obj.Invert()
        elif isinstance(axis, int):
            self.sign[axis] = -self.sign[axis]
            self._object[axis].Invert()
        else:
            for ind in axis:
                self.sign[ind] = -self.sign[ind]
                self._object[ind].Invert()

    def Set_Reference(self, pos=None, axis=None, units=None, verbose=False):
        """Sets the reference (0 value position).

        Args:
            pos (None, float or np.ndarray): If None, the current position is set as the position reference. If array, that number is used as position reference. Default: None.
            axis (None, int or list): Number of motor to set the reference. If None, all will be referenced. Default: None.
            units (str or list): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            verbose (bool): If True and pos is None, the current position is printed. Default: False.

        Returns:
            pos (np.ndarray): Current position.

        Supported devices:
            * All.
        """
        # Get current values
        pos, units = adapt_to_multiple(N=self.N, var=(pos, units))
        current_pos = self.Get_Position(
            verbose=False, refered=False, units=False)
        current_ref = self.ref

        # Select
        pos = select_pos(
            pos1=pos,
            pos2=current_pos,
            pos3=current_ref,
            units=units,
            axis=axis)

        # Save refs in individual objects
        self.ref = pos
        for ind, obj in enumerate(self._object):
            obj.Set_Reference(pos=self.ref[ind])

        return self.Get_Position(units=units, verbose=verbose, refered=True)

    def Clear_Reference(self, verbose=True):
        """Clears the reference.

        Supported devices:
            * All."""
        self.ref = np.zeros(self.N)
        for obj in self._object:
            obj.Clear_Reference()

    def Get_Position(self, units=None, refered=True, verbose=False):
        """Get the current position.

        Args:
            units (str or list): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            refered (bool): If True, the current position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the current position is printed. Default: True.

        Supported devices:
            * All.
        """
        if units is None:
            units = self.pos_units
        units = adapt_to_multiple(N=self.N, var=[units])

        pos = np.zeros(self.N, dtype=float)
        for ind, obj in enumerate(self._object):
            pos[ind] = obj.Get_Position(
                units=units[ind], refered=refered, verbose=False)

        if verbose:
            print('Current position is:')
            for ind, p in enumerate(pos):
                print('-  Motor {}: {} {}.'.format(ind, p, units[ind]))
        return pos

    def Move_Relative(self,
                      dist,
                      move_time=DEFAULT_MOVE_TIME,
                      units=None,
                      refered=True,
                      verbose=False,
                      waiting='busy', 
                      busy = False):
        """Move stage relative to the current position.

        Args:
            dist (float or list): Distance to move.
            units (str or list): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            move_time (float or list): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            seconds_wait (int): Waiting time until the movementis completed. Default: 3
            refered (bool): If True, the current position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        ESP300, InteliDrives and DT50 args:
            waiting (str): If 'busy', all movements will be simultaneous and the program will wait to the end of all movements. If 'sequential', the program will execute one movement after the other and will wait till the end of the last one. Otherwise, all movements will be performed simultaneously and the program will continue without waiting. Default: 'busy'.

        Returns:
            pos (float): Final position.

        Supported devices:
            * All.
        """
        if not units:
            units = self.pos_units

        units, dist, move_time = adapt_to_multiple(
            N=self.N, var=(units, dist, move_time))
        dist = np.array(dist, dtype=float)
        for ind, obj in enumerate(self._object):
            obj.Move_Relative(
                dist=dist[ind],
                units=units[ind],
                move_time=move_time[ind],
                verbose=False,
                busy=waiting=='sequential')
        
        if waiting == 'busy':
            self.Wait_Movement(seconds_wait=np.max(move_time))

        return self.Get_Position(units=units, refered=refered, verbose=verbose)

    def Move_Absolute(self,
                      pos=0,
                      move_time=DEFAULT_MOVE_TIME,
                      axis=None,
                      units=None,
                      refered=True,
                      verbose=False,
                      waiting='busy'):
        """Move stage to a given position.

        Args:
            pos (float or iterable): Position to move to. Default: 0.
            axis (None, int or list of ints): Axes to move. If None, all axis are moved. Default: None.
            units (str or list): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            move_time (float or iterable): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            seconds_wait (int): Waiting time until the movementis completed. Default: 3
            refered (bool): If True, the absolute position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        ESP300, InteliDrives and DT50 args:
            waiting (str): If 'busy', all movements will be simultaneous and the program will wait to the end of all movements. If 'sequential', the program will execute one movement after the other and will wait till the end of the last one. Otherwise, all movements will be performed simultaneously and the program will continue without waiting. Default: 'busy'.

        Returns:
            pos (float): Final position.

        Supported devices:
            * All.
        """
        axis = transform_axis_variable(axis, self.N)
        if not units:
            units = self.pos_units
        units, pos, move_time = adapt_to_multiple(
            N=self.N, var=(units, pos, move_time), axis=axis)

        busy = True if waiting == 'sequential' else False
    
        for ind, obj in enumerate(self._object):
            if ind in axis:
                obj.Move_Absolute(
                    pos=pos[ind],
                    units=units[ind],
                    move_time=move_time[ind],
                    verbose=False,
                    busy=busy)
        if waiting == 'busy':
            self.Wait_Movement(seconds_wait=np.max(move_time)*1.1)
        #time.sleep(0.1)
        return self.Get_Position(units=units, refered=refered, verbose=verbose)

    def Get_Velocity(self, units=None, verbose=False):
        """Get the current stage velocity.

        Args:
            units (str or list): Position units to choose between m/s, mm/s and um/s, or deg/s and rad/s. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Supported devices:
            * All.
        """
        if not units:
            units = self.vel_units
        units = adapt_to_multiple(N=self.N, var=[units])
        vel = np.zeros(self.N, dtype=float)

        for ind, obj in enumerate(self._object):
            vel[ind] = obj.Get_Velocity(units=units[ind], verbose=False)

        if verbose:
            print('Current velocity is:')
            for ind, v in enumerate(vel):
                print('-  Motor {}: {} {}.'.format(ind, v, units[ind]))
        return vel

    def Set_Velocity(self, vel, units=None, verbose=False):
        """Get the current stage velocity.

        Args:
            vel (float or iterable): New velocity.
            units (str or list): Position units to choose between m/s, mm/s and um/s, or deg/s and rad/s. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            vel (float): Set velocity.

        Supported devices:
            * All.
        """
        if not units:
            units = self.vel_units
        units, vel = adapt_to_multiple(N=self.N, var=(units, vel))

        for ind, obj in enumerate(self._object):
            obj.Set_Velocity(vel=vel[ind], units=units[ind], verbose=False)

        return self.Get_Velocity(units=units, verbose=verbose)

    def Save_Position(self,
                      name="default",
                      pos=None,
                      axis=None,
                      units=None,
                      refered=True,
                      verbose=True):
        """Saves a position in the stored positions dictionary.

        Args:
            name (str): Name of the reference. Default: 'default'.
            pos (iterable, float or None): Position to save. If None, it is the current position. Default: None.
            axis (None, int or list): Number of motor to set the reference. If None, all will be referenced. Default: None.
            units (str or list): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            refered (bool): If True, the absolute position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the current position is printed. Default: False.

        Returns:
            pos (float): Saved position.

        Supported devices:
            * All.
        """
        # Adapt
        if not units:
            units = self.pos_units
        units, pos = adapt_to_multiple(N=self.N, var=(units, pos))
        current_pos = self.Get_Position(
            units=None, refered=False, verbose=False)
        if name in self.stored_pos:
            current_ref = self.stored_pos[name]
        else:
            current_ref = None

        # Choose
        pos = select_pos(
            pos1=pos,
            pos2=current_pos,
            pos3=current_ref,
            units=units,
            axis=axis)

        # Save
        self.stored_pos[name] = pos

        return self.stored_pos[name]

    def Clear_Position(self, name='default'):
        """Saves the current position in the references library.

        Args:
            name (str): Name of the reference. Default: 'default'.

        Supported devices:
            * All.
        """
        self.stored_pos.pop(name, 0)

    def Move_To_Position(self,
                         name="default",
                         move_time=DEFAULT_MOVE_TIME,
                         units=None,
                         refered=True,
                         verbose=False,
                         waiting='busy'):
        """Move stage to a stored position.

        Args:
            name (str): Name of the reference. Default: 'default'.
            move_time (float): If None, motor velocity is not modified. Otherwise, the velocity is calculated to obtain this move time in seconds (very long movements might require longer times). Old velocity is restored afterwards. Default: DEFAULT_MOVE_TIME.
            units (str or list): Position units to choose between m, mm and um, or deg and rad. If None, the units are taken from the default set in the object. Default: None.
            refered (bool): If True, the absolute position is calculated respect to the stored reference. Default: False.
            verbose (bool): If True, the final position is printed. Default: False.

        ESP300, InteliDrives and DT50 args:
            waiting (str): If 'busy', all movements will be simultaneous and the program will wait to the end of all movements. If 'sequential', the program will execute one movement after the other and will wait till the end of the last one. Otherwise, all movements will be performed simultaneously and the program will continue without waiting. Default: 'busy'.

        Returns:
            pos (float): Final position.

        Supported devices:
            * All.
        """
    
        if name in self.stored_pos:
            if not units:
                units = self.pos_units
            units, move_time = adapt_to_multiple(
                N=self.N, var=(units, move_time))

            busy = True if waiting == 'sequential' else False
            for ind, obj in enumerate(self._object):
                obj.Move_Absolute(
                    pos=self.stored_pos[name][ind],
                    units=None,
                    move_time=move_time[ind],
                    verbose=False,
                    busy=busy)
            if waiting == 'busy':
                self.Wait_Movement(seconds_wait=np.max(move_time))

        else:
            raise ValueError(
                'There is no saved reference of name {}'.format(name))

        return self.Get_Position(units=units, refered=refered, verbose=verbose)

    def Is_Moving(self, individual=False, verbose=False):
        """Sees if any motor is moving or not.

        Args:
            individual (bool): If True, the result is a np.ndarray with the information of each individual motor. If not, the result is a bool which is True if any motor is moving and False if all motors are still. Default: False.
            verbose (bool): If True, the current status is printed. Default: False.

        Returns:
            moving (bool or np.ndarray): Global or individual motor information.

        Supported devices:
            * All.
        """
        moving = np.zeros(self.N, dtype=bool)

        for ind, obj in enumerate(self._object):
            if self.name[ind] in ("ESP300", "DT50","InteliDrives","Zaber"):
                moving[ind] = obj.Is_Moving(verbose=False)

        #print(moving)
        if individual:
            if verbose and moving.any():
                print("Motors {} are moving.".format(self.name[moving]))
            elif verbose:
                print("No motor is moving")
        else:
            moving = moving.any()
            if verbose and moving:
                print("At least one motor is moving")
            elif verbose:
                print("No motor is moving")

        return moving

    def Wait_Movement(self, seconds_wait = 0, verbose=False):
        """Gets the python kernel busy until the movement has ended.

        Args:
            seconds_wait (int): Waiting time until the movement is completed. Default: 0
            verbose (bool): If True, the final position is printed. Default: False.

        Supported devices:
            * ESP300.
            * DT50.
            * Newport8742.
        """
        
        moving = self.Is_Moving(verbose=False, individual=False)
    
        initial = time.time()
        dt = 0
        while moving.any() and dt < (CONF_INTEL["timeout"] + seconds_wait):
            current = time.time()
            dt = current - initial
            if verbose:
                self.Get_Position(verbose=True)
            #time.sleep(DEFAULT_SLEEPING_TIME)
            moving = self.Is_Moving(verbose=False, individual=False)

    def Open_Shutter(self, wait_time=DEFAULT_SHUTTER_TIME):
        """Opens the associated mechanical shutter.

        Args:
            wait_time (float): Waiting time after sending the order. Defauult: DEFAULT_SHUTTER_TIME.

        Supported devices:
            * ESP300.
        """
        if self._esp is not None:
            self._esp.openShutter()
            time.sleep(wait_time)

    def Close_Shutter(self, wait_time=DEFAULT_SHUTTER_TIME):
        """Closes the associated mechanical shutter.

        Args:
            wait_time (float): Waiting time after sending the order. Defauult: DEFAULT_SHUTTER_TIME.

        Supported devices:
            * ESP300.
        """
        if self._esp is not None:
            self._esp.closeShutter()
            time.sleep(wait_time)

    def Abort(self):
        """Inmediately stops the motion of all axes.

        Supported devices:
            * Newport843.
        """
        if self._8742 is not None:
            self._8742.send_command("AB")

    def Stop_Motion(self):
        """Decelerates and stops the motion of the current axis.

        Supported devices:
            * All.
        """
        for obj in self._object:
            obj.Stop_Motion()

    def Get_Acceleration(self, units=None, verbose=False):
        """Get the aceleration.

        Args:
            units (str): Position units to choose between m/s2, mm/s2 and um/s2, or deg/s2 and rad/s2. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            ac (float): Current acceleration.

        Supported devices:
            * Newport8742.
            * InteliDrives.
        """
        if not units:
            units = self.vel_units
        units = adapt_to_multiple(N=self.N, var=[units])
        ac = np.zeros(self.N)

        for ind, obj in enumerate(self._object):
            ac[ind] = obj.Get_Acceleration(units=units[ind], verbose=False)

        if verbose:
            print('Current acceleration is:')
            for ind, v in enumerate(ac):
                print('-  Motor {}: {} {}.'.format(ind, v, units[ind]))
        return ac

    def Set_Acceleration(self, ac, units=None, verbose=False):
        """Get the aceleration.

        Args:
            ac (number or iterable): Acceleration.
            units (str): Position units to choose between m/s2, mm/s2 and um/s2, or deg/s2 and rad/s2. If None, it takes the object default units. Default: None.
            verbose (bool): If True, the current velocity is printed. Default: False.

        Returns:
            ac (float): Set acceleration.

        Supported devices:
            * Newport8742.
            * InteliDrives.
        """
        if not units:
            units = self.vel_units
        units, ac = adapt_to_multiple(N=self.N, var=(units, ac))

        for ind, obj in enumerate(self._object):
            obj.Set_Acceleration(ac=ac[ind], units=units[ind], verbose=False)

        return self.Get_Acceleration(units=units, verbose=verbose)
    
    def Read_Error(self):
        """Clears errors of ESP300"""
        for name in self.name:
            if name == "ESP300":
                return self._esp.read_error()
        else:
            raise ValueError("This method is not valid for motors other than ESP300")
