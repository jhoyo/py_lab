import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import os
from datetime import datetime
import time
from threading import Thread
import screeninfo
import cv2
from PIL import Image
from instrumental import instrument, list_instruments ,u

from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio import mm

from py_lab.config import CONF_IMAGING_SOURCE, CONF_IMAGING_SOURCE_2, CONF_DC1645C, CONF_UEYE, CONF_UEYE_SLM, CONF_UEYE_LCD
import py_lab.drivers.cameras.ImagingSource.tisgrabber as IC
try:
    from py_lab.drivers.cameras.uEye.uEye import uEye
except:
    pass
import py_lab.utils as utils

um = 1e3
clear_background_properties = ("resolution", "gain", "exposure", "pixelclock", "lineargain", "gainlinear")
cv2_colormap_names = {}
cv2_colormap_names["jet"] = cv2.COLORMAP_JET
cv2_colormap_names["rainbow"] = cv2.COLORMAP_RAINBOW
cv2_colormap_names["hot"] = cv2.COLORMAP_HOT
cv2_colormap_names["viridis"] = cv2.COLORMAP_VIRIDIS
cv2_colormap_names["twilight"] = cv2.COLORMAP_TWILIGHT


class Camera(object):
    """General class for cameras.

    Args:
        name (string): Name of the camera.
        M (float): Magnification. Default: 1.
        wavelength (float): Wavelength (in mm). Default: Config.

    uEye args:
        uEye_HIDS (int): Number of camera. Default: Config.
        uEye_nBitsPerPixel (int): Number of bits per pixel. Default: Config.

    Atributes:
        name (string): Name of the camera.
        resolution (np.array): Resolution in pixels.
        pixel_size (np.array): Pizel size (meters).
        dynamic_range (int): Bit depth.
        M (float): Magnification.
        wavelength (float): Wavelength (in order to interact with Diffractio).
        _object (variable): Camera object. Its class depends on which device is being used.
        live (bool): Stores if Live was started.
        wait_time (float): Default wait time before taking a frame.
        wait_time_param (float): Default wait time after changing a property like exposure.

    ImagingSource atributes:
        resolution_string (str): Name of the resolution name.

    Supported devices:
        * ImagingSource (CAM07) and ImagingSource2 (CAM08)
        * DCC1645C (Cedida por el CLUR).
        * uEye (inicial polarimetro).
    """

    def __init__(self,
                 name="ImagingSource",
                 M=1,
                 wavelength=None,
                 uEye_HIDS=None,
                 uEye_nBitsPerPixel=None):
        """Initialize the object."""
        self.name = name

        if self.name == "ImagingSource":
            self._object = IC.TIS_CAM()
            self.resolution = CONF_IMAGING_SOURCE["resolution"]
            self.resolution_string = CONF_IMAGING_SOURCE["resolution_string"]
            self.dynamic_range = CONF_IMAGING_SOURCE["dynamic_range"]
            self.pixel_size = CONF_IMAGING_SOURCE["pixel_size"]
            self.wavelength = wavelength or CONF_IMAGING_SOURCE["wavelength"]
            self.wait_time = CONF_IMAGING_SOURCE["wait_time"]
            self.wait_time_param = CONF_IMAGING_SOURCE["wait_time_param"]

        elif self.name == "ImagingSource2":
            self._object = IC.TIS_CAM()
            self.resolution = CONF_IMAGING_SOURCE_2["resolution"]
            self.resolution_string = CONF_IMAGING_SOURCE_2["resolution_string"]
            self.dynamic_range = CONF_IMAGING_SOURCE_2["dynamic_range"]
            self.pixel_size = CONF_IMAGING_SOURCE_2["pixel_size"]
            self.wavelength = wavelength or CONF_IMAGING_SOURCE_2["wavelength"]
            self.wait_time = CONF_IMAGING_SOURCE_2["wait_time"]
            self.wait_time_param = CONF_IMAGING_SOURCE_2["wait_time_param"]

        elif self.name == "DCC1645C":
            try:
                self._object = instrument(CONF_DC1645C["name"])
            except:
                paramsets = self.List_Devices()
                for par in paramsets:
                    for val in par.values():
                        if val == 'UC480_Camera':
                            self._object = instrument(par)
                            self._object.save_instrument(CONF_DC1645C["name"])
                            break
            self.resolution = CONF_DC1645C["resolution"]
            self.dynamic_range = CONF_DC1645C["dynamic_range"]
            self.pixel_size = wavelength or CONF_DC1645C["pixel_size"]
            self.wait_time = CONF_DC1645C["wait_time"]
            self.wait_time_param = CONF_DC1645C["wait_time_param"]

        elif self.name == "uEye":
            self._object = uEye(
                uEye_HIDS or CONF_UEYE["HIDS"], uEye_nBitsPerPixel
                or CONF_UEYE["nBitsPerPixel"])
            self.resolution = CONF_UEYE["resolution"]
            self.dynamic_range = CONF_UEYE["dynamic_range"]
            self.pixel_size = wavelength or CONF_UEYE["pixel_size"]
            self.wait_time = CONF_UEYE["wait_time"]
            self.wait_time_param = CONF_UEYE["wait_time_param"]

        elif self.name == "uEye_SLM":
            self._object = uEye(
                uEye_HIDS or CONF_UEYE_SLM["HIDS"], uEye_nBitsPerPixel
                or CONF_UEYE_SLM["nBitsPerPixel"])
            self.resolution = CONF_UEYE_SLM["resolution"]
            self.dynamic_range = CONF_UEYE_SLM["dynamic_range"]
            self.pixel_size = wavelength or CONF_UEYE_SLM["pixel_size"]
            self.wait_time = CONF_UEYE["wait_time"]
            self.wait_time_param = CONF_UEYE["wait_time_param"]

        elif self.name == "uEye_LCD":
            self._object = uEye(
                uEye_HIDS or CONF_UEYE_LCD["HIDS"], uEye_nBitsPerPixel
                or CONF_UEYE_LCD["nBitsPerPixel"])
            self.resolution = CONF_UEYE_LCD["resolution"]
            self.dynamic_range = CONF_UEYE_LCD["dynamic_range"]
            self.pixel_size = wavelength or CONF_UEYE_LCD["pixel_size"]
            self.wait_time = CONF_UEYE["wait_time"]
            self.wait_time_param = CONF_UEYE["wait_time_param"]

        else:
            raise ValueError('{} is not a valid camera name.'.format(name))

        self.M = M  # Camera is going to be the reference of space, so M=1.
        self.live = False
        self.live_window = False
        self.ROI = None
        self.x, self.y = self.Get_Diffractio_Space()
        self.Clear_Background()

    def __enter__(self):
        pass

    def __exit__(self):
        """Delete object last function.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        if self.name in ("ImagingSource", "ImagingSource2"):
            self.Stop_Live()

    def Get_Diffractio_Space(self):
        """Calculates diffractio x and y variables for diffractio.

        Returns:
            x, y (np.ndarray): Arrays of space.
        """
        lim = self.pixel_size[0] * mm * (self.resolution[0] - 1) / 2
        x = np.linspace(-lim, lim, self.resolution[0])
        lim = self.pixel_size[1] * mm * (self.resolution[1] - 1) / 2
        y = np.linspace(-lim, lim, self.resolution[1])
        return x, y

    def Open(self):
        """This method initiates the camera.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        old_object = self._object
        try:
            if self.name == "ImagingSource":
                self._object.open(CONF_IMAGING_SOURCE["name"])

            if self.name == "ImagingSource2":
                self._object.open(CONF_IMAGING_SOURCE_2["name"])

            elif self.name == "DCC1645C":
                self._object.open()
                self._object.auto_framerate = True

            elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
                self._object.open(self.resolution)
        except:
            pass

    def Close(self):
        """This method closes the camera.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        if self.name in ("ImagingSource", "ImagingSource2", "ImagingSource3"):
            self.Stop_Live()  # This is just enough

        elif self.name == "DCC1645C":
            self._object.close()

        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            self._object.close()

    def List_Devices(self, verbose=False):
        """This method lists all devices.

        Args:
            verbose (bool): If True, the list is printed. Default: False.

        Output:
            list_devices (list): List of devices.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
        """
        if self.name in ("ImagingSource", "ImagingSource2", "ImagingSource3"):
            list_devices = self._object.GetDevices()
            if verbose:
                for ind, value in enumerate(list_devices):
                    print(str(ind) + " : " + str(value))

        elif self.name == "DCC1645C":
            list_devices = list_instruments(module='cameras')

        else:
            print(
                "Method List_Devices is not available for {} cameras.".format(
                    self.name))
            return None

        return list_devices

    def Set_Property(self, name, value, is_switcher=False, wait_time="default", stop_live=True):
        """This method sets the values of all properties.

        Args:
            name (string): Name of the property.
            value (variable): Value to set the property.
            is_switcher (bool): If True, the property is a switcher. Maintained for retrocompatibility. Default: False.
            wait_time (number or string): If it is a number, the system waits after implementing the change to let the camera to apply the new configuration. If not, the default time is used. Default: Default.
            stop_live (bool): If True and the camera is live, stops it for changing the property. Default: True.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        # Old is_switcher is used to change to the new method Set_Property_Auto
        if is_switcher:
            print(
                "The use of is_switcher is depercated. Please use method Set_Property_Auto instead."
            )
            self.Set_Property_Auto(name, value)
        # If a window is opened, destroy it for safety
        return_to_live = False
        if stop_live and self.live_window:
            self.Stop_Live()
            return_to_live = True
        # Get default time
        if not isinstance(wait_time, (float, int)):
            wait_time = self.wait_time_param

        if self.name in ("ImagingSource", "ImagingSource2"):
            if name.lower() == "resolution":
                if value in CONF_IMAGING_SOURCE["resolutions"]:
                    self._object.SetVideoFormat(value)
                    self.resolution = CONF_IMAGING_SOURCE["resolutions"][value]
                    self.resolution_string = value
            elif name.lower() == "framerate":
                self._object.SetFrameRate(value or 5)
            # TODO: Descomentar cuando acabe Optica Digital
            # elif name.lower() == "exposure":
            #     self._object.SetPropertyAbsoluteValue("Exposure", "Value", value)
            else:
                self._object.SetPropertyValue(name, "Value", value)

        elif self.name == "DCC1645C":
            if name.lower() == 'gamma':
                self._object.gamma = value
            elif name.lower() == 'gain':
                self._object.master_gain = value
            elif name.lower() == 'exposure':
                self._object._set_exposure(value * u.ms)
            elif name.lower() == 'framerate':
                self._object._dev.SetFrameRate(value)
                # self._object.start_live_video(framerate = Q_(value, "Hz"))
                # self._object.stop_live_video()
            elif name.lower() == "resolution":
                self.Set_ROI(width=value[0], height=value[1])
            else:
                print("Property {} is not defined for camera {}".format(
                    name, self.name))
                return

        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            if name.lower() == "framerate":
                self._object.set_fps(value)
            elif name.lower() == "exposure":
                self._object.set_exposure(value)
            elif name.lower() == "pixelclock":
                self._object.set_pixelclock(value)
            elif name.lower() == "resolution":
                self.Set_ROI(width=value[0], height=value[1])
            elif name.lower() == "gain":
                self._object.set_gain(value)
            elif name.lower() in ("lineargain", "gainlinear"):
                self._object.set_gain_linear(value)
            else:
                print("Property {} is not defined for camera {}".format(
                    name, self.name))
                return

        if name.lower() in clear_background_properties:
            self.Clear_Background(verbose=True)

        time.sleep(wait_time)

        if return_to_live:
            self.Start_Live(view=True)

    def Set_Property_Auto(self, name, value, wait_time="default"):
        """Function to change if one property is auto handled by the camera or not.

        Args:
            name (string): Name of the property.
            value (bool or number): Value to set the property.
            wait_time (number or string): If it is a number, the system waits after implementing the change to let the camera to apply the new configuration. If not, the default time is used. Default: Default.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        # Change values to correct ones
        if isinstance(value, bool) and value:
            value = 1
        elif isinstance(value, bool):
            value = 0
        elif value != 0:
            value = 1
        # Get default time
        if not isinstance(wait_time, (float, int)):
            wait_time = self.wait_time_param

        # Set property
        if self.name in ("ImagingSource", "ImagingSource2"):
            self._object.SetPropertySwitch(name, "Auto", value or 0)

        elif self.name == "DCC1645C":
            if name.lower() == 'exposure':
                self._object._dev.SetFrameRate(value)
            elif name.lower() == 'blacklevel':
                self._object.auto_blacklevel(value)
            else:
                print(
                    "Property {} cannot be automatically handled by {}".format(
                        name, self.name))
                return

        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            if name.lower() == "gain":
                self._object.set_gain_auto(value)
            elif name.lower() == "exposure":
                self._object.set_exposure_auto(value)
            else:
                print(
                    "Property {} cannot be automatically handled by {}".format(
                        name, self.name))
                return

        if name.lower() in clear_background_properties:
            self.Clear_Background(verbose=True)

    def Get_Property(self, name, verbose=True, is_switcher=False):
        """This method sets the values of all properties.

        Args:
            name (string): Name of the property.
            verbose (bool): If True, the current position is printed. Default: True.
            is_switcher (bool): If True, the property is a switcher. Maintained for retrocompatibility. Default: False.

        Output:
            result (variable): The property value.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        # Old is_switcher is used to change to the new method Set_Property_Auto
        if is_switcher:
            print(
                "The use of is_switcher is depercated. Please use method Set_Property_Auto instead."
            )
            self.Get_Property_Auto(name, verbose)
        # Get the property
        if self.name in ("ImagingSource", "ImagingSource2"):
            if name.lower() == "resolution":
                result = "Y800 ({}x{})".format(
                    self._object.get_video_format_width(),
                    self._object.get_video_format_height())
            # TODO: Descomentar cuando acabe Optica Digital
            # elif name.lower() == "exposure":
            #     result = self._object.GetPropertyAbsoluteValue("Exposure", "Value", value)
            else:
                result = self._object.GetPropertyValue(name, "Value")

        elif self.name == "DCC1645C":
            if name.lower() == 'gamma':
                result = self._object.gamma
            elif name.lower() == 'gain':
                result = self._object.master_gain
            elif name.lower() == 'exposure':
                result = self._object._get_exposure()
            elif name.lower() == 'resolution':
                w = self._object.width
                h = self._object.height
                result = np.array([w, h])
            elif name.lower() == 'framerate':
                result = self._object.framerate
            else:
                print("Property {} is not defined for camera {}".format(
                    name, self.name))
                return
        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            if name.lower() == "framerate":
                result = self._object.get_fps()
            elif name.lower() == "exposure":
                result = self._object.get_exposure()
            elif name.lower() == "pixelclock":
                result = self._object.get_pixelclock()
            elif name.lower() == "gain":
                result = self._object.get_gain()
            elif name.lower() == "resolution":
                result = self.resolution
            else:
                print("Property {} is not defined for camera {}".format(
                    name, self.name))
                return

        if verbose:
            print("The value of the property {} is {}.".format(name, result))
        return result

    def Get_Property_Auto(self, name, verbose=True):
        """This method sets the values of all properties.

        Args:
            name (string): Name of the property.
            verbose (bool): If True, the current position is printed. Default: True.

        Output:
            result (bool): The property value.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        if self.name in ("ImagingSource", "ImagingSource2"):
            result = self._object.GetPropertySwitch(name, "Auto")

        elif self.name == "DCC1645C":
            if name.lower() == 'gain':
                result = self._object.auto_gain
            elif name.lower() == 'exposure':
                result = self._object.auto_exposure
            elif name.lower() == 'framerate':
                result = self._object.auto_framerate
            else:
                print("Property {} is not defined for camera {}".format(
                    name, self.name))
                return

        else:
            print("Method Get_Property_Auto is not available for {} cameras.".
                  format(self.name))
            return None

        if verbose:
            print("The value of the property {} is {}.".format(name, result))
        return result

    def Start_Live(self, mode=None, normalize = False, view=True, colormap = "gray"):
        """This method starts measuring with the camera.

        Args:
            mode (int): If 1, the video is shown. If 0, the video is not shown. Default: 1. Depercated.
            view (bool): If True, the video is shown in an external window. Default: True.
            colormap (string): CV2 colormap to use in the video. Other options are "gray" (black and white) and "original" (original RGB used). Default: 'gray'.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        if mode is not None:
            print(
                'WARNING: The use of mode is depercated. Please use the view argument instead.'
            )
            view = mode != 0
        colormap = colormap.lower()

        def show_in_window():
            """Inner function to call in a different thread."""
            # Common variables
            key = 0
            self.live_window = True
            # Draw the figure each frame
            while self.live and key != 27:
                try:
                    # Get image
                    im = self.Get_Image(wait_time=0,
                                        is_background=False,
                                        rest_background=False,
                                        diffractio=False,
                                        stadistics=False,
                                        normalize = normalize,
                                        monochannel = colormap == "original",
                                        draw=False).astype(np.uint8)
                    # Create the window
                    cv2.namedWindow(self.name, cv2.WINDOW_NORMAL)
                    # Resize the image to preserv aspect ratio
                    win = cv2.getWindowImageRect(self.name)
                    ar_image = im.shape[0] / im.shape[1]
                    ar_window = win[3] / win[2]
                    if ar_window > ar_image:
                        # Width is limiting
                        width = win[2]
                        height = int(width * ar_image)
                    else:
                        # Height is limiting
                        height = win[3]
                        width = int(height / ar_image)
                    cv2.resizeWindow(self.name, width, height)
                    # print(win[2], "   ->    ", width)
                    # print(win[3], "   ->    ", height)
                    # Print image
                    if colormap == "grey":
                        pass
                    elif colormap in cv2_colormap_names.keys():
                        im = cv2.applyColorMap(im, cv2_colormap_names[colormap])
                    cv2.imshow(self.name, im)
                    key = cv2.waitKey(int(wait_time))
                # When a problem arises, destroy the window
                except:
                    cv2.destroyWindow(self.name)
                    self.live_window = False
            # When aborted, destroy the window
            try:
                cv2.destroyWindow(self.name)
                self.Stop_Live()
            except:
                pass

        # Set live property on
        self.live = True

        # Start live
        if self.name in ("ImagingSource", "ImagingSource2"):
            self._object.StartLive(0)
            #print(CONF_IMAGING_SOURCE["framerates"][self.resolution_string])
            wait_time = 1000 / \
                CONF_IMAGING_SOURCE["framerates"][self.resolution_string]

        elif self.name == "DCC1645C":
            self._object.start_live_video(framerate = self._object.framerate, exposure = self._object._get_exposure(),exposure_time = self._object._get_exposure(), gain =  self._object.master_gain)
            # self._object.start_live_video()
            wait_time = 1000 / CONF_DC1645C["framerate"]

        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            self._object.start_live()
            wait_time = 1000 / CONF_UEYE["max_fps"]

        # Open window
        if view:
            Thread(target=show_in_window).start()

    def Stop_Live(self):
        """This method stops measuring with the camera. This enables using external programs to control it.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """

        self.live = False
        self.live_window = False

        if self.name in ("ImagingSource", "ImagingSource2"):
            self._object.StopLive()

        elif self.name == "DCC1645C":
            time.sleep(0.5)
            self._object.stop_live_video()

        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            self._object.stop_live()

    def Get_Image(self,
                  wait_time='default',
                  monochannel=True,
                  is_background=False,
                  rest_background=True,
                  diffractio=False,
                  stadistics=False,
                  binning = (1,1),
                  normalize = False,
                  draw=False):
        """Extract a photogram as a numpy array.

        Args:
            wait_time (None, float or 'auto'): If float, the program will wait that time before snapping the image. If 'auto', the time will be the default settings of each camera. Default: 'auto'.
            monochannel (bool): If True and three RGB channels are acquired, it transforms it into a black and white monochannel image. Default: True.
            is_background (bool): If True, stores the current image as background. Default: False.
            rest_background (bool): If True and is_background is False, the stored background is substracted from the measured image. Default: True. WARNING: If some properties as gain, contrast or acquisition time are varied between the measurement of the background and the execution of this function, the result will be incorrect.
            diffractio (bool): If True, the output variable is a Diffractio Scalar_Field_XY. Default: False.
            stadistics (bool): If True, some stadistics of the image are printed. Default: False.
            binning (array): Binning the image. Default: (1,1).
            normalize (bool): If True, the image is normalized to 256. Default: False.
            draw (bool): If True, plot the image. Default: False.

        Output:
            result (np.array): Measured photogram.

        Supported devices:
            * ImagingSource (CAM07 and CAM08).
            * DCC1645C.
            * uEye.
        """
        # Start live if it wasn't
        stop_live = False
        if not self.live:
            self.Start_Live(view=False)
            stop_live = True

        if wait_time is not None:
            if isinstance(wait_time, str):
                wait_time = self.wait_time
            time.sleep(wait_time)

        if self.name in ("ImagingSource", "ImagingSource2"):
            self._object.SnapImage()
            time.sleep(wait_time or self.wait_time)
            result = self._object.GetImage()

        elif self.name == "DCC1645C":
            result = self._object.grab_image(exposure_time = self._object._get_exposure(), gain =  self._object.master_gain)

        elif self.name in ("uEye", "uEye_SLM", "uEye_LCD"):
            result = self._object.get_image()

        if is_background:
            self.background = np.array(result, dtype=float)
        elif rest_background and (self.background is not None):
            if self.background.shape != result.shape:
                raise ValueError(
                    "Resolution of image ({}) and background ({}) are different"
                    .format(result.shape, self.background.shape))
            else:
                result = np.array(result, dtype=float)
                result = result - self.background
                if result.min() < 0:
                    #result = result - result.min()
                    result[result < 0] = 0
                result = np.array(result, dtype=int)

        if result.ndim == 3 and monochannel:
            result = utils.Color_to_BW(result)

        if normalize: 
            result = 256 * result / result.max()

        if self.ROI is not None and self.name not in ("uEye", "uEye_SLM"):
            x = self.ROI["x"]
            y = self.ROI["y"]
            width = self.ROI["width"]
            height = self.ROI["height"]
            if result.ndim == 2:
                result = result[y:y + height, x:x + width]
            else:
                result = result[y:y + height, x:x + width, :]

        if stadistics:
            print(
                "Staistics of the image:\n  - Max:  {}.\n  - Min:  {}.\n  - Mean: {:.1f}\n  - Std:  {:.1f}"
                .format(result.max(), result.min(), result.mean(),
                        result.std()))

        if draw:
            limit = self.resolution * self.pixel_size
            extent = np.array(
                [-limit[0] / 2, limit[0] / 2, -limit[1] / 2, limit[1] / 2])
            plt.figure()
            plt.imshow(result, extent=extent, cmap=cm.gray)
            plt.colorbar(orientation='horizontal')

        if diffractio:
            x, y, w = utils.Get_Diffractio_Space(self)
            field = Scalar_field_XY(x=x, y=y, wavelength=w)
            result = np.array(result, dtype=float)
            field.u = np.sqrt(result)
            result = field

        # If live wasn't active before measuring, start it now
        if stop_live:
            self.Stop_Live()

        # If binning: 
        # if result.shape[0] % binning[0] != 0: 
        #     print("WARNING: in X dimension, binning is not divisible")
        # elif result.shape[1] % binning[1] != 0:
        #     print("WARNING: in Y dimension, binning is not divisible")
        # else:
        #     result = result.reshape( result.shape[0]//binning[0], binning[0], result.shape[1]//binning[1], binning[1]).mean(3).mean(1)

        return result

    def Save_Image(self,
                   file_name=None,
                   path=None,
                   wait_time='auto',
                   monochannel=True,
                   is_background=False,
                   rest_background=True,
                   stadistics=False,
                   draw=False,
                   draw_external=False,
                   return_array=False):
        """Extract a photogram as a numpy array and save it.

        Args:
            file_name (string): Name of the file without extension. Default uses date and time to create it.
            path (string): If not None, location to save the image. Default: None.
            wait_time (None, float or 'auto'): If float, the program will wait that time before snapping the image. If 'auto', the time will be the default settings of each camera. Default: 'auto'.
            monochannel (bool): If True and three RGB channels are acquired, it transforms it into a black and white monochannel image. Default: True.
            is_background (bool): If True, stores the current image as background. Default: False.
            rest_background (bool): If True and is_background is False, the stored background is substracted from the measured image. Default: True. WARNING: If some properties as gain, contrast or acquisition time are varied between the measurement of the background and the execution of this function, the result will be incorrect.
            stadistics (bool): If True, some stadistics of the image are printed. Default: False.
            draw (bool): If True, plot the image. Default: False.
            draw_external (bool): If True, plot the image in an external image viewer. Default: False.
            return_array (bool): If True, returns the array. Default: False.

        Output:
            image (np.array): Measured photogram.

        Supported devices:
            * All.
        """
        # Go to desired location
        if path is not None:
            cwd = os.getcwd()
            os.chdir(path)

        # Autofilename
        if file_name is None:
            now = datetime.now()
            file_name = now.strftime("Image %Y_%m_%d_%H_%M_%S.bmp")

        # Get the image
        image = self.Get_Image(wait_time=wait_time,
                               is_background=is_background,
                               rest_background=rest_background,
                               monochannel=monochannel,
                               stadistics=stadistics,
                               draw=draw)
        image = np.array(image, dtype=int)

        # Save it
        if draw_external:
            im = Image.fromarray(image)
            im.show()

        plt.figure()
        plt.imsave(file_name, image, cmap='gray', dpi=300, origin='lower')
        plt.close()
        if path is not None:
            os.chdir(cwd)
        if return_array:
            return image

    def Set_ROI(self, width=None, height=None, x=None, y=None):
        """Set a Region Of Interest. The camera will measure only the ROI, droping the rest of the image.

        Args (if None, they are not updated):
            width (int or None): Width of the ROI.
            height (int or None): Height of the ROI.
            x (int or None): Starting left pixel of the ROI.
            y (int or None): Starting upper pixel of the ROI.
        """
        # Default values
        res = self.Get_Property("resolution")
        if self.ROI:
            x = int(x) if x is not None else self.ROI["x"]
            y = int(y) if y is not None else self.ROI["y"]
            width = int(width) if width is not None else self.ROI["width"] - x
            height = int(
                height) if height is not None else self.ROI["height"] - y
        else:
            x = int(x) if x is not None else 0
            y = int(y) if y is not None else 0
            width = int(width) if width is not None else res[0] - x
            height = int(height) if height is not None else res[1] - y

        # Safety
        if width > res[0]:
            print(
                "ROI width {} higher than resolution ({}). Changed to maximum."
                .format(width, res[0]))
            width = res[0]
        if x + width > res[0]:
            print(
                "ROI x ({}) plus width ({}) higher than resolution ({}). x changed to maximum."
                .format(x, width, res[0]))
            x = res[0] - width
        if height > res[1]:
            print(
                "ROI height {} higher than resolution ({}). Changed to maximum."
                .format(height, res[1]))
            height = res[1]
        if y + height > res[1]:
            print(
                "ROI y ({}) plus height ({}) higher than resolution ({}). x changed to maximum."
                .format(y, height, res[1]))
            y = res[1] - height

        # Cameras that set it internally
        if self.name in ("uEye", "uEye_SLM"):
            x = int(x) if x else 0
            y = int(y) if y else 0
            self._object.set_aoi(width, height, x, y)

        # For the rest we have to do it externally
        self.ROI = {}
        self.ROI["width"] = width
        self.ROI["height"] = height
        self.ROI["x"] = x
        self.ROI["y"] = y

    def Get_ROI(self, verbose=False):
        """Get the Region Of Interest.

        Args:
            verbose (bool): If True, prints the result. Default: False.

        Returns:
            width, height, x, y (ints): Width, height and bottom-left corner of the ROI.
        """
        # Get values
        if self.ROI is None:
            width = self.resolution[0]
            height = self.resolution[1]
            x = 0
            y = 0
        else:
            width = self.ROI["width"]
            height = self.ROI["height"]
            x = self.ROI["x"]
            y = self.ROI["y"]

        # print
        if verbose:
            print("Region Of Interest (ROI):")
            print(
                " - Width  = {}\n - Height = {}\n - x      = {}\n - y      = {}"
                .format(width, height, x, y))
        return width, height, x, y

    def Clear_ROI(self):
        """Remoes the Region Of Interest, so the whole image will be used."""
        if self.name in ("uEye", "uEye_SLM"):
            self.Set_ROI(width=self.resolution[0], height=self.resolution[1])
        self.ROI = None

    def Set_Background(self,
                       file_name=None,
                       path=None,
                       wait_time='auto',
                       monochannel=True,
                       stadistics=False,
                       draw=False,
                       draw_external=False,
                       return_array=False):
        """Measure the background. This is a wrapper of Get_Image with the parameter is_background set to True.

        Args:
            file_name (string): Name of the file without extension. Default uses date and time to create it.
            path (string): If not None, location to save the image. Default: None.
            wait_time (None, float or 'auto'): If float, the program will wait that time before snapping the image. If 'auto', the time will be the default settings of each camera. Default: 'auto'.
            monochannel (bool): If True and three RGB channels are acquired, it transforms it into a black and white monochannel image. Default: True.
            stadistics (bool): If True, some stadistics of the image are printed. Default: False.
            draw (bool): If True, plot the image. Default: False.
            draw_external (bool): If True, plot the image in an external image viewer. Default: False.
            return_array (bool): If True, returns the array. Default: False.

        Output:
            result (np.array): Measured photogram.

        Supported devices:
            * All.
        """
        return self.Get_Image(file_name=file_name,
                              path=path,
                              wait_time=wait_time,
                              monochannel=monochannel,
                              stadistics=stadistics,
                              draw=draw,
                              draw_external=draw_external,
                              return_array=return_array)

    def Clear_Background(self, verbose=True):
        """Clears the background.

        Args:
            verbose (bool): If True, a message saying that the background has been removed is printed. Default: False.

        Supported devices:
            * All."""
        self.background = None
        if verbose:
            print("Background cleared")

    def Get_Background(self, monochannel=True, draw=False):
        """Extract a photogram as a numpy array.

        Args:
            monochannel (bool): If True and three RGB channels are acquired, it transforms it into a black and white monochannel image. Default: True.
            draw (bool): If True, plot the image. Default: False.

        Output:
            result (np.array): Measured background.

        Supported devices:
            * All.
        """
        result = self.background
        if result is None:
            print("No background acquired.")
            return

        if result.ndim == 3 and monochannel:
            result = utils.Color_to_BW(result)

        if draw:
            limit = self.resolution * self.pixel_size
            extent = np.array(
                [-limit[0] / 2, limit[0] / 2, -limit[1] / 2, limit[1] / 2])
            plt.figure()
            plt.imshow(result, extent=extent, cmap=cm.gray)
            plt.colorbar()

        return result
