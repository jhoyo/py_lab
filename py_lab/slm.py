import screeninfo
import os
import cv2
import numpy as np
from threading import Thread
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from py_lab.config import CONF_HOLOEYE2500, CONF_PLUTO, CONF_PLUTO2, CONF_KOPIN
from py_lab.drivers.slms.kopin.kopin import Kopin

import py_lab.utils as utils

from diffractio import mm
from diffractio.scalar_masks_XY import Scalar_mask_XY
from diffractio.scalar_fields_XY import Scalar_field_XY
from diffractio.scalar_sources_XY import Scalar_source_XY

TYPES_DIFFRACTIO = (Scalar_mask_XY, Scalar_field_XY, Scalar_source_XY)

um = 1e3


class SLM(object):
    """General class for SLMs.

    Args:
        name (string): Name of the SLM.
        M (float): Optical system magnification. Default: 1.
        wavelength (float): Wavelength used in the experiment. Default: 632.8 nm.

    Atributes:
        name (string): Name of the SLM.
        resolution (np.array): Resolution in pixels.
        pixel_size (np.array): Pizel size (meters).
        M (float): Optical system magnification.
        ID_screen (int): ID of the screen used by the SLM. Default: Config file.
        wait_time (float): Waiting time after sending an image to the SLM. Default: Config file.
        calibration (tuple): A tuple containing the amplitude and phase transmissions in amplitude and phase modulation configurations (see Load_Calibration).
        _images (dict): Dictionary with the used image names and values.
        bw (bool): If True converts RGB images to black and white. Default HoloEye2500: True. Default HoloEyePluto: False

    Supported devices:
        * HoloEye2500.
        * HoloEyePluto.
        * Kopin

    """

    def __init__(self, name="HoloEye2500", M=1, wavelength=632.8e-6):
        """Initialize the object."""
        self.name = name

        if name == "HoloEye2500":
            self._object = None
            self.M = M
            self.resolution = CONF_HOLOEYE2500["resolution"]
            self.pixel_size = CONF_HOLOEYE2500['pixel_size']
            self.dynamic_range = CONF_HOLOEYE2500['dynamic_range']
            self.wait_time = int(1000 / CONF_HOLOEYE2500['framerate'])
            self.ID_screen = CONF_HOLOEYE2500['ID_screen']
            self.bw = True

            self = utils.Calculate_Space(self)

            # TODO:
            if CONF_HOLOEYE2500['callibration_table']:
                pass

        elif name == "HoloEyePluto":
            self._object = None
            self.M = M
            self.resolution = CONF_PLUTO["resolution"]
            self.pixel_size = CONF_PLUTO['pixel_size']
            self.dynamic_range = CONF_PLUTO['dynamic_range']
            self.wait_time = int(1000 / CONF_PLUTO['framerate'])
            self.ID_screen = CONF_PLUTO['ID_screen']
            self.bw = False

        elif name == "HoloEyePluto2":
            self._object = None
            self.M = M
            self.resolution = CONF_PLUTO2["resolution"]
            self.pixel_size = CONF_PLUTO2['pixel_size']
            self.dynamic_range = CONF_PLUTO2['dynamic_range']
            self.wait_time = int(1000 / CONF_PLUTO2['framerate'])
            self.ID_screen = CONF_PLUTO2['ID_screen']
            self.bw = False
            
        elif name == "Kopin":
            self._object = Kopin(wavelength=wavelength)
            self.M = M
            self.resolution = CONF_KOPIN["resolution"]
            self.pixel_size = CONF_KOPIN['pixel_size']
            self.dynamic_range = CONF_KOPIN['dynamic_range']
            self.wait_time = int(1000 / CONF_KOPIN['framerate'])
            self.ID_screen = CONF_KOPIN['ID_screen']
            self.bw = False
            self._object.led_on()

            
        else:
            raise ValueError('{} is not a valid SLM name.'.format(name))

        # Common
        self.wavelength = wavelength * um
        self._images = {}
        self._window_ind = 0
        self = utils.Calculate_Space(self)
        self.x, self.y = self.Get_Diffractio_Space()

        # Check second screen
        monitors = screeninfo.get_monitors()
        condX = monitors[self.ID_screen].width == self.resolution[0]
        condY = monitors[self.ID_screen].height == self.resolution[1]
        #if not condX * condY:
        #print("WARNING: SLM screen has different resolution ({}x{}) than SLM ({}x{}).".format(monitors[self.ID_screen].width, monitors[self.ID_screen].height, self.resolution[0], self.resolution[1]))

        # Load calibration
        # self.Load_Calibration()

    def __del__(self):
        """Delete object last function.

        Supported devices:
            * HoloEye2500 *HoloEyePluto.
        """
        if self.name in ("HoloEye2500", "HoloEyePluto", "HoloEyePluto2"):
            self.Close_All_Images()

    def Get_Diffractio_Space(self):
        """Calculates diffractio x and y variables for diffractio.

        Returns:
            x, y (np.ndarray): Arrays of space.
        """
        lim = self.pixel_size[0] * mm * (self.resolution[0] - 1) / 2
        x = np.linspace(-lim, lim, self.resolution[0])
        lim = self.pixel_size[1] * mm * (self.resolution[1] - 1) / 2
        y = np.linspace(-lim, lim, self.resolution[1])
        return x, y

    def Send_Image(self,
                   image,
                   image2=None,
                   path=None,
                   var_name=None,
                   kind='phase',
                   norm=None,
                   draw=False,
                   return_array=False,
                   window_name='SLM'):
        """Send image to SLM. Gray level of the image will be directly translated to gray level of SLM pixels.

        Args:
            image (string, np.array or Scalar_mask_XY): Image to send to the SLM. If string, it is supposed to be a file in an image format (PNG, JPEG, etc.) or a npz file saved with np.savez(). If Scalar_mask_XY, only the amplitude, intensity or phase can be sent to the SLM.
            image2 (np.array or Scalar_mask_XY): Imagen to send to the red/blue channel of HoloEyePluto SLM. If Scalar_mask_XY, only the amplitude, intensity or phase can be sent to the SLM.
            path (string): If IMAGE is string, this is the path of the image. If None, the image must be located in the current working directory. Default: None.
            var_name (str): If IMAGE is the name of a npz file, var is the name of the variable where the image is stored. If None, the first variable is picked. Default: None.
            kind (string): If IMAGE is Scalar_mask_XY, selects the information sent to the SLM. There are three options: 'phase', 'amplitude' or 'intensity'. Default: 'phase'.
            norm (float): If not None, this value will correspond to the maximum gray level. Default: None.
            draw (bool): If True, the array sent to the SLM is drawn.
            return_array (bool): If True, returns the sent array. Default: False.
            window_name (str): Name of the new window. Default: 'SLM'.

        Returns:
            im (np.ndarray): Array sent to the SLM (only if return_array is True).

        Supported devices:
            * HoloEye2500 * HoloEyePluto.
        """
        window_name += str(self._window_ind)

        def show_in_window():
            """Inner function to call in a different thread."""
            screen = screeninfo.get_monitors()[self.ID_screen]
            key = 0

            while window_name in self._images and key != 27:
                try:
                    cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)
                    cv2.moveWindow(window_name, screen.x - 1, screen.y - 1)
                    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                                          cv2.WINDOW_FULLSCREEN)
                    cv2.imshow(window_name, self._images[window_name])
                    key = cv2.waitKey(int(1))
                except:
                    self.Close_Image(window_name)
                    cv2.destroyWindow(window_name)

            try:
                self.Close_Image(window_name)
                cv2.destroyWindow(window_name)
            except:
                pass

        # Load the image as a numpy array
        if self.name in ("HoloEye2500", 'Kopin'):
            im = utils.Get_Image(self,
                                 image,
                                 path=path,
                                 var_name=var_name,
                                 kind=kind,
                                 norm=norm,
                                 bw=self.bw)

        elif self.name in ("HoloEyePluto", "HoloEyePluto2"):
            data = np.zeros((self.resolution[1], self.resolution[0], 3))

            if isinstance(image, TYPES_DIFFRACTIO):
                data[:, :, 1] = image.u

            elif isinstance(image, np.ndarray):
                data[:, :, 1] = image

            # Si queremos utilizar los dos SLMs Pluto, enviamos otra imagen por el canal rojo/azul.
            if image2 != None:
                if isinstance(image2, TYPES_DIFFRACTIO):
                    data[:, :, 0] = image2.u
                    data[:, :, 2] = image2.u

                elif isinstance(image2, np.ndarray):
                    data[:, :, 0] = image2
                    data[:, :, 2] = image2

            #filename = 'imagen.bmp'
            #cv2.imwrite('imagen.bmp', data)
            #plt.imshow(data[:,:,1])
            #plt.colorbar()
            #plt.show()
            
            im = utils.Get_Image(self,
                                 image=image,
                                 path=path,
                                 var_name=var_name,
                                 kind=kind,
                                 norm=norm,
                                 bw=self.bw)

            #plt.imshow(im)
            #plt.colorbar()
            #plt.show()

        # Check dimensions
        image_res = np.flip(np.array(im.shape))
        
        #if not np.all(image_res == self.resolution):
        #print(
        #'WARNING: Image resolution ({}) is different tha SLM resolution ({}).'.
        #format(image_res, self.resolution))


        # Send to SLM
        if self.name in ("HoloEye2500", "HoloEyePluto", "HoloEyePluto2"):
            # Transform to the desired units
            im = 255 * im #135 zernikes
            im = im.astype(np.uint8)

            # Draw it
            if draw:
                plt.figure(figsize=(8, 8))
                plt.imshow(im, cmap=cm.gray, extent=self.extent)
                plt.colorbar(orientation='horizontal')
                plt.xlabel('X (um)')
                plt.ylabel('Y (um)')

            # Add window name
            # if window_name in self._images:
            #     self.Close_Image(window_name=window_name)
            # self._images.append(window_name)
            if window_name not in self._images:
                self._images[window_name] = im
                Thread(target=show_in_window).start()
            else:
                self._images[window_name] = im
                
        elif self.name == 'Kopin':
            print("in send")
            im = 255 * im
            im = im.astype(np.uint8)
            self._object.mask.u = im
            self._object.send_image()

            # Draw it
            if draw:
                plt.figure(figsize=(8, 8))
                plt.imshow(im, cmap=cm.gray, extent=self.extent)
                plt.colorbar(orientation='horizontal')
                plt.xlabel('X (um)')
                plt.ylabel('Y (um)')

            # Add window name
            # if window_name in self._images:
            #     self.Close_Image(window_name=window_name)
            # self._images.append(window_name)
            if window_name not in self._images:
                self._images[window_name] = im
                Thread(target=show_in_window).start()
            else:
                self._images[window_name] = im

        if return_array:
            return im

    def Pluto_Send_Image(self, image1, image2=None):
        """Send only one image to HoloEyePluto SLM.

        Args:
            image1:  (np.array or Scalar_mask_XY) Image send to green channel of the DVI video signal splitter.
            image2:  (np.array or Scalar_mask_XY) Image send to blue/red channel of the DVI video signal splitter.
        """
        data = np.zeros((self.resolution[1], self.resolution[0], 3))

        if isinstance(image1, TYPES_DIFFRACTIO):
            data[:, :, 1] = image1.u

        elif isinstance(image1, np.ndarray):
            data[:, :, 1] = image1

        # Si queremos utilizar los dos SLMs Pluto, enviamos otra imagen por el canal rojo/azul.
        if image2 != None:
            if isinstance(image2, TYPES_DIFFRACTIO):
                data[:, :, 0] = image2.u
                data[:, :, 2] = image2.u

            elif isinstance(image2, np.ndarray):
                data[:, :, 0] = image2
                data[:, :, 2] = image2

        filename = 'imagen.bmp'
        cv2.imwrite('imagen.bmp', data)

        self.Send_Image(image='imagen.bmp',
                        kind='amplitude',
                        draw=True,
                        window_name='SLM')

    def Send_Mask(self,
                  image,
                  path=None,
                  var_name=None,
                  kind='phase',
                  norm=True,
                  draw=False,
                  return_array=False,
                  window_name='SLM'):
        """Send a mask to the SLM. Unlike Send_Image, this method will correct the image so it is a true amplitude or phase mask.

        Args:
            image (string, np.array or Scalar_mask_XY): Image to send to the SLM. If string, it is supposed to be a file in an image format (PNG, JPEG, etc.) or a npz file saved with np.savez(). If Scalar_mask_XY, only the amplitude, intensity or phase can be sent to the SLM.
            path (string): If IMAGE is string, this is the path of the image. If None, the image must be located in the current working directory. Default: None.
            var_name (str): If IMAGE is the name of a npz file, var is the name of the variable where the image is stored. If None, the first variable is picked. Default: None.
            kind (string): If IMAGE is Scalar_mask_XY, selects the information sent to the SLM. There are three options: 'phase', 'amplitude' or 'intensity'. Default: 'phase'.
            norm (bool): If True, amplitude masks are normalized to maximum values. Default: True.
            draw (bool): If True, the array sent to the SLM is drawn.
            return_array (bool): If True, returns the sent array. Default: False.
            window_name (str): Name of the new window. Default: 'SLM'.

        Returns:
            im (np.ndarray): Array sent to the SLM (only if return_array is True).

        Supported devices:
            * HoloEye2500.
        """
        print('TODO')

    def Close_Image(self, window_name='SLM'):
        """Close all images sent to SLM.

        Args:
            window_name (str): Name of the window to close. Default: 'SLM'.

        Supported devices:
            * HoloEye2500.
            * HoloEyePluto.
        """
        window_name += str(self._window_ind)
        if self.name in ("HoloEye2500", "HoloEyePluto", "HoloEyePluto2"):
            self._images.pop(window_name)
            self._window_ind += 1

    def Close_All_Images(self):
        """Close all images sent to SLM.

        Supported devices:
            * HoloEye2500 *HoloEyePluto.
        """
        # Send to SLM
        if self.name in ("HoloEye2500", "HoloEyePluto", "HoloEyePluto2"):
            cv2.destroyAllWindows()
            self._images = {}
            self._window_ind += 1

    def Load_Calibration(self,
                         filename='Found_angles.npz',
                         path='default',
                         method='hoyo_ext'):
        """Function to load a SLM calibration and store it in case it is necessary.

        Args:
            filename (str): File name, including extension. Default: 'Found_angles.npz'.
            path (string or None): If not None, the function will look for the files at this path location. If 'default', searches in py_lab/py_lab/slm/ Default: 'default'.
            method (str): Desired calibration method. Default: 'hoyo_ext'.

        Returns
            cal (tuple): 6-element tuple with the following ellements: (levels_A, amp_A, phase_A, levels_P, amp_P, phase_P), where amp and phase correspond to amplitude and phase transmission, levels correspond to the usable levels, and _A denotes amplitude modulation and _P phase modulation configurations.
        """
        # Go to path
        if path and path == 'default':
            old_path = os.getcwd()
            path = os.path.dirname(__file__) + r'\slms'
            os.chdir(path)
        elif path:
            old_path = os.getcwd()
            os.chdir(path)

        # Load the information
        loaded = np.load(filename, allow_pickle=True)
        results = np.take(loaded["results"], 0)
        amp_A = results["amp_" + method]["amplitude"]
        phase_A = results["amp_" + method]["phase"]
        amp_P = results["phase_" + method]["amplitude"]
        phase_P = results["phase_" + method]["phase"]

        # Filter some random peaks
        amp_A = utils.Filter_Curves(amp_A,
                                    method='widepeaks',
                                    max_width_peaks=(3, 3))
        phase_P = utils.Filter_Curves(phase_P,
                                      method='widepeaks',
                                      max_width_peaks=(3, 3))

        # Return to original folder
        if path:
            os.chdir(old_path)

        # Unitarize and smooth
        amp_A, levels_A = utils.Prepare_For_Interpolation(amp_A,
                                                          kind='amplitude')
        phase_A = phase_A[levels_A]

        phase_P, levels_P = utils.Prepare_For_Interpolation(phase_P,
                                                            kind='phase')
        amp_P = amp_P[levels_P]

        self.calibration = (levels_A, amp_A, phase_A, levels_P, amp_P, phase_P)

    def fix_aberrations(self,
                        mask,
                        f1=CONF_PLUTO['f1'],
                        f2=CONF_PLUTO['f2'],
                        theta=CONF_PLUTO['theta']):
        """""
        Fix aberrations of set-up B (SLM Pluto).

        Parameters:
        f1 (int): Parameter to create elliptical phase map. Default: CONF_PLUTO['f1']
        f2 (int): Parameter to create elliptical phase map. Default: CONF_PLUTO['f2']
        theta (float): Parameter to create elliptical phase map. Default: CONF_PLUTO['theta']
        mask (Scalar_mask_XY): Desired phase mask to send to SLM.

        Return:
        phase_map (Scalar_mask_XY): Mask to send to SLM.
        """ ""

        phase = Scalar_mask_XY(self.x, self.y, wavelength=0.632)
        phase.elliptical_phase(f1=f1, f2=f2, angle=theta)

        phase_map = phase * mask

        return phase_map
    

    def phase_2_gl(mod_phase,DOE,draw=False):
        """"
        Function to convert the phase of a DOE to gray levels

        Args:
        mod_phase (numpy.ndarray): Phase modulation curve
        DOE (Scalar_mask_XY or numpy nd.array): Phase of the DOE/array that we need to send to the SLM.
        draw (bool): If True, the phase modulation curve is shown. Default: False.


        Returns:
        i_positions (numpy.ndarray,numpy.ndarray): Gray level mask to send to the SLM.
        """""

        if isinstance(DOE, TYPES_DIFFRACTIO):
            phase_DOE = np.angle(DOE.u)
        
        elif isinstance(DOE,np.ndarray):
            phase_DOE = np.angle(DOE)
            

        phase_unwrap = np.unwrap(phase_DOE)
        phase_wrap = phase_unwrap % (mod_phase.max())

        resta = np.abs(np.subtract.outer(mod_phase, phase_wrap))
        gl = np.zeros((phase_wrap.shape[0],phase_wrap.shape[1]),dtype='int')

        for i in range(phase_wrap.shape[0]):
            for j in range(phase_wrap.shape[1]):
                gl[i,j] = resta[:,i,j].argmin()

        if draw:
            V = np.arange(256)
            plt.plot(V,mod_phase)
            plt.title('Curva de modulación de fase: [177.39, 178.89,  90.09, 179.15]')
            plt.xlabel('Gray level')
            plt.ylabel('Phase (rad)')

        return gl
