### SPECTRAL POLARIMETER V 2.0 ###

# Made by: Joaquín Andrés Porras
# Date: 27/01/2025
# Last modification: 27/01/2025
# Version: 2.0.0

# Description: This file contains the functions needed to calibrate and use the spectral polarimeter. 
# Classes: 
#   - SpectralPolarimeterCalibration
#   - SpectralPolarimeter
#   - SpectralPolarimeter_utils

############################################################################################################################################################################
# IMPORTS
# GENERAL MODULES

import numpy as np                                              # Numpy
import os                                                       # OS
import datetime                                                 # Datetime
import time                                                     # Time

# CALCULUS MODULES

import numpy as np                                              # Numpy
import matplotlib.pyplot as plt                                 # Matplotlib
# %matplotlib widget                                            # Widget
from scipy.optimize import least_squares                        # Least squares
from pyswarms.single.global_best import GlobalBestPSO           # PSO

# PY-LAB MODULES

import py_lab.utils as utils                                    # Utils py-lab
from py_lab.motor import Motor_Multiple
from py_lab.spectrometer import Spectrometer
from py_lab.config import degrees, CONF_DT_50, CONF_POLARIMETER, CONF_INTEL, CONF_ZABER, CONF_AVANTES
from py_lab.utils import Rotation_Poincare, PSG_angles_2_states, PSA_angles_2_states, PSG_states_2_angles, PSA_states_2_angles
from py_lab.setups.polarimeter_utils import Calculate_Mueller_Matrix_0D, Calculate_Mueller  # Mueller - Polarimeter

# MÓDULOS PY-POL

from py_pol.mueller import Mueller                              # Mueller
from py_pol.stokes import Stokes                                # Stokes

############################################################################################################################################################################

# SPECTRAL POLARIMETER CALIBRATION CLASS

class SpectralPolarimeterCalibration(object):
    
    """
    Class for calibrating the spectral polarimeter. 
    It contains the following functions:
        - Create_Cal_Files
        - Load_Cal_Files
        - Print_Calibration_Parameters
        - Save_Cal_Files
        - make_step
        - model_step
        - error_step
        - analyze_step
        - Process_Iterative_Calibration
        - Process_Simultaneous_Calibration
    """

    def Create_Cal_Files(single: bool = False, wavelengths: np.ndarray = None):
        """
        Function to generate the initial cal_dict.
        
        Args:
            single (bool): If True, individual dict; else, spectral dict. Default: False.
            wavelengths (np.ndarray): Array with the wavelengths. Default: None.

        Returns:
            cal_dict (dict): Dictionary with the calibration configuration.
        """

        # Initialize the dictionary
        cal_dict = {}

        # For single wavelength
        if single:
            if wavelengths is None:
                print("Wavelengths must be specified")
                return 
            elif type(wavelengths) in (int, float):
                cal_dict['num_wavelengths'] = 1
                cal_dict['wavelengths'] = wavelengths
            else: 
                print("Wavelengths must be a number when single is True")
                return
            
        # For multiple wavelengths
        else:  
            if wavelengths is None: 
                print("Wavelengths must be specified")
                return
            elif type(wavelengths) not in (np.ndarray, list):
                print("Wavelengths must be an array")
                return
            elif isinstance(wavelengths, list):
                    wavelengths = np.array(wavelengths)  
            if wavelengths.size == 1:
                print("Single wavelength, set single = True")
                return
            cal_dict['num_wavelengths'] = wavelengths.size
            cal_dict['wavelengths'] = wavelengths
        
        # CREATE THE DICTIONARY
        
        # General parameters
        cal_dict['Twait'] = 0.2 
        cal_dict["N_measures"] = 361 
        cal_dict["max_angle"] = 180 
        cal_dict["angles"] = np.linspace(0, cal_dict["max_angle"], cal_dict["N_measures"])
        
        # if single wavelength
        if single: 
            cal_dict["S0"] = 1 
            cal_dict["S0_error"] = 0 
            cal_dict["S0_az"] = 0 * degrees
            cal_dict["S0_el"] = 45 * degrees
            cal_dict["S0_pol_degree"] = 1 
            cal_dict["P0_az"] = 0 * degrees

            cal_dict["S1"] = 1 
            cal_dict["S1_error"] = 0
            cal_dict["S1_az"] = 0 * degrees 
            cal_dict["S1_el"] = 45 * degrees 
            cal_dict["S1_pol_degree"] = 1 

            cal_dict["P1_p1"] = 1 
            cal_dict["P1_p2"] = 0 
            cal_dict["P1_az"] = 0 * degrees 
            cal_dict["P1_Ret"] = 0 * degrees 

            cal_dict["P2_p1"] = 1 
            cal_dict["P2_p2"] = 0 
            cal_dict["P2_az"] = 0 * degrees 
            cal_dict["P2_Ret"] = 0 * degrees 

            cal_dict["Pc_p1"] = 1 
            cal_dict["Pc_p2"] = 0 

            cal_dict["R1_p1"] = 1 
            cal_dict["R1_p2"] = 1 
            cal_dict["R1_az"] = 0 * degrees 
            cal_dict["R1_Ret"] = 90 * degrees 

            cal_dict["R2_p1"] = 1 
            cal_dict["R2_p2"] = 1 
            cal_dict["R2_az"] = 0 * degrees
            cal_dict["R2_Ret"] = 90 * degrees 
            
            cal_dict["Rc_p1"] = 1 
            cal_dict["Rc_p2"] = 1 
            cal_dict["Rc_Ret"] = 90 * degrees 
            cal_dict["Rc_offset"] = 0 * degrees 
        
        # if multiple wavelengths 
        else: 

            cal_dict["S0"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["S0_error"] = 0 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["S0_az"] = 0 * degrees 
            cal_dict["S0_el"] = 45 * degrees * np.ones_like(cal_dict['wavelengths'])
            cal_dict["S0_pol_degree"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["P0_az"] = 0 * degrees 
            cal_dict["S1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["S1_error"] = 0 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["S1_az"] = 0 * degrees 
            cal_dict["S1_el"] = 45 * degrees * np.ones_like(cal_dict['wavelengths'])
            cal_dict["S1_pol_degree"] = 1 * np.ones_like(cal_dict['wavelengths'])

            cal_dict["P1_p1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["P1_p2"] = 0 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["P1_az"] = 0 * degrees 
            cal_dict["P1_Ret"] = 0 * degrees * np.ones_like(cal_dict['wavelengths'])

            cal_dict["P2_p1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["P2_p2"] = 0 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["P2_az"] = 0 * degrees 
            cal_dict["P2_Ret"] = 0 * degrees * np.ones_like(cal_dict['wavelengths'])

            cal_dict["Pc_p1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["Pc_p2"] = 0 * np.ones_like(cal_dict['wavelengths'])

            cal_dict["R1_p1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["R1_p2"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["R1_az"] = 0 * degrees 
            cal_dict["R1_Ret"] = 90 * degrees * np.ones_like(cal_dict['wavelengths'])

            cal_dict["R2_p1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["R2_p2"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["R2_az"] = 0 * degrees 
            cal_dict["R2_Ret"] = 90 * degrees * np.ones_like(cal_dict['wavelengths'])

            cal_dict["Rc_p1"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["Rc_p2"] = 1 * np.ones_like(cal_dict['wavelengths'])
            cal_dict["Rc_Ret"] = 90 * degrees * np.ones_like(cal_dict['wavelengths'])
            cal_dict["Rc_offset"] = 0 * degrees * np.ones_like(cal_dict['wavelengths'])

        return cal_dict
    
    def Save_Cal_Files(cal_dict : dict = None, filename : str = None, folder : str = None):
        """
        Function to save the calibration files.

        Args:
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            filename (string): Name of the calibration files. Default: None.
            folder (string): Folder where the calibration files are located. Default: None.
        
        Returns:
            None
        """ 
        # Open the folder
        if folder is not None:
            old_folder = os.getcwd()
            os.chdir(folder)

        # Check if the dictionary is specified
        if cal_dict is None:
            print("Calibration dictionary must be specified")
            return
        
        # Save the files
        if cal_dict and filename:
            np.savez("Step_data_" + filename, num_wavelengths = cal_dict['num_wavelengths'], wavelengths = cal_dict['wavelengths'], Twait = cal_dict['Twait'], N_measures = cal_dict['N_measures'], max_angle = cal_dict['max_angle'], angles = cal_dict['angles'])
            np.savez("Step_0_" + filename, S0 = cal_dict["S0"], S0_error = cal_dict["S0_error"], S0_az = cal_dict["S0_az"], S0_el = cal_dict["S0_el"], S0_pol_degree = cal_dict["S0_pol_degree"], P0_az = cal_dict["P0_az"], 
                                           S1 = cal_dict["S1"], S1_error = cal_dict["S1_error"], S1_az = cal_dict["S1_az"], S1_el = cal_dict["S1_el"], S1_pol_degree = cal_dict["S1_pol_degree"])
            np.savez("Step_1a_" + filename, Iexp = cal_dict["I_step_1a"], angles = cal_dict["Angles_step_1a"])
            np.savez("Step_1b_" + filename, Iexp = cal_dict["I_step_1b"], angles = cal_dict["Angles_step_1b"])
            np.savez("Step_1c_" + filename, Iexp = cal_dict["I_step_1c"], angles = cal_dict["Angles_step_1c"])
            np.savez("Step_2a_" + filename, Iexp = cal_dict["I_step_2a"], angles = cal_dict["Angles_step_2a"])
            np.savez("Step_2b_" + filename, Iexp = cal_dict["I_step_2b"], angles = cal_dict["Angles_step_2b"])
            np.savez("Step_2c_" + filename, Iexp = cal_dict["I_step_2c"], angles = cal_dict["Angles_step_2c"])
            np.savez("Step_3_" + filename, Iexp = cal_dict["I_step_3"], angles = cal_dict["Angles_step_3"])
            np.savez("Step_4a_" + filename, Iexp = cal_dict["I_step_4a"], angles = cal_dict["Angles_step_4a"])
            np.savez("Step_4b_" + filename, Iexp = cal_dict["I_step_4b"], angles = cal_dict["Angles_step_4b"])
            np.savez("Step_4c_" + filename, Iexp = cal_dict["I_step_4c"], angles = cal_dict["Angles_step_4c"])
            np.savez("Step_5_" + filename, Iexp = cal_dict["I_step_5"], angles = cal_dict["Angles_step_5"])
            np.savez("Calibration_measurements.npz", **cal_dict)
            
        if cal_dict and not filename:
            np.savez("Calibration_measurements.npz", **cal_dict)
        
        # Return to the old folder
        if folder and old_folder:
            os.chdir(old_folder)
            
        return
    
    def Load_Cal_Files(filename: str | None =None, folder: str | None = None):
        """
        Function to load the initial cal_dict, except calibration files are loaded.
        
        Args:
            folder (string or None): Folder where the calibration files are located. Default: None.
            filename (string or None): Name of the calibration files. Default: None.            
            
        Returns:
            cal_dict (dict): Dictionary with the calibration configuration.
        """
        # Open the folder
        if folder is not None:
            old_folder = os.getcwd()
            os.chdir(folder)
        
        # Check if the filename is specified
        if filename is None:
            print("Calibration filename must be specified")
            return

        # Load the files
        if filename == "Calibration_measurements.npz":
            data = np.load("Calibration_measurements.npz")
            cal_dict = dict(data) 
        
        # Load the files if filename is not Calibration_measurements.npz
        else:
            
            if np.load("Step_data_" + filename + ".npz")["num_wavelengths"] == 1: 
                single = True
            else:
                single = False
            
            wavelengths = np.load("Step_data_" + filename + ".npz")["wavelengths"]
            
            cal_dict = SpectralPolarimeterCalibration.Create_Cal_Files(single = single, wavelengths = wavelengths)
            
            data = np.load("Step_data_" + filename + ".npz")
            cal_dict["num_wavelengths"] = data["num_wavelengths"]
            cal_dict["wavelengths"] = data["wavelengths"]
            cal_dict["Twait"] = data["Twait"]
            cal_dict["N_measures"] = data["N_measures"]
            cal_dict["max_angle"] = data["max_angle"]
            cal_dict["angles"] = data["angles"]

            data = np.load("Step_0_" + filename + ".npz")
            cal_dict["S0"] = data["S0"]
            cal_dict["S0_error"] = data["S0_error"]
            cal_dict["S0_az"] = data["S0_az"]
            cal_dict["S0_el"] = data["S0_el"]
            cal_dict["S0_pol_degree"] = data["S0_pol_degree"]
            cal_dict["P0_az"] = data["P0_az"]
            cal_dict["S1"] = data["S1"]
            cal_dict["S1_error"] = data["S1_error"]
            cal_dict["S1_az"] = data["S1_az"]
            cal_dict["S1_el"] = data["S1_el"]
            cal_dict["S1_pol_degree"] = data["S1_pol_degree"]
            
            data = np.load("Step_1a_" + filename + ".npz")
            cal_dict["I_step_1a"] = data["Iexp"]
            cal_dict["Angles_step_1a"] = data["angles"]
            
            data = np.load("Step_1b_" + filename + ".npz")
            cal_dict["I_step_1b"] = data["Iexp"]
            cal_dict["Angles_step_1b"] = data["angles"]
            
            data = np.load("Step_1c_" + filename + ".npz")
            cal_dict["I_step_1c"] = data["Iexp"]
            cal_dict["Angles_step_1c"] = data["angles"]
            
            data = np.load("Step_2a_" + filename + ".npz")
            cal_dict["I_step_2a"] = data["Iexp"]
            cal_dict["Angles_step_2a"] = data["angles"]

            data = np.load("Step_2b_" + filename + ".npz")
            cal_dict["I_step_2b"] = data["Iexp"]
            cal_dict["Angles_step_2b"] = data["angles"]

            data = np.load("Step_2c_" + filename + ".npz")
            cal_dict["I_step_2c"] = data["Iexp"]
            cal_dict["Angles_step_2c"] = data["angles"]

            data = np.load("Step_3_" + filename + ".npz")
            cal_dict["I_step_3"] = data["Iexp"]
            cal_dict["Angles_step_3"] = data["angles"]

            data = np.load("Step_4a_" + filename + ".npz")
            cal_dict["I_step_4a"] = data["Iexp"]
            cal_dict["Angles_step_4a"] = data["angles"]

            data = np.load("Step_4b_" + filename + ".npz")
            cal_dict["I_step_4b"] = data["Iexp"]
            cal_dict["Angles_step_4b"] = data["angles"]

            data = np.load("Step_4c_" + filename + ".npz")
            cal_dict["I_step_4c"] = data["Iexp"]
            cal_dict["Angles_step_4c"] = data["angles"]

            data = np.load("Step_5_" + filename + ".npz")
            cal_dict["I_step_5"] = data["Iexp"]
            cal_dict["Angles_step_5"] = data["angles"]

        # Return to the old folder
        if folder and old_folder:
            os.chdir(old_folder)
        
        # Return the dictionary
        return cal_dict    
    
    def Set_Cal_Parameter(cal_dict: dict, param : str, value: float | np.ndarray, wavelengths: float | np.ndarray = None):
        """ 
        Function to set a calibration parameter in the dictionary.

        Args:
            cal_dict (dict): Dictionary with the calibration configuration.
            param (string): Parameter to change.
            value (float): Value of the parameter.
            wavelength (float): Wavelength of the calibration. Default: None.
            
        Returns:
            cal_dict (dict): Dictionary with the calibration configuration.
        """
        
        # Check if the parameter is in the dictionary
        if param not in cal_dict.keys():
            print("Parameter not found")
            return cal_dict
        # Check if the parameter is num_wavelengths or wavelengths
        if param in ['wavelengths', 'num_wavelengths']:
            print("wavelengths and num_wavelengths cannot be changed")
            return cal_dict
                        
        # Individual dictionary
        if cal_dict['num_wavelengths'] == 1:
            print('To change the parameter, write "cal_dict[param] = value" ')
            return cal_dict
        
        # Spectral dictionary
        else:
            # Check if wavelength is specified
            if wavelengths is None: 
                print("Wavelengths must be specified when num_wavelengths > 1")
                return cal_dict
            
            # Check if value and wavelength are arrays and the same size
            if type(value) in (int,float,list):
                value = np.array(value)
            if type(wavelengths) in (int,float,list): 
                wavelengths = np.array(wavelengths)
            
            if value.size == wavelengths.size:
                if value.size == 1: 
                    cal_dict[param][np.argmin(np.abs(cal_dict['wavelengths'] - wavelengths))] = value
                else:
                    for idx, val in enumerate(value):
                        cal_dict[param][np.argmin(np.abs(cal_dict['wavelengths'] - wavelengths[idx]))] = val
            else: 
                print("Value and wavelength must have the same size")
                return cal_dict
        
        return cal_dict
    
    def Print_Cal_Files(cal_dict : dict, wavelength : float | int | list | np.ndarray = None): 
        """Print the calibration parameters.
        
        Args:
            cal_dict (dict): Dictionary with the calibration configuration.
            wavelength (float): Wavelength of the calibration. Default: None.
            
        Returns:
            None
        """
        
        # If single wavelength
        if cal_dict['num_wavelengths'] == 1: 
            if wavelength is not None: 
                print("Wavelength must not be defined for single wavelength")
                return
            # Print the calibration parameters
            print("Parámetros de Calibración: ")
            print("Longitud de onda: {:.2f} nm".format(cal_dict['wavelengths']))
            print("Datos de la fuente: ")
            print("\tIntensidad: {:.2f}".format(cal_dict['S1']))
            print("\tAzimuth: {:.2f}°".format(cal_dict['S1_az']/degrees))
            print("\tElipticidad: {:.2f}°".format(cal_dict['S1_el']/degrees))
            print("\tGrado de polarización: {:.2f}".format(cal_dict['S1_pol_degree']))

            print("Datos de P1, P2 y Pc: ")
            print("\tP1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P1_p1'],cal_dict['P1_p2'],cal_dict['P1_Ret']/degrees,cal_dict['P1_az']/degrees))
            print("\tP2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P2_p1'],cal_dict['P2_p2'],cal_dict['P2_Ret']/degrees,cal_dict['P2_az']/degrees))
            print("\tPc:\tp1: {:.2f}".format(cal_dict['Pc_p1']))
            
            print("Datos de R1, R2 y Rc: ")
            print("\tR1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R1_p1'],cal_dict['R1_p2'],cal_dict['R1_Ret']/degrees, cal_dict['R1_az']/degrees))
            print("\tR2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R2_p1'],cal_dict['R2_p2'],cal_dict['R2_Ret']/degrees, cal_dict['R2_az']/degrees))
            print("\tRc:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\toffset: {:.2f}°".format(cal_dict['Rc_p1'],cal_dict['Rc_p2'],cal_dict['Rc_Ret']/degrees, cal_dict['Rc_offset']/degrees))
        
        else: 
            if wavelength is None: 
                print("Wavelength must be specified when num_wavelengths > 1")
                return
            else: 
                idx = np.argmin(np.abs(cal_dict['wavelengths'] - wavelength))
                print(idx)
            # Print the calibration parameters
            print("Parámetros de Calibración: ")
            print("Longitud de onda: {:.2f} nm".format(cal_dict['wavelengths'][idx]))
            print("\tIntensidad: {:.2f}".format(cal_dict['S1'][idx]))
            print("\tAzimuth: {:.2f}°".format(cal_dict['S1_az']/degrees))
            print("\tElipticidad: {:.2f}°".format(cal_dict['S1_el'][idx]/degrees))
            print("\tGrado de polarización: {:.2f}".format(cal_dict['S1_pol_degree'][idx]))

            print("Datos de P1, P2 y Pc: ")
            print("\tP1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P1_p1'][idx],cal_dict['P1_p2'][idx],cal_dict['P1_Ret'][idx]/degrees,cal_dict['P1_az']/degrees))
            print("\tP2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['P2_p1'][idx],cal_dict['P2_p2'][idx],cal_dict['P2_Ret'][idx]/degrees,cal_dict['P2_az']/degrees))
            print("\tPc:\tp1: {:.2f}\tp2: {:.2f}".format(cal_dict['Pc_p1'][idx],cal_dict['Pc_p2'][idx]))
            
            print("Datos de R1, R2 y Rc: ")
            print("\tR1:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R1_p1'][idx],cal_dict['R1_p2'][idx],cal_dict['R1_Ret'][idx]/degrees, cal_dict['R1_az']/degrees))
            print("\tR2:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\taz: {:.2f}°".format(cal_dict['R2_p1'][idx],cal_dict['R2_p2'][idx],cal_dict['R2_Ret'][idx]/degrees, cal_dict['R2_az']/degrees))
            print("\tRc:\tp1: {:.2f}\tp2: {:.2f}\tR: {:.2f}°\toffset: {:.2f}°".format(cal_dict['Rc_p1'][idx],cal_dict['Rc_p2'][idx],cal_dict['Rc_Ret'][idx]/degrees, cal_dict['Rc_offset'][idx]/degrees))
        return
        
    
    def Reduce_Cal_Files(cal_dict : dict, N : int, intensities : bool = False): 
        """Function to reduce the number of measurements in the calibration files.
        
        Args:
            cal_dict (dict): Dictionary with the calibration configuration.
            N (int): New number of measurements.
            intensities (bool): If True, the intensities are reduced. Default: True.
            
        Returns:
            cal_dict_new (dict): New dictionary with the calibration configuration.
        """
        # Check if the number of wavelengths is 1
        cal_dict_new = cal_dict.copy()
        if cal_dict['num_wavelengths'] == 1:
            print("Single wavelength, no reduction needed")
            return cal_dict
        
        # Reduce the number of measurements
        else: 
            N_old = cal_dict['num_wavelengths']-1
            
            cal_dict_new['num_wavelengths'] = N
            cal_dict_new['wavelengths'] = cal_dict['wavelengths'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['S0'] = cal_dict['S0'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S0_error'] = cal_dict['S0_error'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S0_az'] = cal_dict['S0_az'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S0_el'] = cal_dict['S0_el'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S0_pol_degree'] = cal_dict['S0_pol_degree'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1'] = cal_dict['S1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1_error'] = cal_dict['S1_error'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1_pol_degree'] = cal_dict['illum_pol_degree'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1_az'] = cal_dict['illum_az'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['S1_el'] = cal_dict['illum_el'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P0_az'] = cal_dict['P0_az'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['P1_p1'] = cal_dict['P1_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P1_p2'] = cal_dict['P1_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P1_Ret'] = cal_dict['P1_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P1_az'] = cal_dict['P1_az'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['P2_p1'] = cal_dict['P2_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P2_p2'] = cal_dict['P2_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P2_Ret'] = cal_dict['P2_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['P2_az'] = cal_dict['P2_az'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['Pc_p1'] = cal_dict['Pc_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Pc_p2'] = cal_dict['Pc_p2'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['R1_p1'] = cal_dict['R1_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R1_p2'] = cal_dict['R1_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R1_Ret'] = cal_dict['R1_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R1_az'] = cal_dict['R1_az'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['R2_p1'] = cal_dict['R2_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R2_p2'] = cal_dict['R2_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R2_Ret'] = cal_dict['R2_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['R2_az'] = cal_dict['R2_az'][np.linspace(0,N_old,N,dtype=int)]
            
            cal_dict_new['Rc_p1'] = cal_dict['Rc_p1'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_p2'] = cal_dict['Rc_p2'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_Ret'] = cal_dict['Rc_Ret'][np.linspace(0,N_old,N,dtype=int)]
            cal_dict_new['Rc_offset'] = cal_dict['Rc_offset'][np.linspace(0,N_old,N,dtype=int)]
            
            # Reduce the intensities if needed
            if intensities: 
                cal_dict_new['I_step_1a'] = cal_dict['I_step_1a'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_1b'] = cal_dict['I_step_1b'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_1c'] = cal_dict['I_step_1c'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_2a'] = cal_dict['I_step_2a'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_2b'] = cal_dict['I_step_2b'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_2c'] = cal_dict['I_step_2c'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_3'] = cal_dict['I_step_3'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_4a'] = cal_dict['I_step_4a'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_4b'] = cal_dict['I_step_4b'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_4c'] = cal_dict['I_step_4c'][:,np.linspace(0,N_old,N,dtype=int)]
                cal_dict_new['I_step_5'] = cal_dict['I_step_5'][:,np.linspace(0,N_old,N,dtype=int)]

            return cal_dict_new

    
    def make_step(pol = None, cal_dict = None, step = None, single = False, verbose = False, mode = None):
        
        """Function to make a calibration step.

        Args:
            pol (SpectralPolarimeter): Spectral polarimeter object.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            step (string): Step to make. Default: None.
            single (bool): If True, only one measurement is made. Default: False.
            verbose (bool): If True, the function is verbose. Default: False.
            mode(bool): If True, the function changes measurement. Default: False.

        Returns:
            mode 'intensity': 
                mean (np.ndarray): Array with the mean intensity of the step.
                error (np.ndarray): Array with the error intensity of the step.

            mode 'manual or 'motor':
                angles (np.ndarray): Array with the angles of the step.
                Iexp (np.ndarray): Array with the intensities of the step.
        """

        # Initialize the variables
        Iexp = []
        
        # Step 0 : INITIAL LIGHT MEASUREMENT 
    
        if step == "0": 
            
            if mode == "intensity":
                for ind in range(cal_dict["N_measures"]): 
                    if single:
                        Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                    else: 
                        Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    time.sleep(cal_dict['Twait'])
                    print("Measurement {} of {} done".format(ind+1, cal_dict["N_measures"]), end='\r', flush=True)
                
                # Calculate the mean and error
                Iexp = np.array(Iexp) 
                mean = np.mean(Iexp, axis=0)
                error = np.std(Iexp, axis=0)
                
                # Verbose
                if verbose: 
                    if single: 
                        print("\nMean intensity: {:.2f}".format(mean))
                        print("Error intensity: {:.2f}".format(error))
                    else: 
                        plt.subplot(1,2,1)
                        plt.plot(cal_dict['wavelengths'],mean)
                        plt.title("\nMean intensity - S0")
                        plt.xlabel("Wavelength (nm)")
                        plt.ylabel("Intensity (a.u.)")
                        plt.subplot(1,2,2)
                        plt.plot(cal_dict['wavelengths'], error)
                        plt.title("\nError intensity - S0")
                        plt.xlabel("Wavelength (nm)")
                        plt.ylabel("Intensity (a.u.)")
                        
                return mean, error
            
            elif mode == "manual": 
                angles = np.zeros(100)
                # Make the measurements
                for ind in range(100):
                    ang = input('Angle (in degrees). Type "End" to close.')
                    try:
                        angles[ind] = float(ang)
                        if single:
                            Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                        else: 
                            Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    except:
                        if ang.lower() in ('fin', 'end', 'stop'):
                            angles = angles[:ind]
                            break
                        else:
                            print('Value {} not accepted'.format(ang))
                            
                Iexp = np.array(Iexp)
                angles = np.array(angles*degrees)
                
                return Iexp, angles
            
            elif mode == "motor":
                utils.percentage_complete()
                angles = np.zeros_like(cal_dict["angles"])
                # Make the measurements
                for ind, angle in enumerate(cal_dict["angles"]):
                    theta = np.array([angle, 0, 0, 0])
                    angles_aux = pol.motor.Move_Absolute(
                        pos=theta, waiting='busy', units="deg", move_time=0.5)
                    angles[ind] = angles_aux[0] * degrees
                    if single:
                        Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                    else: 
                        Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                        
                    utils.percentage_complete(ind+1, cal_dict["N_measures"])
            
                Iexp = np.array(Iexp)
                
                return Iexp, angles
            
            if mode is None: 
                print("'mode' parameter must be specified")
                return
            
        # Step 1a, 1b, 1c : POLARIZER MEASUREMENTS
        
        elif step == "1a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([angle, 0, 0, 0])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[0] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
                        
            Iexp = np.array(Iexp)
                        
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        elif step == "1b" or step == "1c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, 0, angle])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[3] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
                        
            Iexp = np.array(Iexp)
                        
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
           
        
        # Step 2a - Malus Law to measure Pref and P1
        elif step == "2a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, angle, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 2b - Malus Law to measure Pref and P2
        elif step == "2b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, 0, angle + cal_dict['R2_az']/degrees, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angle * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 2c - Malus Law to measure P1 and P2
        elif step == "2c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            angle_2c = cal_dict["P0_az"]/degrees + 45 
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([angle_2c, 0, angle + angle_2c - cal_dict['P1_az']/degrees + cal_dict['R2_az']/degrees, angle_2c - cal_dict['P1_az']/degrees + cal_dict['P2_az']/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[2] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        
        # Step 3 - Illumination analysis
        elif step == '3': 
            angles = utils.PSA_states_2_angles(S = utils.S_20)
            # Make the measurements
            utils.percentage_complete()
            for ind in range(len(angles[0])):
                theta = np.array((0, 0, angles[1][ind]+cal_dict['R2_az'], angles[0][ind] + cal_dict['P2_az']))
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="rad")
                #print(angles_aux)
                angles[1][ind] = angles_aux[2]
                angles[0][ind] = angles_aux[3]
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                utils.percentage_complete(ind, 20)
            Iexp = np.array(Iexp)
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles

        # Step 4a - Retarder R1 Mesaurement
        elif step == "4a":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, angle, 0, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 4b - Retarder R1 - Rc Measurement
        elif step == "4b":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([cal_dict["P1_az"]/degrees, angle + cal_dict['R1_az']/degrees, 0, cal_dict["P2_az"]/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angle * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        elif step == "4c":
            utils.percentage_complete()
            angles = np.zeros_like(cal_dict["angles"])
            angle_4c = cal_dict['P0_az']/degrees + 45
            # Make the measurements
            for ind, angle in enumerate(cal_dict["angles"]):
                theta = np.array([angle_4c, angle + angle_4c - cal_dict['P1_az']/degrees + cal_dict['R1_az']/degrees , 0, angle_4c - cal_dict['P1_az']/degrees + cal_dict['P2_az']/degrees])
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="deg", move_time=0.5)
                angles[ind] = angles_aux[1] * degrees
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                    
                utils.percentage_complete(ind+1, cal_dict["N_measures"])
           
            Iexp = np.array(Iexp)
            
            
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles
        
        # Step 5 - Air Mueller Matrix
        elif step == '5': 
            angles = utils.calculate_polarimetry_angles(144, type='perfect')
            # Make the measurements
            utils.percentage_complete()
            for ind, angle in enumerate(angles):
                theta_0 = np.array((cal_dict['P1_az'],cal_dict['R1_az'],cal_dict['R2_az'],cal_dict['P2_az']))
                theta = theta_0 + angle
                angles_aux = pol.motor.Move_Absolute(
                    pos=theta, waiting='busy', units="rad")
                angles[ind] = angle 
                if single:
                    Iexp.append(pol.spectrometer.Get_Measurement(mode = "wavelength", wavelength = cal_dict['wavelengths']))
                else: 
                    Iexp.append(pol.spectrometer.Get_Signal(verbose = False,  wavelengths = cal_dict['wavelengths']))
                utils.percentage_complete(ind, 144)
            Iexp = np.array(Iexp)
            # Save data in dictionary
            cal_dict["I_step_" + step] = Iexp
            cal_dict["Angles_step_" + step] = angles
            
            return Iexp, angles

        else: 
            return("Error. Step not recognized")
      
    
    def model_step(par = None,  step = None,  angle = None, cal_dict = None): 
        """Function to model the calibration step.
        
        Args:
            step (string): Step to model. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            par (np.ndarray): Parameters of the model. Default: None.
            angle (np.ndarray): Array with the angles of the step. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        """

        if step == "polyfit":
            Imodel = model_polyfit(par, angle)
            return Imodel
        if step == "cos_2":
            Imodel = model_cos2(par, angle)
            return Imodel
        elif step == "cos_2_2":
            Imodel = model_cos2_2(par, angle)
            return Imodel              
            
        elif step == '1': 
            # Rename parameters
            (P1_p1, P2_p1, Pc_p1) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S0"],
                azimuth = cal_dict['S0_az'],
                ellipticity = cal_dict["S0_el"],
                degree_pol = cal_dict["S0_pol_degree"])
            S1 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S1"],
                azimuth = cal_dict["S1_az"],
                ellipticity = cal_dict["S1_el"],
                degree_pol = cal_dict["S1_pol_degree"])

            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=P1_p1, p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict['R2_p1'], p2=cal_dict['R2_p2'], R=cal_dict['R2_Ret'], azimuth=-cal_dict['R2_az'])
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=P2_p1, p2=cal_dict['P2_p2'], R = cal_dict['P2_Ret'], azimuth=-cal_dict['P2_az'])
            Mpc = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=Pc_p1, p2=cal_dict['Pc_p2'], azimuth=0)
            
            # Step 1a
            Mp1_rot = Mp1.rotate(angle=cal_dict["Angles_step_1a"], keep=True)
            Sf = (Mp1_rot * (Mpc * S1))
            I_1a = Sf.parameters.intensity()
            
            # Step 1b
            Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_1b"], keep=True)
            Sf = (Mp2_rot * (Mpc * S1))
            I_1b = Sf.parameters.intensity()
            
            # Step 1c
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mp2_rot = Mp2.rotate(angle=cal_dict["Angles_step_1c"], keep=True)
            Sf = (Mp2_rot * (Mp1_rot * S1))
            I_1c = Sf.parameters.intensity()
            
            return I_1a, I_1b, I_1c    
            
        elif step == '2': 
            # Rename parameters
            (R2_p1, R2_p2, R2_Ret, R2_error) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S0"],
                azimuth = cal_dict['S0_az'],
                ellipticity = cal_dict["S0_el"],
                degree_pol = cal_dict["S0_pol_degree"])
            S1 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S1"],
                azimuth = cal_dict["S1_az"],
                ellipticity = cal_dict["S1_el"],
                degree_pol = cal_dict["S1_pol_degree"])

            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'] - R2_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P2_p1'], p2=cal_dict['P2_p2'], R = cal_dict['P2_Ret'], azimuth=-cal_dict['P2_az'])
            
            # Step 2a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict['angles']*degrees , keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S1)))
            I_2a = Sf.parameters.intensity()
            
            # Step 2c
            angle_2c = cal_dict['P0_az'] + 45*degrees 
            Mp1_rot = Mp1.rotate(angle = angle_2c, keep=True)            
            Mr2_rot = Mr2.rotate(angle = cal_dict["Angles_step_2c"] , keep=True)
            Mp2_rot = Mp2.rotate(angle = angle_2c-cal_dict['P1_az']+cal_dict['P2_az'], keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S0)))
            I_2c = Sf.parameters.intensity()
            
            return I_2a, I_2c

        elif step == '2_global': 
            # Rename parameters
            (P1_p2, P1_Ret, R2_p1, R2_p2, R2_Ret, R2_error) = par
            
            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S0"],
                azimuth = cal_dict['S0_az'],
                ellipticity = cal_dict["S0_el"],
                degree_pol = cal_dict["S0_pol_degree"])
            S1 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S1"],
                azimuth = cal_dict["S1_az"],
                ellipticity = cal_dict["S1_el"],
                degree_pol = cal_dict["S1_pol_degree"])

            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=P1_p2, R = P1_Ret, azimuth=-cal_dict['P1_az'])
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=R2_p1, p2=R2_p2, R=R2_Ret, azimuth=-cal_dict['R2_az'] - R2_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P2_p1'], p2=cal_dict['P2_p2'], R = cal_dict['P2_Ret'], azimuth=-cal_dict['P2_az'])
            
            # Step 2a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr2_rot = Mr2.rotate(angle=cal_dict["Angles_step_2a"], keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S1)))
            I_2a = Sf.parameters.intensity()
            
            # Step 2c
            angle_2c = cal_dict['P0_az'] + 45*degrees 
            Mp1_rot = Mp1.rotate(angle = angle_2c, keep=True)            
            Mr2_rot = Mr2.rotate(angle = cal_dict["Angles_step_2c"] , keep=True)
            Mp2_rot = Mp2.rotate(angle = angle_2c-cal_dict['P1_az']+cal_dict['P2_az'], keep=True)
            Sf = (Mp2_rot * (Mr2_rot * (Mp1_rot * S0)))
            I_2c = Sf.parameters.intensity()
            
            return I_2a, I_2c
                
        elif step == '4': 
            # Rename parameters
            (R1_p1, R1_p2, R1_Ret, R1_error) = par

            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S0"],
                azimuth = cal_dict['S0_az'],
                ellipticity = cal_dict["S0_el"],
                degree_pol = cal_dict["S0_pol_degree"])
            S1 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S1"],
                azimuth = cal_dict["S1_az"],
                ellipticity = cal_dict["S1_el"],
                degree_pol = cal_dict["S1_pol_degree"])

            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P1_az'])
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'] - R1_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P2_p1'], p2=cal_dict['P2_p2'], R = cal_dict['P2_Ret'], azimuth=-cal_dict['P2_az'])
            
            # Step 4a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_4a"], keep=True)
            Sf = (Mp1_rot * (Mr1_rot * (Mp2_rot * S1)))
            I_4a = Sf.parameters.intensity()
            
            # Step 4c
            angle_4c = cal_dict['P0_az'] + 45*degrees 
            Mp1_rot = Mp1.rotate(angle = angle_4c, keep=True)            
            Mr1_rot = Mr1.rotate(angle = cal_dict["Angles_step_4c"] , keep=True)
            Mp2_rot = Mp2.rotate(angle = angle_4c - cal_dict['P1_az'] + cal_dict['P2_az'], keep=True)
            Sf = (Mp1_rot * (Mr1_rot * (Mp2_rot * S0)))
            I_4c = Sf.parameters.intensity()
            
            return I_4a, I_4c
        
        elif step == '4_global': 
            # Rename parameters
            (P2_p2, P2_Ret, R1_p1, R1_p2, R1_Ret, R1_error) = par
            
            # Create objects
            S0 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S0"],
                azimuth = cal_dict['S0_az'],
                ellipticity = cal_dict["S0_el"],
                degree_pol = cal_dict["S0_pol_degree"])
            S1 = Stokes().general_azimuth_ellipticity(
                intensity = cal_dict["S1"],
                azimuth = cal_dict["S1_az"],
                ellipticity = cal_dict["S1_el"],
                degree_pol = cal_dict["S1_pol_degree"])
            
            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P1_p1'], p2=cal_dict['P1_p2'], R = cal_dict['P1_Ret'], azimuth=-cal_dict['P1_az'])
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=R1_p1, p2=R1_p2, R=R1_Ret, azimuth=-cal_dict['R1_az'] - R1_error)
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(
                p1=cal_dict['P2_p1'], p2=P2_p2, R = P2_Ret, azimuth=-cal_dict['P2_az'])
            
            # Step 4a
            Mp2_rot = Mp2.rotate(angle=cal_dict["P2_az"], keep=True)
            Mp1_rot = Mp1.rotate(angle=cal_dict["P1_az"], keep=True)
            Mr1_rot = Mr1.rotate(angle=cal_dict["Angles_step_4a"], keep=True)
            Sf = (Mp1_rot * (Mr1_rot * (Mp2_rot * S1)))
            I_4a = Sf.parameters.intensity()
            
            # Step 4c
            angle_4c = cal_dict['P0_az'] + 45*degrees 
            Mp1_rot = Mp1.rotate(angle = angle_4c, keep=True)            
            Mr1_rot = Mr1.rotate(angle = cal_dict["Angles_step_4c"] , keep=True)
            Mp2_rot = Mp2.rotate(angle = angle_4c - cal_dict['P1_az'] + cal_dict['P2_az'], keep=True)
            Sf = (Mp1_rot * (Mr1_rot * (Mp2_rot * S0)))
            I_4c = Sf.parameters.intensity()
            
            return I_4a, I_4c
        
        else: 
            return("Error. Step not recognized")
        
            
        
    def error_step(par = None, step = None, angle = None, Iexp = None, cal_dict = None):
        """Function to calculate the error for adjusting the model to the experimental data.
        
        Args:
            step (string): Step to model. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            par (np.ndarray): Parameters of the model. Default: None.
            angle (np.ndarray): Array with the angles of the step. Default: None.
            Iexp (np.ndarray): Array with the intensities of the step. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
        """

        if step == "polyfit":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "polyfit", angle)
            dif = Iexp - Imodel
            return dif
        if step == "cos_2":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "cos_2", angle)
            dif = Iexp - Imodel
            return dif
        elif step == "cos_2_2":
            Imodel = SpectralPolarimeterCalibration.model_step(par, "cos_2_2", angle)
            dif = Iexp - Imodel
            return dif
        
        elif step == '1':
            I_1a, I_1b, I_1c = SpectralPolarimeterCalibration.model_step(par,'1',angle = angle,cal_dict=cal_dict)
            
            E_1a = (cal_dict["I_step_1a"] - I_1a)
            E_1b = (cal_dict["I_step_1b"] - I_1b)
            E_1c = (cal_dict["I_step_1c"] - I_1c)
            
            Error = np.concatenate((E_1a, E_1b, E_1c))
            return Error
        
        elif step == "2":
            I_2a, I_2c = SpectralPolarimeterCalibration.model_step(par,'2',angle = None,cal_dict=cal_dict)

            E_2a = (cal_dict["I_step_2a"] - I_2a)
            E_2c = (cal_dict["I_step_2c"] - I_2c)
            
            Error = np.concatenate((E_2a, E_2c))
            return Error
        
        elif step == "2_global":
            
            I_2a, I_2c = SpectralPolarimeterCalibration.model_step(par,'2_global',angle = None,cal_dict=cal_dict)
            
            E_2a = (cal_dict["I_step_2a"] - I_2a)
            E_2c = (cal_dict["I_step_2c"] - I_2c)
            
            Error = np.concatenate((E_2a, E_2c))
            return Error

        elif step == "4":
            I_4a, I_4c = SpectralPolarimeterCalibration.model_step(par,'4',angle = None,cal_dict=cal_dict)

            E_4a = (cal_dict["I_step_4a"] - I_4a)
            E_4c = (cal_dict["I_step_4c"] - I_4c)
            
            Error = np.concatenate((E_4a, E_4c))
            return Error
        
        elif step == "4_global":
            I_4a, I_4c = SpectralPolarimeterCalibration.model_step(par,'4_global',angle = None,cal_dict=cal_dict)

            E_4a = (cal_dict["I_step_4a"] - I_4a)
            E_4c = (cal_dict["I_step_4c"] - I_4c)
            
            Error = np.concatenate((E_4a, E_4c))
            return Error
        
        else: 
            return("Error. Step not recognized")
        
    def analyze_step(step = None, cal_dict = None, single = False, Iexp = None, angles = None, verbose = True):
        """Function to analyze the calibration step.
        
        Args: 
            step (string): Step to analyze. Default: None.
            cal_dict (dict): Dictionary with the calibration configuration. Default: None.
            single (bool): If True, the function will analyze a single measurement. Default: False.
            Iexp (np.ndarray): Array with the intensities of the step. Default: None.
            angles (np.ndarray): Array with the angles of the step. Default: None.
            verbose (bool): If True, the function will print the results. Default: True.
            
        Returns:
            np.ndarray: Array with the optimized parameters."""
        
        if step == "polyfit":
            # Create the initial parameters
            optm_result = []
            # If single measurement
            if single: 
                I = Iexp# /cal_dict['S0']
                par0 = [1,1,1,1,np.random.rand()*90*degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step,par0, args=("polyfit", angles, I) )
                optm_result.append(result.x)
            # If multiple measurements
            else:
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i]# /cal_dict['S0'][i]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, args=("polyfit", angles, I[:,i]))
                    optm_result.append(result.x)
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")
            
            # Plot result and print results
            if single: 
                optm_result = optm_result[0]

                Imodel = model_polyfit(optm_result, angles)
                if verbose: 
                    utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Polyfit')
                    print('The values obtained are:')
                    print('   - Coefficients: {}'.format(optm_result))

            return np.array(optm_result)
            

        if step == "cos_2": 
            # Create the initial parameters
            optm_result = []
            # If single measurement
            if single: 
                I = Iexp# /cal_dict['S0']
                (Imax, Imin) = (np.max(I), np.min(I))
                bounds = ([0, 0, -180*degrees], [Imax,Imax, 180 * degrees])
                par0 = [np.max((0,Imin)), Imax , np.random.rand() * 180*degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("cos_2", angles, I), bounds=bounds)
                optm_result.append(result.x)
            # If multiple measurements
            else: 
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i]# /cal_dict['S0'][i]
                    (Imax, Imin) = (np.max(I[:,i]), np.min(I[:,i]))
                    bounds = ([0, 0, -180*degrees], [Imax+1,Imax+1, 180 * degrees])
                    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), 90 *degrees]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("cos_2", angles, I[:,i]), bounds=bounds)
                    result.x[2] = result.x[2]
                    optm_result.append(result.x)
                    
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")

            # Plot result and print results
            if single: 
                optm_result = optm_result[0]

                Imodel = model_cos2(optm_result, angles)

                # utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Step cos_2')
                # print('The values obtained are:')
                # print('   - Imax       : {:.4f} V'.format(optm_result[1] + optm_result[0]))
                # print('   - Imin       : {:.4f} V'.format(optm_result[0]))
                # print('   - Maximum angle : {:.2f} deg'.format(optm_result[2]/degrees))  
            else: 
                Imodel = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    Imodel[:,i] = model_cos2(optm_result[i], angles)
                draw_values(cal_dict['wavelengths'], angles/degrees, Iexp, Imodel, param_y_name='Angles (°)', param_x_name='Wavelengths (nm)', values_name='Intensity {} (u.a.)'.format(step))
                
            return np.array(optm_result)
        
        elif step == "cos_2_2": 
            # Create the initial parameters
            optm_result = []
            # If single measurement
            if single: 
                I = Iexp # /cal_dict['S0']
                (Imax, Imin) = (np.max(I), np.min(I))
                bounds = ([0, 0, 0*degrees], [Imax, Imax, 180 * degrees])
                par0 = [np.max((0,Imin)), Imax - np.abs(Imin),  45 *degrees]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("cos_2_2", angles, I), bounds=bounds)
                optm_result.append(result.x)
            # If multiple measurements
            else: 
                I = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    I[:,i] = Iexp[:,i] # /cal_dict['S0'][i]
                    (Imax, Imin) = (np.max(I[:,i]), np.min(I[:,i]))
                    bounds = ([0, 0, -180*degrees], [Imax+1,Imax+1, 180 * degrees])
                    par0 = [np.max((0,Imin)), Imax - np.abs(Imin), 75 *degrees]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, args=("cos_2_2", angles, I[:,i]), bounds=bounds)
                    optm_result.append(result.x)
                    print("Medida {} de {}".format(i+1, len(I[0,:])), end="\r")

            # Plot result and print results
            if single: 
                optm_result = optm_result[0]
                
                Imodel = model_cos2_2(optm_result, angles)
                
                utils.plot_experiment_residuals_1D(angles, I, Imodel, title='Step cos_2_2')
                print('The values obtained are:')
                print('   - Imax       : {:.4f} V'.format(optm_result[1] + optm_result[0]))
                print('   - Imin       : {:.4f} V'.format(optm_result[0]))
                print('   - Maximum angle : {:.2f} deg'.format(optm_result[2]/degrees))  
            else: 
                Imodel = np.zeros_like(Iexp)
                for i in range(len(Iexp[0,:])):
                    Imodel[:,i] = model_cos2_2(optm_result[i], angles)
                draw_values(cal_dict['wavelengths'], angles/degrees, Iexp, Imodel, param_y_name='Angles (°)', param_x_name='Wavelengths (nm)', values_name='Intensity {} (u.a.)'.format(step))
                
                print('The values obtained are:')
                print('   - Maximum angle : {:.2f} deg'.format(np.mean(np.array(optm_result)[:,2]/degrees))) 
                
            return np.array(optm_result)
        
        elif step == "2":
            # Create the initial parameters
            optm_result = []
            # Bounds
            bound_up = np.array([
                1,                  # R2_p1 
                1,                  # R2_p2  
                180*degrees,        # R2_Ret
                15*degrees,          # R2_az
            ])
            bound_down = np.array([
                0,                  # R2_p1 
                0,                  # R2_p2
                0,                  # R2_Ret 
                -15*degrees,         # R2_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares initial parameters
            par0 = (1,1,90*degrees,0*degrees)
            # Single measurement
            if single: 
                args = ['2', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            # Multiple measurements
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['2', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)
        
        elif step == "2_global":
            # Create the initial parameters
            optm_result = []
            # Bounds
            bound_up = np.array([
                1,                  # P1_p2 
                180*degrees,        # P1_Ret
                1,                  # R2_p1 
                1,                  # R2_p2  
                180*degrees,        # R2_Ret
                15*degrees,          # R2_az
            ])
            bound_down = np.array([
                0,                  # P1_p2 
                0,                  # P1_Ret 
                0,                  # R2_p1 
                0,                  # R2_p2
                0,                  # R2_Ret 
                -15*degrees,         # R2_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares initial parameters
            par0 = (1,90*degrees,1,1,90*degrees,0*degrees)

            # Single measurement
            if single: 
                args = ['2_global', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            # Multiple measurements
            else: 
                
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i)
                    args = ['2_global', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)

        elif step == "3":
            # Define the system
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], R = cal_dict['P2_Ret'], azimuth=-cal_dict["P2_az"]*np.ones_like(cal_dict["P2_Ret"]), ellipticity=np.zeros_like(cal_dict["P2_Ret"]))
            Mr2 = Mueller().diattenuator_retarder_linear(p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-cal_dict["R2_az"]*np.ones_like(cal_dict["R2_Ret"]))
            system = (Mr2, Mp2)
            # Calculate the Stokes parameters
            if single is False: 
                illum_I = []
                illum_az = []
                illum_el = []
                illum_pol_degree = []
                for i in range(len(cal_dict["wavelengths"])): 
                    system = (Mr2[i], Mp2[i])
                    S = Calculate_Stokes(I = Iexp[:,i], angles = angles, system = system)
                    illum_I.append(S.parameters.intensity())
                    illum_az.append(S.parameters.azimuth(use_nan = False))
                    illum_el.append(S.parameters.ellipticity_angle())
                    illum_pol_degree.append( np.round(S.parameters.degree_polarization(),10))
            # Single measurement
            if single: 
                S = Calculate_Stokes(I = Iexp, angles = angles, system = system)
                illum_I = S.parameters.intensity()
                illum_az = S.parameters.azimuth(use_nan = False)
                illum_el = S.parameters.ellipticity_angle()
                illum_pol_degree = np.round(S.parameters.degree_polarization(),10)
                
                print('Azimuth: ', S.parameters.azimuth(use_nan = False)/degrees)
                print("Intensidad: ", S.parameters.intensity())
                print("Ángulo de elipticidad: ", S.parameters.ellipticity_angle()/degrees)
                print("Grado de polarización: ", np.round(S.parameters.degree_polarization(),10))
            
            optm_result = [illum_I, illum_az, illum_el, illum_pol_degree]
            return np.array(optm_result)


        elif step == "4":
            # Create the initial parameters
            optm_result = []
            # Bounds
            bound_up = np.array([
                1,                  # R1_p1 
                1,                  # R1_p2  
                180*degrees,        # R1_Ret
                15*degrees,          # R1_az
            ])
            bound_down = np.array([
                0,                  # R1_p1 
                0,                  # R1_p2
                0,                  # R1_Ret 
                -15*degrees,         # R1_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares initial parameters
            par0 = (1,1,90*degrees,0*degrees)
            # Single measurement
            if single: 
                args = ['4', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            # Multiple measurements
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i, step = '4')
                    args = ['4', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)
        
        elif step == "4_global":
            # Create the initial parameters
            optm_result = []
            # Bounds
            bound_up = np.array([
                1,                  # P2_p2 
                180*degrees,        # P2_Ret
                1,                  # R1_p1 
                1,                  # R1_p2  
                180*degrees,        # R1_Ret
                15*degrees,          # R1_az
            ])
            bound_down = np.array([
                0,                  # P2_p2 
                0,                  # P2_Ret 
                0,                  # R1_p1 
                0,                  # R1_p2
                0,                  # R1_Ret 
                -15*degrees,         # R1_az
            ])

            bounds = (bound_down, bound_up)

            # Least squares initial parameters
            par0 = (1,90*degrees,1,1,90*degrees,0*degrees)
            # Single measurement
            if single: 
                args = ['4_global', None, None, cal_dict]
                result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args, ftol = 1e-10)
                optm_result.append(result.x)
            # Multiple measurements
            else: 
                for i in range(len(cal_dict["wavelengths"])):
                    cal_dict_temp = create_temp_dict(cal_dict, i, step = '4')
                    args = ['4_global', None, None, cal_dict_temp]
                    result = least_squares(SpectralPolarimeterCalibration.error_step, par0, bounds=bounds, args=args)
                    optm_result.append(result.x)
                    print("Medida {} de {} : {}".format(i+1, len(cal_dict["wavelengths"]), np.round(result.x,3)), end="\r")         
            
            return np.array(optm_result)
        
        elif step == "5":

            # Define the system
            
            Mp1 = Mueller().diattenuator_retarder_azimuth_ellipticity(p1=cal_dict["P1_p1"], p2=cal_dict["P1_p2"], R=cal_dict['P1_Ret'], azimuth=-np.zeros_like(cal_dict['P1_Ret']), ellipticity=np.zeros_like(cal_dict['P1_Ret']))
    
            Mr1 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R1_p1"], p2=cal_dict["R1_p2"], R=cal_dict["R1_Ret"], azimuth=-np.zeros_like(cal_dict['P1_Ret']))
            
            Mp2 = Mueller().diattenuator_retarder_azimuth_ellipticity(p1=cal_dict["P2_p1"], p2=cal_dict["P2_p2"], R=cal_dict['P2_Ret'], azimuth=-np.zeros_like(cal_dict['P1_Ret']), ellipticity=np.zeros_like(cal_dict['P1_Ret']))
            
            Mr2 = Mueller().diattenuator_retarder_linear(
                p1=cal_dict["R2_p1"], p2=cal_dict["R2_p2"], R=cal_dict["R2_Ret"], azimuth=-np.zeros_like(cal_dict['P1_Ret']))
            
            S = Stokes().general_azimuth_ellipticity(
                intensity=cal_dict["S1"],
                azimuth=cal_dict["S1_az"],
                ellipticity=cal_dict["S1_el"],
                degree_pol=cal_dict["S1_pol_degree"])
            
            # Calculate the Mueller matrix for the system

            # M = Calculate_Mueller_Matrix_0D(I=Iexp, angles=angles, Ms=system)
            if single:
                system = [S, Mp1, Mr1, Mr2, Mp2]
                M = Calculate_Mueller(I = Iexp, angles=angles, system = system, system_aux=None, filter=True)
            else:
                # M = []
                M = Mp1
                for i in range(len(cal_dict["wavelengths"])):
                    system = [S[i], Mp1[i], Mr1[i], Mr2[i], Mp2[i]]
                    M[i] = Calculate_Mueller(I = Iexp[:,i], angles=angles, system = system, system_aux=None, filter=True)

              
            print("The Mueller matrix is:\n", M)
            cal_dict["M"] = M.M
            return M
        
        else:
            return "Error. Step not recognized"
        
    def p2_Ret_Cal(Iexp_a, angles_a, Iexp_c, angles_c, cal_dict, step = '2'):

        """Function to get p2 parameter and Retardance of a polarizer. 
        
        Args:
            Iexp_a (np.ndarray): Array with the intensities of the step a.
            angles_a (np.ndarray): Array with the angles of the step a.
            Iexp_c (np.ndarray): Array with the intensities of the step c.
            angles_c (np.ndarray): Array with the angles of the step c.
            cal_dict (dict): Dictionary with the calibration configuration.
            step (string): Step to analyze. Default: '2'.
            
        Returns:
            float: p2 parameter.
            float: Retardance of the polarizer.
            dict: Dictionary with the calibration configuration."""

        # Calculate the angle of the polarizer
        angle_c = cal_dict['P0_az'] + 45*degrees

        # Calculate the index of the reference angle
        if step == '2': 
            angle = angle_c - cal_dict['P1_az'] + cal_dict['R2_az']
            indice_ref_a = np.argmin(np.abs(angles_a - cal_dict['R2_az']))
            
        elif step == '4': 
            angle = angle_c - cal_dict['P2_az'] + cal_dict['R1_az']
            indice_ref_a = np.argmin(np.abs(angles_a - cal_dict['R1_az']))

        indice_ref_c = np.argmin(np.abs(angles_c - angle))

        # Reorder the arrays
        Iexp_a_r = np.concatenate((Iexp_a[indice_ref_a:], Iexp_a[:indice_ref_a]))
        Iexp_c_r = np.concatenate((Iexp_c[indice_ref_c:], Iexp_c[:indice_ref_c]))

        # Calculate the minimum values of the intensity for the step a        
        result = SpectralPolarimeterCalibration.analyze_step(step = 'polyfit', cal_dict = cal_dict, Iexp = Iexp_a_r, angles = angles_a, single = True, verbose = False)
        I_model = model_polyfit(result, np.linspace(angles_a[0], angles_a[-1], len(angles_a)*10000))
        min_a_1 = np.min(I_model[:len(I_model)//2])
        min_a_2 = np.min(I_model[len(I_model)//2:])

        # Calculate the minimum values of the intensity for the step c
        result = SpectralPolarimeterCalibration.analyze_step(step = 'polyfit', cal_dict = cal_dict, Iexp = Iexp_c_r, angles = angles_c, single = True, verbose = False)
        I_model = model_polyfit(result, np.linspace(angles_c[0], angles_c[-1], len(angles_c)*10000))
        min_c_1 = np.min(I_model[:len(I_model)//2])
        min_c_2 = np.min(I_model[len(I_model)//2:])
        
        # Calculate the p2 parameter and the Retardance of the polarizer
        if step == '2':
            cal_dict['P1_Ret'] = (np.arctan2((min_c_2-min_c_1),(min_a_2-min_a_1)))
            P1_p2_1 = (min_a_2-min_a_1) / (cal_dict['S1']*cal_dict['P1_p1']*np.cos(cal_dict['P1_Ret'])*cal_dict['P2_p1']**2)
            P1_p2_2 = (min_c_2-min_c_1) / (cal_dict['S0']*cal_dict['P1_p1']*np.sin(cal_dict['P1_Ret'])*cal_dict['P2_p1']**2)
            P1_p2 = (P1_p2_2+P1_p2_1)/2
            cal_dict['P1_p2'] = P1_p2
            return cal_dict['P1_Ret'], P1_p2, cal_dict
        elif step == '4':
            cal_dict['P2_Ret'] = (np.arctan2((min_c_2-min_c_1),(min_a_2-min_a_1)))
            P2_p2_1 = (min_a_2-min_a_1) / (cal_dict['S1']*cal_dict['P2_p1']*np.cos(cal_dict['P2_Ret'])*cal_dict['P1_p1']**2)
            P2_p2_2 = (min_c_2-min_c_1) / (cal_dict['S0']*cal_dict['P2_p1']*np.sin(cal_dict['P2_Ret'])*cal_dict['P1_p1']**2)
            P2_p2 = (P2_p2_2+P2_p2_1)/2
            cal_dict['P2_p2'] = P2_p2
            return cal_dict['P2_Ret'], P2_p2, cal_dict
        

    def p2_Ret_Cal_deprecated(Iexp_a, angles_a, Iexp_c, angles_c, cal_dict, dist = 10, step = '2'):

        """Function to get p2 parameter and Retardance of a polarizer.
        
        Args:
            Iexp_a (np.ndarray): Array with the intensities of the step a.
            angles_a (np.ndarray): Array with the angles of the step a.
            Iexp_c (np.ndarray): Array with the intensities of the step c.
            angles_c (np.ndarray): Array with the angles of the step c.
            cal_dict (dict): Dictionary with the calibration configuration.
            dist (int): Distance to calculate the minimum of the intensity. Default: 10.
            step (string): Step to analyze. Default: '2'.
            
        Returns:
            float: p2 parameter.
            float: Retardance of the polarizer.
            dict: Dictionary with the calibration configuration.
            """
        
        angle_c = cal_dict['P0_az'] + 45*degrees

        if step == '2': 
            angle = angle_c - cal_dict['P1_az'] + cal_dict['R2_az']
            indice_ref_a = np.argmin(np.abs(angles_a - cal_dict['R2_az']))
            
        elif step == '4': 
            angle = angle_c - cal_dict['P2_az'] + cal_dict['R1_az']
            indice_ref_a = np.argmin(np.abs(angles_a - cal_dict['R1_az']))

        Iexp_a_r = np.concatenate((Iexp_a[indice_ref_a:], Iexp_a[:indice_ref_a]))

        indice_ref_c = np.argmin(np.abs(angles_c - angle))
        Iexp_c_r = np.concatenate((Iexp_c[indice_ref_c:], Iexp_c[:indice_ref_c]))

        arg_min_a_1 = np.argmin(Iexp_a_r[:len(Iexp_a_r)//2])
        arg_min_a_2 = np.argmin(Iexp_a_r[len(Iexp_a_r)//2:]) + len(Iexp_a_r)//2
        arg_min_c_1 = np.argmin(Iexp_c_r[:len(Iexp_c_r)//2])
        arg_min_c_2 = np.argmin(Iexp_c_r[len(Iexp_c_r)//2:]) + len(Iexp_c_r)//2

        result = SpectralPolarimeterCalibration.analyze_step(step = 'polyfit', cal_dict = cal_dict, Iexp = Iexp_a_r[arg_min_a_1-dist:arg_min_a_1+dist], angles = angles_a[arg_min_a_1-dist:arg_min_a_1+dist], single = True, verbose = False)
        # min_a_1 = (-result[1]**2)/(4*result[0]) + result[2]       
        min_a_1 = result[0] + result[2]

        result = SpectralPolarimeterCalibration.analyze_step(step = 'polyfit', cal_dict = cal_dict, Iexp = Iexp_a_r[arg_min_a_2-dist:arg_min_a_2+dist], angles = angles_a[arg_min_a_2-dist:arg_min_a_2+dist], single = True, verbose = False)
        # min_a_2 = (-result[1]**2)/(4*result[0]) + result[2]
        min_a_2 = result[0] + result[2]

        result = SpectralPolarimeterCalibration.analyze_step(step = 'polyfit', cal_dict = cal_dict, Iexp = Iexp_c_r[arg_min_c_1-dist:arg_min_c_1+dist], angles = angles_c[arg_min_c_1-dist:arg_min_c_1+dist], single = True, verbose = False)
        # min_c_1 = (-result[1]**2)/(4*result[0]) + result[2]
        min_c_1 = result[0] - result[2]

        result = SpectralPolarimeterCalibration.analyze_step(step = 'polyfit', cal_dict = cal_dict, Iexp = Iexp_c_r[arg_min_c_2-dist:arg_min_c_2+dist], angles = angles_c[arg_min_c_2-dist:arg_min_c_2+dist], single = True, verbose = False)
        # min_c_2 = (-result[1]**2)/(4*result[0]) + result[2]
        min_c_2 = result[0] - result[2]

        if step == '2':
            cal_dict['P1_Ret'] = (np.arctan2((min_c_2-min_c_1),(min_a_2-min_a_1)))
            P1_p2_1 = (min_a_2-min_a_1) / (cal_dict['S1']*cal_dict['P1_p1']*np.cos(cal_dict['P1_Ret'])*cal_dict['P2_p1']**2)
            P1_p2_2 = (min_c_2-min_c_1) / (cal_dict['S0']*cal_dict['P1_p1']*np.sin(cal_dict['P1_Ret'])*cal_dict['P2_p1']**2)
            print(P1_p2_1, P1_p2_2)
            P1_p2 = (P1_p2_2+P1_p2_1)/2
            cal_dict['P1_p2'] = P1_p2
            return cal_dict['P1_Ret'], P1_p2, cal_dict
        elif step == '4':
            cal_dict['P2_Ret'] = (np.arctan2((min_c_2-min_c_1),(min_a_2-min_a_1)))
            P2_p2_1 = (min_a_2-min_a_1) / (cal_dict['S1']*cal_dict['P2_p1']*np.cos(cal_dict['P2_Ret'])*cal_dict['P1_p1']**2)
            P2_p2_2 = (min_c_2-min_c_1) / (cal_dict['S1']*cal_dict['P2_p1']*np.sin(cal_dict['P2_Ret'])*cal_dict['P1_p1']**2)
            P2_p2 = P2_p2_2
            cal_dict['P2_p2'] = P2_p2
            return cal_dict['P2_Ret'], P2_p2, cal_dict


############################################################################################################################################################################

# SPECTRAL POLARIMETER CLASS

class SpectralPolarimeter(object):
    
    """
    Class for spectral polarimeter. 
    It will include control of the rotary motors of PSG and PSA elements, the spectrometer for measuring signal.

    Args:
        calibration (string): Load all calibration files ("full"), only the corresponding to illumination, PSG and PSA ("minimal") or none ("none"). Default: "full".
        conf_dict (dict): Dictionary with the calibration configuration. Default: CONF_POLARIMETER.
        name_motor (string): Name of the rotating motors used. Default: "DT50".
        name_spectrometer (string): Name of the spectrometer used. Default: "AVANTES".

    Atributes:
        motor (Motor_Multiple): Set of motors object.
        daca (DACA): Data acquisition card object.
        norm (float): Intensity normalization constant.
        I_Distribution (np.ndarray): Intensity normalization array.
    """
    def __init__(self, calibration="full", conf_dict=CONF_POLARIMETER, name_motor="DT50", name_spectrometer="Avantes"): 

        """Initialize the class.
        
        Args:
            calibration (string): Load all calibration files ("full"), only the corresponding to illumination, PSG and PSA ("minimal") or none ("none"). Default: "full".
            conf_dict (dict): Dictionary with the calibration configuration. Default: CONF_POLARIMETER.
            name_motor (string): Name of the rotating motors used. Default: "DT50".
            name_spectrometer (string): Name of the spectrometer used. Default: "AVANTES".
        
        Returns:
            None
        """

        # Initialize the objects
        self.motor = Motor_Multiple(name=name_motor, N=4)
        self.spectrometer = Spectrometer(name=name_spectrometer)
        
        self.name_motor = name_motor 
        self.name_spectrometer = name_spectrometer

        # # Load calibration info
        if calibration.lower() == "full":
             self.Load_Calibration(filename=conf_dict['cal_file'], folder=None) #conf_dict["cal_folder"])

    def Open(self): 

        """Open all devices."""

        # Motor configuration
        if self.name_motor == "DT50":
            config_motor = CONF_DT_50
        
        elif self.name_motor == "InteliDrives":
            config_motor = CONF_INTEL

        elif self.name_motor == "Zaber":
            config_motor = CONF_ZABER
        
        if self.name_spectrometer == "Avantes":
            config_spectrometer = CONF_AVANTES
        
        # Open the motor
        self.motor.Open(port=config_motor["ports"], invert=config_motor["invert"],axis=config_motor["axes"])
        self.motor.Home()
        print('\n')
        
        # Open the spectrometer
        self.spectrometer.Open()
        self.spectrometer.Get_Wavelength()
        self.spectrometer.Get_Number_Px()
        self.spectrometer.Set_Parameters(exposure = config_spectrometer["exposure"], N_average= config_spectrometer["N_average"])   

    def Load_Calibration(self, filename=None, folder=None): 

        """Load some or all calibration files. If any of the filename parameters is None, that calibration will be ignored.

        Args:
            filename (str or None): Filename of the main calibration. Default: None.
            folder (str or None): Filename of the main calibration. Default: None.

            
        Returns:
            None
        """
        # Go to calibration folder
        if folder:
            try:
                old_folder = os.getcwd()
                os.chdir(folder)
            except:
                print("Folder {} nonexistent".format(folder))

        # Main calibration
        if filename:
            try: 
                data = dict(np.load(filename))

                self.angles_0 = np.array([data["P1_az"], data["R1_az"], data["R2_az"], data["P2_az"]])

                self.S = Stokes().general_azimuth_ellipticity(intensity = data['S1'], azimuth=data["S1_az"], ellipticity=data["S1_el"], degree_pol=data["S1_pol_degree"])
                self.P1 = Mueller().diattenuator_retarder_azimuth_ellipticity(p1=data["P1_p1"], p2=data["P1_p2"], R=data["P1_Ret"], azimuth=-data["P1_az"])
                self.R1 = Mueller().diattenuator_retarder_linear(p1=data["R1_p1"], p2=data["R1_p2"], R=data["R1_Ret"], azimuth=-data["R1_az"])
                self.R2 = Mueller().diattenuator_retarder_linear(p1=data["R2_p1"], p2=data["R2_p2"], R=data["R2_Ret"], azimuth=-data["R2_az"])
                self.P2 = Mueller().diattenuator_retarder_azimuth_ellipticity(p1=data["P2_p1"], p2=data["P2_p2"], R=data["P2_Ret"], azimuth=-data["P2_az"])

                self.cal_dict = data

            except:
                loc = os.getcwd()
                print("File {} at location {} nonexistent".format(filename, loc))
                return

        if folder:
            try:
                os.chdir(old_folder)
            except:
                print("Unable to return to original folder")

        
    def Ready(self):
        """Put the rotary motors at the position of all elements axes paralel to X axis (position 0 if polarimeter is not calibrated)."""
        self.motor.Move_Absolute(pos=np.zeros(4), units="rad")
        pass

    def Close(self):
        """Close all devices."""
        self.motor.Close()
        self.spectrometer.Close()
    
    def Measure_ND_Mueller_Matrix(self, I=None, angles="perfect", Nmeasures=144, 
                                   is_ref=False, filename="Sample", 
                                   add_angle_0=False, filter = False, exposure_time = 25,   verbose=True):

        """New function to measure the Mueller matrix 
            
            Args:
                I (np.ndarray or None) If None, the intensities are measured. Default: None.
                angles (Nx4 np.ndarray or str): Nx4 array of angles. If I is None so the intensities are going to be measured, angles can be a string with the method to calculate the angles. Default: 'perfect'.
                    "perfect": PSG and PSA states distributed in the poincare sphere as a regular solid. Algorithm calculates the nearest lower number of measurements to match two regular solids.
                    "spiral": PSG and PSA states distributed in the poincare sphere as a spiral with the same number of states in each case. N will be reduced to a number in the form of M^2 where M is an integer.
                    "random": Random angles.
                    'linspace': Angles generated by linspace. N will be reduced to a number in the form of M^4 where M is an integer.
                Nmeasures (int): Number of measurements used (when I is None). Default: 144.
                is_ref (bool): If True, this measurement will be used as reference for adjusting intensity. Default: False.
                filename (str or None): If not None, the filename where the measurement is stored. Default: None.
                add_angle_0 (bool): If True, add original angles in the motor positions. Default: False
                verbose (bool): If True, the information is printed after performing the measurement. Default: True.

            Returns:
                result (Mueller): Measured Mueller matrix.
        """
            
        # Normalization not performed
        if not is_ref and self.norm == 1:
            print("WARNING: Normalization not performed")

        # Make measurement
        if I is None:
            
            # Generate angles
            angles = utils.calculate_polarimetry_angles(Nmeasures, type=angles)

            # Initialize I
            I = []
            
            # Measurement
            angles_def = np.zeros((Nmeasures, 4))

            for ind, angles_m in enumerate(angles): 
                                    
                if add_angle_0:
                    angles_m = angles_m + self.angles_0
                
                angles_def[ind] = self.motor.Move_Absolute(pos=angles_m, waiting='busy', units="rad")


                I.append(self.spectrometer.Get_Measurement(mode = "wavelength", wavelength = self.cal_dict['wavelengths']))
                
                utils.percentage_complete(ind + 1, Nmeasures)
            # Save Intensities in array
            Inorm = (np.array(I))

        # Use given intensities    
        else:                
            angles_def = angles
            Inorm = I

        # Define system from calibration dictionary
        system = [self.S, self.P1, self.R1, self.R2, self.P2]
        
        # Calculate Mueller matrix
        M = Calculate_Mueller(I = Inorm, angles=angles_def, system = system, filter=filter)
        
        # Normalize Mueller matrix
        if is_ref:
            self.norm = M.M[0,0,0]
        if not is_ref: 
            M = M / self.norm
        
        # Save data
        if filename:
            if filename.lower() == "auto":
                filename = "Mueller_0D_{}".format(datetime.date.today())

        np.savez(filename, angles=angles_def, I=Inorm, norm=self.norm, exposure_time = exposure_time, Mcomp=np.array(M.parameters.components()), date=datetime.date.today())

        # Print information
        if verbose:
            M.name = filename or "Sample"
            print("The Mueller matrix is:\n", M)
            # M.analysis.decompose_polar(verbose=True)
    
        return M



############################################################################################################################################################################

# SPECTRAL POLARIMETER UTILS CLASS

# class SpectralPolarimeter_utils(object):
    
#     """
#     Class for spectral polarimeter utilities. 
        
#     Args:
        

#     Atributes:
        
#     """

#     def Draw_Mueller_Matrix(): 
#         pass
#     def Calculate_Mueller_Matrix(): 
#         pass
#     def MSystem_from_dict():
#         pass   
#     def Calculate_Stokes():
#         pass
#     def Analyze_Mueller_Matrix():
#         pass
    
    ############################################################################################################################################################################


# OTHER FUNCTIONS


def model_cos2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(angle - par[2])**2
    return Imodel

def error_cos2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2(par, angle)
    dif = Iexp - Imodel
    return dif

def model_cos2_2(par, angle):
    """Funcion cos**2 with parameters."""
    Imodel = par[0] + par[1] * np.cos(2*(angle - par[2]))**2
    return Imodel

def error_cos2_2(par, angle, Iexp):
    """Function that serves as optimization for cos**2."""
    Imodel = model_cos2_2(par, angle)
    dif = Iexp - Imodel
    return dif

def model_polyfit(par, angle):
    """Function polyfit with parameters."""
    return par[0] + par[1] * np.cos(2*(angle-par[4]))**2 + par[2] * np.sin(2*(angle-par[4])) + par[3] * np.cos(2*(angle-par[3])) 

def error_polyfit(par, angle, Iexp):
    """Function that serves as optimization for polyfit."""
    Imodel = model_polyfit(par, angle)
    dif = Iexp - Imodel
    return dif

def model_polyfit_deprecated(par, angle):
    """Function polyfit with parameters."""
    return par[0] + par[1] * np.cos(2*(angle-par[4]))**2 + par[2] * np.sin(2*(angle-par[4]))

def error_polyfit_deprecated(par, angle, Iexp):
    """Function that serves as optimization for polyfit."""
    Imodel = model_polyfit_deprecated(par, angle)
    dif = Iexp - Imodel
    return dif

def draw_values(param_x, param_y, values, values_2 = None, param_x_name='Param_X', param_y_name='Param_Y', values_name='Values'):
    """Draw the values in 3d projection plot of the polarimeter.
    
    Args:
        param_x (np.ndarray): Array with the values of the x axis.
        param_y (np.ndarray): Array with the values of the y axis.
        values (np.ndarray): Array with the values to plot.
        values_2 (np.ndarray): Array with the values to plot. Default: None.
        param_x_name (str): Name of the x axis. Default: 'Param_X'.
        param_y_name (str): Name of the y axis. Default: 'Param_Y'.
        values_name (str): Name of the values. Default: 'Values'.
        
    Returns:
        None"""

    fig = plt.figure(figsize=(5, 5))
    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.meshgrid(param_x, param_y[:, np.newaxis])
    ax.plot_surface(X, Y, values, cmap='Blues')
    if values_2 is not None:
        ax.plot_surface(X, Y, values_2, cmap='Reds')
    ax.set_xlabel(param_x_name)
    ax.set_ylabel(param_y_name)
    ax.set_zlabel(values_name)
    ax.set_title(values_name)
    return 

def create_temp_dict(cal_dict, value, step = None):

    """Create a dictionary with the same keys as cal_dict but with the value given.
    
    Args:
        cal_dict (dict): Dictionary with the calibration configuration.
        value (int): Value to put in the dictionary.
        step (string): Step to analyze. Default: None.
        
    Returns:
        cal_dict_temp (dict): Dictionary with the calibration configuration."""

    cal_dict_temp = cal_dict.copy()
    cal_dict_temp['wavelengths'] = cal_dict['wavelengths'][value]
    cal_dict_temp['S0'] = cal_dict['S0'][value]
    cal_dict_temp['S0_az'] = cal_dict['S0_az']
    cal_dict_temp['S0_el'] = cal_dict['S0_el'][value]
    cal_dict_temp['S0_pol_degree'] = cal_dict['S0_pol_degree'][value]
    cal_dict_temp['S0_error'] = cal_dict['S0_error'][value]
    cal_dict_temp['S1'] = cal_dict['S1'][value]
    cal_dict_temp['S1_error'] = cal_dict['S1_error'][value]
    cal_dict_temp['S1_az'] = cal_dict['S1_az'][value]
    cal_dict_temp['S1_el'] = cal_dict['S1_el'][value]
    cal_dict_temp['S1_pol_degree'] = cal_dict['S1_pol_degree'][value]
    cal_dict_temp['P0_az'] = cal_dict['P0_az']
    cal_dict_temp['P1_p1'] = cal_dict['P1_p1'][value]
    cal_dict_temp['P1_p2'] = cal_dict['P1_p2'][value]
    cal_dict_temp['P1_az'] = cal_dict['P1_az']
    cal_dict_temp['P1_Ret'] = cal_dict['P1_Ret'][value]
    cal_dict_temp['P2_p1'] = cal_dict['P2_p1'][value]
    cal_dict_temp['P2_p2'] = cal_dict['P2_p2'][value]
    cal_dict_temp['P2_az'] = cal_dict['P2_az']
    cal_dict_temp['P2_Ret'] = cal_dict['P2_Ret'][value]
    cal_dict_temp['Pc_p1'] = cal_dict['Pc_p1'][value]
    cal_dict_temp['Pc_p2'] = cal_dict['Pc_p2'][value]
    cal_dict_temp['R1_p1'] = cal_dict['R1_p1'][value]
    cal_dict_temp['R1_p2'] = cal_dict['R1_p2'][value]
    cal_dict_temp['R1_az'] = cal_dict['R1_az']
    cal_dict_temp['R1_Ret'] = cal_dict['R1_Ret'][value]
    cal_dict_temp['R2_p1'] = cal_dict['R2_p1'][value]
    cal_dict_temp['R2_p2'] = cal_dict['R2_p2'][value]
    cal_dict_temp['R2_az'] = cal_dict['R2_az']
    cal_dict_temp['R2_Ret'] = cal_dict['R2_Ret'][value]
    cal_dict_temp['Rc_p1'] = cal_dict['Rc_p1'][value]
    cal_dict_temp['Rc_p2'] = cal_dict['Rc_p2'][value]
    cal_dict_temp['Rc_Ret'] = cal_dict['Rc_Ret'][value]
    cal_dict_temp['Rc_offset'] = cal_dict['Rc_offset'][value]
    cal_dict_temp['I_step_2a'] = cal_dict['I_step_2a'][:,value]
    cal_dict_temp['I_step_2b'] = cal_dict['I_step_2b'][:,value]
    cal_dict_temp['I_step_2c'] = cal_dict['I_step_2c'][:,value]
    
    if step == '4':
        cal_dict_temp['I_step_4a'] = cal_dict['I_step_4a'][:,value]
        cal_dict_temp['I_step_4b'] = cal_dict['I_step_4b'][:,value]
        cal_dict_temp['I_step_4c'] = cal_dict['I_step_4c'][:,value]
    
    return cal_dict_temp
    
def Calculate_Stokes(I, angles, system):
    """Function to calculate the Mueller matrix from the intensity measurements.

    Args:
        I (np.array): First dimension is the corresponding to different measurements.
        angles (Nx2 np.array): Angles array.
        system (list) List with the Stokes and Mueller objects for illumination, R2 and P2.

    Returns:
        result (Stokes): Result.
    """
    
    R2, P2 = system
    Mr2_rot = R2.rotate(angle=angles[1], keep=True)
    Mp2_rot = P2.rotate(angle=angles[0], keep=True)
    
    PSA = Mp2_rot * Mr2_rot
    a = PSA.M[0,:,:].T
    
    at = a.T
    ai = np.linalg.inv(at @ a) @ at
    s = np.dot(ai,I[:, np.newaxis])

    S = Stokes()
    
    S.from_components(components=s)
    
    S = S.analysis.filter_physical_conditions(tol = 1e-10)

    return S

    

    
    