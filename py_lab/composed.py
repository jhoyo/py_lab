"""This file includes several classes used to control systems composed by two or
more subsystems. For example, a camera mounted on top of a translation stage
corresponds to the the class Motorized_Camera, which can control both the camera
and the motor of the stage.This allows more options like acquiring multiple
images at differnt positions."""

import numpy as np
import matplotlib.pyplot as plt
import os
from datetime import datetime
import time

from py_lab.config import CONF_IMAGING_SOURCE
#import py_lab.camera.ImagingSource.tisgrabber as IC
import py_lab.utils as utils
from py_lab.camera import Camera
from py_lab.motor import Motor

DEFAULT_MOVE_TIME = 1  # 1 second
DEFAULT_POS_UNITS = 'mm'


class Motorized_Camera(object):
    """
    General class for Motorized_Camera

    Args:
        name_camera (string): Name of the camera.
        name_motor (string): Name of the motor. Available motor is: SMC100.

    Atributes:
        camera (Camera): Name of the camera.
        motor (Motor): Name of the motor.

    Supported devices:
        * ImagingSource.
        * SMC100.
    """

    def __init__(self, name_camera="ImagingSource", name_motor="SMC100"):
        """Initialize the object."""
        self.camera = Camera(name_camera)
        self.motor = Motor(name_motor)

    def __del__(self):
        """Delete object last function."""
        self.camera.Close()
        self.motor.Close()

    def Get_Multiple_Images(self,
                            number,
                            dist_increase,
                            x0=None,
                            returns=True,
                            units=DEFAULT_POS_UNITS,
                            one_direction=True,
                            draw=False):
        """
        Extract multiple photograms as numpy arrays

        Args:
            number (int): Number of images you want to extract.
            dist_increase (float): Movement of the camera between images.
            x0 (float): Initial Position of the camera. Default: Actual Position.
            return (bool): If True return to initial position. Default: True
            units (str): Position units to choose between m, mm and um. Default: DEFAULT_POS_UNITS.
            one_direction (bool): If True the camera only moves in one direction. If False half of the images are taken in one direction and the other half in the opposite direction. Default: True
            draw (bool):  If True, plot the image. Default: False.

        Output:
            result (np.arrays): Measured photogram.

        """
        imagenes = []
        # Si no le damos una posición inicial, toma por defecto en la que se encuentra el motor.
        if x0 is None:
            x0 = self.motor.Get_Position(units=units, verbose=False)
            self.x0 = x0

        # Te mueves a la posición inicial
        self.motor.Move_Absolute(
            pos=x0, units=units, move_time=DEFAULT_MOVE_TIME, verbose=True)

        # Si quieres tomar imágenes en dos direcciones, te mueves a un extremo.
        if not one_direction:
            movement = -int(number / 2)
            self.motor.Move_Relative(
                dist=movement * dist_increase,
                units=units,
                move_time=DEFAULT_MOVE_TIME,
                verbose=False)

        for ind in range(0, number):
            # Take an image
            image = self.camera.Get_Image(monochanel=False, draw=False)
            imagenes.append(image)
            filename = "Imagen_{}.npz".format(ind)
            np.savez(filename, image=image)
            time.sleep(0.01)

            if draw:
                limit = self.camera.resolution * self.camera.pixel_size
                extent = np.array(
                    [-limit[0] / 2, limit[0] / 2, -limit[1] / 2, limit[1] / 2])
                plt.figure()
                plt.imshow(image, extent=extent)

            # Move the camera a certain distance
            self.motor.Move_Relative(
                dist=dist_increase,
                units=units,
                move_time=DEFAULT_MOVE_TIME,
                verbose=False)

        # Return to initial position
        if returns:
            self.motor.Move_Absolute(
                pos=x0, units=units, move_time=DEFAULT_MOVE_TIME, verbose=True)

        return imagenes
